#!/usr/bin/python2

import commands
import glob
import os
import sys
import tempfile

name = 'blender%s_%s.zip'

def cdFirsDir():
	os.chdir(glob.glob("*")[0])

def build_windows(source, destination):
	#print("build_windows", source, destination)
	blender_path = 'rend.exe'
	
	working_dir = tempfile.mkdtemp()
	os.chdir(working_dir)
	commands.getoutput("unzip %s"%(source))
	cdFirsDir()
	commands.getoutput("mv blender.exe rend.exe")
	commands.getoutput("rm -f blenderplayer.exe")
	commands.getoutput("mkdir portable")
	
	if os.path.exists(blender_path) == False:
		print("build windows failed, missing %s"%(blender_path))
		sys.exit(4)
	
	commands.getoutput("zip -r %s ."%(destination))
	os.chdir("/")
	commands.getoutput("rm -rf %s"%(working_dir))

def build_mac(source, destination):
	#print("build_mac", source, destination)
	commands.getoutput("wget https://www.7-zip.org/a/7z2201-linux-x64.tar.xz -O /tmp/7zip.tar.xz")
	sevenZipDir = tempfile.mkdtemp()
	os.chdir(sevenZipDir)
	commands.getoutput("tar xvf /tmp/7zip.tar.xz")



	blender_path = os.path.join('Blender', 'Blender.app', 'Contents', 'MacOS', 'Blender')
	
	working_dir = tempfile.mkdtemp()
	working_dir_app = os.path.join(working_dir, "Blender")
	#os.mkdir(working_dir_app)
	os.chdir(working_dir)
	#print("os.chdir(%s)"%(working_dir))
	#commands.getoutput("unzip %s"%(source))
	#print("%s x %s"%(os.path.join(sevenZipDir, "7zz"), source))
	commands.getoutput("%s x %s"%(os.path.join(sevenZipDir, "7zz"), source))
	commands.getoutput("rm -fr blenderplayer.app")
	commands.getoutput("mkdir %s"%(os.path.join(working_dir, "Blender", "Blender.app", "Contents", "Resources", "portable")))
	#print("working dir for mac ", working_dir)
	os.chdir(working_dir);
	
	if os.path.exists(blender_path) == False:
		print("build mac failed, missing %s"%(blender_path))
		sys.exit(4)
	
	commands.getoutput("zip -r %s ."%(destination))
	os.chdir("/")
	commands.getoutput("rm -rf %s"%(working_dir))

def build_linux(source, arch, destination):
	#print("build_linux", source, destination)
	blender_path = 'rend.exe'
	
	working_dir = tempfile.mkdtemp()
	os.chdir(working_dir)
	commands.getoutput("tar xf %s"%(source))
	cdFirsDir()
	commands.getoutput("mv blender rend.exe")
	commands.getoutput("rm -f blenderplayer")
	commands.getoutput("mkdir portable")

	if os.path.exists(blender_path) == False:
		print("build linux failed, missing %s"%(blender_path))
		sys.exit(4)

	commands.getoutput("zip -r %s ."%(destination))
	os.chdir("/")
	commands.getoutput("rm -rf %s"%(working_dir))

def main(version, linux64, windows64, mac64_intel, mac64_arm_m1):
	local_dir = tempfile.mkdtemp(prefix='blender_'+version+'_sheepit_')
	print("destination %s"%(local_dir))
	
	build_windows(windows64, os.path.join(local_dir, name%(version, 'windows_64bit')))
	build_mac(mac64_intel, os.path.join(local_dir, name%(version, 'mac_64bit')))
	build_mac(mac64_arm_m1, os.path.join(local_dir, name%(version, 'macm1_64bit')))
	build_linux(linux64, 'linux_64bit', os.path.join(local_dir, name%(version, 'linux_64bit')))

if __name__ == '__main__':
	if len(sys.argv) == 2:
		version = sys.argv[1]
		linux64 = '/tmp/blender-%s-linux-x64.tar.xz'%(version)
		windows64 = '/tmp/blender-%s-windows-x64.zip'%(version)
		mac64_intel = '/tmp/blender-%s-macos-x64.dmg'%(version)
		mac64_arm_m1 = '/tmp/blender-%s-macos-arm64.dmg'%(version)
		main(version, linux64, windows64, mac64_intel, mac64_arm_m1)
	elif len(sys.argv) != 6:
		print("Path file must be absolute")
		print("Version is '3.2.2', no blender prefix")
		print("Usage: version linux64.tar.bz2 windows64.zip mac64_intel.zip mac64_arm_m1.zip")
		sys.exit(2)
	else:
		main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])


