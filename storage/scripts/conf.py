import bpy

def mute_all_file_output_nodes():
	# Mute File Output Nodes in scenes
	for scene in bpy.data.scenes:
		mute_nodes(scene.node_tree)

	# Mute File Output Nodes in node groups
	for node_group in bpy.data.node_groups:
		mute_nodes(node_group)

def mute_nodes(node_tree):
	if node_tree and node_tree.type == 'COMPOSITING':
		# Iterate through all compositing nodes in the node tree
		for node in node_tree.nodes:
			if node.type == 'OUTPUT_FILE':
				node.mute = True


# bpy.context.scene.render.threads_mode = 'AUTO'

# if it's a movie clip, switch to png
fileformat = bpy.context.scene.render.image_settings.file_format
if fileformat != 'BMP' and fileformat != 'PNG' and fileformat != 'JPEG' and fileformat != 'TARGA' and fileformat != 'TARGA_RAW':
	bpy.context.scene.render.image_settings.file_format = 'PNG'
	bpy.context.scene.render.filepath = ''

if bpy.context.scene.render.image_settings.file_format == 'PNG':
	bpy.context.scene.render.image_settings.compression = 100

try:
	bpy.context.scene.render.use_stamp_filename = False
	bpy.context.scene.render.use_stamp_date = False
	bpy.context.scene.render.use_stamp_hostname = False
	bpy.context.scene.render.use_stamp_memory = False
	bpy.context.scene.render.use_stamp_render_time = False
except AttributeError:
	pass

try:
	bpy.context.scene.render.threads_mode = 'AUTO'
except AttributeError:
	pass

try:
	bpy.context.scene.render.use_persistent_data = False
except AttributeError:
	pass

try:
	bpy.context.scene.render.use_render_cache = False
except AttributeError:
	pass


mute_all_file_output_nodes()
print("All File Output Nodes muted.")
