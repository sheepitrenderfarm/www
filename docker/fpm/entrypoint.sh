#!/bin/sh
set -e

if [ "$1" = 'php-fpm' ]; then
    service rsyslog start
    service cron start

    su - www-data -s /bin/bash -c "cd /var/www/html/www && APP_ENV=prod bin/console cache:clear"
fi

exec docker-php-entrypoint "$@"
