<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Persistent\Cache;

class CacheTest extends BaseTestCase {

    public function testIsFilled(): void {
        $c = new Cache();

        $this->assertFalse($c->isFilled());
    }

    public function testSetFile(): void {
        $c = new Cache();

        $this->assertFalse($c->isFilled());
        $c->setFill(true);
        $this->assertTrue($c->isFilled());
    }

    public function testAccess(): void {
        $c = new Cache();

        $this->assertFalse($c->exists(42));
        $c->add('test', 42);
        $this->assertTrue($c->exists(42));
        $this->assertEquals('test', $c->get(42));
        $c->remove(42);
        $this->assertFalse($c->exists(42));
    }

    public function testAdd(): void {
        $c = new Cache();

        $this->assertFalse($c->exists(42));
        $c->add('test1', 42);
        $this->assertTrue($c->exists(42));
        $this->assertEquals('test1', $c->get(42));
        $c->add('test2', 42);
        $this->assertTrue($c->exists(42));
        $this->assertEquals('test2', $c->get(42));
    }

    public function testGetAll(): void {
        $c = new Cache();

        $this->assertFalse($c->exists(42));
        $this->assertFalse($c->exists(43));

        $c->add('alpha', 42);
        $c->add('beta', 43);

        $this->assertTrue($c->exists(42));
        $this->assertTrue($c->exists(43));

        $output = $c->getAll();

        $this->assertArrayHasKey(42, $output);
        $this->assertArrayHasKey(43, $output);

        $this->assertEquals('alpha', $output[42]);
        $this->assertEquals('beta', $output[43]);
    }
}
