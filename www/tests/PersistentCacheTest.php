<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Persistent\PersistentCacheFile;

class PersistentCacheTest extends BaseTestCase {

    public function testPersistentCacheFile(): void {
        $root = tempnam('/tmp/', 'cache').'st/';
        mkdir($root);
        $c = new PersistentCacheFile($root);

        $this->assertCount(0, $c->getAllKeys());

        $key = uniqid('', true);

        $this->assertTrue($c->get($key) == false); // false => no value/failed

        $value = array('some_id' => 'some_value');

        $this->assertTrue(is_array($c->set($key, $value)));

        $this->assertCount(1, $c->getAllKeys());

        $stored_value = $c->get($key);

        $this->assertTrue(is_array($stored_value));

        /** @phpstan-ignore-next-line */
        $this->assertArrayHasKey('some_id', $stored_value);
        $this->assertTrue($stored_value['some_id'] == 'some_value');

        $c->remove($key);
        $this->assertEquals(false, $c->get($key)); // false => no value/failed
    }

    public function testAllKeys(): void {
        $root = tempnam('/tmp/', 'cache').'st/';
        mkdir($root);
        mkdir($root.'/'.'dir');
        $cache = new PersistentCacheFile($root);
        $keys = $cache->getAllKeys();
        $this->assertCount(0, $keys); // directory should not be a key
    }
}
