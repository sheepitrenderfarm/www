<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\FrameChessboard;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Task;
use App\Entity\TileReal;
use App\Repository\SessionRepository;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class StatsTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testRenderingFrame(): void {
        $user = $this->unittestCreateUser();

        // add project
        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // check stats
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($project_db));

        $stats0 = $project_db->getCachedStatistics();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project_db->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $stats1 = $project_db->getCachedStatistics();
        $this->assertEquals(1, $stats1[Constant::PROCESSING]);
        $this->assertEquals(9, $stats1[Constant::WAITING]);

        $a_frame->reset();

        $stats2 = $project_db->getCachedStatistics();
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats2));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats2));
        $this->assertEquals(0, $stats2[Constant::PROCESSING]);
        $this->assertEquals(10, $stats2[Constant::WAITING]);

        $a_frame->reset(); // should do nothing

        $stats3 = $project_db->getCachedStatistics();
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats3));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats3));
        $this->assertEquals(0, $stats3[Constant::PROCESSING]);
        $this->assertEquals(10, $stats3[Constant::WAITING]);
    }

    public function testStatsJobSplitAnimationOnRender(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $nb_tile = $nb_frame * $nb_tile_per_frame;
//        $class_name = '\App\Entity\Job'.Job::$type_animation_tile;
//        $job = new $class_name();
//        $job->setPath('path_fake');
//        $job->setPictureExtension('jpg');
//        $this->entityManager->persist($job);
//        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
//        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);
//        $this->assertEquals($nb_tile, $project->addproject($job, 1, $nb_frame, 1, 1920, 1080, 0, $filename, true));

        $project = $this->unittestCreateOrphanProject($user);

        // revert file from unittest_create_orphan_project to have a new file on addFrames(...) (to add a daemon task)
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.zip';
        copy($config['storage']['path'].'projects'.'/'.$this->getChunkProject1(), $filename);

//        $nb_frame = 10;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, $filename));

//        $project = $this->unittest_create_project($user);
//        $this->assertTrue(is_object($project));
//
//        $nb_frame = 10;
//        $nb_tile = $nb_frame * $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
//        $class_name = '\App\Entity\Job'.Job::$type_animation_tile;
//        $job = new $class_name();
//        $job->setPath('path_fake');
//        $job->setPictureExtension('jpg');
//        $this->entityManager->persist($job);
//        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
//        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);
//        $this->assertEquals($nb_tile, $project->addproject($job, 1, $nb_frame, 1, 1920, 1080, 0, $filename, true));

        // render a frame
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $stats = $project->getCachedStatistics();
        $this->assertEquals($nb_tile, $stats[Constant::STATS_TOTAL]);
        $this->assertEquals(0, $stats[Constant::PROCESSING]);
        $this->assertEquals(1, $stats[Constant::FINISHED]);
        $this->assertEquals($nb_tile - 1, $stats[Constant::WAITING]);
    }

    public function testreGenerateStats(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

//        $project = $this->unittest_create_project($user);
//        $this->assertTrue(is_object($project));
//
        $nb_frame = 10;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $nb_tile = $nb_frame * $nb_tile_per_frame;
//        $class_name = '\App\Entity\Job'.Job::$type_animation_tile;
//        $job = new $class_name();
//        $job->setPath('path_fake');
//        $job->setPictureExtension('jpg');
//        $this->entityManager->persist($job);
//        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
//        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);
//        $this->assertEquals($nb_tile, $project->addproject($job, 1, $nb_frame, 1, 1920, 1080, 0, $filename, true));

        $project = $this->unittestCreateOrphanProject($user);

        // revert file from unittest_create_orphan_project to have a new file on addFrames(...) (to add a daemon task)
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.zip';
        copy($config['storage']['path'].'projects'.'/'.$this->getChunkProject1(), $filename);

//        $nb_frame = 10;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, $filename));


        // render a frame
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $stats = $project->getCachedStatistics();
        $this->assertEquals($nb_tile, $stats[Constant::STATS_TOTAL]);
        $this->assertEquals(0, $stats[Constant::PROCESSING]);
        $this->assertEquals(1, $stats[Constant::FINISHED]);
        $this->assertEquals($nb_tile - 1, $stats[Constant::WAITING]);

        // everything is ok, now do the actual test

        $stats1 = $project->generateStatsCache();
        $this->assertTrue(array_key_exists(Constant::STATS_LAST_VALIDATION, $stats1));
        $this->assertTrue($stats1[Constant::STATS_LAST_VALIDATION] != 0);
    }

    public function testreGenerateStatsOnNegativeValue(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        // check stats
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertIsObject($project_db);

        $stats = $project_db->getCachedStatistics();
        $this->assertEquals(0, $stats[Constant::PROCESSING]);
        $this->assertEquals(10, $stats[Constant::WAITING]);

        // desync
        $project->updateStatsCache([Constant::WAITING => -20]);

        // check
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // TaskProjectGenerateStats

        // validate another frame -> do not add another ProjectGenerateStats task

        $project->updateStatsCache([Constant::WAITING => -20]);

        // check
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // TaskProjectGenerateStats
    }

    public function testGenerateStatsNegativeRenderTime(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // generate bad data
        // reset() will generate -1 on
        //        request_time
        //        validation_time
        //        render_time
        //        remaining_render_time
        foreach ($project->tiles() as $tile) {
            $tile->reset();
        }

        $stats = $project->generateStatsCache();
        $this->assertEquals(0, $stats[Constant::STATS_RENDERTIME_ACTUAL]);
        $this->assertEquals(0, $stats[Constant::STATS_RENDERTIME_REF]);
        $this->assertEquals(0, $stats[Constant::STATS_LAST_VALIDATION]);
    }

    public function testGenerateStatsOnSessionRemove(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // start a session
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // generate a processing frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $stats = $project->getCachedStatistics();

        $this->assertArrayHasKey(Constant::STATS_RENDERTIME_ACTUAL, $stats);
        $this->assertArrayHasKey(Constant::STATS_RENDERTIME_REF, $stats);
        $this->assertArrayHasKey(Constant::FINISHED, $stats);
        $this->assertArrayHasKey(Constant::PROCESSING, $stats);

        $this->assertEquals(0, $stats[Constant::STATS_RENDERTIME_ACTUAL]);
        $this->assertEquals(1, $stats[Constant::PROCESSING]);
        $this->assertEquals(0, $stats[Constant::FINISHED]);

        /** @var SessionRepository $sessionRepository */
        $sessionRepository = $this->entityManager->getRepository(Session::class);


        $sessionRepository->remove($handler->getSession());

        $statsAfter = $project->getCachedStatistics();

        $this->assertEquals(0, $statsAfter[Constant::STATS_RENDERTIME_ACTUAL]);
        $this->assertEquals(0, $statsAfter[Constant::PROCESSING]);
        $this->assertEquals(0, $statsAfter[Constant::FINISHED]);
    }
}
