<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Utils\Mail;

class MailTest extends BaseTestCase {

    public function testIsSpam(): void {
        $this->assertFalse(Mail::isSpamDomain('toto@gmail.com'));
        $this->assertTrue(Mail::isSpamDomain('toto@yopmail.com'));
    }

    public function testIsSpamBadInput(): void {
        $this->assertFalse(Mail::isSpamDomain('toto'));
    }
}
