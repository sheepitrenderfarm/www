<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\User;

class AffiliateTest extends BaseTestCase {

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAffiliateIsNotAUser(): void {
        $config = $this->configService->getData();
        $user_referred = $this->unittestCreateUser();

        $user_referred->setAffiliate('http://www.blender.org');
        $user_referred->setRenderedFramesPublic($config['affiliate_minimum_frame']); // fake some rendering
        $this->entityManager->flush();

        $this->assertEquals(0, $user_referred->getPoints());

        $user_referred->affiliate();

        $this->assertEquals(0, $user_referred->getPoints());
    }

    public function testAffiliateIsAUser(): void {
        $config = $this->configService->getData();

        $user_referer = $this->unittestCreateUser();
        $user_referred = $this->unittestCreateUser();

        $user_referred->setAffiliate($user_referer->getId());
        $user_referred->setRenderedFramesPublic($config['affiliate_minimum_frame']); // fake some rendering
        $this->entityManager->flush();


        $this->assertEquals(0, $user_referer->getPoints());
        $this->assertEquals(0, $user_referred->getPoints());

        $user_referred->affiliate();

        // reload users
        $user_referer = $this->entityManager->getRepository(User::class)->find($user_referer->getId());
        $this->assertNotNull($user_referer);
        $user_referred = $this->entityManager->getRepository(User::class)->find($user_referred->getId());
        $this->assertNotNull($user_referred);

        $this->assertEquals($config['affiliate_reward'], $user_referer->getPoints());
        $this->assertEquals($config['affiliate_reward'], $user_referred->getPoints());
    }
}
