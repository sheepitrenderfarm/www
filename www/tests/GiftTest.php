<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\Gift;
use App\Entity\User;
use App\Repository\GiftRepository;

class GiftTest extends BaseTestCase {
    private GiftRepository $giftRepository;

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    protected function setUp(): void {
        parent::setUp();
        $this->giftRepository = $this->entityManager->getRepository(Gift::class);
    }

    public function testAdd(): void {
        $user = $this->unittestCreateUser();

        $this->assertEquals(0, (int)($user->getPoints()));
        $this->assertEquals(0, $user->gifts()->count());

        $value = 1000;
        $coupon = new Gift();
        $coupon->setValue($value);
        $coupon->setOwner($user);
        $coupon = $this->giftRepository->generateAccess($coupon);
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();
        $this->entityManager->refresh($user);
        $this->assertEquals(1, $user->gifts()->count());
    }

    public function testTakeExistingUser(): void {
        $user = $this->unittestCreateUser();

        $this->assertTrue((int)($user->getPoints()) == 0);
        $this->assertEquals(0, count($user->gifts()));

        $value = 1000;
        $coupon = new Gift();
        $coupon->setValue($value);
        $coupon->setOwner($user);
        $coupon = $this->giftRepository->generateAccess($coupon);
        $this->entityManager->persist($coupon);
        $this->entityManager->flush();
        $this->entityManager->refresh($user);
        $this->assertEquals(1, $user->gifts()->count());

        $coupon = $user->gifts()->first();
        $this->assertTrue(is_object($coupon));
        $this->assertTrue($this->giftRepository->take($coupon, $user));

        $user_db = $this->entityManager->getRepository(User::class)->find($user->getId()); // to reload the user
        $this->assertTrue(is_object($user_db));

        $this->assertEquals($value, (int)($user_db->getPoints()));
    }
}
