<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Scheduler\ClientError;

class ClientErrorTest extends BaseTestCase {

    /**
     * @return array<int, array{0: string, 1: int}>
     */
    public function providerErrorType(): array {
        return array(
            array(__DIR__.'/data/error_1658052947_14.log', ClientError::TYPE_BAD_UPLOAD_RESPONSE),
            array(__DIR__.'/data/error_1654829436_209910.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656128382_228062.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656115962_228421.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656116995_228062.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656136826_226161.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656117663_228552.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656119168_228421.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656144146_227918.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1656126853_227255.log', ClientError::TYPE_SERVER_DOWN),
            array(__DIR__.'/data/error_1665344084_366498.log', ClientError::TYPE_NETWORK_ISSUE),
            array(__DIR__.'/data/error_1655936417_225314.log', ClientError::TYPE_RENDERER_OUT_OF_MEMORY),
        );
    }

    /**
     * @return array<int, array{0: string, 1: int}>
     */
    public function providerErrorBlock(): array {
        return array(
            array(__DIR__.'/data/error_1696919293_793074.log', 15),
            array(__DIR__.'/data/error_1655936417_225314.log', -1),
        );
    }

    /**
     * @dataProvider providerErrorType
     */
    public function testGetErrorTypeFromFile(string $path, int $expected_type): void {
        $error_str = ClientError::getErrorTypeFromFile($path);

        $error_type = ClientError::fromClientLog($error_str);
        $this->assertEquals($expected_type, $error_type);
    }

    /**
     * @dataProvider providerErrorBlock
     */
    public function testShouldBlockFromError(string $path, int $expected_block): void {
        $block = ClientError::shouldBlockProject(file_get_contents($path));

        $this->assertEquals($expected_block, $block);
    }

}
