<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Gift;
use App\Entity\Patreon;
use App\Entity\RenderDay;
use App\Entity\Session;
use App\Entity\StatsUser;
use App\Entity\Tile;
use App\Entity\User;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpFoundation\FileBag;

class UserTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAdd(): void {
        $user_db = $this->unittestCreateUser();
        $this->assertTrue($user_db->canDoRendering());
    }

    public function testRemove(): void {
        $user = $this->unittestCreateUser();

        // fake some rendering
        $user->setRenderTime(1000);
        $user->setRenderedFrames(1000);
        $user->setOrderedFrames(1000);
        $user->setOrderedFramesTime(1000);
        $user->setPoints(1000);
        $user->setPointsEarn(1000);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        // add blocked user
        $otherUser = $this->unittestCreateUser();
        $user->addBlockedRenderer($otherUser->getId());
        $blocks1 = $user->getBlockedRenderers();
        $this->assertCount(1, $blocks1);

        // generate some stats
        $application = new Application(self::$kernel);

        $command5Minutes = $application->find('sheepit:cron:5minutes');
        $commandTester = new CommandTester($command5Minutes);
        $commandTester->execute([]);
        $commandHourly = $application->find('sheepit:cron:hourly');
        $commandTester = new CommandTester($commandHourly);
        $commandTester->execute([]);
        $commandDaily = $application->find('sheepit:cron:daily:a');
        $commandTester = new CommandTester($commandDaily);
        $commandTester->execute([]);
        $commandDaily = $application->find('sheepit:cron:daily:b');
        $commandTester = new CommandTester($commandDaily);
        $commandTester->execute([]);
        $commandDaily = $application->find('sheepit:cron:daily:c');
        $commandTester = new CommandTester($commandDaily);
        $commandTester->execute([]);

        $this->entityManager->getRepository(User::class)->remove($user);


        $this->assertNull($this->entityManager->getRepository(User::class)->find($user->getId()));

        $fake_user = new User();
        $fake_user->setId($user->getId());
        $this->assertCount(0, $fake_user->getBlockedRenderers());
    }

    public function testRemovePriorityList(): void {
        // remove me from user who prioritized me

        $user = $this->unittestCreateUser();
        $otherUser = $this->unittestCreateUser();

        $user->addHighPriorityUser($otherUser->getId());

        $this->assertCount(1, $user->getHighPriorityUsers());

        $this->entityManager->getRepository(User::class)->remove($otherUser);

        // "new user" so all the cache are clean
        $this->entityManager->refresh($user);

        $this->assertCount(0, $user->getHighPriorityUsers());
    }

    public function testRemoveHasClearBestRenderer(): void {
        $config = $this->configService->getData();

        $user = null;

        // create some other user to have a filled bestRenderers
        for ($i = 0; $i < 10; $i++) {
            $otherUser = $this->unittestCreateUser();

            // fake some rendering
            //$user->setTime(time() - rand(0, 100000));
            $otherUser->setRenderTime(1000 + rand(0, 10000));
            $otherUser->setRenderedFrames(1000 + rand(0, 10000));
            $otherUser->setOrderedFrames(1000 + rand(0, 10000));
            $otherUser->setOrderedFramesTime(1000 + rand(0, 10000));
            $otherUser->setPoints(1000 + rand(0, 10000));
            $otherUser->setPointsEarn(1000 + rand(0, 10000));
            $this->entityManager->flush();

            // generate some stats
            $su = new StatsUser();
            $su->setDate(new \DateTime());
            $su->setUser($otherUser);
            $su->setRenderedFrames($otherUser->getRenderedFrames() - rand(0, 100));
            $su->setPoints($otherUser->getPoints() - rand(0, 100));
            $su->setPointsEarn($otherUser->getPointsEarn() - rand(0, 100));
            $su->setRenderTime($otherUser->getRenderTime() - rand(0, 100));
            $this->entityManager->persist($su);
            $this->entityManager->flush($su);

            $user = $otherUser;
        }

        $this->main->getPersistentCache()->set('best_renderer', serialize($this->main->getRenderers($config['rank']['duration'])));

        $best1 = $this->main->getBestRenderersFromCache();
        $search = false;
        foreach ($best1 as $dic) {
            if ($dic->getLogin() == $user->getId()) {
                $search = true;
                break;
            }
        }
        $this->assertTrue($search);

        $this->entityManager->getRepository(User::class)->remove($user);


        $this->assertNull($this->entityManager->getRepository(User::class)->find($user->getId()));

        $best2 = $this->main->getBestRenderersFromCache();

        $search = false;
        foreach ($best2 as $dic) {
            if ($dic->getLogin() == $user->getId()) {
                $search = true;
                break;
            }
        }
        $this->assertFalse($search);
    }

    public function testPointsEarnWithGift(): void {
        $user = $this->unittestCreateUser();

        $this->assertTrue((int)($user->getPoints()) == 0);
        $this->assertTrue((int)($user->getPointsEarn()) == 0);
        $this->assertEquals(0, count($user->gifts()));

        $value = 1000;
        $gift = new Gift();
        $gift->setValue($value);
        $gift->setOwner($user);
        $gift = $this->entityManager->getRepository(Gift::class)->generateAccess($gift);
        $this->entityManager->persist($gift);
        $this->entityManager->flush();
        $this->entityManager->refresh($user);
        $this->assertEquals(1, $user->gifts()->count());

        $gift = $user->gifts()->first();
        $this->assertTrue(is_object($gift));
        $this->assertTrue($this->entityManager->getRepository(Gift::class)->take($gift, $user));

        $this->entityManager->refresh($user); // to reload the user

        $this->assertEquals($value, (int)($user->getPoints()));
        $this->assertEquals(0, (int)($user->getPointsEarn()));
    }

    public function testPointEarnOnRender(): void {
        $user = $this->unittestCreateUser();
        $user->setRenderTime(1000);
        $user->setRenderedFrames(10); // to avoid power detection frame
        $this->entityManager->flush();

        $this->assertEquals(0, (int)($user->getPoints()));
        $this->assertEquals(0, (int)($user->getPointsEarn()));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $nb_frames = 10;
        $project = $this->unittestCreateProject($user, $nb_frames);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $user_db = $this->entityManager->getRepository(User::class)->find($user->getId()); // to reload the user

        $this->assertTrue((int)($user_db->getPoints()) > 0);
        $this->assertTrue((int)($user_db->getPointsEarn()) > 0);
    }

    public function testPointEarnOnFailure(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();
        $user->setRenderTime(1000);
        $user->setRenderedFrames(10); // to avoid power detection frame
        $this->entityManager->flush();

        $this->assertEquals(0, (int)($user->getPoints()));
        $this->assertEquals(0, (int)($user->getPointsEarn()));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();
        $cpu->setRenderedFrames(10); // to avoid power detection frame
        $this->entityManager->flush();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($user, $nb_frames);

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        for ($i = 0; $i < $nb_frames; $i++) {
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $this->assertEquals(ClientProtocol::OK, $status);
            $this->assertTrue(is_object($frame));
            $this->assertFalse($frame->isComputeMethod());

            $path = $config['tmp_dir'].'/'.'error_'.$this->unittestRand();
            list($status, $job_id, $frame_number, $render_time, $type) = $handler->errorInput(array('frame' => $frame->getNumber(), 'job' => $frame->getId()), new FileBag(), $path);

            // same code as server/error.php
            $frame = $this->entityManager->getRepository(Tile::class)->find($job_id);
            $this->assertEquals(Constant::PROCESSING, $frame->getStatus());
            $this->assertEquals($handler->getSession()->getUser()->getId(), $frame->getUser()->getId());
            $frame->setRenderTime(10);
            $frame->reset(true);
        }

        $user = $this->entityManager->getRepository(User::class)->find($user->getId()); // reload the object
        $session = $this->entityManager->getRepository(Session::class)->find($handler->getSession()->getId());  // reload the object

        $this->assertTrue($user->getPoints() > 0);
//         $this->assertTrue($user->getPointsEarn() > 0); // ??
        $this->assertTrue($session->getPoints() > 0);
    }

    public function testLastRenderedFrameAttribute(): void {
        $user = $this->unittestCreateUser();

        // init
        $this->assertNull($user->getLastRenderedFrame());

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $nb_frames = 10;
        $this->unittestCreateProject($user, $nb_frames);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        // compute method and power detection should not count as rendered frames
        $this->entityManager->refresh($user);
        $this->assertNull($user->getLastRenderedFrame());

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->entityManager->refresh($user);
        $this->assertIsObject($user->getLastRenderedFrame());
    }

    public function testPublicRenderedFramesCountFromPublicProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 20;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $user = $this->entityManager->getRepository(User::class)->find($user->getId()); // reload user
        $this->assertEquals(1, $user->getRenderedFramesPublic());
        $this->assertEquals(1, $user->getOrderedFramesPublic());
    }

    public function testPublicRenderedFramesCountFromPrivateProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 20;
        $project = $this->unittestCreateProject($user, $nb_frame, false);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $user = $this->entityManager->getRepository(User::class)->find($user->getId()); // reload user
        $this->assertEquals(0, $user->getRenderedFramesPublic());
        $this->assertEquals(0, $user->getOrderedFramesPublic());
    }

    public function testACLManagerSomeoneElseProject(): void {
        $user = $this->unittestCreateUser();

        $owner = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $this->assertFalse($user->canSeeAllProjects());
        $this->assertFalse($user->canModifyAllProjects());
        $this->assertFalse($user->isAdmin());
        $this->assertTrue($user->canDoRendering());
        $this->assertTrue($user->canRenderProject($project));
        $this->assertFalse($project->canSeeProject($user));
        $this->assertFalse($project->canManageProject($user));

        $this->assertFalse($owner->isAdmin());
        $this->assertTrue($project->canSeeProject($owner));
    }

    public function testWorkerStatusDown(): void {
        $user = $this->unittestCreateUser();

        $this->assertEquals(Constant::DOWN, $user->getWorkerStatus());
    }

    public function testWorkerStatusWaiting(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        $session = $handler->getSession();

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $this->assertEquals(Constant::WAITING, $user->getWorkerStatus());  // because the compute_method frame doesn't count
    }

    public function testWorkerStatusProcessing(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertEquals(Constant::WAITING, $user->getWorkerStatus());

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $this->assertEquals(Constant::PROCESSING, $user->getWorkerStatus());
    }

    public function testWorkerStatusMultipleSessionA(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler1 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $handler2 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler1, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler2, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertEquals(Constant::WAITING, $user->getWorkerStatus());

        list($status, $frame, $remaining_frames) = $handler1->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $this->assertEquals(Constant::PROCESSING, $user->getWorkerStatus());
    }

    public function testWorkerStatusMultipleSessionB(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler1 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $handler2 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler1, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler2, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertEquals(Constant::WAITING, $user->getWorkerStatus());

        list($status, $frame, $remaining_frames) = $handler2->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $this->assertEquals(Constant::PROCESSING, $user->getWorkerStatus());
    }

    public function testAddBlockedRenderer(): void {
        $owner = $this->unittestCreateUser();

        $rendererO = $this->unittestCreateUser();

        $renderer2 = $this->unittestCreateUser();

        $this->assertCount(0, $owner->getBlockedRenderers());

        $owner->addBlockedRenderer($rendererO->getId());

        $blocks1 = $owner->getBlockedRenderers();
        $this->assertCount(1, $blocks1);

        $blocks2 = $rendererO->getBlockedRenderers();
        $this->assertCount(0, $blocks2);

        $blocks3 = $rendererO->getUsersWhoBlockedMe();
        $this->assertCount(1, $blocks3);

        $owner->addBlockedRenderer($renderer2->getId());
        $blocks4 = $owner->getBlockedRenderers();
        $this->assertCount(2, $blocks4);
    }

    public function testAddBlockedOwner(): void {
        $user = $this->unittestCreateUser();

        $owner1 = $this->unittestCreateUser();

        $owner2 = $this->unittestCreateUser();

        $this->assertCount(0, $user->getBlockedOwners());

        $user->addBlockedOwner($owner1->getId());
        $this->assertCount(1, $user->getBlockedOwners());

        $user->addBlockedOwner($owner2->getId());
        $this->assertCount(2, $user->getBlockedOwners());
    }

    public function testUserWhoBlockedMe(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        // Block the renderer
        $this->assertCount(0, $owner->getBlockedRenderers());
        $owner->addBlockedRenderer($renderer->getId());

        $renderer->init(); // reset cache
        $this->assertCount(1, $owner->getBlockedRenderers());
        $this->assertCount(1, $renderer->getUsersWhoBlockedMe());
        $this->assertCount(0, $owner->getUsersWhoBlockedMe());
    }

    public function testCountAvailableFramesRenderFrame1(): void {
        $user = $this->unittestCreateUser();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($user, $nb_frames);

        $this->assertEquals($nb_frames, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user));

        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $this->assertEquals($nb_frames - 1, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user));
    }

    public function testCountAvailableFramesRenderFrame2(): void {
        $user = $this->unittestCreateUser();

        $owner = $this->unittestCreateUser();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($owner, $nb_frames);
        $this->entityManager->flush();

        $this->assertEquals($nb_frames, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($owner));
        $this->assertEquals($nb_frames, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user));

        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $this->assertEquals($nb_frames - 1, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user));
        $this->assertEquals($nb_frames - 1, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($owner));
    }

    public function testCountAvailableFramesFromPrivateProject1(): void {
        $owner = $this->unittestCreateUser();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($owner, $nb_frames, false);

        $this->assertEquals($nb_frames, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($owner));
    }

    public function testCountAvailableFramesFromPrivateProject2(): void {
        $user1 = $this->unittestCreateUser();

        $user2 = $this->unittestCreateUser();

        $owner = $this->unittestCreateUser();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($owner, $nb_frames, false);

        $this->assertEquals($nb_frames, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($owner));
        $this->assertEquals(0, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user1));
        $this->assertEquals(0, $this->entityManager->getRepository(Tile::class)->countAvailableFramesForUser($user2));
    }

    public function testAddRenderDay(): void {
        $user = $this->unittestCreateUser();

        $repo = $this->entityManager->getRepository(RenderDay::class);

        $this->assertCount(0, $repo->getConsecutiveRenderDaysFor($user));
        $this->entityManager->getRepository(RenderDay::class)->addFor($user);
        $this->assertCount(1, $repo->getConsecutiveRenderDaysFor($user));

        // give a render day for the day, it should not generate duplicate
        $this->entityManager->getRepository(RenderDay::class)->addFor($user);
        $this->assertCount(1, $repo->getConsecutiveRenderDaysFor($user));
    }

    /**
     * If a user is a patron AND a paypal donor, he should not appear twice on the list
     */
    public function testUniqueDonors(): void {
        $user = $this->unittestCreateUser();
        $repo = $this->entityManager->getRepository(User::class);

        // paypal
        $user->setPayPalDonation(new DateTime());
        $this->entityManager->flush();
        $this->assertTrue($user->isPayPalDonator());

        // patron
        $patreon = new Patreon();
        $patreon->setUser($user);
        $patreon->setId('something'.$this->unittestRand().'@localhost');
        $patreon->setLastCharge(date_format(new DateTime(), 'Y-m'));
        $this->entityManager->persist($patreon);
        $this->entityManager->flush();
        $this->assertTrue($user->isPatreon());

        $this->assertCount(1, $repo->getAllDonors());
    }
    /*

    public function testCountAvailableFramesFromPrivateSceneWithTeam() {
        $config = $this->configService->getData();

        $user1 = $this->unittest_create_user();
        $this->assertTrue(is_object($user1));

        $owner = $this->unittest_create_user();
        $this->assertTrue(is_object($owner));

        $project = $this->unittest_create_scene($owner, 0);
        $this->assertTrue(is_object($project));
        $this->assertTrue($project->update());

        $nb_frames = 10;
        $job = $this->unittest_create_job($project, $nb_frames);
        $this->assertTrue(is_object($job));

        $this->assertEquals($owner->getCountAvailableFrames(), $nb_frames);
        $this->assertEquals($user1->getCountAvailableFrames(), 0);

        // add user1 to team
        $team = new Team();
        $team->setName('name'.'_'.$this->unittest_rand());
        $team->setPoints(0);
        $team->setOwner($owner->getId());
        $team->setAttribute('description', 'something...');
        $team->setAttribute('creation_time', time());
        $this->assertTrue($team->add());

        $this->assertTrue($team->addMember($user1) == Team::OK);

        $this->assertEquals($nb_frames, $owner->getCountAvailableFrames());
        $this->assertEquals($nb_frames, $user1->getCountAvailableFrames());
    }
    */
}
