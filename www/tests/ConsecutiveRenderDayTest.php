<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\AwardConsecutiveDaysSession;
use App\Entity\CPU;
use App\Entity\RenderDay;
use App\Entity\Session;
use App\Entity\User;
use App\Scheduler\ClientProtocol60;
use DateTime;

class ConsecutiveRenderDayTest extends BaseTestCase {
    private User $user;

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();

        $this->user = $this->unittestCreateUser();
        $this->assertNotNull($this->user);
    }

    protected function tearDown(): void {
        $this->entityManager->getRepository(User::class)->remove($this->user);
        $this->unittestClear();
        parent::tearDown();
    }

    private function createSession(int $start): Session {
        $new_session = new Session();
        $new_session->setUser($this->user);
        $new_session->setCpu($this->entityManager->getRepository(CPU::class)->findOneBy([]));
        $new_session->setTimestamp($start);
        $new_session->setOS('linux');
        $new_session->setVersion(ClientProtocol60::MINIMUM_VERSION);
        $new_session->setCreationtime($start);
        $new_session->setComputeMethod(-1);
        $new_session->setGpu(null);
        $new_session->setMemory(-1);
        $new_session->setBlocked(0);
        $new_session->setRunning(true);

        $this->entityManager->persist($new_session);
        $this->entityManager->flush($new_session);

        return $new_session;
    }

    private function reloadUser(): void {
        $this->entityManager->refresh($this->user);
    }

    public function testSessionUnderRenderTimePerDayLimit(): void {
        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());

        $session = $this->createSession(time() - 3600 * AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY / 2);
        $session->updateOwnerConsecutiveRenderDays();
        $this->reloadUser();

        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());
    }

    public function testSessionOverRenderTimePerDayLimit(): void {
        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());

        $session = $this->createSession(time() - 3600 * AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY - 60);
        $session->updateOwnerConsecutiveRenderDays();
        $this->reloadUser();

        // if it's call between midnight and 08:00 (when MINIMUM_RENDERTIME_PER_DAY is 8), yesterday will be a renderday so result is 1
        // if it's between 8:01 and 23:59, the session will have been started on the same day, so result is 0
        if ((new DateTime())->format('H') >= 8) {
            $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());
        }
        else {
            $this->assertEquals(1, $this->user->getConsecutiveRenderDaysCurrent());
        }
    }

    public function testSessionBlocked(): void {
        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());

        $session = $this->createSession(time() - 86400 * 10); // long session
        $session->setBlocked(2); // block the session
        $session->updateOwnerConsecutiveRenderDays();
        $this->reloadUser();

        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent()); // blocked session -> no renderday
    }

    public function testConsecutiveRenderDays(): void {
        for ($i = -10; $i <= 0; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() + $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $this->assertEquals(10, $this->user->getConsecutiveRenderDaysCurrent());
    }

    public function testRenderDaysInThePast(): void {
        for ($i = -10; $i <= -4; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() + $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());
    }

    public function testUserWithNonConsecutiveDays(): void {
        for ($i = -10; $i <= -4; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() + $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $session = $this->createSession(time() - 3600 * AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY - 60);
        $session->updateOwnerConsecutiveRenderDays();

        // if it's call between midnight and 08:00 (when MINIMUM_RENDERTIME_PER_DAY is 8), yesterday will be a renderday so result is 1
        // if it's between 8:01 and 23:59, the session will have been started on the same day, so result is 0
        if ((new DateTime())->format('H') >= 8) {
            $this->assertEquals(0, $this->user->getConsecutiveRenderDaysCurrent());
        }
        else {
            $this->assertEquals(1, $this->user->getConsecutiveRenderDaysCurrent());
        }
    }

    public function testUserWithConsecutiveDaysWhereSessionStartIsAfterStartDay(): void {
        $nb_day_past = 10;
        for ($i = 0; $i <= 10; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() - $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $this->assertEquals($nb_day_past, $this->user->getConsecutiveRenderDaysCurrent());

        $session = $this->createSession(time() - 86400 * ($nb_day_past - 2));
        $session->updateOwnerConsecutiveRenderDays();
        $this->reloadUser();

        $this->assertEquals($nb_day_past, $this->user->getConsecutiveRenderDaysCurrent()); // no update on the consecutive render days
    }

    public function testUserWithConsecutiveDaysWhereSessionStartIsBeforeStartDay(): void {
        $nb_day_past = 10;
        for ($i = 0; $i <= 10; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() - $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $session = $this->createSession(time() - 86400 * ($nb_day_past + 2));
        $session->updateOwnerConsecutiveRenderDays();
        $this->reloadUser();

        $this->assertEquals($nb_day_past + 2, $this->user->getConsecutiveRenderDaysCurrent());
    }

    public function testLongSession(): void {
        for ($i = -10; $i <= -3; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($this->user, time() + $i * 86400);
        }
        $this->entityManager->flush();
        $this->reloadUser();

        $session = $this->createSession(time() - 86400 * 5);
        $session->updateOwnerConsecutiveRenderDays();

        $this->assertEquals(10, $this->user->getConsecutiveRenderDaysCurrent());
    }
}
