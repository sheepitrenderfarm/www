<?php

namespace App\Test;

use App\CDN\CDNInternal;

class CDN extends BaseTestCase {

    public function testInternalCheckPath(): void {
        $internal = new CDNInternal();
        $this->assertTrue($internal->fileExists("blender3.5.1_linux_64bit.zip")); // touch on .gitlab-ci.yml

        $this->assertFalse($internal->fileExists("blender2.0.0_linux_64bit.zip")); // should not exist
    }
}