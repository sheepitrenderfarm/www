<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use DateTime;

class UserPaypalDonationTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testIsPaypalDonatorNoData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPayPalDonator());
    }

    public function testIsPaypalDonatorWithData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPayPalDonator());

        $user->setPayPalDonation(new DateTime());
        $this->entityManager->flush();

        $this->assertTrue($user->isPayPalDonator());
    }

    public function testIsPaypalDonatorWithExpireData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPayPalDonator());


        $user->setPayPalDonation((new DateTime())->sub(\DateInterval::createFromDateString('65 days')));
        $this->entityManager->flush();

        $this->assertFalse($user->isPayPalDonator());
    }
}