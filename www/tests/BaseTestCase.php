<?php

namespace App\Test;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseTestCase extends KernelTestCase {
    use TestTrait;

    protected function setUp(): void {
        parent::setUp();
        $this->init();
    }
}