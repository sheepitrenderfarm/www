<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Tile;
use App\Entity\Project;
use App\Utils\Misc;

class FrameTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testTake(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // check stats
        $stats0 = $project->getCachedStatistics();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));
        $this->assertFalse($a_frame->take($session) === 0); // second time, it should fail

        $stats1 = $project->getCachedStatistics();
        $this->assertEquals(1, $stats1[Constant::PROCESSING]);
        $this->assertEquals(9, $stats1[Constant::WAITING]);

        $a_frame->reset();

        $stats2 = $project->getCachedStatistics();
        $this->assertEquals(0, $stats2[Constant::PROCESSING]);
        $this->assertEquals(10, $stats2[Constant::WAITING]);

        $a_frame->reset(); // should do nothing

        $stats3 = $project->getCachedStatistics();
        $this->assertEquals(0, $stats3[Constant::PROCESSING]);
        $this->assertEquals(10, $stats3[Constant::WAITING]);
    }

    public function testTakeOnGpuSessionButCpuProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $this->entityManager->flush();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $gpu = $this->unittestCreateGpu();
        $session = $this->unittestCreateSession($user, $machine, $gpu);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $a_frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId()); // reload the object
        $this->assertNull($a_frame->getGPU()); // don't use the gpu for this frame
    }

    public function testTakeOnGpuSessionOnGpuCpuProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);
        // cpu and gpu
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1) + Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $gpu = $this->unittestCreateGpu();
        $session = $this->unittestCreateSession($user, $machine, $gpu);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $a_frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId()); // reload the object
        $this->assertEquals($a_frame->getGPU(), $gpu); // use the gpu for this frame
    }

    public function testTakeOnGpuSessionWithGpuOnlyProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);
        // gpu only
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $gpu = $this->unittestCreateGpu();
        $session = $this->unittestCreateSession($user, $machine, $gpu);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $a_frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId()); // reload the object
        $this->assertEquals($a_frame->getGPU(), $gpu); // use the gpu for this frame
    }

    public function testValidate(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // check stats
        $stats0 = $project->getCachedStatistics();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];
        $this->assertEquals(0, $a_frame->take($session));

        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        $stats1 = $project->getCachedStatistics();
        $this->assertEquals($nb_frame - 1, $stats1[Constant::WAITING]);
        $this->assertEquals(0, $stats1[Constant::PROCESSING]);
        $this->assertEquals(1, $stats1[Constant::FINISHED]);

        $this->assertTrue($session->validateFrame($a_frame, $this->unittestGenerateImageFile(), $this->phpSession) !== 0);

        $stats2 = $project->getCachedStatistics();
        $this->assertEquals($nb_frame - 1, $stats2[Constant::WAITING]);
        $this->assertEquals(0, $stats2[Constant::PROCESSING]);
        $this->assertEquals(1, $stats2[Constant::FINISHED]);
    }

    public function testResetOnFinish(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 1;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // check stats
        $stats0 = $project->getCachedStatistics();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        $job_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($job_db));
        $this->assertEquals(Constant::FINISHED, $job_db->getStatus());

        $frames = $project->tiles();
        $a_frame = $frames[array_rand($frames)];
        $a_frame->reset();

        $job_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($job_db));
        $this->assertEquals(Constant::WAITING, $job_db->getStatus());
    }

    public function testResetOnPaused(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 1;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // check stats
        $stats0 = $project->getCachedStatistics();

        // render a frame
        $machine = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $machine, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->take($session));

        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        $job_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($job_db));
        $this->assertEquals(Constant::FINISHED, $job_db->getStatus());

        $job_db->pause();
        $this->assertEquals(Constant::PAUSED, $job_db->getStatus());

        $frames = $project->tiles();
        $a_frame = $frames[array_rand($frames)];
        $a_frame->reset();

        $job_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($job_db));
        $this->assertEquals(Constant::PAUSED, $job_db->getStatus());
    }

    public function testCostOnWaitingFrame(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];

        $this->assertEquals(0, $a_frame->getCost());
    }

    public function testCostOnRenderedFrame(): void {
        $user = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($renderer, $cpu, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];
        $this->assertEquals(0, $a_frame->take($session));
        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        // reload frame
        $frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId());

        $this->assertTrue($frame->getCost() >= Tile::MINIMUM_REWARD_PER_TILE);
    }

    public function testCostOnResetedFrame(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];
        $this->assertEquals(0, $a_frame->take($session));
        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        // reload frame
        $a_frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId());
        $a_frame->reset();
        $a_frame = $this->entityManager->getRepository(Tile::class)->find($a_frame->getId());

        $this->assertEquals(0, $a_frame->getCost());
    }

    public function testTotalCostAProject(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render frames
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        for ($i = 0; $i < $nb_frame; $i++) {
            $frames = $project->tilesWithStatus(Constant::WAITING);
            $a_frame = $frames[array_rand($frames)];
            $this->assertEquals(0, $a_frame->take($session));
            $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));
        }

        $status = $project->getStatus();
        $this->assertEquals(Constant::FINISHED, $status);

        $this->assertTrue($nb_frame * Tile::MINIMUM_REWARD_PER_TILE >= $project->getCost());
    }
}
