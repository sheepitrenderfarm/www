<?php

namespace App\Test;

use App\Constant;
use App\Entity\Blender;
use App\Entity\BlenderArch;
use App\Entity\CPU;
use App\Entity\FrameChessboard;
use App\Entity\GPU;
use App\Entity\HWIDBlacklist;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\ShepherdReal;
use App\Entity\Tile;
use App\Entity\TilePowerDetection;
use App\Entity\TileReal;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Entity\WebSessionClient;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocolService;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

trait TestTrait {
    private string $user_login_prefix = 'unittest';

    protected EntityManagerInterface $entityManager;
    protected UrlGeneratorInterface $router;
    protected Main $main;
    protected SessionInterface $phpSession;
    protected ClientProtocolService $clientProtocolService;
    protected ConfigService $configService;

    protected function init(): void {
        $this->router = static::getContainer()->get('router');
        $this->phpSession = new \Symfony\Component\HttpFoundation\Session\Session(new MockArraySessionStorage());
        /** @var EntityManagerInterface $obj */
        $obj = static::getContainer()->get('doctrine')->getManager();
        $this->entityManager = $obj;
        $this->configService = static::getContainer()->get('App\\Service\\ConfigService');
        $this->main = static::getContainer()->get('App\\Service\\Main');
        $this->clientProtocolService = new ClientProtocolService(
            $this->configService,
            $this->router,
            $this->entityManager,
            $this->main,
            $this->entityManager->getRepository(Session::class),
            $this->entityManager->getRepository(HWIDBlacklist::class),
            $this->entityManager->getRepository(Project::class),
            $this->entityManager->getRepository(Tile::class),
            $this->entityManager->getRepository(CPU::class),
            $this->entityManager->getRepository(GPU::class),
            $this->entityManager->getRepository(User::class),
            $this->entityManager->getRepository(UserPublicKey::class),
            $this->entityManager->getRepository(WebSessionClient::class),
            $this->entityManager->getRepository(BlenderArch::class)
        );

        GlobalInject::setInject($this->main, $this->router, $this->entityManager, $this->configService);
    }

    protected function unittestRand(): string {
        return rand().'_'.microtime(true);
    }

    public function getChunkProject1(): string {
        /** @var Project $o */
        $o = $this->entityManager->getRepository(Project::class)->find(1);
        return $o->getChunkStorageName(array_values($o->getChunks())[0]);
    }

    public function unittestCreateUser(bool $withAvatar = true, ?int $registration_time = null): User {
        $login = $this->user_login_prefix.'_'.Misc::code(3).'_'.rand();
        $user = new User();
        $user->setId($login);
        $user->setPassword(User::generateHashedPassword($user->getId(), $login));
        $user->setRegistrationTime(is_int($registration_time) ? $registration_time : time() - 180 * 86400);
        $user->setEmail($login.'@localhost');
        $user->setAffiliate('');
        $user->setTeamContribution(0.0);
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1);
        $user->setNotificationFromMask(User::NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED, 1);
        $user->setNotificationFromMask(User::NOTIFICATION_MASK_NEWSLETTER, 1);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $user_db = $this->entityManager->getRepository(User::class)->find($login);

        if (is_object($user_db) == false) {
            echo "failed to add user";
            exit(1);
        }

        if ($withAvatar) {
            $config = $this->configService->getData();
            copy(__DIR__.'/../../www/public/media/image/title.png', $config['storage']['path'].'users/avatars/small/'.$user_db->getId().'.png');
            copy(__DIR__.'/../../www/public/media/image/title.png', $config['storage']['path'].'users/avatars/big/'.$user_db->getId().'.png');
        }

        return $user_db;
    }

    protected function unittestCreateCpu(): CPU {
        $cpu = new CPU();
        $cpu->setModel('model_'.$this->unittestRand());
        $cpu->setCores(2);

        $cpu_sql = $this->entityManager->getRepository(CPU::class)->findFromData($cpu);

        if (is_object($cpu_sql) == false) {
            $this->entityManager->persist($cpu);
            $this->entityManager->flush();
        }
        else {
            $cpu = $cpu_sql;
        }

        return $cpu;
    }

    protected function unittestCreateGpu(int $vram = 200000000): GPU {
        $gpu = new GPU();
        $gpu->setType(GPU::TYPE_OPTIX);
        $gpu->setModel('gpu_model_'.$this->unittestRand());
        $gpu->setMemory($vram);

        $gpu_db = $this->entityManager->getRepository(GPU::class)->findFromData($gpu);
        if (is_object($gpu_db) == false) { // does the machine exists?
            $this->entityManager->persist($gpu);
            $this->entityManager->flush();
        }
        else {
            $gpu = $gpu_db;
        }

        return $gpu;
    }

    protected function unittestCreateSession(User $user, CPU $cpu, ?GPU $gpu, ?int $power_cpu = null, ?int $power_gpu = null): Session {
        $config = $this->configService->getData();

        $new_session = new Session();
        $new_session->setUser($user);
        $new_session->setCpu($cpu);
        $new_session->setMemory(20000000);
        $new_session->setTimestamp(time());
        $new_session->setOS('linux');
        $new_session->setVersion($config['client']['jar']['stable']['version']);
        $new_session->setCreationtime(time());
        $new_session->setComputeMethod(-1);
        $new_session->setGpu($gpu);
        $new_session->setBlocked(0);
        if (is_int($power_cpu)) {
            $new_session->setPowerCpu($power_cpu);
        }
        if (is_int($power_gpu)) {
            $new_session->setPowerCpu($power_gpu);
        }

        $this->entityManager->persist($new_session);
        $this->entityManager->flush();

        return $new_session;
    }

//    protected function unittest_create_scene(User $user, $public_render = '1') {
//        $config = $this->configService->getData();
//
//        // add scene
//        $default_binary = array_values($config['renderer']['binary'])[0]['filename'];
//        $project = new Scene();
//        $project->setExecutable($default_binary);
//        $project->setPublicRender($public_render);
//        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
//        $project->setName('name_'.$this->unittest_rand());
//        $project->setOwner($user);
//        $project->setPublicThumbnail('1');
//        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
//
//        $this->entityManager->persist($project);
//        $this->entityManager->flush();
//        return $project;
//    }

    protected function unittestCreateOrphanProject(User $user, bool $public_render = true): Project {
        $config = $this->configService->getData();

        // add project
        $project = new Project();
        $project->setExecutable($this->entityManager->getRepository(Blender::class)->getDefault());
        $project->setPublicRender($public_render);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName('name_'.$this->unittestRand());
        $project->setOwner($user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setPath('path_fake');
        $project->setPictureExtension('jpg');
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdInternal::ID));
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.zip';
        copy($config['storage']['path'].'projects'.'/'.$this->getChunkProject1(), $filename);

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $this->entityManager->getRepository(Project::class)->createChunks($project, $filename);

        return $project;
    }

    protected function unittestCreateProject(User $user, int $nb_frame, bool $public_render = true): Project {
        $config = $this->configService->getData();
        $project = $this->unittestCreateOrphanProject($user, $public_render);

        // revert file from unittest_create_orphan_project to have a new file on addFrames(...) (to add a daemon task)
        $dirname = $config['tmp_dir'].'/dir_'.$this->unittestRand();
        mkdir($dirname);
        file_put_contents($dirname.'/'.$project->getPath(), '');

        if ($this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, 1, FrameChessboard::class, TileReal::class, $dirname) != $nb_frame) {
            echo "failed to add job A";
            exit(1);
        }
        //    $this->entityManager->flush();
        return $project;
    }

    protected function unittestGenerateComputeMethodImageFile(): UploadedFile {
        $config = $this->configService->getData();
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.png';
        copy(__DIR__.'/../../www/public/media/image/compute-method-reference.png', $filename);
        return new UploadedFile($filename, basename($filename), 'image/png', test: true);
    }

    protected function unittestGenerateImageFile(): UploadedFile {
        $config = $this->configService->getData();
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.png';
        copy(__DIR__.'/../../www/public/media/image/title.png', $filename);
        return new UploadedFile($filename, basename($filename), 'image/png', test: true);
    }

    protected function unittestGenerateBlackImageFile(): UploadedFile {
        $config = $this->configService->getData();
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.jpg';
        copy(__DIR__.'/data/black.jpg', $filename);
        return new UploadedFile($filename, basename($filename), 'image/png', test: true);
    }

    protected function unittestClear(): void {
        foreach ($this->entityManager->getRepository(User::class)->findAll() as $u) {
            if (str_starts_with($u->getId(), $this->user_login_prefix)) {
                $this->entityManager->getRepository(User::class)->remove($u); // will clear projects, jobs and tasks
            }
        }
    }

    protected function unittestRemoveAllShepherds(): void {
        foreach ($this->entityManager->getRepository(Shepherd::class)->findAll() as $m) {
            if ($m instanceof ShepherdReal) {
                $this->entityManager->getRepository(Shepherd::class)->remove($m);
            }
        }
    }

    protected function unittestEnableAllShepherds(): void {
        foreach ($this->entityManager->getRepository(Shepherd::class)->findAll() as $m) {
            $m->setEnable(true);
            $m->setTimeout(false);
            $this->entityManager->flush();
        }
    }

    protected function unittestDisableAllShepherds(): void {
        foreach ($this->entityManager->getRepository(Shepherd::class)->findAll() as $m) {
            $m->setEnable(false);
            $m->setTimeout(true);
            $this->entityManager->flush();
        }
    }

    protected function unittestRemoveAllProjects(): void {
        foreach ($this->entityManager->getRepository(Project::class)->findAll() as $project) {
            if ($project->getId() != Project::COMPUTE_METHOD_PROJECT_ID && $project->getId() != Project::POWER_DETECTION_PROJECT_ID) {
                $this->entityManager->getRepository(Project::class)->remove($project);
            }
        }
        $this->entityManager->flush();

    }

    protected function unitestRenderComputeMethodTile(ClientProtocol $handler, ?User $user, int $computemethod): bool {
        if (is_object($user) == false) {
            Logger::error(__METHOD__.':'.__LINE__.'. failed');
            return false;
        }

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => '20000000',
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');

        if ($handler->configInput($user, null, $request) != ClientProtocol::OK) {
            Logger::error(__METHOD__.':'.__LINE__.'. failed');
            return false;
        }

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => $computemethod));
        if ($status != ClientProtocol::OK) {
            Logger::error(__METHOD__.':'.__LINE__.'. failed');
            return false;
        }

        if (is_object($frame) == false) {
            Logger::error(__METHOD__.':'.__LINE__." ($handler) failed");
            return false;
        }

        if ($frame->isComputeMethod() == false) {
            Logger::error(__METHOD__.':'.__LINE__." ($handler) failed $frame");
            return false;
        }

        $frame->setRenderTime(2);
        $retValidate = $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession);
        if ($retValidate != 0) {
            Logger::error(__METHOD__.':'.__LINE__.'. failed ret: '.$retValidate);
            return false;
        }

        return true;
    }

    protected function unitestRenderPowerDetectionFrame(ClientProtocol $handler, int $computemethod): bool {
        $config = $this->configService->getData();

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => $computemethod));
        if ($status != ClientProtocol::OK) {
            Logger::error(__METHOD__.':'.__LINE__.' failed status: '.$status);
            return false;
        }
        if (is_object($frame) == false) {
            Logger::error(__METHOD__.':'.__LINE__.' failed');
            return false;
        }

        if ($frame->isComputeMethod()) {
            Logger::error(__METHOD__.':'.__LINE__.' failed');
            return false;
        }

        if ($frame->isPowerDetection() == false) {
            Logger::error(__METHOD__.':'.__LINE__.' failed');
            return false;
        }

        $session = $handler->getSession();

        $frame->setRenderTime($config['power']['default_time'] / 2.0);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($config['power']['cpu']['const'] * 2.0 / TilePowerDetection::SCALE);
        $retValidate = $session->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession);
        if ($retValidate != 0) {
            Logger::error(__METHOD__.':'.__LINE__.'. failed ret: '.$retValidate);
            return false;
        }

        $cpuIsEnabled = Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpuIsEnabled = Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_GPU);

        if ($cpuIsEnabled && $gpuIsEnabled) {
            // a third power detection frame is given
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => $computemethod));
            if ($status != ClientProtocol::OK) {
                Logger::error(__METHOD__.':'.__LINE__.'. failed');
                return false;
            }
            if (is_object($frame) == false) {
                Logger::error(__METHOD__.':'.__LINE__.'. failed');
                return false;
            }

            if ($frame->isComputeMethod()) {
                Logger::error(__METHOD__.':'.__LINE__.'. failed');
                return false;
            }

            if ($frame->isPowerDetection() == false) {
                echo __METHOD__."($handler) failed line: ".__LINE__."\n";
                return false;
            }

            $session = $handler->getSession();

            $frame->setRenderTime($config['power']['default_time'] / 2.0);
            /** @var TilePowerDetection $frame */
            $frame->setSpeedSamplesRendered($config['power']['cpu']['const'] * 2.0 / TilePowerDetection::SCALE);
            if ($session->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession) != 0) {
                echo __METHOD__."($handler) failed line: ".__LINE__."\n";
                return false;
            }
        }

        return true;
    }

    protected function unittestRenderFirstTwoTiles(ClientProtocol $handler, ?User $user, int $computemethod): bool {
        if ($this->unitestRenderComputeMethodTile($handler, $user, $computemethod) == false) {
            Logger::error(__METHOD__.':'.__LINE__.' failed compute methode frame');
            return false;
        }

        if ($this->unitestRenderPowerDetectionFrame($handler, $computemethod) == false) {
            Logger::error(__METHOD__.':'.__LINE__.' failed power detection frame');
            return false;
        }


        return true;
    }

    protected function unittestRenderOneTile(ClientProtocol $handler, int $computemethod): bool {
        list($status, $tile, $remaining_tiles, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => $computemethod));
        if ($status != ClientProtocol::OK) {
            Logger::error(__METHOD__.' jobRequestInput failed ret: '.$status);
            echo "unittest_render_one_frame($handler, $computemethod) failed line: ".__LINE__." got: ".$status."\n";
            return false;
        }

        if (is_object($tile) == false) {
            echo "unittest_render_one_frame($handler, $computemethod) failed line: ".__LINE__."\n";
            return false;
        }

//        $job = $frame->getJob();
//        if (is_object($job) == false) {
//            echo "unittest_render_one_frame($handler, $computemethod) failed line: ".__LINE__."\n";
//            return false;
//        }

        $retValidateFromShepherd = $tile->validateFromShepherd(10, 0, 10000);
        if ($retValidateFromShepherd != 0) {
            Logger::error(__METHOD__.' validateFromShepherd failed ret: '.$retValidateFromShepherd);
            return false;
        }
        return true;
    }

}