<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\HWIDBlacklist;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use App\Utils\Misc;
use DOMDocument;
use Symfony\Component\HttpFoundation\FileBag;

class ClientProtocol60Test extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testConfigNoAuth(): void {
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $login, $publickey_id) = $handler->userAuth(array());
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $config_node->getAttribute('status'));
    }

    public function testConfigAuthFailedWithWrongPublicKey(): void {
        $user = $this->unittestCreateUser();

        $publickey = $this->entityManager->getRepository(UserPublicKey::class)->add($user);
        $this->assertTrue(is_object($publickey));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $login, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $publickey->getId().'WRONG'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $config_node->getAttribute('status'));
    }

    public function testConfigAuthOk(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $login, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $config_node->getAttribute('status'));
    }

    public function testConfigAuthOkWithPublicKey(): void {
        $user = $this->unittestCreateUser();

        $publickey = $this->entityManager->getRepository(UserPublicKey::class)->add($user);
        $this->assertTrue(is_object($publickey));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $publickey->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $config_node->getAttribute('status'));
    }

    public function testConfigAuthOkHasFullConf(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $config_node->getAttribute('status'));

        $request_nodes = $dom->getElementsByTagName('request');

        $mandatoryTypes = array("request-job" => "request-job", "download-binary" => "download-binary", "error" => "error", "keepmealive" => "keepmealive", "logout" => "logout");

        foreach ($request_nodes as $request_node) {
            $this->assertTrue($request_node->hasAttribute('type'));
            $this->assertTrue($request_node->hasAttribute('path'));
            if (in_array($request_node->getAttribute('type'), $mandatoryTypes)) {
                unset($mandatoryTypes[$request_node->getAttribute('type')]);
            }
        }
        $this->assertEquals(0, count($mandatoryTypes));
    }

    public function testJobOutputUserNotRightToRender(): void {
        $user = $this->unittestCreateUser();
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 0);
        $this->entityManager->flush();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 20000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER, $status);
        $this->assertNull($frame);
        $this->assertEquals(0, $remaining_frames);

        $xmlstring = $handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, array());
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobrequest_node = $dom->getElementsByTagName('jobrequest')->item(0);
        $this->assertFalse(is_null($jobrequest_node));
        $this->assertTrue($jobrequest_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER, $jobrequest_node->getAttribute('status'));
    }

    public function testJobOutputHardwareNotRightToRender(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler('7.23272.0', null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $hwid = md5(Misc::code(100));

        $this->entityManager->getRepository(HWIDBlacklist::class)->add($hwid, 'user66666666', 1); // different user

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 20000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit',
            'hwid' => $hwid,
        );

        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
        $this->assertNull($frame);
        $this->assertEquals(0, $remaining_frames);
    }

    public function testJobOutputNoJob(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);

        $xmlstring = $handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, array());
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobrequest_node = $dom->getElementsByTagName('jobrequest')->item(0);
        $this->assertFalse(is_null($jobrequest_node));
        $this->assertTrue($jobrequest_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $jobrequest_node->getAttribute('status'));
    }

    public function testJobOutputServerIsMaintenanceAfter3220(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler("3.220.0000", null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->configService->set('scheduler', 'running', false);

        // reload handler (due to config change)
        $handler = $this->clientProtocolService->getHandler("3.220.0000", $handler->getSession(), $this->phpSession);

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_SERVER_IN_MAINTENANCE, $status);

        $xmlstring = $handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, array());
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobrequest_node = $dom->getElementsByTagName('jobrequest')->item(0);
        $this->assertFalse(is_null($jobrequest_node));
        $this->assertTrue($jobrequest_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_SERVER_IN_MAINTENANCE, $jobrequest_node->getAttribute('status'));

        // put back the value (for the next test)
        $this->configService->set('scheduler', 'running', true);
    }

    public function testJobOutputServerOverloadedAfter220(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler("3.220.0000", null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $old_trigger = $config['scheduler']['trigger_overload'];
        $this->configService->set('scheduler', 'trigger_overload', 0); // to be sure it's going to trigger it

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_SERVER_OVERLOADED, $status);

        $xmlstring = $handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, array());
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobrequest_node = $dom->getElementsByTagName('jobrequest')->item(0);
        $this->assertFalse(is_null($jobrequest_node));
        $this->assertTrue($jobrequest_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_SERVER_OVERLOADED, $jobrequest_node->getAttribute('status'));

        // put back the value (for the next test)
        $this->configService->set('scheduler', 'trigger_overload', $old_trigger);
    }

    public function testJobOutputOK(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;

        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);

        $xmlstring = $handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, array());
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobrequest_node = $dom->getElementsByTagName('jobrequest')->item(0);
        $this->assertFalse(is_null($jobrequest_node));
        $this->assertTrue($jobrequest_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $jobrequest_node->getAttribute('status'));

        $stats_nodes = $jobrequest_node->getElementsByTagName('stats');
        $this->assertEquals(1, $stats_nodes->length);
        $stats_node = $stats_nodes->item(0);
        $this->assertTrue($stats_node->hasAttribute('credits_session'));
        $this->assertTrue($stats_node->hasAttribute('credits_total'));
        $this->assertTrue($stats_node->hasAttribute('frame_remaining'));
        $this->assertTrue($stats_node->hasAttribute('waiting_project'));
        $this->assertTrue($stats_node->hasAttribute('connected_machine'));
        $this->assertTrue($stats_node->hasAttribute('renderable_project'));

        $job_nodes = $jobrequest_node->getElementsByTagName('job');
        $this->assertEquals(1, $job_nodes->length);
        $job_node = $job_nodes->item(0);
        $this->assertTrue($job_node->hasAttribute('id'));
        $this->assertTrue($job_node->hasAttribute('use_gpu'));
        $this->assertTrue($job_node->hasAttribute('path'));
        $this->assertTrue($job_node->hasAttribute('frame'));
        $this->assertTrue($job_node->hasAttribute('synchronous_upload'));
        $this->assertTrue($job_node->hasAttribute('name'));

        $renderer_nodes = $job_node->getElementsByTagName('renderer');
        $this->assertEquals(1, $renderer_nodes->length);
        $renderer_node = $renderer_nodes->item(0);
        $this->assertTrue($renderer_node->hasAttribute('md5'));
        $this->assertTrue($renderer_node->hasAttribute('commandline'));
        $this->assertTrue($renderer_node->hasAttribute('update_method'));

        $chunks_nodes = $job_node->getElementsByTagName('chunks');
        $this->assertEquals(1, $chunks_nodes->length);
    }

    public function testJobValidationOutputOK(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;

        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));

//        $job = $frame->getJob();
//        $this->assertTrue(is_object($job));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateImageFile());
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files); // true => $disable_security_check
        $this->assertEquals(ClientProtocol::OK, $status);

        $xmlstring = $handler->jobValidationOutput($status);
        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $jobvalidate_node = $dom->getElementsByTagName('jobvalidate')->item(0);
        $this->assertFalse(is_null($jobvalidate_node));
        $this->assertTrue($jobvalidate_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $jobvalidate_node->getAttribute('status'));
    }

    public function testRenderTimeNotANumber(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();
        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => '20000000',
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');

        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        // test compute method frame
        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));

//        $job = $frame->getJob();
//        $this->assertTrue(is_object($job));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => "\u09eb\u09ec\u09ef", // a machine from Bangladesh have actually sent that...
            'extras' => '');

        $files = new FileBag();
        $files->set('file', $this->unittestGenerateImageFile());
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files); // true => $disable_security_check
        $this->assertEquals(ClientProtocol::JOB_VALIDATION_ERROR_MISSING_PARAMETER, $status);
    }

    public function testConfigDoGivePublicKey(): void {

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $login, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
        $this->assertIsObject($publickey_id);

        $xmlstring = $handler->configOutput($error_state, $publickey_id);

        $this->assertTrue(strlen($xmlstring) > 0);

        $dom = new DomDocument('1.0', 'utf-8');
        $this->assertTrue(@$dom->loadXML($xmlstring));
        $this->assertTrue($dom->hasChildNodes());

        $config_node = $dom->getElementsByTagName('config')->item(0);
        $this->assertFalse(is_null($config_node));
        $this->assertTrue($config_node->hasAttribute('status'));
        $this->assertEquals(ClientProtocol::OK, $config_node->getAttribute('status'));

        $this->assertTrue($config_node->hasAttribute('publickey'));
    }

    public function testConfigDoGiveSamePublicKey(): void {

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user1, $publickey_id1) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
        $this->assertIsObject($publickey_id1);

        list($error_state, $user2, $publickey_id2) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
        $this->assertIsObject($publickey_id2);

        $this->objectEquals($publickey_id1, $publickey_id2);
    }
}
