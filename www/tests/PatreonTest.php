<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\Patreon;
use DateTime;

class PatreonTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testIsPatreonNoData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPatreon());
    }

    public function testIsPatreonWithData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPatreon());

        $patreon = new Patreon();
        $patreon->setUser($user);
        $patreon->setId('something'.$this->unittestRand().'@localhost');
        $patreon->setLastCharge(date_format(new DateTime(), 'Y-m'));
        $this->entityManager->persist($patreon);
        $this->entityManager->flush();

        $this->assertTrue($user->isPatreon());
    }

    public function testIsPatreonWithExpireData(): void {
        $user = $this->unittestCreateUser();
        $this->assertFalse($user->isPatreon());

        $patreon = new Patreon();
        $patreon->setUser($user);
        $patreon->setId('something'.$this->unittestRand().'@localhost');
        $patreon->setLastCharge(date_format(DateTime::createFromFormat('Y-m-d', date('Y-m-01'))->sub(\DateInterval::createFromDateString('2 month')), 'Y-m')); // use 01 due to bug on month duration in datatime
        $this->entityManager->persist($patreon);
        $this->entityManager->flush();

        $this->assertFalse($user->isPatreon());
    }
}
