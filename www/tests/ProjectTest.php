<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\FrameChessboard;
use App\Entity\Project;
use App\Entity\TileReal;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class ProjectTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAnimationActualStatusWaiting(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $this->assertEquals(Constant::WAITING, $project->getStatus());
    }

    public function testAnimationActualStatusRendering(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        $frames = $project->tilesWithStatus(Constant::WAITING);

        $frame = $frames[array_rand($frames)];

        // render a frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        $this->assertEquals(0, $frame->take($session));

        $this->assertEquals(Constant::PROCESSING, $project->getStatus());
    }

    public function testAnimationActualStatusFinished(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        // render all frames
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        for ($i = 0; $i < $nb_frame; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $job = $this->entityManager->getRepository(Project::class)->find($project->getId());// reload the object
        $this->assertTrue(is_object($job));

        $this->assertEquals(Constant::FINISHED, $job->getStatus());
    }

    public function testAnimationActualStatusPaused(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $project->pause();

        $this->assertEquals(Constant::PAUSED, $project->getStatus());
    }


    public function testAnimationTileActualStatusWaiting(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $project = $this->unittestCreateOrphanProject($user);

        $nb_frame = 5;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, null));

        $this->assertEquals(Constant::WAITING, $project->getStatus());
    }

    public function testAnimationTileActualStatusRendering(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $project = $this->unittestCreateOrphanProject($user);

        $nb_frame = 5;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, null));

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        $frames = $project->tilesWithStatus(Constant::WAITING);

        $frame = $frames[array_rand($frames)];

        // render a frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        $this->assertEquals(0, $frame->take($session));

        $this->assertEquals(Constant::PROCESSING, $project->getStatus());
    }

    public function testAnimationTileActualStatusFinished(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $project = $this->unittestCreateOrphanProject($user);

        $nb_frame = 5;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, null));

        // render all frames
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        for ($i = 0; $i < $nb_frame * $nb_tile_per_frame; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $project = $this->entityManager->getRepository(Project::class)->find($project->getId());// reload the object
        $this->assertTrue(is_object($project));

        $this->assertEquals(Constant::FINISHED, $project->getStatus());
    }

    public function testAnimationTileActualStatusPaused(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $project = $this->unittestCreateOrphanProject($user);

        $nb_frame = 5;
        $nb_tile_per_frame = $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
        $this->assertEquals($nb_frame * $nb_tile_per_frame, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $nb_tile_per_frame, FrameChessboard::class, TileReal::class, null));


        $project->pause();

        $this->assertEquals(Constant::PAUSED, $project->getStatus());
    }

    public function testBlockingAProjectDoesNOTUnPausedIt(): void {
        $owner = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        // pause the project
        $project->pause();
        $this->assertEquals(Constant::PAUSED, $project->getStatus());

        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, 1);

        // the project should remain paused
        $this->assertEquals(Constant::PAUSED, $project->getStatus());
    }

    public function testNewProjectStatusIsWaiting(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $status = $project->getStatus();
        $this->assertEquals(Constant::WAITING, $status);
    }

    public function testProjectStatusIsRendering(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render a frame
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        $frames = $project->tilesWithStatus(Constant::WAITING);
        $a_frame = $frames[array_rand($frames)];
        $this->assertEquals(0, $a_frame->take($session));

        $status = $project->getStatus();
        $this->assertEquals(Constant::PROCESSING, $status);

        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        $status = $project->getStatus();
        $this->assertEquals(Constant::PROCESSING, $status);
    }

    public function testProjectStatusIsFinished(): void {
        $user = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($user, $nb_frame);

        // render frames
        $cpu = $this->unittestCreateCpu();
        $session = $this->unittestCreateSession($user, $cpu, null);

        for ($i = 0; $i < $nb_frame; $i++) {
            $frames = $project->tilesWithStatus(Constant::WAITING);
            $a_frame = $frames[array_rand($frames)];
            $this->assertEquals(0, $a_frame->take($session));
            $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));
        }

        $status = $project->getStatus();
        $this->assertEquals(Constant::FINISHED, $status);
    }
}
