<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Project;
use App\Entity\Tile;
use App\Entity\User;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class ClientKeepMeAliveTest extends BaseTestCase {
    private User $renderer;

    protected function setUp(): void {
        parent::setUp();

        $this->unittestClear();
        $this->unittestRemoveAllProjects();

        $this->renderer = $this->unittestCreateUser();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
        parent::tearDown();
    }

    public function testFrameDoesNotExists(): void {
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $this->renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $session = $handler->getSession();

        $job_id = rand();
        while (is_object($this->entityManager->getRepository(Project::class)->find($job_id))) {
            $job_id = rand();
        }

        $request = array(
            'job' => $job_id,
            'frame' => '0000',
            'extras' => '',
            'rendertime' => '10',
            'remainingtime' => '20');

        $status = $handler->keepMeAliveInput($request);
        $this->assertEquals(ClientProtocol::KEEPMEALIVE_STOP_RENDERING, $status);
    }

    public function testFrameAlreadyRendered(): void {
        $nb_frame = 20;
        $project = $this->unittestCreateProject($this->renderer, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $this->renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $session = $handler->getSession();

        $tiles = $session->tilesWithStatus(Constant::FINISHED);
        $this->assertCount(1, $tiles);
        /** @var Tile $tile */
        $tile = array_pop($tiles);

        $request = array(
            'job' => $tile->getId(),
            'frame' => '0000',
            'extras' => '',
            'rendertime' => '10',
            'remainingtime' => '20');

        $status = $handler->keepMeAliveInput($request);
        $this->assertEquals(ClientProtocol::KEEPMEALIVE_STOP_RENDERING, $status);
    }

    public function testRendering(): void {
        $nb_frame = 20;
        $project = $this->unittestCreateProject($this->renderer, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $this->renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'extras' => '',
            'rendertime' => '10',
            'remainingtime' => '20');

        $status = $handler->keepMeAliveInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
    }

    public function testWillTakeTooMunchTime(): void {
        $config = $this->configService->getData();

        $nb_frame = 20;
        $project = $this->unittestCreateProject($this->renderer, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $this->renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'extras' => '',
            'rendertime' => $config['power']['rendertime_max_reference'] + 100,
            'remainingtime' => $config['session']['timeoutrender'] + 100);

        $status = $handler->keepMeAliveInput($request);
        $this->assertEquals(ClientProtocol::KEEPMEALIVE_STOP_RENDERING, $status);
    }
}
