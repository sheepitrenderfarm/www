<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\Tile;
use App\Entity\TilePowerDetection;
use App\Entity\User;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use Symfony\Component\HttpFoundation\FileBag;

class ClientTest extends BaseTestCase {

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAuthNoCredentials(): void {
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array());
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);
    }

    public function testAuthUserDoesnotExist(): void {
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => 'some_user', 'password' => 'some_password'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);
    }

    public function testAuthNoPasswordGiven(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => 'some_password'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);
    }

    public function testAuthFailed(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => 'wrong password'));
        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED, $error_state);
    }

    public function testAuthOk(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => $user->getId(), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
    }

    public function testAuthOkNoCaseSensitive(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        list($error_state, $user, $publickey_id) = $handler->userAuth(array('login' => strtoupper($user->getId()), 'password' => $user->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
    }

    public function testConfigEmpty(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertEquals(ClientProtocol::CONFIGURATION_ERROR_MISSING_PARAMETER, $handler->configInput($user, null, array()));
    }

    public function testConfigOK(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $request = array(
            'cpu_family' => 'family1',
            'cpu_model' => 'model1',
            'cpu_model_name' => 'name1',
            'ram' => '80000000',  // 80GB of ram
            'cpu_cores' => '1',
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));
    }

    public function testUserHaveNoRightToRender(): void {
        $user = $this->unittestCreateUser();
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 0);
        $this->entityManager->flush();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        list($status, $frame, $remaining_frames, $renderable_project) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER, $status);
        $this->assertNull($frame);
        $this->assertEquals(0, $remaining_frames);
    }

    public function testFirstFrameIsComputeMethod(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $request = array(
            'cpu_family' => 'family1',
            'cpu_model' => 'model1',
            'cpu_model_name' => 'name1',
            'ram' => '80000000',  // 80GB of ram
            'cpu_cores' => '1',
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());
    }

    public function testSecondFrameIsPowerDetectionOnNewMachine(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());
    }

    public function testgetPowerDetectionOnNewMachineOnCoresChange(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());
        $frame->setRenderTime(2);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($this->configService->getData()['power']['cpu']['const'] / TilePowerDetection::SCALE);

        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession));

        // add a job and check if we do get a 'regular' frame
        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());
        $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));

        // don't change the number of core => 'regular' frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());
        $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));

        // change the number of core => 'PowerDetection' frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY, 'cpu_cores' => $cpu->getCores() + 1));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());
    }

    public function testGPURenderingComputeMethodFrameOK(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $gpu = $this->unittestCreateGpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateComputeMethodImageFile());
        /** @var ?Tile $frame */
        /** @phpstan-ignore-next-line */
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertTrue($frame->isComputeMethod());
        $this->assertEquals(0, $session->validateFrame($a_frame, $file, $this->phpSession));
    }

    public function testGPURenderingComputeMethodFrameBlackImage(): void {
        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $gpu = $this->unittestCreateGpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateBlackImageFile());
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files); // true => $disable_security_check
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertIsObject($a_frame);
        $this->assertTrue($frame->isComputeMethod());
        $this->assertEquals(ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE, $session->validateFrame($a_frame, $file, $this->phpSession));
    }
}
