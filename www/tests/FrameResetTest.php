<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Blender;
use App\Entity\FrameChessboard;
use App\Entity\FrameLayer;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\TileReal;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use App\Utils\Misc;

class FrameResetTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
        parent::tearDown();
    }

    /*
    public function testRenderingTooMuchTime() {
        $config = $this->configService->getData();

        $user = $this->unittest_create_user();
        $this->assertTrue(is_object($user));
        $user->setRenderedFrames(2); // to avoid power detection frame
        $this->assertTrue($user->update());

        $project = $this->unittest_create_project($user);
        $this->assertTrue(is_object($project));

        $nb_frame = 10;
        $job = $this->unittest_create_job($project, $nb_frame);
        $this->assertTrue(is_object($job));

        $cpu = $this->unittest_create_cpu();
        $this->assertTrue(is_object($cpu));
        $cpu->setRenderedFrames(2); // to avoid power detection frame
        $cpu->setPower(10000);
        $this->assertTrue($cpu->update());

        $session = $this->unittest_create_session($user, $cpu, null);
        $this->assertTrue(is_object($session));

        $handler = ClientProtocol::getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->router, $this->phpSession);
        $this->assertTrue(is_object($handler));

        $session = $handler->getSession();
        $this->assertTrue(is_object($session));

        $request = array(
                    'cpu_model_name' => $cpu->getModel(),
                    'ram' => 8000000,
                    'cpu_cores' => $cpu->getCores(),
                    'os' => 'linux',
                    'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $request = array(
                    'job' => $job->getId(),
                    'frame' => $frame->getNumber(),
                    'rendertime' => 10,
                    'extras' => '');
        $files = array();
        $files['file']['tmp_name'] = unittest_generate_compute_method_image_file();
        $tmp_file_post = $files['file']['tmp_name'];
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files, true); // true => $disable_security_check
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertTrue($frame->isComputeMethod());
        $this->assertTrue($handler->getSession()->validateFrame($a_frame, $file) == 0);

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY, 'cpu_cores' => $cpu->getCores()));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $request = array(
                    'job' => $job->getId(),
                    'frame' => $frame->getNumber(),
                    'rendertime' => 10,
                    'extras' => '');
        $files = array();
        $files['file']['tmp_name'] = unittest_generate_compute_method_image_file();
        $tmp_file_post = $files['file']['tmp_name'];
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files, true); // true => $disable_security_check
        $this->assertTrue($status == ClientProtocol::OK);
        $this->assertTrue(is_object($a_frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($handler->getSession()->validateFrame($a_frame, $file) == 0);

        // third frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY, 'cpu_cores' => $cpu->getCores()));
        $this->assertTrue($status == ClientProtocol::OK);
        $this->assertTrue(is_object($frame));

        $frame_db = $this->main->importFrame($frame->getJob(), $frame->getNumber());
        $this->assertTrue(is_object($frame_db));
        $this->assertTrue($frame_db->getStatus() == Constant::PROCESSING);

        $project = $frame_db->getScene();
        $this->assertTrue(is_object($project));

        // check stats
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($project_db));
        $stats0 = $project_db->getCachedStatistics();
        $this->assertTrue(is_array($stats0));
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats0));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats0));
        $processing = $stats0[Constant::PROCESSING];
        $waiting = $stats0[Constant::WAITING];
        $frame_db->setRequestTime(time() - $config['session']['timeoutrender'] - 600);
        $frame_db->setRenderTime($config['session']['timeoutrender']);
        $this->assertTrue($frame_db->update());

        $points = $this->entityManager->getRepository(User::class)->find($user->getId())->getPoints();
        $this->main->clearCache();
        cron_5minutes();
        $this->main->clearCache();

        $frame_db = $this->main->importFrame($frame->getJob(), $frame->getNumber());
        $this->assertTrue(is_object($frame_db));
        $this->assertTrue($frame_db->getStatus() == Constant::WAITING);

        // check stats again
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($project_db));
        $stats0 = $project_db->getCachedStatistics();
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats0));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats0));
        $this->assertTrue($stats0[Constant::PROCESSING] == ($processing - 1));
        $this->assertTrue($stats0[Constant::WAITING] == ($waiting + 1));

        // on reset, user should have got points for the render time
        $user_db = $this->entityManager->getRepository(User::class)->find($user->getId());
        $this->assertTrue($user_db->getPoints() > $points);
    }

    public function testRenderingDeadSession() {
        $config = $this->configService->getData();

        $user = $this->unittest_create_user();
        $this->assertTrue(is_object($user));
        $user->setRenderedFrames(2); // to avoid power detection frame
        $this->assertTrue($user->update());

        $project = $this->unittest_create_scene($user);
        $this->assertTrue(is_object($project));

        $nb_frame = 10;
        $job = $this->unittest_create_job($project, $nb_frame);
        $this->assertTrue(is_object($job));

        $cpu = $this->unittest_create_cpu();
        $this->assertTrue(is_object($cpu));
        $cpu->setRenderedFrames(2); // to avoid power detection frame
        $cpu->setAttribute('power', 10000);
        $this->assertTrue($cpu->update());

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue(is_object($handler));

        $request = array(
                    'cpu_model_name' => $cpu->getModel(),
                    'ram' => 8000000,
                    'cpu_cores' => $cpu->getCores(),
                    'os' => 'linux',
                    'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $request = array(
                    'job' => $job->getId(),
                    'frame' => $frame->getNumber(),
                    'rendertime' => 10,
                    'extras' => '');
        $files = array();
        $files['file']['tmp_name'] = unittest_generate_compute_method_image_file();
        $tmp_file_post = $files['file']['tmp_name'];
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files, true); // true => $disable_security_check
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertTrue($frame->isComputeMethod());
        $this->assertTrue($handler->getSession()->validateFrame($a_frame, $file) == 0);

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isPowerDetection());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $request = array(
                    'job' => $job->getId(),
                    'frame' => $frame->getNumber(),
                    'rendertime' => 10,
                    'extras' => '');
        $files = array();
        $files['file']['tmp_name'] = unittest_generate_compute_method_image_file();
        $tmp_file_post = $files['file']['tmp_name'];
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files, true); // true => $disable_security_check
        $this->assertTrue($status == ClientProtocol::OK);
        $this->assertTrue(is_object($a_frame));
        $this->assertTrue($frame->isPowerDetection());
        $this->assertTrue($handler->getSession()->validateFrame($a_frame, $file) == 0);

        // third frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($status == ClientProtocol::OK);
        $this->assertTrue(is_object($frame));

        $frame_db = $this->main->importFrame($frame->getJob(), $frame->getNumber());
        $this->assertTrue(is_object($frame_db));
        $this->assertTrue($frame_db->getStatus() == Constant::PROCESSING);

        $project = $frame_db->getScene();
        $this->assertTrue(is_object($project));

        // check stats
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($project_db));
        $stats0 = $project_db->getCachedStatistics();
        $this->assertTrue(is_array($stats0));
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats0));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats0));
        $processing = $stats0[Constant::PROCESSING];
        $waiting = $stats0[Constant::WAITING];

        $handler->getSession()->setTimestamp(time() - $config['session']['timeout'] - 600);
        $this->assertTrue($handler->getSession()->update());

        $this->main->clearCache();
        cron_5minutes();
        $this->main->clearCache();

        $frame_db = $this->main->importFrame($frame->getJob(), $frame->getNumber());
        $this->assertTrue(is_object($frame_db));
        $this->assertTrue($frame_db->getStatus() == Constant::WAITING);

        // check stats again
        $project_db = $this->entityManager->getRepository(Project::class)->find($project->getId());
        $this->assertTrue(is_object($project_db));
        $stats0 = $project_db->getCachedStatistics();
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats0));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats0));
        $this->assertTrue($stats0[Constant::PROCESSING] == ($processing - 1));
        $this->assertTrue($stats0[Constant::WAITING] == ($waiting + 1));
    }

    public function testResetRenderedFrame() {
        $config = $this->configService->getData();

        $user = $this->unittest_create_user();
        $this->assertTrue(is_object($user));
        $user->setRenderTime(1000);
        $user->setRenderedFrames(10); // to avoid power detection frame
        $this->assertTrue($user->update());

        $this->assertTrue((int)($user->getPoints()) == 0);
        $this->assertTrue((int)($user->getPointsEarn()) == 0);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue(is_object($handler));

        $cpu = $this->unittest_create_cpu();
        $this->assertTrue(is_object($cpu));
        $cpu->setAttribute('power', $config['power']['const'] / 2);
        $cpu->setRenderedFrames(1); // to avoid power detection frame
        $this->assertTrue($cpu->update());

        $project = $this->unittest_create_scene($user);
        $this->assertTrue(is_object($project));

        $job = new JobAnimation();
        $job->setPath('path_fake');
        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);

        $nb_frames = 10;
        $this->assertTrue($project->addproject($job, 1, $nb_frames, 1, 0, $filename, false) == $nb_frames);

        $request = array(
                'cpu_model_name' => $cpu->getModel(),
                'ram' => 8000000,
                'cpu_cores' => $cpu->getCores(),
                'os' => 'linux',
                'bits' => '64bit');
        $this->assertTrue($handler->configInput($user, null, $request) == ClientProtocol::OK);

        $session = $handler->getSession();
        $this->assertTrue(is_object($session));

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $frame->setRenderTime(10);
        $this->assertTrue($session->validateFrame($frame, $this->unittest_generate_compute_method_image_file()) == 0);

        // second power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $frame->setRenderTime(10);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittest_generate_image_file()));

        // third actual frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($status == ClientProtocol::OK);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());

        $job = $frame->getJob();
        $this->assertTrue(is_object($job));

        $frame->setRenderTime(10);
        sleep(10);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittest_generate_image_file()));

        $stats0 = $project->getCachedStatistics();
        $this->assertTrue(is_array($stats0));

        $this->assertTrue(array_key_exists(Constant::FINISHED, $stats0));
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats0));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats0));
        $this->assertTrue(array_key_exists('rendertime_actual', $stats0));
        $this->assertTrue(array_key_exists('rendertime_ref', $stats0));
        $this->assertTrue($stats0[Constant::FINISHED] == 1);
        $this->assertTrue($stats0[Constant::PROCESSING] == 0);
        $this->assertTrue($stats0[Constant::WAITING] == 9);
        $this->assertTrue($stats0['rendertime_actual'] > 0);
        $this->assertTrue($stats0['rendertime_ref'] > 0);

        $frame->reset();

        $stats1 = $project->getCachedStatistics();
        $this->assertTrue(is_array($stats1));
        $this->assertTrue(array_key_exists(Constant::FINISHED, $stats1));
        $this->assertTrue(array_key_exists(Constant::WAITING, $stats1));
        $this->assertTrue(array_key_exists(Constant::PROCESSING, $stats1));
        $this->assertTrue(array_key_exists('rendertime_actual', $stats1));
        $this->assertTrue(array_key_exists('rendertime_ref', $stats1));
        $this->assertTrue($stats1[Constant::FINISHED] == 0);
        $this->assertTrue($stats1[Constant::PROCESSING] == 0);
        $this->assertTrue($stats1[Constant::WAITING] == 10);
        $this->assertTrue($stats1['rendertime_actual'] == 0);
        $this->assertTrue($stats1['rendertime_ref'] == 0);
    }

    */

    public function testUpdateJobAnimationStatusOnResetFrame(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $nb_frame = 5;
        $nb_tile = $nb_frame;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        // render a frame
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        for ($i = 0; $i < $nb_tile; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $this->assertEquals(Constant::FINISHED, $project->getStatus());
        $project = $this->entityManager->getRepository(Project::class)->find($project->getId()); // reload the job from cache
        $this->assertEquals(Constant::FINISHED, $project->getStatus());

        // reset one frame
        $frames = $project->tiles();
        $frame = $frames[array_rand($frames)];
        $frame->reset();

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        $job2 = $frame->getFrame()->getProject();
        $this->assertEquals(Constant::WAITING, $job2->getStatus());
    }

    public function testUpdateJobAnimationTileStatusOnResetFrame(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

//        $project = $this->unittest_create_scene($user);
//        $this->assertTrue(is_object($project));

        $nb_frames = 5;
        $nb_tile = $nb_frames * $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'];
//        $class_name = '\App\Entity\Job'.Job::$type_animation_tile;
//        $job = new $class_name();
//        $job->setPath('path_fake');
//        $job->setPictureExtension('jpg');
//        $this->entityManager->persist($job);
//        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
//        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);
//        $this->assertEquals($nb_tile, $project->addproject($job, 1, $nb_frame, 1, 1920, 1080, 0, $filename));

        // add project
        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();
        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName('name_'.$this->unittestRand());
        $project->setOwner($user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setPath('path_fake');
        $project->setPictureExtension('jpg');
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdInternal::ID));

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $this->assertEquals($nb_tile, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frames, 1, 1920, 1080, $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'], FrameChessboard::class, TileReal::class));

        $this->entityManager->getRepository(Project::class)->createChunks($project, $config['storage']['path'].'projects'.'/'.$this->getChunkProject1());

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        // render a frame
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        for ($i = 0; $i < $nb_tile; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $this->assertEquals(Constant::FINISHED, $project->getStatus());
        $job = $this->entityManager->getRepository(Project::class)->find($project->getId()); // reload the job from cache
        $this->assertEquals(Constant::FINISHED, $job->getStatus());

        // reset one frame
        $frames = $project->tiles();
        $frame = $frames[array_rand($frames)];
        $frame->reset();

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        $job2 = $frame->getFrame()->getProject();
        $this->assertEquals(Constant::WAITING, $job2->getStatus());
        $this->assertEquals(Constant::WAITING, $this->entityManager->getRepository(Project::class)->find($job->getId())->getStatus());
    }

    public function testUpdateJobAnimationSplitSamplesStatusOnResetFrame(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

//        $project = $this->unittest_create_scene($user);
//        $this->assertTrue(is_object($project));
//
        $nb_frame = 5;
        $nb_tile = $nb_frame * $config['project']['singleframe']['nb_split'];
//        $class_name = '\App\Entity\Job'.Job::$type_animation_split_samples;
//        $job = new $class_name();
//        $job->setPath('path_fake');
//        $job->setPictureExtension('jpg');
//        $this->entityManager->persist($job);
//        $filename = $config['tmp_dir'].'/file_'.$this->unittest_rand().'.zip';
//        copy($config['storage']['path'].'projects'.'/'.$this->CHUNK_PROJECT_1, $filename);
//        $this->assertEquals($nb_tile, $project->addproject($job, 1, $nb_frame, 1, 1920, 1080,   0, $filename));

        // add project
        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();
        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName('name_'.$this->unittestRand());
        $project->setOwner($user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setPath('path_fake');
        $project->setPictureExtension('jpg');
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdInternal::ID));

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $this->entityManager->getRepository(Project::class)->createChunks($project, $config['storage']['path'].'projects'.'/'.$this->getChunkProject1());

        $this->assertEquals($nb_tile, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frame, 1, 1920, 1080, $config['project']['singleframe']['nb_tile'] * $config['project']['singleframe']['nb_tile'], FrameLayer::class, TileReal::class));


        $this->assertEquals(Constant::WAITING, $project->getStatus());

        // render a frame
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        for ($i = 0; $i < $nb_tile; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $this->assertEquals(Constant::FINISHED, $project->getStatus());
        $job = $this->entityManager->getRepository(Project::class)->find($project->getId()); // reload the job from cache
        $this->assertEquals(Constant::FINISHED, $job->getStatus());

        // reset one frame
        $frames = $project->tiles();
        $frame = $frames[array_rand($frames)];
        $frame->reset();

        $this->assertEquals(Constant::WAITING, $project->getStatus());

        $job2 = $frame->getFrame()->getProject();
        $this->assertEquals(Constant::WAITING, $job2->getStatus());
        $this->assertEquals(Constant::WAITING, $this->entityManager->getRepository(Project::class)->find($job->getId())->getStatus());
    }
}
