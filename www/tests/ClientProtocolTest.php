<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class ClientProtocolTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
        parent::tearDown();
    }

    /**
     * The authentication is not case-sensitive, the user returned by ClientProtocol::userAuth should be the actual user not the one given.
     */
    public function testBugfixProtocolAuthWithCaseNonSensitiveLogin1(): void {
        $localUser = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        list($error_state, $remoteUser, $publickey_id) = $handler->userAuth(array('login' => strtoupper($localUser->getId()), 'password' => $localUser->getId()));
        $this->assertEquals(ClientProtocol::OK, $error_state);
        $this->assertEquals($localUser, $remoteUser);
    }
}
