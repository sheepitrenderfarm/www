<?php

namespace App\Test\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AvatarControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    public function testLoggedSmall(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_avatar_getfile', ['user' => 'renderer', 'size' => 'big']));
        $this->assertResponseIsSuccessful();
    }

    public function testLoggedBig(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_avatar_getfile', ['user' => 'renderer', 'size' => 'big']));
        $this->assertResponseIsSuccessful();
    }

    public function testAnonymousSmall(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_donation_main', ['user' => 'renderer', 'size' => 'small']));
        $this->assertResponseIsSuccessful();
    }

    public function testAnonymousBig(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_donation_main', ['user' => 'renderer', 'size' => 'big']));
        $this->assertResponseIsSuccessful();
    }
}