<?php

namespace App\Test\Controller;

use App\Entity\Message;
use App\Repository\UserRepository;
use App\Test\TestTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase {
    use TestTrait;

    protected function setUp(): void {
        $this->init();
        parent::setUp();
        self::ensureKernelShutdown();
    }

    public function testMainRouteAnonymous(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_admin_main'));
        $this->assertResponseRedirects($router->generate('app_user_signin', ['redirect' => urlencode(urlencode($router->generate('app_admin_main')))]));
    }

    public function testLoggedUsersRegistration(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        // add some data
        for ($i = 0; $i < 100; $i++) {
            $this->unittestCreateUser(false, time() - rand(0, 700 * 3600 * 24));
        }

        $client->request('GET', $router->generate('app_admin_users_registration'));
        $this->assertResponseIsSuccessful();
    }

    /**
     * @return array<int, array{0: string, 1: int}>
     */
    public function providerLogged(): array {
        return array(
            array('app_admin_frames', 200),
            array('app_admin_projects', 200),
            array('app_admin_config', 200),
            array('app_admin_event_prediction', 200),
            array('app_admin_hwid', 200),
            array('app_admin_log', 200),
            array('app_admin_gifts', 200),
            array('app_admin_persistentcache', 200),
            array('app_admin_errors', 200),
            array('app_admin_host', 200),
            array('app_admin_tasks', 200),
            array('app_admin_awards', 200),
            array('app_admin_tasks_shepherd', 200),
            array('app_admin_misc_chart', 200),
            array('app_admin_efficiency', 200),
            array('app_admin_mirrors', 200),
            array('app_admin_shepherds_health', 200),
            array('app_admin_responsetime', 200),
            array('app_admin_main', 200),
        );
    }

    /**
     * @dataProvider providerLogged
     */
    public function testLogged(string $route, int $http_code): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate($route));
        $this->assertResponseIsSuccessful();
    }

    public function testLoggedUsersMessages(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);
        $entityManager = static::getContainer()->get(EntityManagerInterface::class);

        $other_user = 'renderer';

        $message1 = new Message();
        $message1->setSender('admin');
        $message1->setReceiver($other_user);
        $message1->setContent(Message::MESSAGE_RENDERING_FORBID);
        $message1->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message1);
        $entityManager->flush($message1);

        $message2 = new Message();
        $message2->setSender('admin');
        $message2->setReceiver($other_user);
        $message2->setContent(Message::MESSAGE_RENDERING_ALLOW);
        $message2->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message2);
        $entityManager->flush($message2);

        $message3 = new Message();
        $message3->setSender('admin');
        $message3->setReceiver($other_user);
        $message3->setContent(Message::MESSAGE_RENDERING_ALLOW);
        $message3->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message3);
        $entityManager->flush($message3);

        $message4 = new Message();
        $message4->setSender('admin');
        $message4->setReceiver($other_user);
        $message4->setContent(Message::MESSAGE_RENDERING_FORBID);
        $message4->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message4);
        $entityManager->flush($message4);


        $message5 = new Message();
        $message5->setSender('admin');
        $message5->setReceiver($other_user);
        $message5->setContent('Hello there!');
        $message5->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message5);
        $entityManager->flush($message5);

        $message6 = new Message();
        $message6->setSender($other_user);
        $message6->setReceiver('admin');
        $message6->setContent('General Kenobi!');
        $message6->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message6);
        $entityManager->flush($message6);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_admin_users_messages'));
        $this->assertResponseIsSuccessful();
    }
}