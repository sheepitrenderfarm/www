<?php

namespace Controller;

use App\Entity\GiftAnonymous;
use App\Entity\User;
use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GiftAnonymousControllerTest extends WebTestCase {
    use TestTrait;

    private const VALUE = 1000;

    protected KernelBrowser $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->init();
    }

    public function testClaim(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();

        $this->client->loginUser($testUser);
        $this->assertEquals(0, $testUser->getPoints());

        $gift = $this->createGift();

        // show
        $this->client->request('GET', $router->generate('app_giftanonymous_show', ['gift' => $gift->getId()]));
        $this->assertResponseIsSuccessful();

        // claim
        $this->client->request('GET', $router->generate('app_giftanonymous_claim', ['gift' => $gift->getId()]));
        $this->assertResponseIsSuccessful();

        // check balance
        $testUser1 = $this->entityManager->getRepository(User::class)->find($testUser->getId()); //$this->entityManager->refresh($testUser);
        $this->assertEquals(self::VALUE, $testUser1->getPoints());
    }

    public function testAdd(): void {
        $router = static::getContainer()->get('router');

        // only an admin can create gift
        $this->client->loginUser($this->entityManager->getRepository(User::class)->find('admin'));

        // add
        $comment = time().'_'.uniqid('', false); // unique name
        $this->client->request('POST', $router->generate('app_giftanonymous_add'), ['gift_anonymous_comment' => $comment, 'gift_anonymous_amount' => self::VALUE]);
        //$this->assertResponseIsSuccessful();
        $this->assertResponseRedirects($router->generate('app_admin_gifts'));

        $this->assertEquals(1, $this->entityManager->getRepository(GiftAnonymous::class)->count(['comment' => $comment]));
    }

    private function createGift(): GiftAnonymous {
        $gift = new GiftAnonymous();
        $gift->setComment(time().'_'.uniqid('', false));
        $gift->setValue(self::VALUE);
        $gift->setCreation(time());
        $gift->setType(GiftAnonymous::TYPE_OTHER); // must NOT be patreon or paypal
        $this->entityManager->getRepository(GiftAnonymous::class)->generateAccess($gift);
        $this->entityManager->persist($gift);
        $this->entityManager->flush();

        return $gift;
    }
}