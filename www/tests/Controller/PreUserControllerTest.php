<?php

namespace App\Test\Controller;

use App\Entity\PreUser;
use App\Entity\User;
use App\Test\TestTrait;
use App\Utils\Misc;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PreUserControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;
    protected UrlGeneratorInterface $router;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->router = static::getContainer()->get('router');
        $this->init();
    }

    public function testCheckEmail(): void {
        // OK test
        $this->client->request('GET', $this->router->generate('app_preuser_check_email', ['email' => 'hello@localhost.com']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('OK', $this->client->getResponse()->getContent());

        // NOK test
        $this->client->request('GET', $this->router->generate('app_preuser_check_email', ['email' => 'hellolocalhost.com']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());
    }

    public function testCheckUsername(): void {
        // number
        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => 123456756]));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());

        // short login
        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => 'a']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());

        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => 'ab']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());

        // existing user
        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => 'admin']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());

        // existing preuser
        $testUser = $this->createPreUser();
        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => $testUser->getId()]));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('NOK', $this->client->getResponse()->getContent());

        // ok test
        $this->client->request('GET', $this->router->generate('app_preuser_check_username', ['username' => 'alpha43']));
        $this->assertResponseIsSuccessful();
        $this->assertEquals('OK', $this->client->getResponse()->getContent());
    }

    public function testSendEmail(): void {

        $testUser = $this->createPreUser();

        $this->client->loginUser($this->entityManager->getRepository(User::class)->find('admin'));
        $this->client->request('GET', $this->router->generate('app_preuser_send_email', ['user' => $testUser->getId()]));
        $this->assertResponseIsSuccessful('OK');
    }

    private function createPreUser(): PreUser {
        $key = Misc::code(32);

        $preuser = new PreUser();
        $preuser->setId(time().'_'.Misc::code(16));
        $preuser->setPassword(User::generateHashedPassword($preuser->getId(), Misc::code(32)));
        $preuser->setRegistrationTime(time());
        $preuser->setEmail($preuser->getId().'@localhost.com');
        $preuser->setKey($key);
        $preuser->setAffiliate('');

        $this->entityManager->persist($preuser);
        $this->entityManager->flush($preuser);

        return $preuser;
    }

}