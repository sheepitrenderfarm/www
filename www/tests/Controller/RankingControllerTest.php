<?php

namespace App\Test\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RankingControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    public function testRouteUserLogged(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_ranking_user'));
        $this->assertResponseIsSuccessful();
    }

    public function testRouteUserAnonymous(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_ranking_user'));
        $this->assertResponseRedirects($router->generate('app_user_signin', ['redirect' => urlencode(urlencode($router->generate('app_ranking_user')))]));
    }

    public function testRouteMachineLogged(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_ranking_machine'));
        $this->assertResponseIsSuccessful();
    }

    public function testRouteMachineAnonymous(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_ranking_machine'));
        $this->assertResponseRedirects($router->generate('app_user_signin', ['redirect' => urlencode(urlencode($router->generate('app_ranking_machine')))]));
    }
}