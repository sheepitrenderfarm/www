<?php

namespace App\Test\Controller;

use App\Entity\Project;
use App\Repository\UserRepository;
use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HomeControllerTest extends WebTestCase {

    use TestTrait;

    protected KernelBrowser $client;
    protected UrlGeneratorInterface $router;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->router = static::getContainer()->get('router');
        $this->init();
    }

    public function testIndexRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_home_index'));
        $this->assertResponseRedirects($this->router->generate('app_home_main')); // redirect to /home
    }

    public function testIndexRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_index'));
        $this->assertResponseRedirects($this->router->generate('app_home_main')); // redirect to /home
    }

    public function testMainRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_home_main'));
        $this->assertResponseIsSuccessful();
    }

    public function testMainRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_main'));
        $this->assertResponseIsSuccessful();
    }

    public function testMachinesRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        // fill up the page
        for ($i = 0; $i < 5; $i++) {
            $this->unittestCreateSession($this->unittestCreateUser(), $this->unittestCreateCpu(), null);
            $this->unittestCreateSession($this->unittestCreateUser(), $this->unittestCreateCpu(), $this->unittestCreateGpu());
        }

        $this->client->request('GET', $this->router->generate('app_home_machines'));
        $this->assertResponseIsSuccessful();
    }

    public function testMachinesRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_machines'));
        $this->assertResponseRedirects(); // redirect to login page
    }

    public function testBinariesRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_home_binaries'));
        $this->assertResponseIsSuccessful();
    }

    public function testBinariesRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_binaries'));
        $this->assertResponseIsSuccessful();
    }

    public function testFramesRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_home_frames'));
        $this->assertResponseIsSuccessful();
    }

    public function testFramesRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_frames'));
        $this->assertResponseRedirects(); // redirect to login page
    }

    public function testProjectsRouteLogged(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        // fill up the page
        for ($i = 0; $i < 5; $i++) {
            $this->createProject();
        }

        $this->client->request('GET', $this->router->generate('app_home_projects'));
        $this->assertResponseIsSuccessful();
    }

    public function testProjectsRouteAnonymous(): void {
        $this->client->request('GET', $this->router->generate('app_home_projects'));
        $this->assertResponseRedirects(); // redirect to login page
    }

    private function createProject(): Project {
        return $this->unittestCreateProject($this->unittestCreateUser(), 10);
    }
}