<?php

namespace App\Test\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NewsControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    public function testLoggedAllNews(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_news_all'));
        $this->assertResponseIsSuccessful();
    }

    public function testAnonymousAllNews(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_news_all'));
        $this->assertResponseIsSuccessful();
    }

    public function testLoggedSingleNews(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_news_single', ['news' => 1646856678]));
        $this->assertResponseIsSuccessful();
    }

    public function testAnonymousSingleNews(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_news_single', ['news' => 1646856678]));
        $this->assertResponseIsSuccessful();
    }
}