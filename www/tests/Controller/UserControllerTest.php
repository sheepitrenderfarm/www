<?php

namespace App\Test\Controller;

use App\Entity\Message;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Test\TestTrait;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;
    protected UrlGeneratorInterface $router;
    protected UserRepository $userRepository;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->router = static::getContainer()->get('router');
        $this->userRepository = static::getContainer()->get(UserRepository::class);
        $this->init();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAccountMain(): void {
        $login = 'admin';
        $testUser = $this->userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_user_profile', ['user' => $testUser->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountEdit(): void {
        $login = 'admin';
        $testUser = $this->userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_user_edit', ['user' => $testUser->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountLostPassword(): void {
        $this->client->request('GET', $this->router->generate('app_user_lostpassword'));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountSignIn(): void {
        $this->client->request('GET', $this->router->generate('app_user_signin'));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountRegister(): void {
        $this->client->request('GET', $this->router->generate('app_user_print_register'));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountDoRegister(): void {
        $username = time().'_'.Misc::code(16);
        $password = Misc::code(8);
        $this->client->request('POST', $this->router->generate('app_user_register'),
            array(
                'login' => $username,
                'password' => $password,
                'password_confirm' => $password,
                'email' => $username.'@localhost.com',
            )
        );
        $this->assertResponseIsSuccessful();
        $this->assertEquals('OK', $this->client->getResponse()->getContent());
    }

    public function testAccountMessage(): void {
        $other_user = 'renderer';

        $entityManager = static::getContainer()->get(EntityManagerInterface::class);

        $message1 = new Message();
        $message1->setSender('admin');
        $message1->setReceiver($other_user);
        $message1->setContent(Message::MESSAGE_RENDERING_FORBID);
        $message1->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message1);
        $entityManager->flush($message1);

        $message2 = new Message();
        $message2->setSender('admin');
        $message2->setReceiver($other_user);
        $message2->setContent(Message::MESSAGE_RENDERING_ALLOW);
        $message2->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message2);
        $entityManager->flush($message2);

        $message3 = new Message();
        $message3->setSender('admin');
        $message3->setReceiver($other_user);
        $message3->setContent(Message::MESSAGE_RENDERING_ALLOW);
        $message3->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message3);
        $entityManager->flush($message3);

        $message4 = new Message();
        $message4->setSender('admin');
        $message4->setReceiver($other_user);
        $message4->setContent(Message::MESSAGE_RENDERING_FORBID);
        $message4->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message4);
        $entityManager->flush($message4);


        $message5 = new Message();
        $message5->setSender('admin');
        $message5->setReceiver($other_user);
        $message5->setContent('Hello there!');
        $message5->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message5);
        $entityManager->flush($message5);

        $message6 = new Message();
        $message6->setSender($other_user);
        $message6->setReceiver('admin');
        $message6->setContent('General Kenobi!');
        $message6->setTime(time() - random_int(0, 10000));
        $entityManager->persist($message6);
        $entityManager->flush($message6);

        $login = 'admin';
        $testUser = $this->userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_user_messages', ['user' => $other_user]));
        $this->assertResponseIsSuccessful();
    }

    public function testAccountEditSchedulerRenderMyProjectFirst(): void {
        $testUser = $this->unittestCreateUser();
        $this->assertTrue($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST));

        $this->client->loginUser($testUser);

        $this->client->request('POST', $this->router->generate('app_user_update_scheduler', ['key' => 'render_my_project_first', 'value' => 0]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertFalse($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST));

        $this->client->request('POST', $this->router->generate('app_user_update_scheduler', ['key' => 'render_my_project_first', 'value' => 1]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertTrue($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST));
    }

    public function testAccountEditSchedulerRenderMyTeamProjectFirst(): void {
        $testUser = $this->unittestCreateUser();
        $this->assertFalse($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST));

        $this->client->loginUser($testUser);

        $this->client->request('POST', $this->router->generate('app_user_update_scheduler', ['key' => 'render_my_team_projects_first', 'value' => 1]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertTrue($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST));

        $this->client->request('POST', $this->router->generate('app_user_update_scheduler', ['key' => 'render_my_team_projects_first', 'value' => 0]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertFalse($testUser->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST));
    }

    public function testAccountEditNotificationRequestJoinTeamSchedulerRenderMyProjectFirst(): void {
        $testUser = $this->unittestCreateUser();
        $this->assertFalse($testUser->noficationIsEnabled(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM));

        $this->client->loginUser($testUser);

        $this->client->request('POST', $this->router->generate('app_user_update_notification', ['key' => User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM, 'value' => 1]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertTrue($testUser->noficationIsEnabled(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM));

        $this->client->request('POST', $this->router->generate('app_user_update_notification', ['key' => User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM, 'value' => 0]));
        $this->assertResponseIsSuccessful();
        $testUser = $this->userRepository->find($testUser->getId()); // refresh

        $this->assertFalse($testUser->noficationIsEnabled(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM));
    }

}