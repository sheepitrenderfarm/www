<?php

namespace App\Test\Controller;

use App\Entity\User;
use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CheeseControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->init();
    }

    public function testMainRouteLogged(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();

        $this->client->loginUser($testUser);

        $this->client->request('GET', $router->generate('app_cheesegiveaway_main'));
        $this->assertResponseIsSuccessful();
    }

    public function testMainRouteAnonymous(): void {
        $router = static::getContainer()->get('router');

        $this->client->request('GET', $router->generate('app_cheesegiveaway_main'));
        $this->assertResponseRedirects($router->generate('app_user_signin', ['redirect' => urlencode(urlencode($router->generate('app_cheesegiveaway_main')))]));
        $this->assertResponseRedirects();
    }

    public function testTakeLogged(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();
        $this->entityManager->persist($testUser);
        $this->entityManager->flush();

        $this->client->loginUser($testUser);

        // does not have the award
        $this->assertCount(0, $testUser->awards());

        // go on the info page, still does not earn it
        $this->client->request('GET', $router->generate('app_cheesegiveaway_main'));
        $this->assertResponseIsSuccessful();
        $this->assertCount(0, $testUser->awards());

        // earn
        $this->client->request('GET', $router->generate('app_cheesegiveaway_cheese'));
        $this->assertResponseIsSuccessful();

        $testUser = $this->entityManager->getRepository(User::class)->find($testUser->getId()); // manual reload  ->em->reshed() doesn't work

        $this->assertCount(1, $testUser->awards());
    }
}
