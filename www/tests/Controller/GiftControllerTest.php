<?php

namespace Controller;

use App\Entity\Gift;
use App\Entity\User;
use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GiftControllerTest extends WebTestCase {
    use TestTrait;

    private const VALUE = 1000;

    protected KernelBrowser $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->init();
    }

    public function testClaim(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();

        $this->client->loginUser($testUser);
        $this->assertEquals(0, $testUser->getPoints());

        $gift = $this->createGift($testUser);

        // show
        $this->client->request('GET', $router->generate('app_gift_show', ['gift' => $gift->getId(), 'key' => $gift->getKey()]));
        $this->assertResponseIsSuccessful();

        // claim
        $this->client->request('GET', $router->generate('app_gift_claim', ['gift' => $gift->getId(), 'key' => $gift->getKey()]));
        $this->assertResponseIsSuccessful();

        // check balance
        $testUser1 = $this->entityManager->getRepository(User::class)->find($testUser->getId()); //$this->entityManager->refresh($testUser);
        $this->assertEquals(self::VALUE, $testUser1->getPoints());
    }

    public function testAdd(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();
        $this->assertEquals(0, $testUser->gifts()->count());

        // only an admin can create gift
        $this->client->loginUser($this->entityManager->getRepository(User::class)->find('admin'));

        // add
        $this->client->request('GET', $router->generate('app_gift_add', ['owner' => $testUser->getId(), 'amount' => self::VALUE, 'send_email' => 1]));
        $this->assertResponseIsSuccessful();

        // check if created
        $this->entityManager->refresh($testUser);
        $this->assertEquals(1, $testUser->gifts()->count());
        $this->assertEquals(self::VALUE, $testUser->gifts()->first()->getValue());
    }

    private function createGift(User $owner): Gift {
        $gift = new Gift();
        $gift->setValue(self::VALUE);
        $gift->setOwner($owner);
        $gift = $this->entityManager->getRepository(Gift::class)->generateAccess($gift);
        $this->entityManager->persist($gift);
        $this->entityManager->flush();

        return $gift;
    }
}