<?php

namespace App\Test\Controller;

use DOMDocument;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    private function authOkData(): array {
        return [
            "login" => "admin",
            "password" => "sheepit",
            "cpu_family" => "6",
            "cpu_model" => "158",
            "cpu_model_name" => "Intel(R) Core(TM) i5-9600KF CPU @ 3.70GHz",
            "cpu_cores" => "3",
            "os" => "linux",
            "os_version" => "linux",
            "ram" => "41156608",
            "bits" => "64bit",
            "version" => "6.21175.0",
            "hostname" => "kepler",
            "ui" => "GuiSwing",
            "extras" => "",
            "headless" => "0",
        ];
    }

    public function testConfigNoParam(): void {
        $client = static::createClient();

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'));
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testConfigAuthOK(): void {
        $client = static::createClient();


        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());

        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testConfigAuthFailed(): void {
        $client = static::createClient();

        $data = [];
        $data["login"] = "admin";
        $data["password"] = "lollollollollollollol";
        $data["cpu_family"] = "6";
        $data["cpu_model"] = "158";
        $data["cpu_model_name"] = "Intel(R) Core(TM) i5-9600KF CPU @ 3.70GHz";
        $data["cpu_cores"] = "3";
        $data["os"] = "linux";
        $data["os_version"] = "linux";
        $data["ram"] = "41156608";
        $data["bits"] = "64bit";
        $data["version"] = "6.21175.0";
        $data["hostname"] = "kepler";
        $data["ui"] = "GuiSwing";
        $data["extras"] = "";
        $data["headless"] = "0";

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $data);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testKeepMeAliveNoParameter(): void {
        $client = static::createClient();
        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));

        $client->request('GET', static::getContainer()->get('router')->generate('app_client_keepmealive'), []);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testRequestJobBadParameter(): void {
        $client = static::createClient();
        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_request_job'), []);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testRequestJobCPU(): void {
        $client = static::createClient();
        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));


        $data = [
            "computemethod" => 1,
            "network_dl" => 4567755,
            "network_up" => 23455,
            "cpu_cores" => 3,
            "ram_max" => 21156608,
            "rendertime_max" => 2,
        ];

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_request_job'), $data);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testRequestJobGPU(): void {
        $client = static::createClient();
        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));


        $data = [
            "computemethod" => 1,
            "network_dl" => 4567755,
            "network_up" => 23455,
            "cpu_cores" => 3,
            "ram_max" => 21156608,
            "rendertime_max" => 2,

            "gpu_model" => "some nvidia gpu",
            "gpu_ram" => 7654356,
            "gpu_type" => "OPTIX",
        ];

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_request_job'), $data);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    public function testRequestJobBadGPU(): void {
        $client = static::createClient();
        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $this->authOkData());
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));


        $data = [
            "computemethod" => 1,
            "network_dl" => 4567755,
            "network_up" => 23455,
            "cpu_cores" => 3,
            "ram_max" => 21156608,
            "rendertime_max" => 2,

            "gpu_model" => "some intel gpu",
            "gpu_ram" => 7654356,
            "gpu_type" => "INTEL",
        ];

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_request_job'), $data);
        $this->assertResponseIsSuccessful();
        $this->assertTrue($this->isXMLValid($client->getResponse()->getContent()));
    }

    private function isXMLValid(string $xml): bool {
        $dom = new DomDocument('1.0', 'utf-8');
        return @$dom->loadXML($xml) != null; // only check if the xml has valid syntax but not contents checking yet.
    }
}