<?php

namespace App\Test\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProjectControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;
    protected UrlGeneratorInterface $router;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->router = static::getContainer()->get('router');
        $this->init();
    }

    public function testRegeneratestats(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_project_regeneratestats', ['project' => 1]));
        $this->assertResponseRedirects($this->router->generate("app_project_manage", ['project' => 1]));
    }

    public function testRegeneratetoken(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('GET', $this->router->generate('app_project_regeneratetoken', ['project' => 1]));
        $this->assertResponseRedirects($this->router->generate("app_project_manage", ['project' => 1]));
    }

    public function testEstimator(): void {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $this->client->loginUser($testUser);

        $this->client->request('POST', $this->router->generate('app_project_estimator'), array('time' => 10, 'count' => 20, 'device' => 'gpu_1'));
        $this->assertResponseIsSuccessful();
    }

    public function testManageProjectOwner(): void {

        $testUser = $this->unittestCreateUser();
        $project = $this->createProject($testUser);


        $this->client->loginUser($testUser);
        $this->client->request('GET', $this->router->generate('app_project_manage', ['project' => $project->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testManageProjectAdmin(): void {

        $testUser = $this->unittestCreateUser();
        $project = $this->createProject($testUser);

        $this->client->loginUser($this->entityManager->getRepository(User::class)->find('admin'));
        $this->client->request('GET', $this->router->generate('app_project_manage', ['project' => $project->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testSeeFramesProjectOwner(): void {

        $testUser = $this->unittestCreateUser();
        $project = $this->createProject($testUser);


        $this->client->loginUser($testUser);
        $this->client->request('GET', $this->router->generate('app_project_frames', ['project' => $project->getId(), 'interval' => 1]));
        $this->assertResponseIsSuccessful();
    }

    public function testSeeFramesProjectAdmin(): void {

        $testUser = $this->unittestCreateUser();
        $project = $this->createProject($testUser);

        $this->client->loginUser($this->entityManager->getRepository(User::class)->find('admin'));
        $this->client->request('GET', $this->router->generate('app_project_frames', ['project' => $project->getId(), 'interval' => 1]));
        $this->assertResponseIsSuccessful();
    }

    private function createProject(User $owner): Project {
        $nb_frame = 10;
        return $this->unittestCreateProject($owner, $nb_frame);
    }
}