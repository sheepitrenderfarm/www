<?php

namespace App\Test\Controller;

use App\Entity\Team;
use App\Repository\TeamRepository;
use App\Test\TestTrait;
use App\Utils\Misc;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TeamControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->init();
    }

    public function testMainRouteLogged(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();

        $this->client->loginUser($testUser);

        $this->client->request('GET', $router->generate('app_team_all'));
        $this->assertResponseIsSuccessful();
    }

    /**
     * Teams page can be browsed in anonymous user
     */
    public function testMainRouteAnonymous(): void {
        $router = static::getContainer()->get('router');

        $this->client->request('GET', $router->generate('app_team_all'));
        $this->assertResponseIsSuccessful();
    }

    public function testSingleTeamMyTeam(): void {
        $router = static::getContainer()->get('router');
        $teamRepository = static::getContainer()->get(TeamRepository::class);

        $testUser = $this->unittestCreateUser();

        $team = new Team();
        $team->setName(Misc::code(30));
        $team->setPoints(0);
        $team->setOwner($testUser);
        $team->setDescription(Misc::code(100));
        $team->setCreationTime(time());
        $teamRepository->add($team);

        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $this->unittestCreateUser()));

        $this->client->loginUser($testUser);

        $this->client->request('GET', $router->generate('app_team_single', ['team' => $team->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testSingleTeamOtherUser(): void {
        $router = static::getContainer()->get('router');
        $teamRepository = static::getContainer()->get(TeamRepository::class);

        $ownerUser = $this->unittestCreateUser();
        $testUser = $this->unittestCreateUser();

        $team = new Team();
        $team->setName(Misc::code(30));
        $team->setPoints(0);
        $team->setOwner($ownerUser);
        $team->setDescription(Misc::code(100));
        $team->setCreationTime(time());
        $teamRepository->add($team);

        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $this->unittestCreateUser()));

        $this->client->loginUser($testUser);

        $this->client->request('GET', $router->generate('app_team_single', ['team' => $team->getId()]));
        $this->assertResponseIsSuccessful();
    }
}
