<?php

namespace App\Test\Controller;

use App\Entity\Session;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SessionControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    private function createSession(KernelBrowser $client): Session {
        $data = [];
        $data["login"] = "admin";
        $data["password"] = "sheepit";
        $data["cpu_family"] = "6";
        $data["cpu_model"] = "158";
        $data["cpu_model_name"] = "Intel(R) Core(TM) i5-9600KF CPU @ 3.70GHz";
        $data["cpu_cores"] = "3";
        $data["os"] = "linux";
        $data["os_version"] = "linux";
        $data["ram"] = "41156608";
        $data["bits"] = "64bit";
        $data["version"] = "6.21175.0";
        $data["hostname"] = "kepler";
        $data["ui"] = "GuiSwing";
        $data["extras"] = "";
        $data["headless"] = "0";

        $client->request('POST', static::getContainer()->get('router')->generate('app_client_config'), $data);
        $this->assertResponseIsSuccessful();

        /** @var SessionRepository $sessionRepository */
        $sessionRepository = static::getContainer()->get(SessionRepository::class);

        return $sessionRepository->findOneBy(['user' => 'admin']);
    }

    public function testLogged(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $session = $this->createSession($client);

        $client->request('GET', $router->generate('app_session_single', ['sessionid' => $session->getId()]));
        $this->assertResponseIsSuccessful();
    }

    public function testAnonymous(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $session = $this->createSession($client);

        $client->request('GET', $router->generate('app_session_single', ['sessionid' => $session->getId()]));

        $this->assertResponseRedirects($router->generate('app_user_signin', ['redirect' => urlencode(urlencode($router->generate("app_session_single", ['sessionid' => $session->getId()])))]));
        $this->assertResponseRedirects();
    }
}