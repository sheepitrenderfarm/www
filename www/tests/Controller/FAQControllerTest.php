<?php

namespace App\Test\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FAQControllerTest extends WebTestCase {
    protected function setUp(): void {
        self::ensureKernelShutdown();
    }

    public function testMainRouteLogged(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');
        $userRepository = static::getContainer()->get(UserRepository::class);

        $login = 'admin';
        $testUser = $userRepository->find($login);

        $client->loginUser($testUser);

        $client->request('GET', $router->generate('app_faq_main'));
        $this->assertResponseIsSuccessful();
    }

    public function testMainRouteAnonymous(): void {
        $client = static::createClient();
        $router = static::getContainer()->get('router');

        $client->request('GET', $router->generate('app_faq_main'));
        $this->assertResponseIsSuccessful();
    }
}