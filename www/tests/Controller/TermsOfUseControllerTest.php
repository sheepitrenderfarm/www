<?php

namespace App\Tests\Controller;

use App\Test\TestTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TermsOfUseControllerTest extends WebTestCase {
    use TestTrait;

    protected KernelBrowser $client;

    protected function setUp(): void {
        parent::setUp();
        $this->client = static::createClient();
        $this->init();
    }

    public function testMainRouteLogged(): void {
        $router = static::getContainer()->get('router');

        $testUser = $this->unittestCreateUser();

        $this->client->loginUser($testUser);

        $this->client->request('GET', $router->generate('app_termsofuse_main'));
        $this->assertResponseIsSuccessful();
    }

    /**
     * Terms of use page can be browsed in anonymous user
     */
    public function testMainRouteAnonymous(): void {
        $router = static::getContainer()->get('router');

        $this->client->request('GET', $router->generate('app_termsofuse_main'));
        $this->assertResponseIsSuccessful();
    }
}
