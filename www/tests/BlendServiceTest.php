<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Service\BlendService;

class BlendServiceTest extends BaseTestCase {

    private BlendService $blendService;

    protected function setUp(): void {
        parent::setUp();

        $this->blendService = new BlendService(static::getContainer()->get('logger'), $this->configService, $this->entityManager);
    }

//    public function testListBLendFileNotExists() {
//        $path = __DIR__.'/data/ff000033333333333.png';
//
//        $result = $this->blendService->get_blender_infos($path);
//
//        $this->assertSame(-3, $result);
//    }
//
//    public function testListBLendNotBlend() {
//        $path = __DIR__.'/data/ff0000.png';
//
//        $result = $this->blendService->get_blender_infos($path);
//
//        $this->assertIsArray($result);
//        $this->assertArrayHasKey('error', $result);
//    }
//
//    public function testListBLendWithBlend() {
//        $path = __DIR__.'/data/330-cycles.blend';
//
//        $result = $this->blendService->get_blender_infos($path);
//        $this->assertIsArray($result);
//
//        $this->assertArrayHasKey('version', $result);
//        $this->assertArrayNotHasKey('error', $result);
//        $this->assertEquals('blender303', $result['version']);
//    }

    /**
     * If a multi layer exr blend has more than 6 layers, an error must be display
     */
    public function testEXRMultiLayer(): void {
        $path = __DIR__.'/data/330-exr-mutli.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);

        $this->assertArrayHasKey('error', $result);
        $this->assertTrue(strlen($result['error']) > 0);
    }

    /**
     * If a single layer exr blend has more than 6 layers, NO error must be display because it can be ignored
     */
    public function testEXRSingleLayer(): void {
        $path = __DIR__.'/data/330-exr-single.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);

        $this->assertArrayNOtHasKey('error', $result);
    }

    public function testBlender40(): void {
        $path = __DIR__.'/data/400-cycles.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('version', $result);

        $this->assertEquals('blender400', $result['version']);
    }

    public function testEXRMaxDimensionPortrait(): void {
        $path = __DIR__.'/data/exr_2160_3840.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey('error', $result); // no error, it's under the permitted dimension
    }

    public function testEXRMaxDimensionLandscape(): void {
        $path = __DIR__.'/data/exr_3840_2160.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey('error', $result); // no error, it's under the permitted dimension
    }

    public function testEXRMaxDimensionOversize(): void {
        $path = __DIR__.'/data/exr_2160_4000.blend';

        $result = $this->blendService->getBlenderInfos($path);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('error', $result); // error, it's over the permitted dimension
    }

}
