<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class UserSponsorTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testgetSponsorUsersCheck(): void {
        $user = $this->unittestCreateUser();

        $user_sponsored = $this->unittestCreateUser();
        $user_sponsored->setSponsorReceive(false);

        $user->addSponsorUser($user_sponsored);

        $this->assertCount(1, $user->getSponsorUsers(false));
        $this->assertCount(0, $user->getSponsorUsers(true));
    }

    public function testRenderFrame(): void {
        $user = $this->unittestCreateUser();
        $user->setSponsorGive(true);

        $user_sponsored = $this->unittestCreateUser();
        $user_sponsored->setSponsorReceive(true);

        $user->addSponsorUser($user_sponsored);

        $this->entityManager->flush();

        // init render
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->unittestCreateProject($user, 10);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->entityManager->refresh($user);
        $this->entityManager->refresh($user_sponsored);

        // when sponsoring a render, the actual renderer will get the points earn but not the 'regular' points
        // a random user from their sponsor user list will get the points.

        $config = $this->configService->getData();
        $points_init_frames = $config['rank']['points']['rendered']['compute_method_frame'] + $config['rank']['points']['rendered']['power_detection_frame'];

        $this->assertEquals($points_init_frames, $user->getPoints()); // Compute methode frame + power
        $this->assertGreaterThan($points_init_frames, $user->getPointsEarn());

        $this->assertEquals(0, $user_sponsored->getPointsEarn());
        $this->assertGreaterThan(0, $user_sponsored->getPoints());
    }
}
