<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Utils\Misc;

class MiscTest extends BaseTestCase {

    public function testHumanTime(): void {
        $this->assertEquals('7h58m', Misc::humanTime(28716));
    }
}
