<?php

namespace App\Test;

use App\BestRenderer;
use App\Entity\Award7daySession;
use App\Entity\RenderDay;

class AwardConsecutiveDaysSessionTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    /**
     * @return array<int, array{0: int, 1: bool}>
     */
    public function provider(): array {
        return array(
            array(4, false),
            array(8, true),
        );
    }

    /**
     * @dataProvider provider
     */
    public function testRenderDays(int $nb_days, bool $expected): void {
        $user = $this->unittestCreateUser();
        $nb_day_past = -1 * $nb_days;

        $o = new BestRenderer();
        $o->setLogin($user->getId());
        $o->setRenderedFrames(0);
        $o->setRenderTime(0);

        $this->main->getPersistentCache()->set('best_renderer', serialize([$o]));

        $awards = $user->awardsWithStatus();
        $this->assertArrayHasKey('Award7daySession', $awards);
        $this->assertFalse($awards['Award7daySession']);

        // add data
        for ($i = $nb_day_past; $i <= 0; $i++) {
            $this->entityManager->getRepository(RenderDay::class)->add($user, time() + $i * 86400);
        }

        $award = new Award7daySession();
        $award->cronDaily();

        $awards = $user->awardsWithStatus();
        $this->assertArrayHasKey('Award7daySession', $awards);
        $this->assertEquals($expected, $awards['Award7daySession']);
    }
}
