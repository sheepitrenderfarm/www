<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Session;
use App\Entity\Tile;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class SessionResetTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
        parent::tearDown();
    }

    public function testRemoveIdleSession(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());

        // modify session and frame to fake old request
        $handler->getSession()->setTimestamp(time() - $config['session']['timeout'] - 100);
        $this->entityManager->flush($handler->getSession());

        $oldId = $handler->getSession()->getId();
        $this->main->removeIdleSession();

        $this->assertNull($this->entityManager->getRepository(Session::class)->find($oldId));

        // check if the frame had been reseted
        $this->entityManager->refresh($frame);
        $this->assertEquals(null, $frame->getSession());
        $this->assertEquals(null, $frame->getUser());
        $this->assertEquals(Constant::WAITING, $frame->getStatus());
        $this->assertNull($frame->getCpu());
    }

    public function testResetIdleFrameRequest(): void {
        $config = $this->configService->getData();

        $user = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($user, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());

        // modify sesion and frame to fake old request
        $frame->setRequestTime(time() - $config['session']['timeoutrender'] - 100);
        $this->entityManager->flush();

        $this->main->removeIdleSession();

        // check if the frame had been reseted
        $frame_reload = $this->entityManager->getRepository(Tile::class)->find($frame->getId());
        $this->assertTrue(is_object($frame_reload));
        $this->assertEquals(null, $frame_reload->getSession());
        $this->assertEquals(null, $frame_reload->getUser());
        $this->assertEquals(Constant::WAITING, $frame_reload->getStatus());
        $this->assertTrue($frame_reload->getCpu() == '' or $frame_reload->getCpu() == '0');
    }
}
