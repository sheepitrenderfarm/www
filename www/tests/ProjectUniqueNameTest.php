<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Blender;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\ShepherdMock;
use App\Entity\User;
use App\Utils\Misc;

class ProjectUniqueNameTest extends BaseTestCase {

    /**
     * @var User
     */
    private $user;

    protected function setUp(): void {
        parent::setUp();
        $this->user = $this->unittestCreateUser();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testUniqueShortName(): void {
        $config = $this->configService->getData();

        $prefix = 'short_name';

        // add first project
        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();
        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName($prefix);
        $project->setPath($prefix.'.blend');
        $project->setOwner($this->user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdMock::ID));
        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $name = $this->entityManager->getRepository(Project::class)->generateUniqueProjectName($prefix, 0, 100);

        $this->assertStringStartsWith($prefix, $name);
        $this->assertNotEquals($prefix, $name);
    }

    public function testUniqueLongName(): void {
        $config = $this->configService->getData();

        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();

        $prefix = 'Chocofur_'.time().'_Interior_Scene_06_eevee_New_WorkedOn_CartoonMouseArm2-compressed-dollycamera3-facemouse';

        // add first project
        $name1 = $this->entityManager->getRepository(Project::class)->generateUniqueProjectName($prefix, 0, 100);

        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName($name1);
        $project->setPath($name1.'.blend');
        $project->setOwner($this->user);
        $project->setPublicThumbnail(true);
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdMock::ID));
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $this->entityManager->persist($project);
        $this->entityManager->flush();


        // add second project, will generate "prefix 0-100"
        $name2 = $this->entityManager->getRepository(Project::class)->generateUniqueProjectName($prefix, 0, 100);
        $this->assertStringStartsWith(substr($prefix, 0, 50), $name2); //  max lenght is 64 char
        $this->assertNotEquals($prefix, $name2);

        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName($name2);
        $project->setPath($name2.'.blend');
        $project->setOwner($this->user);
        $project->setPublicThumbnail(true);
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdMock::ID));
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $this->entityManager->persist($project);
        $this->entityManager->flush();

        // add third project, will generate "prefix 0-100 XXXX"
        $name3 = $this->entityManager->getRepository(Project::class)->generateUniqueProjectName($prefix, 0, 100);
        $this->assertStringStartsWith(substr($prefix, 0, 50), $name3); //  max lenght is 64 char
        $this->assertNotEquals($prefix, $name3);

        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName($name3);
        $project->setPath($name3.'.blend');
        $project->setOwner($this->user);
        $project->setPublicThumbnail(true);
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdMock::ID));
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }
}
