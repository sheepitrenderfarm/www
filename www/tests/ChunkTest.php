<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\Project;
use App\Repository\ProjectRepository;

class ChunkTest extends BaseTestCase {
    private ProjectRepository $repo;

    protected function setUp(): void {
        parent::setUp();
        $this->repo = $this->entityManager->getRepository(Project::class);
    }

    public function testSmallFile(): void {
        $input = 2000000;

        $output = $this->repo->getSizeChunk($input);
        $this->assertEquals((int)($input / 4), $output);
    }

    public function testBigFile(): void {
        $input = 280123123;

        $output = $this->repo->getSizeChunk($input);
        $this->assertEquals(50000000, $output);
    }

    public function test3(): void {
        $input = 161208871;

        $output = $this->repo->getSizeChunk($input);
        $this->assertEquals((int)($input / 4), $output);
    }
}
