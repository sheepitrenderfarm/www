<?php

namespace App\Test;

use App\Constant;
use App\Entity\Project;

class ClusterTest extends BaseTestCase {

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testProjectsWaiting(): void {
        $user = $this->unittestCreateUser();

        $project = $this->unittestCreateOrphanProject($user);
        $project->setCacheRendertimeRef(0); // NO frame rendered
        $this->entityManager->flush();

        for ($i = 0; $i < 10; $i++) {
            $project = $this->unittestCreateOrphanProject($user);
            $project->setCacheRendertimeRef(random_int(1, 10000000));  // project with some frames rendered
            $this->entityManager->flush();
        }

        $this->assertEquals(Constant::CLUSTER_HIGH, $this->entityManager->getRepository(Project::class)->getCluster($project)); // no frame -> HIGH
    }
}