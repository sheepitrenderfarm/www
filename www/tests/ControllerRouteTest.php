<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Auhtor Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace App\Test;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ControllerRouteTest extends WebTestCase {

    private KernelBrowser $client;

    public function setUp(): void {
        self::ensureKernelShutdown();
        $this->client = static::createClient();
    }

    public function provider(): array {
        return [
            ['/media/applet/client-binary-latest.php', 200],
            ['/media/applet/client-latest.php', 200],
            ['/media/applet/client-info.php?get=md5', 200],
            ['/media/applet/client-info.php?get=version', 200],
        ];
    }

    /**
     * @dataProvider provider
     */
    public function testDownload(string $url, int $expected): void {
        $this->client->request('GET', $url);

        $this->assertEquals($expected, $this->client->getResponse()->getStatusCode(), "$url failed"); // only check for PHP notice
    }
}
