<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\ShepherdReal;
use App\Entity\User;

class ShepherdSelectionTest extends BaseTestCase {
    protected function setUp(): void {
        parent::setUp();

        $this->unittestDisableAllShepherds();
        $this->unittestRemoveAllShepherds();
    }

    protected function tearDown(): void {
        $this->unittestEnableAllShepherds();
        parent::tearDown();
    }

    public function testwithDisabledShepherd(): void {
        $paris = (new ShepherdReal())
            ->setId("paris")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('48.8633881 2.3270696')
            ->setLocation('FR')
            ->setTimeout(false)
            ->setAllowNewBlend(true)
            ->setEnable(true);

        $berlin = (new ShepherdReal())
            ->setId("berlin")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('52.5069704 13.2846505')
            ->setLocation('DE')
            ->setTimeout(false)
            ->setAllowNewBlend(true)
            ->setEnable(false);

        $newyork = (new ShepherdReal())
            ->setId("newyork")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('40.7325521 -73.9878345')
            ->setLocation('US')
            ->setTimeout(false)
            ->setAllowNewBlend(true)
            ->setEnable(true);

        $this->entityManager->persist($paris);
        $this->entityManager->persist($berlin);
        $this->entityManager->persist($newyork);
        $this->entityManager->flush();

        $shepherds = $this->main->bestShepherdServersFromLocation('BE');
        $this->assertEquals(2, count($shepherds));
    }

    public function testwithNotAllowNewBlendShepherd(): void {
        $paris = (new ShepherdReal())
            ->setId("paris")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('48.8633881 2.3270696')
            ->setLocation('FR')
            ->setTimeout(false)
            ->setAllowNewBlend(true)
            ->setEnable(true);

        $berlin = (new ShepherdReal())
            ->setId("berlin")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('52.5069704 13.2846505')
            ->setLocation('DE')
            ->setTimeout(false)
            ->setAllowNewBlend(false)
            ->setEnable(true);

        $newyork = (new ShepherdReal())
            ->setId("newyork")
            ->setOwner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setMaintainner($this->entityManager->getRepository(User::class)->find('admin'))
            ->setLocationGeographic('40.7325521 -73.9878345')
            ->setLocation('US')
            ->setTimeout(false)
            ->setAllowNewBlend(true)
            ->setEnable(true);

        $this->entityManager->persist($paris);
        $this->entityManager->persist($berlin);
        $this->entityManager->persist($newyork);
        $this->entityManager->flush();

        $shepherds = $this->main->bestShepherdServersFromLocation('BE');
        $this->assertEquals(2, count($shepherds));
    }
}