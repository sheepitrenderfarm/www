<?php
/**
 * Copyright (C) 2018 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class CronTest extends BaseTestCase {
    // simply call the methods to make sure there is no obvious bug

    public function testMinutely(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:minutely');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function test5Minutes(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:5minutes');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testHourly(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:hourly');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testDailyA(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:daily:a');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testDailyB(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:daily:b');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testDailyC(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:daily:c');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testCleanUp(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:cleanup');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testMontly(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:monthly');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testResetStats(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:resetstats');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

    public function testLogRotate(): void {
        $application = new Application(self::$kernel);

        $command = $application->find('sheepit:cron:logrotate');
        $commandTester = new CommandTester($command);
        $this->assertEquals(Command::SUCCESS, $commandTester->execute([]));
    }

}
