<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Utils\Network;

class NetworkTest extends BaseTestCase {

    public function testIsGoogle(): void {
        $this->assertFalse(Network::isGoogleCloud(''));
        $this->assertFalse(Network::isGoogleCloud('127.0.0.1'));
        $this->assertFalse(Network::isGoogleCloud('51.89.41.122')); // www
        $this->assertFalse(Network::isGoogleCloud('8.8.8.8')); // google DNS
        $this->assertTrue(Network::isGoogleCloud('104.155.2.22')); // collab in Brussels
    }

    public function testLocation(): void {
        $this->assertFalse(Network::getLocationFromIP(''));

        $data = Network::getLocationFromIP('169.197.182.76');
        $this->assertArrayHasKey('country', $data);
        $this->assertEquals('US', $data['country']);
    }
}
