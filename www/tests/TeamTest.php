<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\Tile;
use App\Entity\Team;
use App\Entity\User;
use App\Repository\TeamRepository;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;

class TeamTest extends BaseTestCase {
    private TeamRepository $teamRepository;

    protected function setUp(): void {
        parent::setUp();
        $this->teamRepository = $this->entityManager->getRepository(Team::class);
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAdd(): void {
        $user = $this->unittestCreateUser();

        $teams = $this->teamRepository->findAll();
        $before = count($teams);

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($user);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $teams = $this->teamRepository->findAll();
        $after = count($teams);
        $this->assertEquals($before + 1, $after);
    }

    public function testOwnerIsAMember(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $members = $team->members();
        $this->assertCount(1, $members);
        $this->assertTrue($members->contains($owner));

        $team1 = $owner->getTeam();
        $this->assertTrue(is_object($team1));
        $this->assertTrue($team1->getId() == $team->getId());
    }

    public function testAddMember(): void {
        $owner = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user));

        $members = $team->members();
        $this->assertCount(2, $members);

        $this->assertTrue($members->contains($owner));
        $this->assertTrue($members->contains($user));
    }

    public function testRemoveMember(): void {
        $owner = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $team->setPointsFormerUser(0);
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user));

        $members = $team->members();
        $this->assertCount(2, $members);
        $this->assertTrue($members->contains($owner));
        $this->assertTrue($members->contains($user));

        $this->assertEquals(Team::OK, $this->teamRepository->removeMember($team, $user));

        $members = $team->members();
        $this->assertCount(1, $members);
        $this->assertTrue($members->contains($owner));
    }

    public function testChangeOwnerNotMember(): void {
        $owner = $this->unittestCreateUser();

        $user_not_member = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $team->setPointsFormerUser(0);
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertFalse($this->teamRepository->changeOwner($team, $user_not_member));
    }

    public function testChangeOwner(): void {
        $owner = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $team->setPointsFormerUser(0);
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user));

        $members = $team->members();
        $this->assertCount(2, $members);
        $this->assertTrue($members->contains($owner));
        $this->assertTrue($members->contains($user));

        $this->assertTrue($this->teamRepository->changeOwner($team, $user));

        $members = $team->members();
        $this->assertCount(1, $members);
        $this->assertFalse($members->contains($owner));
        $this->assertEquals($user, $team->getOwner());
    }

    public function testMaxMember(): void {
        $config = $this->configService->getData();

        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        for ($i = 2; $i <= $config['team']['max_member']; $i++) { // start at 2 because the owner is a member
            $user = $this->unittestCreateUser();
            $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user));
        }

        $members = $team->members();
        $this->assertEquals($config['team']['max_member'], $members->count());

        // adding a user should failled
        $user = $this->unittestCreateUser();
        $this->assertEquals(Team::ERROR_TEAM_FULL, $this->teamRepository->addMember($team, $user));
    }

    public function testRemoveTeam(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $members = $team->members();
        $this->assertEquals(1, $members->count());

        $user = $this->unittestCreateUser();
        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user));

        $members = $team->members();
        $this->assertCount(2, $members);

        $team1 = $user->getTeam();
        $this->assertTrue(is_object($team1));
        $this->assertTrue($team1->getId() == $team->getId());

        $teamId = $team->getId();
        $this->assertTrue($this->teamRepository->remove($team));
        $this->assertNull($this->teamRepository->find($teamId));

        $this->entityManager->refresh($user);

        $this->assertNull($user->getTeam());
        $this->assertEquals(Team::NO_JOIN, $user->getTeamJoinTime());

        $this->entityManager->refresh($owner);

        $this->assertNull($owner->getTeam());
        $this->assertEquals(Team::NO_JOIN, $owner->getTeamJoinTime());
    }

    public function testAskForJoin(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $user = $this->unittestCreateUser();

        $this->assertEquals(Team::OK, $this->teamRepository->askForJoin($team, $user));
        $val = $user->getWantedTeams();
        $this->assertCount(1, $val);
        $team1 = $val[0];
        $this->assertEquals($team->getId(), $team1->getId());
    }

    public function testRemoveTeamWillRemoveAskForJoin(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $user = $this->unittestCreateUser();

        $this->assertEquals(Team::OK, $this->teamRepository->askForJoin($team, $user));
        $val = $user->getWantedTeams();
        $this->assertCount(1, $val);

        $this->teamRepository->remove($team);

        $this->entityManager->refresh($user);
        $val = $user->getWantedTeams();
        $this->assertCount(0, $val);
    }

    public function testJoinWillCancelOtherAskForJoin(): void {
        $owner1 = $this->unittestCreateUser();

        $owner2 = $this->unittestCreateUser();

        $team1 = new Team();
        $team1->setName('name'.'_'.$this->unittestRand());
        $team1->setPoints(0);
        $team1->setOwner($owner1);
        $team1->setDescription('something1...');
        $team1->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team1));

        $team2 = new Team();
        $team2->setName('name_'.$this->unittestRand());
        $team2->setPoints(0);
        $team2->setOwner($owner2);
        $team2->setDescription('something2...');
        $team2->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team2));

        $user = $this->unittestCreateUser();

        $this->assertEquals(Team::OK, $this->teamRepository->askForJoin($team1, $user));
        $this->assertEquals(Team::OK, $this->teamRepository->askForJoin($team2, $user));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team2, $user)); // first team's owner accept user

        // it should have removed the 2nd team request
        $this->assertEquals(0, count($user->getWantedTeams()));
    }

    public function testCancelJoin(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $user = $this->unittestCreateUser();

        $this->assertEquals(Team::OK, $this->teamRepository->askForJoin($team, $user));
        $val = $user->getWantedTeams();
        $this->assertCount(1, $val);
        $team1 = $val[0];
        $this->assertEquals($team->getId(), $team1->getId());

        // cancel join request
        $this->entityManager->refresh($user);

        $this->assertEquals(Team::OK, $this->teamRepository->cancelAskJoin($team, $user));

        $this->entityManager->refresh($user);

        $val = $user->getWantedTeams();
        $this->assertCount(0, $val);
    }

    public function testContribution(): void {
        $config = $this->configService->getData();

        // owner don't get any penality
        $config['rank']['points']['ordered']['minute'] = 10.0;
        $config['rank']['points']['rendered']['minute'] = 10.0;
        $config['rank']['points']['coef_owner'] = 1.0;

        $owner = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertTrue($this->teamRepository->addMember($team, $user) == Team::OK);

        $members = $team->members();
        $this->assertCount(2, $members);

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->entityManager->refresh($user);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE + $config['rank']['points']['rendered']['power_detection_frame'] + $config['rank']['points']['rendered']['compute_method_frame'], (int)($user->getPoints()));
        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($user->getTeamContribution()));

        $this->entityManager->refresh($team);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($team->getPoints()));
    }

    public function testContributionOldUser(): void {
        $config = $this->configService->getData();

        // owner don't get any penality
        $config['rank']['points']['ordered']['minute'] = 10.0;
        $config['rank']['points']['rendered']['minute'] = 10.0;
        $config['rank']['points']['coef_owner'] = 1.0;

        $owner = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertTrue($this->teamRepository->addMember($team, $user) == Team::OK);

        $members = $team->members();
        $this->assertCount(2, $members);

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->entityManager->refresh($user);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE + $config['rank']['points']['rendered']['power_detection_frame'] + $config['rank']['points']['rendered']['compute_method_frame'], (int)($user->getPoints()));
        $this->assertTrue((int)($user->getTeamContribution()) == Tile::MINIMUM_REWARD_PER_TILE);

        $this->entityManager->refresh($team);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($team->getPoints()));

        // remove the user
        $this->assertEquals(Team::OK, $this->teamRepository->removeMember($team, $user));

        $this->entityManager->refresh($user);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE + $config['rank']['points']['rendered']['power_detection_frame'] + $config['rank']['points']['rendered']['compute_method_frame'], (int)($user->getPoints()));
        $this->assertTrue((int)($user->getTeamContribution()) == 0);
        $this->assertNull($user->getTeam());

        $this->entityManager->refresh($team);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($team->getPoints()));
        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($team->getPointsFormerUser()));
    }

    public function testContributionNewUser(): void {
        $owner1 = $this->unittestCreateUser();
        $owner2 = $this->unittestCreateUser();

        $user = $this->unittestCreateUser();

        $team1 = new Team();
        $team1->setName('name'.'_'.$this->unittestRand());
        $team1->setPoints(0);
        $team1->setOwner($owner1);
        $team1->setDescription('something...');
        $team1->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team1));

        $team2 = new Team();
        $team2->setName('name'.'_'.$this->unittestRand());
        $team2->setPoints(0);
        $team2->setOwner($owner2);
        $team2->setDescription('something...');
        $team2->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team2));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team1, $user));

        $members = $team1->members();
        $this->assertEquals(2, $members->count());

        // fake the contribution
        $user->setPoints(2000);
        $user->setTeamContribution(1000);
        $this->entityManager->flush();

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team2, $user));

        $this->entityManager->refresh($user);
        $this->assertEquals(0, (int)($user->getTeamContribution()));
    }

    public function testContributionOnPrivateProject(): void {
        $config = $this->configService->getData();

        // owner don't get any penality
        $config['rank']['points']['ordered']['minute'] = 10.0;
        $config['rank']['points']['rendered']['minute'] = 10.0;
        $config['rank']['points']['coef_owner'] = 1.0;

        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $owner));

        $members = $team->members();
        $this->assertEquals(1, $members->count());

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame, false); // false is private project

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->entityManager->refresh($owner);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE + $config['rank']['points']['rendered']['power_detection_frame'] + $config['rank']['points']['rendered']['compute_method_frame'], (int)($owner->getPoints()));
        $this->assertEquals(0, (int)($owner->getTeamContribution()));

        $this->entityManager->refresh($team);

        $this->assertEquals(0, (int)($team->getPoints()));
    }

    public function testContributionOnPrivateProjectButRendeableByTheTeam(): void {
        $config = $this->configService->getData();

        // owner don't get any penality
        $config['rank']['points']['ordered']['minute'] = 10.0;
        $config['rank']['points']['rendered']['minute'] = 10.0;
        $config['rank']['points']['coef_owner'] = 1.0;

        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $owner));

        $members = $team->members();
        $this->assertEquals(1, $members->count());

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame, false); // 0 is private project

        $this->assertTrue($project->addTeamRenderer($team));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->entityManager->refresh($owner);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE + $config['rank']['points']['rendered']['power_detection_frame'] + $config['rank']['points']['rendered']['compute_method_frame'], (int)($owner->getPoints()));
        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($owner->getTeamContribution()));

        $this->entityManager->refresh($team);

        $this->assertEquals(Tile::MINIMUM_REWARD_PER_TILE, (int)($team->getPoints()));
    }

    /**
     * Two users renderer frames, check the points given to the team
     **/
    public function testPointsGenerated(): void {
        $owner = $this->unittestCreateUser();
        $user1 = $this->unittestCreateUser();
        $user2 = $this->unittestCreateUser();
        $user3 = $this->unittestCreateUser();

        $this->entityManager->flush();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user1));
        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user2));
        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $user3));

        $nb_frame = 20;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler1 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $handler2 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $handler3 = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler1, $user1, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler2, $user2, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler3, $user3, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // render some frames
        for ($i = 0; $i < 5; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler1, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $this->assertTrue($this->unittestRenderOneTile($handler2, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $this->assertTrue($this->unittestRenderOneTile($handler3, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $this->entityManager->refresh($user1);
        $this->entityManager->refresh($user2);
        $this->entityManager->refresh($user3);
        $team = $user1->getTeam(); // reload the object
        $this->assertTrue(is_object($team));

        $users_contrib = $user1->getTeamContribution() + $user2->getTeamContribution() + $user3->getTeamContribution();
        $team_contrib = $team->getPoints();

        $this->assertTrue(abs($users_contrib - $team_contrib) < 0.01);
    }

    public function testLastNewMember(): void {
        $owner = $this->unittestCreateUser();
        $user1 = $this->unittestCreateUser();
        $user2 = $this->unittestCreateUser();

        $user3 = $this->unittestCreateUser();
        $this->entityManager->flush();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertTrue($this->teamRepository->addMember($team, $user1) == Team::OK);
        sleep(1); // to be sure registration time is different
        $this->assertTrue($this->teamRepository->addMember($team, $user3) == Team::OK);
        sleep(1); // to be sure registration time is different
        $this->assertTrue($this->teamRepository->addMember($team, $user2) == Team::OK);

        $last = $team->getLastNewMember();
        $this->assertTrue(is_object($last));
        $this->assertTrue($last->getId() == $user2->getId());

        // remove the last user
        $this->assertEquals(Team::OK, $this->teamRepository->removeMember($team, $user2));
        $members = $team->members();
        $this->assertFalse($members->contains($user2));

        $last = $team->getLastNewMember();
        $this->assertTrue(is_object($last));
        $this->assertEquals($user3->getId(), $last->getId());
    }

    public function testLastNewMemberOnNewTeam(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $last = $team->getLastNewMember();
        $this->assertTrue(is_object($last));
        $this->assertTrue($last->getId() == $owner->getId());
    }

    public function testLastNewMemberOnEmptyTeam(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->removeMember($team, $owner));
        $members = $team->members();
        $this->assertFalse($members->contains($owner));

        $last = $team->getLastNewMember();
        $this->assertNull($last);
    }

    public function testOwnerLeaveHisTeam(): void {
        $owner = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->removeMember($team, $owner));
        $members = $team->members();
        $this->assertFalse($members->contains($owner));

        $this->entityManager->refresh($team);
        $this->assertEquals('admin', $team->getOwner()->getId()); // when the owner leave the team, give it to the admin
    }

    public function testRemoveMemberDoesNOTRemoveTeam(): void {
        // there were a bug when a member of a team delete his account, it was removing the ownership of the team
        // EVEN if that member was not the owner of the team !

        // init
        $owner = $this->unittestCreateUser();
        $member1 = $this->unittestCreateUser();
        $member2 = $this->unittestCreateUser();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($this->teamRepository->add($team));

        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $member1));
        $this->assertEquals(Team::OK, $this->teamRepository->addMember($team, $member2));

        $this->entityManager->refresh($team);
        $this->assertEquals($owner->getId(), $team->getOwner()->getId());

        // action
        $this->entityManager->getRepository(User::class)->remove($member1);

        // check
        $this->entityManager->refresh($team);
        $this->assertEquals($owner->getId(), $team->getOwner()->getId());
    }
}
