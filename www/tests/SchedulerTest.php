<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Project;
use App\Entity\Task;
use App\Entity\Team;
use App\Entity\Tile;
use App\Entity\TilePowerDetection;
use App\Entity\User;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\FileBag;

class SchedulerTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        $this->unittestRemoveAllProjects();
        parent::tearDown();
    }

    public function testComputeMethodCPU(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $project_cpu = $this->unittestCreateProject($owner, 10);
        $project_cpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));

        $project_gpu = $this->unittestCreateProject($owner, 10);
        $project_gpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($renderer, null, $request) == ClientProtocol::OK);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);

        $projects = $handler->getSession()->getUser()->getAvailableProjects($handler->getSession()->getComputeMethod(), $handler->getSession()->getMemoryAllowed2());

        // only CPU project should be there
        $this->assertCount(1, $projects);
    }

    public function testComputeMethodGPU(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $project_cpu = $this->unittestCreateProject($owner, 10);
        $project_cpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));

        $project_gpu = $this->unittestCreateProject($owner, 10);
        $project_gpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();


        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $gpu = $this->unittestCreateGpu();
        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($renderer, null, $request) == ClientProtocol::OK);

        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);

        $projects = $handler->getSession()->getUser()->getAvailableProjects($handler->getSession()->getComputeMethod(), $handler->getSession()->getMemoryAllowed2());

        // only GPU project should be there
        $this->assertCount(1, $projects);
    }

    public function testComputeMethodBoth(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $project_cpu = $this->unittestCreateProject($owner, 10);
        $project_cpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));

        $project_gpu = $this->unittestCreateProject($owner, 10);
        $project_gpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $gpu = $this->unittestCreateGpu();
        $cpu = $this->unittestCreateCpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'cpu_cores' => $cpu->getCores(),
            'ram' => '80000000',  // 80GB of ram
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertTrue($handler->configInput($renderer, null, $request) == ClientProtocol::OK);

        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_BOTH,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);

        $projects = $handler->getSession()->getUser()->getAvailableProjects($handler->getSession()->getComputeMethod(), $handler->getSession()->getMemoryAllowed2());

        // CPU and GPU projects should be there
        $this->assertCount(2, $projects);
    }

    public function testRenderMyProjectFirstOnAccurateScheduler(): void {
        $config = $this->configService->getData();

        $other = $this->unittestCreateUser();
        $other->setRenderedFrames(2); // to avoid power detection frame
        $other->setPoints($config['rank']['points']['max']);
        $this->entityManager->flush();

        $me = $this->unittestCreateUser();
        $me->setRenderedFrames(2); // to avoid power detection frame
        $me->setPoints(-1 * $config['rank']['points']['max']);
        $this->entityManager->flush();

        $nb_frame = 10;

        $project_other = $this->unittestCreateProject($other, $nb_frame);

        $project_me = $this->unittestCreateProject($me, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $me, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());

        $this->assertEquals($frame->getFrame()->getProject()->getId(), $project_me->getId());
    }

    public function testDoNotRenderMyProjectFirst(): void {
        $config = $this->configService->getData();

        $user1 = $this->unittestCreateUser();
        $user1->setPoints($config['rank']['points']['max']);
        $this->entityManager->flush();

        $user2 = $this->unittestCreateUser();
        $user2->setPoints(-1 * $config['rank']['points']['max']);
        $user2->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST, 0);
        $this->entityManager->flush();

        $nb_frame = 10;

        $project1 = $this->unittestCreateProject($user1, $nb_frame);
        $project2 = $this->unittestCreateProject($user2, $nb_frame);

        $cpu = $this->unittestCreateCpu();

        $session = $this->unittestCreateSession($user2, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        $session = $handler->getSession();

        $this->unittestRenderFirstTwoTiles($handler, $user2, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // 3rd frame => actual work
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertEquals($frame->getFrame()->getProject()->getId(), $project1->getId());
    }

    public function testDoNotRenderPrivateProject(): void {
        $user_owner = $this->unittestCreateUser();
        $this->entityManager->flush();

        $user_render = $this->unittestCreateUser();

        $nb_frame = 100;

        $project = $this->unittestCreateProject($user_owner, $nb_frame, false); // not a public project

        $cpu = $this->unittestCreateCpu();

        $session = $this->unittestCreateSession($user_render, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        $this->unittestRenderFirstTwoTiles($handler, $user_render, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // third frame => actual job
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    public function testRenderPrivateProjectWhenIsAuthorizedRenderer(): void {
        $user_owner = $this->unittestCreateUser();
        $this->entityManager->flush();

        $user_render = $this->unittestCreateUser();

        $nb_frame = 100;

        $project = $this->unittestCreateProject($user_owner, $nb_frame, false); // not a public project

        // add user_render to the private project
        $this->assertTrue($project->addRenderer($user_render));

        $cpu = $this->unittestCreateCpu();
        $cpu->setRenderedFrames(2); // to avoid power detection frame
        $cpu->setPower(10000);
        $this->entityManager->flush();

        $session = $this->unittestCreateSession($user_render, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        $this->unittestRenderFirstTwoTiles($handler, $user_render, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // third frame => actual job
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertEquals($frame->getFrame()->getProject()->getId(), $project->getId());
    }

    public function testVerySlowComputerDontGetNewProject(): void {
        $config = $this->configService->getData();

        $user_owner = $this->unittestCreateUser();

        $user_render = $this->unittestCreateUser();

        $nb_frame = 10;

        $project = $this->unittestCreateProject($user_owner, $nb_frame);

        $cpu = $this->unittestCreateCpu();

        $session = $this->unittestCreateSession($user_render, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        // first frame => compute method
        $this->unitestRenderComputeMethodTile($handler, $user_render, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $frame->setRenderTime(60);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered(($config['power']['threshold_very_slow_computer'] - 10) / TilePowerDetection::SCALE);
        $this->assertEquals(0, $handler->getSession()->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession));

        // third frame => actual project
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status); // only new project are available
    }

    public function testVerySlowComputerDontNearlyFinishedProject(): void {
        $config = $this->configService->getData();

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($user, $nb_frames);

        $this->unittestRenderFirstTwoTiles($handler, $user, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        for ($i = 1; $i <= $nb_frames - 2; $i++) {
            // third frame => actual job
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $this->assertEquals(ClientProtocol::OK, $status);
            $this->assertTrue(is_object($frame));
            $this->assertFalse($frame->isComputeMethod());
            $this->assertFalse($frame->isPowerDetection());

            $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));
        }

        $user_render = $this->unittestCreateUser();

        $cpu = $this->unittestCreateCpu();

        $session = $this->unittestCreateSession($user_render, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        // first frame => compute method
        $this->unitestRenderComputeMethodTile($handler, $user_render, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $frame->setRenderTime(60);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered(($config['power']['threshold_very_slow_computer'] - 10) / TilePowerDetection::SCALE);
        $this->assertEquals(0, $handler->getSession()->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession));

        // third frame => actual project
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status); // only nearly finished project are available
    }

    public function testRenderMyTeamProjectsFirst(): void {
        $teamRepository = $this->entityManager->getRepository(Team::class);
        $config = $this->configService->getData();

        $user_team_owner = $this->unittestCreateUser();

        $user_team_member = $this->unittestCreateUser();

        $user_renderer = $this->unittestCreateUser();
        $user_renderer->setRenderedFrames(2); // to avoid power detection frame
        $user_renderer->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST, 1);
        $this->entityManager->flush();

        $user_project = $this->unittestCreateUser();
        $user_project->setPoints($config['rank']['points']['max']);
        $this->entityManager->flush();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($user_team_owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($teamRepository->add($team));

        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $user_team_member));
        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $user_renderer));

        $nb_frame = 10;
        $project_team_member = $this->unittestCreateProject($user_team_member, $nb_frame);

        $project2 = $this->unittestCreateProject($user_project, $nb_frame);

        $cpu = $this->unittestCreateCpu();
        $cpu->setRenderedFrames(2); // to avoid power detection frame
        $cpu->setPower(10000);
        $this->entityManager->flush();

        $session = $this->unittestCreateSession($user_renderer, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        $this->unittestRenderFirstTwoTiles($handler, $user_renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // third frame, actual project
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertEquals($frame->getFrame()->getProject()->getId(), $project_team_member->getId());
    }

    public function testDoNotRenderMyTeamProjectsFirst(): void {
        $teamRepository = $this->entityManager->getRepository(Team::class);
        $config = $this->configService->getData();

        $user_team_owner = $this->unittestCreateUser();

        $user_team_member = $this->unittestCreateUser();

        $user_renderer = $this->unittestCreateUser();
        $user_renderer->setRenderedFrames(2); // to avoid power detection frame
        $user_renderer->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST, 0);
        $this->entityManager->flush();

        $user_project = $this->unittestCreateUser();
        $user_project->setPoints($config['rank']['points']['max']);
        $this->entityManager->flush();

        $team = new Team();
        $team->setName('name'.'_'.$this->unittestRand());
        $team->setPoints(0);
        $team->setOwner($user_team_owner);
        $team->setDescription('something...');
        $team->setCreationTime(time());
        $this->assertTrue($teamRepository->add($team));

        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $user_team_member));
        $this->assertEquals(Team::OK, $teamRepository->addMember($team, $user_renderer));

        $nb_frame = 10;
        $project_team_member = $this->unittestCreateProject($user_team_member, $nb_frame);

        $project2 = $this->unittestCreateProject($user_project, $nb_frame);

        $cpu = $this->unittestCreateCpu();
        $cpu->setRenderedFrames(2); // to avoid power detection frame
        $cpu->setPower(10000);
        $this->entityManager->flush();

        $session = $this->unittestCreateSession($user_renderer, $cpu, null);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, $session, $this->phpSession);

        $this->unittestRenderFirstTwoTiles($handler, $user_renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        // third frame, actual project
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertEquals($frame->getFrame()->getProject()->getId(), $project2->getId());
    }

    public function testRenderBrokenGPUProjectLast(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();
        $this->entityManager->flush();

        $nb_frame = 10;
        $project_high = $this->unittestCreateProject($owner, $nb_frame);
        $project_high->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

        $project_high->setVramFailure(2000000000);
        $this->entityManager->flush();

        $owner->setPoints($owner->getPoints() - 100000);
        $project_low = $this->unittestCreateProject($owner, $nb_frame);
        $project_low->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));

        $project_low->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
        $this->entityManager->flush();

//        $job_low = $this->unittest_create_project($project_low, $nb_frame);
//        $this->assertTrue(is_object($job_low));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $gpu = $this->unittestCreateGpu();

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($renderer, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

//        $job = $frame->getJob();
//        $this->assertTrue(is_object($job));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateComputeMethodImageFile());
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertTrue($frame->isComputeMethod());
        $this->assertEquals(0, $session->validateFrame($a_frame, $file, $this->phpSession));

        // second frame => power frame
        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());

//        $job = $frame->getJob();
//        $this->assertTrue(is_object($job));

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateImageFile());
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertFalse($frame->isComputeMethod());
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($this->configService->getData()['power']['cpu']['const'] * 2.0 / TilePowerDetection::SCALE);
        $this->assertEquals(0, $session->validateFrame($a_frame, $file, $this->phpSession));

        // my project
        $request = array(
            'computemethod' => ClientProtocol::COMPUTE_METHOD_GPU_ONLY,
            'gpu_model' => $gpu->getModel(),
            'gpu_type' => $gpu->getType(),
            'gpu_ram' => $gpu->getMemory());
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
//        $project_work = $frame->getScene();
//        $this->assertTrue(is_object($project_work));

        $this->assertEquals($project_low->getId(), $frame->getFrame()->getProject()->getId());
    }

    public function testDonotRenderBlockedProject(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));


        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, 1);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    public function testOwnerCanRenderHisBlockedProjectOnLightBlock(): void {
        $owner = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));


        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, 1);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
    }

    public function testOwnerCanRenderHisBlockedProjectOnHeavyBlock(): void {
        $owner = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));


        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, 97);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    public function testBlockUserFromRenderingMyProject(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // Block the renderer
        $this->assertCount(0, $owner->getBlockedRenderers());

        $owner->addBlockedRenderer($renderer->getId());
        $renderer->init(); // reset cache

        $blocks = $owner->getBlockedRenderers();
        $this->assertCount(1, $blocks);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    public function testBlockAnAnotherUserFromRenderingMyProject(): void {
        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();

        $rendererO = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // ban the renderer, another user
        $this->assertCount(0, $owner->getBlockedRenderers());

        $owner->addBlockedRenderer($rendererO->getId());

        $blocks = $owner->getBlockedRenderers();
        $this->assertCount(1, $blocks);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
    }

    public function testBlockAnAnotherUserToRenderHisProject(): void {
        $other_owner = $this->unittestCreateUser();

        $me = $this->unittestCreateUser();

        $nb_frame = 10;
        $project = $this->unittestCreateProject($other_owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $me, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // Block the owner
        $this->assertCount(0, $me->getBlockedOwners());

        $me->addBlockedOwner($other_owner->getId());

        $blocks = $me->getBlockedOwners();
        $this->assertCount(1, $blocks);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    public function testPreemptiveBlockOwnerCanRenderHisProject(): void {
        $owner = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));


        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, Project::PREEMPTIVE_BLOCK_CONST);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
    }

    public function testPreemptiveBlockUnBlockProjectOnGoodOwnerRender(): void {
        $owner = $this->unittestCreateUser();

        $nb_frame = 5;
        $project = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // block the project
        $this->entityManager->getRepository(Project::class)->block($project, Project::PREEMPTIVE_BLOCK_CONST);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());

        // hack the request time to actually create a long rendertime
        $frame->setRequestTime(time() - 1000);
        $this->entityManager->flush();

        $request = array(
            'job' => $frame->getId(),
            'frame' => $frame->getNumber(),
            'rendertime' => 10,
            'extras' => '');
        $files = new FileBag();
        $files->set('file', $this->unittestGenerateImageFile());
        /** @var ?Tile $a_frame */
        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($a_frame));
        $this->assertFalse($a_frame->isComputeMethod());
        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));

        $project = $this->entityManager->getRepository(Project::class)->find($project->getId()); // reload the object
        $this->assertTrue(is_object($project));
        $this->assertFalse($project->isBlocked());
    }

//    public function testPreemptiveBlock_BadRenderFromOwnerDoesNotUnBlockProject() {
//        $config = $this->configService->getData();
//
//        $owner = $this->unittest_create_user();
//        $this->assertTrue(is_object($owner));
//
//        $project = $this->unittest_create_scene($owner);
//        $this->assertTrue(is_object($project));
//        $nb_frame = 5;
//        $job = $this->unittest_create_job($project, $nb_frame);
//        $this->assertTrue(is_object($job));
//
//        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
//        $this->assertTrue(is_object($handler));
//
//        $this->assertTrue($this->unittest_render_first_two_frames($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
//
//        // block the project
//        $project->block(Job::$preemptiveBlockConst);
//
//        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
//        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
//        $this->assertEquals(ClientProtocol::OK, $status);
//        $this->assertTrue(is_object($frame));
//        $this->assertFalse($frame->isComputeMethod());
//
//        $job = $frame->getJob();
//        $this->assertTrue(is_object($job));
//
//        $rendertime = $config['power']['rendertime_max_reference'] * 10;
//        // hack the request time to actually create a long rendertime
//        $frame->setRequestTime(time() - $rendertime * 2);
//        $frame->update();
//
//        $request = array(
//                    'job' => $job->getId(),
//                    'frame' => $frame->getNumber(),
//                    'rendertime' => $rendertime,
//                    'extras' => '');
//        $files = array();
//        $files['file']['tmp_name'] = $this->unittest_generate_image_file();
//        $tmp_file_post = $files['file']['tmp_name'];
//        list($status, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request, $files, true); // true => $disable_security_check
//        $this->assertEquals(ClientProtocol::OK, $status);
//        $this->assertTrue(is_object($a_frame));
//        $this->assertFalse($a_frame->isComputeMethod());
//        $this->assertEquals(0, $a_frame->validateFromShepherd(2, 0, 10000));
//
//
//        $project = $this->entityManager->getRepository(Project::class)->find($project->getId()); // reload the object
//        $this->assertTrue($project->isBlocked());
//    }

    public function testRenderFirstOnMyHighPriorityUser(): void {
        $owner_my_high_priority = $this->unittestCreateUser();
        $owner_my_high_priority->setPoints(-1000000);
        $this->entityManager->flush();

        $owner_with_lot_of_points = $this->unittestCreateUser();
        $owner_with_lot_of_points->setPoints(1000000);
        $this->entityManager->flush();

        $me = $this->unittestCreateUser();

        $nb_frame = 10;
        $project_my_high_priority = $this->unittestCreateProject($owner_my_high_priority, $nb_frame);

//        $job_my_high_priority = $this->unittest_create_job($project_my_high_priority, $nb_frame);
//        $this->assertTrue(is_object($job_my_high_priority));

        $project_with_lot_of_points = $this->unittestCreateProject($owner_with_lot_of_points, $nb_frame);
//        $job_with_lot_of_points = $this->unittest_create_job($project_with_lot_of_points, $nb_frame);
//        $this->assertTrue(is_object($job_with_lot_of_points));

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $me, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // Add the owner to my high priority userlist
        $this->assertCount(0, $me->getHighPriorityUsers());

        $me->addHighPriorityUser($owner_my_high_priority->getId());

        $highpriorityusers = $me->getHighPriorityUsers();
        $this->assertCount(1, $highpriorityusers);

        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertEquals($project_my_high_priority->getId(), $frame->getFrame()->getProject()->getId());
    }

    /**
     * Even if the project is being rate limited, the owner can render his own project (with more than 10 machines)
     */
    public function testProjectRateLimitRenderMyProject(): void {
        $config = $this->configService->getData();

        $owner = $this->unittestCreateUser();
        $nb_frame = 10 + $config['project']['rate_limit']['max_rendering_tile'];
        $project = $this->unittestCreateProject($owner, $nb_frame);

        // launch more session have the rate limit
        for ($i = 0; $i < $config['project']['rate_limit']['max_rendering_tile'] + 5; $i++) {
            $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
            $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
            $this->assertEquals(ClientProtocol::OK, $status);
        }
    }

    /**
     * If the owner doesn't have a running session, it should be limited to 10 sessions
     */
    public function testProjectRateLimitNoSession(): void {
        $config = $this->configService->getData();

        $owner = $this->unittestCreateUser();
        $renderer = $this->unittestCreateUser();

        $nb_frame = 10 + $config['project']['rate_limit']['max_rendering_tile'];
        $project = $this->unittestCreateProject($owner, $nb_frame);

        // launch sessions have the rate limit
        for ($i = 0; $i < $config['project']['rate_limit']['max_rendering_tile']; $i++) {
            $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
            $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
            $this->assertEquals(ClientProtocol::OK, $status);
        }

        // extra session must fail because of rate limit
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $status);
    }

    /**
     * If the owner have a running session, it should be NOT be limited
     */
    public function testProjectRateLimitOwnerWithSession(): void {
        $config = $this->configService->getData();

        $owner = $this->unittestCreateUser();
        $renderer = $this->unittestCreateUser();

        $nb_frame = 10 + $config['project']['rate_limit']['max_rendering_tile'];
        $project = $this->unittestCreateProject($owner, $nb_frame);

        // launch sessions have the rate limit
        for ($i = 0; $i < $config['project']['rate_limit']['max_rendering_tile']; $i++) {
            $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
            $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
            $this->assertEquals(ClientProtocol::OK, $status);
        }

        // owner launchs a session
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $owner, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // extra session must be ok because of rate limit
        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);
        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $request = array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY);
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput($request);
        $this->assertEquals(ClientProtocol::OK, $status);
    }

    /**
     * Test of Session::setMaxRendertime() for cpu only
     */
    public function testSessionMaxRendertimePerFrameCPU(): void {
        $nb_frame = 5;
        $owner = $this->unittestCreateUser();
        $owner->setPoints(10000);

        $renderer1 = $this->unittestCreateUser();
        $renderer2 = $this->unittestCreateUser();

        $project_cpu = $this->unittestCreateProject($owner, $nb_frame);
        $project_cpu->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));

        // cpu1 => 200%
        $cpu1 = $this->unittestCreateCpu();
        $session1 = $this->unittestCreateSession($renderer1, $cpu1, null, (int)($this->configService->getData()['power']['cpu']['const'] * 2.0));
        $session1->setRenderedframes(2); // to avoid the compute method and power detection
        $session1->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $this->entityManager->flush();

        // cpu2 => 400%
        $cpu2 = $this->unittestCreateCpu();
        $session2 = $this->unittestCreateSession($renderer2, $cpu2, null, (int)($this->configService->getData()['power']['cpu']['const'] * 4.0));
        $session2->setRenderedframes(2); // to avoid the compute method and power detection
        $session2->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $this->entityManager->flush();

        // session1 renders a frame in 2min with 200% machine
        $tiles = $project_cpu->tilesWithStatus(Constant::WAITING);
        $a_tile = $tiles[array_rand($tiles)];
        $this->assertEquals(0, $a_tile->take($session1));
        $a_tile->setRequestTime(time() - 150);
        $this->entityManager->flush();
        $this->assertEquals(0, $a_tile->validateFromShepherd(120, 0, 1000));

        // session2 has no render time limit, so it should get a job
        list($j, $remaining_frames, $renderable_jobs) = $session2->requestJob($this->phpSession);
        $this->assertIsObject($j);

        // session2 has a limit of 20s, so it shouldn't get project
        $session2->setMaxRendertime(20);
        $this->entityManager->flush($session2);
        list($j, $remaining_frames, $renderable_jobs) = $session2->requestJob($this->phpSession);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $j);
    }

    /**
     * Test of Session::setMaxRendertime() for both compute method
     */
    public function testSessionMaxRendertimePerFrameBOTH(): void {
        $nb_frame = 5;
        $owner = $this->unittestCreateUser();
        $owner->setPoints(10000);

        $renderer1 = $this->unittestCreateUser();
        $renderer2 = $this->unittestCreateUser();

        $project = $this->unittestCreateProject($owner, $nb_frame);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));

        // gpu1 => 100%
        $cpu1 = $this->unittestCreateCpu();
        $gpu1 = $this->unittestCreateGpu();
        $session1 = $this->unittestCreateSession($renderer1, $cpu1, $gpu1, (int)($this->configService->getData()['power']['cpu']['const'] * 1.0), (int)($this->configService->getData()['power']['gpu']['const'] * 1.0));
        $session1->setRenderedframes(2); // to avoid the compute method and power detection
        $session1->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $this->entityManager->flush();

        // cpu2 => 250% gpu2 => 50%
        $cpu2 = $this->unittestCreateCpu();
        $gpu2 = $this->unittestCreateGpu();
        $session2 = $this->unittestCreateSession($renderer2, $cpu2, $gpu2, (int)($this->configService->getData()['power']['cpu']['const'] * 2.5), (int)($this->configService->getData()['power']['cpu']['const'] * 0.5));
        $session2->setRenderedframes(2); // to avoid the compute method and power detection
        $session2->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $this->entityManager->flush();

        // session1 renders a frame in 2min with 100% machine
        $tiles = $project->tilesWithStatus(Constant::WAITING);
        $a_tile = $tiles[array_rand($tiles)];
        $this->assertEquals(0, $a_tile->take($session1));
        $a_tile->setRequestTime(time() - 150);
        $this->entityManager->flush();
        $this->assertEquals(0, $a_tile->validateFromShepherd(120, 0, 1000));

        // session2 has a limit of 60s, so it shouldn't get project
        $session2->setMaxRendertime(60);
        $this->entityManager->flush($session2);
        list($j, $remaining_frames, $renderable_jobs) = $session2->requestJob($this->phpSession);
        $this->assertEquals(ClientProtocol::JOB_REQUEST_NOJOB, $j);
    }
}
