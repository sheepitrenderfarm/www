<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Entity\AdventCalendarClaimedDay;

class AdventCalendarTest extends BaseTestCase {

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testUniqueUsers(): void {
        $repo = $this->entityManager->getRepository(AdventCalendarClaimedDay::class);
        $user1 = $this->unittestCreateUser();
        $user2 = $this->unittestCreateUser();

        $this->assertCount(0, $repo->getUniqueUsers());

        $repo->addFor($user1);
        $this->assertCount(1, $repo->getUniqueUsers());

        $repo->addFor($user2);
        $data = $repo->getUniqueUsers();
        $this->assertCount(2, $data);
        $this->assertTrue(in_array($user1->getId(), $data));
        $this->assertTrue(in_array($user2->getId(), $data));
    }
}
