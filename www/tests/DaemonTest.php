<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Constant;
use App\Entity\Blender;
use App\Entity\FrameChessboard;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\Task;
use App\Entity\TilePowerDetection;
use App\Entity\TileReal;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use App\Service\Daemon;
use App\Utils\Misc;

class DaemonTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
        $this->unittestClear();
    }

    protected function tearDown(): void {
        $this->unittestClear();
        parent::tearDown();
    }

    public function testAddTaskJobAnimationEndOfProject(): void {
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $nb_frames = 10;
        $project = $this->unittestCreateProject($user, $nb_frames);

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));


        // second is power detection frame
        $this->unitestRenderPowerDetectionFrame($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY);

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));


        for ($i = 1; $i <= $nb_frames; $i++) {
            // third actual frame
            list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
            $this->assertEquals(ClientProtocol::OK, $status);
            $this->assertTrue(is_object($frame));
            $this->assertFalse($frame->isComputeMethod());
            $this->assertFalse($frame->isPowerDetection());

            $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));
        }

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // project upload is not run (since no daemon is enabled)
    }

    public function testAddTaskJobAnimation(): void {
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        $project = $this->unittestCreateProject($user, 10);

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // second power detection frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $frame->setRenderTime(2);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($this->configService->getData()['power']['cpu']['const'] / TilePowerDetection::SCALE);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateImageFile(), $this->phpSession));

        // third actual frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());

        $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // project upload is not run (since no daemon is enabled)
    }

    public function testAddTaskJobSingleFrame(): void {
        $config = $this->configService->getData();

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $user = $this->unittestCreateUser();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        // add project
        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();
        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName('name_'.$this->unittestRand());
        $project->setOwner($user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setPath('path_fake');
        $project->setPictureExtension('jpg');
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdInternal::ID));

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $this->assertEquals($config['project']['singleframe']['nb_tile'] * $config['project']['singleframe']['nb_tile'], $this->entityManager->getRepository(Project::class)->addFrames($project, 100, 100, 1, 1920, 1080, $config['project']['singleframe']['nb_tile'] * $config['project']['singleframe']['nb_tile'], FrameChessboard::class, TileReal::class, 'some_path'));

        $this->entityManager->getRepository(Project::class)->createChunks($project, $config['storage']['path'].'projects'.'/'.$this->getChunkProject1());

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $frame->setRenderTime(2);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($this->configService->getData()['power']['cpu']['const'] / TilePowerDetection::SCALE);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // third => actual frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());

        $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // project upload is not run (since no daemon is enabled)
    }

    public function testAddTaskJobAnimationTile(): void {
        $config = $this->configService->getData();

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $user = $this->unittestCreateUser();
        $this->entityManager->flush();

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $cpu = $this->unittestCreateCpu();

        // add project
        $nb_frames = 10;
        $default_binary = $this->entityManager->getRepository(Blender::class)->getDefault();
        $project = new Project();
        $project->setExecutable($default_binary);
        $project->setPublicRender(true);
        $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        $project->setName('name_'.$this->unittestRand());
        $project->setOwner($user);
        $project->setPublicThumbnail(true);
        $project->setStatus(Constant::WAITING); // it should be initArchive who do that
        $project->setPath('path_fake');
        $project->setPictureExtension('jpg');
        $project->setShepherd($this->entityManager->getRepository(Shepherd::class)->find(ShepherdInternal::ID));

        $this->entityManager->persist($project);
        $this->entityManager->flush();

        $dirname = $config['tmp_dir'].'/dir_'.$this->unittestRand();
        mkdir($dirname);
        file_put_contents($dirname.'/'.$project->getPath(), '');

        $this->assertEquals($config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'] * $nb_frames, $this->entityManager->getRepository(Project::class)->addFrames($project, 1, $nb_frames, 1, 1920, 1080, $config['project']['singleframe']['nb_tile_animation'] * $config['project']['singleframe']['nb_tile_animation'], FrameChessboard::class, TileReal::class, $dirname));

        $this->entityManager->getRepository(Project::class)->createChunks($project, $dirname.'/'.$project->getPath());

        $request = array(
            'cpu_model_name' => $cpu->getModel(),
            'ram' => 8000000,
            'cpu_cores' => $cpu->getCores(),
            'os' => 'linux',
            'bits' => '64bit');
        $this->assertEquals(ClientProtocol::OK, $handler->configInput($user, null, $request));

        $session = $handler->getSession();

        // first frame => compute method
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertTrue($frame->isComputeMethod());

        $frame->setRenderTime(2);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // second frame => power detection
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertTrue($frame->isPowerDetection());

        $frame->setRenderTime(2);
        /** @var TilePowerDetection $frame */
        $frame->setSpeedSamplesRendered($this->configService->getData()['power']['cpu']['const'] / TilePowerDetection::SCALE);
        $this->assertEquals(0, $session->validateFrame($frame, $this->unittestGenerateComputeMethodImageFile(), $this->phpSession));

        // third =>  actual frame
        list($status, $frame, $remaining_frames, $renderable_projects) = $handler->jobRequestInput(array('computemethod' => ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        $this->assertEquals(ClientProtocol::OK, $status);
        $this->assertTrue(is_object($frame));
        $this->assertFalse($frame->isComputeMethod());
        $this->assertFalse($frame->isPowerDetection());

        $this->assertEquals(0, $frame->validateFromShepherd(2, 0, 10000));

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // project upload is not run (since no daemon is enabled)
    }

    public function testTaskProjectArchive(): void {
        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $n_task = count($tasks);

        $owner = $this->unittestCreateUser();

        $renderer = $this->unittestCreateUser();
        $this->entityManager->persist($renderer);
        $this->entityManager->flush();

        $nb_frame = 10;
        $job = $this->unittestCreateProject($owner, $nb_frame);

        $handler = $this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $this->phpSession);

        $this->assertTrue($this->unittestRenderFirstTwoTiles($handler, $renderer, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));

        // render some frames
        for ($i = 0; $i < $nb_frame; $i++) {
            $this->assertTrue($this->unittestRenderOneTile($handler, ClientProtocol::COMPUTE_METHOD_CPU_ONLY));
        }

        $tasks = $this->entityManager->getRepository(Task::class)->findAll();
        $this->assertCount($n_task + 1, $tasks); // project upload is not run (since no daemon is enabled)

        $daemon = new Daemon($this->entityManager, $this->configService, $this->entityManager->getRepository(Task::class));
        $this->assertTrue($daemon->canWork());


        $this->assertTrue($daemon->workOneTime()); // project upload
        $this->assertCount($n_task, $this->entityManager->getRepository(Task::class)->findAll());
    }
}
