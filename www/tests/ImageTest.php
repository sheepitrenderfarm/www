<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

use App\Service\Image;

class ImageTest extends BaseTestCase {

    public function testIsImage(): void {
        $path = $this->unittestGenerateImageFile();
        $this->assertTrue(file_exists($path));

        $im = new Image($path);

        $this->assertTrue($im->isImage());
        $this->assertFalse($im->isFullyBlack());
    }

    public function testIsBlack(): void {
        $file = $this->unittestGenerateBlackImageFile();
        $this->assertTrue(file_exists($file->getRealPath()));

        $im = new Image($file->getRealPath());

        $this->assertTrue($im->isImage());
        $this->assertTrue($im->isFullyBlack());
    }

    public function testIsBlackGpu1(): void {
        $im = new Image(__DIR__.'/data/bad_gpu_black_frame/fully_black_image.png');
        $this->assertTrue($im->isImage());
        $this->assertTrue($im->isFullyBlack());
    }

    public function testIsBlackGpu2(): void {
        $im = new Image(__DIR__.'/data/bad_gpu_black_frame/nvidia-nv300-cuda-1.2-blackframe-rgb.png');
        $this->assertTrue($im->isImage());
        $this->assertTrue($im->isFullyBlack());
    }

    public function testIsBlackGpu3(): void {
        $im = new Image(__DIR__.'/data/bad_gpu_black_frame/nvidia-nv300-cuda-1.2-blackframe-rgba.png');
        $this->assertTrue($im->isImage());
        $this->assertTrue($im->isFullyBlack());
    }

    public function testIsBlackGpu4(): void {
        $im = new Image(__DIR__.'/data/bad_gpu_black_frame/nvidia-nv300-cuda-1.2-blackframe.jpg');
        $this->assertTrue($im->isImage());
        $this->assertTrue($im->isFullyBlack());
    }

    public function testIsBlackTGA(): void {
        $im = new Image(__DIR__.'/data/image.tga');

        $this->assertTrue($im->isImage());
        $this->assertFalse($im->isFullyBlack());
    }

    public function testIsTGAWithoutExtension(): void {
        $config = $this->configService->getData();
        $filename = $config['tmp_dir'].'/file_'.$this->unittestRand().'.sheepit';
        copy(__DIR__.'/data/image.tga', $filename);
        $im = new Image($filename);

        $this->assertTrue($im->isImage());
        $this->assertFalse($im->isFullyBlack());
    }
}
