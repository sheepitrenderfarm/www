<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Test;

class ConfigTest extends BaseTestCase {

    protected function setUp(): void {
        parent::setUp();
    }

    public function testCriterionExists(): void {
        $config = $this->configService->getData();

        foreach ($config['scheduler']['criterion'] as $sheduler_name => $criterions) {
            foreach ($criterions as $class_name => $weight) {
                $class = "\\App\\Scheduler\\Criterion\\".$class_name;
                $this->assertTrue(class_exists($class), $class.' does NOT exist');
            }
        }
    }
}
