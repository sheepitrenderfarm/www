$(function () {
	$(document).tooltip({
		content: function () {
			return $(this).prop('title');
		}
	});
});

function requestReloadOnSuccess(path_, data_) {
	$.ajax({
		type: 'POST',
		url: path_,
		data: data_,
		success: function(responseText) {
			if (responseText === 'OK') {
				// force reload of page (instead of the deprecated windows.location.reload(true)
				window.location.href = window.location.href
			}
			else {
				alert(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function requestThemShowOnId(id_, path_) {
	$.ajax({
		type: 'POST',
		url: path_,
		success: function(responseText) {
			var element = document.getElementById(id_);
			element.innerHTML = responseText
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function requestActionThemShowOnId(action_url_, id_, display_url_) {
	$.ajax({
		type: 'POST',
		url: action_url_,
		data: {},
		success: function(responseText) {
			if (responseText === 'OK') {
				requestThemShowOnId(id_, display_url_);
			}
			else {
				alert(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function keepPHPSessionAlives() {
	setInterval(function() {
		$.ajax({
			type: 'GET',
			url: '/'
		});
		},
		600000);
}

function isJSON(str) {
	try {
		return (JSON.parse(str) && !!str);
	}
	catch (e) {
		return false;
	}
}