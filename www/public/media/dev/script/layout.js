var config_interval = null;

$(document).ready(function() {
  /** FAQ **/

  if (window.location.pathname == '/faq') {
    var hash = window.location.hash;
    $(hash)
        .collapse('show')
        .closest('.panel-group')
        .find('.panel-collapse.in')
        .collapse('hide');
    $('html, body')
        .animate({
          scrollTop: $(hash).offset().top - 40
        }, 'slow');
  }

});

