(function($) {
  $.fn.sheepit_slider = function(options) {
    function show_sheepit_item(main_dom, index) {
      main_dom.find('ul').find('a').each(function() { $j(this).removeClass('selected') });
      main_dom.find('li').each(function(j) {
        if (j == index) {
          $j(this).css('display', 'inherit');
          $j(this).animate({'opacity': 1.0})
        }
        else {
          $j(this).animate({'opacity': 0.0}, function() { $j(this).css('display', 'none') });
        }
      });
      main_dom.siblings('div.centered').find('ul').find('li').find('a').each(function(j) {
        if (j == index)
          $j(this).addClass('selected');
        else
          $j(this).removeClass('selected');
      });
    }

    this.each(function() {
      var main_dom = $j('#recent_images_slider');
      var length = main_dom.find('li').length;
      var ul_html = $j('<ul>').addClass('circles');
      for(var i = 0; i < length; i++) {
        ul_html.append($j('<li>').html('&nbsp;').append($j('<a>').click(function() {
          show_sheepit_item(main_dom, $j(this).closest('ul').children().index($j(this).parent()));
        })));
      }
      circles_html = $j('<div>').addClass('centered').append(ul_html);
      main_dom.after(circles_html);
      var timer = setInterval(function() {
        var selected = main_dom.find('li').filter(function() { return $j(this).css('opacity') == '1'; });
        var selected_id = main_dom.children().index(selected);
        var next_selected_id = (selected_id + 1) % length;
        show_sheepit_item(main_dom, next_selected_id);
      }, 5000);
      show_sheepit_item(main_dom, 0);
    });

    return this;
  };
})(jQuery);
