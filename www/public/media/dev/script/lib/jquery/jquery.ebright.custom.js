jQuery(document).ready(function($) {
	//Carousels
	$('.carousel').carousel({
		interval: 5000,
		pause	: 'hover'
	});
	// Sortable list
	$('#ulSorList').mixitup();
	$(".frames > a, .fancybox").fancybox({
		'type': 'image',
	});
	// Fancybox
	/*

	// Fancybox	
	$(".ext-source").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'autoScale'     	: false,
		'type'				: 'iframe',
		'width'				: '50%',
		'height'			: '60%',
		'scrolling'   		: 'no'
	});
*/
	// Masonry
	var container = document.querySelector('#masonryWr');
	var msnry = new Masonry( container, {
	  itemSelector: '.item'
	});
	// Scroll to top
	$().UItoTop({ easingType: 'easeOutQuart' });
	//Animate hover slide
	$( ".animate-hover-slide figure" ).each(function() {
		var animateImgHeight = $(this).find("img").height();
		$(this).find("figcaption").css({"height":animateImgHeight+"px"});
	});
	var date = new Date();
	date.setTime(date.getTime() + (5 * 60 * 1000));

	var sparkline_options = { type: "bar", barColor: "#FFF", barWidth: "4px", height: "45px", barSpacing: "2px", chartRangeMin: 0 };
	$('.sparkline').sparkline('html', sparkline_options);
	$('.accordion-link').click(function() {
		var id = $(this).attr('href');
		$(id).closest('.panel-group').find('.panel-collapse.collapse.in').collapse('hide');
		$(id).collapse('show');
	});

	$('table.sortable th a.sortable').click(function() {
		var td = $(this).closest('td,th');
		var index = $(this).closest('tr').find('td,th').index(td);
		var table = $(this).closest('table');
		var order = $(this).attr('data-default-order');
		var dataType = $(this).attr('data-type');
		if (order === undefined) { order = 'asc' };
		if (dataType === undefined) { dataType = 'string'; };
		if (td.attr('order') == 'asc') order = 'desc';
		if (td.attr('order') == 'desc') order = 'asc';
		td.closest('tr').find('td,th').attr('order','');
		td.closest('tr').find('i.fa').remove();
		td.attr('order', order);
		td.append(' <i class="fa fa-sort-' + order + '">');
		table.find('tr:not(:first)').attr('data-sorted', false);
		while(table.find('tr[data-sorted=false]').length) {
			var biggest_element = null;
			var biggest_value = null;
			table.find('tr[data-sorted=false]').each(function(i, tr) {
				var cell = $($(tr).find('td')[index]);
				var value = cell.attr('data-sort');
				if (value === undefined) {
					value = cell.text();
				}
				if (dataType == 'int') {
					if (value == '') { value = -1; }
					else { value = parseInt(value); }
				}
				if (biggest_value == null || ((order == 'asc') && (value < biggest_value)) || ((order == 'desc') && (value > biggest_value))) {
					biggest_element = $(this);
					biggest_value = value;
				}
			});
			biggest_element.attr('data-sorted', '');
			table.append(biggest_element);
		}
		return false;
	});

});

jQuery(window).resize(function(){
	//Animate hover slide
	$( ".animate-hover-slide figure" ).each(function() {
		var animateImgHeight = $(this).find("img").height();
		$(this).find("figcaption").css({"height":animateImgHeight+"px"});
	});
});
