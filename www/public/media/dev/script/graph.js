function piechart(title_, data_, div_) {
	var data = google.visualization.arrayToDataTable(data_);
	const ChartStyles = getComputedStyle(document.documentElement);
	let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();

	var options = {
		backgroundColor: { fill: "transparent" },
		title: title_,
		titleTextStyle: {
			color: colorChartText,
		},
		legend: {
			textStyle: { color: colorChartText },
		},
		fontName: "'Roboto Condensed', sans-serif !important",
	};
	
	var chart = new google.visualization.PieChart(document.getElementById(div_));
	chart.draw(data, options);
}

function linechart2(title_, data_, div_) {
	var data = google.visualization.arrayToDataTable(data_);

	const ChartStyles = getComputedStyle(document.documentElement);
	let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();
	let colorChartGridline = ChartStyles.getPropertyValue('--color-chart-gridline').trim();
	let colorChartMinorgridline = ChartStyles.getPropertyValue('--color-chart-minorgridline').trim();
	let colorChartLine = ChartStyles.getPropertyValue('--color-chart-line').trim();

	var options = {
		backgroundColor: { fill: "transparent" },
		title: title_,
		isStacked: true,
		titleTextStyle: {
			color: colorChartText,
		},
		legend: {
			textStyle: {color: colorChartText},
		},
		vAxis: {
			minValue: 0,
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
			gridlines: {color: colorChartGridline },
			minorGridlines: {color: colorChartMinorgridline},
		},
		hAxis: {
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
		},
	};

	if (data_[0].length === 2) {
		// data name + actual data
		options['colors'] = [colorChartLine]
	}


	var chart = new google.visualization.LineChart(document.getElementById(div_));
	chart.draw(data, options);
}

function linechart(title_, options_, data_, div_) {
	var data = google.visualization.arrayToDataTable(data_);

	var options = {
		backgroundColor: {fill: "transparent"},
		title: title_,
	};

	for (property in options_) {
		options[property] = options_[property];
	}
	
	var chart = new google.visualization.LineChart(document.getElementById(div_));
	chart.draw(data, options);
}

function areachart(title_, options_, data_, div_) {
	const ChartStyles = getComputedStyle(document.documentElement);
	let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();
	let colorChartGridline = ChartStyles.getPropertyValue('--color-chart-gridline').trim();
	let colorChartMinorgridline = ChartStyles.getPropertyValue('--color-chart-minorgridline').trim();
	let colorChartLine = ChartStyles.getPropertyValue('--color-chart-line').trim();

	var data = google.visualization.arrayToDataTable(data_);
	var options = {
		backgroundColor: { fill: "transparent" },
		title: title_,

		// title: title_,
		// isStacked: true,
		titleTextStyle: {
			color: colorChartText,
		},
		legend: {
			textStyle: {color: colorChartText},
		},
		vAxis: {
			minValue: 0,
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
			gridlines: {color: colorChartGridline },
			minorGridlines: {color: colorChartMinorgridline},
		},
		hAxis: {
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
		},
	};

	if (data_[0].length === 2) {
		// data name + actual data
		options['colors'] = [colorChartLine]
	}

	for (property in options_) {
		options[property] = options_[property];
	}
	// var options_area = {
	// 	'vAxis': { minValue: 0 },
	// 	isStacked: true,
	// 	'backgroundColor': { fill: 'transparent' },
	// 	legend: {position: 'in'}
	// };



	var chart = new google.visualization.AreaChart(document.getElementById(div_));
	chart.draw(data, options);
}


function timeline(title_, columns, rows, div_) {
	var container = document.getElementById(div_);
	var chart = new google.visualization.Timeline(container);
	var dataTable = new google.visualization.DataTable();
	
	for (var i = 0; i < columns.length; i++) {
		dataTable.addColumn(columns[i]);
	}
	dataTable.addRows(rows);
	var options = {
		backgroundColor: { fill: "transparent" },
		timeline: { groupByRowLabel: true},
		title: title_
	};
	
	chart.draw(dataTable, options);
}

function stackedcolumnchart(title_, data_, div_) {
	const ChartStyles = getComputedStyle(document.documentElement);
	let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();
	let colorChartGridline = ChartStyles.getPropertyValue('--color-chart-gridline').trim();
	let colorChartMinorgridline = ChartStyles.getPropertyValue('--color-chart-minorgridline').trim();

	var data = google.visualization.arrayToDataTable(data_);
	var options = {
		backgroundColor: { fill: "transparent" },
		title: title_,
		isStacked: true,
		titleTextStyle: {
			color: colorChartText,
		},
		legend: {
			textStyle: {color: colorChartText},
		},
		vAxis: {
			minValue: 0,
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
			gridlines: {color: colorChartGridline },
			minorGridlines: {color: colorChartMinorgridline},
		},
		hAxis: {
			textStyle: { color: colorChartText },
			titleTextStyle: { color: colorChartText },
		}
	};
	
	var chart = new google.visualization.ColumnChart(document.getElementById(div_));
	chart.draw(data, options);
}

function stackedcolumnchartwithoptions(title_, data_, div_, options) {
	var data = google.visualization.arrayToDataTable(data_);
	var chart = new google.visualization.ColumnChart(document.getElementById(div_));
	chart.draw(data, options);
}
