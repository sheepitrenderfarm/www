function doAddACLUserProjectManage() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_manager_add_form_project').val() + '/acl/manager/user/add/' + $('#project_acl_manager_add_form_user').val(),
		{}
	)
}

function doDelACLUserProjectManager() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_manager_del_form_project').val() + '/acl/manager/user/del/' + $('#project_acl_manager_del_form_user').val(),
		{}
	)
}

function doAddACLUserProjectRenderer() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_renderer_add_form_project').val() + '/acl/renderer/user/add/' + $('#project_acl_renderer_add_form_user').val(),
		{}
	)
}

function doDelACLUserProjectRenderer() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_renderer_del_form_project').val() + '/acl/renderer/user/del/' + $('#project_acl_renderer_del_form_user').val(),
		{}
	)
}

function doAddACLTeamProjectRenderer() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_team_add_form_project').val() + '/acl/renderer/team/add/' + $('#project_acl_team_add_form_user_value').val(),
		{}
	)
}

function doDelACLTeamProjectRenderer() {
	requestReloadOnSuccess(
		'/project/' + $('#project_acl_team_del_form_project').val() + '/acl/renderer/team/del/' + $('#project_acl_team_del_form_user').val(),
		{}
	)
}

function doModifyAttributeFromCheckbox(project_id_, attribute_, id_checkbox_attribute_) {
	var a_value = "0";
	if ($('#'+id_checkbox_attribute_).prop('checked')) {
		a_value = "1";
	}

	if (attribute_ === 'public_render') {
		requestReloadOnSuccess(
			'/project/' + project_id_ + '/visibility/' + a_value,
			{}
		);
	}
	else if (attribute_ === 'generate_mp4') {
		requestReloadOnSuccess(
			'/project/' + project_id_ + '/mp4/' + a_value,
			{}
		);
	}
}

function doModifyComputeMethod(project_id_, attribute_, value_) {
	requestReloadOnSuccess(
		'/project/' + project_id_ + '/computemethod/' + attribute_ + '/' + value_,
		{}
	);
}

function doAskForProjectPartialArchiveFrame(url_) {
	var res = confirm("To create the partial archive, the project will be automatically paused.\n" +
		"Once the partial frame archive file is generated, you will receive an email with instructions on how to download it.\n" +
		"After that, you must resume the project yourself if you want the rendering to continue.\n" +
		"When you resume the project, the partial archive will be destroyed.\n" +
		"Do you really want to ask for a partial archive for this project?");
	if (res == true) {
		$.ajax({
			type: 'POST',
			url: url_,
			success: function(responseText) {
				if (responseText == 'OK') {
					alert('Request successfully made, you will receive an email shorty');
					// force reload of page
					window.location.href = window.location.href
				}
				else {
					alert(responseText);
				}
			},
			onFailure: function() {
				alert(responseText);
			}
		});
	}
}

jQuery(document).ready(function($) {
	$('.filters button.filter').click(function() {
		doShowHideFilter($(this));
		doShowHideProjectList();
	});
	doShowHideProjectList();
});

function doShowHideFilter(dom_filter) {
	$(dom_filter).blur();
	if ($(dom_filter).is('.active')) {
		$(dom_filter).removeClass('active');
	}
	else {
		$(dom_filter).addClass('active');
	}
}
function doShowHideProjectList() {
	var ids_to_show = [];
	$('.filters button.active').each(function(i, filter) {
		ids_to_show = ids_to_show.concat(JSON.parse($(filter).attr('data-ids')));
	});
	$('.projects tr:not(:first)').each(function() {
		var $project_id = parseInt($(this).attr('data-project_id'));
		if ($.inArray($project_id, ids_to_show) >= 0) {
			$(this).show('fast');
		}
		else {
			$(this).hide('fast');
		}
	});
}

function doResetTile(uid) {
	$.ajax({
		type: 'GET',
		url: '/tile/' + uid + '/reset',
		success: function(responseText) {
			if (responseText === 'OK') {
				var tile_div = document.getElementById('tile_'+uid+'_div');
				$('#tile_'+uid+'_div').html("<div style=\"text-align: center; vertical-align: middle;width: "+tile_div.offsetWidth+"px;height: "+tile_div.offsetHeight+"px\">Tile reseted</div>");
			}
			else {
				alert(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function doChangeProjectBinary(id) {
	requestReloadOnSuccess(
		'/project/' + id + '/binary/' + $("#change_renderer").val(),
		{}
	);
}
