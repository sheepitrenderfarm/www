$(document).ready(function($) {
	$('#register_login').change(checkRegisterLogin);
	$('#register_password').change(checkRegisterPassword);
	$('#register_password_confirm').change(checkPasswordConfirm);
	$('#register_email').change(checkEmail);
	$('#resetpassword').submit(function() {
		checkEmail();
		doResetPassword();
	});
});

function checkRegisterLogin() {
	$.ajax('/preuser/check/username/' + $('#register_login').val() , { method: 'get'})
		.done(function(data) {
			if (data === 'OK')
				$('#register_login').css('background','#cfc');
			else
				$('#register_login').css('background', '#fcc');
		})
		.fail(function() {
			$('#register_login').css('background', '#fcc');
		});
}

function checkRegisterPassword() {
	if ($('#register_password').val() !== '') {
		$('#register_password').css('background', '#cfc');
	}
	else {
		$('#register_password').css('background', '#fcc');
	}
}

function checkPasswordConfirm() {
	if ($('#register_password').val() === $('#register_password_confirm').val()) {
		$('#register_password_confirm').css('background', '#cfc');
	}
	else {
		$('#register_password_confirm').css('background', '#fcc');
	}
}

function checkEmail() {
	$.ajax('/preuser/check/email/' + $('#register_email').val(), { method: 'get'})
		.done(function(data) {
			if (data === 'OK')
				$('#register_email').css('background','#cfc');
			else
				$('#register_email').css('background','#fcc');
		})
		.fail(function() {
			$('#register_email').css('background','#fcc');
		})
}

function doRegister() {
	$('#register_login, #register_password, #register_password_confirm, #register_email, #register_submit').prop('disabled', true);

	var data = {
		login: $('#register_login').val(),
		password: $('#register_password').val(),
		password_confirm: $('#register_password_confirm').val(),
		email: $('#register_email').val()
	};

	$.ajax('/user/do_register', { method: 'post', data: data })
		.done(function(data1) {
			if (data1 === 'OK') {
				$('#register_content').html('<div class="alert alert-success">Inscription is done, a confirmation email has been sent to you.</div>');
				$('#register_error_box').html('')
			}
			else {
				$('#register_error_box').html('<div class="alert alert-danger">'+data1+'</div>');
			}
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			$('#register_error_box').html('<div class="alert alert-danger">'+errorThrown+'</div>');
		});

	setTimeout(function() {
		$('#register_login, #register_password, #register_password_confirm, #register_email, #register_submit').prop('disabled', false);
	}, 1000);

	return false;
}

function doResetPassword() {
	$('#register_email, #resetpassword_submit').prop('disabled', true);

	var data = {
		email: $('#register_email').val()
	};

	$.ajax('/user/reset/password', { method: 'post', data: data })
		.done(function(data) {
			if (data == 'OK') {
				$('#resetpassword_error_box').html('<div class="alert alert-success">If you have reset on this address, you will get an link to reset your password.</div');
			} else {
				$('#resetpassword_error_box').html('<div class="alert alert-success">'+data+'</div>');
			}
		})
		.fail(function(data) {
			$('#resetpassword_error_box').html('<div class="alert alert-danger">Error: '+data+'</div>');
		});

	setTimeout(function() {
		$('#register_email, #resetpassword_submit').prop('disabled', false);
	}, 1000);

	return false;
}

function doRemoveAccount() {
	var res = confirm("Are you sure you want to remove your account?\nIT CANNOT BE UNDONE.");
	if (res == false) {
		return;
	}
	
	$.ajax({
		type: 'POST',
		url: '/user/remove_actual',
		data: {
			comment: $('#account_remove_comment').val()
		},
		success: function(responseText) {
			if (responseText == 'OK') {
				alert("Account removed.\nIt might take few minutes for actual removal of account to be effective on every servers");
				window.location.replace("/");
			}
			else {
				alert(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function terms_of_use() {
	if ($('#checkbox_termssofuse').is(':checked')) {
		$('#register_submit').show();
	}
	else {
		$('#register_submit').hide();
	}
}
