function DoChangePassword(path_) {
	requestReloadOnSuccess(
		path_,
		{
			old: $('#update_password_old').val(),
			new: $('#update_password_new').val(),
			verif: $('#update_password_new_verif').val()
		}
	);
}

function DoChangeNotification(mask, id) {
	requestReloadOnSuccess(
		'/user/update/notification/' + mask + '/' + ($('#'+id).is(':checked') ? '1' : '0'),
		{}
	);
}

function DoChangeNotificationUnsubscribe(token, mask, id) {
	requestReloadOnSuccess(
		'/user/unsubscribe/' + token + '/action/' + mask + '/' + ($('#'+id).is(':checked') ? '1' : '0'),
		{}
	);
}

function DoChangeEmail(path_) {
	requestReloadOnSuccess(
		path_,
		{
			email: $('#new_email').val()
		}
	);
}

function DoChangeWebsite(path_) {
	requestReloadOnSuccess(
		path_,
		{
			website: $('#new_website').val()
		}
	);
}

function DoChangeSocialNetwork(path_, user) {
	$.ajax({
		type: 'POST',
		url: path_,
		data: {
			facebook: $('#change_social_networks_facebook').val(),
			twitter: $('#change_social_networks_twitter').val(),
			instagram: $('#change_social_networks_instagram').val(),
			youtube: $('#change_social_networks_youtube').val()
		},
		success: function(responseText) {
			if (responseText === 'OK') {
				alert("Update successful");
				requestThemShowOnId(
					'user_settings_section',
					'/user/'+user+'/edit/settings_section/social_network'
				);
			}
			else {
				alert(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function DoChangeSchedulerValue(mask, id) {
	requestReloadOnSuccess(
		'/user/update/scheduler/' + mask + '/' + ($('#'+id).is(':checked') ? '1' : '0'),
		{}
	);
}

function profileEditSwitchto(target_, values) {
	for (var i = 0; i < values.length; i++) {
		if (values[i] == target_) {
			$('#profile_edit_category_'+values[i]).show();
		}
		else {
			$('#profile_edit_category_'+values[i]).hide();
		}
	}
}
