function doAddProject(i) {
	var start_frame = 0;
	var end_frame = 0;
	var step_frame = 1;
	var split = 1;
	
	var type_job = $("input[type='radio'][name='addproject_change_type_"+i+"']:checked").val();
	if (type_job == 'animation') {
		start_frame = parseInt($('#addproject_animation_start_frame_' + i).val());
		end_frame = parseInt($('#addproject_animation_end_frame_' + i).val());
		step_frame = parseInt($('#addproject_animation_step_frame_' + i).val());
		split = $('#addproject_split_animation_sample_range_value_' + i).val();

		if (start_frame < 0 || step_frame < 0) {
			alert("Start or End frame can not be negative number.")
			return;
		}
	}
	else {
		start_frame = parseInt($('#addproject_singleframe_start_frame_' + i).val());
		split = $('#addproject_split_sample_range_value_' + i).val();
	}
	
	var compute_method = 0;
	if ($('#compute_method_cpu').is(':checked')) {
		compute_method += 1;
	}
	if ($('#compute_method_gpu').is(':checked')) {
		compute_method += 8;
	}
	
// 	var start_frame = parseInt($('#addproject_start_frame_' + i).val());
// 	var end_frame = parseInt($('#addproject_end_frame_' + i).val());
// 	if (((start_frame +1) == end_frame) || (start_frame == end_frame)) {
// 		var res = confirm("A single frame project have been detected.\nThe render farm support this type of project BUT in this case, compositing will not be enabled.\nDo you really want to add this project?");
// 		if (res == false) { return; }
// 	}
	
// 	if ($('#addproject_split_tiles_' + i).is(':checked')) {
// 		var res = confirm("When you split an animation in tile, compositing will not be enabled.\nDo you really want to add this project?");
// 		if (res == false) { return; }
// 	}
	
	if (compute_method == 0) {
		alert("You need to set a compute method before adding a project.");
	}
	else {
		$('#addproject_submit_div_'+i).html('<img src="/media/image/loading-alpha.gif" class="pull-right">');
	
		$('#addproject_start_frame_' + i).prop('disabled', true);
		$('#addproject_end_frame_' + i).prop('disabled', true);
		$('#addproject_max_ram_optional_' + i).prop('disabled', true);
		
		new $.ajax('/project/add_internal',
			{
				method: 'post',
				data: {
					type: type_job,
					compute_method: compute_method,
					executable: $('#addproject_exe_' + i).val(),
					engine: $('#addproject_engine_' + i).val(),
					render_on_gpu_headless: $('#addproject_render_on_gpu_headless_' + i).val(),
					token: $('#token').val(),
					public_render: $('#public_render').is(':checked') ? "1" : "0",
					public_thumbnail: $('#public_thumbnail').is(':checked') ? "1" : "0",
					generate_mp4: $('#generate_mp4').is(':checked') ? "1" : "0",
					start_frame: start_frame,
					end_frame: end_frame,
					step_frame: step_frame,
					archive: $('#addproject_archive_' + i).val(),
					max_ram_optional: $('#addproject_max_ram_optional_' + i).val(),
					path: $('#addproject_path_' + i).val(),
					framerate: $('#addproject_framerate_' + i).val(),
					width: $('#addproject_width_' + i).val(),
					height: $('#addproject_height_' + i).val(),
					split_tiles: $('#addproject_split_tiles_number_' + i).val(),
					split_samples: split,
					use_adaptive_sampling: $('#addproject_use_adaptive_sampling_'+ i ).val(),
					cycles_samples: $('#addproject_cycles_samples_'+ i ).val(),
					samples_pixel: $('#addproject_samples_pixel_'+ i ).val(),
					image_extension: $('#addproject_image_extension_' + i).val()
					
				},
				asynchronous: false
			})
			.done(function(data) {
				if (data.startsWith("http")) {
					document.location.href = data;
				}
				else {
					$('#addproject_content_' + i).html(data);
				}
			})
			.fail(function(data) {
				$('#addproject_error_box_' + i).html('<p class="important">Error ('+ data +')</p>');
			});
	}
}

function doAddProject_split_samples(id, newValue) {
	if (newValue > 1) {
		document.getElementById(id).innerHTML = newValue+" tiles";
	}
	else {
		document.getElementById(id).innerHTML = newValue+" tile";
	}
}

function doAddProject_handle_change_of_type(i, type3) {
	if (type3 == 'animation') {
		$('#addproject_animation_div10_' + i).show();
		$('#addproject_animation_div11_' + i).show();
		$('#addproject_singleframe_div20_' + i).hide();
		$('#addproject_singleframe_div21_' + i).hide();
	}
	else {
		$('#addproject_animation_div10_' + i).hide();
		$('#addproject_animation_div11_' + i).hide();
		$('#addproject_singleframe_div20_' + i).show();
		$('#addproject_singleframe_div21_' + i).show();
	}
}

function addproject_advanced_settings(id) {
	if ($('#checkbox_ad_'+id).prop('checked') == true) {
		$('#checkbox_advanced_option_'+id).show();
	}
	else {
		$('#checkbox_advanced_option_'+id).hide();
	}
}

function addproject_upload_progress_fct(uid) {
	$('#upload_progress_bar').progressbar({
		enable: true
	});
	$('#upload_progress_label').text('0%');
	$('.ui-progressbar-value').css({
		'background': '#EEB0A0',
		'border-right': '#98a0ac'
	});
	
	$('#upload_progress_bar').show();
	$('#td_upload_submit').hide();
	addproject_loopProgressBar(uid);
}

function addproject_loopProgressBar(uid) {
	$.ajax({
		type: 'POST',
		url: '/project/internal/progress',
		data: {'uid' : uid}
	})
	.done(function(data_) {
		$('#upload_progress_bar').progressbar({
			value: parseInt(data_.bytes_processed),
			max: parseInt(data_.content_length),
			change: function() {
				$('#upload_progress_label').text(parseInt($('#upload_progress_bar').progressbar('option', 'value')/$('#upload_progress_bar').progressbar('option', 'max')*100)+'%');
			},
			complete: function() {
				$('#upload_progress_label').text('100%');
			}
		});
	});
	
	setTimeout(function() {
		addproject_loopProgressBar(uid);
	}, 3000);
	
	return true;
}

function doAnalyseUploadedProject(token) {
	$.ajax({
		type: 'GET',
		url: '/project/add_analyse/' + token,
		success: function(responseText) {
			if (isJSON(responseText)) {
				// is JSON
				const o = JSON.parse(responseText);
				if (o.status === 'RETRY') {
					setTimeout(doAnalyseUploadedProject, 5000, token);
					$('#project_add_analyse_status').html("Waiting for analyser to start.");
				}
				else if (o.status === 'PROCESSING') {
					setTimeout(doAnalyseUploadedProject, 5000, token);
					if (o.analysed != 0 && o.total != 0) {
						$('#project_add_analyse_status').html("Project pick up by analyser.<br>Blend analysed: " + o.analysed + " / " + o.total);
					}
					else if (o.total != 0) {
						$('#project_add_analyse_status').html("Project pick up by analyser.<br>Blend file detected: " + o.total);
					}
					else {
						$('#project_add_analyse_status').html("Project pick up by analyser.");
					}
				}
			}
			else {
				// is plain html/text
				$('#project_add_analyse_result').html(responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function calculate_estimate_project() {
	$.ajax('/project/estimator',
		{
			method: 'post',
			data: {
				time: $('#addproject_estimator_time').val(),
				count: $('#addproject_estimator_count_frame').val(),
				device: $("#addproject_estimator_device_form_search_text_value").val()
			},
			asynchronous: false
		})
		.done(function(data) {
			$('#addproject_estimator_result').html(data);
			$('#addproject_estimator_result').show();
		})
		.fail(function(data) {
			alert(data);
		}
	);
}

