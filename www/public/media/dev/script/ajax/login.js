$(document).ready(function($) {
	$('#login_submit').click(function() {
		doLogin();
	});
	$('#login-header_submit').click(function() {
		doHeaderLogin();
	})
});

function doLogin() {
	$('#login_login, #login_password, #login_submit').prop('disabled', true);
	
	const timezone = jstz.determine();

	var data = {
		login: $('#login_login').val(),
		password: $('#login_password').val(),
		timezone: timezone.name()
	};
	$.ajax('/user/authenticate', { method: 'post', data: data })
		.done(function(data) {
			if (data === 'OK') {
				$('#login_error_box').html('<p class="ok">Connected</p>');
				setTimeout(function() {
					var redirect = '/user/profile';
					if ($('#login_redirect').length) {
						redirect = $('#login_redirect').val()
					}
					window.location.href = redirect;
				}, 1000);
			} else {
				$('#login_login').css('background-color','#fcc');
				$('#login_password').css('background-color', '#fcc');
				$('#login_error_box').html('<p class="important">Error: '+data+'</p>');
			}
		})
		.error(function (httpObj, textStatus) {
			$('#login_login').css('background-color','#fcc');
			$('#login_password').css('background-color', '#fcc');

			if (httpObj.status == 401) {
				$('#login_error_box').html('<p style="text-align: center"><span style="color:red">Error: '+httpObj.responseText+'</span></p>');
			}
		})
	
	setTimeout(function() {
		$('#login_login, #login_password, #login_submit').prop('disabled', false);
	}, 1000);
}

function doHeaderLogin() {
	var tz = jstz.determine(); // Timezone
	
	var data = {
		login: $('#login-header_login').val(),
		password: $('#login-header_password').val(),
		timezone: tz.name()
	};
	$.ajax('/user/authenticate', { method: 'post', data: data })
		.done(function(data) {
			if (data === 'OK') {
				window.location.href = '/user/profile';
			} else {
				window.location.href = '/user/signin'
			}
		})
		.fail(function() {
			window.location.href = '/user/signin'
		})
}

function doTogglePasswordVisibility(sourceClick) {
	var spanElement = $('#' + sourceClick);
	spanElement.toggleClass("fa-eye fa-eye-slash");
	var input = $(spanElement.attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	}
	else {
		input.attr("type", "password");
	}
}