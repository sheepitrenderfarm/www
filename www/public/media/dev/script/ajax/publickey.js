function public_key_add(url_, user) {
	if ($('#public_key_add_comment').val() == '') {
		alert('A comment is mandatory');
		return false;
	}
	else {
		requestActionThemShowOnId(
			url_ + $('#public_key_add_comment').val(),
			'user_settings_section',
			'/user/'+user+'/edit/settings_section/render_keys'
		)
	}
}
