function projectAction(id_, action_) {
	if (action_ == 'remove_no_redirect') {
		if (confirm("Are you sure you want to delete this project?\nIt can NOT be undone.") == false) {
			return;
		}
	}
	$('project_job_'+id_+'_div_actions').html('<img src="/media/image/loading-alpha.gif">');
	
	$.ajax({
		type: 'POST',
		url: '/project/' + id_ + '/' + action_,
		data: {},
		success: function(responseText) {
			if (responseText == 'OK') {
				window.location.reload();
			}
			else if (responseText == 'EMPTY') {
				window.location = '/user/profile';
			}
			else {
				alert("Problem "+responseText);
			}
		},
		onFailure: function() {
			alert("Failed to do request");
		}
	});
}

function admin_preuser_delete(login_) {
	requestReloadOnSuccess('/preuser/' + login_ + '/remove ' , {})
}

function admin_preuser_send_registration_confirmation_email(login_) {
	requestReloadOnSuccess('/preuser/' + login_ + '/send_email' , {})
}

function admin_session_block(session_id, blocked) {
	requestReloadOnSuccess('/session/'+ session_id + '/block/' + blocked , {})
}

function admin_session_block_hwid(session_id, reason) {
	requestReloadOnSuccess('/session/'+ session_id + '/block_hwid/' + reason , {})
}

function doBlockSession(id) {
	admin_session_block(id, $('#blocked').val())
}

function doBlockHWID(login, hwid) {
	requestReloadOnSuccess('/hwid/blacklist/add/' + login + '/' + hwid + '/' + $('#blocked_hwid').val()  , {})
}

function doSearchHWID() {
	$('#search_hwid_result').html('<div style="width: 44px;height: 44px;background: url(\'/media/image/fancybox_loading.gif\') center center no-repeat;"></div>');
	$.ajax({
		type: 'GET',
		url: '/hwid/search/' + $('#hwid_search_input').val(),
		data: [],
		success: function(responseHTML) {
			$('#search_hwid_result').html(responseHTML);
			new LazyLoad(); // must be called after all the <img are loaded
		},
	});
}

function admin_hwid_blacklist_delete(id_) {
	requestReloadOnSuccess('/hwid/blacklist/' + id_ + '/remove', {})
}

function admin_gift_create() {
	requestReloadOnSuccess('/gift/add/' + $('#gift_owner').val()  + '/' + $('#gift_value').val() + '/' + ($('#gift_send_email').is(':checked') ? "1" : "0"),
		{ 'comment' : $('#gift_comment').val() }
	);
}

function admin_gift_delete(id_) {
	requestReloadOnSuccess('/gift/delete/' + id_ , {})
}

function admin_award_give() {
	requestReloadOnSuccess('/fulladmin/give_award/' + $('#award_owner').val() + '/' + $('#award_type').val(), {})
}

function admin_mirror_enable(mirror_id, enable) {
	requestReloadOnSuccess('/mirror/' + mirror_id + '/enable/' + enable, {})
}

function admin_shepherd_enable(shepherd_id, enable) {
	requestReloadOnSuccess('/shepherd/' + shepherd_id + '/enable/' + enable, {})
}

function admin_shepherd_allow_new_blend(shepherd_id, enable) {
	requestReloadOnSuccess('/shepherd/' + shepherd_id + '/allow_new_project/' + enable, {})
}

function admin_reset_task(id) {
	requestReloadOnSuccess('/task/' + id +'/reset', {})
}

function admin_reset_task_on_shepherd(id, shepherd_id) {
	requestReloadOnSuccess('/shepherd/' + shepherd_id + '/task/reset/' + id, {})
}

function DoSendMessage(path_) {
	requestReloadOnSuccess(path_, {
		'comment': $('#message').val()
	})
}
