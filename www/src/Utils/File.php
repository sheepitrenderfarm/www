<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Utils;

class File {
    public static function rmdirRecurse(string $path): void {
        if (is_dir($path)) {
            $subelements = scandir($path);
            if (is_array($subelements)) {
                foreach ($subelements as $object) {
                    if ($object != '.' && $object != '..') {
                        if (is_dir($path.'/'.$object)) {
                            self::rmdirRecurse($path.'/'.$object);
                        }
                        else {
                            @unlink($path.'/'.$object);
                        }
                    }
                }
                reset($subelements);
            }
            @rmdir($path);
        }
        else {
            if (is_file($path)) {
                @unlink($path);
            }
        }
    }

    public static function tempdir(string $dir, string $prefix = '', int $mode = 0700): string {
        if (str_ends_with($dir, '/') == false) {
            $dir .= '/';
        }

        do {
            $path = $dir.$prefix.mt_rand(0, 9999999);
        }
        while (!mkdir($path, $mode));

        return $path;
    }


    public static function dirsize(string $dir): int {
        $size = 0;

        foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : self::dirsize($each);
        }

        return $size;
    }
}