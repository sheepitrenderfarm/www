<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Utils;

use App\Service\GlobalInject;
use App\Service\Logger;
use DOMDocument;
use ReflectionClass;
use ReflectionException;

class Misc {
    public static function code(int $nc, string $st = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'): string {
        $l = strlen($st) - 1;
        $r = '';

        while ($nc-- > 0) {
            $r .= $st[rand(0, $l)];
        }

        return $r;
    }

    public static function printArray(mixed $data_, int $n_ = 0): void {
        if (is_array($data_)) {
            foreach ($data_ as $k => $v) {
                for ($i = 0; $i < $n_; $i++) {
                    echo '.';
                }
                echo $k.' => { ';
                if (is_array($v)) {
                    echo "<br>\n";
                    self::printArray($v, $n_ + 4);
                    for ($i = 0; $i < $n_; $i++) {
                        echo '.';
                    }
                }
                else {
                    if (is_bool($v)) {
                        echo $v ? 'true' : 'false';
                    }
                    else {
                        echo $v;
                    }
                }
                echo " }<br>\n";
            }
        }
        else {
            for ($i = 0; $i < $n_; $i++) {
                echo '.';
            }
            echo $data_."<br>\n";
        }
    }

    public static function str2num(string $str_): int {
        // hoho
        $num = 0;
        $str = md5($str_);

        for ($i = 0; $i < strlen($str); $i++) {
            $num += ($i + 1) * ord($str[$i]);
        }

        return $num;
    }

    public static function humanTime(int $time, bool $with_seconds_ = false, bool $with_minutes_ = true, bool $with_hours_ = true): string {
        $ret = '';
        $jours = (int)($time / (3600 * 24));
        $heures = (int)(($time - $jours * 3600 * 24) / 3600);
        $minutes = (int)(($time - $jours * (3600 * 24) - $heures * 3600) / 60);
        if ($jours != 0) {
            if ($jours > 365) {
                $annee = (int)($jours / 365);
                $jours = $jours - $annee * 365;
                $ret .= $annee.'y';
                $ret .= $jours.'d';
            }
            else {
                $ret .= $jours.'d';
            }
        }
        if ($heures != 0 && $with_hours_ == true) {
            if ($heures < 10) {
                $ret .= '0'.$heures.'h';
            }
            else {
                $ret .= $heures.'h';
            }
        }
        if ($with_minutes_ == true) {
            if ($minutes == 0) {
                if ($with_seconds_ == true || strlen($ret) == 0) {
                    $ret .= '00m';
                }
            }
            else {
                if ($minutes < 10) {
                    $ret .= '0'.$minutes.'m';
                }
                else {
                    $ret .= $minutes.'m';
                }
            }
        }

        if ($with_seconds_ == true) {
            $seconds = (int)($time - $jours * (3600 * 24) - $heures * 3600 - $minutes * 60);
            if ($seconds < 10) {
                $ret .= '0';
            }
            $ret .= $seconds.'s';
        }

        if (strlen($ret) > 0 && $ret[0] == '0') {
            $ret = substr($ret, 1);
        }

        return $ret;
    }

    public static function humanSize(int|float $size, bool $maxMB = false, int $base = 1000): string {
        $n = $size;
        if ($n < 0) {
            $n = -1 * $n;
        }

        if ($maxMB && $size > $base * $base) {
            return number_format($size / ($base * $base)).' M';
        }

        if (1 <= ($n / ($base * $base * $base * $base)) && ($n / ($base * $base * $base * $base)) < $base) {
            $str = sprintf("%.1f T", $n / ($base * $base * $base * $base));
        }
        else {
            if (1 < ($n / ($base * $base * $base)) && ($n / ($base * $base * $base)) < $base) {
                $str = sprintf("%.1f G", $n / ($base * $base * $base));
            }
            else {
                if (1 < ($n / ($base * $base)) && ($n / ($base * $base)) < $base) {
                    $str = sprintf("%.1f M", $n / ($base * $base));
                }
                else {
                    if (1 < ($n / $base) && ($n / $base) < $base) {
                        $str = sprintf("%.1f k", $n / $base);
                    }
                    else {
                        $str = sprintf("%.1f", $n);
                    }
                }
            }
        }
        if ($size < 0) {
            $str = '-'.$str;
        }

        return $str;
    }

    public static function loadImputXML(): bool|DomDocument {
        $xml = @file_get_contents('php://input');

        if (is_string($xml) == false || strlen($xml) == 0) {
            Logger::debug('load_imput_xml empty string (input: '.serialize($xml).')');
            return false;
        }

        $dom = new DomDocument('1.0', 'utf-8');
        $buf = @$dom->loadXML($xml);

        if (!$buf) {
            Logger::debug('load_imput_xml failed to load xml (input: '.serialize($xml).')');
            return false;
        }

        if ($dom->hasChildNodes() == false) {
            Logger::debug('load_imput_xml xml has no child (input: '.serialize($xml).')');
            return false;
        }

        return $dom;
    }

    public static function versionToInteger(string $string): int {
        $values = explode('.', $string);
        $ret = 0;
        $i = 100000000;
        foreach ($values as $val) {
            $ret += intval($val) * intval($i);
            $i /= 10000;
        }
        return $ret;
    }

    /**
     * Old machine don't get enough points on render, so they should be boosted
     **/
    public static function powerAlignCpu(float $x): float {
        $config = GlobalInject::getConfigService()->getData();

        if ($x < $config['power']['cpu']['const']) {
            $p = (float)($x) / (float)($config['power']['cpu']['const']);
            return (float)($config['power']['cpu']['const']) * (0.002321083 + 2.205996 * $p - 1.208897 * $p * $p);
        }

        return $x;
    }

    /**
     * Old machine don't get enough points on render, so they should be boosted
     **/
    public static function powerAlignGpu(float $x): float {
        $config = GlobalInject::getConfigService()->getData();

        if ($x < $config['power']['gpu']['const']) {
            $p = (float)($x) / (float)($config['power']['gpu']['const']);
            return (float)($config['power']['gpu']['const']) * (0.002321083 + 2.205996 * $p - 1.208897 * $p * $p);
        }

        return $x;
    }


    public static function isMaskEnabled(int $attribute_, int $mask): bool {
        return (($attribute_ / $mask) % 2 === 1);
    }

    public static function setMaskValue(int $input_, int $mask_, int $value_): int {
        if ($value_ == 1) {
            if ((($input_ / $mask_) % 2 === 1) == false) {
                return $input_ + $mask_;
            }
        }
        else {
            if ((($input_ / $mask_) % 2 === 1) == true) {
                return $input_ - $mask_;
            }
        }
        return $input_;
    }

    /**
     * @return string[]
     */
    public static function findSubClasses(string $root): array {
        $ret = [];
        $classMap = require(__DIR__.'/../../vendor/composer/autoload_classmap.php');
        foreach (array_keys($classMap) as $className) {
            if ($className != $root && str_starts_with($className, $root)) {
                try {
                    $aClass = new ReflectionClass($className);
                    if ($aClass->isAbstract() == false) {
                        $ret [] = $className;
                    }
                }
                catch (ReflectionException $e) {
                }
            }
        }
        return $ret;
    }

    /**
     * Cleanup a server host, 'https://static-fr-eu.sheepit-renderfarm.com' become 'fr-eu'
     */
    public static function humanServerHost(string $url_): string {
        $clean = str_replace(array('.sheepit-renderfarm.com', 'www', 'static-', 'shepherd-'), '', parse_url($url_, PHP_URL_HOST));
        if ($clean == '') {
            // maybe too much...
            $clean = $url_;
        }
        return $clean;
    }

    public static function stringLimit(string $str_, int $limit_): string {
        if (strlen($str_) > $limit_) {
            return substr($str_, 0, $limit_).'...';
        }
        else {
            return $str_;
        }
    }
}
