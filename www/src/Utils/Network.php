<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Utils;

use App\Service\GlobalInject;

class Network {
    /**
     * Get location from an ip.
     * Return false on error.
     * On success return an array of 'country', 'latitude', 'longitude'
     * @return bool|array{country: string, latitude: float, longitude: float}
     */
    public static function getLocationFromIP(?string $ip): bool|array {
        if (is_null($ip) || $ip === "") {
            return false;
        }

        $data = GlobalInject::getMain()->getPersistentCache()->get('ip_'.$ip);
        if (is_array($data) == false) {
            $data = self::getData($ip);
        }
        if (array_key_exists('location', $data)) {
            return $data['location'];
        }
        return false;
    }

    /**
     * Check if the ip is coming from Google.
     * Mostly use for Collab detection
     */
    public static function isGoogleCloud(?string $ip): bool {
        if (is_null($ip) || $ip === "") {
            return false;
        }

        $data = GlobalInject::getMain()->getPersistentCache()->get('ip_'.$ip);
        if (is_array($data) == false) {
            $data = self::getData($ip);
        }
        if (array_key_exists('is_google', $data)) {
            return $data['is_google'];
        }
        return false;
    }

    private static function getData(string $ip): array {
        $data = [];
        $reader = new \GeoIp2\Database\Reader(__DIR__.'/../../public/media/database/GeoLite2-Country.mmdb');

        try {
            $record = $reader->country($ip);
            $isoCode = $record->country->isoCode;
            $countries = json_decode(file_get_contents(__DIR__.'/../../public/media/database/country-location.json'), true);
            foreach ($countries as $country) {
                if ($country['country'] == $isoCode) {
                    $data['location'] = $country;
                }
            }

        }
        catch (\GeoIp2\Exception\AddressNotFoundException $e) {
        }
        catch (\MaxMind\Db\Reader\InvalidDatabaseException $e) {
        }

        $fqdn = @gethostbyaddr($ip);
        if ($fqdn === false) {
            $data['is_google'] = false;
        }
        else {
            $pos = strpos(strtolower($fqdn), 'googleusercontent.com');
            $data['is_google'] = $pos !== false;
        }

        return GlobalInject::getMain()->getPersistentCache()->set('ip_'.$ip, $data);
    }
}