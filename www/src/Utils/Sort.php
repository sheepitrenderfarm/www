<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 */

namespace App\Utils;

use App\Base\BaseProcessingUnit;
use App\Entity\Award;
use App\Entity\Gift;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Tile;
use App\Entity\User;

class Sort {

    public static function sortTeamPoints(array $arr1, array $arr2): int {
        return $arr2['points'] <=> $arr1['points'];
    }

    public static function sortByAttributeScoreINV(?object $o1, ?object $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->score <=> $o2->score;
    }

    public static function sortByAttributeProject(?Tile $o1, ?Tile $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->getFrame()->getProject()->getId() <=> $o2->getFrame()->getProject()->getId();
    }

    public static function sortByAttributeRegistrationTime(?object $o1, ?object $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->getRegistrationTime() <=> $o2->getRegistrationTime();
    }

    public static function sortByAttributeId(?object $o1, ?object $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return strnatcasecmp($o1->getId(), $o2->getId());
    }

    public static function sortReverseByAttributeId(?object $o1, ?object $o2): int {
        if (is_object($o2) == false) {
            return -1;
        }
        if (is_object($o1) == false) {
            return 1;
        }

        return strnatcasecmp($o2->getId(), $o1->getId());
    }

    public static function sortByAttributeLastupdatearchive(?Project $o1, ?Project $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }
        return $o1->getLastUpdateArchive() <=> $o2->getLastUpdateArchive();
    }

    public static function sortByAttributeRequestTime(?Tile $o1, ?Tile $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->getRequestTime() <=> $o2->getRequestTime();
    }

    public static function sortByAttributeValidationTime(?object $o1, ?object $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->getValidationTime() <=> $o2->getValidationTime();
    }

    public static function sortByAttributeExpiration(?Gift $o1, ?Gift $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->getExpiration() <=> $o2->getExpiration();
    }

    public static function sortByAttributePointsEarn(?object $o1, ?object $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o2->getPointsEarn() <=> $o1->getPointsEarn();
    }

    public static function sortAwardByLevel(?Award $o1, ?Award $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o1->level() <=> $o2->level();
    }

    public static function sortAwardByCategory(?Award $o1, ?Award $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        $ret = strnatcasecmp($o1->category(), $o2->category());
        if ($ret == 0) {
            return self::sortAwardByLevel($o1, $o2);
        }
        return $ret;
    }

    public static function sortProjectByOwnerPoints(?Project $o1, ?Project $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o2->getOwnerPointsOnlastupdate() <=> $o1->getOwnerPointsOnlastupdate();
    }

    public static function sortByAttributeHostname(?Session $o1, ?Session $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        if ($o1->getHostname() == $o2->getHostname()) {
            return $o1->getId() <=> $o2->getId();
        }
        else {
            return strnatcasecmp($o1->getHostname(), $o2->getHostname());
        }
    }

    public static function sortTeamMember(User $o1, User $o2): int {
        if ($o1->getTeamContribution() == $o2->getTeamContribution()) {
            return $o2->getPointsEarn() <=> $o1->getPointsEarn();
        }
        else {
            return $o2->getTeamContribution() <=> $o1->getTeamContribution();
        }
    }

    public static function sortPower(?BaseProcessingUnit $o1, ?BaseProcessingUnit $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        return $o2->getPower() <=> $o1->getPower();
    }

    public static function sortLoginSession(?Session $o1, ?Session $o2): int {
        if (is_object($o1) == false) {
            return -1;
        }
        if (is_object($o2) == false) {
            return 1;
        }

        $s1 = strtolower($o1->getUser()->getId());
        $s2 = strtolower($o2->getUser()->getId());
        if ($s1 != $s2) {
            return strcmp($s1, $s2);
        }
        else {
            $d1 = $o1->getComputeDevice();
            $d2 = $o2->getComputeDevice();

            if (is_object($d1) == false) {
                return -1;
            }
            if (is_object($d2) == false) {
                return 1;
            }
            return $d1->getPower() <=> $d2->getPower();
        }
    }

    public static function sortVersion(string $v1, string $v2): int {
        return Misc::versionToInteger($v2) <=> Misc::versionToInteger($v1);
    }
}
