<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Utils;

use App\Service\Logger;
use ZipArchive;

class Zip {
    public static function zipDir(string $path_zip, string $rootdir, string $password = ''): bool {
        Logger::debug(__METHOD__."(destination: $path_zip, source: $rootdir, $password)");

        if (!is_dir($rootdir)) {
            Logger::error(__METHOD__."($path_zip, $rootdir) '$rootdir' is not a directory");
            return false;
        }

        // do not use the ZipArchive api because it corrupts files over 4GB
        $command = 'zip -0 ';
        if ($password != '') {
            $command .= ' -P '.$password;
        }
        $command .= " -r $path_zip .";
        chdir($rootdir);
        exec($command, $output);
        Logger::debug(__METHOD__."'$command' output: ".serialize($output));

        return true;
    }

    public static function zipFile(string $path_zip, string $file, string $password = ''): bool {
        Logger::debug(__METHOD__."(destination: $path_zip, source: $file, $password)");

        if (is_file($file) == false) {
            Logger::error(__METHOD__."($path_zip, $file)  is not a file");
            return false;
        }

        // do not use the ZipArchive api because it corrupts files over 4GB

        chdir(dirname($file));
        $command = 'zip ';
        if ($password != '') {
            $command .= ' -P '.$password;
        }
        $command2 = "$command $path_zip ".str_replace(array(' ', '(', ')', "'"), array('\ ', '\(', '\)', '\\\''), basename($file));
        exec($command2, $output);
        Logger::debug(__METHOD__."'$command2' output: ".serialize($output));
        return true;
    }

    public static function unzip(string $path_zip, string $destdir, string $password = ''): int {
        Logger::debug(__METHOD__."('$path_zip', '$destdir')");

        if (is_dir($destdir) == false) {
            Logger::error(__METHOD__."('$path_zip', '$destdir') destdir is not a directory");
            return -1;
        }

        $zip = new ZipArchive();
        $ret2 = $zip->open($path_zip);
        if ($ret2 !== true) {
            Logger::error("unzip('$path_zip', '$destdir') unable to open file");
            return -2;
        }

        if ($password != '') {
            $zip->setPassword($password);
        }

        $ret3 = $zip->extractTo($destdir);
        $zip->close();
        if ($ret3 != true) {
            Logger::error("unzip('$path_zip', '$destdir') failed to extract file (ret ".serialize($ret3).")");
            return -3;
        }

        return 0;
    }
}