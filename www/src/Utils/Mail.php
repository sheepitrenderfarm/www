<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Utils;

use App\Service\Logger;
use App\Service\Mailer;
use Exception;

class Mail {
    public static function isSpamDomain(string $email): bool {
        $pos = strpos($email, '@');
        if ($pos === false) {
            return false; // should not happen
        }

        $email_domain = substr($email, $pos + 1);
        foreach (explode("\n", file_get_contents(__DIR__.'/spamdomains.txt')) as $a_domain) {
            if ($a_domain == $email_domain) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param ?string[] $attachments
     */
    public static function sendamail(array|string $to, string $subject, string $message, string $template, ?array $attachments = null): bool {
        try {
            $mailer = new Mailer($to, $subject, $message, $attachments, $template);
            return $mailer->send();
        }
        catch (Exception $e) {
            Logger::error("Failed to send email to $to subject: $subject $e");
        }
        return false;
    }
}