<?php

namespace App\DataFixtures;

use App\Constant;
use App\Entity\Blender;
use App\Entity\FrameChessboard;
use App\Entity\FrameLayer;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\TileComputeMethod;
use App\Entity\TilePowerDetection;
use App\Entity\User;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use App\Utils\Misc;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProjectFixtures extends Fixture implements DependentFixtureInterface {

    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, EntityManagerInterface $entityManager) {
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    public function load(ObjectManager $manager): void {
        $projectRepository = $manager->getRepository(Project::class);
        $shepherdRepository = $manager->getRepository(Shepherd::class);
        $blenderRepository = $manager->getRepository(Blender::class);

        $binary = $blenderRepository->getDefault();

        if (is_null($projectRepository->find(Project::COMPUTE_METHOD_PROJECT_ID))) {
            $project = new Project();
            $project->setId(Project::COMPUTE_METHOD_PROJECT_ID);
            $project->setStatus(Constant::WAITING);
            $project->setName('machine');
            $project->setOwner($manager->find(User::class, 'admin'));
            $project->setChunks(['1ebeaa3e8e00e03b1cd14a7fdff17ae6']);
            $project->setExecutable($binary);
            $project->setPassword('');
            $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1) + Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
            $project->setEngine('CYCLES');
            $project->setPath('compute-method.blend');
            $project->setSamples(10);
            $project->setShepherd($shepherdRepository->find(ShepherdInternal::ID));
            $manager->persist($project);

            $projectRepository->addFrames($project, TileComputeMethod::FRAME_NUMBER, TileComputeMethod::FRAME_NUMBER, 1, 1920, 1080, 1, FrameChessboard::class, TileComputeMethod::class, null);
        }

        if (is_null($projectRepository->find(Project::POWER_DETECTION_PROJECT_ID))) {
            $projectPowerDetection = new Project();
            $projectPowerDetection->setStatus(Constant::WAITING);
            $projectPowerDetection->setId(Project::POWER_DETECTION_PROJECT_ID);
            $projectPowerDetection->setName('machine-power');
            $projectPowerDetection->setOwner($manager->find(User::class, 'admin'));
            $projectPowerDetection->setChunks(['49d2402ec54dd1ee6ec00d5a89202f91']);
            $projectPowerDetection->setExecutable($binary);
            $projectPowerDetection->setPassword('');
            $projectPowerDetection->setEngine('CYCLES');
            $projectPowerDetection->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1) + Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
            $projectPowerDetection->setPath('power-detection.blend');
            $projectPowerDetection->setSamples(2147483647); // INFINITE
            $projectPowerDetection->setShepherd($shepherdRepository->find(ShepherdInternal::ID));
            $manager->persist($projectPowerDetection);

            $projectRepository->addFrames($projectPowerDetection, TilePowerDetection::FRAME_NUMBER, TilePowerDetection::FRAME_NUMBER, 1, 1920, 1080, 1, FrameLayer::class, TilePowerDetection::class, null);
        }

        $manager->flush();
    }

    /**
     * @return array<int, string>
     */
    public function getDependencies(): array {
        return array(
            UserFixtures::class,
            ShepherdServerFixtures::class,
            BlenderFixtures::class,
        );
    }
}
