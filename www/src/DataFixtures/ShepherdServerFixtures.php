<?php

namespace App\DataFixtures;

use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Entity\User;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ShepherdServerFixtures extends Fixture implements DependentFixtureInterface {
    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, EntityManagerInterface $entityManager) {
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    public function load(ObjectManager $manager): void {
        if (is_null($manager->getRepository(Shepherd::class)->find(ShepherdMock::ID))) {
            $shepherdServer = new ShepherdMock();
            $shepherdServer->setId(ShepherdMock::ID);
            $shepherdServer->setEnable(true);
            $shepherdServer->setAllowNewBlend(true);
            $shepherdServer->setTimeout(false);
            $shepherdServer->setLocation('US');
            $shepherdServer->setLocationGeographic('32.95 -96.83');
            $shepherdServer->setOwner($manager->getRepository(User::class)->find('admin'));
            $shepherdServer->setMaintainner($manager->getRepository(User::class)->find('admin'));
            $shepherdServer->setCountryOnly('');
            $shepherdServer->setCountryExclude('');
            $shepherdServer->setStorageMax(2000000000000);
            $manager->persist($shepherdServer);
        }

        if (is_null($manager->getRepository(Shepherd::class)->find(ShepherdInternal::ID))) {
            $shepherdServerInternal = new ShepherdInternal();
            $shepherdServerInternal->setId(ShepherdInternal::ID);
            $shepherdServerInternal->setEnable(true);
            $shepherdServerInternal->setAllowNewBlend(true);
            $shepherdServerInternal->setTimeout(false);
            $shepherdServerInternal->setLocation('US');
            $shepherdServerInternal->setLocationGeographic('32.95 -96.83');
            $shepherdServerInternal->setOwner($manager->getRepository(User::class)->find('admin'));
            $shepherdServerInternal->setMaintainner($manager->getRepository(User::class)->find('admin'));
            $shepherdServerInternal->setCountryOnly('');
            $shepherdServerInternal->setCountryExclude('');
            $shepherdServerInternal->setStorageMax(2000000000000);
            $manager->persist($shepherdServerInternal);
        }

        $manager->flush();
    }

    /**
     * @return array<int, string>
     */
    public function getDependencies(): array {
        return array(
            UserFixtures::class,
        );
    }
}
