<?php

namespace App\DataFixtures;

use App\Entity\Blender;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BlenderFixtures extends Fixture {

    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, EntityManagerInterface $entityManager) {
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    public function load(ObjectManager $manager): void {
        $default_version = "blender4.3.1";

        if (is_null($manager->getRepository(Blender::class)->find($default_version))) {
            $blender = new Blender();
            $blender->setId($default_version);
            $blender->setHuman('Blender 4.3');
            $blender->setBfCode('blender403');
            $blender->setEnable(true);
            $manager->persist($blender);
        }

        $manager->flush();
    }
}