<?php

namespace App\DataFixtures;

use App\Entity\AwardEvent500kProjects;
use App\Entity\News;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NewsFixtures extends Fixture {
    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, EntityManagerInterface $entityManager) {
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    public function load(ObjectManager $manager): void {
        // CAUTION not all the news are here

        if (is_null($manager->getRepository(News::class)->find(1355097600))) {
            $news = new News();
            $news->setId(1355097600);
            $news->setTitle('Blender 2.65 available!');
            $news->setImage('/media/image/news/blender265_splash.jpg');
            $news->setContent('<p>Yesterday, the Blender Foundation released the 2.65 version of Blender and it\'s already available on the render farm.</p><p>Look at the release announcement:</p><p>The Blender Foundation and online developer community is proud to present Blender 2.65. We focused on making this the most stable release in the 2.6 cycle yet, fixing over 200 bugs.</p><p>Fire simulation was added along with many improvements in smoke simulation. In Cycles, motion blur, open shading language and anisotropic shading support was added. For mesh modeling, the bevel tool was much improved, a new symmetrize mesh tool was added, and new Laplacian smooth, decimate, and triangulate modifiers are available. </p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1367791201);
            $news->setTitle('Blender 2.66a available!');
            $news->setImage('/media/image/news/blender266a_splash.jpg');
            $news->setContent('<p>Yesterday, the Blender Foundation released the 2.66 version of Blender it\'s already available on the render farm.</p><p>Look at the release announcement:</p><p>The Blender Foundation and online developer community is proud to present Blender 2.66a. This release contains long awaited features like rigid body physics simulation, dynamic topology sculpting and matcap display.</p><p>Other new features include Cycles hair rendering, support for high pixel density displays, much better handling of premultiplied and straight alpha transparency, a vertex bevel tool, a mesh cache modifier and a new SPH particle fluid dynamics solver.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1367938801);
            $news->setTitle('Blender 2.67 available!');
            $news->setImage('/media/image/news/blender267_splash.jpg');
            $news->setContent('<p>Yesterday, the Blender Foundation released the 2.67 version of Blender and it\'s already available on the render farm.</p><p>Look at the release announcement:</p><p>The Blender Foundation and online developer community is proud to present Blender 2.67. New in this release is the Freestyle render engine for non-photorealistic rendering, which can generate 2D line drawings in various styles.</p><p>The paint and sculpt system is now much more consistent across different paint modes and has gained various new features. Motion tracking was made faster, and Cycles now has initial support for subsurface scattering.</p><p>The node system now support more flexible group editing. For developers of external render engine addons it is now possible to support node based materials.</p><p>For 3D printing an addon was added to analyze and export the meshes. </p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1368133201);
            $news->setTitle('Cumulated render time record broken!');
            $news->setImage('/media/image/news/ORAC3_0330.jpg');
            $news->setContent('Congrats to ForgetTheNorm for breaking the cumulated render time record, his project took <strong>10 days 1 hours and 15 min</strong> of render time.You can see the result here <a href="https://vimeo.com/65803189">https://vimeo.com/65803189</a>.');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1369681644);
            $news->setTitle('GPU supported');
            $news->setImage('/media/image/news/GPU.jpg');
            $news->setContent('<p>We are really proud to annonce support of GPU rendering.</p><p>It\'s only supported by the standalone client, the web client will come later. To use your GPU add "--use-gpu=CUDA_0" to the client launch command line.</p><br><p>Edit of 2013-05-30, you can use the GPU via the java applet also.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1374620400);
            $news->setTitle('Blender 2.68a available!');
            $news->setImage('/media/image/news/blender268_splash.jpg');
            $news->setContent('<p>Yesterday the Blender Foundation released  Blender 2.68 and it\'s already available on the render farm.</p><p>Look at the release announcement:</p><p>The Blender Foundation and online developer community is proud to present Blender 2.68.</p><p>Many different mesh modelling tools were added and improved. Cycles render performance was improved on Windows and on GPU\'s. The motion tracker can now automate placement and choice of markers more with new tools. Smoke rendering quality has been improved to reduce blockiness.</p><p>Python scripts and drivers are now disabled by default when loading files. This is for improved security, without this feature malicious .blend files could cause problems. See the Python security documentation for details.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1379438534);
            $news->setTitle('Support of single frame');
            $news->setImage('/media/image/news/chessboard.jpg');
            $news->setContent('We are proud to announce a better support for single frame projects. <br>Now you can render a frame on multiple computers. To do so, on the job creation you have put the same frame number for the start_frame and end_frame then the frame will be split in 64 parts.<br>In this specific type of project, the 20min render time limitation have been removed, now it\'s 22h (64*20min).<br>Currently, the trade-off is that <strong>the compositor is not supported</strong> for those projects.<br>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1383255850);
            $news->setTitle('Blender 2.69 available!');
            $news->setImage('/media/image/news/blender269_splash.jpg');
            $news->setContent('<p>Today the Blender Foundation released the 2.69 version of Blender and it\'s already available on the render farm.</p><p>Look at the release announcement:</p><p>The Blender Foundation and online developer community is proud to present Blender 2.69.</p><p>FBX import is now supported, along with improved export. New mesh modeling tools were added and existing ones improved. Cycles subsurface scattering and hair shading were improved, along with the addition of a new sky model, shading nodes and tone mapping in the viewport. The motion tracker now supports plane tracking, to track and replace flat objects in footage. </p><p><a href="http://www.blender.org/development/release-logs/blender-269/">Official announcement</a></p><p>Presently, SheepIt-renderfarm supports Blender 2.65a, 2.66a, 2.67, 2.68a and now 2.69</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1384214400);
            $news->setTitle('200,000 rendered frames!');
            $news->setImage('/media/image/news/200k.jpg');
            $news->setContent('<p>We are very proud to announce that the 200 000th image has been rendered today.</br>The last few weeks, more and more users have created and successfully rendered projects, nearly half of the rendered frames have been calculated in the last month. </p><p>Thanks to you, renderers, who made it possible. Together you have calculated 1000 projects, you are on average 40 machines to render frames, and you went up to a speed of 460 frames per hour!!</p><p>Here the nice image, made by Yeti who hit the milestone.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1391644800);
            $news->setTitle('500,000 rendered frames!');
            $news->setImage('/media/image/news/500k.jpg');
            $news->setContent('<p>We are very proud to announce that the 500 000th image has been rendered today!</p><p>Thanks to you, renderers, who made this possible! Together you have calculated more than 3000 projects, you are on average 40 machines to render frames with a high peak at 90 machines.<br/>As a thank you, anyone who render a frame in the next days, until Monday, will get a 1000 extras credits.</p><p>We never stop to making improvements on the service, and we have a lot of new features who are coming soon.<br>For example, splitting frames in tiles even for animation will available soon, it will be very useful for small machine who have little cpu power.<br>It will be useful for owners who want to go beyond of the 20-min boundary.</p><p>We have also a nice feature nearly ready who will allow you to share a preview of the output to the renderers.</p><p>It\'s Papa_Dragon who hits the milestone, with this great image. He also shares a video output <a href="http://www.youtube.com/watch?v=4MjOmF0OgQY.">http://www.youtube.com/watch?v=4MjOmF0OgQY.</a></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1395358189);
            $news->setTitle('Blender 2.70a available!');
            $news->setImage('/media/image/news/blender270_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 2.70 version of Blender few days ago and it\'s already available on the render farm.</p><p>Look at the release announcement:<br>The Blender Foundation and online developer community is proud to present Blender 2.70.<br>The first of a new 2.7 series has been released! Work on UI improvements started, and a lot of features were added for Cycles render, motion tracking, animation, modeling, and so on.<br><a href="http://www.blender.org/development/release-logs/blender-270/">Official announcement</a></p><p>Presently, SheepIt-renderfarm supports Blender 2.65a, 2.66a, 2.67, 2.68a, 2.69 and now 2.70.<br>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p><p>There is some great improvements but for SheepIt-renderfarm, the most visible will a change on the calculation of the render\'s progression. Before the 2.70 a percentage was calculated (and something it was not very accurate), now with the 2.70 a <i>remaining time</i> is displayed, we hope you\'ll like it.</p><p><strong>Update on April 12th, we have updated to the bugfix release 2.70a.</strong></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1399766400);
            $news->setTitle('1 million rendered frames!');
            $news->setImage('/media/image/news/1million.jpg');
            $news->setContent('<p>We are very proud to announce that the 1,000,000th image has been rendered today!</br>Thanks to you, renderers, who made this possible! Together you have calculated more than 5500 projects, you are on average 80 machines to render frames with a high peak at 185 machines.</p><p>As a thank you, anyone who render a frame in the next days, until Friday, will get a 2000 extras credits.</p><p>We never stop to making improvements on the service, and we have a lot of new features who are coming soon.<br>A lot of users asked for a forum, so we open one! You can find at <a href="https://www.sheepit-renderfarm.com/forum">https://www.sheepit-renderfarm.com/forum</a> no need to create a specific user for it, as long as you log in on sheepit\'s website first, you will be automatically logged in on the forum.<br>We are still keeping the G+ and Facebook communities, but we are asking you, from now, to ask all the support question on the forum.<br></p><p>We are also thanking Blackschmoll who hit the milestone with this nice image. You can find his work on <a href="http://maxew.deviantart.com">http://maxew.deviantart.com</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1403813453);
            $news->setTitle('Blender 2.71 available!');
            $news->setImage('/media/image/news/blender271_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 2.71 version of Blender today and it\'s already available on Sheepit render farm.</p><p>Look at the release announcement:<br><p>The Blender Foundation and online developer community is proud to present Blender 2.71.<br>Much awaited new features for Cycles renderer include deformation motion blur, fire/smoke and volume rendering and a baking API. <br>User interface now allows for draggable popups and resizable preview widgets. <br>Animation has new interpolation types with “easing equations” presets.<br>Modeling now allows to “split normals” and Sculpting/Painting has new HSL color wheel and constant detail in dyntopo. <br>Game development now allows deactivating logic bricks, multithreaded animations, cast only materials and  “unlimited” action layers.<br>Freestyle NPR rendering has a new textured strokes feature, along with line sorting options.</p><a href="http://www.blender.org/features/2-71/">Official announcement</a></p><p>Presently, SheepIt-renderfarm supports Blender 2.65a, 2.66a, 2.67, 2.68, 2.69, 2.70 and now 2.71.<br>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1412620895);
            $news->setTitle('Blender 2.72b available!');
            $news->setImage('/media/image/news/blender272_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 2.72 version of Blender a few days ago and it\'s available on sheepit render farm.</p><p>Look at the release announcement:<br><p>The Blender Foundation and online developer community is proud to present Blender 2.72. <br>Cycles GPU rendering supports Volume and Subsurface Scattering now. <br>The User Interface comes with optional Pie Menus now and the Tooltips have been improved. For Modeling, a new intersection tool has been added in Edit Mode. <br>Texture painting workflow has been streamlined with easy access to painted images and UV layers. <br>The Compositor now comes with a Sun Beam node.<br> Freestyle NPR rendering is now available with Cycles as well. <br>Additionally, Blender 2.72 features three new addons.</p><a href="http://www.blender.org/features/2-72/">Official announcement</a></p><p>Presently, SheepIt-renderfarm supports Blender 2.65a, 2.66a, 2.67b, 2.68a, 2.69, 2.70a, 2.71 and now 2.72.<br>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p><p>Update October 28th: 2.72b have been added.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1602171952);
            $news->setTitle('Help wanted!');
            $news->setImage('/media/image/news/ms-store.jpg');
            $news->setContent('<p>To help SheepIt continue growing, we need some help.</p><P>We want to add the SheepIt client to the Microsoft Store, but we have a technical issue. Our client is written in Java, but Microsoft doesn\'t like it.<br>They want a Universal Windows Application. So for us, it means a fat binary to wrap around the Java code.<br>But we can\'t make it to work.<br>The goal is to stop using the current auto-extract exe and create a true fat binary where the java code and JVM are included. But from the OS and user points of view, it must be a single exe.<br>If you want to help and have some knowledge about Microsoft please join the <a href="https://discord.io/blender">Discord Server.</ap></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1605436605);
            $news->setTitle('150 million rendered frames!');
            $news->setImage('/media/image/news/150million.jpg');
            $news->setContent('<p>We are very proud to announce that 150,000,000 frames have been rendered on SheepIt Renderfarm</p><p>Since everyone involved with SheepIt is doing it in their spare time, events like this are really heartwarming!<br>Here are some stats for the stats lovers out there:<br>* 1474 years of cumulated render time<br>* 297,000 projects created<br>In 2020, more and more users have joined SheepIt, and a lot more project have been created, which you can see on this chart.<br><a href="/media/image/news/chat-projects-2020.big.jpg"><img src="/media/image/news/chat-projects-2020.mini.jpg"></a></p><p>The first "community event" was in 2013, celebrating <a href="news.php?id=1384214400">200,000 rendered frames</a>.  Back then, it took us two years to reach that milestone.  These days, we render that many frames in 48hrs!</p><p>Our next areas for improvement are bringing the client to the Microsoft Store (See <a href="news.php?id=1602171952">this Call for Help</a>) and improving the SheepIt Network in the US and Asia. If you\'re willing to host a project mirror in these regions for us and/or have a server that meets the <a href="servers.php">requirements</a>, let us know!</p><p>Everyone who has their machine connected is the reason why we are motivated to keep working on Sheepit.</p><p>We would also like to thank Karl, who agreed to share his frame. The image is a still of an upcoming video, on <a href="https://www.youtube.com/user/karlpoyzer">his youtube channel.</a></p><p><i>To thank everyone who has made this possible, a 2,000 point gift will be given to anyone who renders a frame over the next few days (offer ends Monday, November 23rd at 00:00 UTC+0).</i></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1606387645);
            $news->setTitle('Blender 2.91 available!');
            $news->setImage('/media/image/news/blender291_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 2.91 version of Blender <i>yesterday</i> and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/features/releases/2-91/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1622661709);
            $news->setTitle('Blender 2.93 available!');
            $news->setImage('/media/image/news/blender293_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 2.93 version of Blender <i>today</i> and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/features/releases/2-93/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1646856678);
            $news->setTitle('Blender 3.1 is available!');
            $news->setImage('/media/image/news/blender310_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 3.1.0 version of Blender <i>today</i> and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-1/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1654762667);
            $news->setTitle('Blender 3.2 is available!');
            $news->setImage('/media/image/news/blender320_splash.jpg');
            $news->setContent('<p>The Blender Foundation released the 3.2.0 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-2/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $a = new AwardEvent500kProjects();
            $news = new News();
            $news->setId($a->getDate());
            $news->setTitle(' 500k Projects Rendered - Free Render Week Celebration');
            $news->setImage('/media/image/news/500000_projects.jpg');
            $news->setContent("<p>Today we are celebrating a big milestone in Sheepit history--half a million projects rendered!<br>Sheepit went live in January 2008. <strong>For the last few years we have rendered a project every 5min.</strong><br>Thank you for contributing your computing power (and projects) to SheepIt to make this possible!</p><p>We have updated and add new features on the farm:</p><p>If you see a user with a really low-end machine you might want to help him by sharing your computer with that specific user. Before doing that you needed to ask for their render-key to them re-log your machine into their account.<br>To simplify this process you can now \"sponsor\" them directly, by donating the points your earn on your computer to their account.<br>Go on your account, them \"edit profile\" and \"Sponsorship\" to configure the option.</p><p>A lot of people are asking how they can support SheepIt financially. You can already do donations on Patreon for a monthly subscription. You can now also do it via PayPal for a one-time donation or monthly.<br>The goal is for you to support us, but we don't want to be a paid renderfarm or even worst a freemium system, so you will \"contributor award\" (with 10,000 points) on your first donation, but it's a one-time deal</p><p>We always looking for a new fun and serious idea on awards, if you have any ideas share them with us on Discord.</p><p>We would also like to thank Pufutama, who made a drawing for the specific event, you can follow him on <a href=\"https://www.youtube.com/pufutama\">his youtube channel.</a></p><p><i><strong>To thank everyone who has made this possible, a 2,000 points gift will be given to anyone who renders a frame over the next few days</strong> (the offer ends Monday, July 7th at 00:00 UTC+0). <br><strong>Also, all rendering is FREE for the event duration! No points will be deducted from your account for frames rendered during this time.</strong></p>");
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1657623684);
            $news->setTitle('500,000 Projects Rendered - Event results');
            $news->setImage('/media/image/news/500kprojects_results_main.jpg');
            $news->setContent('<p>Our free rendering event in celebration of 500k rendered projects on sheepit is over and was a massive success!</p><p>It was not only a good stress test for how capable this farm is but more importantly showed how many differently talented blender people really like to use sheepit renderfarm for rendering their projects which you guys rocked with  3198 projects during the event!<br>And we couldn\'t have done this without the people who connected their clients to crush over 200k+ frames during the event which we averaged at 768 connected clients which we had peaked at 938!</p><p>Special thanks for our top renderes during the event:<br><a href="/user/hoffbeck20/profile">hoffbeck20</a><br><a href="/user/Dfair/profile">Dfair</a><br><a href="/user/GenesisCloud/profile">GenesisCloud</a><br><br><img  width="100%" src="/media/image/news/500kprojects_results_rank.jpg" /><p>And a big thank you for the people who are supporting us to keep it running the way it does with servers, codeing, Patreon, donations, spreading the word, helping on questions, doing the bits and bops and mehs and anything else that we forgot.</p><p>Thank you all for this amazing event!</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1662440937);
            $news->setTitle('Support for Multilayer EXR');
            $news->setImage('/media/image/news/exr.jpg');
            $news->setContent('<p>With this news, we want to give the community an update about the latest feature that was added to the farm: <strong>Multilayer EXR support!</strong></p><p>Ever since SheepIt saw the light of day back in 2008, it has constantly evolved, with more and more features added and more and more Blender versions coming out and being supported. Even now, 14 years later, we are still looking for ways to improve the service for our users.</p><p>While we appreciate all the people coming to us with new ideas for SheepIt, we often have to reject a feature request on the basis that it does not lend itself to the situation we find ourselves in SheepIt glues together hundreds of different devices across the globe, each with different specifications, network speed and within distinct jurisdictions.One feature we have been asked about <strong>consistently</strong> over the years is support for Multilayer EXR, which allows an artist to store the render passes that get generated during the render process and use them at a later time, for example, for compositing in Nuke or Blender. To give just a simple example, this would allow users to get denoising passes and perform the denoising step later on their own machine.<br>But EXR is helpful beyond just that, as it allows for greater bit depth than most common output formats like PNG.</p><p>The reason we have always put off allowing EXR on the farm is the file size of the frames, which gets bigger with each additional layer added to the final result. We also try not to lose focus on our target group: artists and hobbyists who cannot afford a professional render farm. It was always our stance that once you care about advanced features like EXR, you are likely in a professional setting and therefore should have the funds needed to pay for a render elsewhere.<p><p>However, with the introduction of support for lossy codecs in Blender, file sizes became more controllable, sometimes even outperforming PNG. And in our survey that we put out at the beginning of the year, people once again showed great interest in EXR support.<p><p><i>We\'re happy to announce today that we\'ve been working on this over the last couple of months and that our investment has paid off!</i></p><p>Before you run off and try it out, please be aware of the <a href="https://www.sheepit-renderfarm.com/faq#exr">constraints</a> we\'ve put in place for the reasons outlined above. Keep in mind that those might change over time as we see how people use this feature.<br>Therefore we would love to hear your feedback on the current implementation! You can reach out to us via the <a href="https://www.sheepit-renderfarm.com/forum">forum</a> or on <a href="https://discord.io/blender">Discord</a> or <a href="https://www.reddit.com/r/sheepit/">Reddit</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1662722979);
            $news->setTitle('Blender 3.3 is available!');
            $news->setImage('/media/image/news/blender330_splash.jpg');
            $news->setContent('<p>Yesterday, the Blender Foundation released the 3.3 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-3/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1669793777);
            $news->setTitle('Happy Sheepmess!');
            $news->setImage('/media/image/news/christmas_2022.jpg');
            $news->setContent('<p>It\'s the last month of the year and what greater way to celebrate with all of you for being such a nice community other than with some amazing daily gifts from our handmade (with love and cookies) advent calendar!<br>Come back every day to the homepage and open up a door to get your gift because you can only open the door for today!</p><p>You will also get a special community award on December 25th if you opened up at least 10 doors<i> (the doors that you opened are marked with a gift next to day number).</i></p><p>The community event awards can only be earned for a very limited time, check out your profile page to see the full list.</p><p>Thanks again for being part of SheepIt community!</p><p><i>Christmas event images generated through AI tools by <a href=/user/Foxel/profile">Foxel</></i></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1670444120);
            $news->setTitle('Blender 3.4 is available!');
            $news->setImage('/media/image/news/blender340_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 3.4 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-4/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1673627791);
            $news->setTitle('SheepIt in 2023');
            $news->setImage('/media/image/news/2023_newyear_main.jpg');
            $news->setContent('<p>Happy new year!</p><p>Let\'s hope you are still holding up your new year\'s resolution ;)<br>We are holding ours, in giving you the best time on SheepIt!</p><p>Speaking of which, the Christmas event went very well thanks to all of you. We had more people participating in the event than expected and managed to peak at over 700 claims per day!</p><p><img src="/media/image/news/2023_newyear_advent.jpg"  width="100%"  /></p><p>Once you are done clapping, let us take a look back at 2022 and the many changes SheepIt had that might not be known by everyone to keep everything operating smoothly, like the 134 GitLab commits for the client and a little of 532 commits for the servers! </p><p>In case you are wondering why the server section had so many commits and changes then let me tell you that it\'s not only for optimizations and performance but also because we moved our main server from Germany to the USA with a much more performant server since our old one could barely crunch through 3000 frames per hour and the new one can handle 8000 frames per hour without any sweat, and we don\'t even know the upper limit of it yet and that\'s allthanks to the help of <a href="https://www.shareandmakeaware.org">Shareandmakeaware</a> a non-profit that helped us out with not only their servers but in many ways too, including a lot of their time to make it possible for SheepIt to handle the massive growth of users, projects, and frames  where last year we peaked at 90 simultaneous projects in December and 280,000 frames per day in July with an average of 160,000 fpd, so make sure to check them out and support them with some <a href="https://www.shareandmakeaware.org/donate/">donations</a> which are even tax-deductible in the USA!</p><p>And if you happen to have a server in the Pacific/Asia area for us then please let us know!</p><p>We also had many other changes and newly added features that you might have already used like the support for EXR and the advent calendar. For Mac’s we have developed a special DMG client, but we are currently not able to give support for the Apple M1 because we do not have a device to test, debug and run code on, if you could support or donate us a M1 device to work with then please contact us.</p><p> Up until January SheepIt was still using CUDA as a backend, even though Optix had already been introduced back in 2019. Optix is an extension of CUDA that allows for the effective use of modern hardware like for hardware-accelerated raytracing.<br> We have been monitoring the composition of devices on the farm and feel confident by now that enough machines can use Optix. So in January of this year, we flicked the switch. This brought a significant boost in render power to many Nvidia GPUs:</p><p><img src="/media/image/news/2023_newyear_cuda.jpg" style="width:100%" /></p><p>Looking at the future in 2023, and we already got some changes lined up like dropping the support for Blender 2.8 and AMD GPU’s, one is getting retired and the other is not worth our time since we only have 10 or fewer AMD GPU’s connected at a time and therefore not worth the time for code maintenance.</p><p>And finally, some official Sheepit merch is planned and many other things to improve your experience, if you would like to help us with that then please take some time to fill out the survey: <a href="https://forms.gle/gdbxenVDLSDR3DcX9">https://forms.gle/gdbxenVDLSDR3DcX9</a><br></p><p>Thanks for all your support so far, and we wish you a great 2023!</p> ');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1680171820);
            $news->setTitle('Blender 3.5 is available!');
            $news->setImage('/media/image/news/blender350_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 3.5 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-5/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);


            $news = new News();
            $news->setId(1687881581);
            $news->setTitle('Blender 3.6 is available!');
            $news->setImage('/media/image/news/blender360_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 3.6 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/3-6/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1698323860);
            $news->setTitle('Blender conference 2023 event');
            $news->setImage('/media/image/news/bconf2023.jpg');
            $news->setContent('<p>To celebrate the <a href="https://conference.blender.org/2023/">Blender Conference</a> until October 29th, 11:59PM Amsterdam time, all projects will cost nothing and you can add up to 5 projects simultaneously!<br>Renderer will still earn points.</p><p>Thanks again for using SheepIt!</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1698775981);
            $news->setTitle('Goodies store');
            $news->setImage('/media/image/news/spreadshirt.jpg');
            $news->setContent('<p>We are thrilled to announce the grand opening of our brand-new Spreadshirt store.<br>Don\'t miss out on the opportunity to upgrade your wardrobe with our exclusive merchandise.</p><p><a href="https://sheepit-renderfarm-usa.myspreadshop.com">Spreadshirt store</a></p>');
            $news->setHomepage(true);
            $manager->persist($news);

            $news = new News();
            $news->setId(1699984405);
            $news->setTitle('Blender 4.0 is available!');
            $news->setImage('/media/image/news/blender400_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 4.0 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/4-0/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1701385200);
            $news->setTitle('Happy Sheepmess!');
            $news->setImage('/media/image/news/christmas_2023.jpg');
            $news->setContent('<p>It\'s the last month of the year and what greater way to celebrate with all of you for being such a nice community other than with some amazing daily gifts from our handmade (with love and cookies) advent calendar!<br>Come back every day to the homepage and open up a door to get your gift because you can only open the door for today!</p><p>You will also get a special community award on December 25th if you opened up at least 10 doors<i> (the doors that you opened are marked with a gift next to day number).</i></p><p><i>Event start on December 1st</i></p><p>The community event awards can only be earned for a very limited time, check out your profile page to see the full list.</p><p>Thanks again for being part of SheepIt community!</p><p><i>Christmas event images generated through AI tools</i></p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1711465057);
            $news->setTitle('Increase project size');
            $news->setImage('/media/image/news/project-size-2GB.jpg');
            $news->setContent('<p>Being an internet render farm, the question of data transfer is always delicate, that\'s why for long time we had to limit the project size to 500MB, last year it was increased to 750MB but it\'s now not enough.</p><p>At first the limitation was due to the SheepIt infrastructure since we have migrated to Cloudflare we can handle more traffic.</p><p>In 2024, most of users have high speed internet connection, that\'s why <strong>we are proud to announce the increase of project size from 750MB to 2GB.</strong></p><p>If you have a metered connection or a slow speed to you can choose on the client side to only render small size project (under 750MB)<br><img src="/media/image/news/project-size-2GB-client.jpg"></p><p>Happy rendering !</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1718964000);
            $news->setTitle('Happy solstice!');
            $news->setImage('/media/image/news/event_2024_june.jpg');
            $news->setContent('<p>We believe every moment is a good moment to express our gratitude to the amazing SheepIt community. This week, we\'re celebrating the solstice, and we\'d like to make it extra special for you!</p><p>From now until next Friday, every user who renders a frame will receive a special community award along with a surprise reward as a token of our appreciation.</p><p>Thank you for sharing your computing power and helping keep the SheepIt project alive!</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1721298849);
            $news->setTitle('Blender 4.2 is available!');
            $news->setImage('/media/image/news/blender420_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 4.2 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/4-2/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);

            $news = new News();
            $news->setId(1732035563);
            $news->setTitle('Blender 4.3 is available!');
            $news->setImage('/media/image/news/blender430_splash.jpg');
            $news->setContent('<p>Today, the Blender Foundation released the 4.3 version of Blender and it\'s already available on SheepIt Render Farm.</p><p>This version brings a lot of cool features, checkout the <a href="https://www.blender.org/download/releases/4-3/">Official announcement</a>.</p><p>You can see all binaries available on this page: <a href="/home/binaries">binaries</a>.</p>');
            $news->setHomepage(false);
            $manager->persist($news);
        }

        $manager->flush();
    }
}
