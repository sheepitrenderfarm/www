<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserFixtures extends Fixture {
    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, EntityManagerInterface $entityManager) {
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    public function load(ObjectManager $manager): void {
        $this->createUser('admin', 17923, $manager);
        $this->createUser('renderer', 3, $manager);
        $this->createUser(User::REMOVED_USER_ID, 0, $manager);

        $manager->flush();
    }

    private function createUser(string $login, int $level, ObjectManager $manager): void {
        if (is_null($manager->getRepository(User::class)->find($login))) {
            $user = new User();
            $user->setId($login);
            $user->setPassword(User::generateHashedPassword($user->getId(), 'sheepit'));
            $user->setEmail($login.'@localhost');
            $user->setLevel($level);
            $user->setRegistrationTime(time());
            $user->setAffiliate('');
            $user->setSponsorReceive(false);
            $user->setSponsorGive(false);
            $manager->persist($user);
        }
    }
}
