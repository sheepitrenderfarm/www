<?php

namespace App\AdventCalendar\ui;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Entity\User;
use App\Repository\AdventCalendarClaimedDayRepository;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdventCalendar {

    /**
     * Logged (or anonymous) user
     */
    private ?User $user;

    private AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository;
    private UrlGeneratorInterface $router;
    private EntityManagerInterface $entityManager;
    private HTML $HTML;

    public function __construct(Security $security, HTML $HTML, EntityManagerInterface $entityManager, AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository, UrlGeneratorInterface $router) {
        /** @var ?User $user */
        $user = $security->getUser();
        $this->user = $user;

        $this->router = $router;
        $this->HTML = $HTML;
        $this->entityManager = $entityManager;
        $this->adventCalendarClaimedDayRepository = $adventCalendarClaimedDayRepository;
    }

    public static function isActive(): bool {
        return date('m') == 12 && date('d') <= 25;
    }

    public function htmlCalendar(string $claimed_text): string {
        $html = '';
        $html .= '<section class="slice advent-bg">';
        $html .= '<div class="w-section inverse">';
        $html .= '<div class="container">';
        $html .= '<div class="advent">';
        $html .= '<div class="toppart blabla ac-title"> Meeehhhhry Christmas! </div>';
        $html .= '<div class="calender">';
        $html .= '<div class="windows">';

        if ($claimed_text != '') {
            $html .= '<div class="ac-popupwindow" id="advent_popup">';
            $html .= '<p>';
            $html .= $claimed_text;
            $html .= '</p>';
            $html .= '<div class="btn btn-two" onclick="$(\'#advent_popup\').hide(); return true;" >Close window</div>';
            $html .= '</div>';
        }

        for ($i = 1; $i < 25; $i++) {
            $html .= $this->htmlBox($i);
        }
        $html .= '<img src="/media/image/advent-calendar_2024.jpg" alt="" />';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="bottompart blabla">';
        $html .= '<span class="ac-title">';
        $html .= 'Open all doors to get some amazing gifts!';
        $html .= '</span>';
        $html .= '<span class="ac-desc">';
        $html .= 'You can only open the door for today, so make sure to come back ';
        $html .= 'everyday so you don\'t miss all the meeehhhhgical gifts and the ';
        $html .= 'Christmas achievement for opening at least 10 doors!<br>';
        $html .= '</span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</section>';
        return $html;
    }

    public function htmlBox(int $i): string {
        $css = '';
        $css .= $this->isOpen($i) ? 'open ' : '';
        $css .= $this->isToday($i) ? 'today ' : '';
        $css .= $this->isClaimed($i) ? 'claimed ' : '';

        $onclick = $this->isToday($i) && $this->isClaimed($i) == false ? 'onclick="requestThemShowOnId(\'advent_calendar\', \''.$this->router->generate('app_adventcalendarclaimedday_claim').'\'); return false"' : '';

        $class_name = '\App\AdventCalendar\gift\AdventCalendarGiftDay'.$i;
        /** @var AdventCalendarGiftDay $gift */
        $gift = new $class_name($this->user, $this->entityManager);

        return sprintf('
        <div id="ac-d%d" class="door %s" %s>
                  <p class="ac-day">%d</p>
                  <p class="ac-text">%s</p>
                </div>',
            $i, $css, $onclick, $i, $this->isInFuture($i) ? '' : $gift->getText()
        );

    }

    private function isToday(int $i): bool {
        return $this->getDay() == $i && $this->getMonth() == 12;
    }

    private function isInFuture(int $i): bool {
        return $i > $this->getDay() && $this->getMonth() == 12;
    }

    private function isOpen(int $i): bool {
        $in_past = $i < $this->getDay();

        return $this->getMonth() == 12 && ($this->isClaimed($i) || $in_past);
    }

    private function isClaimed(int $i): bool {
        // TODO: slow!
        // TODO: december, for now every months

        if (is_null($this->user)) {
            return false;
        }

        $date = (new \DateTime())->setDate((int)date('Y'), 12, $i);
        return is_object($this->adventCalendarClaimedDayRepository->findOneBy(['user' => $this->user->getId(), 'date' => $date]));
    }

    private function getDay(): int {
        return intval($this->HTML->getTimeWithLocalTimeZone('d', time()));
    }

    private function getMonth(): int {
        return intval($this->HTML->getTimeWithLocalTimeZone('m', time()));
    }
}
