<?php

namespace App\AdventCalendar\gift;

use App\Service\GlobalInject;

class AdventCalendarGiftDay10 implements AdventCalendarGiftDay {

    public function getText(): string {
        return '<a href="'.$this->getUrl().'">SheepIt Discord</a>';
    }

    public function onClaim(): string {
        return sprintf('Go on <a href="%s">%s</a> to try our Discord server', $this->getUrl(), $this->getUrl());
    }

    private function getUrl(): string {
        $config = GlobalInject::getConfigService()->getData();
        return $config['site']['communities']['Discord']['link'];
    }
}
