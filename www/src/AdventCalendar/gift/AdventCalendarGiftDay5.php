<?php

namespace App\AdventCalendar\gift;

use App\Service\GlobalInject;

class AdventCalendarGiftDay5 implements AdventCalendarGiftDay {

    public function getText(): string {
        $url = GlobalInject::getConfigService()->getData()['donation']['spreadshirt']['ID'];
        return '<a href="'.$url.'">spreadshirt store</a>';
    }

    public function onClaim(): string {
        $url = GlobalInject::getConfigService()->getData()['donation']['spreadshirt']['ID'];
        return 'Go on <a href="'.$url.'">'.$url.'</a> to try our spreadshirt store';
    }
}
