<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay2 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 150000;
    }
}
