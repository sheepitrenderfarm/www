<?php

namespace App\AdventCalendar\gift\type;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Entity\RenderDay;
use App\Entity\User;
use App\Repository\RenderDayRepository;
use Doctrine\ORM\EntityManagerInterface;

class AdventCalendarFreeRenderDay implements AdventCalendarGiftDay {
    protected ?User $user;
    protected EntityManagerInterface $entityManager;
    protected RenderDayRepository $renderDayRepository;

    public function __construct(?User $user, EntityManagerInterface $entityManager) {
        $this->user = $user;
        $this->entityManager = $entityManager;
        $this->renderDayRepository = $entityManager->getRepository(RenderDay::class);
    }

    public function getText(): string {
        return 'Free render day in the past';
    }

    public function onClaim(): string {
        if (is_object($this->user)) {
            $date = $this->renderDayRepository->fillUpCheeseHole($this->user);

            return sprintf("Do you like cheese with or without holes?<br> Here is a free render day to plug the hole on your render streak for the %s.<br><i style=\"font-size: 2.0rem;\">It might take up to 48h to update your max consecutive render day value.</i>", $date->format('F jS, Y'));
        }
        else {
            return 'Need to log in to claim a gift';
        }
    }
}