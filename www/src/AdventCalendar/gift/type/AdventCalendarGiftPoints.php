<?php

namespace App\AdventCalendar\gift\type;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

abstract class AdventCalendarGiftPoints implements AdventCalendarGiftDay {
    protected ?User $user;
    protected EntityManagerInterface $entityManager;

    public function __construct(?User $user, EntityManagerInterface $entityManager) {
        $this->user = $user;
        $this->entityManager = $entityManager;
    }

    abstract public function getReward(): int;

    public function getText(): string {
        return number_format($this->getReward()).' points';
    }

    public function onClaim(): string {
        if (is_object($this->user)) {
            $this->user->setPoints($this->user->getPoints() + $this->getReward());
            $this->entityManager->flush($this->user);

            return "You have claimed your gift of ".number_format($this->getReward())." points.<br>You have now ".number_format($this->user->getPoints()).' points.';
        }
        else {
            return 'Need to log in to claim a gift';
        }
    }
}