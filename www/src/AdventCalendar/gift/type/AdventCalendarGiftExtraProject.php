<?php

namespace App\AdventCalendar\gift\type;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

abstract class AdventCalendarGiftExtraProject implements AdventCalendarGiftDay {
    protected ?User $user;
    protected EntityManagerInterface $entityManager;

    public function __construct(?User $user, EntityManagerInterface $entityManager) {
        $this->user = $user;
        $this->entityManager = $entityManager;
    }

    abstract public function getReward(): int;

    public function getText(): string {
        return 'Can add '.number_format($this->getReward()).' projects at once';
    }

    public function onClaim(): string {
        if (is_object($this->user)) {
            return "You have claimed your gift.\nYou can add ".number_format($this->getReward())." projects  at once until the end of event";
        }
        else {
            return 'Need to log in to claim a gift';
        }
    }
}