<?php

namespace App\AdventCalendar\gift\type;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

abstract class AdventCalendarGiftRatioRendererPoints implements AdventCalendarGiftDay {
    protected ?User $user;
    protected EntityManagerInterface $entityManager;

    public function __construct(?User $user, EntityManagerInterface $entityManager) {
        $this->user = $user;
        $this->entityManager = $entityManager;
    }

    abstract public function getReward(): float;

    public function getText(): string {
        return sprintf('Earn %.1fx more points while rendering', $this->getReward());
    }

    public function onClaim(): string {
        if (is_object($this->user)) {
            $this->user->setPoints($this->user->getPoints() + $this->getReward());
            $this->entityManager->flush($this->user);

            return sprintf("You have claimed your gift.\n You will earn %.1fx more points while rendering", $this->getReward());
        }
        else {
            return 'Need to log in to claim a gift';
        }
    }
}