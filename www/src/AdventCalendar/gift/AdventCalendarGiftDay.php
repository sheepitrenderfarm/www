<?php

namespace App\AdventCalendar\gift;

interface AdventCalendarGiftDay {
    public function getText(): string;

    public function onClaim(): string;

}