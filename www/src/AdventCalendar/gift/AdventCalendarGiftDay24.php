<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay24 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 1000000;
    }
}
