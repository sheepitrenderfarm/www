<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay12 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 250000;
    }
}
