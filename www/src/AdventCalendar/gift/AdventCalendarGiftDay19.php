<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay19 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 300000;
    }
}
