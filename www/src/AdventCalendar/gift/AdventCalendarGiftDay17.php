<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay17 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 250000;
    }
}
