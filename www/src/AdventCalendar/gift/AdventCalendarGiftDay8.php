<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftRatioRendererPoints;

class AdventCalendarGiftDay8 extends AdventCalendarGiftRatioRendererPoints {

    public function getReward(): float {
        return 1.5;
    }
}
