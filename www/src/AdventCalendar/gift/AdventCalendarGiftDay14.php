<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftRatioOwnerPoints;

class AdventCalendarGiftDay14 extends AdventCalendarGiftRatioOwnerPoints {

    public function getReward(): float {
        return 2.5;
    }
}
