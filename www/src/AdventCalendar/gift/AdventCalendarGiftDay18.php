<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftRatioRendererPoints;

class AdventCalendarGiftDay18 extends AdventCalendarGiftRatioRendererPoints {

    public function getReward(): float {
        return 3.0;
    }
}
