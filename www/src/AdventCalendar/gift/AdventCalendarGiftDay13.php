<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftExtraProject;

class AdventCalendarGiftDay13 extends AdventCalendarGiftExtraProject {

    public function getReward(): int {
        return 5;
    }
}
