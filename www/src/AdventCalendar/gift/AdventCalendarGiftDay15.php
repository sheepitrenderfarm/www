<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay15 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 80000;
    }
}
