<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay23 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 190000;
    }
}
