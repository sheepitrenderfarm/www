<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay9 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 200000;
    }
}
