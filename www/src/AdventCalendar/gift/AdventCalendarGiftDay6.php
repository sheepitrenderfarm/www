<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftRatioOwnerPoints;

class AdventCalendarGiftDay6 extends AdventCalendarGiftRatioOwnerPoints {

    public function getReward(): float {
        return 2.0;
    }
}