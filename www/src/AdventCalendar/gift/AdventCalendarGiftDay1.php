<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftPoints;

class AdventCalendarGiftDay1 extends AdventCalendarGiftPoints {

    public function getReward(): int {
        return 100000;
    }
}