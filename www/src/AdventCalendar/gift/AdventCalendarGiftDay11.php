<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftRatioRendererPoints;

class AdventCalendarGiftDay11 extends AdventCalendarGiftRatioRendererPoints {

    public function getReward(): float {
        return 2.5;
    }
}
