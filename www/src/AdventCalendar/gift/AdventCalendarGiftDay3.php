<?php

namespace App\AdventCalendar\gift;

use App\AdventCalendar\gift\type\AdventCalendarGiftExtraProject;

class AdventCalendarGiftDay3 extends AdventCalendarGiftExtraProject {

    public function getReward(): int {
        return 3;
    }
}
