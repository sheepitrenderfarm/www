<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Service\ConfigService;
use App\Service\Daemon;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:minutely',
    description: 'Minutely cron',
)]
class CronMinutelyCommand extends Command {
    private Daemon $daemon;
    private ConfigService $configService;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, Daemon $daemon) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->daemon = $daemon;
        $this->configService = $configService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $config = $this->configService->getData();

        if ($config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        if ($this->daemon->canWork()) {
            $end = time() + $config['daemon']['max_runningtime'];
            do {
                $status = $this->daemon->workOneTime();
            }
            while ($status && time() < $end);
        }
        else {
//         Logger::debug('cron error can not work'); // most likely because the cron is already running
        }
        $this->daemon->shutdown();

        return Command::SUCCESS;
    }
}
