<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\Tile;
use App\Entity\StatsServer;
use App\Repository\ShepherdServerRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:5minutes',
    description: '5 minutes cron',
)]
class Cron5MinutesCommand extends Command {
    private array $config;
    private EntityManagerInterface $entityManager;
    private Main $main;
    private ShepherdServerRepository $shepherdServerRepository;


    public function __construct(EntityManagerInterface $entityManager, Main $main, ShepherdServerRepository $shepherdServerRepository, ConfigService $configService, UrlGeneratorInterface $router) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->entityManager = $entityManager;
        $this->main = $main;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->config = $configService->getData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $now = time();

        $this->main->removeIdleSession();

        $stats = StatsServer::getCurrent();
        $stats->setId($now);
        $this->entityManager->persist($stats);
        $this->entityManager->flush($stats);

        // shepherds
        foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
            $shepherd->checkIfStillTimeout();
            $shepherd->updateStats($now);
        }

        $this->main->getPersistentCache()->set('count_rendered_frame_last_12hours', $this->entityManager->getRepository(Tile::class)->getCountRenderedFramesSinceLast12hours());

        return Command::SUCCESS;
    }
}
