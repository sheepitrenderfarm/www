<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Repository\RenderDayRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:renderday',
    description: 'Give a render day for today to everyone',
)]
class RenderDayForTodayCommand extends Command {
    private RenderDayRepository $renderDayRepository;
    private UserRepository $userRepository;
    private Main $main;


    public function __construct(Main $main, EntityManagerInterface $entityManager, ConfigService $configService, UrlGeneratorInterface $router, RenderDayRepository $renderDayRepository, UserRepository $userRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->userRepository = $userRepository;
        $this->renderDayRepository = $renderDayRepository;
        $this->main = $main;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->main->getBestRenderersFromCache() as $bestRenderer) {
            $user = $this->userRepository->find($bestRenderer->getLogin());
            if (is_object($user)) {
                $this->renderDayRepository->addFor($user);
            }
        }


        return Command::SUCCESS;
    }
}
