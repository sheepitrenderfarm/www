<?php
/**
 * Copyright (C) 2025 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\Message;
use App\Repository\SessionRepository;
use App\Service\ConfigService;
use App\Service\DiscordWebHook;
use App\Service\GlobalInject;
use App\Service\Main;
use App\Utils\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:message_dead_session',
    description: 'Send a message to every dead session owner',
)]
class MessageDeadSessionCommand extends Command {
    private array $config;
    private UrlGeneratorInterface $router;
    private DiscordWebHook $discord;
    private EntityManagerInterface $entityManager;
    private SessionRepository $sessionRepository;


    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, SessionRepository $sessionRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->discord = $main->getDiscordWebHook();
        $this->sessionRepository = $sessionRepository;
    }

    protected function configure(): void {
        $this->addArgument('day', InputArgument::REQUIRED, 'Duration in days');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $day = (int)$input->getArgument('day');
        $start = time() - $day * 86400;

        $sent = [];
        foreach ($this->sessionRepository->findAll() as $session) {
            if ($session->getLastvalidatedframe() != 0 && $session->getLastvalidatedframe() < $start && in_array($session->getUser()->getId(), $sent) == false) {
                $sent [] = $session->getUser()->getId();
                $content = 'Hello,<br> I\'m an admin on SheepIt-renderfarm.<br><br>';
                $content .= 'Your machine has not rendered anything in few days, you might want to check it to found out why no project has be completed.<br><br>Thanks for sharing your hardware.';
                $content .= '<br><br>You can get the list of your machine on your account page: '.$this->config['site']['url'].$this->router->generate('app_user_profile_generic');

                // message from admin to user

                $message = new Message();
                $message->setSender('admin');
                $message->setReceiver($session->getUser()->getId());
                $message->setContent(strip_tags($content));
                $message->setTime(time());
                $this->entityManager->persist($message);
                $this->entityManager->flush($message);

                $footer = '<br><br>Answering this email directly will NOT work.<br><br><a href="'.$this->config['site']['url'].$this->router->generate('app_user_messages', ['user' => $session->getUser()->getId()]).'">You can answer this message by going on this link</a>';

                Mail::sendamail($session->getUser()->getEmail(), 'Your machine on SheepIt renderfarm might be broken', $content.$footer, 'send_message');

                $this->discord->userMessage($session->getUser()->getId(), $session->getUser()->getId(), $this->config['site']['url'].$this->router->generate('app_user_messages', ['user' => $session->getUser()->getId()]), strip_tags($content));
            }
        }

        return Command::SUCCESS;
    }
}
