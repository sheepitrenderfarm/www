<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Constant;
use App\Repository\TaskRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:task:single',
    description: 'Execute a task',
)]
class TaskSingleCommand extends Command {
    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;


    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, TaskRepository $taskRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
    }

    protected function configure(): void {
        $this->addArgument('id', InputArgument::REQUIRED, 'Task id');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $task = $this->taskRepository->find($input->getArgument('id'));

        if (is_object($task)) {
            $task->setStatus(Constant::PROCESSING);
            $this->entityManager->flush();

            if ($task->execute()) {
                $this->entityManager->remove($task);
                $this->entityManager->flush();
                return Command::SUCCESS;
            }
        }
        return Command::FAILURE;
    }
}
