<?php
/**
 * Copyright (C) 2025 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:image2base64',
    description: 'Convert an image to base64',
)]
class ImageToBase64Command extends Command {
    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    protected function configure(): void {
        $this->addArgument('path', InputArgument::REQUIRED, 'Image');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $path = $input->getArgument('path');
        $data = file_get_contents($path);

        echo "------------------\n";
        echo base64_encode($data);
        echo "\n------------------\n";

        return Command::SUCCESS;
    }
}
