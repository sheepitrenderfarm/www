<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Repository\ProjectRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:projects:chunk',
    description: 'Create chunks for all projects',
)]
class ProjectsCreateChunkCommand extends Command {
    private ProjectRepository $projectRepository;


    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, ProjectRepository $projectRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->projectRepository = $projectRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->projectRepository->findAll() as $project) {
            if (count($project->getChunks()) == 0) {
                echo "missing chunks for ".$project->getId()." ".$project->getName()."\n";

                $path = GlobalInject::getConfigService()->getData()['storage']['path'].'projects'.'/'.$project->getId().'.'.'zip';
                $this->projectRepository->createChunks($project, $path);
                //@unlink($path);
            }
        }
        return Command::SUCCESS;
    }
}
