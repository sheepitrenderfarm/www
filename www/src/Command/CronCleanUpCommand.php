<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\SessionError;
use App\Service\Cleaner;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:cleanup',
    description: 'Cleanup cron',
)]
class CronCleanUpCommand extends Command {
    private array $config;
    private EntityManagerInterface $entityManager;
    private Cleaner $cleaner;

    public function __construct(
        ConfigService $configService,
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        Cleaner $cleaner) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
        $this->cleaner = $cleaner;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $this->cleaner->removeOldProjects();
        $this->cleaner->removeOldPausedProjects();
        $this->cleaner->removeOrphanProjectFromShepherd();
        $this->cleaner->removeBlockedProjects();
        $this->cleaner->removeOldTmpUploadProjectFiles();
        $this->cleaner->removeOldTmpFiles();
        $this->cleaner->removeOldPreusers();
        $this->cleaner->removeExpiredGifts();
        $this->cleaner->removeOrphanLiaisons();
        $this->cleaner->removeOrphanTeams();
        $this->cleaner->removeOldProjectAnalyses();
        $this->cleaner->removeOrphanCDNElements();

        $this->entityManager->getRepository(SessionError::class)->removeCache();

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
