<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:newsletter',
    description: 'Send the newsletter',
)]
class NewsLetterCommand extends Command {
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;
    private UrlGeneratorInterface $router;
    private array $config;

    public function __construct(Main $main, EntityManagerInterface $entityManager, ConfigService $configService, UrlGeneratorInterface $router, UserRepository $userRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->config = $configService->getData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $message = "Hello<br><p>We believe every moment is a good moment to express our gratitude to the amazing SheepIt community. <br>This week, we're celebrating the solstice, why ? why not! and we'd like to make it extra special for you!<br>From now until next Friday, every user who renders a frame will receive a special community award along with a surprise reward as a token of our appreciation.";

        $email_title = "Happy solstice - community event 2024";
        // for final
        $users = $this->userRepository->findAll();

        // for testing
        // $users = [];
        // $users []= $main->importUser('clouclou');

        $nb_users = 0;
        $nb_emails = 0;

        $nb_total_users = count($users);
        foreach ($users as $u) {
            $nb_users++;
            if ($u->noficationIsEnabled(User::NOTIFICATION_MASK_NEWSLETTER)) {
                if ($u->getNotificationToken() == '') {
                    $u->setNotificationToken(Misc::code(32));
                    $this->entityManager->flush($u);
                }

                $url_unsubscribe = $this->config['site']['url'].$this->router->generate('app_user_unsubscribe', ['token' => $u->getNotificationToken()], UrlGeneratorInterface::ABSOLUTE_PATH);

                $footer = "<br>C.<p><i>You are receiving this email because you have checked the option \"Get notified by e-mail when: news or an important event is posted\" on your <a href=\"https://www.sheepit-renderfarm.com/user/profile\">profile page</a>, you can disable the option at any moment by following this <a href=\"".$url_unsubscribe."\">unsubscribe link</a></i></p>";

                $nb_emails++;
                Mail::sendamail($u->getEmail(), $email_title, $message.$footer, 'sheepit');
                sleep(1.1);
            }

            if ($nb_users % 100 == 0) {
                echo "emails sent: $nb_emails users: $nb_users / $nb_total_users\n";
            }
        }

        return Command::SUCCESS;
    }
}
