<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\CDN\CDNBucket;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cdn:list',
    description: 'List of files on the CDN bucket',
)]
class CDNListCommand extends Command {
    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ((new CDNBucket())->listAll() as $f) {
            echo "$f\n";
        }

        return Command::SUCCESS;
    }
}
