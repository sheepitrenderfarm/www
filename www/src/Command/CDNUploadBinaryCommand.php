<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\CDN\CDNBucket;
use App\Repository\BlenderArchRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cdn:upload:binary',
    description: 'Sync binary to the CDN',
)]
class CDNUploadBinaryCommand extends Command {
    private array $config;
    private EntityManagerInterface $entityManager;
    private BlenderArchRepository $blenderArchRepository;

    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, BlenderArchRepository $blenderArchRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);
        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
        $this->blenderArchRepository = $blenderArchRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $this->syncBinaries();

        return Command::SUCCESS;
    }

    private function syncBinaries(): void {
        foreach ($this->blenderArchRepository->findAll() as $blenderArch) {
            $this->syncFile($this->config['storage']['path'].'binaries/'.$blenderArch->getPath()); // full zip
            foreach ($blenderArch->getChunks() as $chunk) {
                $this->syncFile($this->config['storage']['path'].'projects'.'/'.$chunk);
            }
            $blenderArch->setSyncedBucket(true);
            $this->entityManager->flush();
        }
    }

    private function syncFile(string $path): void {
        $cdn = new CDNBucket();
        $filename = basename($path);
        if (file_exists($path) && $cdn->fileExists($filename) == false) {
            echo "Uploading ".$path."\n";
            $cdn->uploadFile($path);
        }
    }
}
