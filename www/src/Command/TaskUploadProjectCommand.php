<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\TaskProjectUploadToMirror;
use App\Repository\ProjectRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:project:upload',
    description: 'Upload a project on the CDN',
)]
class TaskUploadProjectCommand extends Command {
    private ProjectRepository $projectRepository;

    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, ProjectRepository $projectRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->projectRepository = $projectRepository;
    }

    protected function configure(): void {
        $this->addArgument('id', InputArgument::REQUIRED, 'Project id');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $project = $this->projectRepository->find($input->getArgument('id'));

        if (is_object($project)) {
            $task = new TaskProjectUploadToMirror();
            $task->setProject($project->getId());
            return $task->execute() ? Command::SUCCESS : Command::FAILURE;
        }
        return Command::FAILURE;
    }
}
