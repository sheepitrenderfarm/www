<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Constant;
use App\Entity\BlenderArch;
use App\Repository\BlenderArchRepository;
use App\Repository\BlenderRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:updatebinarymd5',
    description: 'Update md5 of Blender binary'
)]
class UpdateBinaryMD5Command extends Command {
    private BlenderArchRepository $blenderArchRepository;
    private BlenderRepository $blenderRepository;

    private array $config;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, BlenderRepository $blenderRepository, BlenderArchRepository $blenderArchRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->blenderArchRepository = $blenderArchRepository;
        $this->blenderRepository = $blenderRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach (glob($this->config['storage']['path'].'binaries'.'/blender*.'.$this->config['archive']['extension']) as $path) {
            echo basename($path)."\n";
            list($version, $arch) = explode('_', basename($path));

            $blender = $this->blenderRepository->find($version);
            if (is_null($blender)) {
                echo $version." not found (will not add it)\n";
                continue;
            }

            $blenderArch = $this->blenderArchRepository->findOneBy(['blender' => $blender->getId(), 'arch' => $arch]);
            if (is_null($blenderArch)) {
                // adding
                $blenderArch = new BlenderArch();
                $blenderArch->setBlender($blender);
                $blenderArch->setArch($arch);
                $blenderArch->setChunks($this->generate($path));
                GlobalInject::getEntityManager()->persist($blenderArch);
            }

            // updating
            $blenderArch->setMd5(md5_file($path));
            $blenderArch->setArchiveSize(filesize($path));
        }

        GlobalInject::getEntityManager()->flush();

        return Command::SUCCESS;
    }

    private function generate(string $path): array {
        $md5s = [];

        $file = @fopen($path, 'r');
        if ($file === false) {
            return [];
        }

        while (feof($file) == false) {
            $data = fread($file, (int)Constant::MAX_CHUNK_SIZE);
            $md5 = md5($data);

            $md5s [] = $md5;

            file_put_contents($this->config['storage']['path'].'projects/'.$md5, $data);
        }
        fclose($file);

        return $md5s;
    }

}
