<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use App\Service\RequestTimeProfiler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:monthly',
    description: 'Monthly cron',
)]
class CronMonthlyCommand extends Command {
    private ConfigService $configService;
    private RequestTimeProfiler $requestTimeProfiler;


    public function __construct(ConfigService $configService, Main $main, EntityManagerInterface $entityManager, UrlGeneratorInterface $router, RequestTimeProfiler $requestTimeProfiler) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->configService = $configService;
        $this->requestTimeProfiler = $requestTimeProfiler;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $config = $this->configService->getData();

        if ($config['cron']['enable'] == false) {
            return Command::FAILURE;
        }

        $this->requestTimeProfiler->reset();

        return Command::SUCCESS;
    }
}
