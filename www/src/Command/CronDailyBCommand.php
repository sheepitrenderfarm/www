<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\Award;
use App\Repository\AwardRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// split the daily cron in multiple command due to ram usage
#[AsCommand(
    name: 'sheepit:cron:daily:b',
    description: 'Daily cron (b)',
)]
class CronDailyBCommand extends Command {
    private array $config;
    private Main $main;
    private EntityManagerInterface $entityManager;
    private AwardRepository $awardRepository;

    public function __construct(
        ConfigService $configService,
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        AwardRepository $awardRepository) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->main = $main;
        $this->entityManager = $entityManager;
        $this->awardRepository = $awardRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $this->main->getPersistentCache()->set('best_renderer', serialize($this->main->getRenderers($this->config['rank']['duration'])));

        // awards
        foreach ($this->awardRepository->awardsAvailable() as $award_name) {
            $class_name = "\\App\\Entity\\".$award_name;
            /** @var Award $award */
            $award = new $class_name();
            $award->cronDaily();
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
