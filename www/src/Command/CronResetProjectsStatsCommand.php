<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Constant;
use App\Repository\ProjectRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:resetstats',
    description: 'Reset projects stats',
)]
class CronResetProjectsStatsCommand extends Command {
    private EntityManagerInterface $entityManager;
    private ProjectRepository $projectRepository;
    private array $config;

    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ProjectRepository $projectRepository, ConfigService $configService) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->entityManager = $entityManager;
        $this->projectRepository = $projectRepository;
        $this->config = $configService->getData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        foreach ($this->projectRepository->findAll() as $project) {
            if ($project->shouldHideFromUser() == false) {
                $stats = $project->getCachedStatistics();
                if ($stats[Constant::PROCESSING] != 0 || $stats[Constant::WAITING] != 0) {
                    $project->generateStatsCache();
                    $this->entityManager->flush();
                }
            }
        }
        return Command::SUCCESS;
    }
}
