<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\AwardRandomLootDrop;
use App\Entity\StatsUser;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// split the daily cron in multiple command due to ram usage
#[AsCommand(
    name: 'sheepit:cron:daily:a',
    description: 'Daily cron (a)',
)]
class CronDailyACommand extends Command {
    private array $config;
    private Main $main;
    private EntityManagerInterface $entityManager;
    private SessionRepository $sessionRepository;
    private UserRepository $userRepository;

    public function __construct(
        ConfigService $configService,
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        SessionRepository $sessionRepository,
        UserRepository $userRepository) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->main = $main;
        $this->entityManager = $entityManager;
        $this->sessionRepository = $sessionRepository;
        $this->userRepository = $userRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $now = time();

        foreach ($this->sessionRepository->findAll() as $session) {
            AwardRandomLootDrop::addSession($this->main->getPersistentCache(), $session);
        }

        // update the best renderers
        $users = $this->userRepository->findAll();
        foreach ($users as $a_user) {
            if ($a_user->getRenderedFrames() != 0 || $a_user->getPoints() != 0 || $a_user->getTeam() != null) {
                $su = new StatsUser();
                $su->setDate((new \DateTime())->setTimestamp($now));
                $su->setUser($a_user);
                $su->setRenderedFrames($a_user->getRenderedFrames());
                $su->setPoints($a_user->getPoints());
                $su->setPointsEarn($a_user->getPointsEarn());
                $su->setRenderTime($a_user->getRenderTime());
                $team = $a_user->getTeam();
                $su->setTeam($team == null ? -1 : $team->getId());
                $su->setTeamContribution($a_user->getTeamContribution());
                $this->entityManager->persist($su);
                $this->entityManager->flush($su);
            }
        }
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
