<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\CDN\CDNBucket;
use App\Repository\ProjectRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cdn:upload:project',
    description: 'Sync project\'s chunks to the CDN',
)]
class CDNUploadProjectCommand extends Command {
    private EntityManagerInterface $entityManager;
    private ProjectRepository $projectRepository;

    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, ConfigService $configService, ProjectRepository $projectRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);
        $this->entityManager = $entityManager;
        $this->projectRepository = $projectRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $this->syncProjects();

        return Command::SUCCESS;
    }

    private function syncProjects(): void {
        foreach ($this->projectRepository->findAll() as $project) {
            foreach ($project->getChunks() as $chunk) {
                $this->syncFile($project->fullPathOfChunk($chunk));
            }
            $project->setSyncedBucket(true);
            $this->entityManager->flush();
        }
    }

    private function syncFile(string $path): void {
        $cdn = new CDNBucket();
        $filename = basename($path);
        if (file_exists($path) && $cdn->fileExists($filename) == false) {
            echo "Uploading ".$path."\n";
            $cdn->uploadFile($path);
        }
    }
}
