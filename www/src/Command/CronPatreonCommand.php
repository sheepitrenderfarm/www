<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\Patreon;
use App\Entity\User;
use App\Repository\PatreonRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use App\Service\PatreonAPI;
use App\Utils\Mail;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:patreon',
    description: 'Patreons email',
)]
class CronPatreonCommand extends Command {
    private EntityManagerInterface $entityManager;
    private PatreonRepository $patreonRepository;
    private UrlGeneratorInterface $router;
    private array $config;
    private string $client_id;
    private string $client_secret;
    private string $creator_refresh_token;

    public function __construct(EntityManagerInterface $entityManager, Main $main, UrlGeneratorInterface $router, PatreonRepository $patreonRepository, ConfigService $configService) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->entityManager = $entityManager;
        $this->patreonRepository = $patreonRepository;
        $this->config = $configService->getData();
        $this->router = $router;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $cache = GlobalInject::getMain()->getPersistentCache();

        if ($this->getConfigFromCache() == false) {
            Logger::debug(__METHOD__.' failed to get infos from memcache');
            // from config file
            if ($this->getConfigFromFile() == false) {
                Logger::debug(__METHOD__.' failed to get infos from config file');
                return Command::FAILURE;
            }
        }

        $api_client = new PatreonAPI($this->client_id, $this->client_secret, $this->creator_refresh_token);
        if ($api_client->updateToken() == false) {
            Logger::error(__METHOD__.' Failed to update access_token (1)');

            // it's most likely because the credentials on memcached are not right, let's try the config file ones.
            if ($this->getConfigFromFile() == false) {
                Logger::debug(__METHOD__.' failed to get infos from config file (from fallback on failure of updateToken)');
                return Command::FAILURE;
            }
            $api_client = new PatreonAPI($this->client_id, $this->client_secret, $this->creator_refresh_token);
            if ($api_client->updateToken() == false) {
                Logger::error(__METHOD__.'Failed to update access_token (2)');
                return Command::FAILURE;
            }
        }

        // save id to cache for next time
        $this->client_id = $api_client->getClientID();
        $this->client_secret = $api_client->getClientSecret();
        $this->creator_refresh_token = $api_client->getRefreshToken();

        $cache->set('patreon_client_id', $this->client_id);
        $cache->set('patreon_client_secret', $this->client_secret);
        $cache->set('patreon_creator_refresh_token', $this->creator_refresh_token);

        $campaigns = $api_client->getCampaigns();
        $members = $api_client->getMembersOfCampaign(reset($campaigns));

        foreach ($members as $json_member) {
            $data = $json_member['attributes'];
            if ($data['patron_status'] == 'active_patron') {
                $p = $this->patreonRepository->find($data['email']);
                if (is_null($p)) {
                    // create a new patreon
                    $p = new Patreon();
                    $p->setId($data['email']);
                    $p->setLastCharge('');
                    $this->patreonRepository->add($p);
                }
                $date = DateTime::createFromFormat(DateTimeInterface::RFC3339_EXTENDED, $data['last_charge_date']);
                if ($date !== false && date_format($date, 'Y-m') != $p->getLastCharge()) {
                    $email = $data['email'];
                    // patreon display name: $data['Name']
                    $this->patronEmail($p, $email, date_format($date, 'Y-m'));
                }
            }
        }

        return Command::SUCCESS;
    }


    private function patronEmail(Patreon $patreon, string $email, string $lastCharge): void {
        if (is_object($patreon->getUser())) {
            // has a user, simply add the points

            $patreon->setLastCharge($lastCharge);
            $this->entityManager->flush();
            $this->patreonRepository->giveReward($patreon);

            if ($patreon->getUser()->noficationIsEnabled(User::NOTIFICATION_MASK_PATREON)) {
                $email_subject = 'Thank you for being a SheepIt patron';
                $email_contents = "Hello,<br>";
                $email_contents .= "<p>I'm Laurent, the creator of SheepIt-renderfarm, I want to thank you for being a SheepIt patron.<br>";
                $email_contents .= "I have added ".number_format(Patreon::REWARD_PER_MONTH)." points to your account.<br>";
                $email_contents .= "</p>";
                $email_contents .= "<p><i>You are receiving this email because you have checked the option \"I get my Patreon reward\" on your <a href=\"https://www.sheepit-renderfarm.com/user/profile\">profile page</a>, you can disable the option at any moment.</i></p>";
                Mail::sendamail($patreon->getUser()->getEmail(), $email_subject, $email_contents, 'sheepit');
            }
        }
        elseif (is_null($patreon->getEmailSent()) || $patreon->getEmailSent()->getTimestamp() < time() - 30 * 86400) {
            // first time
            // must link the email to a sheepit user
            $this->sendEmailAskUsername($patreon, $email);
        }
        else {
            Logger::error(__METHOD__.' do nothing for '.$email);
        }
    }

    public function sendEmailAskUsername(Patreon $patreon, string $email): void {
        $url = $this->config['site']['url'].$this->router->generate("app_user_patreon", ["email" => $email], UrlGeneratorInterface::ABSOLUTE_PATH);
        $email_subject = 'Thank you for being a SheepIt patron';
        $email_contents = "Hello,<br>";
        $email_contents .= "I'm Laurent, the creator of SheepIt-renderfarm, I want to thank you for being a SheepIt patron.<br>";
        $email_contents .= "Since I don't know your SheepIt username, I need you to click on the link below to link your SheepIt account with your Patreon account.<br>It will not in any way modify your Patreon account, it will just tell me where to send the SheepIt points";
        $email_contents .= '<br>Even if you are not a new patron, you still need to follow this link: I am changing the way patreon is handled because as a user it was a bit annoying to claim points every month, now it will be automatic.<br>';
        $email_contents .= 'Simply <a href="'.$url.'">follow this link</a>, log in.';
        Mail::sendamail($email, $email_subject, $email_contents, 'sheepit');

        $patreon->setEmailSent(new DateTime());
        $this->entityManager->flush();
    }

    private function getConfigFromFile(): bool {
        if (isset($this->config['patreon']['client_id']) == false || strlen($this->config['patreon']['client_id']) == 0) {
            return false;
        }
        if (isset($this->config['patreon']['client_secret']) == false || strlen($this->config['patreon']['client_secret']) == 0) {
            return false;
        }
        if (isset($this->config['patreon']['creator_refresh_token']) == false || strlen($this->config['patreon']['creator_refresh_token']) == 0) {
            return false;
        }

        $this->client_id = $this->config['patreon']['client_id'];
        $this->client_secret = $this->config['patreon']['client_secret'];
        $this->creator_refresh_token = $this->config['patreon']['creator_refresh_token'];

        return true;
    }

    private function getConfigFromCache(): bool {
        $cache = GlobalInject::getMain()->getPersistentCache();

        $this->client_id = $cache->get('patreon_client_id');
        $this->client_secret = $cache->get('patreon_client_secret');
        $this->creator_refresh_token = $cache->get('patreon_creator_refresh_token');

        if ($this->client_id === false || strlen($this->client_id) == 0) {
            return false;
        }
        if ($this->client_secret === false || strlen($this->client_secret) == 0) {
            return false;
        }
        if ($this->creator_refresh_token === false || strlen($this->creator_refresh_token) == 0) {
            return false;
        }

        return true;
    }
}
