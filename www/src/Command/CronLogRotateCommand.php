<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:logrotate',
    description: 'log rotate',
)]
class CronLogRotateCommand extends Command {
    public function __construct(
        ConfigService $configService,
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $date = date('Ymd', strtotime("-1 days"));
        @rename(Logger::file(), Logger::file().'.'.$date);

        return Command::SUCCESS;
    }
}
