<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Entity\StatsCDN;
use App\Entity\StatsTeam;
use App\Repository\CDNRepository;
use App\Repository\TeamRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// split the daily cron in multiple command due to ram usage
#[AsCommand(
    name: 'sheepit:cron:daily:c',
    description: 'Daily cron (c)',
)]
class CronDailyCCommand extends Command {
    private array $config;
    private EntityManagerInterface $entityManager;
    private CDNRepository $CDNRepository;
    private TeamRepository $teamRepository;

    public function __construct(
        ConfigService $configService,
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        CDNRepository $CDNRepository,
        TeamRepository $teamRepository) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
        $this->CDNRepository = $CDNRepository;
        $this->teamRepository = $teamRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        // update team
        foreach ($this->teamRepository->findAll() as $team) {
            $st = new StatsTeam();
            $st->setDate(new \DateTime());
            $st->setTeam($team);
            $st->setPoints($team->getPoints());
            $st->setMembers(count($team->members()));
            $this->entityManager->persist($st);
            $this->entityManager->flush($st);
        }

        // mirrors
        foreach ($this->CDNRepository->findAll() as $mirror) {
            $sm = new StatsCDN();
            $sm->setDate(new \DateTime('-1 day'));
            $sm->setUrl($mirror->getId());
            $sm->setDownload($mirror->getDownLoad());
            $sm->setTotal($mirror->getTotal());

            $mirror->resetStats();
            $this->entityManager->persist($sm);
            $this->entityManager->flush();
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
