<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Repository\PreUserRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:emailpreusers',
    description: '(re)Send registration emails to all pre-user',
)]
class EmailPreUsersCommand extends Command {
    private PreUserRepository $preUserRepository;

    public function __construct(Main $main, EntityManagerInterface $entityManager, ConfigService $configService, PreUserRepository $preUserRepository, UrlGeneratorInterface $router) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->preUserRepository = $preUserRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        foreach ($this->preUserRepository->findAll() as $preuser) {
            $preuser->sendRegistrationConfirmationEmail();
        }

        return Command::SUCCESS;
    }
}
