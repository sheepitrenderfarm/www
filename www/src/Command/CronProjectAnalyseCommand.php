<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Constant;
use App\Repository\ProjectAnalyseRepository;
use App\Service\BlendService;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use App\Utils\File;
use App\Utils\Zip;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:projectanalyse',
    description: 'Analyse files uploaded by user',
)]
class CronProjectAnalyseCommand extends Command {
    private ProjectAnalyseRepository $projectAnalyseRepository;
    private BlendService $blendService;
    private EntityManagerInterface $entityManager;
    private array $config;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, BlendService $blendService, ProjectAnalyseRepository $projectAnalyseRepository) {
        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->entityManager = $entityManager;
        $this->projectAnalyseRepository = $projectAnalyseRepository;
        $this->blendService = $blendService;
        $this->config = $configService->getData();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        // to improve response time, do no exit once a single analyse has been done
        // but wait a bit in hope a new task will be done.
        // since the cron is every minute, it should reduce the 'launch time'

        $remaining_wait = 50; // in second

        while ($remaining_wait > 0) {
            if ($this->work() == false) {
                $wait = random_int(1, 10);
                $remaining_wait -= $wait;
                sleep($wait);
            }
        }

        return Command::SUCCESS;
    }

    /**
     * @return bool status if some work has been done
     */
    private function work(): bool {
        $task = $this->projectAnalyseRepository->findOneBy(['status' => Constant::WAITING]);
        if (is_object($task)) {
            $task->setStatus(Constant::PROCESSING);
            $this->entityManager->flush($task);

            $start = microtime(true);

            // is it a blend or a zip ?
            $path_parts = pathinfo($task->getPath());
            if ($path_parts['extension'] == 'blend') {
                $working_dir = dirname($task->getPath());
            }
            else {
                $working_dir_extracted = File::tempdir($this->config['tmp_dir']);
                $working_dir = dirname($task->getPath());

                // replace the old working directory (getPath) with one with extracted data
                Zip::unzip($task->getPath(), $working_dir_extracted);
                File::rmdirRecurse($working_dir);
                rename($working_dir_extracted, $working_dir);
            }

            $blend_files = $this->blendService->listBlendFilesFromDirectory($working_dir, $task);
            $end = microtime(true);
            Logger::debug(__METHOD__.' analyse took '.sprintf("%.1f", $end - $start));
            $task->setStatus(Constant::FINISHED);
            $task->setResult(json_encode($blend_files));
            $this->entityManager->flush($task);
            return true;
        }
        else {
            return false;
        }
    }
}
