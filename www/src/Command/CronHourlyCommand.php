<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Command;

use App\Constant;
use App\Entity\Award;
use App\Entity\SessionsStatus;
use App\Entity\StatsGlobal;
use App\Entity\User;
use App\Repository\AwardRepository;
use App\Repository\ProjectRepository;
use App\Repository\ShepherdServerRepository;
use App\Repository\TileRepository;
use App\Repository\SessionRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[AsCommand(
    name: 'sheepit:cron:hourly',
    description: 'Hourly cron',
)]
class CronHourlyCommand extends Command {
    private array $config;
    private EntityManagerInterface $entityManager;
    private Main $main;
    private AwardRepository $awardRepository;
    private SessionRepository $sessionRepository;
    private TileRepository $tileRepository;
    private ProjectRepository $projectRepository;
    private TaskRepository $taskRepository;
    private UserRepository $userRepository;
    private ShepherdServerRepository $shepherdServerRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        Main $main,
        UrlGeneratorInterface $router,
        ConfigService $configService,
        AwardRepository $awardRepository,
        SessionRepository $sessionRepository,
        TileRepository $tileRepository,
        ProjectRepository $projectRepository,
        TaskRepository $taskRepository,
        UserRepository $userRepository,
        ShepherdServerRepository $shepherdServerRepository
    ) {

        parent::__construct();
        GlobalInject::setInject($main, $router, $entityManager, $configService);

        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
        $this->main = $main;
        $this->awardRepository = $awardRepository;
        $this->sessionRepository = $sessionRepository;
        $this->tileRepository = $tileRepository;
        $this->projectRepository = $projectRepository;
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        if ($this->config['cron']['enable'] == false) {
            Logger::debug('cron disabled');
            return Command::FAILURE;
        }

        $now = time();

        $sg = new StatsGlobal();
        $sg->setId($now);

        $sessions = $this->sessionRepository->findAll();
        $total_power = array('value' => 0, 'number' => 0);
        foreach ($sessions as $a_session) {
            $machine = $a_session->getComputeDevice();
            if (is_object($machine) && $machine->getPower() != 0) {
                $total_power['value'] += $machine->getPower();
                $total_power['number'] += 1;
            }
        }

        $sg->setSessions($total_power['number']);
        $sg->setPower($total_power['value']);


        $userStats = $this->userRepository->getSumStats();
        $sg->setFramesRendered($userStats['rendered_frames']);
        $sg->setTimeRendered($userStats['render_time']);
        $sg->setFramesOrdered($userStats['ordered_frames']);
        $sg->setTimeOrdered($userStats['ordered_frames_time']);
        $sg->setProjectsCreated($userStats['nb_projects']);
        $sg->setTasks($this->taskRepository->count([]));

        $sg->setFramesRemaining($this->tileRepository->countAvailablePublicFrame());

        $sg->setActiveRenderers(count($this->main->getBestRenderersFromCache()));

        //     $request = 'SELECT 1 FROM @1,@2,@3 WHERE @1.@4 = %5 and @1.@6 = @2.@7 and @2.@8 = %9 and @2.@10 = %11 and @2.@12 = @3.@13 and @3.@14 = %15';
        //     $SQL->DoQuery($request, 'frame', 'job', 'scene', 'status', Constant::WAITING, 'job', 'id', 'status', Constant::WAITING, 'blocked', '0', 'scene', 'id', 'public_render', '1');
        $user = new User();
        $user->setId('login_'.time());
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1);
        $sg->setFramesRemainingPublic($this->tileRepository->countAvailableFramesForUser($user));

        $active_projects = 0;
        $projects = $this->projectRepository->findAll();
        foreach ($projects as $project) {
            $status = $project->getStatus();
            if ($status == Constant::PROCESSING || $status == Constant::WAITING) {
                if ($project->isBlocked() == false) {
                    $active_projects += 1;
                }
            }
        }
        $sg->setActiveProjects($active_projects);

        $sg->setFramesProcessing($this->tileRepository->count(['status' => Constant::PROCESSING]));

        $this->entityManager->persist($sg);
        $this->entityManager->flush($sg);

        // Session Status
        $status = array('disabled' => 0, 'unknown' => 0, 'running' => 0, 'paused' => 0, 'sleeping' => 0, 'broken_zero_validation' => 0, 'rendering' => 0, 'idle' => 0);

        $tiles_processing = $this->tileRepository->findBy(['status' => Constant::PROCESSING]);
        $sessions = $this->sessionRepository->findAll();
        foreach ($sessions as $session) {
            if ($session->getBlocked() != 0) {
                $status['disabled'] += 1;
            }
            elseif ($session->getPaused()) {
                $status['paused'] += 1;
            }
            elseif ($session->getSleeping()) {
                $status['sleeping'] += 1;
            }
            else {
                if ($now - $session->getLastvalidatedframe() < 60 * 3) {
                    $status['running'] += 1;
                }
                else {
                    if ($session->getRenderedFrames() == 0 && ($now - $session->getCreationTime()) > 60 * 15) {
                        $status['broken_zero_validation'] += 1;
                    }
                    else {
                        $rendering = false;
                        foreach ($tiles_processing as $tile) {
                            if ($tile->getSession() == $session->getId()) {
                                $rendering = true;
                                break;
                            }
                        }
                        if ($rendering) {
                            $status['rendering'] += 1;
                        }
                        else {
                            $status['idle'] += 1;
                        }
                    }
                }
            }
        }

        $s = new SessionsStatus();
        $s->setDisabled($status['disabled']);
        $s->setRunning($status['running']);
        $s->setBrokenZeroValidation($status['broken_zero_validation']);
        $s->setRendering($status['rendering']);
        $s->setPaused($status['paused']);
        $s->setSleeping($status['sleeping']);
        $s->setIdle($status['idle']);
        $s->setTime($now);
        $this->entityManager->persist($s);
        $this->entityManager->flush($s);

        // awards
        foreach ($this->awardRepository->awardsAvailable() as $award_name) {
            $class_name = "\\App\\Entity\\".$award_name;
            /** @var Award $award */
            $award = new $class_name();
            $award->cronHourly();
        }

        // render days
        foreach ($this->sessionRepository->findAll() as $session) {
            $session->updateOwnerConsecutiveRenderDays();
        }


        foreach ($this->projectRepository->findAll() as $project) {
            if ($project->isBlocked() == false && ($project->getStatus() == Constant::PROCESSING || $project->getStatus() == Constant::WAITING)) {
                $cache = $project->getCachedStatistics();

                // Progression
                // if it's after 50% of completion, it's too late to block the project.
                if ($cache['total'] != 0 && ((float)($cache[Constant::FINISHED]) / (float)($cache['total']) < 0.50) && $project->getAverageRenderTimeMachineRef() > (int)($this->config['power']['rendertime_max_reference']) * 2.0) {
                    Logger::info(__METHOD__.sprintf("Project %s (id: %d) should be block due to high render time %s vs %s", $project->getName(), $project->getId(), Misc::humanTime($project->getAverageRenderTimeMachineRef()), Misc::humanTime($this->config['power']['rendertime_max_reference'])));
                    // should be blocked
                    $this->projectRepository->block($project, 1);
                }
            }
        }

        // project size prediction
        foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
            $shepherd->updatePredictedProjectsSize();
        }

        return Command::SUCCESS;
    }
}
