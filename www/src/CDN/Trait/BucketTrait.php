<?php

namespace App\CDN\Trait;

use Aws\Credentials\Credentials;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

trait BucketTrait {
    public function fileExists(string $path): bool {
        if (array_key_exists('bucket_name', $this->getRootConfig()) == false || $this->getRootConfig()['bucket_name'] == '') {
            // bad config
            return false;
        }
        try {
            return $this->getS3Client()->doesObjectExist($this->getRootConfig()['bucket_name'], $path);
        }
        catch (S3Exception $e) {
            return false;
        }
    }

    public function getId(): string {
        return $this->getRootConfig()['id'];
    }

    public function getTarget(string $target): string {
        if ($this->getRootConfig()['bucket_name'] != '') {
            return $this->getId().'/'.$target;
        }
        return '';
    }

    public function removeFile(string $path): bool {
        if ($this->getRootConfig()['bucket_name'] != '') {
            $this->getS3Client()->deleteObject([
                'Bucket' => $this->getRootConfig()['bucket_name'],
                'Key' => $path,
            ]);

            return true;
        }
        else {
            return false;
        }
    }

    public function uploadFile(string $path): bool {
        if ($this->getRootConfig()['bucket_name'] != '') {
            $this->getS3Client()->putObject([
                'Bucket' => $this->getRootConfig()['bucket_name'],
                'Key' => basename($path),
                'Body' => fopen($path, 'r'),
                'ACL' => 'public-read',
            ]);

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @return string[]
     */
    public function listAll(): array {
        $ret = [];

        // use paginator to get more than 1000 elements
        $results = $this->getS3Client()->getPaginator('ListObjects', [
            'Bucket' => $this->getRootConfig()['bucket_name'],
        ]);

        foreach ($results as $result) {
            if (isset($result['Contents'])) {
                foreach ($result['Contents'] as $arr) {
                    $ret [] = $arr['Key'];
                }
            }
        }
        return $ret;
    }

    private function getS3Client(): S3Client {
        $credentials = new Credentials($this->getRootConfig()['credentials']['access_key_id'], $this->getRootConfig()['credentials']['secret_access_key']);

        $options = [
            'region' => 'auto',
            'endpoint' => $this->getRootConfig()['credentials']['endpoint'],
            'version' => 'latest',
            'credentials' => $credentials,
        ];

        return new S3Client($options);
    }

}