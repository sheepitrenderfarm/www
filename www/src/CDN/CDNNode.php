<?php

namespace App\CDN;

use App\Service\GlobalInject;

abstract class CDNNode {
    abstract public function getId(): string;

    abstract public function fileExists(string $path): bool;

    abstract public function getTarget(string $target): string;

    /**
     * Use local file or remote url?
     */
    abstract public function isLocal(): bool;

    protected function getCacheKey(): string {
        return 'cdn_'.$this->getId();
    }

    public function resetStats(): array {
        $cache = GlobalInject::getMain()->getPersistentCache();
        $data = $this->getCacheData();

        $cache->set($this->getCacheKey(), ['download' => 0, 'total' => 0]);

        return $data;
    }

    public function addDownload(int $filesize): void {
        $cache = GlobalInject::getMain()->getPersistentCache();
        $data = $this->getCacheData();

        $data['download'] += 1;
        $data['total'] += $filesize;

        $cache->set($this->getCacheKey(), $data);
    }

    public function getDownLoad(): int {
        $data = $this->getCacheData();
        return $data['download'];
    }

    public function getTotal(): int {
        $data = $this->getCacheData();
        return $data['total'];
    }

    private function getCacheData(): array {
        $cache = GlobalInject::getMain()->getPersistentCache();
        $data = $cache->get($this->getCacheKey());
        return is_array($data) ? $data : ['download' => 0, 'total' => 0];
    }
}