<?php

namespace App\CDN;

use App\CDN\Trait\BucketTrait;
use App\Service\GlobalInject;

class CDNBucket extends CDN {
    use BucketTrait;

    public function isLocal(): bool {
        return false;
    }

    protected function getRootConfig(): array {
        $config = GlobalInject::getConfigService()->getData();
        return $config['cdn'];
    }

    public function getId(): string {
        return $this->getRootConfig()['id'];
    }

    public function isConfigurated(): bool {
        return $this->getRootConfig()['bucket_name'] != '';
    }
}