<?php

namespace App\CDN;

use App\Service\GlobalInject;

class CDNInternal extends CDN {

    public function isLocal(): bool {
        return true;
    }

    public function getId(): string {
        return 'internal';
    }

    public function getBaseDir(): string {
        return 'projects';
    }

    public function uploadFile(string $path): bool {
        return true;
    }

    public function removeFile(string $path): bool {
        return true;
    }

    public function listAll(): array {
        $config = GlobalInject::getConfigService()->getData();
        return array_map(function ($path) {
            return basename($path);
        }, glob($config['storage']['path'].$this->getBaseDir().'/*'));
    }

    public function fileExists(string $path): bool {
        $config = GlobalInject::getConfigService()->getData();
        return file_exists($config['storage']['path'].$this->getBaseDir().'/'.$path);
    }

    public function getTarget(string $target): string {
        $config = GlobalInject::getConfigService()->getData();
        return $config['storage']['path'].$this->getBaseDir().'/'.$target;
    }

    public function isConfigurated(): bool {
        $config = GlobalInject::getConfigService()->getData();
        return $config['storage']['path'] != '';
    }

}