<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\CDN;

abstract class CDN extends CDNNode {
    abstract public function isConfigurated(): bool;

    abstract public function uploadFile(string $path): bool;

    abstract public function removeFile(string $path): bool;

    /**
     * @return string[]
     */
    abstract public function listAll(): array;
}
