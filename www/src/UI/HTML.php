<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\UI;

use App\Constant;
use App\Controller\ProjectController;
use App\Entity\AwardConsecutiveDaysSession;
use App\Entity\Frame;
use App\Entity\FrameChessboard;
use App\Entity\FrameLayer;
use App\Entity\GPU;
use App\Entity\Message;
use App\Entity\News;
use App\Entity\PastProject;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\User;
use App\Repository\AdventCalendarClaimedDayRepository;
use App\Repository\AwardRepository;
use App\Repository\MessageRepository;
use App\Repository\ProjectRepository;
use App\Repository\RenderDayRepository;
use App\Repository\SessionPastRepository;
use App\Repository\SessionRepository;
use App\Repository\StatsUserRepository;
use App\Repository\TileRepository;
use App\Repository\UserRepository;
use App\Service\Clustering;
use App\Service\ConfigService;
use App\Service\Main;
use App\Utils\Misc;
use DateTime;
use DateTimeZone;
use Exception;
use Mobile_Detect;
use NumberFormatter;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Display HTML UI
 */
class HTML {
    public const THEME_DARK = 'dark';
    public const THEME_LIGHT = 'light';
    
    private ?User $user; // Logged (or anonymous) user
    private array $config;
    private UrlGeneratorInterface $router;
    private RequestStack $requestStack;
    private Main $main;
//    private AdventCalendar $adventCalendar;
    private SessionRepository $sessionRepository;
    private UserRepository $userRepository;
    private ProjectRepository $projectRepository;
    private TileRepository $frameRepository;
    private AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository;
    private RenderDayRepository $renderDayRepository;
    private AwardRepository $awardRepository;
    private SessionPastRepository $sessionPastRepository;
    private StatsUserRepository $statsUserRepository;
    private MessageRepository $messageRepository;
    private TileRepository $tileRepository;
    
    public function __construct(Security $security, ConfigService $configService, UrlGeneratorInterface $router, Main $main, /* AdventCalendar $adventCalendar, */ TileRepository $tileRepository, SessionRepository $sessionRepository, TileRepository $frameRepository, UserRepository $userRepository, AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository, ProjectRepository $projectRepository, RenderDayRepository $renderDayRepository, AwardRepository $awardRepository, SessionPastRepository $sessionPastRepository, StatsUserRepository $statsUserRepository, MessageRepository $messageRepository, RequestStack $requestStack) {
        /** @var ?User $user */
        $user = $security->getUser();
        $this->user = $user;
        $this->config = $configService->getData();
        $this->router = $router;
        $this->main = $main;
//        $this->adventCalendar = $adventCalendar;
        $this->tileRepository = $tileRepository;
        $this->sessionRepository = $sessionRepository;
        $this->userRepository = $userRepository;
        $this->projectRepository = $projectRepository;
        $this->frameRepository = $frameRepository;
        $this->adventCalendarClaimedDayRepository = $adventCalendarClaimedDayRepository;
        $this->renderDayRepository = $renderDayRepository;
        $this->awardRepository = $awardRepository;
        $this->sessionPastRepository = $sessionPastRepository;
        $this->statsUserRepository = $statsUserRepository;
        $this->messageRepository = $messageRepository;
        $this->requestStack = $requestStack;
    }
    
    public function printProjectStatus(array $cache_data_project): void {
        echo sprintf('Sent %s ago', Misc::humanTime(time() - $cache_data_project['time']));
    }
    
    public function printProjectLink(array $cache_data_project): void {
        $link = Misc::stringLimit($cache_data_project['name'], 25);
        if (is_object($this->user) && $this->user->isModerator()) {
            $link = '<a href="'.$this->router->generate("app_project_manage", ['project' => $cache_data_project['id']]).'">'.$link.'</a>';
        }
        echo $link;
    }
    
    public function printFaqItem(string $title, string $content, string $anchor, int $id, int $accordion_id): void { ?>
        <a anchor="<?php echo $anchor; ?>"></a>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion<?php echo $accordion_id ?>" href="#<?php echo $anchor; ?>">
                        <?php echo $title; ?>
                    </a>
                </h4>
            </div>
            <div id="<?php echo $anchor; ?>" class="panel-collapse collapse ">
                <div class="panel-body">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <?php
    }
    
    public function printFaqItems(string $section_title, array $questions, int $accordion_id): void {
        echo '<h3>'.$section_title.'</h3>';
        echo '<div class="panel-group" id="accordion'.$accordion_id.'">';
        $id = 1;
        foreach ($questions as $question) {
            $title = $question['title'];
            $contents = $question['contents'];
            $link = $question['link'];
            if ($link == '') {
                $link = 'faq'.Misc::str2num($title);
            }
            self::printFaqItem($title, $contents, $link, $id, $accordion_id);
            $id++;
        }
        echo '</div>';
    }
    
    public function printOwnerLink(string $owner): void {
        if ($owner != '') {
            echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $owner]).'">'.Misc::stringLimit($owner, 25).'</a>';
        }
    }
    
    public function retUserProfileLink(string $login): string {
        if ($login != '') {
            return '<a href="'.$this->router->generate('app_user_profile', ['user' => $login]).'">'.Misc::stringLimit($login, 25).'</a>';
        }
        else {
            return '';
        }
    }
    
    public function printOwnerAvatar(string $owner, bool $small= true): void {
        $size = $small ? 'small' : 'big';
        echo '<span class="avatar-'.$size.'">';
        $image = '<img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => $size, 'user' => $owner]).'" alt="" class="avatar">';
        if (is_object($this->user)) {
            $image = '<a href="'.$this->router->generate('app_user_profile', ['user' => $owner]).'">'.$image.'</a>';
        }
        echo $image;
        
        $user = $this->userRepository->find($owner);
        if (is_object($user) && $user->isDonator()) {
            echo '<img class="patreon-badge" data-src="/media/image/patreon-badge.svg" />';
        }
        
        echo '</span>';
    }
    
    public function printOwnerAvatarFooter(string $owner): void {
        echo '<span class="avatar-footer">';
        $image = '<img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => 'small', 'user' => $owner]).'" alt="" class="img-thumbnail pull-left">';
        if (is_object($this->user)) {
            $image = '<a href="'.$this->router->generate('app_user_profile', ['user' => $owner]).'">'.$image.'</a>';
        }
        echo $image;
        
        $user = $this->userRepository->find($owner);
        if (is_object($user) && $user->isDonator()) {
            echo '<a href="'.$this->router->generate('app_donation_main').'">';
            echo '<img class="patreon-badge" data-src="/media/image/patreon-badge.svg" />';
            echo '</a>';
        }
        
        echo '</span>';
    }
    
    public function printAvatar(string $userId, bool $linkToProfile= false, bool $linkToPatron= false): void {
        echo '<span class="avatar-small">';
        if ($linkToProfile) {
            echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $userId]).'">';
        }
        
        echo '<img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => 'small', 'user' => $userId]).'" class="avatar">';
        if ($linkToProfile) {
            echo '</a>';
        }
        
        $user = $this->userRepository->find($userId);
        if (is_object($user) && $user->isDonator()) {
            if ($linkToPatron) {
                echo '<a href="';
                echo $this->router->generate('app_donation_main');
                echo '"/>';
            }
            
            echo '<img class="patreon-badge" data-src="/media/image/patreon-badge.svg" />';
            
            if ($linkToPatron) {
                echo '</a>';
            }
        }
        echo '</span>';
    }
    
    public function printOrangeHeader(string $title, string $content, string $label, bool $show_ads= true): void { ?>
        <section class="slice color-one">
            <div class="w-section inverse">
                <?php
                if ($show_ads) {
                    self::displayGoogleAdsense();
                }
                ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h2><?php echo $title; ?></h2>
                                <p><?php echo $content; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
            <div style="height:26px;"></div>
            <div class="section-title color-three">
                <h3><?php echo $label; ?></h3>
            </div>
        </section>
        <?php
    }
    
    public function printInfo(string $title, string $content): void { ?>
        <section class="slice color-two">
            <div class="w-section inverse">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h2><i class="fa fa-rocket"></i> <?php echo $title; ?></h2>
                                <p><?php echo $content; ?></p>
                                <span class="clearfix"></span>
                                <div class="text-center">
                                    <a href="<?php echo $this->router->generate('app_getstarted_main'); ?>" class="btn btn-lg btn-one margin-t-20" title="">Get started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </section>
        <?php
    }
    
    public function displayGoogleAdsense(): void {
        if ((is_null($this->user) || $this->user->isDonator() == false) && $this->config['adsense']['enable']) {
            $detect = new Mobile_Detect();
            if ($detect->isMobile() || $detect->isTablet()) {
                echo $this->config['adsense']['contents_mobile'];
            }
            else {
                echo $this->config['adsense']['contents'];
            }
        }
    }
    
    public function printBlend(Project $project): void {
        $infos = array();
        
        if ($this->user->isModerator()) {
            $infos [] = sprintf('Owner: %s', $this->retUserProfileLink($project->getOwner()->getId()));
            $infos [] = sprintf('Uploaded on: %s', $this->getTimeWithLocalTimeZone('H:i M d Y', $project->getLastUpdateArchive()));
            $infos [] = sprintf('Archive size: %sB', Misc::humanSize($project->getArchiveSize()));
            
        }
        
        $time = (int)($project->estinationEndOfRender());
        if ($time != PHP_INT_MAX) {
            $time78 = Misc::humanTime($time);
            $infos [] = sprintf('The project will be done in %s', $time78);
        }
        
        $infos [] = sprintf('Storage used: %sB', Misc::humanSize($project->getShepherd()->getStorageUsed($project)));
        if ($this->user->isModerator()) {
            $infos [] = sprintf('Storage used prediction: %sB', Misc::humanSize($project->getShepherd()->getStoragePrediction($project)));
        }
        
        $project_frames = $project->tiles();
        $cumulated_time = 0;
        $time_first_frame = time() + 10; // max value
        $time_last_frame = $project->getCacheLastValidation();
        
        $frames_a_faire = count($project_frames);
        foreach ($project_frames as $a_frame) {
            if ($a_frame->getStatus() == Constant::FINISHED || $a_frame->getStatus() == Constant::PAUSED) {
                $frames_a_faire--;
            }
            if ($a_frame->getStatus() == Constant::FINISHED) {
                $cumulated_time += $a_frame->getRenderTime();
                
                if ($a_frame->getRequestTime() < $time_first_frame) {
                    $time_first_frame = $a_frame->getRequestTime();
                }
            }
        }
        
        if ($cumulated_time > 0) {
            $infos [] = sprintf('Cumulated time of render: %s', Misc::humanTime($cumulated_time, false));
            $infos [] = sprintf('Points spent: %s points', number_format($project->getCost()));
        }
        if ($frames_a_faire == 0 && count($project_frames) != 0) {
            $infos [] = sprintf('Real duration of render: %s', Misc::humanTime($time_last_frame - $time_first_frame, false));
        }
        $ref = $project->getAverageRenderTimeMachineRef();
        $style_warning = $ref > $this->config['power']['rendertime_max_reference'] ? 'style="color:red"' : '';
        if ($this->user->isModerator()) {
            $infos [] = sprintf('Actual per frame rendertime: <span %s>%s</span>', $style_warning, Misc::humanTime($project->getAverageRenderTime(), true));
        }
        $infos [] = sprintf('On reference per frame rendertime: <span %s>%s</span>', $style_warning, Misc::humanTime($ref, true));
        
        if ($this->user->isModerator()) {
            if ($project->getZipGenerated() > 1) { // not 0 because it was a boolean before (true <=> 1)
                $infos [] = sprintf('Duration generation ZIP: %s', Misc::humanTime($project->getZipGenerated() - $time_last_frame, false));
            }
            if ($project->getMp4FinalGenerated() > 1) { // not 0 because it was a boolean before (true <=> 1)
                $infos [] = sprintf('Duration generation MP4 final: %s', Misc::humanTime($project->getMp4FinalGenerated() - $time_last_frame, false));
            }
            if ($project->getMp4PreviewGenerated() > 1) { // not 0 because it was a boolean before (true <=> 1)
                $infos [] = sprintf('Duration generation MP4 preview: %s', Misc::humanTime($project->getMp4PreviewGenerated() - $time_last_frame, false));
            }
        }
        
        if ($cumulated_time > 0) {
            $infos [] = '<a href="'.$this->router->generate('app_project_chart', ['project' => $project->getId()]).'" />Statistics about the render</a>';
        }
        
        $ram = $project->getMaxMemoryUsed();
        if ($ram != 0) {
            $infos []= sprintf('RAM usage: %s', Misc::humanSize(intval($ram) * 1000).'B');
        }
        
        if ($project->isFinished()) {
            $target = $time_last_frame + $this->config['storage']['duration']['project']['rendered'] * 86400;
            $date = new DateTime();
            $date->setTimestamp($target);
            if ($date->format('H') == 0 && $date->format('i') < 10) {
                // between midnight and  00h10 (time of daily cron)
                $date->setTimestamp($target + 86400);
            }
            $date->setTime(0, 10, 0);
            $infos []= '<span style="color:red">Project will be automatically deleted on '.$this->getTimeWithLocalTimeZone('dS M H:i', $date->getTimestamp()).'</span>';
        }
        
        echo '<div class="w-box blog-post">';
        echo '    <h2>Summary</h2>';
        echo '    <ul>';
        foreach ($infos as $info) {
            echo '<li>'.$info.'</li>';
        }
        echo '    </ul>';
        echo '</div>';
        
        if ($project->getPublicRender() == false) {
            $status = $project->getStatus();
            if ($status != Constant::PAUSED && $status != Constant::FINISHED) {
                echo '<div class="alert alert-warning" role="alert">';
                echo "Your project is <strong>private</strong>, not every worker will be able to participate in your project.<br>To make it renderable by everyone, check 'Renderable by all members' on the renderers options.";
                echo '</div>';
            }
        }
        
        if ($project->isBlocked()) {
            echo '<div class="alert alert-danger" role="alert">';
            if (array_key_exists($project->getBlocked(), Project::BLOCK_MESSAGES)) {
                if (Project::BLOCK_MESSAGES[$project->getBlocked()]['ownercanrender']) {
                    echo sprintf("Your project has been blocked because '%s'.<br>Members can not render your project BUT you still can.", sprintf(Project::BLOCK_MESSAGES[$project->getBlocked()]['long'], $this->config['power']['rendertime_max_reference'] / 60));
                }
                else {
                    /** @phpstan-ignore-next-line */
                    echo sprintf("Your project has been blocked because '%s'.<br>Members (including you) can not render your project.", sprintf(Project::BLOCK_MESSAGES[$project->getBlocked()]['long'], $this->config['power']['rendertime_max_reference'] / 60));
                }
            }
            else {
                echo "Your project has been blocked.";
            }
            echo '</div>';
        }
        
        if ($project->getOwner()->getPoints() <= 0.0 && $project->getStatus() != Constant::FINISHED) {
            echo '<div class="alert alert-danger" role="alert">';
            echo 'You are currently rated limited to '.number_format($this->config['project']['rate_limit']['max_rendering_tile']).' machines because you have 0 points.<br>';
            echo 'You can remove this limitation by connecting your machine to the farm.<br>';
            echo 'As long as you are rendering, your project will be unthrottled.';
            echo '</div>';
        }
        
        if ($project->getShepherd()->getTimeout()) {
            echo '<div class="alert alert-danger" role="alert">';
            echo 'The server hosting your project is currently not available.<br>No download or rendering is possible.<br><a href="'.$this->router->generate('app_servers_main').'">More information about SheepIt network on the servers page</a>';
            echo '</div>';
        }
        
        $can_manage = $project->canManageProject($this->user);
        
        // Status
        $cache = $project->getCachedStatistics();
        $status = $project->getStatus();
        $human = $this->humanStatus($status);
        
        // Progression
        $progression = $cache[Constant::FINISHED].'/'.$cache[Constant::STATS_TOTAL];
        if (($cache[Constant::STATS_TOTAL] != 0) and ($cache[Constant::STATS_TOTAL] != $cache[Constant::FINISHED]) and $status != Constant::PAUSED) {
            $time = (int)($project->estinationEndOfRender());
            if ($time != PHP_INT_MAX) {
                $time78 = Misc::humanTime($time);
                $progression .= ' (remaining '.$time78.')';
            }
        }
        
        // Actions
        $actions = '';
        $actions .= '<a href="'.$this->router->generate("app_project_frames", ['project' => $project->getId()]).'" class="btn btn-neutral btn-round" title="See frames"><i class="fa fa-search-plus"></i></a> ';
        if ($can_manage && $project->isFinished()) {
            if ($project->getZipGenerated() > 0) {
                $actions .= '<a class="btn btn-success btn-round" href="'.$project->getZipOnShepherd().'" title="Download frames"><i class="fa fa-download"></i></a> ';
            }
            else {
                $task_status = $project->getShepherd()->getTaskQueuePosition($project);
                $generating_archive_text = '<strong>Generating archive.</strong>';
                if (is_array($task_status)) {
                    $NumberFormatter_US = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
                    $generating_archive_text .= '<br>Current position: '.$NumberFormatter_US->format($task_status['queue_position']);
                    $generating_archive_text .= '<br>Tasks for this project: '.number_format($task_status['tasks']);
                    $generating_archive_text .= '<br>Total tasks on Shepherd: '.number_format($task_status['total_task']);
                }
                $actions .= '<a href="#" class="btn btn-primary btn-round" title="'.$generating_archive_text.'"><i class="fa fa-cog fa-spin"></i></a> ';
            }
            
            if ($project->getUseExr() == false && $project->getMp4FinalGenerated() > 0) {
                $actions .= '<a class="btn btn-success btn-round" href="'.$project->getMp4FinalOnShepherd().'" title="Download video"><i class="fa fa-file-video"></i></a> ';
            }
        }
        else {
            if ($can_manage && count($project->tilesWithStatus(Constant::FINISHED)) > 0) {
                if ($project->getZipGenerated() == 0) {
                    $actions .= '<a class="btn btn-success btn-round" title="Ask for a partial Archive Frame" href="#" onclick="doAskForProjectPartialArchiveFrame(\''.$this->router->generate('app_project_askpartialframearchive', ['project' => $project->getId()]).'\');return false;"><i class="fa fa-download"></i></a> ';
                }
                else {
                    $actions .= '<a class="btn btn-success btn-round" href="'.$project->getZipOnShepherd().'" title="Download frames"><i class="fa fa-download"></i></a> ';
                }
            }
        }
        if ($status == Constant::PAUSED) {
            $max_concurent = $this->config['project']['max_concurrent_project_per_user'];
            $current = $project->getOwner()->getNumberOfConcurrentProject();
            if ($current >= $max_concurent) {
                $actions .= '<a href="#" class="btn btn-success btn-round disabled" title="Resume is disabled because you have too many active projects"><i class="fa fa-play"></i></a>';
            }
            else {
                $actions .= '<a href="#" class="btn btn-success btn-round" onclick="projectAction(\''.$project->getId().'\', \'resume\'); return false" id="project_job_'.$project->getId().'_action1" title="Resume"><i class="fa fa-play"></i></a> ';
            }
        }
        if ($can_manage && ($status == Constant::WAITING or $status == Constant::PROCESSING)) {
            $actions .= '<a href="#" class="btn btn-warning btn-round" onclick="projectAction(\''.$project->getId().'\', \'pause\'); return false" id="project_job_'.$project->getId().'_action2" title="Pause""><i class="fa fa-pause"></i></a> ';
        }
        if ($this->user->isModerator() && ($status == Constant::PAUSED or $status == Constant::FINISHED)) {
            $actions .= '<a href="#" class="btn btn-danger btn-round" onclick="projectAction(\''.$project->getId().'\', \'reset\'); return false" id="project_job_'.$project->getId().'_action3" title="Reset"><i class="fa fa-close"></i></a> ';
        }
        if ($can_manage) {
            $actions .= '<a href="#" class="btn btn-danger btn-round" onclick="projectAction(\''.$project->getId().'\', \'remove_no_redirect\'); return false" id="project_job_'.$project->getId().'_action4" title="Remove"><i class="fa fa-trash"></i></a> ';
        }
        
        ?>

        <div class="w-box blog-post">
            <div class="row">
                <div class="col-md-8">
                    <h2 id="project_job_<?php echo $project->getId(); ?>_path"><?php echo Misc::stringLimit($project->getPath(), 57); ?></h2>
                    <?php
                    $new_style = '';
                    if ($this->user->isModerator()) {
                        echo '<div style="display:block;padding:0; border-left:0;padding:0px 0px 0px 15px;"  >';
                        echo $this->getHumanType($project);
                        echo '<br >';
                        echo 'Engine: '.$project->getEngine();
                        if ($project->getEngine() == 'CYCLES') {
                            echo ' ';
                            echo 'samples: '.number_format($project->getSamples());
                        }
                        if ($project->getStatus() == Constant::PROCESSING || $project->getStatus() == Constant::WAITING) {
                            echo '<br>';
                            $cluster = $this->projectRepository->getCluster($project);
                            echo 'Cluster: ';
                            echo Clustering::toHuman($cluster);
                        }
                        echo '</div>';
                        echo '<div style="display:block;padding:0; border-left:0;padding:0px 0px 0px 15px;"  >';
                        echo 'shepherd: <a href="'.$project->getShepherd()->getId().'/admin/status">'.MISC::humanServerHost($project->getShepherd()->getId()).'</a>';
                        echo '</div>';
                        $new_style = 'style="padding-top: 0px"';
                    }
                    ?>
                    <ul class="meta-list" <?php echo $new_style; ?> >
                        <li class="msg_<?php echo $human['style'] ?>"><?php echo $human['text'] ?></li>
                        <li id="project_job_<?php echo $project->getId(); ?>_progression">
                            <?php echo $progression; ?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-right" id="project_job_<?php echo $project->getId(); ?>_div_actions" style="padding:15px 30px;">
                    <?php echo $actions; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 tiles">
                    <?php $this->UIProgression($project); ?>
                </div>
            </div>
            
            <?php
            $users = array();
            $processing_tiles = $project->tilesWithStatus(Constant::PROCESSING);
            foreach ($processing_tiles as $tile) {
                if (is_object($tile->getUser())) {
                    $users[$tile->getUser()->getId()] = $tile->getUser()->getId();
                }
            }
            natcasesort($users);
            
            if (count($users) > 0) {
                echo '<div class="row">';
                echo '<div class="col-md-12 tiles">';
                echo 'Current renderers:<br >';
                echo '<div class="row">';
                foreach ($users as $id) {
                    echo '<div class="col-md-3"><a href="'.$this->router->generate('app_user_profile', ['user' => $id]).'"><img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => 'small', 'user' => $id]).'" alt="" class="img-thumbnail pull-left">&nbsp;'.$id.'</a></div>';
                }
                echo '</div>';
                echo '</div>';
                echo '</div>';
            }
            if ($this->user->isModerator() && count($processing_tiles) > 0) {
                ?>
                <table class="table table-bordered table-striped table-comparision table-responsive sortable">
                    <thead>
                    <tr>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Frame</a></th>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Tile</a></th>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc">User</a></th>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Started (ago)</a></th>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Estimated</a></th>
                        <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Remaining</a></th>
                        <th style="text-align: center; vertical-align: middle;">Machine</th>
                        <th style="text-align: center; vertical-align: middle;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($processing_tiles as $tile) {
                        $duration = time() - $tile->getRequestTime();
                        
                        // Estimated time
                        $est_time_value = $tile->getEstimatedRenderTime();
                        $style = '';
                        if ($est_time_value != 0 && $this->user->isAdmin() && ($this->config['estimation_work']['coef'] * $est_time_value) < $duration) {
                            $style = ' style="color: red;"';
                        }
                        $est_time = '<span '.$style.'>'.Misc::humanTime($est_time_value, false).'</span>';
                        
                        // Remaining time
                        $rem_time = '';
                        $rem_time_value = '';
                        if ($tile->getRemainingRenderTime() != 0) {
                            $rem_time = Misc::humanTime($tile->getRemainingRenderTime());
                            $rem_time_value = $tile->getRemainingRenderTime();
                        }
                        
                        // machine
                        $session = $this->sessionRepository->find($tile->getSession());
                        $machine_str = 'unknown';
                        if (is_object($session)) {
                            $machine_str = '<a href="'.$this->router->generate("app_session_single", ['sessionid' => $tile->getSession()]).'">'.$session->getRenderingComputeDeviceStrHuman($tile).'</a>';
                        }
                        ?>
                        <tr>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo $tile->getFrame()->getNumber(); ?>"><?php echo $tile->getFrame()->getNumber(); ?></td>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo $tile->getNumber(); ?>"><?php echo $tile->getNumber(); ?></td>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo is_null($tile->getUser()) ? '' : $tile->getUser()->getId(); ?>"><?php echo $this->retUserProfileLink(is_null($tile->getUser()) ? '' : $tile->getUser()->getId()); ?></td>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo $duration; ?>"><?php echo Misc::humanTime((int)($duration / 60.0) * 60); ?></td>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo $est_time_value; ?>"><?php echo $est_time; ?></td>
                            <td style="text-align: center; vertical-align: middle;" data-sort="<?php echo $rem_time_value; ?>"><?php echo $rem_time > 0 ? $rem_time : ''; ?></td>
                            <td style="text-align: center; vertical-align: middle;"><?php echo $machine_str; ?></td>
                            <td style="text-align: center; vertical-align: middle;">
                                <form class="form-inline" action="javascript:;">
                                    <input type="button" class="btn btn-danger btn-xs" name="mass_delete" onclick="requestReloadOnSuccess('<?php echo $this->router->generate('app_tile_reset', ['tile' => $tile->getId()])?>', {}); return false" value="Reset"/>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            }
        echo '</div>';
    }
    
    public function printProjectTRHeader(array $extras= array()): void {
        echo '<thead>';
        echo '<th><a class="sortable" href="#">Project</a></th>';
        echo '<th><a class="sortable" href="#">Owner</a></th>';
        echo '<th><a class="sortable" href="#">Status</a></th>';
        echo '<th><a class="sortable" href="#" data-type="int">Progress</a></th>';
        echo '<th><a class="sortable" href="#" data-type="int">Device</a></th>';
        
        if (in_array('public_render', $extras)) {
            echo '<th>Public project</th>';
        }
        
        if (in_array('average_rendertime', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Average render time</a></th>';
        }
        
        if (in_array('cluster', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="string">Cluster</a></th>';
        }
        
        
        if (in_array('last_validation', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Last validation</a></th>';
        }
        
        if (in_array('user_error', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">User errors</a></th>';
        }
        if (in_array('memory_used', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Memory used</a></th>';
        }
        if (in_array('gpu_memory_used', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">vram used</a></th>';
        }
        if (in_array('creation_time', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Creation</a></th>';
        }
        if (in_array('points_on_creation', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Points</a></th>';
        }
        if (in_array('samples', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">samples</a></th>';
        }
        if (in_array('shepherd', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc">shepherd</a></th>';
        }
        if (in_array('blocked', $extras)) {
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Blocked</a></th>';
        }
        
        echo '</thead>';
    }
    
    public function printProjectTR(Project $project, bool $visible= true, array $extras= array()): void {
        if ($project->getId() == 0) {
            return;
        }
        
        $tr_attributes = '';
        $blocked = $project->getBlocked() != 0 ? ' class="blocked"' : '';
        if ($project->getOwner()->getId() == $this->user->getId() && $this->user->isModerator() == false) {
            $blocked = '';
        }
        if ($visible == false || ($blocked != '' && $this->user->isModerator() == false)) {
            $tr_attributes = ' style="display: none;"';
        }
        else {
            if ($this->user->isModerator()) {
                $tr_attributes .= $blocked;
            }
            
        }
        
        $stats = $project->getCachedStatistics();
        $total = $stats['total'];
        
        $finished = $stats[Constant::FINISHED];
        
        $human = $this->humanStatus($project->getStatus());
        
        if ($total != 0) {
            $percentage = (int)($finished * 100 / $total);
            $percentage_rendering = (int)($stats[Constant::PROCESSING] * 100 / $total);
        }
        else {
            $percentage = 100;
            $percentage_rendering = 0;
        }
        
        $nb_worker = $stats[Constant::PROCESSING];
        
        # one : rouge / two : bleu foncé / three : noir / four: blanc
        $progress_bar_color = 'progress-bar-two';
        $status_txt = $human['text'];
        $status_order = $project->getStatus();
        if ($project->getStatus() == Constant::PROCESSING) {
            $progress_bar_color = 'progress-bar-one';
            if ($nb_worker > 0) {
                $status_txt = sprintf('%d %s frames', $nb_worker, $status_txt);
                $status_order = $project->getStatus().'_'.sprintf('%04d', 10000 - $nb_worker);
            }
        }
        else {
            if ($project->getStatus() == Constant::FINISHED) {
                $progress_bar_color = 'progress-bar-three';
            }
        }
        
        $done_in = '';
        if ($project->getStatus() == Constant::PROCESSING) {
            if (in_array('estimation_endofrender', $extras)) {
                $time = (int)($project->estinationEndOfRender());
                if ($time != PHP_INT_MAX) {
                    if ($time > 172800) { // more than 2 days
                        $done_in = sprintf(' - done in %s', Misc::humanTime($time, false, false));
                    }
                    else {
                        $done_in = sprintf(' - done in %s', Misc::humanTime($time));
                    }
                }
            }
        }
        
        if (in_array('last_validation', $extras)) {
            $last_validation = $stats[Constant::STATS_LAST_VALIDATION];
        }
        else {
            $last_validation = 0;
        }
        
        echo '<tr data-project_id="'.$project->getId().'"'.$tr_attributes.'>';
        
        $link_to_project = $project->getName();
        if ($project->canSeeProject($this->user)) {
            $link_to_project = '<a href="'.$this->router->generate("app_project_manage", ['project' => $project->getId()]).'">'.$project->getName().'</a>';
        }
        
        echo '<td class="project_link" style="overflow-wrap: break-word;" data-sort="'.$project->getName().'"><div class="truncatable"><strong>'.$link_to_project.'</strong></div></td>';
        
        echo '<td class="table_avatar" data-sort="'.$project->getOwner()->getId().'" style="word-break: break-word;">';
        $this->printAvatar($project->getOwner()->getId(), true, true);
        echo '&nbsp;';
        echo '<span style="display: none;">'.$project->getOwner()->getId().'</span>';
        $this->printOwnerLink($project->getOwner()->getId());
        echo '</td>';
        echo '<td class="status" data-sort="'.$status_order.'">';
        echo '<span class="msg_'.$human['style'].'">'.$status_txt.'</span>';
        echo $done_in;
        echo '</td>';
        echo '<td class="progressbar-width" data-sort="'.$percentage.'">'; // TODO Remove this and set better width
        ?>
        <div class="progress progress-striped">
            <div class="progress-bar <?php echo $progress_bar_color; ?>" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%">
                <span class="sr-only"><?php echo $finished; ?> / <?php echo $total; ?></span>
            </div>
            <div class="progress-bar progress-bar-five" role="progressbar" style="width: <?php echo $percentage_rendering; ?>%">
            </div>
        </div>
        <?php
        echo '</td>';
        echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$project->getComputeMethod().'">'; // compute method
        if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU)) {
            echo '<img src="/media/image/cpu_enabled.png" title="CPU enabled" alt="cpu enabled"/>';
        }
        else {
            echo '<img src="/media/image/cpu_disabled.png" title="CPU disabled" alt="cpu disabled"/>';
        }
        echo '&nbsp;';
        if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU)) {
            echo '<img src="/media/image/gpu_enabled.png" title="GPU nvidia enabled" alt="gpu enabled"/>';
        }
        else {
            echo '<img src="/media/image/gpu_disabled.png" title="GPU nvidia disabled" alt="gpu disabled"/>';
        }
        
        echo '</td>';
        
        if (in_array('public_render', $extras)) {
            echo '<td style="text-align: center; vertical-align: middle;" >';
            echo '<span style="display: none;"></span>';
            if ($project->getPublicRender()) {
                echo 'Yes';
            }
            else {
                echo '<span style="color: red">';
                echo 'No';
                echo '</span>';
            }
            echo '</td>';
        }
        
        if (in_array('average_rendertime', $extras)) {
            $val_rendertime = $project->getAverageRenderTime();
            $val_power = $project->getAverageRenderTimeMachineRef();
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$val_power.'">';
            if ($val_rendertime != 0 && $val_power != 0) {
                echo Misc::humanTime($val_rendertime, true);
                echo ' / ';
                echo '<span style= "color: ';
                echo $val_power > $this->config['power']['rendertime_max_reference'] ? 'red' : 'green';
                echo ';">'.Misc::humanTime((int)($val_power), true).'</span>';
            }
            echo '</td>';
        }
        
        if (in_array('cluster', $extras)) {
            $cluster = $this->projectRepository->getCluster($project);
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$cluster.'">';
            echo Clustering::toHuman($cluster);
            echo '</td>';
        }
        
        if (in_array('last_validation', $extras)) {
            echo '<td style="text-align: center; vertical-align: middle;"  data-sort="'.(int)(time() - $last_validation).'">';
            if ($last_validation != 0) {
                echo Misc::humanTime(time() - $last_validation, true);
            }
            echo '</td>';
        }
        
        if (in_array('user_error', $extras)) {
            $user_error = $project->getUserError();
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$user_error.'">';
            if ($user_error != 0) {
                echo $user_error;
            }
            echo '</td>';
        }
        
        
        if (in_array('memory_used', $extras)) {
            $ram = $project->getMaxMemoryUsed();
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$ram.'">';
            if ($ram != 0) {
                echo Misc::humanSize($ram * 1000).'B';
            }
            echo '</td>';
        }
        
        if (in_array('gpu_memory_used', $extras)) {
            echo '<td style="text-align: center; vertical-align: middle;" >';
            $ram_success = $project->getVramSuccess();
            $ram_failure = $project->getVramFailure();
            if ($ram_success != PHP_INT_MAX) {
                echo '<span style="color:green">'.Misc::humanSize($ram_success).'B'.'</span>';
                if ($ram_failure != 0) {
                    echo '/';
                }
            }
            if ($ram_failure != 0) {
                echo '<span style="color:red">'.Misc::humanSize($ram_failure).'B'.'</span>';
            }
            echo '</td>';
        }
        
        if (in_array('creation_time', $extras)) {
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$project->getLastUpdateArchive().'">';
            echo $this->getTimeWithLocalTimeZone('M d H:i', $project->getLastUpdateArchive());
            echo '</td>';
        }
        
        if (in_array('points_on_creation', $extras)) {
            $points_on_creation = $project->getOwnerPointsOnlastupdate();
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$points_on_creation.'">';
            echo '<span style="color:'.($points_on_creation >= 0 ? 'green' : 'red').'">';
            echo Misc::humanSize((float)$points_on_creation);
            echo '</span>';
            echo '</td>';
        }
        
        if (in_array('samples', $extras)) {
            $samples = $project->getSamples();
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$samples.'">';
            if ($samples != 0) {
                echo $samples;
            }
            echo '</td>';
        }
        
        if (in_array('shepherd', $extras)) {
            $shepherd = $project->getShepherd();
            $human_host = MISC::humanServerHost($shepherd->getId());
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$human_host.'">';
            echo $human_host;
            echo '</td>';
        }
        
        if (in_array('blocked', $extras)) {
            $blocked = $project->getBlocked();
            
            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$blocked.'">';
            if (array_key_exists($blocked, Project::BLOCK_MESSAGES)) {
                echo Project::BLOCK_MESSAGES[$blocked]['short'];
            }
            echo '</td>';
        }
        
        echo '</tr>';
    }
    
    public function formAddProject(string $project_name, string $working_dir, string $token, array|int $blend_files): void {
        if (is_dir($working_dir) == false) {
            $error_message = 'Could not open the file';
            $this->printError('An error occured...', $error_message);
            return;
        }
//        $blend_files = $this->blendService->list_blend_files_from_directory($working_dir);
//        File::rmdir_recurse($working_dir);
        
        if ($blend_files === -9) { // windows does not support some characters on path
            $this->printError('An error occured...', 'Cannot use this file because it (or the files it contains) have an unsupported character. Character forbidden: \/ \\ : * ? \" < > |');
            return;
        }
        
        if ($blend_files === -8) { // -8 ascii character
            $this->printError('An error occured...', 'Cannot use this file because it (or the files it contains) have at least one path with an unsupported, or non-ascii, character.');
            return;
        }
        
        if (count($blend_files) == 0) {
            $this->printError('An error occured...', 'No blender file have been detected on your archive.');
            return;
        }
        else {
            $use_exr = false;
            foreach ($blend_files as $blend_name => $blend_info) {
                if (is_array($blend_info) && array_key_exists('output_file_extension', $blend_info) && $blend_info['output_file_extension'] == '.exr') {
                    $use_exr = true;
                }
            }
            
            // queue position
            $numberFormatter_US = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
            $user_place_with_team_cpu = 1;
            $user_place_with_team_gpu = 1;
            $nb_total_project_cpu = 0;
            $nb_total_project_gpu = 0;
            $user_points_with_team = $this->user->getExtraPointsFromTeam();
            
            $projects = $this->projectRepository->findAll();
            foreach ($projects as $project) {
                $status = $project->getStatus();
                if ($status == Constant::PROCESSING || $status == Constant::WAITING) {
                    if ($project->isBlocked() == false && $project->getPublicRender()) {
                        if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU)) {
                            $nb_total_project_cpu++;
                            
                            if ($user_points_with_team < $project->getOwnerPointsOnlastupdate()) {
                                $user_place_with_team_cpu += 1;
                            }
                        }
                        else {
                            $nb_total_project_gpu++;
                            
                            if ($user_points_with_team < $project->getOwnerPointsOnlastupdate()) {
                                $user_place_with_team_gpu += 1;
                            }
                        }
                    }
                }
            }
            
            
            ?>
            <section class="slice color-two">
                <div class="w-section inverse">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                                <div class="w-section inverse">
                                    <div class="w-box dark">
                                        <div class="form-light padding-15">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="checkbox persistent">
                                                        <input type="checkbox" name="public_render" id="public_render" value="1" checked="checked"/>
                                                        <span title="By default every member can render your project. If you want to restrict the access to your project do not check this box. On the project administration page you will be able to modify this setting and add specific members to renderers.">Renderable by all members</span>
                                                    </label>
                                                    <?php
                                                    if ($use_exr) {
                                                        echo '<input type="hidden" id="generate_mp4" name="generate_mp4" value="0"/>';
                                                    }
                                                    else {
                                                    ?>
                                                    <label class="checkbox persistent">
                                                        <input type="checkbox" name="generate_mp4" id="generate_mp4" value="0"/>
                                                        <span title="Generates an MP4 video of the projects, it is really resource intensive for the server so only check it if you really need it.">Generate MP4 video</span>
                                                    </label>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if ($this->user->canDisableFrameThumbnail() == false) { ?>
                                                <input type="hidden" id="public_thumbnail" name="public_thumbnail" value="1" checked="checked"/>
                                                <?php
                                            }
                                            else {
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="checkbox persistent">
                                                            <input type="checkbox" name="public_thumbnail" id="public_thumbnail" value="1" checked="checked"/>
                                                            <span title="By default every member can see a thumbnail of your project. If you want to restrict the access to your project do not check this box. On the project administration page you will be able to modify this setting.">Thumbnail viewable by all members</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            $can_use_gpu = false;
                                            foreach ($blend_files as $blend_name => $blend_info) {
                                                if (array_key_exists('version', $blend_info) == false) {
                                                    continue;
                                                }
                                                if (array_key_exists('engine', $blend_info) && (
                                                        $blend_info['engine'] == 'CYCLES' ||
                                                        $blend_info['engine'] == 'BLENDER_WORKBENCH' ||
                                                        $blend_info['engine'] == 'BLENDER_EEVEE_NEXT' ||
                                                        $blend_info['engine'] == 'BLENDER_EEVEE')) {
                                                    $can_use_gpu = true;
                                                }
                                            }
                                            $can_use_cpu = true;
                                            foreach ($blend_files as $blend_name => $blend_info) {
                                                if (array_key_exists('engine', $blend_info) && ($blend_info['engine'] == 'BLENDER_WORKBENCH' || $blend_info['engine'] == 'BLENDER_EEVEE' || $blend_info['engine'] == 'BLENDER_EEVEE_NEXT')) {
                                                    $can_use_cpu = false;
                                                    break;
                                                }
                                                if (array_key_exists('has_GPencil_object', $blend_info) && $blend_info['has_GPencil_object']) {
                                                    $can_use_cpu = false;
                                                    break;
                                                }
                                            }
                                            ?>

                                            <input type="hidden" id="token" name="token" value="<?php echo $token;?>"/>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="checkbox" style="text-align: left; vertical-align: middle;padding: 0;">Compute method:</label>
                                                </div>
                                                <?php
                                                if ($can_use_cpu) {
                                                    ?>
                                                    <div class="col-md-4">
                                                        <label class="checkbox">
                                                            <input type="radio" name="compute_method" id="compute_method_cpu" value="1" checked/>
                                                            <img src="/media/image/cpu_enabled.png" title="CPU" alt="cpu"/> CPU
                                                            
                                                        </label>
                                                        Est. queue position: <?php echo $numberFormatter_US->format($user_place_with_team_cpu)?><br>
                                                        Total projects: <?php echo number_format($nb_total_project_cpu); ?>
                                                    </div>
                                                    <?php
                                                }
                                                if ($can_use_gpu) {
                                                    ?>
                                                    <div class="col-md-4">
                                                        <label class="checkbox">
                                                            <input type="radio" name="compute_method" id="compute_method_gpu" value="8"/>
                                                            <img src="/media/image/gpu_enabled.png" title="GPU" alt="GPU"/> GPU
                                                        </label>
                                                        Est. queue position: <?php echo $numberFormatter_US->format($user_place_with_team_gpu)?><br>
                                                        Total projects: <?php echo number_format($nb_total_project_gpu); ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            $i = 0;
                                            foreach ($blend_files as $blend_name => $blend_info) {
                                                if ($i != 0) {
                                                    echo '<hr>';
                                                }
                                                
                                                if (is_array($blend_info) == false) {
                                                    /*echo '<tr><td><p class="important">Error, no info for file '.$blend_name.'</p></td></tr>';*/
                                                    continue;
                                                }
                                                $start_frame = '';
                                                if (array_key_exists('start_frame', $blend_info)) {
                                                    $start_frame = $blend_info["start_frame"];
                                                }
                                                $end_frame = '';
                                                if (array_key_exists('end_frame', $blend_info)) {
                                                    $end_frame = $blend_info["end_frame"];
                                                }
                                                $step_frame = '1';
                                                if (array_key_exists('step_frame', $blend_info)) {
                                                    $step_frame = $blend_info["step_frame"];
                                                }
                                                
                                                $render_on_gpu_headless = (array_key_exists('has_GPencil_object', $blend_info) && $blend_info['has_GPencil_object'] == false) && $blend_info['engine'] != 'BLENDER_EEVEE' && $blend_info['engine'] != 'BLENDER_EEVEE_NEXT';
                                                ?>
                                                <div id="addproject_content_<?php echo $i; ?>">
                                                    <input type="hidden" id="addproject_exe_<?php echo $i; ?>" name="addproject_exe_<?php echo $i; ?>" value="<?php echo array_key_exists('version', $blend_info) ? $blend_info['version'] : '';?>"/>
                                                    <input type="hidden" id="addproject_path_<?php echo $i; ?>" value="<?php echo $blend_name; ?>"/>
                                                    <h4 style="color: var(--color-form-bg)"><?php echo $blend_name; ?></h4>
                                                    <?php
                                                    if (array_key_exists('error', $blend_info) || $blend_info['have_camera'] == false || (array_key_exists('has_active_file_output_node', $blend_info) && $blend_info['has_active_file_output_node'])) {
                                                        echo '<div class="error" style="color:red">';
                                                        if (array_key_exists('error', $blend_info)) {
                                                            echo $blend_info['error'];
                                                            echo '<br>';
                                                        }
                                                        else {
                                                            if ($blend_info['have_camera'] == false) {
                                                                echo 'No camera in scene, cannot render.';
                                                            }
                                                            else {
                                                                if (array_key_exists('has_active_file_output_node', $blend_info) && $blend_info['has_active_file_output_node']) {
                                                                    echo '<center>';
                                                                    echo 'Since active "output file" nodes result in files being written to arbitrary locations on the renderer\'s system we do not allow it.<br>';
                                                                    echo 'We will accept your .blend if you mute the node.';
                                                                    echo '</center>';
                                                                }
                                                            }
                                                        }
                                                        echo '</div>';
                                                    }
                                                    else {
                                                        ?>
                                                        <form id="addproject_<?php echo $i; ?>" action="javascript:;" onsubmit="doAddProject(<?php echo $i; ?>); return false" class="form-light">
                                                            <input type="hidden" id="addproject_archive_<?php echo $i; ?>" value="<?php echo $project_name; ?>"/>
                                                            <input type="hidden" id="addproject_engine_<?php echo $i; ?>" value="<?php echo $blend_info['engine']; ?>"/>
                                                            <input type="hidden" id="addproject_render_on_gpu_headless_<?php echo $i; ?>" value="<?php echo $render_on_gpu_headless; ?>"/>
                                                            <input type="hidden" id="addproject_use_adaptive_sampling_<?php echo $i; ?>" value="<?php echo $blend_info['use_adaptive_sampling'] ? '1' : '0'; ?>"/>
                                                            <input type="hidden" id="addproject_framerate_<?php echo $i; ?>" value="<?php echo $blend_info['framerate']; ?>"/>
                                                            <input type="hidden" id="addproject_width_<?php echo $i; ?>" value="<?php echo $blend_info['resolution_x'] * $blend_info['resolution_percentage'] / 100.0; ?>"/>
                                                            <input type="hidden" id="addproject_height_<?php echo $i; ?>" value="<?php echo $blend_info['resolution_y'] * $blend_info['resolution_percentage'] / 100.0; ?>"/>
                                                            <input type="hidden" id="addproject_cycles_samples_<?php echo $i; ?>" value="<?php echo array_key_exists('cycles_samples', $blend_info) ? $blend_info['cycles_samples'] : ''; ?>"/>
                                                            <input type="hidden" id="addproject_samples_pixel_<?php echo $i; ?>" value="<?php echo array_key_exists('cycles_pixel_samples', $blend_info) ? $blend_info['cycles_pixel_samples'] : ''; ?>"/>
                                                            <input type="hidden" id="addproject_image_extension_<?php echo $i; ?>" value="<?php echo $blend_info['output_file_extension']; ?>"/>
                                                            
                                                            <?php
                                                            
                                                            if ($blend_info['output_file_extension'] == '.exr' || $blend_info['has_denoising'] == true) {
                                                                $style_animation = '';
                                                                ?>
                                                                <input type="radio" name="addproject_change_type_<?php echo $i; ?>" value="animation" checked=checked style="visibility: hidden"/>
                                                                <input type="hidden" id="addproject_split_tiles_number_<?php echo $i;?>" name="addproject_split_tiles_number_<?php echo $i; ?> value="1" />
                                                                <?php if ($blend_info['output_file_extension'] == '.exr') {
                                                                    ?>
                                                                    EXR output detected<br>Limitation on EXR  support:<br>
                                                                    <ul>
                                                                        <li>Full frame renders only. No split-layers or checkerboarding.</li>
                                                                        <li>Only animations are supported, no single image projects.</li>
                                                                        <li><?php echo sprintf('Maximum number of layers/passes: %d', $this->config['project']['exr']['maximum_layers']); ?></li>
                                                                        <li><?php echo sprintf('Maximum image dimensions %dx%dy px.', $this->config['project']['exr']['maximum_dimension']['width'], $this->config['project']['exr']['maximum_dimension']['height']); ?></li>
                                                                        <li>Use of compression is required (any of <?php echo join(', ', $this->config['project']['exr']['compressions_supported']); ?>).
                                                                    </ul>
                                                                <?php
                                                                }
                                                                else {
                                                                    echo 'Denoising detected: Splits (multiple smaller frames with reduced samples) are not supported.<br>It does not make sense to denoise separate splits and recombine them together.';
                                                                }
                                                                 ?>

                                                                <div class="row" id="addproject_animation_div10_<?php echo $i; ?>" <?php echo $style_animation; ?> >
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_start_frame">Start frame</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_start_frame_<?php echo $i; ?>" value="<?php echo $start_frame; ?>" size="6" maxlength="8" min="0"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_end_frame">End frame</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_end_frame_<?php echo $i; ?>" value="<?php echo $end_frame; ?>" size="6" maxlength="8" min="0" class="form-control"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_step_frame">Step</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_step_frame_<?php echo $i; ?>" value="<?php echo $step_frame; ?>" size="3" maxlength="3" class="form-control"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                            else {
                                                            
                                                                $style_animation = '';
                                                                $style_singleframe = '';
                                                                if ($start_frame != $end_frame) {
                                                                    $style_singleframe = 'style="display: none;"';
                                                                }
                                                                else {
                                                                    $style_animation = 'style="display: none;"';
                                                                }
                                                                
                                                                ?>
    
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="addproject_type">Type</label>
                                                                            <input type="radio" name="addproject_change_type_<?php echo $i; ?>" onclick="doAddProject_handle_change_of_type(<?php echo $i; ?>, 'singleframe');" value="singleframe" <?php echo $style_singleframe == '' ? 'checked=checked' : '' ?> />Single frame
                                                                            <input type="radio" name="addproject_change_type_<?php echo $i; ?>" onclick="doAddProject_handle_change_of_type(<?php echo $i; ?>, 'animation');" value="animation" <?php echo $style_animation == '' ? 'checked=checked' : '' ?> />Animation
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                                <div class="row" id="addproject_animation_div10_<?php echo $i; ?>" <?php echo $style_animation; ?> >
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_start_frame">Start frame</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_start_frame_<?php echo $i; ?>" value="<?php echo $start_frame; ?>" size="6" maxlength="8" min="0"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_end_frame">End frame</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_end_frame_<?php echo $i; ?>" value="<?php echo $end_frame; ?>" size="6" maxlength="8" min="0" class="form-control"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label for="addproject_step_frame">Step</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_animation_step_frame_<?php echo $i; ?>" value="<?php echo $step_frame; ?>" size="3" maxlength="3" class="form-control"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="addproject_animation_div11_<?php echo $i; ?>" <?php echo $style_animation; ?> >
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if ($blend_info['can_use_tile'] == false && $blend_info['engine'] == 'CYCLES') {
                                                                                ?>
                                                                                <label for="addproject_split_animation">
                                                                                    <?php
                                                                                    echo "To increase the render time allowed per frame, you can divide each frame into splits.<br>";
                                                                                    echo "Splits are full frame renders at a lower sample rate, which are then mixed together to generate the final frame at the full sample setting.<br>";
                                                                                    echo sprintf("You are allowed %d minutes per split and a maximum of %s splits.<br>", $this->config['power']['rendertime_max_reference'] / 60, number_format($this->config['project']['max_frames']));
                                                                                    echo sprintf("ex: If you divide your frames into 16 splits you will be up to %s splits.<br>", number_format($this->config['project']['max_frames'] / 4));
                                                                                    ?>
                                                                                </label>
                                                                                Divide each frame into
                                                                                <input type="range" min="1" max="64" value="0" step="1" id="addproject_split_animation_sample_range_value_<?php echo $i; ?>" onkeyup="doAddProject_split_samples(<?php echo $i; ?>, this.value)" onchange="doAddProject_split_samples('addproject_animation_split_sample_value_<?php echo $i; ?>', this.value)" />
                                                                                <span id="addproject_animation_split_sample_value_<?php echo $i; ?>">&nbsp;1 tile</span>
                                                                                <input type="hidden" id="addproject_split_tiles_number_<?php echo $i;?>" value="-1" />
                                                                                <br>
                                                                                <strong>If you split frames, <i>denoising</i> will be disabled.</strong><br>
                                                                                <?php
                                                                            }
                                                                            else {
                                                                            ?>
                                                                                <label for="addproject_split_animation">
                                                                                <?php
                                                                                echo "To increase the render time allowed per frame, you can divide each frame into tiles.<br>";
                                                                                echo "Tiles are grid-cut sections of each frame, which are then stitched together to generate the final full frame.<br>";
                                                                                echo sprintf("You are allowed %d minutes per tile and a maximum of %s tiles.<br>", $this->config['power']['rendertime_max_reference'] / 60, number_format($this->config['project']['max_frames']));
                                                                                echo sprintf("ex: If you divide your frames into 16 tiles you will be up to %s tiles.<br>", number_format($this->config['project']['max_frames'] / 4));
                                                                                ?>
                                                                                <br>
                                                                                Divide each frame into a tile grid of
                                                                                </label>
                                                                                <select id="addproject_split_tiles_number_<?php echo $i; ?>" name="addproject_split_tiles_number_<?php echo $i; ?>">
                                                                                    <option value="1" selected="selected">Full frame</option>
                                                                                    <option value="2">2x2</option>
                                                                                    <option value="4">4x4</option>
                                                                                    <option value="5">5x5</option>
                                                                                    <option value="6">6x6</option>
                                                                                </select>
    
                                                                                <br>
                                                                                <strong><i>Attention: Using tiles will disable compositing and denoising</i>.<br>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="addproject_singleframe_div20_<?php echo $i; ?>" <?php echo $style_singleframe; ?> >
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="addproject_start_frame">Frame</label>
                                                                            <input class="input_text_addproject form-control" id="addproject_singleframe_start_frame_<?php echo $i; ?>" value="<?php echo $start_frame; ?>" size="6" maxlength="8"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row" id="addproject_singleframe_div21_<?php echo $i; ?>" <?php echo $style_singleframe; ?> >
                                                                    <div class="col-md-8">
                                                                        <div class="form-group">
                                                                            <?php
                                                                            if ($blend_info['can_use_tile'] == false && $blend_info['engine'] == 'CYCLES') {
                                                                                ?>
                                                                                <label for="addproject_singleframe">
                                                                                    <?php
                                                                                    echo "To increase the render time allowed per frame, you can divide each frame into splits.<br>";
                                                                                    echo "Splits are full frame renders at a lower sample rate, which are then mixed together to generate the final frame at the full sample setting.<br>";
                                                                                    echo sprintf("You are allowed %d minutes per split.<br>", $this->config['power']['rendertime_max_reference'] / 60);
                                                                                    ?>
                                                                                </label>
                                                                                <label for="addproject_split_sample">Divide the frame into</label>
                                                                                <input type="range" min="4" max="32" value="0" step="1" id="addproject_split_sample_range_value_<?php echo $i; ?>" onkeyup="doAddProject_split_samples(<?php echo $i; ?>, this.value)" onchange="doAddProject_split_samples('addproject_split_sample_value_<?php echo $i; ?>', this.value)" />
                                                                                <span id="addproject_split_sample_value_<?php echo $i; ?>">&nbsp;4 tiles</span>
                                                                                <br>
                                                                                <strong>If you split frames, <i>denoising</i> will be disabled.</strong><br>
                                                                                <?php
                                                                            }
                                                                            else {
                                                                                echo '<label for="addproject_singleframe">';
                                                                                echo "To increase the render time allowed per frame, you can divide each frame into tiles.<br>";
                                                                                echo "Tiles are grid-cut sections of each frame, which are then stitched together to generate the final full frame.<br>";
                                                                                ?>
                                                                                </label>
                                                                                <label for="addproject_split_sample"><?php echo sprintf('The frame will be split in %dx%d tiles.', $this->config['project']['singleframe']['nb_tile'], $this->config['project']['singleframe']['nb_tile']) ?></label>
                                                                                <br>
                                                                                <strong><i>Attention: Using tiles will disable compositor and denoising</>.<br>
                                                                            <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            }
                                                            
                                                            ?>
                                                            <div class="row">

                                                            </div>
                                                            <div><input type="checkbox" name="checkbox_ad_<?php echo $i; ?>" id="checkbox_ad_<?php echo $i; ?>" value="" onchange="addproject_advanced_settings('<?php echo $i; ?>'); return false"/> Advanced options
                                                            </div>
                                                            <div class="row" id="checkbox_advanced_option_<?php echo $i; ?>" style="display: none;">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="addproject_start_frame" title="You can specify the memory used your project. If you think your project will take a lot of ram (more than 20GB), please fill the amount. You can find this value on the top right of Blender. It will help the renderfarm, by allowing the server to give a frame to a small configuration. It is an optional attribute, this value will be detected on the first frame rendered.">Memory used</label>
                                                                        <input class="input_text_addproject form-control" id="addproject_max_ram_optional_<?php echo $i; ?>" value="" size="12" maxlength="12" placeholder="Memory used in Mbytes"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            //TODO
                                                            if (array_key_exists('scripted_driver', $blend_info) && $blend_info['scripted_driver'] == 'True') {
                                                                echo '<span style="color:red">';
                                                                echo '<strong>Drivers will not work</strong> because scripts are disabled for security reasons.';
                                                                echo '</span>';
                                                                echo '<br>';
                                                            }
                                                            if (array_key_exists('missing_files', $blend_info) && count($blend_info['missing_files']) > 0) {
                                                                echo '<span style="color:red">';
                                                                echo '<strong>Warning, files not found:</strong>';
                                                                echo '</span>';
                                                                echo "<br>";
                                                                echo '<ul>';
                                                                foreach ($blend_info['missing_files'] as $missing_file) {
                                                                    echo '<li>'.$missing_file.'</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                            if (array_key_exists('warning', $blend_info)) {
                                                                echo '<span style="color:red">';
                                                                echo '<strong>Warning: </strong>'.$blend_info['warning'].'</span>';
                                                            }
                                                            ?>

                                                            <div class="row">
                                                                <div class="col-md-12" id="addproject_submit_div_<?php echo $i; ?>">
                                                                    <input type="submit" id="addproject_submit_<?php echo $i; ?>" value="Add this blend" class="btn btn-three pull-right"/>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div id="addproject_error_box_<?php echo $i; ?>"></div>
                                                </div>
                                                <?php
                                                $i += 1;
                                            }
                                            ?>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            <?php
        }
    }
    
    public function echoTD(int $nb_, string $content= ''): void {
        if ($nb_ > 0) {
            for ($i = 1; $i <= $nb_; $i++) {
                echo "<td>$content</td>";
            }
        }
    }
    
    /**
     * @return array an array with the human text to display and the css class to use
     */
    public function humanStatus(string $status_): array {
        $text = 'Unknown';
        $style = 'unknown';
        
        switch ($status_) {
            case Constant::PROCESSING:
                $style = 'processing';
                $text = 'Rendering';
                break;
            case Constant::FINISHED:
                $style = 'finished';
                $text = 'Rendered';
                break;
            case Constant::WAITING:
                $style = 'waiting';
                $text = 'Waiting';
                break;
            case Constant::PAUSED:
                $style = 'paused';
                $text = 'Paused';
                break;
        }
        
        return array('style' => $style, 'text' => $text);
    }
    
    public function status2CSSstyle(string $status_): string {
        $style = 'unknown';
        
        switch ($status_) {
            case Constant::PROCESSING:
                $style = 'processing';
                break;
            case Constant::FINISHED:
                $style = 'finished';
                break;
            case Constant::WAITING:
                $style = 'waiting';
                break;
            case Constant::PAUSED:
                $style = 'paused';
                break;
        }
        
        return $style;
    }
    
    public function printMessageLeft(Message $message, ?string $linkToFullConv= null, bool $show_receiver= false): void {
        $message_str = match ($message->getContent()) {
            Message::MESSAGE_PROJECT_ALLOW => 'Allow back user to upload a project',
            Message::MESSAGE_PROJECT_FORBID => 'Forbid user to upload a project',
            Message::MESSAGE_RENDERING_ALLOW => 'Allow user to render project',
            Message::MESSAGE_RENDERING_FORBID => 'Forbid user to render any projects (even their)',
            default => $message->getContent(),
        };
        
        echo '<div class="msg received">';
        echo '<div class="header">';
        echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $message->getSender()]).'" class="avatar">';
        echo '<img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => 'small', 'user' => $message->getSender()]).'" class="picture" />';
        echo '</a>';
        echo '<div '.($show_receiver ? '' : 'class="user"').'>';
        echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $message->getSender()]).'" class="name">';
        echo $message->getSender();
        echo '</a>';
        if ($show_receiver) {
            echo ' -> ';
            echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $message->getReceiver()]).'" class="name">';
            echo $message->getReceiver();
            echo '</a>';
            echo '<br>';
        }
        echo '<span class="timeStamp">'.$this->getTimeWithLocalTimeZone("M d Y H:i.s", $message->getTime()).'</span>';
        echo '</div>';
        echo '</div>';
        if (is_string($linkToFullConv)) {
            echo '<a href="'.$this->router->generate('app_user_messages', ['user' => $linkToFullConv]).'">';
        }
        echo '<span class="textMsg">'.strip_tags($message_str).'</span>';
        if (is_string($linkToFullConv)) {
            echo '</a>';
        }
        echo '</div>';
    }
    
    public function printMessageRight(Message $message, ?string $linkToFullConv= null): void {
        $message_str = match ($message->getContent()) {
            Message::MESSAGE_PROJECT_ALLOW => 'Allow back user to upload a project',
            Message::MESSAGE_PROJECT_FORBID => 'Forbid user to upload a project',
            Message::MESSAGE_RENDERING_ALLOW => 'Allow user to render project',
            Message::MESSAGE_RENDERING_FORBID => 'Forbid user to render any projects (even their)',
            default => $message->getContent(),
        };
        
        echo '<div class="msg send">';
        echo '<div class="header">';
        echo '<div class="user">';
        echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $message->getSender()]).'" class="name">';
        echo $message->getSender();
        echo '</a>';
        echo '<span class="timeStamp">'.$this->getTimeWithLocalTimeZone("M d Y H:i.s", $message->getTime()).'</span>';
        echo '</div>';
        echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $message->getSender()]).'" class="avatar">';
        echo '<img data-src="'.$this->router->generate('app_avatar_getfile', ['size' => 'small', 'user' => $message->getSender()]).'" class="picture" />';
        echo '</a>';
        echo '</div>';
        if (is_string($linkToFullConv)) {
            echo '<a href="'.$this->router->generate('app_user_messages', ['user' => $linkToFullConv]).'">';
        }
        echo '<span class="textMsg">'.strip_tags($message_str).'</span>';
        if (is_string($linkToFullConv)) {
            echo '</a>';
        }
        echo '</div>';
    }
    
    public function printBlueBox(string $icon, string $title, string $content): void { ?>
        <div class="w-box dark first">
            <div class="thmb-img">
                <i class="fa fa-<?php echo $icon; ?>"></i>
            </div>
            <h2><?php echo $title; ?></h2>
            <p class="text-center"><?php echo $content; ?></p>
        </div>
        <?php
    }
    
    public function printIndexFeature(string $icon, string $title, string $content): void {
        ?>
        <div class="col-md-3">
            <div class="row" style="font-size:68px; margin-top:-8px; text-align:center;">
                <i class="fa <?php echo $icon; ?>"></i>
            </div>
            <div class="row">
                <h4>
                    <center><?php echo $title; ?></center>
                </h4>
                <p>
                <center><?php echo $content; ?></center>
                </p>
            </div>
        </div>
        <?php
        
    }
    
    public function printNews(News $news): void {
        $max_lenght = 149;
        ?>
        <div class="col-md-3">
            <div class="w-box">
                <a href="<?php echo $this->router->generate("app_news_single", ['news' => $news->getId()])?>">
                    <img alt="" src="<?php echo $news->getImage() ?>" class="img-responsive">
                <h2><?php echo $news->getTitle(); ?></h2>
                </a>
                <p>
                    <?php echo Misc::stringLimit(strip_tags($news->getContent()), $max_lenght); ?>
                </p>
            </div>
        </div>
        <?php
    }
    
    public function printBreadcrumbs(string $title, array $items): void {
        ?>
        <div class="pg-opt pin">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2><?php echo $title; ?></h2>
                    </div>
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <?php
                            foreach ($items as $item) {
                                if (array_key_exists('url', $item)) {
                                    echo '<li><a href="'.$item['url'].'">'.$item['title'].'</a></li>';
                                }
                                else {
                                    echo '<li class="active">'.$item['title'].'</li>';
                                }
                            }
                            ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    
    public function printError(string $title, string $content, bool $display_getstart_link= true): void { ?>
        <section class="slice color-one">
            <div class="w-section inverse">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h2><i class="fa fa-warning"></i> <?php echo $title; ?></h2>
                                <p><?php echo $content; ?></p>
                                <span class="clearfix"></span>
                                <?php if ($display_getstart_link) { ?>
                                    <div class="text-center">
                                        <a href="<?php echo $this->router->generate("app_getstarted_main");?>" class="btn btn-lg btn-one margin-t-20" title="">Get started</a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </section>
        <?php
    }
    
    public function getTimeWithLocalTimeZone(string $format, int $timestamp): string {
        $date = new DateTime();
        $date->setTimestamp($timestamp);
        if (is_null($this->requestStack->getCurrentRequest()) == false && $this->requestStack->getCurrentRequest()->cookies->has('timezone')) {
            try {
                $date->setTimezone(new DateTimeZone($this->requestStack->getCurrentRequest()->cookies->get('timezone')));
            }
            catch (Exception $e) {
            }
        }
        return $date->format($format);
    }
    
    public function isDarkTheme(): bool {
        return $this->requestStack->getCurrentRequest()->cookies->has('theme') && $this->requestStack->getCurrentRequest()->cookies->get('theme') == HTML::THEME_DARK;
    }
    
    public function UIProgression(Project $project): void {
        $frames = $project->frames();
        if ($project->isSingleFrame()) {
            $isStarted = false;
            $frame = $frames->first();
            if ($frame->getStatus() == Constant::FINISHED) {
                $isStarted = true;
            }
            
            if ($isStarted) {
                echo '<center>';
                echo 'Note: this is not the final image. It is a thumbnail of the on going render.';
                echo '<br>';
                echo '<img style="border: 1px solid black;" src="'.$frame->getUrlThumbnailOnShepherd().'" alt="" title="" align="middle" />';
                echo '</center>';
                echo '<br>';
            }
        }
        elseif ($project->getMp4PreviewGenerated() > 0) {
            echo '<center>';
            echo 'Note: this is not the final video. It is a reduced thumbnail.';
            echo '<br>';
            echo '<video controls loop>';
            echo '<source src="'.$project->getMp4PreviewOnShepherd().'" type="video/mp4">';
            echo 'Your browser does not support the video tag.';
            echo '</video>';
            echo '</center>';
        }
        
        if ($project->isSingleFrame() == false) {
            foreach ($frames as $f1) {
                $v1 = $f1->getStatus();
                echo '<span class="square btn btn-';
                if ($v1 == Constant::FINISHED) {
                    echo 'neutral';
                }
                elseif ($v1 == Constant::WAITING) {
                    echo 'default';
                }
                elseif ($v1 == Constant::PROCESSING) {
                    echo 'warning';
                }
                elseif ($v1 == Constant::PAUSED) {
                    echo 'info';
                }
                elseif ($v1 == Constant::WAITINGFORARCHIVECREATION) {
                    echo 'other';
                }
                else {
                    echo 'default';
                }
                echo '"';
                if ($v1 == Constant::FINISHED || $v1 == Constant::PROCESSING) {
                    echo ' title="<center>frame: '.$f1->getNumber().'<br>';
                    echo 'cost: '.number_format($f1->getCost()).' <br>';
                    echo 'rendertime: '.Misc::humanTime($f1->getRenderTime(), true).'<br>';
                    echo '</center><img src=&quot;'.$f1->getUrlThumbnailOnShepherd().'&quot; />"';
                }
                echo '>';
                echo '</span>';
            }
        }
    }
    
    public function UIShowFrames(Project $project, int $interval): void {
        $project->setTokenThumbnailShepherdServer();
        $ret2 = '';
        
        $frames = $project->framesWithFinishedTiles();
        if (count($frames) >= 1) {
            $display_all = false;
            if (count($frames) <= $interval) {
                $display_all = true;
            }
            
            if ($display_all == false) {
                if ($interval != 1) {
                    $ret2 .= sprintf('Note: One every %d frames is displayed', $interval);
                    $ret2 .= '<br>';
                    $ret2 .= '<a href="'.$this->router->generate("app_project_frames", ['project' => $project->getId(), 'interval' => 1]).'">Display all frames</a>';
                    $ret2 .= '<br>';
                }
                $ret2 .= '<br>';
            }
            
            foreach ($frames as $frame_pic) {
                if ($display_all == false && $frame_pic->getNumber() % $interval != 0) {
                    continue;
                }
                
                $ret2 .= '<div class="col-md-3">';
                $ret2 .= '<div class="w-box frames">';
                $ret2 .= '<a href="'.$frame_pic->getUrlFullOnShepherd().'" rel="gallery" data-fancybox-type="image" data-title="'.$frame_pic->getNumber().'" rel="job_frames"><img data-src="'.$frame_pic->getUrlThumbnailOnShepherd().'" alt="" title="" align="middle" /></a>';
                $ret2 .= '<span class="number-over">'.$frame_pic->getNumber().'</span>';
                $ret2 .= '<span class="date-over"><strong>';
                $ret2 .= '<a href="'.$this->router->generate("app_frame_tiles", ['frame' => $frame_pic->getId()]).'">';
                $ret2 .= '<img src="/media/image/stack_layer.png" style="width:20px;">';
                $ret2 .= '</a>';
                $ret2 .= '</strong></span>';
                $ret2 .= '<center>render time: '.Misc::humanTime($frame_pic->getRenderTime(), true).'<br>cost: '.number_format($frame_pic->getCost()).'</center>';
                $ret2 .= '</div>';
                $ret2 .= '</div>';
            }
        }
        
        echo $ret2;
    }
    
    public function UIShowTiles(Frame $frame): void {
        $ret2 = '';
        $ret2 .= '<div>';
        $ret2 .= $frame->getSplitHumanExplanation();
        $ret2 .= '</div>';
        
        
        $nb_columns = 4;
        if ($frame instanceof FrameChessboard) {
            $nb_columns = sqrt($frame->getSplit());
            $unordered_tiles = [];
            foreach ($frame->tilesWithStatus(Constant::FINISHED) as $t) {
                $unordered_tiles[$t->getNumber()] = $t;
            }
            
            // re-order the tiles for nicer display
            if ($frame->getSplit() == 4) { // 2x2
                $ordered_tiles = [];
                $ordered_tiles = array_fill(0, 16, null);
                $ordered_tiles[0] = $unordered_tiles[1] ?? null;
                $ordered_tiles[1] = $unordered_tiles[3] ?? null;
                $ordered_tiles[4] = $unordered_tiles[0] ?? null;
                $ordered_tiles[5] = $unordered_tiles[2] ?? null;
                $nb_columns = 4;
            }
            elseif ($frame->getSplit() == 16) { // 4x4
                $new_order = [3, 7, 11, 15, 2, 6, 10, 14, 1, 5, 9, 13, 0, 4, 8, 12];
                
                $ordered_tiles = array_map(function($key) use ($unordered_tiles) {
                    return $unordered_tiles[$key] ?? null;
                }, $new_order);
            }
            elseif ($frame->getSplit() == 36) { // 6x6
                $new_order = [5, 11, 17, 23, 29, 35, 4, 10, 16, 22, 28, 34, 3, 9, 15, 21, 27, 33, 2, 8, 14, 20, 26, 32, 1, 7, 13, 19, 25, 31, 0, 6, 12, 18, 24, 30];
                
                $ordered_tiles = array_map(function($key) use ($unordered_tiles) {
                    return $unordered_tiles[$key] ?? null;
                }, $new_order);
            }
            else {
                $nb_columns = 4;
                $ordered_tiles = $frame->tiles();
            }
        }
        else {
            $ordered_tiles = $frame->tiles();
        }
        
        foreach ($ordered_tiles as $number => $tile) {
            if ($number % $nb_columns == 0) {
                $ret2 .= '<div class="row">';
            }
            
            if (is_null($tile)) {
                $ret2 .= '<div class="col-md-'.(12 / $nb_columns).'" >';
                $ret2 .=  '</div>'; // col-md-
            }
            else {
                $ret2 .= '<div id="tile_'.$tile->getId().'_div" class="col-md-'.(12 / $nb_columns).'" style="margin: 10px 0px;" >';
                $ret2 .= '<div class="w-box frames" style="padding: 4px">';
                $ret2 .= '<a href="'.$tile->getUrlFullOnShepherd().'" class="theater" ata-fancybox-type="image" rel="gallery" data-title="'.$tile->getNumber().'">';
                $ret2 .= '<img style="border: 1px solid black;" data-src="'.$tile->getUrlThumbnailOnShepherd().'" alt="" title="" align="middle" />';
                $ret2 .= '</a>';
                $ret2 .= '<br>';
                $ret2 .= '<span class="date-over"><strong>';
                $ret2 .= $tile->getNumber();
                $ret2 .= '</strong></span>';
                $ret2 .= 'render time: '.Misc::humanTime($tile->getRenderTime(), true);
                $ret2 .= '<br>';
                
                if (is_object($tile->getGPU())) {
                    if ($this->user->isModerator()) {
                        $ret2 .= 'gpu : <a href="'.$this->router->generate("app_session_single", ['sessionid' => $tile->getSession()]).'">'.$tile->getGPU()->description().'</a>';
                    }
                    else {
                        $ret2 .= 'gpu : '.$tile->getGPU()->description();
                    }
                }
                elseif (is_object($tile->getCpu())) {
                    if ($this->user->isModerator()) {
                        $ret2 .= 'cpu : <a  href="'.$this->router->generate("app_session_single", ['sessionid' => $tile->getSession()]).'">'.$tile->getCpu()->description().'</a>';
                    }
                    else {
                        $ret2 .= 'cpu : '.$tile->getCpu()->description();
                    }
                }
                $ret2 .= ' ('.$tile->getDevicePowerHuman().')';
                $ret2 .= '<br>';
                $ret2 .= 'cost: '.number_format($tile->getCost()).'<br>';
                $ret2 .= 'renderer: '.$this->retUserProfileLink(is_object($tile->getUser()) ? $tile->getUser()->getId() : '');
                $ret2 .= '<br>';
                $ret2 .= '<input type="button" class="btn btn-danger" onclick="doResetTile(\''.$tile->getId().'\'); return false" value="Reset" />';
                
                $ret2 .=  '</div>'; // w-box
                $ret2 .=  '</div>'; // col-md-
            }
            
            if ($number != 0 && ($number + 1) % $nb_columns == 0) {
                $ret2 .= '</div>'; // row
            }
            
        }
        echo $ret2;
    }
    
    /**
     * Get the type of project who can be read by a human.
     * Like 'Animation with 8x8 chessboard'
     **/
    public function getHumanType(Project $project): string {
        $frame = $project->frames()->first();
        if ($project->isSingleFrame()) {
            if ($frame->getSplit() == 1) {
                return 'Single frame';
            }
            else {
                if ($frame instanceof FrameLayer) {
                    return sprintf('Single frame with %d layers', $frame->getSplit());
                }
                else {
                    return sprintf('Single frame with %dx%d chessboard', sqrt($frame->getSplit()), sqrt($frame->getSplit()));
                }
            }
        }
        else {
            if ($frame->getSplit() == 1) {
                return 'Animation';
            }
            else {
                if ($frame instanceof FrameLayer) {
                    return sprintf('Animation with %d layers', $frame->getSplit());
                }
                else {
                    return sprintf('Animation with %dx%d chessboard', sqrt($frame->getSplit()), sqrt($frame->getSplit()));
                }
            }
        }
    }
    
    
    public function getHeaderPages(): array {
        
        $request = $this->requestStack->getCurrentRequest();
        
        $pages = [
            ['route' => $this->router->generate('app_home_main'), 'text' => 'Home', 'highlight' => false],
            ['route' => $this->router->generate('app_getstarted_main'), 'text' => 'Get started', 'highlight' => false],
            ['route' => $this->router->generate('app_home_projects'), 'text' => 'Projects', 'highlight' => false],
            ['route' => $this->router->generate('app_team_all'), 'text' => 'Teams', 'highlight' => false],
            ['route' => '/forum', 'text' => 'Forum', 'highlight' => false],
            ['route' => $this->router->generate('app_faq_main'), 'text' => 'FAQ', 'highlight' => false],
            ['route' => $this->router->generate('app_servers_main'), 'text' => 'Servers', 'highlight' => false],
        ];
        
        if ($this->config['donation']['enable']) {
            $pages [] = ['route' => $this->router->generate('app_donation_main'), 'text' => 'Donate', 'highlight' => false];
        }
        $pages [] = ['route' => $this->config['donation']['spreadshirt']['ID'], 'text' => 'Store', 'highlight' => false];
        
        if (is_object($this->user) && $this->user->isModerator()) {
            $pages [] = ['route' => $this->router->generate('app_admin_main'), 'text' => 'Admin', 'highlight' => false];
        }
        
        $url_called = explode('/', $request->getUri())[0];
        if ($url_called == '') {
            $url_called = '/index.php';
        }
        
        foreach ($pages as $k => $page) {
            if (str_starts_with($url_called, $page['route'])) {
                $pages[$k]['highlight'] = true;
                break;
            }
        }
        
        return $pages;
    }
    
    public function getShortVersion(): string {
        return getenv('SHORT_VERSION');
    }
    
    public function getFooterCopyrightYear(): string {
        return  '2007-'.date('Y');
    }
    
    public function getFooterProjects(): array {
        $available_projects = [];
        
        if (is_object($this->user)) {
            $available_projects = $this->main->getLatestPublicProjects();
        }
        
        return $available_projects;
    }
    
    public function getFooterRenderers(): array {
        $nb_elements = 3;
        $users = $this->main->getBestRenderersFromCache();
        return array_slice($users, 0, $nb_elements);
    }
    
    public function getFooterDebugUsage(): string {
        $ret = '';
        if (is_object($this->user) && $this->user->isModerator()) {
            $ret .= '<div>';
            $ret .= 'version: '.getenv('VERSION').'<br>';
            $ret .= 'Memory usage '.Misc::humanSize(memory_get_peak_usage(true)).'B <br>';
            $ret .= 'duration: '.sprintf("%.3f", microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]).'s<br>';
            $ret .= '</div>';
        }
        
        return $ret;
        
    }
    
    public function getFooterPaypalButton(): string {
        if ($this->config['donation']['enable'] && isset($this->config['donation']['paypal']['ID']) && $this->config['donation']['paypal']['ID'] != '') {
            $html = '';
            $html .= '<div style="float: left">';
            $html .= '<form style="margin-top: 0px" action="https://www.paypal.com/donate" method="post" target="_top">';
            $html .= '<input type="hidden" name="hosted_button_id" value="'.$this->config['donation']['paypal']['ID'].'"/>';
            $html .= '<input type="image" src="/media/image/paypal-donate-button.png" style="width: 130px;background: transparent" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button"/>';
            $html .= '<img alt="" border="0" src="https://www.paypal.com/en_FR/i/scr/pixel.gif" width="1" height="1"/>';
            $html .= '</form>';
            $html .= '</div>';
            return $html;
        }
        return '';
    }
    
    private function printMaxProjectReached(int $max_concurent): void {
        $this->printError("Oh no!", sprintf('You can not add a project because you have exceeded your limit of concurrent projects. <br>The limit has been set to prevent overloading the render farm with projects from a single person.<br>Once one of your projects have been rendered, you will be able to add another.<br>Your current limit is %d projects.', $max_concurent));
    }
    
    public function printCreateProject(): void {
        if ($this->config['project']['maintenance']['enable']) {
            $this->printError("Server in maintenance, you can't add a project.", $this->config['project']['maintenance']['reason']);
            return;
        }
        
        $avatar_path = $this->config['storage']['path'].'users/avatars/big/'.$this->user->getId().'.png';
        if (file_exists($avatar_path) == false) {
            $this->printError("You can't add a project, yet.", "Friendly community is really important to us. Please add a profile image in order to add a project.<br>To add one, go on your <a href=\"".$this->router->generate('app_user_edit', ['user' => $this->user->getId()])."\">account page</a>.", false);
            return;
        }
        
        $max_concurrent = $this->adventCalendarClaimedDayRepository->getProjectsAddForUser($this->user); // before $this->config['project']['max_concurrent_project_per_user']
        $current = $this->user->getNumberOfConcurrentProject();
        if ($current >= $max_concurrent) {
            $this->printMaxProjectReached($max_concurrent);
            return;
        }
        
        if ($this->user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) == false) {
            $this->printError("Oh no!", 'You can not add a project because an admin explicitly disabled it for your account.<br>If you think it is a mistake, contact us on Discord.');
            return;
        }
        
        // Compute user position
        $user_place_cpu = 1;
        $user_place_with_team_cpu = 1;
        $user_place_gpu = 1;
        $user_place_with_team_gpu = 1;
        $nb_total_project = 0;
        $user_points_with_team = $this->user->getExtraPointsFromTeam();
        
        $projects = $this->projectRepository->findAll();
        foreach ($projects as $project) {
            $status = $project->getStatus();
            if ($status == Constant::PROCESSING || $status == Constant::WAITING) {
                if ($project->isBlocked() == false && $project->getPublicRender()) {
                    $owner_points = -1 * PHP_INT_MAX;
                    $nb_total_project += 1;
                    
                    if ($project->getOwnerPointsOnlastupdate() > $owner_points) {
                        $owner_points = $project->getOwnerPointsOnlastupdate();
                    }
                    if ($this->user->getPoints() < $owner_points) {
                        if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU)) {
                            $user_place_cpu += 1;
                        }
                        else {
                            $user_place_gpu += 1;
                        }
                    }
                    if ($user_points_with_team < $owner_points) {
                        if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU)) {
                            $user_place_with_team_cpu += 1;
                        }
                        else {
                            $user_place_with_team_gpu += 1;
                        }
                    }
                }
            }
        }
        
        ?>
        <section class="slice color-three">
            <div class="container">
                <h3>Add your project</h3>
            </div>
            <?php
            //            display_google_adsense();
            ?>
            <div class="w-section">
                <?php
                $allowUpload = true;
                if ($this->user->getRenderedFrames() < 10 && $this->frameRepository->countPublicRemainingFrames() > 5000) {
                    $allowUpload = false;
                    ?>
                    <div class="container" id="addproject_warning_zero_frame">
                        <section class="slice color-one">
                            <div class="row" style="margin: 10px;">
                                <div class="text-center">
                                    <p class="text-center" style="solid #CCC; color: #CCC;"><font color="black">SheepIt is a renderfarm kept alive thanks to the community.<br/>You have rendered less than 10 frames. We don't expect everyone to render as many frames as they have asked for, but every contribution helps.<br/><strong>You must have rendered at least 10 frames to be able to add a project.</strong></font></p>
                                    <!--                             <span class="clearfix"></span> -->
                                    <!--                             <div class="text-center"> -->
                                    <!--                                 <input type="button" class="btn btn-lg btn-one margin-t-20" value="I understand" onclick="$('#addproject_main_div').show(); $('#addproject_warning_zero_frame').hide(); return false"> -->
                                    <!--                             </div> -->
                                </div>
                        </section>
                    </div>
                    <?php
                }
                if ($allowUpload) {
                    ?>
                    <div class="container" id="addproject_main_div">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="w-section inverse">
                                    <div class="w-box dark sign-in-wr">
                                        <?php
                                        $uploadUID = bin2hex(random_bytes(16));
                                        // keep the php session alive by doing a heartbeat
                                        // (the uploading might take a lot of time and the uploader might not be on the page when the upload finished
                                        echo '<script>';
                                        echo 'keepPHPSessionAlives();';
                                        echo '</script>';
                                        
                                        echo '<div class="w-section inverse">';
                                        echo '<div class="container">';
                                        echo 'You can use a <strong>single blend file</strong>, with packed textures.<br>Or a <strong>ZIP file</strong>, but make sure to use relative paths for all files in the archive.<br>';
                                        echo '<form action="'.$this->router->generate('app_project_add_step1').'" method="post" enctype="multipart/form-data" onsubmit="addproject_upload_progress_fct(\''.$uploadUID.'\')">';
                                        echo '<input type="hidden" name="UPLOAD_IDENTIFIER" value="'.$uploadUID.'" />';
                                        echo '<table border="0" cellspacing="3" cellpadding="5">';
                                        echo '<tr>';
                                        echo '<td style="text-align: right; vertical-align: middle;">File</td>';
                                        echo '<td style="text-align: left; vertical-align: middle;">';
                                        
                                        echo '<input type="file" name="addproject_archive" />';
                                        echo ' ';
                                        echo sprintf('Max: %sB <strong>before</strong> ZIP compression<br>Blender compression is recommended and supported.', Misc::humanSize(ProjectController::MAX_SIZE, true, 1024));
                                        echo '</td>';
                                        echo '</tr>';
                                        
                                        echo '<tr>';
                                        echo '<td style="text-align: center; vertical-align: middle;" colspan="2">';
                                        echo '<div id="upload_progress_bar" style="position: relative; height: 20px; display: none;"><div id="upload_progress_label" style="position: absolute; right: 5px; top: 2px; text-shadow: 1px 1px #fff;"></div></div>';
                                        echo '</td>';
                                        echo '</tr>';
                                        
                                        echo '<tr>';
                                        echo '<td id="td_upload_submit" style="text-align: center; vertical-align: middle;" colspan="2">';
                                        echo '<input type="submit" class="btn btn-primary" value="Send this file" />';
                                        echo '</td>';
                                        echo '</tr>';
                                        echo '</table>';
                                        echo '</form>';
                                        echo '</div>';
                                        echo '</div>';
                                        ?>
                                    </div>
                                </div>
                                <div class="w-section inverse">

                                    <script type="text/javascript">;
                                        jQuery(document).ready(function ($) {
                                            $("#addproject_estimator_device_form_search_text_label").autocomplete({
                                                minLength: 3,
                                                source: "<?php echo $this->router->generate('app_device_search'); ?>",
                                                select: function (event, ui) {
                                                    $("#addproject_estimator_device_form_search_text_label").val(ui.item.label);
                                                    $("#addproject_estimator_device_form_search_text_value").val(ui.item.value);
                                                    return false;
                                                }
                                            });
                                        });
                                    </script>

                                    <h4>Estimator</h4>
                                    <p>
                                        The <?php echo (int)($this->config['power']['rendertime_max_reference'] / 60); ?> minute rule is intended to ensure that even small computers can render any project.<br/>
                                        If a frame of your project can be rendered in under <?php echo (int)($this->config['power']['rendertime_max_reference'] / 60); ?> min on the reference machine then you don't need to split it, but if you do, this formula can help you determine what the best value is.
                                    </p>

                                    <table>
                                        <tr>
                                            <td>Render time:</td>
                                            <td><input id="addproject_estimator_time" type="text" size="5"> minutes per frame.</td>
                                        </tr>
                                        <tr>
                                            <td>Number of frames:</td>
                                            <td><input id="addproject_estimator_count_frame" type="text" size="5"> frames.</td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <form class="form-inline" id="addproject_estimator_device_form_search" action="javascript:;">
                                        <input type="hidden" id="addproject_estimator_device_form_search_text_value" value=""/>
                                        <div class="input-group">
                                            <input class="form-control" id="addproject_estimator_device_form_search_text_label" placeholder="Processor or GPU name"/>
                                            <span class="input-group-btn">
                                            <input class="btn btn-primary" type="button" id="addproject_estimator_device_form_button" onclick="calculate_estimate_project(); return false" value="OK"/>
                                        </span>
                                        </div>
                                    </form>
                                    <div id="addproject_estimator_result" style="display: none">
                                    </div>
                                </div> <!--  estimator -->

                            </div>
                            <div class="col-md-6 col-md-offset-1">
                                <h4>Waiting list</h4>
                                <p>
                                    <?php
                                    $numberFormatter_US = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
                                    $extra_team = '';
                                    $team = $this->user->getTeam();
                                    if (is_object($team)) {
                                        $extra_team = sprintf('<br>Since you are part of a team who generated %s points some of those will give you an extra boost of %s points.', number_format($team->getPoints()), number_format($user_points_with_team - $this->user->getPoints()));
                                    }
                                    echo sprintf('The render order is based on points. The more points you have, the higher priority you get. You currently have %s points.%s<br> Predicted position in queue:<ul><li>CPU: <strong>%s</strong></li><li>GPU: <strong>%s</strong></li></ul>Total projects: %s',
                                        number_format($this->user->getPoints()), $extra_team, $numberFormatter_US->format($user_place_with_team_cpu), $numberFormatter_US->format($user_place_with_team_gpu), $nb_total_project);
                                    ?>
                                </p>
                                <h4>Before adding a file to the render farm you should check if:</h4>
                                <ul>
                                    <li>Your blend version is <strong>3.0 or higher</strong>.</li>
                                    <li>The render engine is compatible. <strong>Cycles</strong>, <strong>Eevee</strong> and <strong>Workbench</strong> renderer are supported.</li>
                                    <li>You set the output file to an <strong>image</strong>, for example jpg or png.</li>
                                    <li><?php echo sprintf('Image output size must be under a width of %s px, and a height of %s px.', number_format($this->config['project']['max']['width']), number_format($this->config['project']['max']['height'])); ?></li>
                                    <li>The path to external data is <strong>relative</strong>.</li>
                                    <li>File names <strong>must not be locale specific</strong>, i.e. do not put any accents in filenames.</li>
                                    <li><?php echo sprintf('<strong>You should keep the render time per frame (or split) under %d min (on the reference machine)</strong>. As an example, <a href="/reference_machine.blend">this scene</a> will be rendered in %dmin on the reference machine (i5-9600).', $this->config['power']['rendertime_max_reference'] / 60, $this->config['power']['rendertime_max_reference'] / 60); ?><br>There is a <a href="<?php echo $this->router->generate('app_ranking_machine'); ?>">list of average power per machine</a>, <a
                                                href="<?php echo $this->router->generate('app_faq_main', ['_fragment' => 'power_rating']); ?>">check out the faq for more info</a>.
                                    </li>
                                    <li>You can add up to <strong><?php echo number_format($this->config['project']['max_frames']); ?></strong> tiles at a time. If you divide it into 8x8, your project can be up to <?php echo number_format((int)($this->config['project']['max_frames'] / 64)); ?> frames; if you don't split frames, you will have 1 tile <=> 1 frame.
                                    </li>
                                    <li>No <strong>nudity</strong> or any <strong>racist</strong> content is allowed.</li>
                                    <li>Be aware that <strong>scripts are disabled due to security reasons.</strong></li>
                                    <li>Be aware that if you have set RGBA as output, the MP4 file will include the alpha layer and will make the file unplayable by some video players. The file could need special treatment in editing software.</li>
                                </ul>
                                <h4>Limitation on EXR  support:</h4>
                                <ul>
                                    <li>Full frame renders only. No split-layers or checkerboarding.</li>
                                    <li>Only animations are supported, no single image projects.</li>
                                    <li><?php echo sprintf('Maximum number of layers/passes: %d', $this->config['project']['exr']['maximum_layers']); ?></li>
                                    <li><?php echo sprintf('Maximum image dimensions %dx%d px.', $this->config['project']['exr']['maximum_dimension']['width'], $this->config['project']['exr']['maximum_dimension']['height']); ?></li>
                                    <li>Use of compression is required (any of <?php echo join(', ', $this->config['project']['exr']['compressions_supported']); ?>).
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </section>
        <?php
    }
    
    public function printRank(int $rank): string {
        $nf = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
        return $nf->format($rank);
    }
    
    public function printHomeFeature2(string $icon, string $title, string $content): void {
        ?>
        <div class="col-md-3">
            <div class="row" style="font-size:68px; margin-top:-8px; text-align:center;">
                <i class="fa <?php echo $icon; ?>"></i>
            </div>
            <div class="row">
                <h4>
                    <center><?php echo $title; ?></center>
                </h4>
                <p>
                <center><?php echo $content; ?></center>
                </p>
            </div>
        </div>
        <?php
    }
    
    public function homeDisplayStatBox(string $title, string $url, float $value, array $values): void {
        ?>
        <div class="col-md-3">
            <a href="<?php echo $url; ?>">
                <div class="w-box stat-box" style="cursor: pointer">
                    <div class="sparkline"><?php echo implode(',', $values) ?></div>
                    <div class="content">
                        <?php echo $title; ?>
                        <h2><?php echo number_format($value); ?></h2>
                    </div>
                </div>
            </a>
        </div>
        <?php
    }
            
    public function printLogin(string $redirect): void { ?>
        <section class="slice ">
            <div class="w-section inverse">
                <?php $this->displayGoogleAdsense(); ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h2>Welcome to SheepIt</h2>
                                <p style="font-size:16px;">
                                    Please sign in to continue.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                            <div class="w-section inverse">
                                <div class="w-box dark sign-in-wr">
                        <span class="form-icon color-two">
                            <i class="fa fa-users"></i>
                        </span>
                                    <div id="login_error_box"></div>
                                    <form id="login" role="form" class="form-light padding-15" action="javascript:;">
                                        <input type="hidden" name="login_redirect" id="login_redirect" value="<?php echo $redirect; ?>"/>
                                        <div class="form-group">
                                            <input type="text" id="login_login" value="" size="32" maxlength="32" class="form-control" placeholder="Enter username"/>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" id="login_password" value="" maxlength="32" class="form-control" placeholder="Password"/>
                                            <span id="login_password_toggle" toggle="#login_password" class="fa fa-fw fa-eye field-icon toggle-password" onclick="doTogglePasswordVisibility('login_password_toggle')" style="float: right; margin-left: -25px;margin-top: -35px;position: relative;z-index: 2;"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" id="login_submit" class="btn btn-three pull-right" value="Sign in"/>

                                            </div>
                                        </div>
                                    </form>
                                    <p>
                                        <?php echo 'If you don\'t have an account, you can <a href="'.$this->router->generate('app_user_print_register').'">register.</a>'; ?><br/>
                                        <?php echo 'If you have forgotten your password, you can <a href="'.$this->router->generate('app_user_lostpassword').'">reset it.</a>'; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    
    public function printResetPassword(): void { ?>
        <section class="slice ">
            <div class="w-section inverse">
                <?php $this->displayGoogleAdsense(); ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                <h2>Forgot your password?</h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                            <div class="w-section inverse">
                                <div class="w-box dark sign-in-wr">
                                    <form id="resetpassword" role="form" class="form-light padding-15" action="javascript:;">
                                        <div id="resetpassword_error_box"></div>
                                        <div class="form-group">
                                            <label for="email">Enter your email to get a new password</label>
                                            <input type="email" id="register_email" value="" maxlength="64" class="form-control" placeholder="Email"/>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" id="resetpassword_submit" value="Reset my password" class="btn btn-three pull-right"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    
    public function printLoginHead(User $user, ?array $rank, string $type): void {
        $best = null;
        if (is_array($rank)) {
            $tops = array(50, 25, 10, 5, 1);
            foreach ($tops as $t) {
                if ($rank[0] / $rank[1] < ($t / 100) && (is_null($best) || $best > $t)) {
                    $best = $t;
                }
            }
        }
        
        echo '<h1>'.$user->getId().'</h1>';
        if (is_null($best) == false) {
            echo '<span class="label label-success" style= "position: absolute; margin: -10px 0 0 00px; font-size: 18px;" title="The number means you are in the first XX % of renderers over 30 day rolling period, <br>full list is available by clicking on <i>Top renderers over 30 day rolling period</i> at the bottom of the page">top '.$best.'% renderers</span>';
        }
        
        $str = '';
        $status = $user->getWorkerStatus();
        if ($status == Constant::PROCESSING) {
            $str = 'Rendering projects';
        }
        else {
            if ($status == Constant::WAITING) {
                $str = 'Waiting to render projects';
            }
            else {
                if ($status == Constant::DOWN) {
                    $str = 'Disconnected';
                }
            }
        }
        if ($str != '') {
            echo '<br>';
            echo $str;
        }
        if ($type != 'public') {
            echo '<br>';
            echo '<a style="font-size: 18px;" href="'.$this->router->generate('app_user_edit', ['user' => $user->getId()]).'">Edit profile</a>';
        }
    }
    
    public function printUserAvatar(User $user): void {
        echo '<div class="polaroid avatar-big">';
        echo '<a href="'.$this->router->generate('app_user_profile', ['user' => $user->getId()]).'">';
        echo '<img src="'.$this->router->generate('app_avatar_getfile', ['size' => 'big', 'user' => $user->getId()], UrlGeneratorInterface::RELATIVE_PATH).'" alt="'.$user->getId().'"/>';
        echo '</a>';
        if ($user->isDonator()) {
            echo '<a href="'.$this->router->generate('app_donation_main').'">';
            echo '<img class="patreon-badge" src="/media/image/patreon-badge.svg" />';
            echo '</a>';
        }
        echo '</div>';
    }
    
    public function printSocialNetworks(User $user): void {
        echo '<div class="row" style="padding: 20px 0px 0px; 0px">';
        $networks = 0;
        $networks += ($user->getSocialnetworkFacebook() != '' ? 1 : 0);
        $networks += ($user->getSocialnetworkTwitter() != '' ? 1 : 0);
        $networks += ($user->getSocialnetworkInstagram() != '' ? 1 : 0);
        $networks += ($user->getSocialnetworkYoutube() != '' ? 1 : 0);
        
        $networks = max(1, $networks); // division per 0
        
        if ($user->getSocialnetworkFacebook() != '') {
            echo '<div class="col-md-'.(12 / $networks).'">';
            echo '<a href="'.$user->getSocialnetworkFacebook().'"/><i class="fa-brands fa-facebook fa-3x" style="border:1px;color:#FFF"></i></a>';
            echo '</div>';
        }
        if ($user->getSocialnetworkTwitter() != '') {
            echo '<div class="col-md-'.(12 / $networks).'">';
            echo '<a href="'.$user->getSocialnetworkTwitter().'"/><i class="fa-brands fa-twitter fa-3x" style="border:1px;color:#FFF"></i></a>';
            echo '</div>';
        }
        if ($user->getSocialnetworkInstagram() != '') {
            echo '<div class="col-md-'.(12 / $networks).'">';
            echo '<a href="'.$user->getSocialnetworkInstagram().'"/><i class="fa-brands fa-instagram fa-3x" style="border:1px;color:#FFF"></i></a>';
            echo '</div>';
        }
        if ($user->getSocialnetworkYoutube() != '') {
            echo '<div class="col-md-'.(12 / $networks).'">';
            echo '<a href="'.$user->getSocialnetworkYoutube().'"/><i class="fa-brands fa-youtube fa-3x" style="border:1px;color:#FFF"></i></a>';
            echo '</div>';
        }
        
        echo '</div>';
    }
    
    public function printSummary(User $user, ?array $rank, string $type): void {
        echo '<dl class="dl-horizontal">';
        
        echo '<dt>Projects created</dt><dd>'.number_format($user->getNbProjects()).'</dd>';
        echo '<dt>Frames ordered</dt><dd>'.number_format($user->getOrderedFrames());
        if ($type == 'admin' && $user->getOrderedFrames() != $user->getOrderedFramesPublic()) {
            echo ' ('.number_format($user->getOrderedFramesPublic()).')';
        }
        echo '</dd>';
        echo '<dt>Frames rendered</dt>';
        echo '<dd>';
        echo number_format($user->getRenderedFrames());
        if ($type == 'admin' && $user->getRenderedFrames() != $user->getRenderedFramesPublic()) {
            echo ' ('.number_format($user->getRenderedFramesPublic()).')';
        }
        echo '</dd>';
        
        
        $str = Misc::humanTime($user->getRenderTime(), false, false);
        if ($str != '') {
            echo '<dt>Time rendered</dt><dd>'.$str.'</dd>';
        }
        if ($type == 'admin') {
            echo '<dt>Time ordered</dt><dd>'.Misc::humanTime($user->getOrderedFramesTime(), false, false).'</dd>';
        }
        
        if (is_null($rank)) {
            if ($user->getRegistrationTime() < time() - 86400) { // new user don't e rank because the daily cron didn't set it yet.
                echo '<dt>Rank</dt><dd>';
                echo sprintf("Has not rendered anything for at least %d days", $this->config['rank']['duration']);
                echo '</dd>';
            }
        }
        else {
            echo '<dt>Rank</dt><dd>';
            $numberFormatter_US = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
            echo $numberFormatter_US->format($rank[0]);
            echo '</dd>';
        }
        
        echo '<dt>Points</dt><dd>'.$this->pointsBadge($user->getPoints(), $type == 'private').'</dd>';
        $team = $user->getTeam();
        if (is_object($team)) {
            echo '<dt>Team</dt><dd><a href="'.$this->router->generate("app_team_single", ["team" => $team->getId()]).'">'.urldecode($team->getName()).'</a></dd>';
        }
        echo '<dt>Registration</dt><dd>'.$this->getTimeWithLocalTimeZone('F jS, Y', $user->getRegistrationTime()).'</dd>';
        $renderdays = $user->getConsecutiveRenderDaysCurrent();
        if ($renderdays > 0) {
            echo '<span  title="Number of consecutive days with at least '.AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY.' hours of rendering per day on a single session. This number is used for the \'Session duration\' awards.">';
            echo '<dt>Consecutive render days</dt>';
            echo '<dd>'.number_format($renderdays).'</dd>';
            echo '</span>';
        }
        if ($user->getConsecutiveRenderDaysMax() > 0) {
            echo '<span  title="Number of consecutive days with at least '.AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY.'hours of rendering per day on a single session. This number is used for the \'Session duration\' awards.">';
            echo '<dt>Max consecutive <br>render days achieved</dt>';
            echo '<dd>'.number_format(max($renderdays, $user->getConsecutiveRenderDaysMax())).'</dd>';
            echo '</span>';
        }
        if ($type == 'admin') {
            echo '<dt>Last session</dt><dd>'.($user->getLastSession() == 0 ? '<span style="color: red">never</span>' : $this->getTimeWithLocalTimeZone('F jS, Y', $user->getLastSession())).'</dd>';
            echo '<dt>Last Render</dt><dd>'.(is_null($user->getLastRenderedFrame()) ? '<span style="color: red">never</span>' : $this->getTimeWithLocalTimeZone('F jS, Y', $user->getLastRenderedFrame()->getTimestamp())).'</dd>';
            echo '<dt>Last connection</dt><dd>'.($user->getLastWebSession() == 0 ? '<span style="color: red">never</span>' : $this->getTimeWithLocalTimeZone('F jS, Y', $user->getLastWebSession())).'</dd>';
        }
        if ($user->getWebsite() != '') {
            $website = $user->getWebsite();
            if (str_starts_with($website, 'http') == false) {
                $website = 'http://'.$website;
            }
            echo '<dt>Website</dt><dd><a href="'.$website.'" target="_blank">'.Misc::stringLimit($user->getWebsite(), 70).'</a></dd>';
        }
        if ($type == 'admin' && $user->getAffiliate() != '') {
            echo '<dt>Affiliate</dt>';
            echo '<dd>';
            $this->printOwnerLink($user->getAffiliate());
            echo '</dd>';
        }
        echo '</dl>';
    }
    
    public function printPrivateMessages(User $user, string $type): void {
        echo '<div class="w-box blog-post">';
        echo '<div class="padding-15">';
        
        $messages = $this->messageRepository->forUser($user);
        if (count($messages) > 0) {
            echo '<h2>Previous messages</h2>';
            echo '<article class="chatSys">';
            echo '<section class="window">';
            foreach ($messages as $message) {
                if ($message->getSender() == $user->getId()) {
                    // from user
                    $this->printMessageRight($message, $user->getId());
                }
                else {
                    // usually from admin
                    $this->printMessageLeft($message, $user->getId());
                }
            }
            echo '</section>';
            echo '</article>';
        }
        ?>
        <h2>Send an message</h2>
        <form class="form" action="javascript:;">
            <div class="form-group">
                <textarea class="form-control" id="message" placeholder="Write you message here, do not ask for personal information. Be aware that a copy of your message will be sent to the admin..." name="comment" style="height:160px;"></textarea>
            </div>
            <button type="button" class="btn btn-three pull-right" onclick="DoSendMessage('<?php echo $this->router->generate('app_user_send_message', ['user' => $user->getId()]); ?>'); return false">Send</button>
        </form>
        </div>
        </div>
    <?php
    }
    
    public function printAddProjectRight(User $user, string $type): void {
        echo '<div class="w-box blog-post">';
        echo '<h2>User\'s permission to add a project</h2>';
        echo '<div class="padding-15">';
        
        ?>
        It will notify by email the user.
        <form method="post" class="form-default">
            <button type="button" class="btn btn-primary" onclick="requestReloadOnSuccess('<?php echo $this->router->generate('app_user_update_project_right', ['user' => $user->getId()]); ?>'); return false">
                <?php echo $user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) ? 'Forbid user to upload a project' : 'Allow back user to upload a project' ?>
            </button>
        </form>
        <?php
        
        echo '</div>';
        echo '</div>';
        
    }
    
    public function printRenderingRight(User $user, string $type): void {
        echo '<div class="w-box blog-post">';
        echo '<h2>User\'s permission to render project</h2>';
        echo '<div class="padding-15">';
        
        ?>
        It will notify by email the user.
        <form method="post" class="form-default">
            <button type="button" class="btn btn-primary" onclick="requestReloadOnSuccess('<?php echo $this->router->generate('app_user_update_render_right', ['user' => $user->getId()]); ?>'); return false">
                <?php echo $user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING) ? 'Forbid user to render any projects (even their)' : 'Allow user to render project'; ?>
            </button>
        </form>
        <?php
        
        echo '</div>';
        echo '</div>';
    }
    
    public function printProjects(User $user, string $type): void {
        $projects = $user->projects();
        
        if (count($projects) > 0) {
            echo '<div class="w-box blog-post">';
            echo '<h2>Latest projects</h2>';
            echo '<div class="padding-15">';
            echo '<table class="table table-bordered table-striped table-comparision table-responsive">';
            
            /** @var \ArrayIterator<Project> */
            $iterator = $projects->getIterator();
            $iterator->uasort('\App\Utils\Sort::sortByAttributeLastupdatearchive');
            
            foreach ($projects as $a_project) {
                if ($a_project->IsBlocked()) {
                    echo '<tr class="danger">';
                }
                else {
                    echo '<tr>';
                }
                
                $project_html = Misc::stringLimit($a_project->getName(), 25);
                if ($a_project->canManageProject($this->user) || $this->user->isModerator()) {
                    $project_html = '<a href="'.$this->router->generate("app_project_manage", ['project' => $a_project->getId()]).'">'.$project_html.'</a>';
                }
                echo '<td>'.$project_html.'</td>';
                
                $status = $a_project->getStatus();
                $human = $this->humanStatus($status);
                echo '<td class="msg_'.$human['style'].'">';
                echo $human['text'];
                if ($status == Constant::WAITING || $status == Constant::PROCESSING) {
                    $stats = $a_project->getCachedStatistics();
                    $total = $stats[Constant::STATS_TOTAL];
                    $finished = $stats[Constant::FINISHED];
                    if ($total != 0) {
                        echo ' ('.(int)($finished * 100 / $total).'%)';
                    }
                    else {
                        $percentage = 100;
                    }
                }
                echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
            
            echo '</div>';
            echo '</div>';
        }
        ?>
        <?php
    }
    
    public function printPastProjects(User $user, string $type): void {
        $projects = $user->projects();
        $pastProjects = $user->pastProjects();
        
        foreach ($pastProjects as $k => $a_pastproject) {
            foreach ($projects as $v) {
                if ($a_pastproject->getId() == $v->getId()) {
                    unset($pastProjects[$k]);
                    break;
                }
            }
        }
        
        if (count($pastProjects) > 0) {
            ?>
            <script>
                $(document).ready(function(){
                    $('tr.header').click(function(){
                        $(this).find('span').text(function(_, value){return value=='-'?'+':'-'});
                        $(this).nextUntil('tr.header').slideToggle(100, function(){
                        });
                    });
                });
            </script>
            <?php
            echo '<div class="w-box blog-post">';
            echo '<h2>Past projects <i>(Admin only)</i></h2>';
            echo '<div class="padding-15">';
            echo '<table class="table table-bordered table-striped table-comparision table-responsive">';
            
            echo '<tr  class="header">';
            echo '<th colspan="7">Expand<span>-</span></th>';
            echo '</tr>';
            echo '<tr style="display: none;">';
            echo '<th style="text-align: center; vertical-align: middle;">Name</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Creation</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Deletion</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Finish</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Blocked</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Rendertime</a></th>';
            echo '<th style="text-align: center; vertical-align: middle;">Duration</a></th>';
            echo '</tr>';
            
            /** @var \ArrayIterator<PastProject> */
            $iterator = $pastProjects->getIterator();
            $iterator->uasort('\App\Utils\Sort::sortReverseByAttributeId');
            
            foreach ($pastProjects as $a_pastproject) {
                echo '<tr style="display: none;">';
                
                echo '<td>'.$a_pastproject->getName();
                echo ' ';
                echo $a_pastproject->getId();
                echo '</td>';
                
                echo '<td style="text-align: center">';
                echo $this->getTimeWithLocalTimeZone('M d Y', $a_pastproject->getCreation());
                echo '</td>';
                
                echo '<td style="text-align: center">';
                if ($a_pastproject->getDeletion() > 0) {
                    echo $this->getTimeWithLocalTimeZone('M d Y', $a_pastproject->getDeletion());
                }
                echo '</td>';
                
                echo '<td style="text-align: center">';
                if ($a_pastproject->getFinish() > 0) {
                    echo $this->getTimeWithLocalTimeZone('M d Y', $a_pastproject->getFinish());
                }
                echo '</td>';
                
                echo '<td style="text-align: center">';
                if ($a_pastproject->getBlocked() > 0) {
                    echo Project::BLOCK_MESSAGES[$a_pastproject->getBlocked()]['short'];
                }
                echo '</td>';
                
                echo '<td style="text-align: center">';
                if ($a_pastproject->getRendertime() > 0) {
                    echo Misc::humanTime($a_pastproject->getRendertime());
                }
                echo '</td>';
                
                echo '<td style="text-align: center">';
                if ($a_pastproject->getFinish() > 0 && $a_pastproject->getCreation() > 0) {
                    echo Misc::humanTime($a_pastproject->getFinish() - $a_pastproject->getCreation());
                }
                echo '</td>';
                
                echo '</tr>';
            }
            echo '</table>';
            echo '</div>';
            echo '</div>';
        }
    }
    
    public function printLastFrames(User $user, string $type): void {
        $n = 10;
        if (isset($_GET['n'])) {
            $n = $_GET['n'];
        }
        
        echo '<div class="w-box blog-post">';
        echo '<h2>'.sprintf('Last %d participations', $n).'</h2>';
        echo '<div class="padding-15">';
        $frames_done = $user->lastFinishedFrames($n);
        if (count($frames_done) > 0) {
            echo '<table class="table table-bordered table-striped table-comparision table-responsive">';
            
            foreach ($frames_done as $frame) {
                $a_project = $frame->getFrame()->getProject();
                if ($a_project->canManageProject($this->user) || $a_project->canRenderProject($this->user)) {
                    echo '<tr>';
                    
                    $project_html = Misc::stringLimit($a_project->getName(), 25);
                    if ($a_project->canManageProject($this->user)) {
                        $project_html = '<a href="'.$this->router->generate("app_project_manage", ['project' => $a_project->getId()]).'">'.$project_html.'</a>';
                    }
                    echo '<td>'.$project_html.'</td>';
                    
                    if ($type != 'public') {
                        echo '<td title="<img src=&quot;'.$frame->getUrlThumbnailOnShepherd().'&quot; />">'.sprintf('on %s', $this->getTimeWithLocalTimeZone("M d, H:i", $frame->getValidationTime()));
                        $s = $this->sessionRepository->find($frame->getSession());
                        if (is_object($s) && $s->getHostname() != '') {
                            echo ' with ';
                            echo $s->getHostname();
                        }
                        echo '</td>';
                    }
                    echo '</tr>';
                }
            }
            echo '</table>';
        }
        else {
            if ($user->getOrderedFrames() == 0) {
                echo "User didn't render any frame.";
            }
            else {
                echo 'User has no frame stored.';
            }
        }
        echo '</div>';
        echo '</div>';
    }
    
    public function printMachines(User $user, bool $not_public): void {
        $sessions = $user->sessions();
        
        echo '<div class="w-box blog-post">';
        
        if (count($sessions) > 0) {
            echo '<h2>'.sprintf(count($sessions) > 1 ? '%d connected machines' : '%d connected machine', count($sessions)).'</h2>';
            echo '<div class="padding-15">';
            
            echo '<table class="table table-bordered table-striped table-comparision table-responsive">';
            /** @var \ArrayIterator<Session> */
            $iterator = $sessions->getIterator();
            $iterator->uasort('\App\Utils\Sort::sortByAttributeHostname');
            foreach ($sessions as $session) {
                echo '<tr '.($session->getBlocked() != 0 ? ' class="danger" ' : '').'>';
                echo '<td>';
                if ($not_public) {
                    if ($this->user->isModerator()) {
                        echo '<img src="/media/image/flag/'.$session->getCountry().'.png" />';
                        echo '&nbsp;';
                    }
                    echo '<a href="'.$this->router->generate("app_session_single", ['sessionid' => $session->getId()]).'">';
                    if ($session->getHostname() != '') {
                        echo '('.$session->getHostname().') ';
                    }
                }
                echo $session->getComputeDeviceStrHuman();
                if ($not_public) {
                    echo '</a>';
                }
                echo '</td></tr>';
            }
            echo '</table>';
        }
        else {
            echo '<h2>Connected Machine</h2>';
            echo '<div class="padding-15">';
            
            echo 'User has no connected machine.';
        }
        echo '</div>';
        echo '</div>';
    }
    
    public function printSessions(User $user): void {
        $limit = 20;
        $sessions = $this->sessionPastRepository->lastFor($user, $limit);
        if (count($sessions) > 0) {
            echo '<div class="w-box blog-post">';
            echo '<h2>'.sprintf('%d last sessions', $limit).'</h2>';
            echo '<ul>';
            foreach ($sessions as $session) {
                echo '<li>';
                echo '<a href="'.$this->router->generate("app_session_past", ['id' => $session->getId()]).'">';
                echo $this->getTimeWithLocalTimeZone("M d, H:i", $session->getCreationtime());
                echo '</a>';
                echo '</li>';
            }
            echo '</div>';
        }
    }
    
    public function printPointsChart(User $user, string $type): void {
        if ($type != 'public') {
            $points = array();
            $frames = array();
            $rendertimes = array();
            $rows = $this->statsUserRepository->findBy(['user' => $user->getId()]);
            foreach ($rows as $o) {
                $points[$o->getDate()->format('Y-m-d')] = (int)($o->getPoints());
                $frames[$o->getDate()->format('Y-m-d')] = (int)($o->getRenderedFrames());
                $rendertimes[$o->getDate()->format('Y-m-d')] = $o->getRenderTime();
            }
            
            if (count($points) == 0) {
                return; // new user
            }
            
            ksort($points);
            $line_points_timeline = "['Time', 'Points'],";
            foreach ($points as $day => $datas1) {
                $line_points_timeline .= "['".$day."',".$datas1.'],';
            }
            
            ksort($frames);
            $line_frames_timeline = "['Time', 'Frames'],";
            foreach ($frames as $day => $datas1) {
                $line_frames_timeline .= "['".$day."',".$datas1.'],';
            }
            
            ksort($rendertimes);
            $line_rendertime_timeline = "['Time', 'rendertime'],";
            foreach ($rendertimes as $day => $rendertime_day) {
                $day_before = date('Y-m-d', strtotime($day. ' - 1 days'));
                if (array_key_exists($day_before, $rendertimes)) {
                    $rendertime_day_before = $rendertimes[$day_before];
                    $line_rendertime_timeline .= "['".$day."',".(($rendertime_day - $rendertime_day_before) / 3600).'],';
                }
            }
            
            ?>
            <script>
                <?php
                echo 'var line_points_timeline = ['.$line_points_timeline.'];'."\n";
                echo 'var line_frames_timeline = ['.$line_frames_timeline.'];'."\n";
                echo 'var line_rendertime_timeline = ['.$line_rendertime_timeline.'];'."\n";
                ?>
                const ChartStyles = getComputedStyle(document.documentElement);
                let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();
                let colorChartGridline = ChartStyles.getPropertyValue('--color-chart-gridline').trim();
                let colorChartMinorgridline = ChartStyles.getPropertyValue('--color-chart-minorgridline').trim();
                let colorChartLine = ChartStyles.getPropertyValue('--color-chart-line').trim();
                var options = {
                    backgroundColor: { fill: "transparent" },
                    titleTextStyle: {
                        color: colorChartText,
                    },
                    hAxis: {
                        textStyle: { color: colorChartText },
                        titleTextStyle: { color: colorChartText },
                    },
                    vAxis: {
                        textStyle: { color: colorChartText },
                        titleTextStyle: { color: colorChartText },
                        gridlines: {color: colorChartGridline },
                        minValue: 0,
                    },
                    legend: {
                        textStyle: { color: colorChartText },
                        position: "in"
                    },
                    colors: [colorChartLine],
                    fontName: "'Roboto Condensed', sans-serif !important",
                    legend: {position: 'none'},
                };
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    linechart2("Points timeline", line_points_timeline, "line_points_timeline_div");
                    linechart2("Frames rendered timeline", line_frames_timeline, "line_frames_timeline_div");
                    linechart2("Rendertime per day (in hours)", line_rendertime_timeline, "line_rendertime_timeline_div");
                }
            </script>
            <?php
            echo '<div class="w-box blog-post">';
            echo '<div id="line_points_timeline_div" style="width: 100%; height: 500px;"></div>';
            echo '<div id="line_frames_timeline_div" style="width: 100%; height: 500px;"></div>';
            echo '<div id="line_rendertime_timeline_div" style="width: 100%; height: 500px;"></div>';
        }
        else {
            echo '<div class="w-box blog-post">';
        }
        
        $renderDays = $this->renderDayRepository->getConsecutiveRenderDaysFor($user);
        $renderYears = ceil((float)(count($renderDays)) / 365.0);
        
        echo '<div class="container" style="margin: 0 auto;width: 80%">';
        echo '<h3>';
        echo $renderYears > 1 ? 'Consecutive render days for last '.$renderYears.' years' : 'Consecutive render days for last year';
        echo '</h3>';
        echo '<div id="consecutive-render-heatmap"></div>';
        echo '</div>';
        ?>
        <script>
            <?php
            echo "var data = [ ";
            foreach ($renderDays as $renderDay) {
                echo '{date:"'.$renderDay->getDate()->format('Y-m-d').'", count: 1},';
            }
            echo "];\n"
            ?>
            $("#consecutive-render-heatmap").CalendarHeatmap(data, {
                months: <?php echo $renderYears * 12;?>,
                labels: {
                    days: true,
                },
                legend: {
                    show: false,
                },
                tooltips: {
                    show: true,
                },
                coloring: "custom",
            });
        </script>
        <?php
        echo '</div>';
    }
    
    public function printChangeAvatar(): void { ?>
        <div id="profile_change_avatar">
            <h4>Change avatar</h4>
            <form action="<?php echo $this->router->generate('app_user_update_avatar'); ?>" method="post" enctype="multipart/form-data" class="form-light">
                <div class="row">
                    <div class="col-md-12">
                        <strong>Warning: </strong> the image will be resized into a square.
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="file" id="new_avatar" name="new_avatar" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="Change" class="btn btn-primary pull-right">
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    
    public function printChangePassword(User $user): void { ?>
        <div id="profile_password">
            <h4>Change password</h4>
            <form class="form-horizontal" action="javascript:;">
                <div class="form-group">
                    <label for="update_password_old" class="col-sm-2 control-label">Current password</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="update_password_old" placeholder="" name="update_password_old">
                    </div>
                </div>
                <div class="form-group">
                    <label for="update_password_new" class="col-sm-2 control-label">New password</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="update_password_new" placeholder="" name="update_password_new">
                    </div>
                </div>
                <div class="form-group">
                    <label for="update_password_new_verif" class="col-sm-2 control-label">Confirm password</label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="update_password_new_verif" placeholder="Retype your new password" name="update_password_new_verif">
                    </div>
                </div>
                <div class="form-group">
                    <div class="text-right col-sm-9">
                        <button type="button" class="btn btn-primary" onclick="DoChangePassword('<?php echo $this->router->generate('app_user_update_password'); ?>'); return false">Change</button>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    
    public function printLinkRemoveAccount(User $user): void { ?>
        <div id="profile_remove">
            <h4>Remove my account</h4>
            <div class="input-group">
                <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                <span class="input-group-btn">
                    <a class="btn btn-primary" href="<?php echo $this->router->generate('app_user_print_remove'); ?>">Remove</a>
                </span>
            </div>
        </div>
        <?php
    }
    
    public function printChangeEmail(User $user): void { ?>
        <div id="profile_email">
            <h4>Change email</h4>
            <form class="form-inline" action="javascript:;">
                <div class="input-group">
                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                    <input type="email" class="form-control ui-autocomplete-input" id="new_email" placeholder="Your new email">
                    <span class="input-group-btn">
                    <input class="btn btn-primary" type="button" onclick="DoChangeEmail('<?php echo $this->router->generate('app_user_update_email')?>'); return false" value="Change">
                </span>
                </div>
            </form>
        </div>
    <?php }
    
    public function printChangeWebsite(User $user): void { ?>
        <div id="profile_change_website">
            <h4>Change website link</h4>
            <form class="form-inline" action="javascript:;">
                <div class="input-group">
                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                    <input type="text" class="form-control ui-autocomplete-input" id="new_website" name="new_website" placeholder="Your new website" value="<?php echo $user->getWebsite(); ?>">
                    <span class="input-group-btn">
                    <input class="btn btn-primary" id="button_website" type="button" onclick="DoChangeWebsite('<?php echo $this->router->generate('app_user_update_website')?>'); return false" value="Change">
                </span>
                </div>
            </form>
        </div>
    <?php }
    
    public function printChangeSocialNetworks(User $user): void { ?>
        <div id="profile_change_social_networks">
            <h4>Change social networks</h4>
            <form class="form-inline" action="javascript:;">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="change_social_networks_facebook" class="col-sm-3 control-label"><i class="fa-brands fa-facebook fa-3x" style="border:1px;"></i></label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="change_social_networks_facebook" placeholder="" name="change_social_networks_facebook" value="<?php echo $user->getSocialnetworkFacebook(); ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label for="change_social_networks_twitter" class="col-sm-3 control-label"><i class="fa-brands fa-twitter fa-3x" style="border:1px;"></i></label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="change_social_networks_twitter" placeholder="" name="change_social_networks_twitter" value="<?php echo $user->getSocialnetworkTwitter(); ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label for="change_social_networks_instagram" class="col-sm-3 control-label"><i class="fa-brands fa-instagram fa-3x" style="border:1px;"></i></label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="change_social_networks_instagram" placeholder="" name="change_social_networks_instagram" value="<?php echo $user->getSocialnetworkInstagram(); ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label for="change_social_networks_youtube" class="col-sm-3 control-label"><i class="fa-brands fa-youtube fa-3x" style="border:1px;"></i></label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="change_social_networks_youtube" placeholder="" name="change_social_networks_youtube" value="<?php echo $user->getSocialnetworkYoutube(); ?>">
                    </div>
                </div>
                <br />
                <div class="form-group">
                    <div class="text-right col-sm-12">
                    <span class="input-group-btn">
                        <input class="btn btn-primary" id="button_change_social_network" type="button" onclick="DoChangeSocialNetwork('<?php echo $this->router->generate('app_user_update_socialnetwork')?>', '<?php echo $user->getId();?>'); return false" value="Change">
                    </span>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    
    public function printAffiliateLink(User $user): void {
        echo '<div id="profile_referer_link">';
        echo '<h4>Referral link</h4>';
        $link = $this->config['site']['url'].'/?tag='.$user->getId();
        echo sprintf('We want to reward users who spread knowledge about SheepIt.<br>When a referred user has rendered %s public frames, you and the user will get %s points.', number_format($this->config['affiliate_minimum_frame']), number_format($this->config['affiliate_reward']));
        echo '<br>';
        echo 'Referral link: <a href="'.$link.'">'.$link.'</a>';
        ?>
        <br>
        <br>
        <div>
            You can also use a selection of banners and logos:
            <div class="container">
                <div class="row" style="padding-top: 10px">
                    <a href="/media/image/public/banner/Sheepit-Banner_01.png"><img src="/media/image/public/banner/Sheepit-Banner_01.png"></a>
                </div>
                <div class="row" style="padding-top: 10px">
                    <a href="/media/image/public/banner/Sheepit-Banner_02.png"><img src="/media/image/public/banner/Sheepit-Banner_02.png"></a>
                </div>
                <div class="row" style="padding-top: 10px">
                    <a href="/media/image/public/title_v5.png"><img src="/media/image/public/title_v5.png" style="width: 40%"></a>
                </div>
                <div class="row" style="padding-top: 10px">
                    <a href="/media/image/public/logo_v5a.png"><img src="/media/image/public/logo_v5a.png" style="width: 160px"></a>
                </div>
                <div class="row" style="padding-top: 10px">
                    <a href="/media/image/public/banner/Sheepit-Banner_05.png"><img src="/media/image/public/banner/Sheepit-Banner_05.png" style="width: 160px"></a>
                </div>

                <div class="row" style="padding-top: 20px">
                    <a href="/media/image/public/banner/Sheepit-Banner_04.png"><img src="/media/image/public/banner/Sheepit-Banner_04.png" ></a>
                    <a href="/media/image/public/banner/Sheepit-Banner_03.png"><img src="/media/image/public/banner/Sheepit-Banner_03.png"style="padding-left: 10px"></a>
                </div>
            </div>
        </div>
        </div>
        <?php
    }
    
    public function printMyEmail(User $user): void {
        ?>
        <h4>My email</h4>
        <p><?php echo $user->getEmail(); ?></p>
        <?php
    }
    
    public function printAwards(User $user): void {
        $awards = $user->awardsWithDate();
        $categories_human_text = array(
            'rendered_frames' => 'Rendered frames',
            'render_time' => 'Render duration',
            'session_duration' => 'Session duration',
            'event' => 'Community events',
            'best_renderer' => 'Strongest renderer',
            'misc' => 'Special achievements');
        $categories = array(
            'rendered_frames' => array(),
            'render_time' => array(),
            'session_duration' => array(),
            'best_renderer' => array(),
            'misc' => array(),
            'event' => array()); // initial value for display order
        
        foreach ($awards as $award) {
            $cat = $award->category();
            if (array_key_exists($cat, $categories) == false) {
                $categories[$cat] = array();
            }
            $categories[$cat] [] = $award;
        }
        
        $awards_count = $this->awardRepository->countByType();
        
        ?>
        <div id="profile_awards" class="col-md-12">
            <div class="w-box blog-post padding-15">
                <h3>Awards</h3>
                <?php
                foreach ($categories as $cat_name => $cat_contents) {
                    if (count($cat_contents) == 0 || $cat_name == 'rendered_frames_ratio') {
                        continue;
                    }
                    usort($cat_contents, '\App\Utils\Sort::sortAwardByLevel');
                    
                    echo '<section class="slice">';
                    echo '<div class="w-section">';
                    echo '<div class="container">';
                    if (array_key_exists($cat_name, $categories_human_text)) {
                        echo '<h4>'.$categories_human_text[$cat_name].'</h4>';
                    }
                    echo '<div class="row">';
                    foreach ($cat_contents as $award) {
                        if ($award->isDisplayable() == false) {
                            continue;
                        }
                        $description = $award->humanDescription();
                        if ($award->reward() > 0 && $award->canBeEarn()) {
                            $description .= '<br>Reward: '.number_format($award->reward()).' points';
                        }
                        
                        $opacity = '';
                        if ($award->isEarned) {
                            $description .= '<br>Earned on '.$this->getTimeWithLocalTimeZone('F jS, Y', $award->earnTime);
                        }
                        else {
                            $opacity = "opacity: 0.45;filter: grayscale(100%); -webkit-filter: grayscale(100%);";
                            $remaining = $award->remainingHumanText($user);
                            if ($remaining != null) {
                                $description .= '<br>'.$remaining;
                            }
                        }
                        
                        $award_type = $award->getDoctrineType();
                        if (array_key_exists($award_type, $awards_count)) {
                            $description .= '<br>'.sprintf('<i>%s users have this award.</i>', number_format($awards_count[$award_type]));
                        }
                        
                        echo '<img style="'.$opacity.'padding: 20px" src="'.$award->imagePath().'" alt="'.strip_tags($description).'"';
                        echo ' title="'.str_replace('"', '&quot;', $description).'">';
                    }
                    echo '</div>';
                    echo '</div>';
                    echo '</div>';
                    echo '</section>';
                }
                ?>
            </div>
        </div>
        <?php
    }
    
    public function printGifts(User $user): void {
        $gifts = $user->gifts();
        if ($gifts->isEmpty() == false) {
            echo '<div class="w-box blog-post">';
            echo '<h2>Gifts</h2>';
            echo '<div class="padding-15">';
            echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
            echo '<thead>';
            echo '<tr>';
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Value</a></th>';
            echo '<th>Description</th>';
            echo '<th><a class="sortable" href="#" data-default-order="desc" data-type="int">Expires on</a></th>';
            echo '</tr>';
            echo '</thead>';
            echo '<tbody>';
            foreach ($gifts as $a_gift) {
                echo '<tr>';
                echo '<td data-sort="'.$a_gift->getValue().'">';
                echo '<a href="'.$this->router->generate('app_gift_show', ['gift' => $a_gift->getId(), 'key' => $a_gift->getKey()]).'">';
                echo number_format($a_gift->getValue());
                echo '</a>';
                echo '<td>';
                echo strip_tags($a_gift->getComment());
                echo '</td>';
                echo '</td>';
                echo '<td data-sort="'.$a_gift->getExpiration().'">';
                echo $this->getTimeWithLocalTimeZone('F, jS', $a_gift->getExpiration());
                echo '</td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
        }
    }
    
    public function printChangeNotifications(User $user, string $type): void {
        $notfications = array(
            User::NOTIFICATION_MASK_ON_PROJECT_FINISHED => 'Project is rendered',
            User::NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED => 'The first frame of my project is rendered',
            User::NOTIFICATION_MASK_NEWSLETTER => 'News or an important event is posted',
            User::NOTIFICATION_MASK_AWARD => 'I get an award',
            User::NOTIFICATION_MASK_AFFILIATE_REFERRED => sprintf('A referral has rendered %s public frames', number_format($this->config['affiliate_minimum_frame'])),
            User::NOTIFICATION_MASK_AFFILIATE_REFERER => 'I get the referrer reward',
            User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM => 'Someone requests to join the team I own',
            User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM_ACCEPTED => 'I am accepted into a team',
            User::NOTIFICATION_MASK_PATREON => 'I get my Patreon reward',
            User::NOTIFICATION_MASK_SESSION_BLOCKED => 'My render session has been blocked',
        );
        ?>
        <div id="profile_notifications">
            <h4>Notifications</h4>
            Get notified by e-mail when:
            <form id="profile_modification_notification" action="javascript:;">
                <?php
                foreach ($notfications as $mask => $human_text) {
                    $javascript = '';
                    if ($type == 'private') {
                        $javascript = 'onclick="DoChangeNotification(\''.$mask.'\', \'notification_'.$mask.'\');"';
                    }
                    else {
                        $javascript = ' disabled="disabled" ';
                    }
                    $checked = '';
                    if ($user->noficationIsEnabled($mask)) {
                        $checked = 'checked="checked"';
                    }
                    
                    echo '<div class="checkbox">';
                    echo '<label>';
                    echo '<input '.$javascript.' '.$checked.' type="checkbox" name="notification_'.$mask.'" id="notification_'.$mask.'" value="notification_'.$mask.'">';
                    echo $human_text;
                    echo '</label>';
                    echo '</div>';
                }
                ?>
            </form>
        </div>
        <?php
    }
    
    public function printChangeScheduler(User $user, string $type): void {
        $javascript_scheduler_rendermyprojectfirst = '';
        if ($type == 'private') { // it's not the admin who looks at the profile
            $javascript_scheduler_rendermyprojectfirst = 'onclick="DoChangeSchedulerValue(\'render_my_project_first\', \'scheduler_rendermyprojectfirst\');"';
        }
        else {
            $javascript_scheduler_rendermyprojectfirst = 'disabled="disabled" ';
        }
        $javascript_scheduler_rendermyteamsprojectsfirst = '';
        if ($type == 'private') { // it's not the admin who looks at the profile
            $javascript_scheduler_rendermyteamsprojectsfirst = 'onclick="DoChangeSchedulerValue(\'render_my_team_projects_first\', \'scheduler_rendermyteamsprojectsfirst\');"';
        }
        else {
            $javascript_scheduler_rendermyteamsprojectsfirst = 'disabled="disabled" ';
        }
        if ($type == 'private') { // it's not the admin who looks at the profile
            $javascript_scheduler_heavy_project = 'onclick="DoChangeSchedulerValue(\'render_heavy_project\', \'scheduler_heavy_project\');"';
        }
        else {
            $javascript_scheduler_heavy_project = 'disabled="disabled" ';
        }
        $checked_scheduler_rendermyprojectfirst = $user->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST) ? 'checked="checked"' : '';
        $checked_scheduler_rendermyteamsprojectsfirst = $user->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST) ? 'checked="checked"' : '';
        $checked_scheduler_heavy_project = $user->schedulerIsEnabled(User::SCHEDULER_MASK_HEAVY_PROJECT) ? 'checked="checked"' : '';
        
        ?>

        <div id="profile_scheduler">
            <h4>Scheduler settings</h4>

            <div>
                By default you will render your project first, but if you have an old machine it might be better to let the job scheduler choose an easier frame.<br/>Highest priority order is : users < my team < myself.
            </div>
            <form id="account_scheduler_settings" action="javascript:;">
                <div class="checkbox">
                    <label>
                        <input <?php echo $javascript_scheduler_rendermyprojectfirst; ?> <?php echo $checked_scheduler_rendermyprojectfirst; ?> type="checkbox" name="scheduler_rendermyprojectfirst" id="scheduler_rendermyprojectfirst" value="1">
                        <span title="By default you will render your project first, regardless of your points.<br>But if you have an old machine it might be better to let the job scheduler choose an easier frame.">Render my projects first</span>
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input <?php echo $javascript_scheduler_rendermyteamsprojectsfirst; ?> <?php echo $checked_scheduler_rendermyteamsprojectsfirst; ?> type="checkbox" name="scheduler_rendermyteamsprojectsfirst" id="scheduler_rendermyteamsprojectsfirst" value="1">
                        Render my team's projects first
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input <?php echo $javascript_scheduler_heavy_project; ?> <?php echo $checked_scheduler_heavy_project; ?> type="checkbox" name="scheduler_heavy_project" id="scheduler_heavy_project" value="1">
                        <span title="If you machine is really powerful, i.e. over 200%, you might want to render heavier project to reduce downtime (ratio between render vs download+extract)">Heavy projects have high prority</span>
                    </label>
                </div>
            </form>
            <?php
            echo '<div id="profile_high_priority_users">';
            echo '<h5>';
            echo 'You will render projects of these users first.';
            echo '</h5>';
            
            $ids = $user->getHighPriorityUsers();
            echo '<table>';
            foreach ($ids as $login) {
                echo '<tr>';
                echo '<td class="table_avatar"><span style="display: none;">'.$login.'</span>';
                $this->printAvatar($login);
                echo '</td>';
                echo '<td class="owner_name"><span style="display: none;">'.$login.'</span><a href="'.$this->router->generate('app_user_profile', ['user' => $login]).'" >'.$login.'</a></td>';
                echo '<td>';
                echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_priority_remove', ['user' => $login]).'\', \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'scheduler']).'\'); return false"" value="Remove" />';
                
                echo '</td>';
                echo '</tr>';
            }
            
            echo '<td>';
            echo '</td>';
            echo '<td>';
            echo '<input id="account_add_high_priority_user" />';
            echo '<script>';
            echo '$(function() {$("#account_add_high_priority_user").autocomplete({minLength: 3, source: "'.$this->router->generate('app_user_list_from_term').'"});});';
            echo '</script>';
            echo '</td>';
            echo '<td>';
            echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\'/user/priority/add/\' + $(\'#account_add_high_priority_user\').val(), \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'scheduler']).'\'); return false" value="Add" />';
            echo '</td>';
            echo '</table>';
            echo '</div>';
            ?>
        </div>
        <?php
    }
    
    public function printSharedKeys(User $user, string $type): void {
        if ($type == 'public') {
            return;
        }
        
        echo '<div id="profile_shared_keys">';
        echo '<h4>Render keys</h4>';
        echo '<p>';
        echo "You can share a 'public key' with someone, so they can render for you. It is especially useful if you want a friend to help render a project, but you don't want to give them access to your account or password.<br>They will be able to log in to the client, but they will <strong>not be able to log in to the website</strong>.<br>The 'public key' is generated randomly, so it is safe to share it.</i><br>";
        echo '</p>';
        echo '<p>';
        echo 'To use it, give them your username and the \'public key\'. They will have to download the .jar or .exe from the <a href="'.$this->router->generate("app_getstarted_main").'">Get Started Page</a>, and then use your username and the public key to log into the client (they have to use the key as a password).';
        echo '</p>';
        echo '<p>';
        echo '<strong>When you remove a render key in use, the session associated will be also removed.</strong>';
        echo '</p>';
        echo '<p>';
        echo 'A public key \'client\' is automatically created on the first connection of a client, it will be used instead of the actual password. That way your password is never stored on your computer';
        echo '</p>';
        
        $sessions = $user->sessions();
        
        $keys = $user->getPublicKeys();
        echo '<table style="width: 95%">';
        echo '<thead>';
        echo '<th>Public key</th>';
        echo '<th>Comment</th>';
        echo '<th>In use</th>';
        echo '<th></th>';
        echo '</thead>';
        if (count($keys) > 0) {
            foreach ($keys as $public_key) {
                echo '<tr>';
                echo '<td>'.$public_key->getId().'</td><td>'.$public_key->getComment().'</td>';
                echo '<td>';
                
                foreach ($sessions as $session) {
                    if (is_object($session->getRenderKey()) && $session->getRenderKey() == $public_key) {
                        echo 'YES';
                        break;
                    }
                }
                echo '</td>';
                echo '<td>';
                echo "\n";
                echo '<input class="btn btn-primary" type="button" onclick="requestReloadOnSuccess(\''.$this->router->generate('app_user_renderkey_del', ['renderkey' => $public_key->getId()]).'\'); return false" value="Remove" />';
                echo "\n";
                echo '</td>';
                echo '</tr>';
            }
        }
        echo '<tr>';
        echo '<td></td>';
        echo '<td>';
        echo '<input type="text" id="public_key_add_comment" name="public_key_add_comment" value="" placeholder="comment for yourself" style="width: 95%;text-align: center;">';
        echo '</td>';
        echo '<td>';
        echo '<input class="btn btn-primary" type="button" onclick="public_key_add(\'/user/renderkey/add/\', \''.$user->getId().'\'); return false" value="Add" />';
        echo '</td>';
        echo '</table>';
        echo '</div>';
    }
    
    public function printSponsor(User $user, string $type): void {
        if ($type == 'public') {
            return;
        }
        
        $javascript_sponsor_give = '';
        if ($type == 'private') { // it's not the admin who looks at the profile
            $javascript_sponsor_give = 'onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_sponsor_give_enable', ['enable' => $user->getSponsorGive() ? '0' : '1']).'\',\'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'sponsor']).'\');"';
        }
        else {
            $javascript_sponsor_give = 'disabled="disabled" ';
        }
        $javascript_sponsor_receive = '';
        if ($type == 'private') { // it's not the admin who looks at the profile
            $javascript_sponsor_receive = 'onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_sponsor_receive_enable', ['enable' => $user->getSponsorReceive() ? '0' : '1']).'\',\'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'sponsor']).'\');"';
        }
        else {
            $javascript_sponsor_receive = 'disabled="disabled" ';
        }
        
        echo '<div id="profile_sponsor">';
        echo '<h4>Sponsorship</h4>';
        echo '<p>';
        echo 'The sponsorship list is for users that you see are struggling to keep enough points, users whose projects you like, or users you know personally. It\'s an easy way for you to support their work. By adding users to this list, the points you earn will go to them and not to yourself. Each time you render a frame, the points earned will go to a random user on this list.<br>SheepIt is about community.';
        echo '</p>';
        
        echo '<form id="account_sponsor_settings" action="javascript:;">';
        echo '<div class="checkbox">';
        echo '<label>';
        echo 'Enable Sponsoring (if you deselect, you will stop donating points to users on your list.)';
        echo '<input type="checkbox" '.($user->getSponsorGive() ? ' checked="checked" ' : '').$javascript_sponsor_give.' name="sponsor_receive_enable" id="sponsor_receive_enable" value="1">';
        echo '</label>';
        echo '</div>';
        echo '<div class="checkbox">';
        echo '<label>';
        echo 'Enable Receiving Sponsorships (if you deselect, you will not receive points when people add you to their sponsorship list)';
        echo '<input type="checkbox" '.($user->getSponsorReceive() ? ' checked="checked" ' : '').$javascript_sponsor_receive.' name="sponsor_give_enable" id="sponsor_give_enable" value="1">';
        echo '</label>';
        echo '</div>';
        
        echo '</form>';
        echo '<br>';
        echo 'Sponsored Users';
        
        $sponsoredUsers = $user->getSponsorUsers();
        uasort($sponsoredUsers, '\App\Utils\Sort::sortByAttributeId');
        echo '<table>';
        foreach ($sponsoredUsers as $sponsoredUser) {
            echo '<tr>';
            echo '<td class="table_avatar"><span style="display: none;">'.$sponsoredUser->getId().'</span>';
            $this->printAvatar($sponsoredUser->getId());
            echo '</td>';
            echo '<td class="owner_name"><span style="display: none;">'.$sponsoredUser->getId().'</span><a href="'.$this->router->generate('app_user_profile', ['user' => $sponsoredUser->getId()]).'" >'.$sponsoredUser->getId().'</a></td>';
            echo '<td>';
            echo "\n";
            echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_sponsor_remove', ['user' => $sponsoredUser->getId()]).'\', \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'sponsor']).'\'); return false" value="Remove" />';
            echo "\n";
            echo '</td>';
            echo '</tr>';
        }
        
        echo '<td>';
        echo '</td>';
        echo '<td>';
        echo '<input id="account_add_sponsor" />';
        echo '<script>';
        echo '$(function() {$("#account_add_sponsor").autocomplete({minLength: 3, source: "'.$this->router->generate('app_user_list_from_term').'"});});';
        echo '</script>';
        echo '</td>';
        echo '<td>';
        echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\'/user/sponsor/add/\' + $(\'#account_add_sponsor\').val(), \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'sponsor']).'\'); return false" value="Add" />';
        echo '</td>';
        echo '</table>';
        echo '</div>';
    }
    
    public function printBlockRenderers(User $user, string $type): void {
        if ($type == 'public') {
            return;
        }
        
        echo '<div id="profile_blacklist_renderer">';
        echo '<h4>Blacklist renderer</h4>';
        echo '<p>';
        echo 'These users will not render my projects.';
        echo '</p>';
        
        $ids = $user->getBlockedRenderers();
        natcasesort($ids);
        echo '<table>';
        foreach ($ids as $login) {
            echo '<tr>';
            echo '<td class="table_avatar"><span style="display: none;">'.$login.'</span>';
            $this->printAvatar($login);
            echo '</td>';
            echo '<td class="owner_name"><span style="display: none;">'.$login.'</span><a href="'.$this->router->generate('app_user_profile', ['user' => $login]).'" >'.$login.'</a></td>';
            echo '<td>';
            echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_block_remove_renderer', ['user' => $login]).'\', \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'blacklist']).'\'); return false" value="Remove" />';
            echo '</td>';
            echo '</tr>';
        }
        
        echo '<td>';
        echo '</td>';
        echo '<td>';
        echo '<input id="account_add_blocked_renderer" />';
        echo '<script>';
        echo '$(function() {$("#account_add_blocked_renderer").autocomplete({minLength: 3, source: "'.$this->router->generate('app_user_list_from_term').'"});});';
        echo '</script>';
        echo '</td>';
        echo '<td>';
        echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\'/user/block/renderer/add/\' + $(\'#account_add_blocked_renderer\').val(), \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'blacklist']).'\'); return false" value="Block" />';
        echo '</td>';
        echo '</table>';
        echo '</div>';
    }
    
    public function printBlockOwners(User $user, string $type): void {
        if ($type == 'public') {
            return;
        }
        
        ?>
        <div id="profile_blacklist_owner">
            <h4>Blacklist owner</h4>
        <?php
        echo '<p>';
        echo 'I will <b>not</b> render projects of these users.';
        echo '</p>';
        
        $ids = $user->getBlockedOwners();
        natcasesort($ids);
        echo '<table>';
        foreach ($ids as $login) {
            echo '<tr>';
            echo '<td class="table_avatar"><span style="display: none;">'.$login.'</span>';
            $this->printAvatar($login);
            echo '</td>';
            echo '<td class="owner_name"><span style="display: none;">'.$login.'</span><a href="'.$this->router->generate('app_user_profile', ['user' => $login]).'" >'.$login.'</a></td>';
            echo '<td>';
            echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\''.$this->router->generate('app_user_block_remove_owner', ['user' => $login]).'\', \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'blacklist']).'\'); return false" value="Remove" />';
            echo '</td>';
            echo '</tr>';
        }
        
        echo '<td>';
        echo '</td>';
        echo '<td>';
        echo '<input id="account_add_blocked_owner" />';
        echo '<script>';
        echo '$(function() {$("#account_add_blocked_owner").autocomplete({minLength: 3, source: "'.$this->router->generate('app_user_list_from_term').'"});});';
        echo '</script>';
        echo '</td>';
        echo '<td>';
        echo '<input class="btn btn-primary" type="button" onclick="requestActionThemShowOnId(\'/user/block/owner/add/\' + $(\'#account_add_blocked_owner\').val(), \'user_settings_section\', \''.$this->router->generate('app_user_print_user_settings_section', ['user' => $user->getId(), 'show_category' => 'blacklist']).'\'); return false" value="Block" />';
        echo '</td>';
        echo '</table>';
        echo '</div>';
    }
    
    public function printChangeAcl(User $user): void {
        echo '<div id="profile_permissions" class="w-box blog-post">';
        echo '<h2>Permissions</h2>';
        echo '<div class="padding-15">';
        echo '<ul>';
        $acl_methods = User::ACL_METHODS;
        foreach ($acl_methods as $method) {
            $res = $user->$method();
            if (is_bool($res) && $res == true) {
                echo '<li>';
                echo $method;
                echo '</li>';
            }
        }
        echo '</ul>';
        echo '</div>';
        echo '</div>';
    }
    
    public function printGiveAward(User $user): void {
        echo '<div id="profile_permissions" class="w-box blog-post">';
        echo '<h2>Give an award</h2>';
        echo '<div class="padding-15">';
        
        $awards = array();
        foreach ($user->awardsWithStatus() as $award_name => $earn) {
            if ($earn == false) {
                $class_name = "\\App\Entity\\".$award_name;
                $a = new $class_name();
                $a->IsEarned = false;
                $awards [] = $a;
            }
        }
        
        usort($awards, '\App\Utils\Sort::sortAwardByCategory');
        echo '<div class="input-group">';
        echo '<select name="type" id="account_give_award_type" name="award_type" class="form-control">';
        foreach ($awards as $award) {
            echo '<option value="'.$award->getShortName().'">'.str_replace('Award', '', $award->getShortName()).'</option>';
        }
        echo '</select>';
        echo '<span class="input-group-btn">';
        echo '<input type="submit" name="account_give_award"  class="btn btn-primary" ';
        echo 'onclick="requestReloadOnSuccess(\'/fulladmin/give_award/'.$user->getId().'/\' + $(\'#account_give_award_type\').val(), {}); return false" ';
        echo 'value="Give"/>';
        echo '</span>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    
    public function pointsBadge(float $c, bool $display_exact_value): string {
        $explanation_points = '';
        $explanation_points .= 'How points are calculated:';
        $explanation_points .= '<ul>';
        $explanation_points .= '<li>';
        $explanation_points .= sprintf('<strong>remove</strong> %s * render time (in minutes) when ordering a frame.', abs($this->config['rank']['points']['ordered']['minute']));
        $explanation_points .= '</li>';
        
        $explanation_points .= '<li>';
        $explanation_points .= sprintf('<strong>add</strong> %s * render time (in minutes) when rendering a frame.', abs($this->config['rank']['points']['rendered']['minute']));
        $explanation_points .= '</li>';
        $explanation_points .= '<li>';
        $explanation_points .= sprintf('If the owner of a project renders a frame, no subtraction is made and they get %d%% of the points.', (int)(100 * $this->config['rank']['points']['coef_owner']));
        $explanation_points .= '</li>';
        
        $explanation_points .= '<li>';
        $explanation_points .= 'The render time is not the actual duration on your machine (since it would give slower machines an advantage).Instead, it\'s scaled to a <i>reference machine</i>.<br >';
        $explanation_points .= 'For example, a 100 frame long project at 15min/frame (on the reference machine), the owner will spend <b>'.number_format(-1 * 100 * 15 * $this->config['rank']['points']['ordered']['minute']).'</b> points ( 1 * 100 * 15 * '.$this->config['rank']['points']['ordered']['minute'].' = '.(1 * 100 * 15 * $this->config['rank']['points']['ordered']['minute']).').<br>';
        $explanation_points .= 'No matter what machine renders a frame, they will earn the same points for it.<br>';
        $explanation_points .= 'Explanation with 200% machine: 100 * 2.0 * 7.5 * '.$this->config['rank']['points']['rendered']['minute'].' = '.number_format(100 * 2.0 * 7.5 * $this->config['rank']['points']['rendered']['minute']).' <br>';
        $explanation_points .= 'and 25% machine: 100 * 0.25 * 45 * '.$this->config['rank']['points']['rendered']['minute'].' = '.number_format(100 * 0.25 * 60 * $this->config['rank']['points']['rendered']['minute']);
        $explanation_points .= '</li>';
        
        $explanation_points .= '<li>';
        $scale_gpu_cpu = (new GPU())->getPowerRatioOverCPU();
        $explanation_points .= 'Since 2023, the CPU and GPU power rating are split, it\'s the same logic for both but on a different scale: GPU are '.(int)($scale_gpu_cpu).'x a CPU.<br>A 100% GPU will earn '.number_format($scale_gpu_cpu * $this->config['rank']['points']['rendered']['minute']).' points per minute instead of '.$this->config['rank']['points']['rendered']['minute'].'.<br>Same for ordering a frame: '.number_format(-1 * $scale_gpu_cpu * $this->config['rank']['points']['ordered']['minute']).' on GPU versus '.(-1 * $this->config['rank']['points']['ordered']['minute']).' on CPU.';
        $explanation_points .= '</li>';
        
        $explanation_points .= '</ul>';
        
        if ($c > 0) {
            return '<span class="label label-success" title="'.$explanation_points.'">'.($display_exact_value ? number_format($c) : Misc::humanSize($c)).'</span>';
        }
        else {
            if ($c < 0) {
                return '<span class="label label-danger" title="'.$explanation_points.'">'.($display_exact_value ? number_format($c) : Misc::humanSize($c)).'</span>';
            }
            else {
                return '<span class="label label-default" title="'.$explanation_points.'">0</span>';
            }
        }
    }
    
//    public function printAdventCalendar(): void {
//        echo $this->adventCalendar->htmlCalendar('');
//    }
    
    public function displayDashboardStats(): void {
        $global_stats = $this->main->globalStats(180); // $this->config['graph']['frame_remaining']['timescope']);
        $global_user_stats = $this->userRepository->getSumStats();
        $frames_remaining = $this->tileRepository->countPublicRemainingFrames();
        
        $a_user = $this->user;
        if (is_null($a_user)) {
            // generic user for the stats
            $a_user = new User();
            $a_user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1);
            $a_user->setId('generic_login_'.time());
        }
        
        $active_projects = $this->main->countPublicActiveProjects();
        
        $frames_graph_data = "[['Time', 'render time (in month)'],";
        
        $days = array();
        foreach ($global_stats as $i => $data) {
            $day = 3600 * 24 * (int)($data->getId() / (3600 * 24));
            if (isset($days[$day]) == false) {
                $days[$day] = array('min' => PHP_INT_MAX, 'max' => 0, 'min_time' => PHP_INT_MAX, 'max_time' => 0);
            }
            
            if ($data->getFramesRendered() < $days[$day]['min']) {
                $days[$day]['min'] = $data->getFramesRendered();
            }
            if ($data->getFramesRendered() > $days[$day]['max']) {
                $days[$day]['max'] = $data->getFramesRendered();
            }
            if ($data->getTimeOrdered() < $days[$day]['min_time']) {
                $days[$day]['min_time'] = $data->getTimeOrdered();
            }
            if ($data->getTimeOrdered() > $days[$day]['max_time']) {
                $days[$day]['max_time'] = $data->getTimeOrdered();
            }
        }
        
        $day = 3600 * 24 * (int)(time() / (3600 * 24));
        if (isset($days[$day]) == false) {
            $days[$day] = array('min' => PHP_INT_MAX, 'max' => 0, 'min_time' => PHP_INT_MAX, 'max_time' => 0);
        }
        
        $day_before = 3600 * 24 * (int)((time() / (3600 * 24)) - 1);
        if (isset($days[$day_before]) == true) {
            $days[$day]['min'] = $days[$day_before]['max'];
        }
        $day_before = 3600 * 24 * (int)((time() / (3600 * 24)) - 1);
        if (isset($days[$day_before]) == true) {
            $days[$day]['min_time'] = $days[$day_before]['max_time'];
        }
        unset($days[$day]);
        
        reset($days);
        unset($days[key($days)]);
        
        $min_timescope_frame_rendered = time() - ($this->config['graph']['frame_rendered']['timescope'] + 1) * 3600 * 24;
        foreach ($days as $day => $datas1) {
            if ($min_timescope_frame_rendered <= $day) {
                $value4 = (float)($datas1['max_time'] - $datas1['min_time']) / (30.0 * 86400.0); // in months
                //Logger::info($day.' -> '.date("M d", $day));
                if ($day == 1725580800) { // 2024-09-06
                    $frames_graph_data .= "['".date("M d", $day)."',".'12.8'.'],';
                }
                elseif ($day == 1732924800) { // 2024-11-30
                    $frames_graph_data .= "['".date("M d", $day)."',".'12.8'.'],';
                }
                else {
                    $frames_graph_data .= "['".date("M d", $day)."',".sprintf("%.1f", $value4).'],';
                }
            }
        }
        
        $frames_graph_data .= ']';
        
        
        $global_stats = array_slice($global_stats, -11, 11);
        
        $connected_clients = $this->sessionRepository->countConnectedMachines();
        $processing_frames = $this->tileRepository->count(['status' => Constant::PROCESSING]);
        $total_frames = $global_user_stats['ordered_frames'];
        $total_projects = $global_user_stats['nb_projects']; // it's actually the number of projects added...
        
        $frames_per_hour = $this->main->getCountRenderedFramesSinceLast12hoursFromCache() / 12.0;
        
        $frames_remaining_data = array();
        $active_projects_data = array();
        $connected_clients_data = array();
        $processing_frames_data = array();
        foreach ($global_stats as $stats) {
            array_push($frames_remaining_data, $stats->getFramesRemainingPublic());
            array_push($active_projects_data, $stats->getActiveProjects());
            array_push($connected_clients_data, $stats->getSessions());
            array_push($processing_frames_data, $stats->getFramesProcessing());
        }
        
        ?>
        <div class="w-section inverse">
            <div class="container">

                <div class="row">
                    <?php $this->homeDisplayStatBox('Frames remaining', $this->router->generate("app_home_frames"), $frames_remaining, $frames_remaining_data); ?>
                    <?php $this->homeDisplayStatBox('Active projects', $this->router->generate("app_home_projects"), $active_projects, $active_projects_data); ?>
                    <?php $this->homeDisplayStatBox('Connected clients', $this->router->generate("app_home_machines"), $connected_clients, $connected_clients_data); ?>
                    <?php $this->homeDisplayStatBox('Processing frames', $this->router->generate("app_home_frames"), $processing_frames, $processing_frames_data); ?>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="w-box">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 stat-side">
                                        <i class="fa fa-database"></i> Sheepit power
                                        <h2><?php echo number_format($frames_per_hour); ?></h2>
                                        computed frames per hour
                                        <div class="bottom">
                                            <?php if ($frames_per_hour != 0) {
                                                echo(Misc::humanTime(3600 * $frames_remaining / $frames_per_hour));
                                                echo ' to complete all projects';
                                            }
                                            else {
                                                echo("It's time to <a href=\"".$this->router->generate("app_getstarted_main")."\">plug your computer!</a>");
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 stat-side">
                                        <i class="fa fa-signal"></i><span style="margin-left: 4px;">How many months of rendering do we complete every day?</span>
                                        <div id="linechart_div_remaining_frames_public_by_hour"></div>
                                    </div>
                                    <div class="col-md-3 stat-side">
                                        <i class="fa fa-clock-o"></i> History
                                        <h2 id="total_frames"><?php echo number_format((int)$total_frames) ?></h2>
                                        total frames rendered
                                        <div class="bottom">
                                            <?php echo number_format((int)$total_projects); ?> computed projects
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    var frames_graph_data = <?php echo $frames_graph_data; ?>;
                    const ChartStyles = getComputedStyle(document.documentElement);
                    let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();
                    var options = {
                        hAxis: {
                            textStyle: {color: colorChartText},
                            titleTextStyle: {color: colorChartText},
                            slantedText: false,
                            maxAlternation: 1,
                            maxTextLines: 1,
                        },
                        legend: {position: 'none'},
                        curveType: 'function',
                        chartArea: {top: 20, left: 60, width: '100%', height: '80%'},
                    };
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        areachart("", options, frames_graph_data, "linechart_div_remaining_frames_public_by_hour");
                    }

                    var frames_per_hour = <?php echo $frames_per_hour; ?>;
                    if (frames_per_hour > 0) {
                        var total_frames = <?php echo $total_frames; ?>;
                        var interval = 500;
                        frames_per_interval = frames_per_hour * interval / 3600000;
                        setInterval(function () {
                            total_frames = total_frames + frames_per_interval;
                            $('#total_frames').html(Math.floor(total_frames).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                        }, interval);
                    }
                </script>
            </div>
        </div>
        <?php
    }
}
