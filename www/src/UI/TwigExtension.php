<?php

namespace App\UI;

use App\Controller\Admin\AdminToolAdventCalendar;
use App\Controller\Admin\AdminToolAwards;
use App\Controller\Admin\AdminToolCache;
use App\Controller\Admin\AdminToolCommunityEvents;
use App\Controller\Admin\AdminToolConfig;
use App\Controller\Admin\AdminToolEfficiency;
use App\Controller\Admin\AdminToolErrors;
use App\Controller\Admin\AdminToolFrames;
use App\Controller\Admin\AdminToolGifts;
use App\Controller\Admin\AdminToolHostStats;
use App\Controller\Admin\AdminToolHWID;
use App\Controller\Admin\AdminToolLog;
use App\Controller\Admin\AdminToolMisc;
use App\Controller\Admin\AdminToolProjects;
use App\Controller\Admin\AdminToolResponseTime;
use App\Controller\Admin\AdminToolServers;
use App\Controller\Admin\AdminToolShepherdsHealth;
use App\Controller\Admin\AdminToolTasks;
use App\Controller\Admin\AdminToolTasksOnShepherdsSumup;
use App\Controller\Admin\AdminToolUsersMessages;
use App\Controller\Admin\AdminToolUsersRegistration;
use App\Scheduler\ClientError;
use App\Service\Clustering;
use App\Utils\Misc;
use Da\QrCode\QrCode;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension {
    public function getFunctions(): array {
        return [
            new TwigFunction('printAvatar', [HTML::class, 'printAvatar']),
            new TwigFunction('printOwnerAvatarFooter', [HTML::class, 'printOwnerAvatarFooter']),
            new TwigFunction('printProjectLink', [HTML::class, 'printProjectLink']),
            new TwigFunction('printProjectStatus', [HTML::class, 'printProjectStatus']),
            new TwigFunction('printPageError', [HTML::class, 'printPageError']),
            new TwigFunction('printProjectStatus', [HTML::class, 'printProjectStatus']),
            new TwigFunction('printProjectLink', [HTML::class, 'printProjectLink']),
            new TwigFunction('printFaqItem', [HTML::class, 'printFaqItem']),
            new TwigFunction('printFaqItems', [HTML::class, 'printFaqItems']),
            new TwigFunction('printOwnerLink', [HTML::class, 'printOwnerLink']),
            new TwigFunction('retUserProfileLink', [HTML::class, 'retUserProfileLink']),
            new TwigFunction('printOwnerAvatar', [HTML::class, 'printOwnerAvatar']),
            new TwigFunction('printOwnerAvatarFooter', [HTML::class, 'printOwnerAvatarFooter']),
            new TwigFunction('printOrangeHeader', [HTML::class, 'printOrangeHeader']),
            new TwigFunction('printInfo', [HTML::class, 'printInfo']),
            new TwigFunction('displayGoogleAdsense', [HTML::class, 'displayGoogleAdsense']),
            new TwigFunction('printBlend', [HTML::class, 'printBlend']),
            new TwigFunction('printProjectTRHeader', [HTML::class, 'printProjectTRHeader']),
            new TwigFunction('printProjectTR', [HTML::class, 'printProjectTR']),
            new TwigFunction('formAddProject', [HTML::class, 'formAddProject']),
            new TwigFunction('echoTD', [HTML::class, 'echoTD']),
            new TwigFunction('humanStatus', [HTML::class, 'humanStatus']),
            new TwigFunction('status2CSSstyle', [HTML::class, 'status2CSSstyle']),
            new TwigFunction('printMessageLeft', [HTML::class, 'printMessageLeft']),
            new TwigFunction('printMessageRight', [HTML::class, 'printMessageRight']),
            new TwigFunction('printBlueBox', [HTML::class, 'printBlueBox']),
            new TwigFunction('printIndexFeature', [HTML::class, 'printIndexFeature']),
            new TwigFunction('printNews', [HTML::class, 'printNews']),
            new TwigFunction('printBreadcrumbs', [HTML::class, 'printBreadcrumbs']),
            new TwigFunction('printError', [HTML::class, 'printError']),
            new TwigFunction('getTimeWithLocalTimeZone', [HTML::class, 'getTimeWithLocalTimeZone']),
            new TwigFunction('UIProgression', [HTML::class, 'UIProgression']),
            new TwigFunction('UIShowFrames', [HTML::class, 'UIShowFrames']),
            new TwigFunction('UIShowTiles', [HTML::class, 'UIShowTiles']),
            new TwigFunction('printHomeFeature2', [HTML::class, 'printHomeFeature2']),
            new TwigFunction('homeDisplayStatBox', [HTML::class, 'homeDisplayStatBox']),
            new TwigFunction('printCreateProject', [HTML::class, 'printCreateProject']),
            new TwigFunction('printRank', [HTML::class, 'printRank']),
            new TwigFunction('printRank', [HTML::class, 'printRank']),
            new TwigFunction('number_format_html', [$this, 'numberFormat']),
            new TwigFunction('humanSize_html', [Misc::class, 'humanSize']),
            new TwigFunction('humanTimeHtml', [Misc::class, 'humanTime']),
            new TwigFunction('printQRCode', [$this, 'printQRCode']),
            new TwigFunction('printArray', [Misc::class, 'printArray']),
            new TwigFunction('ClientError_toHuman', [ClientError::class, 'toHuman']),
            new TwigFunction('Clustering_toHuman', [Clustering::class, 'toHuman']),
            new TwigFunction('Misc_stringLimit', [Misc::class, 'stringLimit']),
            new TwigFunction('printGiveAward', [HTML::class, 'printGiveAward']),
            new TwigFunction('printChangeAcl', [HTML::class, 'printChangeAcl']),
            new TwigFunction('printBlockOwners', [HTML::class, 'printBlockOwners']),
            new TwigFunction('printBlockRenderers', [HTML::class, 'printBlockRenderers']),
            new TwigFunction('printSponsor', [HTML::class, 'printSponsor']),
            new TwigFunction('printSharedKeys', [HTML::class, 'printSharedKeys']),
            new TwigFunction('printChangeScheduler', [HTML::class, 'printChangeScheduler']),
            new TwigFunction('printChangeNotifications', [HTML::class, 'printChangeNotifications']),
            new TwigFunction('printGifts', [HTML::class, 'printGifts']),
            new TwigFunction('printAwards', [HTML::class, 'printAwards']),
            new TwigFunction('printAffiliateLink', [HTML::class, 'printAffiliateLink']),
            new TwigFunction('printMyEmail', [HTML::class, 'printMyEmail']),
            new TwigFunction('printChangeSocialNetworks', [HTML::class, 'printChangeSocialNetworks']),
            new TwigFunction('printChangeWebsite', [HTML::class, 'printChangeWebsite']),
            new TwigFunction('printChangeEmail', [HTML::class, 'printChangeEmail']),
            new TwigFunction('printLinkRemoveAccount', [HTML::class, 'printLinkRemoveAccount']),
            new TwigFunction('printChangePassword', [HTML::class, 'printChangePassword']),
            new TwigFunction('printPointsChart', [HTML::class, 'printPointsChart']),
            new TwigFunction('printSessions', [HTML::class, 'printSessions']),
            new TwigFunction('printLastFrames', [HTML::class, 'printLastFrames']),
            new TwigFunction('printPastProjects', [HTML::class, 'printPastProjects']),
            new TwigFunction('printProjects', [HTML::class, 'printProjects']),
            new TwigFunction('printChangeAvatar', [HTML::class, 'printChangeAvatar']),
            new TwigFunction('printRenderingRight', [HTML::class, 'printRenderingRight']),
            new TwigFunction('printAddProjectRight', [HTML::class, 'printAddProjectRight']),
            new TwigFunction('printPrivateMessages', [HTML::class, 'printPrivateMessages']),
            new TwigFunction('printSummary', [HTML::class, 'printSummary']),
            new TwigFunction('printSocialNetworks', [HTML::class, 'printSocialNetworks']),
            new TwigFunction('printUserAvatar', [HTML::class, 'printUserAvatar']),
            new TwigFunction('printLoginHead', [HTML::class, 'printLoginHead']),
            new TwigFunction('printResetPassword', [HTML::class, 'printResetPassword']),
            new TwigFunction('printLogin', [HTML::class, 'printLogin']),
            new TwigFunction('printMachines', [HTML::class, 'printMachines']),
            new TwigFunction('printAdventCalendar', [HTML::class, 'printAdventCalendar']),
            new TwigFunction('displayDashboardStats', [HTML::class, 'displayDashboardStats']),

            new TwigFunction('fulladmin_tool_advent_calendar', [AdminToolAdventCalendar::class, 'show']),
            new TwigFunction('fulladmin_tool_awards', [AdminToolAwards::class, 'show']),
            new TwigFunction('fulladmin_tool_config', [AdminToolConfig::class, 'show']),
            new TwigFunction('fulladmin_tool_efficiency', [AdminToolEfficiency::class, 'show']),
            new TwigFunction('fulladmin_tool_errors', [AdminToolErrors::class, 'show']),
            new TwigFunction('fulladmin_tool_event_prediction', [AdminToolCommunityEvents::class, 'show']),
            new TwigFunction('fulladmin_tool_frames', [AdminToolFrames::class, 'show']),
            new TwigFunction('fulladmin_tool_gifts', [AdminToolGifts::class, 'show']),
            new TwigFunction('fulladmin_tool_host', [AdminToolHostStats::class, 'show']),
            new TwigFunction('fulladmin_tool_hwid', [AdminToolHWID::class, 'show']),
            new TwigFunction('fulladmin_tool_log', [AdminToolLog::class, 'show']),
            new TwigFunction('fulladmin_tool_mirrors', [AdminToolServers::class, 'show']),
            new TwigFunction('fulladmin_tool_misc_chart', [AdminToolMisc::class, 'show']),
            new TwigFunction('fulladmin_tool_persistentcache', [AdminToolCache::class, 'show']),
            new TwigFunction('fulladmin_tool_projects', [AdminToolProjects::class, 'show']),
            new TwigFunction('fulladmin_tool_responsetime', [AdminToolResponseTime::class, 'show']),
            new TwigFunction('fulladmin_tool_shepherds_health', [AdminToolShepherdsHealth::class, 'show']),
            new TwigFunction('fulladmin_tool_tasks', [AdminToolTasks::class, 'show']),
            new TwigFunction('fulladmin_tool_tasks_shepherd', [AdminToolTasksOnShepherdsSumup::class, 'show']),
            new TwigFunction('fulladmin_tool_users_messages', [AdminToolUsersMessages::class, 'show']),
            new TwigFunction('fulladmin_tool_users_registration', [AdminToolUsersRegistration::class, 'show']),

        ];
    }

    public function numberFormat(int $i): string {
        return number_format($i);
    }

    public function printQRCode(string $id): string {
        return (new QrCode($id))->setSize(200)->writeDataUri();
    }
}