<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Base;

use App\Entity\User;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BaseController extends AbstractController {
    protected Main $main;
    protected HTML $html;
    protected UrlGeneratorInterface $router;
    protected array $config;
    protected EntityManagerInterface $entityManager;


    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, HTML $html) {
        $this->main = $main;
        $this->html = $html;
        $this->router = $router;
        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
        $this->html = $html;

        GlobalInject::setInject($this->main, $this->router, $this->entityManager, $configService);
    }

    protected function getUser(): ?User {
        // force cast

        /** @var ?User $user */
        $user = parent::getUser();
        return $user;
    }

    protected function render(string $view, array $parameters = [], ?Response $response = null): Response {
        // set the footer and header infos, they are common to all HTML response so no need to display the call on regular response

        $header_paramaters = [
            'site_name' => $this->config['site']['name'],
            'header_default_container' => false,
            'short_version' => $this->html->getShortVersion(),
            'header_pages' => $this->html->getHeaderPages(),
            'header_announcement' => $this->config['news']['announcement'],
            'user_logged' => $this->getUser(),
            'header_theme_current' => $this->html->isDarkTheme() ? 'dark' : 'light',
            'header_theme_switch' => $this->html->isDarkTheme() ? 'light' : 'dark',
            'header_adsense' => true,
            'header_title' => '',
        ];

        $footer_parameters = [
            'footer_projects' => $this->html->getFooterProjects(),
            'footer_renderers' => $this->html->getFooterRenderers(),
            'footer_copyright_year' => $this->html->getFooterCopyrightYear(),
            'footer_debug_usage' => $this->html->getFooterDebugUsage(),
            'footer_socialnetworks' => $this->config['site']['communities'],
            'footer_paypal_button' => $this->html->getFooterPaypalButton(),
        ];

        $all_parameters = array_merge($header_paramaters, $footer_parameters, $parameters);

        return parent::render($view, $all_parameters, $response);
    }
}