<?php

namespace App\Base;

use App\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

abstract class BaseUser extends Entity {
    #[ORM\Column(name: 'id', type: Types::STRING, length: 32, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'password', type: Types::STRING, length: 256, nullable: false)]
    protected string $password;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time = 0;

    #[ORM\Column(name: 'email', type: Types::STRING, length: 64, nullable: false)]
    protected string $email;

    #[ORM\Column(name: 'affiliate', type: Types::STRING, length: 32, nullable: false)]
    protected string $affiliate;

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getRegistrationTime(): int {
        return $this->time;
    }

    public function setRegistrationTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getAffiliate(): string {
        return $this->affiliate;
    }

    public function setAffiliate(string $affiliate): self {
        $this->affiliate = $affiliate;

        return $this;
    }
}