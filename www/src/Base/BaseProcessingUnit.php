<?php

namespace App\Base;

use App\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Processing Unit like CPU or GPU
 */
abstract class BaseProcessingUnit extends Entity {

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'model', type: Types::STRING, length: 255, nullable: false)]
    protected string $model;

    #[ORM\Column(name: 'rendered_frames', type: Types::INTEGER, nullable: false, options: ['default' => '0'])]
    protected int $rendered_frames = 0;

    #[ORM\Column(name: 'power', type: Types::INTEGER, nullable: false)]
    protected int $power = 0;


    abstract public function description(): string;

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getModel(): ?string {
        return $this->model;
    }

    public function setModel(string $model): self {
        $this->model = $model;

        return $this;
    }

    public function getRenderedFrames(): int {
        return $this->rendered_frames;
    }

    public function setRenderedFrames(?int $renderedFrames): self {
        $this->rendered_frames = $renderedFrames;

        return $this;
    }

    public function getPower(): int {
        return $this->power;
    }

    public function setPower(int $power): self {
        $this->power = $power;

        return $this;
    }

    /**
     * @return int percent of power example: 118 (for 118%)
     */
    abstract public function getPowerPercent(): int;

    abstract public function getPowerConst(): int;

    /**
     * a gpu is about 18x faster than a cpu, so it should have 18 time more points
     * on GPU it should return about 18.0 and on CPU 1.0
     */
    abstract public function getPowerRatioOverCPU(): float;
}