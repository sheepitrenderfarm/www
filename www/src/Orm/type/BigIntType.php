<?php

namespace App\Orm\type;

use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Type that maps a database BIGINT to a PHP int.
 */
class BigIntType extends \Doctrine\DBAL\Types\BigIntType {

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        // upstream will use string cast, but we do want native int
        // CAUTION: cast to int will only work on 64bits system.

        /** @phpstan-ignore-next-line */
        return null === $value ? null : (int)$value;
    }
}
