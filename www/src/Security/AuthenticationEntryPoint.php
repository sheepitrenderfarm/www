<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class AuthenticationEntryPoint implements AuthenticationEntryPointInterface {
    private UrlGeneratorInterface $router;

    public function __construct(UrlGeneratorInterface $router) {
        $this->router = $router;
    }

    public function start(Request $request, ?AuthenticationException $authException = null): RedirectResponse {
        return new RedirectResponse($this->router->generate('app_user_signin', ['redirect' => urlencode(urlencode($request->getRequestUri()))]));
    }
}
