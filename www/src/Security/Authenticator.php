<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;

class Authenticator extends AbstractAuthenticator {
    private EntityManagerInterface $entityManager;

    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function supports(Request $request): bool {
        // only check credentials on sign in route
        return $request->attributes->get('_route') == 'app_user_authenticate';
    }

    public function authenticate(Request $request): Passport {
        $user = $this->userRepository->find($request->request->get('login'));

        if (filter_var($request->request->get('login'), FILTER_VALIDATE_EMAIL)) {
            throw new CustomUserMessageAuthenticationException("You have enter a email address, you need to use your username.");
        }

        if (is_object($user) && $user->authenticate($request->request->get('password'))) {
            return new SelfValidatingPassport(new UserBadge($user->getId()), [new RememberMeBadge()]);
        }
        else {
            throw new CustomUserMessageAuthenticationException('Authentication failed');
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response {
        return new Response($exception->getMessage(), Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response {
        $response = new Response('OK');
        if ($request->request->has('timezone')) {
            $response->headers->setCookie(new Cookie('timezone', $request->request->get('timezone'), time() + 365 * 86400));
        }

        // update last_web_session
        /** @var User $user */
        $user = $token->getUser();
        $user->setLastWebSession(time());
        $user->setIPLogIn($request->getClientIp());
        $this->entityManager->flush();

        return $response;
    }

    public function supportsRememberMe(): bool {
        return true;
    }
}
