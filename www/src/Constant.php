<?php

namespace App;

class Constant {
    public const WAITING = 'waiting';
    public const PROCESSING = 'rendering';
    public const FINISHED = 'rendered';
    public const PAUSED = 'paused';
    public const WAITINGFORARCHIVECREATION = 'waitingforarchivecreation';

    public const DOWN = 'down';

    public const COMPUTE_CPU = 1;       // 2^0 bit mask
    // 2^1 bit mask  (old CUDA)
    // 2^2 bit mask  (old HIP)
    public const COMPUTE_GPU = 8;        // 2^3 bit mask  (old OPTIX)


    public const STATS_TOTAL = 'total';
    public const STATS_RENDERTIME_ACTUAL = 'rendertime_actual';
    public const STATS_RENDERTIME_REF = 'rendertime_ref';
    public const STATS_LAST_VALIDATION = 'last_validation';
    public const STATS_PREPTIME_ACTUAL = 'preptime_actual';

    public const PROJECT_ARCHIVE_MAX_SIZE = 2147483648; // in B
    public const PROJECT_ARCHIVE_LIMIT_BIG = 786432000; // in B, client can choose to not download project over this limit

    public const MAX_CHUNK_SIZE = 50000000.0;

    public const CLUSTER_LOW = 0;
    public const CLUSTER_MID = 1;
    public const CLUSTER_HIGH = 2;
    public const CLUSTER_UNKNOWN = 99;
}