<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Persistent;

class Cache {
    protected bool $filled;
    protected array $cache;

    public function __construct() {
        $this->filled = false;
        $this->cache = array();
    }

    public function isFilled(): bool {
        return $this->filled;
    }

    public function setFill(bool $val): void {
        $this->filled = $val;
    }

    public function get(string|int $key): mixed {
        if ($this->exists($key) == false) {
            return null;
        }
        else {
            return $this->cache[$key];
        }
    }

    public function getAll(): array {
        return $this->cache;
    }

    public function exists(string|int $key): bool {
        return array_key_exists($key, $this->cache);
    }

    public function add(mixed $value, string|int $key): void {
        $this->cache[$key] = $value;
    }

    public function set(array $array): void {
        $this->cache = $array;
    }

    public function remove(string|int $key): void {
        if ($this->exists($key)) {
            unset($this->cache[$key]);
        }
    }
}
