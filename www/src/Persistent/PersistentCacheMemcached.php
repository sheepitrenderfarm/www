<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Persistent;

use App\Service\Logger;
use Memcached;

class PersistentCacheMemcached implements PersistentCache {
    private ?Memcached $memcached;

    public function __construct(string $root_, int $port_) {
        $this->memcached = new Memcached();
        $this->memcached->addServer($root_, $port_);
    }

    public function get(string|int $key): mixed {
//         Logger::debug("PersistentCacheMemcached::get($key)");
        return $this->memcached->get($key);
    }

    public function getAllKeys(): array {
        return $this->memcached->getAllKeys();
    }

    public function set(string|int $key, mixed $value): mixed {
//         Logger::debug("PersistentCacheMemcached::set($key, ".json_encode($value).")");
        $ret = $this->memcached->set($key, $value);
        if ($ret == false) {
            Logger::error("PersistentCacheMemcached::set($key, ".json_encode($value).") failed error code: ".$this->memcached->getResultCode().' '.$this->memcached->getResultMessage());
        }
        return $value;
    }

    public function remove(string|int $key): void {
//         Logger::debug("PersistentCacheMemcached::remove($key)");
        $this->memcached->delete($key);
    }

    public function close(): void {
        $this->memcached->quit();
        $this->memcached = null;
    }

    public function lock(string|int $key): \NinjaMutex\Mutex {
        $lock = new \NinjaMutex\Lock\MemcachedLock($this->memcached);
        $mutex = new \NinjaMutex\Mutex('mutex_'.$key, $lock);
        $mutex->acquireLock(1000);
        return $mutex;
    }
}
