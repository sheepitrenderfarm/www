<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Persistent;

use NinjaMutex\Mutex;

interface PersistentCache {
    public function get(string|int $key): mixed;

    public function getAllKeys(): array;

    public function set(string|int $key, mixed $value): mixed;

    public function remove(string|int $key): void;

    public function close(): void;

    public function lock(string|int $key): Mutex;
}
