<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Persistent;

use App\Service\Logger;

class PersistentCacheFile implements PersistentCache {
    private string $root;

    public function __construct(string $root_) {
        $this->root = $root_;
    }

    public function get(string|int $key): mixed {
        $path = $this->root.base64_encode($key);
        return unserialize(@file_get_contents($path));
    }

    public function getAllKeys(): array {
        $ret = array();
        $files = glob($this->root."/*");
        foreach ($files as $f) {
            if (is_file($f)) {
                $ret [] = base64_decode(basename($f));
            }
        }
        return $ret;
    }

    public function set(string|int $key, mixed $value): mixed {
        $path = $this->root.base64_encode($key);
        $fp = @fopen($path, "w+");
        if (flock($fp, LOCK_EX)) {
            fwrite($fp, serialize($value));
            fflush($fp);
            flock($fp, LOCK_UN);
        }
        else {
            Logger::error("PersistentCacheFile::set($key, ...) could not lock file");
        }

        fclose($fp);
        @chmod($path, 0777);

        return $value;
    }

    public function remove(string|int $key): void {
        $path = $this->root.base64_encode($key);
        @unlink($path);
    }

    public function close(): void {
    }

    public function lock(string|int $key): \NinjaMutex\Mutex {
        $lock = new \NinjaMutex\Lock\FlockLock($this->root);
        $mutex = new \NinjaMutex\Mutex('mutex_'.base64_encode($key), $lock);
        $mutex->acquireLock(1000);
        return $mutex;
    }
}
