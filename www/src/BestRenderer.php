<?php

namespace App;

class BestRenderer {

    protected string $login;
    protected int $rendered_frames;
    protected int $render_time;
    protected float $points_earn;

    public function getLogin(): ?string {
        return $this->login;
    }

    public function setLogin(string $login): self {
        $this->login = $login;

        return $this;
    }

    public function getRenderedFrames(): int {
        return $this->rendered_frames;
    }

    public function setRenderedFrames(int $renderedFrames): self {
        $this->rendered_frames = $renderedFrames;

        return $this;
    }

    public function getRenderTime(): int {
        return $this->render_time;
    }

    public function setRenderTime(int $renderTime): self {
        $this->render_time = $renderTime;

        return $this;
    }

    public function getPointsEarn(): float {
        return $this->points_earn;
    }

    public function setPointsEarn(float $pointsEarn): self {
        $this->points_earn = $pointsEarn;

        return $this;
    }
}