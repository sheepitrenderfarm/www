<?php

namespace App\DependencyInjection;

use App\Service\ConfigService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class SheepItExtension extends Extension {
    public function load(array $configs, ContainerBuilder $container): void {
        $config = [];
        // let resources override the previous set value
        foreach ($configs as $subConfig) {
            $config = array_replace_recursive($config, $subConfig);
        }

        // a proper way is to process the config but the yaml struct needs to be defined first
        // for now, generate a massive array for a single parameter
        // $configuration = new Configuration();
        // $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter(ConfigService::ROOT, $config);
    }
}