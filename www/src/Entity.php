<?php

namespace App;

class Entity implements \Stringable {
    public function __toString(): string {
        $ret = get_class($this).'(';
        foreach (get_object_vars($this) as $k => $v) {
            $ret .= $k.': ';
            if (is_string($v) || is_numeric($v)) {
                $ret .= $v;
            }
            elseif (is_bool($v)) {
                $ret .= $v ? 'true' : 'false';
            }
            elseif ($v instanceof \DateTime) {
                $ret .= $v->format("Y-m-d");
            }
            elseif (is_null($v)) {
                $ret .= 'null';
            }
            elseif ($v instanceof Entity) {
                $ret .= get_class($v);
                if (method_exists($v, 'getId')) {
                    $ret .= '(id: '.$v->getId().')';
                }
            }
            elseif (is_object($v)) {
                $ret .= get_class($v);
            }
            else {
                $ret .= 'complex';
            }
            $ret .= ', ';
        }
        $ret .= ')';
        return $ret;
    }
}