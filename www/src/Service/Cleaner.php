<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Constant;
use App\Entity\BlenderArch;
use App\Entity\Gift;
use App\Entity\Liaison;
use App\Entity\PreUser;
use App\Entity\Project;
use App\Entity\ProjectAnalyse;
use App\Entity\Shepherd;
use App\Entity\Team;
use App\Entity\User;
use App\Repository\CDNRepository;
use App\Utils\File;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class Cleaner {
    private array $config;
    private EntityManagerInterface $entityManager;
    private CDNRepository $CDNRepository;

    public function __construct(EntityManagerInterface $entityManager, ConfigService $configService, CDNRepository $CDNRepository) {
        $this->entityManager = $entityManager;
        $this->config = $configService->getData();
        $this->CDNRepository = $CDNRepository;
    }

    public function removeOldProjects(): void {
        $min_time = time() - $this->config['storage']['duration']['project']['rendered'] * 3600 * 24;
        $min_time_empty = time() - $this->config['storage']['duration']['project']['empty'] * 3600 * 24;

        /** @var Project[] $all_projects */
        $all_projects = $this->entityManager->getRepository(Project::class)->findAll();
        foreach ($all_projects as $project) {
            if ($project->getId() > 1) {
                if ($project->getLastUpdateArchive() < $min_time) {
                    $status = $project->getStatus();
                    if ($status == Constant::FINISHED) {
                        // check if the last finished frame was sent more than xx days

                        $last = $project->getCacheLastValidation();
                        if ($last < $min_time) {
                            Logger::debug('deleting '.$project->getName().' (id:'.$project->getId().') (because too old)');
                            $this->entityManager->getRepository(Project::class)->removeDelay($project);
                        }
                    }
                }
            }
        }
    }

    public function removeOldPausedProjects(): void {
        $min_time = time() - $this->config['storage']['duration']['project']['paused'] * 3600 * 24;
        $all_projects = $this->entityManager->getRepository(Project::class)->findAll();
        foreach ($all_projects as $project) {
            if ($project->getId() > 10) {
                if ($project->getLastUpdateArchive() < $min_time) {
                    $status = $project->getStatus();
                    if ($status == Constant::PAUSED) {
                        Logger::debug('deleting '.$project->getName().' (id:'.$project->getId().') (because paused too old)');
                        $this->entityManager->getRepository(Project::class)->removeDelay($project);
                    }
                }
            }
        }
    }

    public function removeOrphanProjectFromShepherd(): void {
        foreach ($this->entityManager->getRepository(Shepherd::class)->findAll() as $shepherd) {
            $shepherd->removeOrphan();
        }
    }

    public function removeBlockedProjects(): void {
        $threadhold = time() - $this->config['storage']['duration']['project']['blocked'] * 24 * 3600;

        /** @var Project[] $projects */
        $projects = $this->entityManager->getRepository(Project::class)->findAll();
        foreach ($projects as $s) {
            if ($s->getId() < 2) {
                continue;
            }
            if ($s->isBlocked() == false) {
                continue;
            }

            $last = $s->getCacheLastValidation();
            if ($s->getLastupdatearchive() < $threadhold) {
                echo "$s should be deleted because no validation in ".$this->config['storage']['duration']['project']['blocked']." days\n";
                $this->entityManager->getRepository(Project::class)->remove($s);
            }
            elseif ($last < $threadhold) {
                Logger::debug("$s should be deleted ( diff ".((time() - $last) / (24 * 3600))." days) because last validation at least ".$this->config['storage']['duration']['project']['blocked']." days old");
                $this->entityManager->getRepository(Project::class)->remove($s);
            }
        }
    }

    public function removeOldTmpUploadProjectFiles(): void {
        $threadhold = time() - $this->config['storage']['duration']['tmp'] * 24 * 3600;

        $path_zip = $this->config['storage']['path'].'projects/tmp_*';

        $files = glob($path_zip);
        foreach ($files as $path) {
            if (is_file($path) == false || file_exists($path) == false) {
                continue;
            }

            if (filemtime($path) < $threadhold) {
                Logger::debug("$path should be remove (diff ".(int)((time() - filemtime($path)) / (24 * 3600))." days)");
                unlink($path);
            }
        }
    }

    public function removeOldTmpFiles(): void {
        $threadhold = time() - $this->config['storage']['duration']['tmp'] * 24 * 3600;

        $files = glob($this->config['tmp_dir'].'/*');
        foreach ($files as $path) {
            if (filemtime($path) < $threadhold) {
                Logger::debug("$path should be remove (diff ".(int)((time() - filemtime($path)) / (24 * 3600))." days)");
                if (is_file($path)) {
                    unlink($path);
                }
                else {
                    File::rmdirRecurse($path);
                }
            }
        }
    }

    public function removeOldProjectAnalyses(): void {
        $repo = $this->entityManager->getRepository(ProjectAnalyse::class);
        foreach ($repo->findBy(['date' => (new DateTime())->sub(DateInterval::createFromDateString('2 day'))]) as $o) {
            $repo->remove($o);
        }
    }

    public function removeOldPreusers(): void {
        $threadhold = time() - $this->config['storage']['duration']['pre-user'] * 3600 * 24;

        $preusers = $this->entityManager->getRepository(PreUser::class)->findAll();
        foreach ($preusers as $user) {
            if ($user->getRegistrationTime() < $threadhold) {
                $this->entityManager->getRepository(PreUser::class)->remove($user);
            }
        }
    }

    public function removeExpiredGifts(): void {
        $now = time();
        $gifts = $this->entityManager->getRepository(Gift::class)->findAll();
        foreach ($gifts as $gift) {
            if ($gift->getExpiration() < $now) {
                $this->entityManager->getRepository(Gift::class)->remove($gift);
            }
        }
    }

    public function removeOrphanLiaisons(): void {
        $liaisons = $this->entityManager->getRepository(Liaison::class)->getLiaisons(Liaison::USER_MANAGE_PROJECT);
        foreach ($liaisons as $l) {
            $u = $this->entityManager->getRepository(User::class)->find($l->getPrimary());
            if (is_object($u)) {
                $s = $this->entityManager->getRepository(Project::class)->find($l->getSecondary());
                if (is_object($s) == false) {
                    Logger::error("(cleanup_liaison_UserManageProject) project does not exist $l");
                    $this->entityManager->getRepository(Liaison::class)->remove($l);
                }
            }
            else {
                Logger::error("(cleanup_liaison_UserManageProject) user does not exist $l");
                $this->entityManager->getRepository(Liaison::class)->remove($l);
            }
        }

        $liaisons = $this->entityManager->getRepository(Liaison::class)->getLiaisons(Liaison::USER_RENDER_PROJECT);
        foreach ($liaisons as $l) {
            $u = $this->entityManager->getRepository(User::class)->find($l->getPrimary());
            if (is_object($u)) {
                $s = $this->entityManager->getRepository(Project::class)->find($l->getSecondary());
                if (is_object($s) == false) {
                    Logger::error("(cleanup_liaison_UserRenderProject) project does not exist $l");
                    $this->entityManager->getRepository(Liaison::class)->remove($l);
                }
            }
            else {
                Logger::error("(cleanup_liaison_UserRenderProject) user does not exist $l");
                $this->entityManager->getRepository(Liaison::class)->remove($l);
            }
        }
    }

    public function removeOrphanTeams(): void {
        foreach ($this->entityManager->getRepository(Team::class)->findAll() as $team) {
            $members = $team->members();
            if (count($members) == 0) {
                $this->entityManager->getRepository(Team::class)->remove($team);
                $this->entityManager->flush();
            }
        }
    }

    public function removeOrphanCDNElements(): void {
        $projectRepository = $this->entityManager->getRepository(Project::class);
        $blenderArchRepository = $this->entityManager->getRepository(BlenderArch::class);
        $filenames = array_map(function ($blenderArch) {
            return $blenderArch->getPath();
        }, $blenderArchRepository->findAll());

        foreach ($this->CDNRepository->findAll() as $cdn) {
            foreach ($cdn->listAll() as $chunk) {
                // chunks look like:
                // 1_1ebeaa3e8e00e03b1cd14a7fdff17ae6 <= project
                // 1ebeaa3e84335e03b1cd14a7fdff17ae <= binary
                // blender4.1.1_linux_64bit.zip <= binary

                $parts = explode('_', $chunk, 2);
                if (count($parts) == 2) {
                    if (is_null($projectRepository->findFromChunk($parts[1])) && in_array($chunk, $filenames) == false) {
                        $cdn->removeFile($chunk);
                    }
                }
                elseif (is_null($blenderArchRepository->findFromChunk($chunk))) {
                    $cdn->removeFile($chunk);
                }
            }
        }
    }
}