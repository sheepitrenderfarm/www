<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Base\BaseProcessingUnit;
use App\Utils\Misc;
use Exception;

class DiscordWebHookProd extends DiscordWebHook {
    private const GREEN = 387882;
    private const RED = 15746887;

    private array $config;
    private ?string $token_network = null;
    private ?string $token_shame = null;

    public function __construct(ConfigService $configService) {
        $this->config = $configService->getData();
        if (isset($this->config['discord']['notification']['webhook']['token_network'])) {
            $this->token_network = $this->config['discord']['notification']['webhook']['token_network'];
        }
        if (isset($this->config['discord']['notification']['webhook']['token_shame'])) {
            $this->token_shame = $this->config['discord']['notification']['webhook']['token_shame'];
        }
    }

    public function isEnabled(): bool {
        return is_string($this->token_network) || is_string($this->token_shame);
    }

    public function projectMirrorEnable(string $id, bool $enable, string $author): void {
        $this->request(
            "Network alert",
            $enable ? "A project mirror is enabled" : ":x: A project mirror has been disabled <:this_is_fine:1059607419342311464>",
            '['.MISC::humanServerHost($id).']('.$id.')',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_network
        );
    }

    public function binaryMirrorEnable(string $id, bool $enable, string $author): void {
        $this->request(
            "Network alert",
            $enable ? "A binary mirror is enabled" : ":x: A binary mirror has been disabled <:this_is_fine:1059607419342311464>",
            '['.MISC::humanServerHost($id).']('.$id.')',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_network
        );
    }

    public function shepherdEnable(string $id, bool $enable, string $author): void {
        $this->request(
            "Network alert",
            $enable ? "A shepherd is enabled" : ":x: A shepherd has been disabled <:this_is_fine:1059607419342311464>",
            '['.MISC::humanServerHost($id).']('.$id.')',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_network
        );
    }

    public function shepherdPause(string $id, bool $enable, string $author): void {
        $this->request(
            "Network alert",
            $enable == false ? ":x: Shepherd - PAUSED <:this_is_fine:1059607419342311464>" : "Shepherd - RESUMED",
            '['.MISC::humanServerHost($id).']('.$id.')',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_network);
    }

    public function userRightToRender(string $id, bool $enable, string $author): void {
        $this->request(
            "User right update",
            $enable ? "'".$id.'\' is allowed to render' : "'".$id.'\' is forbid to render',
            $this->config['site']['url'].'/user/'.$id.'/profile',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_shame);
    }

    public function userRightAddProject(string $id, bool $enable, string $author): void {
        $this->request(
            "User right update",
            $enable ? "'".$id.'\' is allowed to add project' : "'".$id.'\' is forbid to add project',
            $this->config['site']['url'].'/user/'.$id.'/profile',
            $enable ? DiscordWebHookProd::GREEN : DiscordWebHookProd::RED,
            $author,
            null,
            $this->token_shame);
    }

    public function userMessage(string $author, string $destination, string $url, string $message): void {
        $this->request(
            "User message",
            Misc::stringLimit($message, 252),
            'From: '.$author.' to: '.$destination."\n".$url,
            DiscordWebHookProd::GREEN,
            null,
            null,
            $this->token_shame);
    }

    public function shepherdTaskFailed(string $id, string $type, string $project, string $logs): void {
        $this->request(
            "Task failed on Shepherd",
            'Task type '.$type.' failed on '.$id,
            'Task '.$type.' failed for '.$project,
            DiscordWebHookProd::RED,
            null,
            $logs,
            $this->token_network
        );
    }

    public function sessionPowerDetectionHack(string $user, string $url, int $from_user, BaseProcessingUnit $device): void {
        // the feature doesn't work, the diff between sql and user is really strange.
        // (the call to discord works)
//        $this->request(
//            "Bad value on power detection",
//            sprintf("A session from %s has a suspicious value on power detection.\nOn database: %d %%\nFrom user %d %%\nDevice: %s", $user, $device->getPowerPercent(), $from_user, $device->description()),
//            $url,
//            DiscordWebHookProd::red,
//            null,
//            $this->token_shame
//        );
    }

    private function request(string $type, string $tile, string $description, int $color, ?string $author, ?string $logs, string $token): void {
        if ($this->isEnabled() == false) {
            Logger::info(' discord webhook not enabled');
            return;
        }

        $data = [
            [
                "title" => $tile,
                "description" => $description,
                "color" => $color,
            ],
        ];

        if (is_string($author)) {
            $data["fields"] = [
                [
                    "name" => "Action by",
                    "value" => $author,
                    "inline" => true,
                ],
            ];
        }

        $json = [
            "username" => $type,
            "avatar_url" => $this->config['site']['url']."/media/image/title.png",
            "embeds" => $data,
        ];

        if (is_string($logs) && $logs != '') {
            $json["content"] = "```\n".$logs."\n```";
        }

        try {
            (new \GuzzleHttp\Client())->post("https://discordapp.com/api/webhooks/".$token, ['json' => $json]);
        }
        catch (Exception $e) {
            Logger::error(__METHOD__." $e");
        }
    }
}
