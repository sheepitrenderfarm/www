<?php
/**
 * This file was taken from sheepit's shepherd repo
 */

namespace App\Service\NetworkUsage;

class MonitoringNetwork {
    private string $type;

    /** @var array{'rx': float, 'tx': float, 'time': int} */
    private array $previous;

    /** @var array{'rx': float, 'tx': float, 'time': int} */
    private array $current;

    /**
     * @param array{'rx': float, 'tx': float, 'time': int} $previousNetworkUsage
     * @param array{'rx': float, 'tx': float, 'time': int} $currentNetworkUsage
     */
    public function __construct(string $type, array $previousNetworkUsage, array $currentNetworkUsage) {
        $this->type = $type;
        $this->previous = $previousNetworkUsage;
        $this->current = $currentNetworkUsage;
    }

    public function getValue(): float {
        if ($this->previous['time'] != $this->current['time']) {
            return (float)($this->current[$this->type] - $this->previous[$this->type]) / (float)($this->current['time'] - $this->previous['time']);
        }
        else {
            return 0.0;
        }
    }
}
