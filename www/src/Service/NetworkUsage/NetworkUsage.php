<?php
/**
 * This file was taken from sheepit's shepherd repo
 */

namespace App\Service\NetworkUsage;

use App\Service\GlobalInject;

class NetworkUsage {
    private string $key;

    public function __construct(string $key) {
        $this->key = 'network_usage_'.$key;
    }

    public function generate(): array {
        $interface = null;

        foreach (glob("/sys/class/net/*") as $anInterface) {
            if (basename($anInterface) != 'lo') {
                $interface = basename($anInterface);
                break;
            }
        }

        $rx = 0;
        $tx = 0;

        if (is_string($interface)) {
            $rx = @file_get_contents("/sys/class/net/$interface/statistics/rx_bytes");
            $tx = @file_get_contents("/sys/class/net/$interface/statistics/tx_bytes");
        }

        $data = array('tx' => (float)$tx, 'rx' => (float)$rx, 'time' => time());

        GlobalInject::getMain()->getPersistentCache()->set($this->key, $data);

        return $data;
    }

    public function get(): array {
        $data = GlobalInject::getMain()->getPersistentCache()->get($this->key);

        if (is_array($data) == false) {
            $data = $this->generate();
        }

        return $data;
    }
}
