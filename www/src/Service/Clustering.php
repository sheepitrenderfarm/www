<?php

namespace App\Service;

class Clustering {

    public static function toHuman(int $cluster): string {
        return match ($cluster) {
            0 => 'LOW',
            1 => 'MID',
            2 => 'HIGH',
            default => 'UNKNOWN_'.$cluster,
        };
    }

    public function getSameSizeClusters(array $objects, string $value_method, int $k = 3): array {
        if (count($objects) < $k) {
            // not enough data
            // one big cluster with everything, the other are empty
            $clusters = [];

            $clusters[] = $objects;
            for ($i = 1; $i < $k; $i++) {
                $clusters [] = [];
            }
            return $clusters;
        }

        usort($objects, function (object $a, object $b) use ($value_method) {
            return ($a->$value_method() ?: PHP_INT_MAX) <=> ($b->$value_method() ?: PHP_INT_MAX);
        });

        return array_chunk($objects, (int)ceil(count($objects) / 3));

    }
}