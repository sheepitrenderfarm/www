<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Base\BaseProcessingUnit;

abstract class DiscordWebHook {

    abstract public function projectMirrorEnable(string $id, bool $enable, string $author): void;

    abstract public function binaryMirrorEnable(string $id, bool $enable, string $author): void;

    abstract public function shepherdEnable(string $id, bool $enable, string $author): void;

    abstract public function shepherdPause(string $id, bool $enable, string $author): void;

    abstract public function shepherdTaskFailed(string $id, string $type, string $project, string $logs): void;

    abstract public function userRightToRender(string $id, bool $enable, string $author): void;

    abstract public function userRightAddProject(string $id, bool $enable, string $author): void;

    abstract public function userMessage(string $author, string $destination, string $url, string $message): void;

    abstract public function sessionPowerDetectionHack(string $user, string $url, int $from_user, BaseProcessingUnit $device): void;
}
