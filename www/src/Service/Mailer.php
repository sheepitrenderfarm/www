<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use MooglePost\Client;
use MooglePost\ClientException;
use MooglePost\Email;

class Mailer {
    private const MAX_RECIPIENTS = 50;

    /** @var string[] $to */
    private array $to;
    private string $subject;
    private string $message;
    /** @var string[] $attachments */
    private array $attachments;
    private string $template;

    /**
     * @param array<int, string>|string $to_
     * @param ?array<int, string> $attachments_
     */
    public function __construct(array|string $to_, string $subject_, string $message_, ?array $attachments_, string $template_ = 'sheepit') {
        if (is_string($to_)) {
            $this->to = [$to_];
        }
        else {
            $this->to = $to_;
        }
        $this->subject = $subject_;
        $this->message = $message_;
        if (is_array($attachments_)) {
            $this->attachments = $attachments_;
        }
        else {
            $this->attachments = array();
        }
        $this->template = $template_;
    }

    public function send(): bool {
        $config = GlobalInject::getConfigService()->getData();

        if ($config['email']['enable'] == false) {
            Logger::debug('Mailer::send, email is disabled');
            return true;
        }

        $footer = '';
        if (isset($config['email']['footer']['all'])) {
            $footer .= '<div id="footer">';
            $footer .= '---<br>';
            $footer .= $config['email']['footer']['all'];
            $footer .= '</div>';
        }

        $html_content = '
            <html>
                <head>
                    <title>'.$this->subject.'</title>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <style>
                        '.file_get_contents(__DIR__.'/../../public/media/dev/style/base-main.css').'
                        '.file_get_contents(__DIR__.'/../../public/media/dev/style/base-light.css').'
                    </style>
                </head>
                <body>
                    <div id="mainWrap">
                        <table width="600" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="3"><img src="cid:emailer_top.png" alt="top" width="600" height="124" />
                                </td>
                            </tr>
                            <tr>
                                <td background="cid:emailer_left.png" width="20">
                                </td>
                                <td width="560" style="padding:0 10 0 10px" bgcolor="#ffffff">
                                    <font face="Verdana, Geneva, sans-serif" size="2">
                                        <p>'.$this->message.'</p>
                                        <p>'.$footer.'</p>
                                    </font>
                                </td>
                                <td background="cid:emailer_right.png" width="20" style="background-position: 499px 0; background-position-x: 500px">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"><img src="cid:emailer_bottom.png" alt="bottom" width="600" height="14" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </body>
            </html>
        ';

        $mooglePostClient = new Client($config['email']['mooglepost_key']);

        foreach (array_chunk($this->to, self::MAX_RECIPIENTS) as $recipients) {
            $mail = new Email();
            foreach ($recipients as $recipient) {
                $mail->addRecipient($recipient);
            }
            $mail->setSubject($this->subject);
            $mail->setText('');
            $mail->setHtml($html_content);
            $mail->setTemplateName($this->template);

            $mail->addEmbeddedImage(__DIR__.'/../../public/media/image/email/emailer_top.png', 'emailer_top.png');
            $mail->addEmbeddedImage(__DIR__.'/../../public/media/image/email/emailer_bottom.png', 'emailer_bottom.png');
            $mail->addEmbeddedImage(__DIR__.'/../../public/media/image/email/emailer_right.png', 'emailer_right.png');
            $mail->addEmbeddedImage(__DIR__.'/../../public/media/image/email/emailer_left.png', 'emailer_left.png');
            foreach ($this->attachments as $file) {
                $mail->addEmbeddedImage($file, basename($file));
            }

            try {
                $ret = $mooglePostClient->sendEmail($mail);
                if ($ret) {
                    GlobalInject::getMain()->getPersistentCache()->set('last_successfully_email', time());
                }
                return $ret;
            }
            catch (ClientException $e) {
                foreach ($e->getRecipients() as $error) {
                    if ($error['code'] != 1000) {
                        Logger::error(__METHOD__.' failed to send email to '.$error['email'].' code: '.$error['code'].' message: '.$error['message']);
                    }
                }
                return false;
            }
        }
        return true;
    }
}
