<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

class Logger {
    public static function append(string $data_, string $level_ = 'info'): void {
        $config = GlobalInject::getConfigService()->getData();

        if ($level_ == 'debug' && (isset($config['log']['debug']) && $config['log']['debug'] == true) == false) {
            return;
        }

        $addr = @$_SERVER['REMOTE_ADDR'] ?? '';

        while (strlen($addr) < strlen('255.255.255.255')) { // to have nice log
            $addr .= ' ';
        }

        @file_put_contents(Logger::file(), @date('M j H:i:s').' - '.$addr.' - '.strtoupper($level_).' - '.$data_."\r\n", FILE_APPEND);
        if (isset($config['log']['stdout']) && $config['log']['stdout'] === true) {
            echo '('.strtoupper($level_).') '.$data_."\n";
        }
    }

    public static function file(): string {
        $config = GlobalInject::getConfigService()->getData();

        $module_ = 'ferme';
        return $config['log']['dir'].'/'.strtolower($module_).'.log';
    }

    public static function debug(string $data_): void {
        Logger::append($data_, 'debug');
    }

    public static function info(string $data_): void {
        Logger::append($data_, 'info ');
    }

    public static function warning(string $data_): void {
        Logger::append($data_, 'warn ');
    }

    public static function error(string $data_): void {
        Logger::append($data_, 'error');
    }

    public static function critical(string $data_): void {
        Logger::append($data_, 'crit ');
    }

    /**
     * Log the backtrace call
     */
    public static function trace(string $message = ''): void {
        $e = new \Exception();
        $call = $e->getTrace()[1];
        self::debug($call['class'].':'.$call['function'].':'.$call['line'].' '.$message);
    }
}
