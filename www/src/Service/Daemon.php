<?php
/**
 * Copyright (C) 2007-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Constant;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\LockInterface;
use Symfony\Component\Lock\Store\FlockStore;

class Daemon {
    private array $config;
    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;
    private ?LockInterface $lockWorker = null;
    private LockFactory $lockFactory;

    public function __construct(EntityManagerInterface $entityManager, ConfigService $configService, TaskRepository $taskRepository) {
        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
        $this->config = $configService->getData();
        $store = new FlockStore($this->config['daemon']['lock_directory']);
        $this->lockFactory = new LockFactory($store);
    }

    public function canWork(): bool {
        $concurrent_tasks = (int)($this->config['daemon']['max_runner']);
        for ($i = 1; $i <= $concurrent_tasks; $i++) {
            $lock = $this->lockFactory->createLock("daemon.task.$i.pid");
            if ($lock->acquire(false)) {
                $this->lockWorker = $lock;
                return true;
            }
        }

        return false;
    }

    public function shutdown(): bool {
        if (is_null($this->lockWorker) == false) {
            $this->lockWorker->release();
        }
        return true;
    }

    public function addTask(Task $task_): void {
        Logger::debug("Daemon::addTask($task_)");
        $task_->setTime(time());
        $this->entityManager->persist($task_);
        $this->entityManager->flush($task_);
    }

    public function nextJob(): ?Task {
        // no need to sort the tasks since every task are independent
        $tasks = $this->taskRepository->findBy(["status" => Constant::WAITING]);
        if (count($tasks) > 0) {
            // all task are independents, can take one at random
            shuffle($tasks);
            $task = array_pop($tasks);
            $task->setStatus(Constant::PROCESSING);
            $this->entityManager->flush($task);
            return $task;
        }

        return null;
    }

    public function workOneTime(): bool {
        $job = $this->nextJob();
        if (is_object($job)) {
            $this->work($job);
            return true; // silent the error for $this->work()
        }
        return false;
    }

    public function work(Task $task): bool {
        $ret = $task->execute();
        if ($ret) {
        }
        else {
            Logger::debug('Daemon::work task failed '.$task.' ret: '.serialize($ret));
        }
        $this->taskRepository->remove($task);

        return $ret;
    }
}
