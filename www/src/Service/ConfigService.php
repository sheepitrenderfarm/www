<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConfigService {
    public const ROOT = 'sheep_it';
    /**
     * @var array<mixed, mixed>
     */
    private static ?array $cache = null;

    private ParameterBagInterface $parameterBag;

    /**
     * ConfigService constructor.
     */
    public function __construct(ParameterBagInterface $parameterBag) {
        $this->parameterBag = $parameterBag;
    }

    /**
     * Get contents of config
     * @return array<string, mixed>
     */
    public function getData(): array {
        if (is_null(self::$cache)) {
            self::$cache = $this->parameterBag->get(self::ROOT);
        }
        return self::$cache;
    }

    public function set(string $key1, string $key2, mixed $value): void {
        $this->getData(); // fill up cache
        if ($key2 == null) {
            self::$cache[$key1] = $value;
        }
        else {
            self::$cache[$key1][$key2] = $value;
        }
    }
}