<?php

namespace App\Service;

use App\BestRenderer;
use App\Constant;
use App\Entity\Event;
use App\Entity\Tile;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\StatsGlobal;
use App\Entity\StatsUser;
use App\Entity\User;
use App\Entity\WebSessionClient;
use App\Persistent\PersistentCache;
use App\Persistent\PersistentCacheFile;
use App\Persistent\PersistentCacheMemcached;
use App\Utils\Misc;
use App\Utils\Network;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/
class Main {
    private EntityManagerInterface $entityManager;
    private ConfigService $configService;
    protected ?PersistentCache $persistent_cache;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
        $this->configService = $configService;
        $this->persistent_cache = null;
    }

    public function getPersistentCache(): PersistentCache {
        $config = $this->configService->getData();

        if (is_null($this->persistent_cache)) {
            switch ($config['persistent_cache']['type']) {
                case 'memcached':
                    $this->persistent_cache = new PersistentCacheMemcached($config['persistent_cache']['path'], $config['persistent_cache']['port']);
                    break;
                default:
                    $this->persistent_cache = new PersistentCacheFile($config['persistent_cache']['path']);
                    break;
            }
        }
        return $this->persistent_cache;
    }

    public function getDiscordWebHook(): DiscordWebHook {
        $config = $this->configService->getData();

        if ($config['discordwebhook'] == 'prod') {
            return new DiscordWebHookProd($this->configService);
        }
        return new DiscordWebHookMock();
    }

    public function shutdownPersistentCache(): void {
        if (is_object($this->persistent_cache)) {
            $this->persistent_cache->close();
        }
        $this->persistent_cache = null;
    }

    public function importWebSessionClientFromCookie(): ?WebSessionClient {
        if (isset($_COOKIE['login']) && isset($_COOKIE['web-session'])) {
            return $this->entityManager->getRepository(WebSessionClient::class)->findOneBy(array('user' => $_COOKIE['login'], 'key' => $_COOKIE['web-session']));
        }
        return null;
    }

    public function countConnectedMachines(): int {
        return $this->entityManager->getRepository(Session::class)->count([]);
    }

    public function countPublicActiveProjects(bool $use_cache = true): int {
        $key = 'count_public_active_projects';

        $ret = 0;
        $set_cache = false;
        $get_actual_value = false;
        if ($use_cache) {
            $cached = $this->getPersistentCache()->get($key);
            if (is_int($cached)) {
                $ret = $cached;
            }
            else {
                $set_cache = true;
                $get_actual_value = true;
            }
        }
        else {
            $get_actual_value = true;
        }

        if ($get_actual_value) {
            $ret = $this->entityManager->getRepository(Project::class)->countPublicActiveProjects();
            $set_cache = true;
        }

        if ($set_cache) {
            $this->getPersistentCache()->set($key, $ret);
        }

        return $ret;
    }

    /**
     * @return array<int, array{id: int, name: string, owner: string, time: int}>
     */
    public function getLatestPublicProjects(): array {
        $data = @unserialize($this->getPersistentCache()->get('latest_public_projects'));
        if (is_array($data) == false) {
            // regenerate the cache
            $data = [];
            foreach ($this->entityManager->getRepository(Project::class)->findBy(['public_render' => 1], ['id' => 'DESC'], 3) as $project) {
                $data[$project->getId()] = array(
                    'id' => $project->getId(),
                    'name' => $project->getName(),
                    'owner' => $project->getOwner()->getId(),
                    'time' => $project->getLastUpdateArchive(),
                );
            }
            krsort($data);
            $this->getPersistentCache()->set('latest_public_projects', serialize($data));
        }
        return $data;
    }

    /**
     * @return array<int, array{id: int, name: string, owner: string, time: int}>
     */
    public function addLatestPublicProject(Project $project): array {
        $data = $this->getLatestPublicProjects();
        if ($project->getPublicRender()) {
            $data[$project->getId()] = array(
                'id' => $project->getId(),
                'name' => $project->getName(),
                'owner' => $project->getOwner()->getId(),
                'time' => $project->getLastUpdateArchive(),
            );

            krsort($data);
            if (count($data) > 3) {
                $data = array_slice($data, 0, 3, true);
            }

            $this->getPersistentCache()->set('latest_public_projects', serialize($data));
        }
        return $data;
    }

    /**
     * Get available shepherd, ordered by priority
     * @return Shepherd[]
     */
    public function bestShepherdServers(): array {
        // default country is US
        $country = 'US';

        $data = Network::getLocationFromIP(@$_SERVER['REMOTE_ADDR']);
        if (is_array($data)) {
            $country = $data['country'];
        }

        return $this->bestShepherdServersFromLocation($country);
    }

    /**
     * Get available shepherd, ordered by priority
     * @return Shepherd[]
     */
    public function bestShepherdServersFromLocation(string $country): array {
        $config = $this->configService->getData();

        $fallback = 'https://shepherd-fallback.sheepit-renderfarm.com';
        $shepherds = [];

        $repoTile = $this->entityManager->getRepository(Tile::class);
        $total_tiles = 1.0;
        $total_strength = 0.0;

        foreach ($this->entityManager->getRepository(Shepherd::class)->findAll() as $k => $s) {
            if ($s instanceof ShepherdInternal || $s->isEnabled() == false || $s->getTimeout() || $s->getAllowNewBlend() == false) {
                continue;
            }
            if ($s->getId() == $fallback) {
                continue;
            }
            if ($s->getCountryOnly() != '' && in_array($country, explode(',', $s->getCountryOnly())) == false) {
                continue;
            }
            if ($s->getCountryExclude() != '' && in_array($country, explode(',', $s->getCountryExclude()))) {
                continue;
            }

            if ($s->allowNewRenderingFrame() == false) {
                continue;
            }

            $stats = $s->getLastStats();
            if (is_object($stats)) {
                if ($s->getStorageMax() > 0 && max($stats->getDiskPrediction(), $stats->getDisk()) + $config['scheduler_shepherd']['disk_safety_net'] > $s->getStorageMax()) { // 80GB safety net
                    continue;
                }
                if ($stats->getTask() > $config['scheduler_shepherd']['max_task']) {
                    continue;
                }
                if ($stats->getCpuPressure() > $config['scheduler_shepherd']['max_cpu_pressure']) {
                    continue;
                }
            }

            $tiles_count = (float)$repoTile->countProcessingTileFor($s);
            $total_tiles += $tiles_count;
            $total_strength += (float)$s->getStrength();
            $shepherds[$s->getId()] = ['shepherd' => $s, 'strength' => (float)$s->getStrength(), 'tiles' => $tiles_count];
        }

        if (count($shepherds) == 0) {
            Logger::error(__METHOD__.' no shepherd available, give back (if enabled) the fallback');
            /** @var ?Shepherd $shepherd_fallback */
            $shepherd_fallback = $this->entityManager->getRepository(Shepherd::class)->find($fallback);
            if (is_object($shepherd_fallback) && $shepherd_fallback->isEnabled() && $shepherd_fallback->getTimeout() == false && $shepherd_fallback->getAllowNewBlend()) {
                return [$shepherd_fallback];
            }
        }

        if (count($shepherds) >= 2) {
            $last_projects = $this->getLatestPublicProjects();
            if (count($last_projects) > 0) {
                $last_project = $this->entityManager->getRepository(Project::class)->find(reset($last_projects)['id']);
                if (is_object($last_project) && array_key_exists($last_project->getShepherd()->getId(), $shepherds)) {
                    $shepherd_id = $last_project->getShepherd()->getId();
                    $total_strength -= $shepherds[$shepherd_id]['strength'];
                    $total_tiles -= $shepherds[$shepherd_id]['tiles'];
                    unset($shepherds[$shepherd_id]);
                }
            }
        }

        foreach ($shepherds as $shepherd_id => $data) {
            $shepherds[$shepherd_id]['strength_percent'] = $data['strength'] / $total_strength;
            $shepherds[$shepherd_id]['tiles_percent'] = $data['tiles'] / $total_tiles;
        }

        foreach ($shepherds as $shepherd_id => $data) {
            $shepherds[$shepherd_id]['score'] = $data['tiles_percent'] / $data['strength_percent'];
        }

        usort($shepherds, function (array $a, array $b) {
            return $a['score'] <=> $b['score'];
        });

        return array_map(function ($pair) {
            return $pair['shepherd'];
        }, $shepherds);
    }

    /**
     * @return Tile[]
     */
    public function frames(?string $status_ = null): array {
        if (is_null($status_)) {
            return $this->entityManager->getRepository(Tile::class)->findAll();
        }
        else {
            return $this->entityManager->getRepository(Tile::class)->findBy(array('status' => $status_));
        }
    }

    public function getCountRenderedFramesSinceLast12hoursFromCache(): int {
        $key = 'count_rendered_frame_last_12hours';
        $data = $this->getPersistentCache()->get($key);
        if ($data === false) {
            $data = $this->entityManager->getRepository(Tile::class)->getCountRenderedFramesSinceLast12hours();
            $this->getPersistentCache()->set($key, $data);
        }
        return $data;
    }

    /**
     * @return StatsGlobal[]
     */
    public function globalStats(int $days = 0): array {
        if ($days == 0) {
            return $this->entityManager->getRepository(StatsGlobal::class)->findBy([], ['id' => 'ASC']);
        }
        else {
            return array_reverse($this->entityManager->getRepository(StatsGlobal::class)->findBy([], ['id' => 'DESC'], (int)($days * 24)));
        }
    }

    public function removeIdleSession(): void {
        $config = $this->configService->getData();
        $now = time();
        $sessions = $this->entityManager->getRepository(Session::class)->findAll();
        foreach ($sessions as $session) {
            if ($now - $session->getTimestamp() > $config['session']['timeout']) {
                Logger::debug("remove $session due to timeout (".Misc::humanTime($now - $session->getTimestamp(), false).' > '.Misc::humanTime($config['session']['timeout'], false).')');
                // the session must be removed
                $this->entityManager->getRepository(Session::class)->remove($session, Event::TYPE_SESSIONCLEAR);
            }
        }

        $now = time();
        $frames = $this->entityManager->getRepository(Tile::class)->findBy(array('status' => Constant::PROCESSING));
        foreach ($frames as $frame) {
            $duration = $now - $frame->getRequestTime();
            if ($duration > $config['session']['timeoutrender']) {
                Logger::debug("reset $frame due to timeout (".Misc::humanTime($duration, false).' > '.Misc::humanTime($config['session']['timeoutrender'], false).')');
                $e = new Event();
                $e->setType(Event::TYPE_FRAMERESET);
                $e->setSession($frame->getSession());
                $e->setTime(time());
                $e->setUser($frame->getUser()->getId());
                $e->setProject($frame->getFrame()->getProject()->getId());

                $this->entityManager->persist($e);
                $this->entityManager->flush($e);

                $frame->reset(true);
            }
        }

        // clean up WebSessionClient (auto-login of client)
        foreach ($this->entityManager->getRepository(WebSessionClient::class)->findExpirateBefore($now) as $session) {
            if ($session->getExpirationTime() != 0 && $session->getExpirationTime() < $now) {
                $this->entityManager->getRepository(WebSessionClient::class)->remove($session);
            }
        }
    }

    /**
     * @return BestRenderer[]
     */
    public function getRenderers(int $days = 30): array {
        $renderer = array();

        $max_render_time = 1;
        $max_rendered_frame = 1;

        $start_data = array();
        foreach ($this->entityManager->getRepository(StatsUser::class)->findBy(['date' => new \DateTime('-'.$days.' day')]) as $su) { // from start of period
            $start_data[$su->getUser()->getId()] = $su;
        }

        $rows = $this->entityManager->getRepository(StatsUser::class)->findBy(['date' => new \DateTime()]); // from today
        foreach ($rows as $su) {
            $o = new BestRenderer();
            $o->setLogin($su->getUser()->getId());
            $o->setRenderedFrames(0);
            $o->setRenderTime(0);
            if (array_key_exists($su->getUser()->getId(), $start_data)) {
                $o->setRenderedFrames($su->getRenderedFrames() - $start_data[$su->getUser()->getId()]->getRenderedFrames());
                $o->setRenderTime($su->getRenderTime() - $start_data[$su->getUser()->getId()]->getRenderTime());
                $o->setPointsEarn($su->getPointsEarn() - $start_data[$su->getUser()->getId()]->getPointsEarn());
            }
            else {
                // if the StatsUser does not exist it's because the user didn't have any rendered frame or was not registered at the time
                $o->setRenderedFrames($su->getRenderedFrames());
                $o->setRenderTime($su->getRenderTime());
                $o->setPointsEarn($su->getPointsEarn());
            }

            if ($o->getRenderedFrames() > 0) {
                if ($su->getUser()->getId() != User::REMOVED_USER_ID) {
                    $renderer[$su->getUser()->getId()] = $o;

                    if ($o->getRenderedFrames() > $max_rendered_frame) {
                        $max_rendered_frame = $o->getRenderedFrames();
                    }
                    if ($o->getRenderTime() > $max_render_time) {
                        $max_render_time = $o->getRenderTime();
                    }
                }
            }
        }

        usort($renderer, '\App\Utils\Sort::sortByAttributePointsEarn');
        return $renderer;
    }

    /**
     * @return BestRenderer[]
     */
    public function getBestRenderersFromCache(): array {
        $cache = $this->getPersistentCache()->get('best_renderer');
        $data = @unserialize($cache);
        if (is_array($data) == false) {
            $data = $this->getRenderers();
            $this->getPersistentCache()->set('best_renderer', serialize($data));
        }
        return $data;
    }
}
