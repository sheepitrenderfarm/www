<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use Exception;
use Imagick;

class Image {
    protected ?string $path;
    protected ?Imagick $resource;

    protected ?string $path_tga; // dirty hack because Imagick does not recognize a TARGA file if it does not have a tga extension

    protected const FORMAT2EXTENSION = array(
        'bmp2' => 'bmp',
        'bmp3' => 'bmp',
        'jpeg' => 'jpg',
        'png24' => 'png',
        'png32' => 'png',
        'png8' => 'png',
    );

    public function __construct(?string $path_) {
        $this->path = $path_;
        $this->path_tga = null;
        $this->resource = null;
    }

    protected function open(): bool {
        if (is_file($this->path) == false) {
            return false;
        }

        try {
            $mypicture = new Imagick();
            $mypicture->readImage($this->path);
            $this->resource = $mypicture;

            return true;
        }
        catch (Exception $e) {
            // dirty hack because Imagick does not recognize a TARGA file if it does not have a tga extension
            $unique_name = tempnam(sys_get_temp_dir(), 'TGA');
            $tmp_file = $unique_name.'.tga';
            copy($this->path, $tmp_file);
            try {
                $mypicture = new Imagick();
                $mypicture->readImage($tmp_file);
                $this->resource = $mypicture;
                $this->path_tga = $tmp_file;
                @unlink($unique_name);
                return true;
            }
            catch (Exception $e) {
                Logger::debug("Image::open2 exception original path '".$this->path."' $e");
                @unlink($tmp_file);
                @unlink($unique_name);
            }
            return false;
        }
    }

    protected function close(): bool {
        $this->resource = null;
        if (is_null($this->path_tga) == false) {
            @unlink($this->path_tga);
        }
        return true;
    }

    public function isImage(): bool {
        $ret = $this->open();

        $this->close();

        return $ret;
    }

    public function generateThumbnail(int $width_, int $height_, string $output_file): bool {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $this->resource->scaleImage($width_, $height_);
                $this->resource->writeImage($output_file);
                $ret = true;
            }
            catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    public function fileExtension(): bool|string {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = strtolower($this->resource->getImageFormat());
            }
            catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        if (array_key_exists($ret, Image::FORMAT2EXTENSION)) {
            $ret = Image::FORMAT2EXTENSION[$ret];
        }

        return $ret;
    }

    public function mimeType(): bool|string {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = $this->resource->getImageFormat();
            }
            catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    /**
     * @return array{width: int, height: int}|bool
     */
    public function getGeometry(): array|bool {
        $ret = false;
        $isImage = $this->open();

        if ($isImage == true) {
            try {
                $ret = $this->resource->getImageGeometry();
            }
            catch (Exception $e) {
                $ret = false;
            }
        }

        $this->close();

        return $ret;
    }

    public function isFullyBlack(): bool {
        if ($this->isImage() == false) {
            Logger::error('Image::isFullyBlack not an image');
            return false;
        }

        $cmd = 'convert '.$this->path.' -format "%[mean]" info:';
        exec($cmd.' 2>&1', $output);

        if (count($output) == 1 && $output[0] == '0') {
            return true;
        }

        return false;
    }

    public function compareTo(string $other, bool $scale = true): bool {
        try {
            if ($this->open() == false) {
                Logger::debug('Image::compareTo, failed to open file '.$this->path);
                return false;
            }

            $image2 = new Imagick();
            $image2->readImage($other);

            if ($scale) {
                // resize to the same dimension for better comparaison
                $this->resource->scaleImage(960, 540);
                $image2->scaleImage(960, 540);
            }

            // set the fuzz factor (must be done BEFORE reading in the images)
            $this->resource->SetOption('fuzz', '1%');

            // compare the images using METRIC=1 (Absolute Error)
            $result = $this->resource->compareImages($image2, 1);

            $this->close();

            return ($result[1] < 5); // 5 pixels difference
        }
        catch (Exception $e) {
            Logger::debug("Image::compareTo, excetion ".$e);
        }

        return false;
    }

    public function isSimilarTo(string $other): bool {
        try {
            if ($this->open() == false) {
                Logger::debug(__METHOD__.', failed to open file '.$this->path);
                return false;
            }

            $this_geometry = null;
            try {
                $this_geometry = $this->resource->getImageGeometry();
            }
            catch (Exception $e) {
                $this_geometry = null;
            }

            $other_img = new Image($other);
            $other_geometry = $other_img->getGeometry();
            if (is_array($other_geometry) && is_array($this_geometry)) {
                if ($this_geometry['height'] != $other_geometry['height'] && $this_geometry['width'] != $other_geometry['width']) {
                    return false;
                }
            }

            // resize to the same dimension for better comparaison
//             $this->resource->scaleImage(960, 540);
            $image2 = new Imagick();
            $image2->readImage($other);
//             $image2->scaleImage(960, 540);

            // set the fuzz factor (must be done BEFORE reading in the images)
            $this->resource->SetOption('fuzz', '1%');

            // compare the images using METRIC=1 (Absolute Error)
            $result = $this->resource->compareImages($image2, 1);

            $this->close();

            return $result[1] < 30;
        }
        catch (Exception $e) {
            Logger::debug(__METHOD__." exception ".$e);
        }

        return false;
    }
}
