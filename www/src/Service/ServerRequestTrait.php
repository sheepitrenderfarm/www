<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

trait ServerRequestTrait {

    /**
     * Do a GET request.
     * If the request failed, the server will be put in timeout.
     * Return false if the http code is not 200 or any error (connection, timeout...)
     */
    public function httpGet(string $url): bool|\Psr\Http\Message\ResponseInterface {
        try {
            $client = new \GuzzleHttp\Client();
            $options = [RequestOptions::CONNECT_TIMEOUT => 5]; // in seconds
            $response = $client->get($url, $options);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) { // http 5xx
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
    }

    /**
     * Do a GET request.
     * If the request failed, the server will NOT be put in timeout.
     * Return false if the http code is not 200 or any error (connection, timeout...)
     * @param String $url
     */
    public function httpGetNoTimeout(string $url): bool|\Psr\Http\Message\ResponseInterface {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get($url);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            return false;
        }
    }

    /**
     * Do a HEAD request.
     * If the request failed, the server will be put in timeout.
     * Return false if the http code is not 200 or any error (connection, timeout...)
     * @param String $url
     */
    public function httpHead(string $url): bool|\Psr\Http\Message\ResponseInterface {
        try {
            $client = new \GuzzleHttp\Client();
            $options = [RequestOptions::CONNECT_TIMEOUT => 5]; // in seconds
            $response = $client->head($url, $options);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) { // http 5xx
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            $this->setServerTimeout(true);
            return false;
        }
    }

    /**
     * Do a POST request.
     * If the request failed, the server will NOT be put in timeout.
     * Return false if the http code is not 200 or any error (connection, timeout...)
     */
    public function httpPostNoTimeout(string $url, array $params): bool|\Psr\Http\Message\ResponseInterface {
        Logger::debug(__METHOD__."($url, ".json_encode($params).")");

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->post($url, ['form_params' => $params]);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            return false;
        }
    }

    /**
     * Do a POST request.
     * If the request failed, the server will NOT be put in timeout.
     * Return false if the http code is not 200 or any error (connection, timeout...)
     */
    public function httpPostWithStringNoTimeout(string $url, string $params): bool|\Psr\Http\Message\ResponseInterface {
        Logger::debug(__METHOD__."($url, ".$params.")");

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->post($url, ['body' => $params]);
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            return false;
        }
    }

    public function httpSendFileNoTimeout(string $url, array $params, string $path): bool|ResponseInterface {
        try {
            $client = new \GuzzleHttp\Client();
            // send with file
            $response = $client->request('POST', $url, array_merge($params, [
                        'multipart' => [
                            [
                                'name' => 'archive',
                                'contents' => fopen($path, 'r'),
                            ],
                        ],
                    ]
                )
            );
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            return false;
        }
    }

    public function httpSendFileWithExecutionLimitNoTimeout(string $url, array $params, string $path): bool|ResponseInterface {
        try {
            $client = new \GuzzleHttp\Client();
            // send with file
            $response = $client->request('POST', $url, array_merge($params, [
                        'timeout' => 15 * 60,
                        'multipart' => [
                            [
                                'name' => 'archive',
                                'contents' => fopen($path, 'r'),
                            ],
                        ],
                    ]
                )
            );
            if ($response->getStatusCode() != 200) {
                return false;
            }
            return $response;
        }
        catch (\GuzzleHttp\Exception\ClientException $e) { // http 4xx
            Logger::debug(__METHOD__.' exception1 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            Logger::critical(__METHOD__.' exception2 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            Logger::debug(__METHOD__.' exception3 for '.$url.' '.$e);
            return false;
        }
        catch (\GuzzleHttp\Exception\RequestException $e) {
            Logger::debug(__METHOD__.' exception4 for '.$url.' '.$e);
            return false;
        }
    }

    private function setServerTimeout(bool $val): void {
        $this->setTimeout($val);
        GlobalInject::getEntityManager()->flush($this);
    }

}
