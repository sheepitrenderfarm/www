<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Entity\ProjectAnalyse;
use App\Utils\File;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SheepItRenderFarm\BlendReader\BlendReaderWithLaunchingBlenderBinary;
use SplFileInfo;
use ZipArchive;

class BlendService {
    private LoggerInterface $logger;
    private array $config;
    private EntityManagerInterface $entityManager;

    public function __construct(LoggerInterface $logger, ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->logger = $logger;
        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
    }

    public function listBlendFilesFromDirectory(string $root, ?ProjectAnalyse $projectAnalyse = null): array|int {
        $files = $this->getBlendFilesFromDirectory($root);
        if (is_array($files) == false) {
            return $files;
        }

        if (is_object($projectAnalyse)) {
            $projectAnalyse->setTotalFileCount(count($files));
            $projectAnalyse->setAnalysedFilesCount(0);
            $this->entityManager->flush($projectAnalyse);
        }

        foreach ($files as $pathname => $empty_array) {
            $info = $this->getBlenderInfos($root.'/'.$pathname);
            if (is_array($info)) {
                $files[$pathname] = $info;
            }
            else {
                unset($files[$pathname]);
            }

            if (is_object($projectAnalyse)) {
                $projectAnalyse->setAnalysedFilesCount($projectAnalyse->getAnalysedFilesCount() + 1);
                $this->entityManager->flush($projectAnalyse);
            }
        }

        return $files;
    }

    private function getBlendFilesFromDirectory(string $path): array|int {
        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
        $files = [];

        /** @var SplFileInfo $file */
        foreach ($rii as $file) {
            if ($file->isDir()) {
                continue;
            }

            if ($this->pathContainsNonSupportedCharacter($file->getPathname())) {
                return -8; // ascii
            }
            elseif ($this->pathContainsNonSupportedWindowsCharacter($file->getPathname())) {
                return -9;
            }
            else {
                if (strtolower($file->getExtension()) == 'blend') {
                    $files[substr($file->getPathname(), strlen($path) + 1)] = []; // end with /
                }
            }
        }
        return $files;
    }

    public function listAllFiles(string $archive_, string $password = ''): array|int {
        $zip = new ZipArchive();
        $ret = $zip->open($archive_);
        if ($ret !== true) {
            Logger::error("list_all_files ($archive_) unable to open file (file_exists: ".serialize(file_exists($archive_)).')');
            return -1;
        }
        if ($password != '') {
            $zip->setPassword($password);
        }

        $b_files = array();
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $stat = $zip->statIndex($i);
            $b_files [] = $stat['name'];
        }
        return $b_files;
    }

    public function listBlendFiles(string $archive_path, string $password = ''): array|int {
        Logger::debug(__METHOD__."($archive_path)");
        $ret = array();
        $temp_dir = File::tempdir($this->config['tmp_dir'], 'FER');
        $blend_files = $this->listAllFiles($archive_path, $password);
        if (is_array($blend_files) === false) {
            Logger::error(__METHOD__."($archive_path) list_blend_files2 did not return an array (return ".serialize($blend_files).")");
            return $blend_files;
        }
        if ($blend_files === array()) {
            return array();
        }

        $zip = new ZipArchive();
        $ret2 = $zip->open($archive_path);
        if ($ret2 !== true) {
            Logger::error(__METHOD__."($archive_path) unable to open file");
            return array();
        }

        if ($password != '') {
            $zip->setPassword($password);
        }

        $ret3 = $zip->extractTo($temp_dir, $blend_files);
        $zip->close();
        if ($ret3 != true) {
            File::rmdirRecurse($temp_dir);
            return $ret;
        }

        foreach ($blend_files as $blender_file) {
            if (file_exists($temp_dir.'/'.$blender_file) == true) {
                $blender_file_in_archive = $temp_dir.'/'.$blender_file;
                if ($this->pathContainsNonSupportedCharacter($blender_file_in_archive)) {
                    return -8; // ascii
                }
                elseif ($this->pathContainsNonSupportedWindowsCharacter($blender_file_in_archive)) {
                    return -9;
                }
                else {
                    if (strtolower(substr($blender_file, -5, 5)) == 'blend') {
                        $ret4 = $this->getBlenderInfos($blender_file_in_archive);
                        if (is_int($ret4) == false) {
                            $ret[$blender_file] = $ret4;
                        }
                    }
                }
            }
        }

        File::rmdirRecurse($temp_dir);
        return $ret;
    }

    public function pathContainsNonSupportedWindowsCharacter(string $path): bool {
        //    / \ : * ? " < > |
        foreach (['\\', ':', '*', '?', '"', '<', '>', '|'] as $char) {
            if (str_contains($path, $char)) {
                return true;
            }
        }
        return false;
    }

    public function pathContainsNonSupportedCharacter(string $path): bool {
        if (mb_detect_encoding($path, 'ASCII', true) == false) {
            return true;
        }
        if (preg_match('/[\!@#\$%\^&\?]/', $path) == 1) {
            return true;
        }

        return false;
    }

    public function getBlenderInfos(string $blender_file): array|int {
        Logger::debug("get_blender_infos($blender_file)");

        $reader = new BlendReaderWithLaunchingBlenderBinary($this->logger, $this->config['reader']['blender4.0']);
        if ($reader->open($blender_file) == false) {
            return -3;
        }
        $infos_file = [];
        if (basename($blender_file) == '.blend') { // no name
            $infos_file['error'] = 'Error: need as actual file name';
            return $infos_file;
        }
        $version = $reader->getVersion();
        if ($version == '') {
            $infos_file['error'] = 'Failed to detect file information';
        }
        else {
            if (floatval($version) < $this->config['renderer']['minimal_support']) {
                $infos_file['error'] = 'Blender version not supported, minimal supported: blender '.$this->config['renderer']['minimal_support'];
            }
            else {
                $infos_file = $reader->getInfos();
                if (is_array($infos_file) == false) {
                    $infos_file = [];
                    $infos_file['error'] = 'Failed to detect file information';
                }
                else {
                    /** @var array<string, mixed> $infos_file */
                    if (array_key_exists('resolution_percentage', $infos_file) && array_key_exists('resolution_x', $infos_file) && array_key_exists('resolution_y', $infos_file)) {
                        $x = intval($infos_file['resolution_percentage']) * intval($infos_file['resolution_x']) / 100.0; // percentage is a number (0-100)
                        $y = intval($infos_file['resolution_percentage']) * intval($infos_file['resolution_y']) / 100.0; // percentage is a number (0-100)
                        if ($x > $this->config['project']['max']['width']) {
                            $infos_file['error'] = 'Width too big. Max allowed width: '.$this->config['project']['max']['width'].' height: '.$this->config['project']['max']['height'];
                        }
                        if ($y > $this->config['project']['max']['height']) {
                            $infos_file['error'] = 'Height too big. Max allowed width: '.$this->config['project']['max']['width'].' height: '.$this->config['project']['max']['height'];
                        }
                    }

                    if (array_key_exists('optix_is_active', $infos_file) && $infos_file['optix_is_active'] == true) {
                        $infos_file['error'] = 'Optix is not supported.<br>Please disable OptiX or switch to another denoising method in this project.';
                    }

                    if (array_key_exists('supported_Color_Management', $infos_file) && $infos_file['supported_Color_Management'] == false) {
                        $infos_file['error'] = 'Project contains unsupported Color Management<br>We only support configs that are shipped with Blender.';
                    }

                    // CAUTION: this need to be sync with conf.py and conf_exr.py
                    if (array_key_exists('output_file_format', $infos_file) && in_array($infos_file['output_file_format'], ['BMP', 'PNG', 'JPEG', 'TARGA', 'TARGA_RAW', 'OPEN_EXR_MULTILAYER', 'OPEN_EXR']) == false) {
                        $infos_file['output_file_extension'] = '.png';
                        $infos_file['warning'] = 'Output file extension not supported, it will override to PNG';
                    }

                    if (array_key_exists('engine', $infos_file) && $infos_file['engine'] == 'MIXED') {
                        $infos_file['error'] = 'Multiple render engine on the same project is not supported.<br>Please check the scenes inside the .blend';
                    }

                    if (array_key_exists('output_file_extension', $infos_file) && $infos_file['output_file_extension'] == '.exr') {
                        if (array_key_exists('exr_codec', $infos_file) && in_array(strtolower($infos_file['exr_codec']), $this->config['project']['exr']['compressions_supported']) == false) {
                            $infos_file['error'] = 'Error: compression methode not supported, needs to be one of '.join(', ', $this->config['project']['exr']['compressions_supported']).'.';
                        }
                        if ($infos_file['output_file_format'] == 'OPEN_EXR_MULTILAYER' && array_key_exists('enabled_passes', $infos_file) && count($infos_file['enabled_passes']) > $this->config['project']['exr']['maximum_layers']) {
                            $infos_file['error'] = sprintf('Error: Since you are using EXR, you are limited to %d layers/passes, you currently using %d.', $this->config['project']['exr']['maximum_layers'], count($infos_file['enabled_passes']));
                        }
                        if (array_key_exists('must_be_disabled_passes', $infos_file) && is_array($infos_file['must_be_disabled_passes']) && count($infos_file['must_be_disabled_passes']) > 0) {
                            $infos_file['error'] = "This blend has still hidden passes who will increase a lot the output exr file size.<br>You need to disabled them, it's usually eevee passes when you use cycles (or the other way around cycles passes when eevee is used).<br>List of disabled passes to be removed:";
                            $infos_file['error'] .= '<ul>';
                            foreach ($infos_file['must_be_disabled_passes'] as $pass1) {
                                $infos_file['error'] .= '<li>'.$pass1.'</li>';
                            }
                            $infos_file['error'] .= '</ul>';
                        }

                        if (array_key_exists('resolution_percentage', $infos_file) && array_key_exists('resolution_x', $infos_file) && array_key_exists('resolution_y', $infos_file)) {
                            $dimension_x = (int)((float)($infos_file["resolution_percentage"]) * (float)($infos_file["resolution_x"]) / 100.0);
                            $dimension_y = (int)((float)($infos_file["resolution_percentage"]) * (float)($infos_file["resolution_y"]) / 100.0);
                            $width_blend = max($dimension_x, $dimension_y);
                            $height_blend = min($dimension_x, $dimension_y);

                            if ($width_blend > $this->config['project']['exr']['maximum_dimension']['width'] || $height_blend > $this->config['project']['exr']['maximum_dimension']['height']) {
                                $infos_file['error'] = 'Maximum image dimension allowed '.$this->config['project']['exr']['maximum_dimension']['width'].'x'.$this->config['project']['exr']['maximum_dimension']['height'];
                            }
                        }
                    }
                }
            }
        }

        return $infos_file;
    }
}