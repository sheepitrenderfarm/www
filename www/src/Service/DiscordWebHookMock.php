<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Service;

use App\Base\BaseProcessingUnit;

class DiscordWebHookMock extends DiscordWebHook {

    public function __construct() {
    }

    public function projectMirrorEnable(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function binaryMirrorEnable(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function shepherdEnable(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function shepherdPause(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function shepherdTaskFailed(string $id, string $type, string $project, string $logs): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function userRightToRender(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function userRightAddProject(string $id, bool $enable, string $author): void {
        Logger::info(__METHOD__." for ".$id);
    }

    public function userMessage(string $author, string $destination, string $url, string $message): void {
        Logger::info(__METHOD__);
    }

    public function sessionPowerDetectionHack(string $user, string $url, int $from_user, BaseProcessingUnit $device): void {
        Logger::info(__METHOD__);
    }
}
