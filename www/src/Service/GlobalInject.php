<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class GlobalInject {
    private static Main $main;
    private static UrlGeneratorInterface $router;
    private static EntityManagerInterface $entityManager;
    private static ConfigService $configService;

    public static function getMain(): Main {
        return self::$main;
    }

    public static function setMain(Main $main): void {
        self::$main = $main;
    }

    public static function getRouter(): UrlGeneratorInterface {
        return self::$router;
    }

    public static function setRouter(UrlGeneratorInterface $router): void {
        self::$router = $router;
    }

    public static function getEntityManager(): EntityManagerInterface {
        return self::$entityManager;
    }

    public static function setEntityManager(EntityManagerInterface $entityManager): void {
        self::$entityManager = $entityManager;
    }

    public static function getConfigService(): ConfigService {
        return self::$configService;
    }

    public static function setConfigService(ConfigService $configService): void {
        self::$configService = $configService;
    }

    public static function setInject(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService): void {
        self::setMain($main);
        self::setRouter($router);
        self::setEntityManager($entityManager);
        self::setConfigService($configService);
    }
}