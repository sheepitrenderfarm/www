<?php

namespace App\Service;

class RequestTimeProfiler {
    public const CREATE_SESSION = "creation_session";
    public const REQUEST_JOB = "request_job";
    public const VALIDATE_JOB = "validate_job";
    public const ARCHIVE_BINARY = "archive_binary";
    public const ARCHIVE_PROJECT = "archive_project";
    public const ARCHIVE_BINARY_FALLBACK = "archive_binary_fallback";
    public const ARCHIVE_PROJECT_FALLBACK = "archive_project_fallback";

    private array $config;

    public function __construct(ConfigService $configService) {
        $this->config = $configService->getData();
    }

    /**
     * @param int $time in ms
     */
    public function setTime(string $route, int $time): void {
        @file_put_contents($this->file(), $route.' '.$time."\r\n", FILE_APPEND);
    }

    /**
     * @return array<string, float>
     */
    public function getAverage(): array {
        $data = [];
        $contents = @file_get_contents($this->file());
        if (is_string($contents)) {
            foreach (explode("\n", $contents) as $line) {
                $request = explode(" ", $line);
                if (count($request) == 2) {
                    if (array_key_exists($request[0], $data) == false) {
                        $data[$request[0]] = ['count' => 0, 'total' => 0];
                    }
                    $data[$request[0]]['count'] += 1.0;
                    $data[$request[0]]['total'] += (float)(trim($request[1]));
                }
            }
        }

        $ret = [];
        foreach ($data as $route => $v) {
            $ret[$route] = (int)($v['total'] / $v['count']);
        }
        return $ret;
    }

    public function reset(): void {
        @unlink($this->file());
    }

    private function file(): string {
        return $this->config['log']['dir'].'/route.log';
    }
}