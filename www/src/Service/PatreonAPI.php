<?php

namespace App\Service;

/**
 * API for patreon.com
 */
class PatreonAPI {
    private string $access_token;

    private string $refresh_token;
    private string $client_id;
    private string $client_secret;

    public function __construct(string $client_id, string $client_secret, string $refresh_token) {
        $this->refresh_token = $refresh_token;
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
    }

    public function getRefreshToken(): string {
        return $this->refresh_token;
    }

    public function getClientID(): string {
        return $this->client_id;
    }

    public function getClientSecret(): string {
        return $this->client_secret;
    }

    /**
     * Get list of campaigns
     */
    public function getCampaigns(): array {
        $patreonVendorAPI = new \Patreon\API($this->access_token);

        $ret = [];
        foreach ($patreonVendorAPI->fetch_campaigns()['data'] as $data) {
            $ret [] = $data['id'];
        }
        return $ret;
    }

    /**
     * Get list of members of a specific campaign
     */
    public function getMembersOfCampaign(string $campaign_id): array {
        $patreonVendorAPI = new \Patreon\API($this->access_token);
        $url = "campaigns/{$campaign_id}/members?fields%5Bmember%5D=full_name,email,last_charge_date,last_charge_status,patron_status&page%5Bsize%5D=1000";
        return $patreonVendorAPI->get_data($url)['data'];
    }

    public function updateToken(): bool {
        // Get a fresher access token

        $oauth_client = new \Patreon\OAuth($this->client_id, $this->client_secret);
        $tokens = $oauth_client->refresh_token($this->refresh_token, null);

        if (array_key_exists('access_token', $tokens) && array_key_exists('refresh_token', $tokens)) {
            $this->access_token = $tokens['access_token'];
            $this->refresh_token = $tokens['refresh_token'];
            return true;
        }
        return false;
    }
}