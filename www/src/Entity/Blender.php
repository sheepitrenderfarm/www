<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\BlenderRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * RenderBinary
 */
#[ORM\Table(name: 'blender')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
#[ORM\Entity(repositoryClass: BlenderRepository::class)]
class Blender extends Entity {
    private const SCRIPT = "import bpy, sys
bpy.context.scene.cycles.use_auto_tile = True
bpy.context.scene.cycles.tile_size = 2048
def sheepit_set_compute_device(device_type, device_family, device_id):
    # either OPTIX, GPU, OPTIX_0
    # or     NONE,  CPU, CPU

    bpy.context.scene.cycles.device = device_family
    
    # fill up the devices list
    bpy.context.preferences.addons['cycles'].preferences.get_devices()
    
    bpy.context.preferences.addons['cycles'].preferences.compute_device_type = device_type
    devices = bpy.context.preferences.addons['cycles'].preferences.devices
    compatible_devices = [d for d in devices if (d.type == device_type or (d.type == 'CPU' and device_type == 'NONE')) and d.id == device_id]
    if not compatible_devices:
        sys.exit(f\"DETECT_DEVICE_ERROR: Couldn't find {device_type} device with id {device_id}\")

    for device in devices:
        device.use = device in compatible_devices
";

    /**
     * Id is the actual version of blender, example "blender4.1.2"
     */
    #[ORM\Column(name: 'id', type: Types::STRING, length: 32, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'human', type: Types::STRING, length: 255, nullable: false)]
    protected string $human;

    /**
     * Blender Foundation version code (4.1.2 -> blender401)
     */
    #[ORM\Column(name: 'bf_code', type: Types::STRING, length: 255, nullable: false)]
    protected string $bf_code;

    #[ORM\Column(name: 'enable', type: Types::BOOLEAN, nullable: false)]
    protected bool $enable = true;

    public function getId(): string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;
        return $this;
    }

    public function getHuman(): string {
        return $this->human;
    }

    public function setHuman(string $human): self {
        $this->human = $human;
        return $this;
    }

    public function getBfCode(): string {
        return $this->bf_code;
    }

    public function setBfCode(string $bf_code): self {
        $this->bf_code = $bf_code;
        return $this;
    }

    public function getEnable(): bool {
        return $this->enable;
    }

    public function setEnable(bool $enable): self {
        $this->enable = $enable;

        return $this;
    }

    public function getScript(): string {
        // $major = substr($this->executable, strlen('blender'), 2);
        // if (in_array($major, ['28', '29'])) {

        return Blender::SCRIPT;
    }
}
