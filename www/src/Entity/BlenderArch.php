<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace App\Entity;

use App\Repository\BlenderArchRepository;
use App\Service\GlobalInject;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Blender sub architecture
 */
#[ORM\Table(name: 'blender_arch')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\Entity(repositoryClass: BlenderArchRepository::class)]
class BlenderArch extends CDNSyncEntity {
    public const SUPPORTED_ARCHS = array(
        'windows' => 'Windows',
        'linux' => 'Linux',
        'mac' => 'OSX Intel',
        'macm1' => 'OSX M',
    );

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'arch', type: Types::STRING, length: 32, nullable: false)]
    protected string $arch;

    #[ORM\Column(name: 'md5', type: Types::STRING, length: 32, nullable: false)]
    protected string $md5;

    #[ORM\JoinColumn(name: 'blender', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Blender::class)]
    protected Blender $blender;

    /**
     * @var array<int, string>
     *
     */
    #[ORM\Column(name: 'chunks', type: 'json', nullable: false)]
    protected array $chunks = [];

    #[ORM\Column(name: 'archive_size', type: 'integer', nullable: false)]
    protected int $archive_size = 0;

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getArch(): string {
        return $this->arch;
    }

    public function setArch(string $arch): self {
        $this->arch = $arch;
        return $this;
    }

    public function getMd5(): string {
        return $this->md5;
    }

    public function setMd5(string $md5): self {
        $this->md5 = $md5;
        return $this;
    }

    public function getBlender(): Blender {
        return $this->blender;
    }

    public function setBlender(Blender $blender): self {
        $this->blender = $blender;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getChunks(): array {
        return $this->chunks;
    }

    public function setChunks(array $chunks): self {
        $this->chunks = $chunks;

        return $this;
    }

    public function getArchiveSize(): int {
        return $this->archive_size;
    }

    public function setArchiveSize(int $archive_size): self {
        $this->archive_size = $archive_size;

        return $this;
    }

    public function getPath(): string {
        $config = GlobalInject::getConfigService()->getData();
        return strtolower($this->blender->getId().'_'.$this->getArch().'_64bit').'.'.$config['archive']['extension'];
    }

    public function fullPathOfChunk(string $chunk): string {
        $config = GlobalInject::getConfigService()->getData();
        return $config['storage']['path'].'projects'.'/'.$chunk;
    }

//    public function getHuman(): string {
//        return match ($this->getArch()) {
//            'windows' => 'Windows',
//            'linux' => 'Linux',
//            'mac' => 'OSX Intel',
//            'macm1' => 'OSX Mx',
//            default => $this->getArch(),
//        };
//    }
}
