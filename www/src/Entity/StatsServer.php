<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsServerRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsServer
 */
#[ORM\Table(name: 'stats_host')]
#[ORM\Entity(repositoryClass: StatsServerRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsServer extends Entity {

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $id;

    #[ORM\Column(name: 'cpu_load', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $cpu_load;

    #[ORM\Column(name: 'ram_used', type: Types::BIGINT, nullable: false)]
    protected int $ram_used;

    #[ORM\Column(name: 'swap_used', type: Types::BIGINT, nullable: false)]
    protected int $swap_used;

    #[ORM\Column(name: 'httpd_workers_www', type: Types::BIGINT, nullable: false)]
    protected int $httpd_workers_www = 0;

    #[ORM\Column(name: 'httpd_workers_api', type: Types::BIGINT, nullable: false)]
    protected int $httpd_workers_api = 0;

    #[ORM\Column(name: 'httpd_workers_client', type: Types::BIGINT, nullable: false)]
    protected int $httpd_workers_client = 0;

    #[ORM\Column(name: 'network_rx', type: Types::BIGINT, nullable: false)]
    protected int $network_rx = 0;

    #[ORM\Column(name: 'network_tx', type: Types::BIGINT, nullable: false)]
    protected int $network_tx = 0;

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getCpuLoad(): float {
        return $this->cpu_load;
    }

    public function setCpuLoad(float $cpuLoad): self {
        $this->cpu_load = $cpuLoad;

        return $this;
    }

    public function getRamUsed(): int {
        return $this->ram_used;
    }

    public function setRamUsed(int $ramUsed): self {
        $this->ram_used = $ramUsed;

        return $this;
    }

    public function getSwapUsed(): int {
        return $this->swap_used;
    }

    public function setSwapUsed(int $swapUsed): self {
        $this->swap_used = $swapUsed;

        return $this;
    }

    public function getHttpdWorkersWWW(): int {
        return $this->httpd_workers_www;
    }

    public function setHttpdWorkersWWW(int $val): self {
        $this->httpd_workers_www = $val;

        return $this;
    }

    public function getHttpdWorkersApi(): int {
        return $this->httpd_workers_api;
    }

    public function setHttpdWorkersApi(int $val): self {
        $this->httpd_workers_api = $val;

        return $this;
    }

    public function getHttpdWorkersClient(): int {
        return $this->httpd_workers_client;
    }

    public function setHttpdWorkersClient(int $val): self {
        $this->httpd_workers_client = $val;

        return $this;
    }

    public function getNetworkTX(): int {
        return $this->network_tx;
    }

    public function setNetworkTX(int $network): self {
        $this->network_tx = $network;

        return $this;
    }

    public function getNetworkRX(): int {
        return $this->network_rx;
    }

    public function setNetworkRX(int $network): self {
        $this->network_rx = $network;

        return $this;
    }

    public static function getCurrent(): StatsServer {
        $stats = new StatsServer();
        $stats->setCpuLoad(sys_getloadavg()[0]);

        // memory
        $meminfo = array();
        foreach (explode("\n", trim(file_get_contents("/proc/meminfo"))) as $line) {
            list($key, $val) = explode(":", $line);
            $meminfo[$key] = intval(trim(str_replace(' kB', '', $val))) * 1000;
        }
        $stats->setSwapUsed($meminfo['SwapTotal'] - $meminfo['SwapFree'] - $meminfo['SwapCached']);
        $stats->setRamUsed($meminfo['MemTotal'] - $meminfo['MemAvailable']);

        foreach (explode("\n", @file_get_contents('http://httpd-www/server-status?auto')) as $line) {
            $data = explode(":", $line);
            if (count($data) == 2 && $data[0] == 'BusyWorkers') {
                $stats->setHttpdWorkersWWW((int)($data[1]));
                break;
            }
        }

        foreach (explode("\n", @file_get_contents('http://httpd-api/server-status?auto')) as $line) {
            $data = explode(":", $line);
            if (count($data) == 2 && $data[0] == 'BusyWorkers') {
                $stats->setHttpdWorkersApi((int)($data[1]));
                break;
            }
        }

        foreach (explode("\n", @file_get_contents('http://httpd-client/server-status?auto')) as $line) {
            $data = explode(":", $line);
            if (count($data) == 2 && $data[0] == 'BusyWorkers') {
                $stats->setHttpdWorkersClient((int)($data[1]));
                break;
            }
        }

        $json_api = json_decode(@file_get_contents('http://httpd-api/api/internal/network?host=api'), true);
        $json_www = json_decode(@file_get_contents('http://httpd-www/api/internal/network?host=www'), true);
        $json_client = json_decode(@file_get_contents('http://httpd-client/api/internal/network?host=client'), true);

        if (is_array($json_api) && is_array($json_www) && is_array($json_client) &&
            array_key_exists('tx', $json_api) && array_key_exists('rx', $json_api) &&
            array_key_exists('tx', $json_www) && array_key_exists('rx', $json_www) &&
            array_key_exists('tx', $json_client) && array_key_exists('rx', $json_client)) {

            $stats->setNetworkTX($json_api['tx'] + $json_www['tx'] + $json_client['tx']);
            $stats->setNetworkRX($json_api['rx'] + $json_www['rx'] + $json_client['rx']);
        }

        return $stats;
    }
}
