<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\SessionErrorRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Error sent by renderer on Session
 */
#[ORM\Table(name: 'session_error')]
#[ORM\Index(name: 'session', columns: ['session'])]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'project', columns: ['project'])]
#[ORM\Entity(repositoryClass: SessionErrorRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class SessionError extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'session', type: Types::INTEGER, nullable: true)]
    protected ?int $session = null;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    #[ORM\Column(name: 'user', type: Types::STRING, length: 50, nullable: false)]
    protected string $user;

    /**
     * Type of error see \App\Scheduler\ClientError
     */
    #[ORM\Column(name: 'type', type: Types::INTEGER, nullable: false)]
    protected int $type = 0;

    #[ORM\Column(name: 'project', type: Types::INTEGER, nullable: false)]
    protected int $project = 0;

    public function getId(): ?int {
        return $this->id;
    }

    public function getSession(): ?int {
        return $this->session;
    }

    public function setSession(?int $session): self {
        $this->session = $session;

        return $this;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getUser(): ?string {
        return $this->user;
    }

    public function setUser(string $user): self {
        $this->user = $user;

        return $this;
    }

    public function getType(): int {
        return $this->type;
    }

    public function setType(int $type): self {
        $this->type = $type;

        return $this;
    }

    public function getProject(): int {
        return $this->project;
    }

    public function setProject(int $project): self {
        $this->project = $project;

        return $this;
    }

    public static function toEvent(SessionError $o): Event {
        $e = new Event();
        $e->setTime($o->getTime());
        $e->setSession($o->getSession());
        $e->setUser($o->getUser());
        $e->setType(Event::TYPE_SENDERROR);
        $e->setProject($o->getProject());
        $e->setData((string)$o->getType());

        return $e;
    }
}
