<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Scheduler\ClientProtocol;
use App\Service\GlobalInject;
use App\Service\Image;
use App\Service\Logger;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TileComputeMethod extends Tile {
    public const FRAME_NUMBER = 340;

    public function reset(bool $give_points_to_renderer = false): bool {
        return false;
    }

    public function resume(): bool {
        return false;
    }

    public function update(): bool {
        return false;
    }

    public function remove(bool $do_sql_query = true): bool {
        return false;
    }

    public function validate(string $picture_file_, Session $session_): int {
        Logger::debug(__METHOD__);
        $config = GlobalInject::getConfigService()->getData();
        $status = ClientProtocol::OK;
        $a_image = new Image($picture_file_);
        if ($a_image->isImage() == false) {
            $status = ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE;
        }
        else {
            $ref = __DIR__.'/../../public/media/image/compute-method-reference.png';
            if ($a_image->compareTo($ref, false) == false) {
                Logger::debug('session id:'.$session_->getId().' login:'.$session_->getUser()->getId().' compute_method:'.$session_->getComputeMethod().' have been disabled because the rendered ref image is not the same as server side');
                $session_->setBlocked(1);
                GlobalInject::getEntityManager()->flush($session_);
                $status = ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE;
            }
            else {
                $points = $config['rank']['points']['rendered']['compute_method_frame'];
                $session_->setPoints($session_->getPoints() + $points);
                GlobalInject::getEntityManager()->flush($session_);

                $renderer = $session_->getUser();
                $renderer->setPoints($renderer->getPoints() + $points);
                $renderer->setPointsEarn($renderer->getPointsEarn() + $points);
                GlobalInject::getEntityManager()->flush($renderer);
            }
        }
        @unlink($picture_file_);

        return $status;
    }

    public function take(Session $a_session_): int {
        return -5;
    }

    public function getPointsForOwner(): float {
        return 0.0;
    }

    public function getPointsForRenderer(): float {
        return 0.0;
    }

    public function getFullPathThumbnail(): string {
        return __DIR__.'/../public/media/image/frame_compute_method.jpg';
    }

    public function getComputeDevice(): ?BaseProcessingUnit {
        return null;
    }

    public function validateFromShepherd(int $rendertime, int $preptime, int $memoryused): int {
        return -99;
    }

    public function getRenderTimeOnRefMachine(): float {
        return 0.0;
    }

    public function getRenderTimeOnRefMachineWithCoefAligned(): float {
        return 0.0;
    }

    public function getUrlThumbnailOnShepherd(bool $ondemand = false): string {
        return "";
    }

    public function getUrlFullOnShepherd(): string {
        return "";
    }

    public function getEstimatedRenderTime(): int {
        return 0;
    }
}