<?php

namespace App\Entity;

use App\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;

#[MappedSuperclass]
class CDNSyncEntity extends Entity {
    /**
     * Is the entity on the CDN bucket (if not, the entity will be downloaded from www directly)
     */
    #[ORM\Column(name: 'synced_bucket', type: Types::BOOLEAN, nullable: false)]
    protected bool $synced_bucket = false;


    public function getSyncedBucket(): ?bool {
        return $this->synced_bucket;
    }

    public function setSyncedBucket(bool $synced_bucket): self {
        $this->synced_bucket = $synced_bucket;

        return $this;
    }
}