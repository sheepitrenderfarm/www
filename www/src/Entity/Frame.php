<?php

namespace App\Entity;

use App\Constant;
use App\Entity;
use App\Repository\FrameRepository;
use App\Scheduler\ClientProtocol;
use App\Service\GlobalInject;
use App\Service\Logger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Frame
 */
#[ORM\Table(name: 'frame')]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Entity(repositoryClass: FrameRepository::class)]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
abstract class Frame extends Entity {

    #[ORM\Column(name: 'id', type: Types::BIGINT, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'number', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $number;

    #[ORM\JoinColumn(name: 'project', referencedColumnName: 'id', onDelete: 'CASCADE', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Project::class, inversedBy: 'frames', cascade: ['persist'])]
    protected Project $project;

    /**
     * Total number of splits
     */
    #[ORM\Column(name: 'split', type: Types::INTEGER, nullable: false)]
    protected int $split;

    /**
     * @var Collection<Tile>
     */
    #[ORM\OneToMany(targetEntity: Tile::class, mappedBy: 'frame', cascade: ['persist', 'remove'])]
    protected Collection $tiles;

    #[ORM\Column(name: 'cache_cost', type: Types::FLOAT, nullable: false, options: ['unsigned' => true])]
    protected float $cache_cost = 0.0;

    #[ORM\Column(name: 'cache_rendertime', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_rendertime = 0;

    /**
     * Render time on reference machine
     */
    #[ORM\Column(name: 'cache_rendertime_ref', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_rendertime_ref = 0;

    #[ORM\Column(name: 'cache_status', type: Types::STRING, length: 32, nullable: false)]
    protected string $cache_status = Constant::WAITING;

    public function __construct() {
        $this->tiles = new ArrayCollection();
    }

    public function __addTile(Tile $tile): self {
        if (!$this->tiles->contains($tile)) {
            $this->tiles->add($tile);
            $tile->setFrame($this);
        }

        return $this;
    }

    public function __removeTile(Tile $tile): self {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    /**
     * @return Collection<Tile>
     */
    public function tiles(): Collection {
        return $this->tiles;
    }

    /**
     * @return Collection<int, Tile>
     */
    public function tilesWithStatus(string $status): Collection {
        return $this->tiles()->filter(function (Tile $element) use ($status) {
            return $element->getStatus() == $status;
        });
    }

    abstract public function getPythonScript(Tile $tile, ClientProtocol $client_protocol, bool $use_gpu): string;

    abstract public function getSplitHumanExplanation(): string;

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getNumber(): int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    public function getSplit(): int {
        return $this->split;
    }

    public function setSplit(int $split): self {
        $this->split = $split;

        return $this;
    }

    public function getProject(): Project {
        return $this->project;
    }

    public function setProject(Project $project): self {
        $this->project = $project;

        return $this;
    }

    public function getStatus(): string {
        return $this->cache_status;
    }

    public function getUrlThumbnailOnShepherd(): string {
        return $this->getProject()->getShepherd()->getId().'/thumb/'.$this->getProject()->getShepherdThumbnailToken().'/'.$this->getProject()->getId().'/frame/'.$this->getNumber().'/thumbnail';
    }

    public function getUrlFullOnShepherd(): string {
        return $this->getProject()->getShepherd()->getId().'/blend/'.$this->getProject()->getShepherdOwnerToken().'/'.$this->getProject()->getId().'/frame/'.$this->getNumber().'/full';
    }

    public function setCacheRenderTime(int $val): void {
        $this->cache_rendertime = $val;
    }

    public function setCacheRenderTimeRef(int $val): void {
        $this->cache_rendertime_ref = $val;
    }

    public function setCacheCost(float $val): self {
        $this->cache_cost = $val;
        return $this;
    }

    public function setCacheStatus(string $val): self {
        $this->cache_status = $val;
        return $this;
    }

    public function getRenderTime(): int {
        return $this->cache_rendertime;
    }

    public function getRenderTimeRef(): int {
        return $this->cache_rendertime_ref;
    }

    public function getCost(): float {
        return $this->cache_cost;
    }

    public function updateCache(float $diff_rendertime, float $diff_rendertime_ref, int $diff_cost): void {
        Logger::error(__METHOD__.' '.$diff_rendertime.' '.$diff_rendertime_ref.' '.$diff_cost);
        $this->cache_cost += $diff_cost;
        $this->cache_rendertime += intval($diff_rendertime);
        $this->cache_rendertime_ref += intval($diff_rendertime_ref);

        $total = 0;
        $finished = 0;
        foreach ($this->tiles() as $tile) {
            if ($tile->getStatus() == Constant::PROCESSING) {
                $this->cache_status = Constant::PROCESSING;
                return;
            }
            elseif ($tile->getStatus() == Constant::FINISHED) {
                $finished++;
            }
            $total++;
        }
        if ($total == $finished) {
            $this->cache_status = Constant::FINISHED;
        }
        elseif ($finished > 0) {
            $this->cache_status = Constant::PROCESSING;
        }
        else {
            $this->cache_status = Constant::WAITING;
        }
    }

    public function updateStatus(): void {
        // update parent frame status
        $frame_status = Constant::WAITING;
        $tiles_status = [Constant::WAITING => 0, Constant::PROCESSING => 0, Constant::FINISHED => 0];
        foreach ($this->tiles() as $aTile) {
            $tiles_status[$aTile->getStatus()] += 1;
            if ($aTile->getStatus() == Constant::PROCESSING) {
                $this->setCacheStatus(Constant::PROCESSING);
                GlobalInject::getEntityManager()->flush($this);
                return;
            }
        }

        if ($tiles_status[Constant::FINISHED] == 0 && $tiles_status[Constant::PROCESSING] == 0) {
            $frame_status = Constant::WAITING;
        }
        elseif ($tiles_status[Constant::FINISHED] == $this->tiles()->count()) {
            $frame_status = Constant::FINISHED;
        }

        $this->setCacheStatus($frame_status);
        GlobalInject::getEntityManager()->flush($this);
    }
}