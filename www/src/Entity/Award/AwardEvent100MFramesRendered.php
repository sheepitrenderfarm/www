<?php
/**
 * Copyright (C) 2018 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent100MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1561058871;
    }

    public function getAuthor(): string {
        return 'zach';
    }

    public function getObjectiveLong(): string {
        return '100,000,000th';
    }

    public function getObjectiveShort(): string {
        return '100M';
    }
}
