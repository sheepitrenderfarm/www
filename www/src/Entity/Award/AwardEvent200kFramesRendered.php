<?php
/**
 * Copyright (C) 2013-2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent200kFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1384214400;
    }

    public function getAuthor(): string {
        return 'Yeti';
    }

    public function getObjectiveLong(): string {
        return '200,000th';
    }

    public function getObjectiveShort(): string {
        return '200k';
    }
}
