<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEventChristmas2022 extends Award {

    public function reward(): int {
        return 10000;
    }

    public function humanDescription(): string {
        return 'Participated during the celebration of advent calendar of 2022. During December 2022, you need to open at least 10 gift boxes on the home page.<br><i>Award image by Foxel.</i>';
    }

    public function imagePath(): string {
        return '/media/image/award/award_event_christmas_2022.png';
    }

    public function category(): string {
        return 'event';
    }

    public function level(): int {
        return 1671980477;
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function cronDaily(): void {
        if (date('Y-m-d') == '2022-12-25' || date('Y-m-d') == '2022-12-26') {
            $repoUser = GlobalInject::getEntityManager()->getRepository(User::class);
            $repoClaimedDay = GlobalInject::getEntityManager()->getRepository(AdventCalendarClaimedDay::class);
            foreach ($repoClaimedDay->getUniqueUsers() as $login) {
                $user = $repoUser->find($login);
                if (is_object($user) && $user->adventCalendarClaimedDays()->count() >= 10) {
                    $user->giveAward('AwardEventChristmas2022');
                }
            }
        }
    }
}
