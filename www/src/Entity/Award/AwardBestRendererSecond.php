<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use App\Service\Logger;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardBestRendererSecond extends Award {
    public function canBeEarn(): bool {
        return true;
    }

    public function reward(): int {
        return 25000;
    }

    public function category(): string {
        return 'best_renderer';
    }

    public function humanDescription(): string {
        return 'Second-best contributor over a 30 days rolling period.<br >Ranks are updated every day, check at the bottom of the page for the current list.';
    }

    public function imagePath(): string {
        return '/media/image/award/award_best_renderer_second.png';
    }

    public function cronDaily(): void {
        $users = GlobalInject::getMain()->getBestRenderersFromCache();
        if (count($users) < 2) {
            Logger::error(__METHOD__.' not enough user to give a 2nd place');
            return;
        }

        reset($users);

        $first_key = key($users);
        next($users);
        $second = key($users);
        next($users);
        $stats = $users[$second];
        $user = GlobalInject::getEntityManager()->getRepository(User::class)->find($stats->getLogin());
        if (is_object($user)) {
            $user->giveAward($this->getShortName());
        }
    }
}
