<?php
/**
 * Copyright (C) 2018 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent70MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1531903000;
    }

    public function getAuthor(): string {
        return 'rrrak';
    }

    public function getObjectiveLong(): string {
        return '70,000,000th';
    }

    public function getObjectiveShort(): string {
        return '70M';
    }
}
