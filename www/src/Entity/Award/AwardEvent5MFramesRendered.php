<?php
/**
 * Copyright (C) 2013-2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent5MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1436119987;
    }

    public function getAuthor(): string {
        return 'Paul Szajner aka Sozap';
    }

    public function getObjectiveLong(): string {
        return '5,000,000th';
    }

    public function getObjectiveShort(): string {
        return '5M';
    }
}
