<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent20MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1481527427;
    }

    public function getAuthor(): string {
        return 'JakeD7';
    }

    public function getObjectiveLong(): string {
        return '20,000,000th';
    }

    public function getObjectiveShort(): string {
        return '20M';
    }
}
