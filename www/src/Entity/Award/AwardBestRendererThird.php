<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use App\Service\Logger;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardBestRendererThird extends Award {
    public function canBeEarn(): bool {
        return true;
    }

    public function reward(): int {
        return 20000;
    }

    public function category(): string {
        return 'best_renderer';
    }

    public function humanDescription(): string {
        return 'Third-best contributor over a 30 days rolling period.<br >Ranks are updated every day, check at the bottom of the page for the current list.';
    }

    public function imagePath(): string {
        return '/media/image/award/award_best_renderer_third.png';
    }

    public function cronDaily(): void {
        $users = GlobalInject::getMain()->getBestRenderersFromCache();
        if (count($users) < 3) {
            Logger::error(__METHOD__.' not enough user to give a 3rd place');
            return;
        }

        reset($users);

        $first_key = key($users);
        next($users);
        $second = key($users);
        next($users);
        $third = key($users);
        $stats = $users[$third];
        $user = GlobalInject::getEntityManager()->getRepository(User::class)->find($stats->getLogin());
        if (is_object($user)) {
            $user->giveAward($this->getShortName());
        }
    }
}
