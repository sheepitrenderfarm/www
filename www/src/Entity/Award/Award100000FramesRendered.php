<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award100000FramesRendered extends AwardRenderedFrames {

    public function getObjective(): int {
        return 100000;
    }

    public function reward(): int {
        return 40000;
    }
}
