<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent500kProjects extends Award {

    public function getDate(): int {
        return 1656690379;
    }

    public function getAuthor(): string {
        return 'pufutama';
    }

    public function reward(): int {
        return 2000;
    }

    public function humanDescription(): string {
        return sprintf('Participated during the celebration of 500,000th project event, celebrated on %s. To get the award, you simply had to render a frame around the date\'s event. We regularly do events like this to thank everyone who keeps SheepIt alive.<br><i>Award image by %s.</i>', date('F, jS Y', $this->getDate()), $this->getAuthor());
    }

    public function imagePath(): string {
        return '/media/image/award/award_event_500000_projects.png';
    }

    public function category(): string {
        return 'event';
    }

    public function level(): int {
        // timestamp of the news
        return $this->getDate();
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        return is_object($tile) && $tile->isPowerDetection() == false && $tile->isComputeMethod() == false && time() < ($this->getDate() + 7 * 86400);
    }

    public function actionOnEarn(?User $user_, ?Tile $tile, ?Session $session): void {
        $giftRepository = GlobalInject::getEntityManager()->getRepository(Gift::class);
        $config = GlobalInject::getConfigService()->getData();
        if (is_object($user_)) {
            $value = 2000;

            $gift = new Gift();
            $gift->setValue($value);
            $gift->setExpiration(time() + $config['gift']['duration']);
            $gift->setOwner($user_);
            $gift->setComment('Gift 2,000 points for a Frame (Event 500,000th project)');
            $gift = $giftRepository->generateAccess($gift);

            GlobalInject::getEntityManager()->persist($gift);
            GlobalInject::getEntityManager()->flush();

            $content_user = sprintf('Thanks for rendering a frame!<br>This week is a special week: 500,000 projects have been rendered on SheepIt.<br>To thanks everyone who have made this possible, a %d points gift is given to anyone who render a frame in next the few of days (offer end on %s).<br>The %d points have been given you to as a gift, you can use in by going to this address: <a href="%%%%GIFT_URL%%%%">use a gift</a>.<br><br>You can also find more a bout the 500,000th project event on the <a href="https://www.sheepit-renderfarm.com/news/%s">news page</a>.', $value, date('F, jS Y', $this->getDate() + 86400 * 7), $value, $this->getDate());
            $giftRepository->sendEmail($gift, $content_user);
        }
    }
}
