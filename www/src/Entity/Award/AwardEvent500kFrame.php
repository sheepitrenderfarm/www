<?php
/**
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent500kFrame extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1391644800;
    }

    public function getAuthor(): string {
        return 'Papa_Dragon';
    }

    public function getObjectiveLong(): string {
        return '500,000th';
    }

    public function getObjectiveShort(): string {
        return '500k';
    }
}