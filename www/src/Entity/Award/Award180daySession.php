<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award180daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 180;
    }

    public function reward(): int {
        return 1400000;
    }
}
