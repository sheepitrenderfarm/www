<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award1hRenderTime extends AwardRenderTime {

    public function getObjective(): int {
        return 3600;
    }

    public function reward(): int {
        return 1000;
    }

    public function humanDescription(): string {
        return sprintf('%s hour of render time', number_format($this->getObjective() / 3600));
    }
}
