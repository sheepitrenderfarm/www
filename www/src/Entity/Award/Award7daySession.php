<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award7daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 7;
    }

    public function reward(): int {
        return 50000;
    }


}
