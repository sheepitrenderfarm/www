<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardCheeseGiveway extends Award {

    public function imagePath(): string {
        return '/media/image/award/award_giveway_cheese.png';
    }

    public function category(): string {
        return 'misc';
    }

    public function reward(): int {
        return 0;
    }

    public function isDisplayable(): bool {
        return false;
    }

    public function humanDescription(): string {
        return "Cheese give way for a free render-day";
    }
}
