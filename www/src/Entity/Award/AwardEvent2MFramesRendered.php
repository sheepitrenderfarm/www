<?php
/**
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent2MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1414627200;
    }

    public function getAuthor(): string {
        return 'Pierre Allard';
    }

    public function getObjectiveLong(): string {
        return '2,000,000th';
    }

    public function getObjectiveShort(): string {
        return '2M';
    }
}
