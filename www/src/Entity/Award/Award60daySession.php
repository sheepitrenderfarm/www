<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award60daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 60;
    }

    public function reward(): int {
        return 400000;
    }
}
