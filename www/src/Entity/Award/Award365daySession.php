<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award365daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 365;
    }

    public function reward(): int {
        return 3365365;
    }
}