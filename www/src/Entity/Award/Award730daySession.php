<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award730daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 730;
    }

    public function reward(): int {
        return 2000000;
    }

    public function level(): int {
        return 3365366;
    }
}