<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award30daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 30;
    }

    public function reward(): int {
        return 200000;
    }
}
