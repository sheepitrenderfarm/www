<?php
/**
 * Copyright (C) 2013-2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardShareReview extends Award {
    public function canBeEarn(): bool {
        return true;
    }

    public function reward(): int {
        return 100000;
    }

    public function category(): string {
        return 'misc';
    }

    public function humanDescription(): string {
        return 'Make a tutorial or a review of SheepIt on YouTube or as an article (Contact us for activating this award)';
    }

    public function imagePath(): string {
        return '/media/image/award/award_made_review_tutorial.png';
    }
}
