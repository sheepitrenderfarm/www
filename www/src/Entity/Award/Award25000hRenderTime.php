<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award25000hRenderTime extends AwardRenderTime {

    public function getObjective(): int {
        return 90000000;
    }

    public function reward(): int {
        return 200000;
    }
}
