<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award5000hRenderTime extends AwardRenderTime {

    public function getObjective(): int {
        return 18000000;
    }

    public function reward(): int {
        return 125000;
    }
}
