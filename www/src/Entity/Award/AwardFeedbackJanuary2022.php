<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardFeedbackJanuary2022 extends Award {

    public function imagePath(): string {
        return '/media/image/award/award_feedback_january2022.png';
    }

    public function category(): string {
        return 'misc';
    }

    public function reward(): int {
        return 20000;
    }

    public function isDisplayable(): bool {
        return false;
    }

    public function humanDescription(): string {
        return "Thank you for give us feedback";
    }
}
