<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardMostConcurrentRenderingComputer extends Award {
    public function humanDescription(): string {
        return 'The user with the highest number of unique computers connected.<br>Each machine has to render at least 2 frames. Only one session per physical machine is counted.<br>Each day an award is given.';
    }

    public function reward(): int {
        return 10000;
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function imagePath(): string {
        return '/media/image/award/award_most_concurrent_rendering_computer.png';
    }

    public function category(): string {
        return 'best_renderer';
    }

    public function cronHourly(): void {
        $users = array();

        foreach (GlobalInject::getEntityManager()->getRepository(Session::class)->findAll() as $session) {
            if ($session->getBlocked() != 0) {
                continue;
            }

            if ($session->getRenderedFrames() < 2) { // 2 for compute method frame and power detection
                continue;
            }

            $user = $session->getUser()->getId();
            if (array_key_exists($user, $users) == false) {
                $users[$user] = array();
            }

            // merge all session from the same hostname together
            $users[$user][$session->gethwid()] = $session->gethwid();
        }

        $counts = array_map('count', $users);
        array_multisort($counts, SORT_DESC, $users);

        if (count($users) > 0) {
            reset($users);
            $current_first = key($users);
            $current_first_sessions = count($users[$current_first]);

            $old_first_sessions = -1;
            $old_first = '';
            $ranking = GlobalInject::getMain()->getPersistentCache()->get('ranking_AwardMostConcurrentRenderingComputer');
            if (is_array($ranking)) {
                $old_first = $ranking['login'];
                $old_first_sessions = $ranking['sessions'];
            }

            if ($current_first_sessions < $old_first_sessions) {
                $current_first_sessions = $old_first_sessions;
                $current_first = $old_first;
            }
            GlobalInject::getMain()->getPersistentCache()->set('ranking_AwardMostConcurrentRenderingComputer', ['login' => $current_first, 'sessions' => $current_first_sessions]);
        }
    }

    public function cronDaily(): void {
        $ranking = GlobalInject::getMain()->getPersistentCache()->get('ranking_AwardMostConcurrentRenderingComputer');
        if (is_array($ranking) && array_key_exists('login', $ranking) && array_key_exists('sessions', $ranking)) {
            $login = $ranking['login'];
            $user = GlobalInject::getEntityManager()->getRepository(User::class)->find($login);
            if (is_object($user)) {
                $user->giveAward($this->getShortName());
            }
        }
        GlobalInject::getMain()->getPersistentCache()->set('ranking_AwardMostConcurrentRenderingComputer', null);
    }
}
