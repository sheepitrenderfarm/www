<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Constant;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardFirstFrameRendered extends Award {
    public function humanDescription(): string {
        return 'One frame rendered.<br>The dummy frame which is used to check the capacity of the machine will not give you this award.';
    }

    public function reward(): int {
        return 1000;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($tile) == true && $tile->getStatus() == Constant::FINISHED) {
            return true;
        }

        if (is_object($user) == true && $user->getRenderedFrames() > 0) {
            return true;
        }

        return false;
    }

    public function imagePath(): string {
        return '/media/image/award/award_framerendered_1.png';
    }

    public function category(): string {
        return 'rendered_frames';
    }
}
