<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardBestTeam extends Award {
    public function canBeEarn(): bool {
        return true;
    }

    public function reward(): int {
        return 15000;
    }

    public function category(): string {
        return 'best_renderer';
    }

    public function humanDescription(): string {
        return 'You can earn this award by being part of the best team.<br>Calculation of the award is done once a day (at 12PM CEST/CET).';
    }

    public function imagePath(): string {
        return '/media/image/award/award_best_team.png';
    }

    public function cronDaily(): void {
        $stats = GlobalInject::getEntityManager()->getRepository(Team::class)->getTeamPointsOverDays(90);
        if (count($stats) > 0) {
            $best_stats = reset($stats);
            $best_team = GlobalInject::getEntityManager()->getRepository(Team::class)->find($best_stats['team']);
            if (is_object($best_team)) {
                foreach ($best_team->members() as $u) {
                    $u->giveAward('AwardBestTeam');
                }
            }
        }
    }
}
