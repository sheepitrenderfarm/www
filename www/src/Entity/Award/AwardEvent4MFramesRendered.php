<?php
/**
 * Copyright (C) 2013-2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent4MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1431610997;
    }

    public function getAuthor(): string {
        return 'Joel Bernis';
    }

    public function getObjectiveLong(): string {
        return '4,000,000th';
    }

    public function getObjectiveShort(): string {
        return '4M';
    }
}
