<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award2hSession extends Award {
    public function humanDescription(): string {
        return 'Have a client connected for 2 consecutive hours.';
    }

    public function reward(): int {
        return 2000;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($session) == true) {
            $now = time();
            $creationtime = $session->getCreationTime();
            if ($now - $creationtime > 7200) {
                return true;
            }
        }

        return false;
    }

    public function imagePath(): string {
        return '/media/image/award/award_sessionduration_2h.png';
    }

    public function category(): string {
        return 'session_duration';
    }
}
