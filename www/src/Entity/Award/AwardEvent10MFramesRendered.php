<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent10MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1457440382;
    }

    public function getAuthor(): string {
        return 'yolonline';
    }

    public function getObjectiveLong(): string {
        return '10,000,000th';
    }

    public function getObjectiveShort(): string {
        return '10M';
    }
}
