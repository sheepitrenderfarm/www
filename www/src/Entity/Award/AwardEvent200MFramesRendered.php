<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent200MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1643030012;
    }

    public function getAuthor(): string {
        return 'coda';
    }

    public function getObjectiveLong(): string {
        return '200,000,000th';
    }

    public function getObjectiveShort(): string {
        return '200M';
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($tile) && $tile->isPowerDetection() == false && $tile->isComputeMethod() == false) {
            return time() < 1643670000; // 2nd february
        }
        return false;
    }

    public function actionOnEarn(?User $user_, ?Tile $tile, ?Session $session): void {
        $giftRepository = GlobalInject::getEntityManager()->getRepository(Gift::class);
        $config = GlobalInject::getConfigService()->getData();
        if (is_object($user_)) {
            $value = 2000;

            $gift = new Gift();
            $gift->setValue($value);
            $gift->setExpiration(time() + $config['gift']['duration']);
            $gift->setOwner($user_);
            $gift->setComment('Gift 2,000 points for a Frame (Event 200 million)');
            $gift = $giftRepository->generateAccess($gift);

            GlobalInject::getEntityManager()->persist($gift);
            GlobalInject::getEntityManager()->flush();

            $content_user = sprintf('Thanks for rendering a frame!<br>This week is a special week, 200,000,000 frames have been rendered on SheepIt.<br>To thanks everyone who have made this possible, a %d points gift is given to anyone who render a frame in next the few of days (offer end on February 1st).<br>The %d points have been given you to as a gift, you can use in by going to this address: <a href="%%%%GIFT_URL%%%%">use a gift</a>.<br><br>You can also find more a bout the 200 million frames event on the <a href="https://www.sheepit-renderfarm.com/news/1643030012">news page</a>.', $value, $value);
            $giftRepository->sendEmail($gift, $content_user);
        }
    }
}
