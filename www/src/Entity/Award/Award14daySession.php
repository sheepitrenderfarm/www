<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award14daySession extends AwardConsecutiveDaysSession {

    public function getObjectiveInDays(): int {
        return 14;
    }

    public function reward(): int {
        return 100000;
    }
}
