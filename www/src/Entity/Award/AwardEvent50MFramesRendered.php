<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent50MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1512323871;
    }

    public function getAuthor(): string {
        return 'Tim Serafin';
    }

    public function getObjectiveLong(): string {
        return '50,000,000th';
    }

    public function getObjectiveShort(): string {
        return '50M';
    }
}
