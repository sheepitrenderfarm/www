<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent30MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1493234649;
    }

    public function getAuthor(): string {
        return 'Jellepostma';
    }

    public function getObjectiveLong(): string {
        return '30,000,000th';
    }

    public function getObjectiveShort(): string {
        return '30M';
    }
}
