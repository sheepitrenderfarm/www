<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award100FramesRendered extends AwardRenderedFrames {

    public function getObjective(): int {
        return 100;
    }

    public function reward(): int {
        return 2000;
    }
}
