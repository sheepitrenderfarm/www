<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award50hRenderTime extends AwardRenderTime {

    public function getObjective(): int {
        return 180000;
    }

    public function reward(): int {
        return 10000;
    }
}
