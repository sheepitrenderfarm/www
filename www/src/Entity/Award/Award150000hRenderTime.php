<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award150000hRenderTime extends AwardRenderTime {

    public function getObjective(): int {
        return 540000000;
    }

    public function reward(): int {
        return 750000;
    }
}
