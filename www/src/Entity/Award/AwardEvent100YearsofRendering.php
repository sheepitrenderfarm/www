<?php
/**
 * Copyright (C) 2013-2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent100YearsofRendering extends Award {
    public function humanDescription(): string {
        return 'Been part of celebration of 100 years of rendering, celebrated on January 7th, 2016.<br>To get the award, you simply had to render a frame around the date event<br>We regularly do events like this to thank everyone who keep SheepIt alives.<br><i>Award image by Exit Studio.</i>';
    }

    public function reward(): int {
        return 2000;
    }

    public function category(): string {
        return 'event';
    }

    public function imagePath(): string {
        return '/media/image/award/award_event_100years_rendering.png';
    }

    public function level(): int {
        // timestamp of the news
        return 1452205924;
    }
}
