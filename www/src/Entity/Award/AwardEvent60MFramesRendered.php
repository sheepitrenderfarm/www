<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent60MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1522330663;
    }

    public function getAuthor(): string {
        return 'Jan';
    }

    public function getObjectiveLong(): string {
        return '60,000,000th';
    }

    public function getObjectiveShort(): string {
        return '60M';
    }
}
