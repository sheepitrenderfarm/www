<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardFeatureImplemented extends Award {
    public function humanDescription(): string {
        return 'One of my feature requests has been added';
    }

    public function reward(): int {
        return 10000;
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        // will be manually activated by the admin
        return false;
    }


    public function imagePath(): string {
        return '/media/image/award/award_feature_added.png';
    }

    public function category(): string {
        return 'misc';
    }
}
