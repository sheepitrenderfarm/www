<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent150MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1605436605;
    }

    public function getAuthor(): string {
        return 'karlpoyzer';
    }

    public function getObjectiveLong(): string {
        return '150,000,000th';
    }

    public function getObjectiveShort(): string {
        return '150M';
    }
}
