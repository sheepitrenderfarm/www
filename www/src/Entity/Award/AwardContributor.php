<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardContributor extends Award {

    public function canBeEarn(): bool {
        return true;
    }

    public function reward(): int {
        return 10000;
    }

    public function category(): string {
        return 'misc';
    }

    public function humanDescription(): string {
        return 'You can earn this award by contributing to SheepIt by: <br>* making a donation<br>* lending us a shepherd <br>* finding a bug...';
    }

    public function imagePath(): string {
        return '/media/image/award/award_contributor.png';
    }
}
