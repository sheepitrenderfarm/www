<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Persistent\PersistentCache;
use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardRandomLootDrop extends Award {
    private const AWARDS_PER_DAY = 5;
    private const CACHE_KEY = 'AwardRandomLootDrop';

    public function humanDescription(): string {
        return sprintf('Random drop! This award will be globally dropped once a day to %d lucky people who have rendered a frame in the last 24 hours (not including blender test and power strength frame).', self::AWARDS_PER_DAY);
    }

    public function reward(): int {
        return 10000;
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function imagePath(): string {
        return '/media/image/award/award_randomlootdrop.png';
    }

    public function category(): string {
        return 'misc';
    }

    public function cronDaily(): void {
        $logins = GlobalInject::getMain()->getPersistentCache()->get(self::CACHE_KEY);
        if (is_array($logins) && count($logins) > 0) {
            for ($i = 0; $i < self::AWARDS_PER_DAY; $i++) {
                $key = array_rand($logins);
                $user = GlobalInject::getEntityManager()->getRepository(User::class)->find($logins[$key]);
                if (is_object($user)) {
                    $user->giveAward($this->getShortName());
                }
            }
        }
        GlobalInject::getMain()->getPersistentCache()->set(self::CACHE_KEY, []);
    }

    public static function addSession(PersistentCache $persistentCache, Session $session): void {
        $logins = $persistentCache->get(self::CACHE_KEY);
        if ($logins === false || $logins === null) {
            $logins = [];
        }

        $login = $session->getUser()->getId();
        $logins[$login] = $login;

        $persistentCache->set(self::CACHE_KEY, $logins);
    }
}
