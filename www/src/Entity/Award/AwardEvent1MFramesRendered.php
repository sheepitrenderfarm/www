<?php
/**
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent1MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1399766400;
    }

    public function getAuthor(): string {
        return 'Blackschmoll';
    }

    public function getObjectiveLong(): string {
        return '1,000,000th';
    }

    public function getObjectiveShort(): string {
        return '1M';
    }
}
