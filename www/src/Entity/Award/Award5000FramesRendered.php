<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award5000FramesRendered extends AwardRenderedFrames {

    public function getObjective(): int {
        return 5000;
    }

    public function reward(): int {
        return 4000;
    }
}
