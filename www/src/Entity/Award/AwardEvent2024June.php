<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use App\Utils\Mail;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent2024June extends Award {

    public function getDate(): int {
        return 1718964000;
    }

    public function getAuthor(): string {
        return 'Milllk_man';
    }

    public function canBeEarn(): bool {
        return true;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($tile) && $tile->isPowerDetection() == false && $tile->isComputeMethod() == false) {
            return time() < 1719439200; // June 27th
        }
        return false;
    }

    public function actionOnEarn(?User $user_, ?Tile $tile, ?Session $session): void {

        if (is_object($user_)) {
            $day = GlobalInject::getEntityManager()->getRepository(RenderDay::class)->fillUpCheeseHole($user_);
            GlobalInject::getEntityManager()->flush();

            if ($user_->noficationIsEnabled(User::NOTIFICATION_MASK_AWARD)) {
                $content_user = sprintf('Thanks for rendering a frame!<br>You earned the \'Community event for June 2024\' award!<br><img src="cid:%s" alt="%s" /><br>This week is a special week, we want to thank the amazing SheepIt community.<br>Do you like cheese with or without holes?<br> Here is a free render day to plug the hole on your render streak for the %s.<br><i style=\"font-size: 2.0rem;\">It might take up to 48h to update your max consecutive render day value.</i><br><br><i>You can unsubscribe to the notification by changing your notification level on your account page. Don\'t worry you will still get the reward even if you don\'t want to be notified.</i>', basename($this->imagePath()), basename($this->imagePath()), $day->format('F jS, Y'));

                Mail::sendamail($user_->getEmail(), 'You earned an award', $content_user, 'earned_award', array(__DIR__.'/../../../public/'.$this->imagePath()));
            }
        }

    }

    public function humanDescription(): string {
        return sprintf('Participated during the celebration of SheepIt community. To get the award, you simply had to render a frame around the date\'s event, on June 21th, 2024.<br>We regularly do events like this to thank everyone who keeps SheepIt alive.<br><i>Award image by %s.</i>', $this->getAuthor());
    }

    public function imagePath(): string {
        return '/media/image/award/award_event_2024_june.png';
    }


    public function reward(): int {
        return 0;
    }

    public function category(): string {
        return 'event';
    }

    public function level(): int {
        // timestamp of the news
        return $this->getDate();
    }
}
