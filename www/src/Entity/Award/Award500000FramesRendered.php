<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Award500000FramesRendered extends AwardRenderedFrames {

    public function getObjective(): int {
        return 500000;
    }

    public function reward(): int {
        return 80000;
    }
}
