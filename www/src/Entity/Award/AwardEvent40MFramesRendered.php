<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AwardEvent40MFramesRendered extends AwardEventFramesRendered {

    public function getDate(): int {
        return 1502207950;
    }

    public function getAuthor(): string {
        return 'Wayne Johnson from Hey Bear Productions';
    }

    public function getObjectiveLong(): string {
        return '40,000,000th';
    }

    public function getObjectiveShort(): string {
        return '40M';
    }
}
