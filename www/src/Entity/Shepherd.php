<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\ShepherdServerRepository;
use App\Service\GlobalInject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\LockMode;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Shepherd
 *
 *
 */
#[ORM\Table(name: 'server_shepherd')]
#[ORM\Entity(repositoryClass: ShepherdServerRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
#[ORM\DiscriminatorMap(['inter' => 'ShepherdInternal', 'mock' => 'ShepherdMock', 'real' => 'ShepherdReal'])]
#[ORM\HasLifecycleCallbacks]
abstract class Shepherd extends Entity {
    #[ORM\Column(name: 'id', type: Types::STRING, length: 100, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'enable', type: Types::BOOLEAN, nullable: false)]
    protected bool $enable = false;

    #[ORM\Column(name: 'timeout', type: Types::BOOLEAN, nullable: false)]
    protected bool $timeout = false;

    #[ORM\Column(name: 'allow_new_blend', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $allow_new_blend = true;

    #[ORM\Column(name: 'location', type: Types::STRING, length: 64, nullable: false)]
    protected string $location;

    #[ORM\Column(name: 'location_geographic', type: Types::STRING, length: 64, nullable: false)]
    protected string $location_geographic;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    protected User $owner;

    #[ORM\JoinColumn(name: 'maintainer', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    protected User $maintainer;

    #[ORM\Column(name: 'country_only', type: Types::STRING, length: 256, nullable: false)]
    protected string $country_only = '';

    #[ORM\Column(name: 'country_exclude', type: Types::STRING, length: 256, nullable: false)]
    protected string $country_exclude = '';

    #[ORM\Column(name: 'storage_max', type: Types::BIGINT, nullable: false)]
    protected int $storage_max = 0;

    /**
     * Number of rendered/validated tiles since last stats
     */
    #[ORM\Column(name: 'rendered_tiles', type: Types::INTEGER, nullable: false)]
    protected int $renderedTiles = 0;

    #[ORM\Column(name: 'strength', type: Types::INTEGER, nullable: false)]
    protected int $strength = 1;

    /**
     * @var Collection<StatsShepherd>
     */
    #[ORM\OneToMany(targetEntity: StatsShepherd::class, mappedBy: 'shepherd', cascade: ['persist'])]
    protected Collection $stats;

    public function __construct() {
        $this->stats = new ArrayCollection();
    }

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getEnable(): bool {
        return $this->enable;
    }

    public function setEnable(bool $enable): self {
        $this->enable = $enable;

        return $this;
    }

    public function getTimeout(): bool {
        return $this->timeout;
    }

    public function setTimeout(bool $timeout): self {
        $this->timeout = $timeout;

        return $this;
    }

    public function getAllowNewBlend(): bool {
        return $this->allow_new_blend;
    }

    public function setAllowNewBlend(bool $allowNewBlend): self {
        $this->allow_new_blend = $allowNewBlend;

        return $this;
    }

    public function getLocation(): ?string {
        return $this->location;
    }

    public function setLocation(string $location): self {
        $this->location = $location;

        return $this;
    }

    public function getLocationGeographic(): ?string {
        return $this->location_geographic;
    }

    public function setLocationGeographic(string $locationGeographic): self {
        $this->location_geographic = $locationGeographic;

        return $this;
    }

    public function getOwner(): User {
        return $this->owner;
    }

    public function setOwner(User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getMaintainer(): User {
        return $this->maintainer;
    }

    public function setMaintainner(User $maintainer): self {
        $this->maintainer = $maintainer;

        return $this;
    }

    public function getCountryOnly(): string {
        return $this->country_only;
    }

    public function setCountryOnly(string $countryOnly): self {
        $this->country_only = $countryOnly;

        return $this;
    }

    public function getCountryExclude(): string {
        return $this->country_exclude;
    }

    public function setCountryExclude(string $countryExclude): self {
        $this->country_exclude = $countryExclude;

        return $this;
    }

    public function getStorageMax(): int {
        return $this->storage_max;
    }

    public function setStorageMax(int $storageMax): self {
        $this->storage_max = $storageMax;

        return $this;
    }

    public function getRenderedTiles(): int {
        return $this->renderedTiles;
    }

    public function setRenderedTiles(int $renderedTiles): self {
        $this->renderedTiles = $renderedTiles;

        return $this;
    }

    public function getStrength(): int {
        return $this->strength;
    }

    public function setStrength(int $strength): self {
        $this->strength = $strength;

        return $this;
    }

    /**
     * @return Collection<StatsShepherd>
     */
    public function stats(): Collection {
        return $this->stats;
    }

    public function allowNewRenderingFrame(): bool {
        $repo = GlobalInject::getEntityManager()->getRepository(Shepherd::class);
        $stats = $repo->getLastStats($this);
        if (is_null($stats)) {
            return true;
        }
        if ($stats->getCpu() > $stats->getCpuMax()) {
            return false;
        }
        if ($stats->getRam() > $stats->getRamMax() * 0.85) {
            return false;
        }

        $config = GlobalInject::getConfigService()->getData();

        if ($this->getRenderingFrameCount() > $stats->getRenderingFrames() + $config['shepherd']['ramp_up']) {
            return false;
        }
        return true;
    }

    /**
     * Update in sql the RenderedTiles (inside a transaction)
     */
    public function updateRenderedTiles(int $update): void {
        $em = GlobalInject::getEntityManager();
        $em->wrapInTransaction(function () use ($em, $update) {
            $em->refresh($this, LockMode::PESSIMISTIC_WRITE);
            $this->renderedTiles = $this->renderedTiles + $update;
        });
    }

    abstract public function isEnabled(): bool;

    abstract public function addProject(Project $project, int $width_, int $height_): bool;

    abstract public function delProject(Project $project): bool;

    abstract public function tileSetRendering(Tile $tile): bool;

    abstract public function tileReset(Tile $tile): bool;

    abstract public function setBlendOwnerToken(Project $project): bool;

    abstract public function setBlendThumbnailToken(Project $project): bool;

    abstract public function setGenerateMP4(Project $project, bool $mp4): bool;

    abstract public function askForPartialFrameArchive(Project $project): bool;

    abstract public function taskReset(int $id): bool;

    abstract public function getTasks(): array;

    abstract public function getStorageUsed(Project $project): float;

    abstract public function getStoragePrediction(Project $project): float;

    abstract public function getVersion(): string;

    abstract public function removeOrphan(): bool;

    abstract public function updatePredictedProjectsSize(): bool;

    abstract public function updateStats(int $now): bool;

    abstract public function checkIfStillTimeout(): void;

    abstract public function getLastStats(): ?StatsShepherd;

    abstract public function getRenderingFrameCount(): int;

    abstract public function getBlendsCount(): int;

    abstract public function generateValidationUrl(Tile $tile): string;

    /**
     * @return bool|array{total_task: int, queue_position:int, tasks: int}
     */
    abstract public function getTaskQueuePosition(Project $project): array|bool;
}
