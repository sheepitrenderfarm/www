<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TaskUserRemove extends Task {

    public function execute(): bool {
        $repo = GlobalInject::getEntityManager()->getRepository(User::class);

        $user = $repo->find($this->getData());
        if (is_null($user)) {
            return true; // hide it...
        }
        $repo->remove($user);
        return true;
    }
}
