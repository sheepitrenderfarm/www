<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Generate stats for a project
 */
#[ORM\Entity]
class TaskProjectGenerateStats extends Task {

    public function execute(): bool {
        $project = GlobalInject::getEntityManager()->getRepository(Project::class)->find($this->getProject());
        if (is_object($project) == false) {
            return false;
        }

        $project->generateStatsCache();
        GlobalInject::getEntityManager()->flush();
        return true;
    }
}
