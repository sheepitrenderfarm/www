<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Constant;
use App\Service\GlobalInject;
use App\Service\Logger;
use Doctrine\ORM\Mapping as ORM;
use GuzzleHttp\Client;

#[ORM\Entity]
class TaskShepherdTransfer extends Task {

    public function execute(): bool {
        // inject job in new shepherd
        // get each tile
        // inject tile on new shepherd
        // remove job from old shepherd

        $project = GlobalInject::getEntityManager()->getRepository(Project::class)->find($this->getProject());
        if (is_null($project)) {
            Logger::error(__METHOD__.' failed to import project: '.$this->getProject());
            return false;
        }

        $project_old = clone $project;

        $old_shepherd = $project->getShepherd();
        $new_shepherd = GlobalInject::getEntityManager()->getRepository(Shepherd::class)->find($this->getData());

        if (is_null($new_shepherd)) {
            Logger::error(__METHOD__.' failed, no new shepherd');
            return false;
        }

        Logger::debug(__METHOD__.' old shepherd '.$old_shepherd->getId());
        Logger::debug(__METHOD__.' new shepherd '.$new_shepherd->getId());

        // reset token on old shepherd (and old clone project)
        $project_old->setShepherdOwnerTokenValidity(0);
        $project_old->setShepherdThumbnailTokenValidity(0);

        if ($old_shepherd->getTimeout() == false && $old_shepherd->setBlendOwnerToken($project_old) == false) {
            Logger::error(__METHOD__.' failed, can not setBlendOwnerToken on old shepherd');
            return false;
        }

        if ($new_shepherd->addProject($project, 0, 0) == false) {
            Logger::error(__METHOD__.' failed, can not add job on new shepherd');
            return false;
        }

        $project->setShepherd($new_shepherd);

        // reset token on new shepherd
        $project->setShepherdOwnerTokenValidity(0);
        $project->setShepherdThumbnailTokenValidity(0);
        if ($new_shepherd->setBlendOwnerToken($project) == false) {
            Logger::error(__METHOD__.' failed, can not setBlendOwnerToken on new shepherd');
            return false;
        }

        if ($new_shepherd->setBlendThumbnailToken($project) == false) {
            Logger::error(__METHOD__.' failed, can not setBlendOwnerToken on new shepherd');
            return false;
        }

        GlobalInject::getEntityManager()->flush();

        $client = new Client();
        foreach ($project->tilesWithStatus(Constant::FINISHED) as $f) {
            if ($old_shepherd->getTimeout() === false) {
                $old_url_tile = $old_shepherd->getId().'/blend/'.$project_old->getShepherdOwnerToken().'/'.$project->getId().'/tile/'.$f->getId().'/full';
                Logger::debug(__METHOD__.' old tile url '.$old_url_tile);

                $image = tempnam(sys_get_temp_dir(), '');
                $response = $client->request('GET', $old_url_tile, ['sink' => $image]);
                if ($response->getStatusCode() != 200) {
                    Logger::error(__METHOD__.' failed, can not get image on old shepherd');
                    return false;
                }
            }

            if ($new_shepherd->tileSetRendering($f) == false) {
                Logger::error(__METHOD__.' failed, can set rendering image on new shepherd');
                return false;
            }

            if ($old_shepherd->getTimeout() === false) {
                $new_url_tile = $new_shepherd->getId().'/renderer/tile?token='.$f->getToken().'&id='.$f->getId()."&rendertime=".$f->getRenderTime()."&memoryused=".$project->getMaxMemoryUsed();
                Logger::debug(__METHOD__.' pushing tile on new shepherd '.$new_url_tile);
                /** @phpstan-ignore-next-line because phpstan does not know that $old_shepherd->getTimeout() === false is not updated */
                $response = $client->request('POST', $new_url_tile, ['multipart' => [['name' => 'file', 'contents' => fopen($image, 'r')]]]);
                if ($response->getStatusCode() != 200) {
                    Logger::error(__METHOD__.' failed, can not send image on new shepherd');
                    return false;
                }

                /** @phpstan-ignore-next-line  because phpstan does not know that getTimeout() is not updated */
                unlink($image);
            }
        }

        if ($old_shepherd->getTimeout() == false) {
            $old_shepherd->delProject($project_old);
        }

        GlobalInject::getEntityManager()->flush();

        return true;
    }
}
