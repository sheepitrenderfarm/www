<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\CDN\CDNBucket;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\File;
use App\Utils\Misc;
use App\Utils\Zip;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TaskProjectUploadToMirror extends Task {

    public function execute(): bool {
        Logger::debug(__METHOD__.':'.__LINE__);


        $projectRepository = GlobalInject::getEntityManager()->getRepository(Project::class);
        $project = $projectRepository->find($this->getProject());
        if (is_object($project) && $this->getData() != '') {
            $password = Misc::code(32);

            $basename = tempnam(GlobalInject::getConfigService()->getData()['tmp_dir'], '');
            unlink($basename);
            $path_zip = $basename.'.zip';
            Zip::zipDir($path_zip, $this->getData(), $password);

            $projectRepository->createChunks($project, $path_zip);

            File::rmdirRecurse($this->getData());
            unlink($path_zip);

            $upload_success = false;
            $node = new CDNBucket();
            foreach ($project->getChunks() as $chunk) {
                $upload_success = $node->uploadFile($project->fullPathOfChunk($chunk));
            }
            $project->setSyncedBucket($upload_success);
            $project->setPassword($password);
            GlobalInject::getEntityManager()->flush($project);
        }

        return true;
    }
}
