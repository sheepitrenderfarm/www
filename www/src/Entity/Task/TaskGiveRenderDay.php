<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Give a render to every active users.
 */
#[ORM\Entity]
class TaskGiveRenderDay extends Task {

    public function execute(): bool {
        $userRepository = GlobalInject::getEntityManager()->getRepository(User::class);

        foreach (GlobalInject::getMain()->getBestRenderersFromCache() as $bestRenderer) {
            $u = $userRepository->find($bestRenderer->getLogin());
            if (is_object($u)) {
                GlobalInject::getEntityManager()->getRepository(RenderDay::class)->addFor($u);
            }
        }

        return true;
    }
}
