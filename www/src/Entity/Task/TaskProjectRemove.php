<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TaskProjectRemove extends Task {

    public function execute(): bool {
        $repo = GlobalInject::getEntityManager()->getRepository(Project::class);

        $project = $repo->find($this->getProject());
        if (is_null($project)) {
            return true; // hide it...
        }
        $repo->remove($project);
        return true;
    }
}
