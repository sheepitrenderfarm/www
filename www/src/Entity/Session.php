<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\CDN\CDN;
use App\CDN\CDNBucket;
use App\CDN\CDNInternal;
use App\Constant;
use App\Persistent\Cache;
use App\Repository\SessionRepository;
use App\Scheduler\ClientProtocol;
use App\Scheduler\Criterion\Criterion;
use App\Service\GlobalInject;
use App\Service\Image;
use App\Service\Logger;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Throwable;

/**
 * Session
 */
#[ORM\Table(name: 'session')]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'gpu', columns: ['gpu'])]
#[ORM\Entity(repositoryClass: SessionRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class Session extends SessionCoreInfos {

    public const SCHEDULER_DEFAULT = 'default';
    public const SCHEDULER_SLOW_COMPUTER = 'slow_computer';
    public const SCHEDULER_VERY_SLOW_COMPUTER = 'very_slow_computer';

    private Cache $previousRenderedProject;
    private ?array $cache_criterions = null;

    public const BLOCK_MESSAGES = array(
        1 => array('short' => 'test failed', 'long' => 'blender test frame failed'),
        2 => array('short' => 'Gpu unsupported', 'long' => 'Gpu not supported'),
        3 => array('short' => 'overprovisionning', 'long' => 'overprovisionning'),
        4 => array('short' => 'dead', 'long' => 'dead client'),
        5 => array('short' => 'low memory', 'long' => 'low memory setting'),
        6 => array('short' => 'python error', 'long' => 'python error'),
        7 => array('short' => 'MacOSX', 'long' => 'MacOSX 10.13 minimum'),
        8 => array('short' => 'collab', 'long' => 'Google collab CPU session should not be used'),
        9 => array('short' => 'denoising', 'long' => 'Denoising not supported'),
        10 => array('short' => 'file permission', 'long' => 'Client cannot create cache directory and file'),
        11 => array('short' => 'cachedir deleted', 'long' => 'Client has lost its cache dir, it is now dead'),
        99 => array('short' => 'Unknown', 'long' => 'Others/Unknown'));

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'sessions')]
    protected User $user;

    #[ORM\Column(name: 'blocked', type: Types::INTEGER, nullable: false)]
    protected int $blocked = 0;

    #[ORM\Column(name: 'running', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $running = true;

    #[ORM\Column(name: 'paused', type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    protected bool $paused = false;

    #[ORM\Column(name: 'sleeping', type: Types::BOOLEAN, nullable: false, options: ['default' => false])]
    protected bool $sleeping = false;

    #[ORM\Column(name: 'memory_allowed', type: Types::INTEGER, nullable: false)]
    protected int $memory_allowed = 0;

    #[ORM\Column(name: 'headless', type: Types::BOOLEAN, nullable: false)]
    protected bool $headless = false;

    #[ORM\Column(name: 'no_big_download', type: Types::BOOLEAN, nullable: false)]
    protected bool $no_big_download = false;

    #[ORM\Column(name: 'hostname', type: Types::STRING, length: 80, nullable: false)]
    protected string $hostname = '';

    #[ORM\Column(name: 'ip', type: Types::STRING, length: 39, nullable: false)]
    protected string $ip = '';

    /**
     * @var ?UserPublicKey
     */
    #[ORM\JoinColumn(name: 'renderkey', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: UserPublicKey::class)]
    private ?UserPublicKey $renderkey;

    #[ORM\Column(name: 'ui', type: Types::STRING, length: 32, nullable: false)]
    protected string $ui = '';

    #[ORM\Column(name: 'country', type: Types::STRING, length: 2, nullable: false)]
    protected string $country = '';

    #[ORM\Column(name: 'user_agent', type: Types::STRING, length: 50, nullable: false)]
    protected string $user_agent = '';

    #[ORM\Column(name: 'scheduler', type: Types::STRING, length: 20, nullable: false)]
    protected string $scheduler = '';

    #[ORM\Column(name: 'compute_method', type: Types::INTEGER, nullable: false, options: ['default' => '-1'])]
    protected int $compute_method = -1;

    #[ORM\Column(name: 'max_rendertime', type: Types::INTEGER, nullable: false)]
    protected int $max_rendertime = -1;

    #[ORM\Column(name: 'power_cpu', type: Types::INTEGER, nullable: false)]
    protected int $power_cpu = -1;

    #[ORM\Column(name: 'power_gpu', type: Types::INTEGER, nullable: false)]
    protected int $power_gpu = -1;

    #[ORM\Column(name: 'network_speed_download', type: Types::INTEGER, nullable: false)]
    protected int $network_speed_download = 0;

    #[ORM\Column(name: 'network_speed_upload', type: Types::INTEGER, nullable: false)]
    protected int $network_speed_upload = 0;

    #[ORM\Column(name: 'timestamp', type: Types::INTEGER, nullable: false)]
    protected int $timestamp = 0;

    public function getBlocked(): int {
        return $this->blocked;
    }

    public function setBlocked(int $blocked): self {
        $this->blocked = $blocked;

        if ($blocked != 0) { // 0 is "not blocked" / "running"
            $this->sendEmailOnBlocked($blocked);
        }

        return $this;
    }

    public function getRunning(): bool {
        return $this->running;
    }

    public function setRunning(bool $running): self {
        $this->running = $running;

        return $this;
    }

    public function getPaused(): bool {
        return $this->paused;
    }

    public function setPaused(bool $paused): self {
        $this->paused = $paused;
//
        return $this;
    }

    public function getSleeping(): bool {
        return $this->sleeping;
    }

    public function setSleeping(bool $sleeping): self {
        $this->sleeping = $sleeping;

        return $this;
    }

    /**
     * @param bool $addToForeignKey need to be set to false when using dummy session (session who is created but not persisted)
     */
    public function setUser(User $user, bool $addToForeignKey = true): self {
        $this->user = $user;
        // it should be automatic but not in doctrine 2 :(
        if ($addToForeignKey) {
            $user->__addSession($this);
        }

        $this->user = $user;

        return $this;
    }

    public function getMemoryAllowed(): int {
        return $this->memory_allowed;
    }

    public function setMemoryAllowed(int $memoryAllowed): self {
        $this->memory_allowed = $memoryAllowed;

        return $this;
    }

    public function getHeadless(): bool {
        return $this->headless;
    }

    public function setHeadless(bool $headless): self {
        $this->headless = $headless;

        return $this;
    }

    public function getNoBigDownload(): bool {
        return $this->no_big_download;
    }

    public function setNoBigDownload(bool $no_big_download): self {
        $this->no_big_download = $no_big_download;

        return $this;
    }

    public function getHostname(): string {
        return $this->hostname;
    }

    public function setHostname(string $hostname): self {
        $this->hostname = $hostname;

        return $this;
    }

    public function getIP(): string {
        return $this->ip;
    }

    public function setIP(string $ip): self {
        $this->ip = $ip;

        return $this;
    }

    public function getRenderKey(): ?UserPublicKey {
        return $this->renderkey;
    }

    public function setRenderKey(?UserPublicKey $renderKey): self {
        $this->renderkey = $renderKey;

        return $this;
    }

    public function getUi(): string {
        return $this->ui;
    }

    public function setUi(string $ui): self {
        $this->ui = $ui;

        return $this;
    }

    public function getCountry(): string {
        return $this->country;
    }

    public function setCountry(string $country): self {
        $this->country = $country;

        return $this;
    }

    public function getUserAgent(): string {
        return $this->user_agent;
    }

    public function setUserAgent(string $userAgent): self {
        $this->user_agent = $userAgent;

        return $this;
    }

    public function getScheduler(): string {
        return $this->scheduler;
    }

    public function setScheduler(string $scheduler): self {
        $this->scheduler = $scheduler;

        return $this;
    }

    public function getComputeMethod(): int {
        return $this->compute_method;
    }

    public function setComputeMethod(int $computeMethod): self {
        $this->compute_method = $computeMethod;

        return $this;
    }

    public function getMaxRendertime(): int {
        return $this->max_rendertime;
    }

    public function setMaxRendertime(int $maxRendertime): self {
        $this->max_rendertime = $maxRendertime;

        return $this;
    }

    public function getPowerCpu(): ?int {
        return $this->power_cpu;
    }

    public function setPowerCpu(int $powerCpu): self {
        $this->power_cpu = $powerCpu;

        return $this;
    }

    public function getPowerGpu(): ?int {
        return $this->power_gpu;
    }

    public function setPowerGpu(int $powerGpu): self {
        $this->power_gpu = $powerGpu;

        return $this;
    }

    public function getNetworkSpeedDownload(): int {
        return $this->network_speed_download;
    }

    public function setNetworkSpeedDownload(int $val): self {
        $this->network_speed_download = $val;

        return $this;
    }

    public function getNetworkSpeedUpload(): int {
        return $this->network_speed_upload;
    }

    public function setNetworkSpeedUpload(int $val): self {
        $this->network_speed_upload = $val;

        return $this;
    }

    public function getTimestamp(): int {
        return $this->timestamp;
    }

    public function setTimestamp(int $timestamp): self {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function __construct() {
        $this->init();
    }

    #[ORM\PostLoad]
    public function init(): void {
        $this->previousRenderedProject = new Cache();
    }

    public function getComputeDeviceStrHuman(): string {
        $cpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        if ($gpu == false) {
            $a_cpu = $this->getCPU();
            if (is_null($a_cpu)) {
                return 'Unknown CPU';
            }
            else {
                return $a_cpu->description();
            }
        }
        else {
            $a_gpu = $this->getGPU();
            if (is_null($a_gpu)) {
                return 'Unknown GPU';
            }
            return $a_gpu->description();
        }
    }

    public function getRenderingComputeDeviceStrHuman(Tile $tile): string {
        $cpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        $a_cpu = $this->getCPU();
        if ($gpu == false) {
            if (is_null($a_cpu)) {
                return 'Unknown CPU';
            }
            else {
                return $a_cpu->description();
            }
        }
        else {
            $project_computemethod = $tile->getFrame()->getProject()->getComputeMethod();
            // $project_cpu = Misc::isMaskEnabled($project_computemethod, Constant::COMPUTE_CPU);
            $project_gpu = Misc::isMaskEnabled($project_computemethod, Constant::COMPUTE_GPU);
            if ($project_gpu == false) {
                if (is_null($a_cpu)) {
                    return 'Unknown CPU';
                }
                else {
                    return $a_cpu->description();
                }
            }

            $a_gpu = $this->getGPU();
            if (is_null($a_gpu)) {
                return 'Unknown GPU';
            }
            return $a_gpu->description();
        }
    }

    public function getComputeDeviceMemoryInMB(): float {
        $cpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        if ($gpu == false) {
            return $this->getMemoryAllowed2() / 1024;
        }
        else {
            $a_gpu = $this->getGPU();
            if (is_object($a_gpu)) {
                return $a_gpu->getMemory() / 1024 / 1024;
            }
        }
        return 0;
    }

    public function getComputeDevice(): ?BaseProcessingUnit {
        $cpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        if ($gpu == false) {
            $device = $this->getCPU();
            if (is_object($device)) {
                $device->setPower($this->getPowerCpu());
            }
            return $device;
        }
        else {
            $device = $this->getGPU();
            if (is_object($device)) {
                $device->setPower($this->getPowerGpu());
            }
            return $device;
        }
    }

    public function getComputeDeviceFor(int $computeMethod): ?BaseProcessingUnit {
        if (Misc::isMaskEnabled($computeMethod, Constant::COMPUTE_CPU)) {
            $device = $this->getCPU();
            if (is_object($device)) {
                $device->setPower($this->getPowerCpu());
            }
        }
        else {
            $device = $this->getGPU();
            if (is_object($device)) {
                $device->setPower($this->getPowerGpu());
            }
        }

        return $device;
    }

    public function getHumanPower(): string {
        $cpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        $gpu = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        if ($gpu == false) {
            $device = $this->getCPU();
            if (is_object($device)) {
                return $this->getHumanPowerCPU();
            }
        }
        else {
            $device = $this->getGPU();
            if (is_object($device)) {
                return $this->getHumanPowerGPU();
            }
        }
        return $this->getHumanPowerCPU();
    }

    public function getHumanPowerCPU(): string {
        return sprintf("%.2d %%", 100 * $this->getPowerCpu() / GlobalInject::getConfigService()->getData()['power']['cpu']['const']);
    }

    public function getHumanPowerGPU(): string {
        return sprintf("%.2d %%", 100 * $this->getPowerGpu() / GlobalInject::getConfigService()->getData()['power']['gpu']['const']);
    }

    public function getMemoryAllowed2(): ?int {
        if ($this->getMemoryAllowed() > 0) {
            return $this->getMemoryAllowed();
        }
        return $this->getMemory();
    }

    /**
     * @return Tile[]
     */
    public function tiles(): array {
        return GlobalInject::getEntityManager()->getRepository(Tile::class)->findBy(['session' => $this->getId()]);
    }

    /**
     * @return Tile[]
     */
    public function tilesWithStatus(?string $status_ = null): array {
        return GlobalInject::getEntityManager()->getRepository(Tile::class)->findBy(['session' => $this->getId(), 'status' => $status_]);
    }

    /**
     * Update the timestamp of the session to the current time
     **/
    public function keepMeAlive(): void {
        $this->setTimestamp(time());
        GlobalInject::getEntityManager()->flush($this);
    }

    /**
     * return list(code, remaining_frame, renderable_jobs)
     */
    public function requestJob(SessionInterface $phpSession): array {
        $config = GlobalInject::getConfigService()->getData();

        $remaining_frames = 0;
        $renderable_jobs = 0;

        $user = $this->getUser();
        if ($user->canDoRendering() == false) {
            Logger::debug('User \''.$this->getUser()->getId().'\' has no right to ask for rendering');
            return array(ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER, $remaining_frames, $renderable_jobs);
        }

        $this->setLastrequestjob(time());
        GlobalInject::getEntityManager()->flush($this);

        if ($this->getBlocked() != 0) {
            Logger::debug('Session '.$this->getId().' of user \''.$this->getUser()->getId().'\' has been disabled');
            return array(ClientProtocol::JOB_REQUEST_ERROR_SESSION_DISABLED, $remaining_frames, $renderable_jobs); // session not enable (no job)
        }

        if ($this->systemIsOverLoad()) {
            Logger::warning('Session::requestJob return no job because the system is overloaded');
            return array(ClientProtocol::JOB_REQUEST_SERVER_OVERLOADED, 0, 0); // no job
        }

        if ($config['scheduler']['max_concurrent_frames'] > 0) {
            $count_rendering_frames = count(GlobalInject::getMain()->frames(Constant::PROCESSING));
            if ($count_rendering_frames > $config['scheduler']['max_concurrent_frames']) {
                Logger::debug(__METHOD__.' return no job because already have '.$config['scheduler']['max_concurrent_frames'].' rendering frames');
                return array(ClientProtocol::JOB_REQUEST_SERVER_OVERLOADED, 0, 0); // no job
            }
        }

        $gpuIsEnabled = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
        if ($this->getPowerGpu() <= 0 && $gpuIsEnabled) { // power detection on every new session
            Logger::debug(__METHOD__.':'.__LINE__.' no power set on gpu, get one');
            $frame = current(GlobalInject::getEntityManager()->getRepository(Project::class)->find(Project::POWER_DETECTION_PROJECT_ID)->tiles());
            return array($frame, $remaining_frames, 0, 0);
        }

        $cpuIsEnabled = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
        if ($this->getPowerCpu() <= 0 && $cpuIsEnabled) { // power detection on every new session
            Logger::debug(__METHOD__.':'.__LINE__.' no power set on cpu, get one');
            $frame = current(GlobalInject::getEntityManager()->getRepository(Project::class)->find(Project::POWER_DETECTION_PROJECT_ID)->tiles());
            Logger::debug(__METHOD__.':'.__LINE__.' frame: '.$frame);
            return array($frame, $remaining_frames, 0, 0);
        }

        // in perfect world, we should do this, but it takes too much time
        // $remaining_frames = GlobalInject::getEntityManager()->getRepository(Tile::class)->countAvailableFramesForUser($user);
        $remaining_frames = GlobalInject::getEntityManager()->getRepository(Tile::class)->countPublicRemainingFrames();

        if (count($this->tilesWithStatus(Constant::PROCESSING)) >= $config['session']['max_concurrent_rendering_frame']) {
            // too much rendering frames, most likely the session have an issue
            return array(ClientProtocol::JOB_REQUEST_NOJOB, $remaining_frames, $renderable_jobs); // no job
        }

        $projects = $user->getAvailableProjects($this->compute_method, $this->getMemoryAllowed2());

        foreach ($projects as $k => $a_project) {
            $score = $a_project->criterionScore($phpSession, $this);
            if ($score >= 0) { // if the score is less than zero -> refuse the job
                $a_project->score = $score;
            }
            else {
                unset($projects[$k]);
//                 Logger::debug(__method__.' remove score '.$score.' '.$a_job);
            }
        }
        usort($projects, '\App\Utils\Sort::sortByAttributeScoreINV');

        $renderable_jobs = count($projects);

        while (count($projects) > 0) {
            $best_job = array_pop($projects);
            Logger::debug(__METHOD__.' $best_job '.$best_job);

            $tile = $this->takeJobIfGoodEnough($best_job);
            if (is_object($tile)) {
                return array($tile, $remaining_frames, $renderable_jobs);
            }
            else {
                Logger::debug(__METHOD__.' failed to take job');
            }
        }

        Logger::debug(__METHOD__.' no job for session id:'.$this->getId().' login:'.$this->getUser()->getId());
        return array(ClientProtocol::JOB_REQUEST_NOJOB, $remaining_frames, 0); // no job
    }

    protected function takeJobIfGoodEnough(?Project $project): Tile|int|null {
        $config = GlobalInject::getConfigService()->getData();

        if (is_null($project)) {
            Logger::error(__METHOD__." project is null");
            return ClientProtocol::JOB_REQUEST_NOJOB;
        }

        // check if the renderer file exists
        $path = $config['storage']['path'].'binaries'.'/'.strtolower($project->getExecutable()->getId().'_'.$this->getOS().'_64bit').'.'.$config['archive']['extension'];
        if (is_file($path) == false) {
            Logger::debug(__METHOD__." the renderer of job $project does not exist ($path is missing)");
            return null;
        }

        // has chunks ? if chunks is empty it's a upload zero size project
        if (count($project->getChunks()) == 0) {
            Logger::debug(__METHOD__.' '.$project->getId().' has no chunks');
            return null;
        }

        $tile = GlobalInject::getEntityManager()->getRepository(Tile::class)->findOneWithStatusFor($project, Constant::WAITING);
        if (is_null($tile)) {
            Logger::debug(__METHOD__." no tile found");
            return ClientProtocol::JOB_REQUEST_NOJOB;
        }

        $ret = $tile->take($this);
        if ($ret == -3) {
            // lock of take failed
            Logger::error("Session::takeJobIfGoodEnough take failed");
            return null;
        }
        elseif ($ret != 0) {
            Logger::debug('Session::takeJobIfGoodEnough (from session='.$this->getId().') => cannot take '.$tile.' (ret '.serialize($ret).')');
            return ClientProtocol::JOB_REQUEST_ERROR_INTERNAL_ERROR;
        }
        else {
            return $tile;
        }
    }

    public function validateFrame(Tile $frame, string $tmp_file, SessionInterface $phpSession): int {
        if ($frame->isComputeMethod() == false && $frame->isPowerDetection() == false) {
            // main server can only validate settings frames, not actual rendering

            // A user should never validate a frame directly on www. A project should always be on a shepherd.
            // If it does it, it's due to an empty shepherd field bug
//            $daemon = new Daemon();
//            $task = new TaskSceneFixShepherd();
//            $task->setScene($frame->getScene()->getId());
//            $daemon->addTask($task);

            @unlink($tmp_file);
            Logger::error(__METHOD__.' failed, try to validate an actual tile');
            return -60;
        }

        $a_image = new Image($tmp_file);
//         if ($a_image->isImage() == false) {
//             return -45;
//         }

        if (is_object($this->getGPU()) && Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU) == false && filesize($tmp_file) < 50000000 && $a_image->isFullyBlack()) {
            Logger::info('session id:'.$this->getId().' login:'.$this->getUser()->getId().' compute_method:'.$this->getComputeMethod().' have render a black image '.$frame);
            @unlink($tmp_file);
            if ($frame->isComputeMethod() || $frame->isPowerDetection()) {
                return ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE;
            }
            else {
                $frame->getFrame()->getProject()->addBanforCriterionBanJobFromUserSendError($phpSession);
                return ClientProtocol::INTERVAL_VALUE;
            }
        }
        if ($frame->getRenderTime() < 60) {
            // if a frame is rendered very quietly, it might due to the "default scene" issue.
            $default_project_frames = array(
                __DIR__.'/../../public/media/image/default_project/linux_279.png',
                __DIR__.'/../../public/media/image/default_project/linux_278c.png',
                __DIR__.'/../../public/media/image/default_project/windows_279.png',
                __DIR__.'/../../public/media/image/default_project/windows_278c.png',
                __DIR__.'/../../public/media/image/default_project/default_full_dallas.png',
                __DIR__.'/../../public/media/image/default_project/default_full_yoshi.png',
                __DIR__.'/../../public/media/image/default_project/full.png',
                __DIR__.'/../../public/media/image/default_project/default_full_delta.png');
            foreach ($default_project_frames as $image_path) {
                if ($a_image->isSimilarTo($image_path)) {
                    Logger::info('session id:'.$this->getId().' login:'.$this->getUser()->getId().' compute_method:'.$this->getComputeMethod().' has render the default project -> broken machine (frame: '.$frame.') image default: '.$image_path);
                    $frame->reset();

                    $job = $frame->getFrame()->getProject();
                    $job->addBanforCriterionBanJobFromUserSendError($phpSession);

                    // either the machine is broken or the scene is actually the default scene

                    $project = $frame->getFrame()->getProject();
                    if (count($project->tilesWithStatus(Constant::FINISHED)) == 0) {
                        // most likely a default scene
                        // an error to be automatically disabled later
                        $job->setUserError($job->getUserError() + 1);
                        return ClientProtocol::OK;
                    }
                    else {
                        $this->setBlocked(2);
                        GlobalInject::getEntityManager()->flush($this);
                        return ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE;
                    }
                }
            }
        }
        $ret = $frame->validate($tmp_file, $this);
        if ($ret == 0) {
            $this->user->updateAwards($frame, $this);
        }
        return $ret;
    }

    /**
     * @return array of project id
     */
    public function getPreviousRenderedProjects(): array {
        if ($this->previousRenderedProject->isFilled() == false) {
            foreach ($this->tilesWithStatus(Constant::FINISHED) as $tile) {
                // in theory, a Frame should never be null. If there is a desync of requests, it will happen
                try {
                    /** @phpstan-ignore-next-line */
                    if (is_object($tile->getFrame()) && is_object($tile->getFrame()->getProject())) {
                        $this->previousRenderedProject->add($tile->getFrame()->getProject()->getId(), $tile->getFrame()->getProject()->getId());
                    }
                }
                catch (Throwable $e) {
                    Logger::critical(__METHOD__.' frame is NULL!  tile(id:'.$tile->getId().')');
                    Logger::critical(__METHOD__.' frame is NULL!  tile '.serialize($tile));
                }
            }

            $this->previousRenderedProject->setFill(true);
        }
        return $this->previousRenderedProject->getAll();
    }

    /**
     * @param bool $strict do not give render day for the day of the end of session (a day without the 8h of render)
     */
    public function updateOwnerConsecutiveRenderDays(bool $strict = false): void {
        if ($this->getBlocked() > 0) {
            return;
        }

        $end = time();
        $start = $this->getCreationTime();

        if ($end - $start > AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY * 3600) {
            $delta = $strict ? AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY * 3600 : 0;
            $user = $this->getUser();

            // session
            for ($i = $start; $i <= $end - $delta; $i += AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY * 3600) {
                GlobalInject::getEntityManager()->getRepository(RenderDay::class)->add($user, $i);
            }

            GlobalInject::getEntityManager()->getRepository(User::class)->updateConsecutiveRenderDays($user);
        }
    }

    public function setExtras(string $value): void {
        $config = GlobalInject::getConfigService()->getData();
        if ($value !== '') {
            if (str_starts_with($value, 'scheduler:')) {
                $val = substr($value, strlen('scheduler:'));
                if (array_key_exists($val, $config['scheduler']['criterion'])) {
                    $this->setScheduler($val);
                }
            }
        }
    }

    /**
     * @return Criterion[]
     */
    public function criterions(): array {
        if (is_null($this->cache_criterions)) {
            $config = GlobalInject::getConfigService()->getData();

            $objs = array();

            if ($this->getScheduler() == '') {
                $power = 0;
                // $cpuIsEnabled = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_CPU);
                $gpuIsEnabled = Misc::isMaskEnabled($this->getComputeMethod(), Constant::COMPUTE_GPU);
                if ($gpuIsEnabled) {
                    $power = $this->getPowerGpu();
                }
                else {
                    $power = $this->getPowerCpu();
                }
                if ($power > 0) {
                    if ($power < $config['power']['threshold_very_slow_computer']) {
                        // it's a VERY slow machine
                        $this->setScheduler(Session::SCHEDULER_VERY_SLOW_COMPUTER);
                        GlobalInject::getEntityManager()->flush($this);
                    }
                    elseif ($power < $config['power']['threshold_slow_computer']) {
                        // it's a slow machine
                        $this->setScheduler(Session::SCHEDULER_SLOW_COMPUTER);
                        GlobalInject::getEntityManager()->flush($this);
                    }
                }
            }

            if (array_key_exists($this->getScheduler(), $config['scheduler']['criterion']) == false) {
                $scheduler = $config['scheduler']['criterion'][Session::SCHEDULER_DEFAULT];
            }
            else {
                $scheduler = $config['scheduler']['criterion'][$this->getScheduler()];
            }

            foreach ($config['scheduler']['criterion']['core'] as $class_name => $weight) {
                $class1 = "\\App\\Scheduler\\Criterion\\".$class_name;
                $objs [] = new $class1(GlobalInject::getRouter(), GlobalInject::getEntityManager(), GlobalInject::getConfigService()->getData(), $weight);
            }

            foreach ($scheduler as $class_name => $weight) {
                $class2 = "\\App\\Scheduler\\Criterion\\".$class_name;
                $objs [] = new $class2(GlobalInject::getRouter(), GlobalInject::getEntityManager(), GlobalInject::getConfigService()->getData(), $weight);
            }

            $this->cache_criterions = $objs;
        }

        return $this->cache_criterions;
    }

    public function canDoAccurateScheduler(): bool {
        $config = GlobalInject::getConfigService()->getData();
        $load = sys_getloadavg(); // last 1, 5 and 15 minutes.
        return (float)($load[1]) < (float)($config['scheduler']['trigger_load']);
    }

    public function systemIsOverLoad(): bool {
        $config = GlobalInject::getConfigService()->getData();
        $load = sys_getloadavg(); // last 1, 5 and 15 minutes.
        return (float)($load[1]) > (float)($config['scheduler']['trigger_overload']);
    }

    public function getCDN(CDNSyncEntity $entity): CDN {
        // do not ask the cdn for data, only www will upload file to cdn, so we already know if it's been uploaded or not

        return $entity->getSyncedBucket() ? new CDNBucket() : new CDNInternal();

        // OLD
        // $external = new CDNProjectBucket();
        // return $external->fileExists($path) ? $external : new CDNProjectInternal();
    }

    public function sendEmailOnBlocked(int $reason): void {
        if ($this->getUser()->noficationIsEnabled(User::NOTIFICATION_MASK_SESSION_BLOCKED)) {
            $email_subject = 'Your render session has been blocked';
            $email_contents = 'Hello,<br>';
            $email_contents .= "<p>You render session has been blocked due to: ".Session::BLOCK_MESSAGES[$reason]['long'];
            $email_contents .= '<br>';
            $email_contents .= 'More info on the <a href="'.GlobalInject::getConfigService()->getData()['site']['url'].GlobalInject::getRouter()->generate('app_session_single', ['sessionid' => $this->getId()]).'">session page</a>.';
            $email_contents .= '</p>';

            $email_contents .= '<br><br><p>';
            $email_contents .= '<i>You can unsubscribe to the notification on this link: <a href="'.$this->getUser()->getGlobalUnsubscribeUrl().'">unsubscribe</a></i>';
            $email_contents .= '</p>';

            Mail::sendamail($this->getUser()->getEmail(), $email_subject, $email_contents, 'sheepit');
        }
    }
}
