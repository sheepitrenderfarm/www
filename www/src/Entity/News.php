<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\NewsRepository;
use App\Utils\Misc;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * News
 */
#[ORM\Table(name: 'news')]
#[ORM\Entity(repositoryClass: NewsRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class News extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $id;

    #[ORM\Column(name: 'title', type: Types::STRING, length: 200, nullable: false)]
    protected string $title;

    #[ORM\Column(name: 'image', type: Types::STRING, length: 1024, nullable: false)]
    protected string $image;

    #[ORM\Column(name: 'content', type: Types::TEXT, nullable: false)]
    protected string $content;

    #[ORM\Column(name: 'homepage', type: Types::BOOLEAN, nullable: false)]
    protected bool $homepage;

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setTitle(string $title): self {
        $this->title = $title;

        return $this;
    }

    public function getImage(): string {
        return $this->image;
    }

    public function setImage(string $image): self {
        $this->image = $image;

        return $this;
    }

    public function getContent(): string {
        return $this->content;
    }

    public function setContent(string $content): self {
        $this->content = $content;

        return $this;
    }

    public function getHomepage(): bool {
        return $this->homepage;
    }

    public function setHomepage(bool $homepage): self {
        $this->homepage = $homepage;

        return $this;
    }

    public function getSumUp(): string {
        return Misc::stringLimit(strip_tags($this->content), 100);
    }
}
