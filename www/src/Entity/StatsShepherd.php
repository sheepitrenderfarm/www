<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsShepherdRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsShepherd
 */
#[ORM\Table(name: 'stats_shepherd')]
#[ORM\Index(name: 'shepherd', columns: ['shepherd'])]
#[ORM\UniqueConstraint(columns: ['time', 'shepherd'])]
#[ORM\Entity(repositoryClass: StatsShepherdRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsShepherd extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    #[ORM\JoinColumn(name: 'shepherd', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: Shepherd::class, inversedBy: 'stats', cascade: ['persist'])]
    protected Shepherd $shepherd;

    #[ORM\Column(name: 'cpu', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $cpu;

    #[ORM\Column(name: 'cpu_max', type: Types::INTEGER, nullable: false)]
    protected int $cpu_max;

    #[ORM\Column(name: 'cpu_pressure', type: Types::FLOAT, precision: 10, scale: 0)]
    protected float $cpu_pressure = 0.0;

    #[ORM\Column(name: 'ram', type: Types::BIGINT, nullable: false)]
    protected int $ram;

    #[ORM\Column(name: 'ram_max', type: Types::BIGINT, nullable: false)]
    protected int $ram_max;

    #[ORM\Column(name: 'disk', type: Types::BIGINT, nullable: false)]
    protected int $disk;

    #[ORM\Column(name: 'disk_pressure', type: Types::FLOAT, precision: 10, scale: 0)]
    protected float $disk_pressure = 0.0;

    #[ORM\Column(name: 'disk_prediction', type: Types::BIGINT, nullable: false)]
    protected int $disk_prediction = 0;

    #[ORM\Column(name: 'network_rx', type: Types::BIGINT, nullable: false)]
    protected int $network_rx;

    #[ORM\Column(name: 'network_tx', type: Types::BIGINT, nullable: false)]
    protected int $network_tx;

    #[ORM\Column(name: 'httpd', type: Types::INTEGER, nullable: false)]
    protected int $httpd;

    #[ORM\Column(name: 'task', type: Types::INTEGER, nullable: false)]
    protected int $task;

    #[ORM\Column(name: 'blends', type: Types::INTEGER, nullable: false)]
    protected int $blends;

    #[ORM\Column(name: 'rendering_frames', type: Types::INTEGER, nullable: false)]
    protected int $renderingFrames;

    /**
     * Number of rendered/validated tiles since last stats
     */
    #[ORM\Column(name: 'rendered_tiles', type: Types::INTEGER, nullable: false)]
    protected int $renderedTiles;

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function setShepherd(Shepherd $shepherd): self {
        $this->shepherd = $shepherd;

        return $this;
    }

    public function getShepherd(): Shepherd {
        return $this->shepherd;
    }

    public function getCpu(): float {
        return $this->cpu;
    }

    public function setCpu(float $cpu): self {
        $this->cpu = $cpu;

        return $this;
    }

    public function getCpuMax(): int {
        return $this->cpu_max;
    }

    public function setCpuMax(int $cpu_max): self {
        $this->cpu_max = $cpu_max;

        return $this;
    }

    public function getCpuPressure(): float {
        return $this->cpu_pressure;
    }

    public function setCpuPressure(float $cpu_pressure): self {
        $this->cpu_pressure = $cpu_pressure;

        return $this;
    }

    public function getRam(): int {
        return $this->ram;
    }

    public function setRam(int $ram): self {
        $this->ram = $ram;

        return $this;
    }

    public function getRamMax(): int {
        return $this->ram_max;
    }

    public function setRamMax(int $ram): self {
        $this->ram_max = $ram;

        return $this;
    }

    public function getDisk(): int {
        return $this->disk;
    }

    public function setDisk(int $disk): self {
        $this->disk = $disk;

        return $this;
    }

    public function getDiskPressure(): float {
        return $this->disk_pressure;
    }

    public function setDiskPressure(float $disk_pressure): self {
        $this->disk_pressure = $disk_pressure;

        return $this;
    }

    public function getDiskPrediction(): int {
        return $this->disk_prediction;
    }

    public function setDiskPrediction(int $disk): self {
        $this->disk_prediction = $disk;

        return $this;
    }

    public function getNetworkRX(): int {
        return $this->network_rx;
    }

    public function setNetworkRX(int $network): self {
        $this->network_rx = $network;

        return $this;
    }

    public function getNetworkTX(): int {
        return $this->network_tx;
    }

    public function setNetworkTX(int $network): self {
        $this->network_tx = $network;

        return $this;
    }

    public function getHttpd(): ?int {
        return $this->httpd;
    }

    public function setHttpd(int $httpd): self {
        $this->httpd = $httpd;

        return $this;
    }

    public function getTask(): int {
        return $this->task;
    }

    public function setTask(int $task): self {
        $this->task = $task;

        return $this;
    }

    public function getBlends(): int {
        return $this->blends;
    }

    public function setBlends(int $blends): self {
        $this->blends = $blends;

        return $this;
    }

    public function getRenderingFrames(): int {
        return $this->renderingFrames;
    }

    public function setRenderingFrames(int $renderingFrames): self {
        $this->renderingFrames = $renderingFrames;

        return $this;
    }

    public function getRenderedTiles(): int {
        return $this->renderedTiles;
    }

    public function setRenderedTiles(int $renderedTiles): self {
        $this->renderedTiles = $renderedTiles;

        return $this;
    }
}