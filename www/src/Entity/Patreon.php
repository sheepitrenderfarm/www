<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\PatreonRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Patreon
 */
#[ORM\Table(name: 'patreon')]
#[ORM\Entity(repositoryClass: PatreonRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Patreon extends Entity {
    public const REWARD_PER_MONTH = 50000;
    #[ORM\Column(name: 'id', type: Types::STRING, length: 100, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'last_charge', type: Types::STRING, length: 30, nullable: false)]
    protected string $lastCharge;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    protected ?User $user = null;

    /**
     * Date on when the email linking the patreon email to SheepIt account has been sent.
     */
    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: true)]
    protected ?DateTimeInterface $email_sent = null;

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getLastCharge(): ?string {
        return $this->lastCharge;
    }

    public function setLastCharge(string $lastCharge): self {
        $this->lastCharge = $lastCharge;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getEmailSent(): ?DateTimeInterface {
        return $this->email_sent;
    }

    public function setEmailSent(?DateTimeInterface $email_sent): self {
        $this->email_sent = $email_sent;

        return $this;
    }
}
