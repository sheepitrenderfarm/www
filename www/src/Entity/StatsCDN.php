<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsCDNRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsMirror
 */
#[ORM\Table(name: 'stats_cdn')]
#[ORM\Index(name: 'date', columns: ['date'])]
#[ORM\Index(name: 'url', columns: ['url'])]
#[ORM\Entity(repositoryClass: StatsCDNRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsCDN extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: false)]
    protected \DateTimeInterface $date;

    #[ORM\Column(name: 'url', type: Types::STRING, length: 128, nullable: false)]
    protected string $url;

    #[ORM\Column(name: 'download', type: Types::BIGINT, nullable: false)]
    protected int $download;

    #[ORM\Column(name: 'total', type: Types::BIGINT, nullable: false)]
    protected int $total;

    public function getId(): int {
        return $this->id;
    }

    public function getDate(): \DateTimeInterface {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self {
        $this->date = $date;

        return $this;
    }

    public function getUrl(): string {
        return $this->url;
    }

    public function setUrl(string $url): self {
        $this->url = $url;

        return $this;
    }

    public function getDownload(): int {
        return $this->download;
    }

    public function setDownload(int $download): self {
        $this->download = $download;

        return $this;
    }

    public function getTotal(): int {
        return $this->total;
    }

    public function setTotal(int $total): self {
        $this->total = $total;

        return $this;
    }
}
