<?php
/**
 * Copyright (C) 2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\LiaisonRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Liaison
 */
#[ORM\Table(name: 'liaison')]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Index(name: 'value', columns: ['value'])]
#[ORM\Index(name: 'primary_2', columns: ['primary'])]
#[ORM\Index(name: 'secondary', columns: ['secondary'])]
#[ORM\Entity(repositoryClass: LiaisonRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Liaison extends Entity {
    public const USER_MANAGE_PROJECT = "UserManageScene";
    public const USER_RENDER_PROJECT = "UserRenderScene";
    public const TEAM_RENDER_PROJECT = "TeamRenderScene";
    public const USER_CHANGE_EMAIL_KEY = 'UserChangeEmailKey';
    public const TEAM_ASK_JOIN = 'TeamAskJoin';
    public const BLOCKED_RENDERER = 'BlockedRenderer';
    public const BLOCKED_OWNER = 'BlockedOwner';
    public const HIGH_PRIORITY_USER = 'HighPriorityUser';
    public const SPONSOR_USER = 'SponsorUser';

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'type', type: Types::STRING, length: 50, nullable: false)]
    protected string $type;

    #[ORM\Column(name: '`primary`', type: Types::STRING, length: 100, nullable: false)]
    protected string $primary;

    #[ORM\Column(name: 'secondary', type: Types::STRING, length: 100, nullable: false)]
    protected string $secondary;

    #[ORM\Column(name: 'value', type: Types::STRING, length: 250, nullable: false)]
    protected string $value = '';

    public function getId(): ?int {
        return $this->id;
    }

    public function getType(): string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getPrimary(): ?string {
        return $this->primary;
    }

    public function setPrimary(string $primary): self {
        $this->primary = $primary;

        return $this;
    }

    public function getSecondary(): ?string {
        return $this->secondary;
    }

    public function setSecondary(string $secondary): self {
        $this->secondary = $secondary;

        return $this;
    }

    public function getValue(): ?string {
        return $this->value;
    }

    public function setValue(string $value): self {
        $this->value = $value;

        return $this;
    }

    public function __toString(): string {
        return 'Liaison(type: '.$this->type.' primary: '.$this->primary.' secondary: '.$this->secondary.')';
    }


}
