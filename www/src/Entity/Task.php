<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Constant;
use App\Entity;
use App\Repository\TaskRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 */
#[ORM\Table(name: 'task')]
#[ORM\Index(name: 'project', columns: ['project'])]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
abstract class Task extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time = 0;

    #[ORM\Column(name: 'status', type: Types::STRING, length: 50, nullable: false)]
    protected string $status = Constant::WAITING;

    #[ORM\Column(name: 'project', type: Types::INTEGER, nullable: false)]
    protected int $project = -1;

    #[ORM\Column(name: 'data', type: Types::TEXT, length: 65535, nullable: false)]
    protected string $data = '';

    public function getId(): int {
        return $this->id;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getStatus(): string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getProject(): int {
        return $this->project;
    }

    public function setProject(int $project): self {
        $this->project = $project;

        return $this;
    }

    public function getData(): string {
        return $this->data;
    }

    public function setData(string $data): self {
        $this->data = $data;

        return $this;
    }

    public function execute(): bool {
        return false;
    }
}
