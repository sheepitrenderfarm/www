<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;

/**
 * Core info about a Session running or past
 */
#[MappedSuperclass]
abstract class SessionCoreInfos extends Entity {

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $id;

    protected User $user;

    #[ORM\JoinColumn(name: 'cpu', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: CPU::class)]
    protected ?CPU $cpu = null;

    #[ORM\JoinColumn(name: 'gpu', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: GPU::class)]
    protected ?GPU $gpu = null;

    /**
     * Value in KB
     */
    #[ORM\Column(name: 'memory', type: Types::INTEGER, nullable: false)]
    protected int $memory;

    #[ORM\Column(name: 'hwid', type: Types::STRING, length: 50, nullable: false, options: ['default' => ''])]
    protected string $hwid = '';

    #[ORM\Column(name: 'os', type: Types::STRING, length: 50, nullable: false)]
    protected string $os;

    #[ORM\Column(name: 'version', type: Types::STRING, length: 50, nullable: false)]
    protected string $version = '';

    #[ORM\Column(name: 'creationtime', type: Types::INTEGER, nullable: false)]
    protected int $creationtime;

    #[ORM\Column(name: 'renderedframes', type: Types::INTEGER, nullable: false)]
    protected int $renderedframes = 0;

    #[ORM\Column(name: 'lastvalidatedframe', type: Types::INTEGER, nullable: false)]
    protected int $lastvalidatedframe = -1;

    #[ORM\Column(name: 'lastrequestjob', type: Types::INTEGER, nullable: false)]
    protected int $lastrequestjob = -1;

    #[ORM\Column(name: 'points', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points = 0;

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getCpu(): ?CPU {
        return $this->cpu;
    }

    public function setCpu(?CPU $cpu): self {
        $this->cpu = $cpu;

        return $this;
    }

    public function getGpu(): ?GPU {
        return $this->gpu;
    }

    public function setGpu(?GPU $gpu): self {
        $this->gpu = $gpu;

        return $this;
    }

    public function getMemory(): int {
        return $this->memory;
    }

    public function setMemory(int $memory): self {
        $this->memory = $memory;

        return $this;
    }

    public function gethwid(): string {
        return $this->hwid;
    }

    public function sethwid(string $hwid): self {
        $this->hwid = $hwid;

        return $this;
    }

    public function getOS(): string {
        return $this->os;
    }

    public function setOS(string $os): self {
        $this->os = $os;

        return $this;
    }

    public function getVersion(): string {
        return $this->version;
    }

    public function setVersion(string $version): self {
        $this->version = $version;

        return $this;
    }

    public function getCreationtime(): int {
        return $this->creationtime;
    }

    public function setCreationtime(int $creationtime): self {
        $this->creationtime = $creationtime;

        return $this;
    }

    public function getRenderedframes(): int {
        return $this->renderedframes;
    }

    public function setRenderedframes(int $renderedframes): self {
        $this->renderedframes = $renderedframes;

        return $this;
    }

    public function getLastvalidatedframe(): ?int {
        return $this->lastvalidatedframe;
    }

    public function setLastvalidatedframe(int $lastvalidatedframe): self {
        $this->lastvalidatedframe = $lastvalidatedframe;

        return $this;
    }

    public function getLastrequestjob(): int {
        return $this->lastrequestjob;
    }

    public function setLastrequestjob(int $lastrequestjob): self {
        $this->lastrequestjob = $lastrequestjob;

        return $this;
    }

    public function getPoints(): float {
        return $this->points;
    }

    public function setPoints(float $points): self {
        $this->points = $points;

        return $this;
    }
}
