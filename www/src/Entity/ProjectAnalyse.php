<?php

namespace App\Entity;

use App\Constant;
use App\Entity;
use App\Repository\ProjectAnalyseRepository;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Analyse of a potential project
 */
#[ORM\Table(name: 'project_analyse')]
#[ORM\Entity(repositoryClass: ProjectAnalyseRepository::class)]
class ProjectAnalyse extends Entity {


    #[ORM\Column(name: 'id', type: Types::STRING, length: 32, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: false)]
    protected \DateTimeInterface $date;

    #[ORM\Column(name: 'status', type: Types::STRING, length: 200, nullable: false, options: ['default' => 'en_attente'])]
    protected string $status = Constant::WAITING;

    #[ORM\Column(name: 'name', type: Types::STRING, length: 64, nullable: false)]
    protected string $name;

    #[ORM\Column(name: 'analysed_file_count', type: Types::INTEGER, nullable: false)]
    protected int $analysed_file_count = 0;

    #[ORM\Column(name: 'total_file_count', type: Types::INTEGER, nullable: false)]
    protected int $total_file_count = 0;

    #[ORM\Column(name: 'path', type: Types::STRING, length: 255, nullable: false)]
    protected string $path;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    protected User $owner;

    #[ORM\Column(name: 'result', type: Types::TEXT, length: 65535, nullable: false)]
    protected string $result = '';

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getDate(): DateTimeInterface {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getAnalysedFilesCount(): int {
        return $this->analysed_file_count;
    }

    public function setAnalysedFilesCount(int $analysed_file_count): self {
        $this->analysed_file_count = $analysed_file_count;

        return $this;
    }

    public function getTotalFileCount(): int {
        return $this->total_file_count;
    }

    public function setTotalFileCount(int $total_file_count): self {
        $this->total_file_count = $total_file_count;

        return $this;
    }

    public function getPath(): string {
        return $this->path;
    }

    public function setPath(string $path): self {
        $this->path = $path;

        return $this;
    }

    public function getOwner(): User {
        return $this->owner;
    }

    public function setOwner(User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getResult(): string {
        return $this->result;
    }

    public function setResult(string $result): self {
        $this->result = $result;

        return $this;
    }
}
