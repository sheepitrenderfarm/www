<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

abstract class AwardRenderedFrames extends Award {

    abstract public function getObjective(): int;

    public function remainingHumanText(User $user): ?string {
        return number_format($this->getObjective() - $user->getRenderedFrames()).' frames remaining to get this award';
    }

    public function humanDescription(): string {
        return sprintf('%s frames rendered', number_format($this->getObjective()));
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($user) == false) {
            return false;
        }

        return ($user->getRenderedFrames() > $this->getObjective());
    }

    public function imagePath(): string {
        return '/media/image/award/award_framerendered_'.$this->getObjective().'.png';
    }

    public function category(): string {
        return 'rendered_frames';
    }
}
