<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Entity;
use App\Repository\TileRepository;
use App\Service\GlobalInject;
use App\Service\Logger;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tile
 */
#[ORM\Table(name: 'tile')]
#[ORM\Index(name: 'status', columns: ['status'])]
#[ORM\Index(name: 'session', columns: ['session'])]
#[ORM\Index(name: 'validation_time', columns: ['validation_time'])]
#[ORM\Index(name: 'frame', columns: ['frame'])]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Entity(repositoryClass: TileRepository::class)]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
abstract class Tile extends Entity {
    public const MINIMUM_REWARD_PER_TILE = 2;

    #[ORM\Column(name: 'id', type: Types::BIGINT, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'number', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $number;

    #[ORM\JoinColumn(name: 'frame', referencedColumnName: 'id', onDelete: 'CASCADE', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Frame::class, inversedBy: 'tiles')]
    protected Frame $frame;

    #[ORM\Column(name: 'status', type: Types::STRING, length: 35, nullable: false, options: ['default' => 'en_attente'])]
    protected string $status = 'en_attente';

    #[ORM\Column(name: 'token', type: Types::STRING, length: 32, nullable: false)]
    protected string $token = '';

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'])]
    protected ?User $user = null;

    #[ORM\Column(name: 'request_time', type: Types::INTEGER, nullable: false)]
    protected int $request_time = 0;

    #[ORM\Column(name: 'validation_time', type: Types::INTEGER, nullable: false)]
    protected int $validation_time = 0;

    #[ORM\Column(name: 'render_time', type: Types::INTEGER, nullable: false)]
    protected int $render_time = 0;

    #[ORM\Column(name: 'prep_time', type: Types::INTEGER, nullable: false)]
    protected int $prep_time = 0;

    #[ORM\Column(name: 'remaining_render_time', type: Types::INTEGER, nullable: false)]
    protected int $remaining_render_time = 0;

    #[ORM\JoinColumn(name: 'cpu', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: CPU::class)]
    protected ?CPU $cpu = null;

    #[ORM\JoinColumn(name: 'gpu', referencedColumnName: 'id', nullable: true)]
    #[ORM\ManyToOne(targetEntity: GPU::class)]
    protected ?GPU $gpu = null;

    #[ORM\Column(name: 'device_power', type: Types::INTEGER, nullable: false, options: ['default' => '-1'])]
    protected int $device_power = -1;

    #[ORM\Column(name: 'session', type: Types::INTEGER, nullable: true)]
    protected ?int $session = null;

    #[ORM\Column(name: 'cost', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $cost = 0.0;

    #[ORM\Column(name: 'reward', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $reward = 0.0;

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getNumber(): int {
        return $this->number;
    }

    public function setNumber(int $number): self {
        $this->number = $number;

        return $this;
    }

    public function getFrame(): Frame {
        /** @phpstan-ignore-next-line */
        if (is_null($this->frame)) {
            Logger::critical(__METHOD__.' frame is NULL!  tile(id:'.$this->getId().')');
        }
        return $this->frame;
    }

    public function setFrame(Frame $frame): self {
        $this->frame = $frame;
        $frame->__addTile($this);

        return $this;
    }

    public function getStatus(): string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getToken(): string {
        return $this->token;
    }

    public function setToken(string $token): self {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getRequestTime(): int {
        return $this->request_time;
    }

    public function setRequestTime(int $request_time): self {
        $this->request_time = $request_time;

        return $this;
    }

    public function getValidationTime(): int {
        return $this->validation_time;
    }

    public function setValidationTime(int $validationTime): self {
        $this->validation_time = $validationTime;

        return $this;
    }

    public function getRenderTime(): int {
        return $this->render_time;
    }

    public function setRenderTime(int $renderTime): self {
        $this->render_time = $renderTime;

        return $this;
    }

    public function getPrepTime(): int {
        return $this->prep_time;
    }

    public function setPrepTime(int $prepTime): self {
        $this->prep_time = $prepTime;

        return $this;
    }


    public function getRemainingRenderTime(): int {
        return $this->remaining_render_time;
    }

    public function setRemainingRenderTime(int $remainingRenderTime): self {
        $this->remaining_render_time = $remainingRenderTime;

        return $this;
    }

    public function getCpu(): ?CPU {
        return $this->cpu;
    }

    public function setCpu(?CPU $cpu): self {
        $this->cpu = $cpu;

        return $this;
    }

    public function getGpu(): ?GPU {
        return $this->gpu;
    }

    public function setGpu(?GPU $gpu): self {
        $this->gpu = $gpu;

        return $this;
    }

    public function getDevicePower(): int {
        return $this->device_power;
    }

    public function setDevicePower(int $devicePower): self {
        $this->device_power = $devicePower;

        return $this;
    }

    public function getSession(): ?int {
        return $this->session;
    }

    public function setSession(?int $session): self {
        $this->session = $session;

        return $this;
    }

    public function getCost(): float {
        return $this->cost;
    }

    public function setCost(float $cost): self {
        $this->cost = $cost;

        return $this;
    }

    public function getReward(): float {
        return $this->reward;
    }

    public function setReward(float $reward): self {
        $this->reward = $reward;

        return $this;
    }

    public function isComputeMethod(): bool {
        return $this instanceof TileComputeMethod;
    }

    public function isPowerDetection(): bool {
        return $this instanceof TilePowerDetection;
    }

    /**
     * Either the machine (i.e. the CPU) or the GPU
     **/
    abstract public function getComputeDevice(): ?BaseProcessingUnit;

    abstract public function reset(bool $give_points_to_renderer = false): bool;

    abstract public function resume(): bool;

    abstract public function validate(string $picture_file_, Session $session_): int;

    abstract public function validateFromShepherd(int $rendertime, int $preptime, int $memoryused): int;

    abstract public function take(Session $a_session_): int;

    abstract public function getPointsForOwner(): float;

    abstract public function getPointsForRenderer(): float;

    abstract public function getRenderTimeOnRefMachine(): float;

    abstract public function getRenderTimeOnRefMachineWithCoefAligned(): float;

    abstract public function getUrlThumbnailOnShepherd(bool $ondemand = false): string;

    abstract public function getUrlFullOnShepherd(): string;

    abstract public function getEstimatedRenderTime(): int;

    public function getBlenderCommandline(): string {
        return GlobalInject::getConfigService()->getData()['renderer']['command']['regular'];
    }

    public function getDevicePowerHuman(): string {
        if (is_null($this->getComputeDevice())) {
            return '';
        }
        return $this->getComputeDevice()->getPowerPercent().' %';
    }
}
