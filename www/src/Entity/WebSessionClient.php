<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\WebSessionClientRepository;
use App\Service\GlobalInject;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * WebSessionClient
 */
#[ORM\Table(name: 'session_web_client')]
#[ORM\Index(name: 'login', columns: ['login'])]
#[ORM\Index(name: 'key', columns: ['key'])]
#[ORM\Entity(repositoryClass: WebSessionClientRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class WebSessionClient extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'expiration_time', type: Types::INTEGER, nullable: false)]
    protected int $expirationTime;

    #[ORM\JoinColumn(name: 'login', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    protected User $user;

    #[ORM\Column(name: '`key`', type: Types::STRING, length: 50, nullable: false)]
    protected string $key = '';

    #[ORM\Column(name: 'session_id', type: Types::INTEGER, nullable: false)]
    protected int $session_id;

    #[ORM\Column(name: 'data', type: Types::TEXT, length: 65535, nullable: true)]
    protected ?string $data = '';

    public function getId(): int {
        return $this->id;
    }

    public function getExpirationTime(): int {
        return $this->expirationTime;
    }

    public function setExpirationTime(int $expirationTime): self {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getKey(): string {
        return $this->key;
    }

    public function setKey(string $key): self {
        $this->key = $key;

        return $this;
    }

    public function getSessionId(): int {
        return $this->session_id;
    }

    public function setSessionId(int $sessionId): self {
        $this->session_id = $sessionId;

        return $this;
    }

    public function getData(): ?string {
        return $this->data;
    }

    public function setData(?string $data): self {
        $this->data = $data;

        return $this;
    }

    public function saveToCookie(): void {
        $config = GlobalInject::getConfigService()->getData();
        global $_COOKIE;

        @setcookie('web-session', $this->getKey(), time() + $config['web-session-client']['duration']);
        @setcookie('login', $this->getUser()->getId(), time() + $config['web-session-client']['duration']);
    }

    public function loadSessionFromData(): ?Session {

        $row = unserialize($this->getData());
        if (is_array($row) == false) {
            return null;
        }

        $session = new Session();
        $session->setBlocked($row['blocked']);
        $session->setRunning($row['running']);
        $session->setUser(GlobalInject::getEntityManager()->getRepository(User::class)->find($row['login']));
        $session->setCpu(GlobalInject::getEntityManager()->getRepository(CPU::class)->find($row['cpu']));
        $session->setGpu(GlobalInject::getEntityManager()->getRepository(GPU::class)->find($row['gpu']));
        $session->setMemory((int)($row['memory']));
        $session->setMemoryAllowed($row['memory_allowed']);
        $session->setHeadless($row['headless']);
        $session->setUi($row['ui']);
        $session->setCountry($row['country']);
        $session->setOS($row['os']);
        $session->setVersion($row['version']);
        $session->setUserAgent($row['user_agent']);
        $session->setScheduler($row['scheduler']);
        $session->setComputeMethod($row['compute_method']);
        $session->setMaxRendertime($row['max_rendertime']);
        $session->setPowerCpu($row['power_cpu']);
        $session->setPowerGpu($row['power_gpu']);
        $session->setCreationtime($row['creationtime']);
        $session->setTimestamp($row['timestamp']);
        $session->setRenderedframes($row['renderedframes']);
        $session->setLastvalidatedframe($row['lastvalidatedframe']);
        $session->setLastrequestjob($row['lastrequestjob']);
        $session->setPoints($row['points']);

        return $session;
    }

    public function saveSessionToData(Session $session): void {
        $row = [];
        $row['blocked'] = $session->getBlocked();
        $row['running'] = $session->getRunning();
        $row['login'] = $session->getUser()->getId();
        $row['cpu'] = $session->getCpu()?->getId();
        $row['gpu'] = $session->getGpu()?->getId();
        $row['memory'] = $session->getMemory();
        $row['memory_allowed'] = $session->getMemoryAllowed();
        $row['headless'] = $session->getHeadless();
        $row['ui'] = $session->getUi();
        $row['country'] = $session->getCountry();
        $row['os'] = $session->getOS();
        $row['version'] = $session->getVersion();
        $row['user_agent'] = $session->getUserAgent();
        $row['scheduler'] = $session->getScheduler();
        $row['compute_method'] = $session->getComputeMethod();
        $row['max_rendertime'] = $session->getMaxRendertime();
        $row['power_cpu'] = $session->getPowerCpu();
        $row['power_gpu'] = $session->getPowerGpu();
        $row['creationtime'] = $session->getCreationtime();
        $row['timestamp'] = $session->getTimestamp();
        $row['renderedframes'] = $session->getRenderedframes();
        $row['lastvalidatedframe'] = $session->getLastvalidatedframe();
        $row['lastrequestjob'] = $session->getLastrequestjob();
        $row['points'] = $session->getPoints();

        $this->setData(serialize($row));
    }

    public function createKey(): void {
        $key = '';
        $key .= $this->getUser()->getId();
        $key .= '_'.time();
        $key .= '_'.rand();

        for ($i = 0; $i < 2000; $i++) {
            $key = md5($key);
        }

        $this->setKey($key);
    }

}
