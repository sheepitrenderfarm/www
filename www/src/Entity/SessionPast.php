<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Repository\SessionPastRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * SessionPast, store all info to get a sumup of a previous session
 */
#[ORM\Table(name: 'session_past')]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'gpu', columns: ['gpu'])]
#[ORM\Entity(repositoryClass: SessionPastRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class SessionPast extends SessionCoreInfos {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id] // Not strategy="IDENTITY" because we want the same id as the running session
    protected int $id;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'sessionPasts')]
    protected User $user;

    #[ORM\Column(name: 'endtime', type: Types::INTEGER, nullable: false)]
    protected int $endtime;

    #[ORM\Column(name: 'reason', type: Types::STRING, length: 80, nullable: false)]
    protected string $reason = '';

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getEndtime(): int {
        return $this->endtime;
    }

    public function setEndtime(int $endtime): self {
        $this->endtime = $endtime;

        return $this;
    }

    public function getReason(): string {
        return $this->reason;
    }

    public function setReason(string $reason): self {
        $this->reason = $reason;

        return $this;
    }
}
