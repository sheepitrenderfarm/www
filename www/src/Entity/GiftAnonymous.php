<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\GiftAnonymousRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * GiftAnonymous
 */
#[ORM\Table(name: 'gift_anonymous')]
#[ORM\UniqueConstraint(name: 'id', columns: ['id'])]
#[ORM\Entity(repositoryClass: GiftAnonymousRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class GiftAnonymous extends Entity {
    public const TYPE_OTHER = 0;
    public const TYPE_PATREON = 1;
    public const TYPE_PAYPAL = 2;

    #[ORM\Column(name: 'id', type: Types::STRING, length: 128, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\Column(name: 'type', type: Types::INTEGER, nullable: true)]
    protected ?int $type = null;

    #[ORM\Column(name: 'creation', type: Types::INTEGER, nullable: false)]
    protected int $creation;

    #[ORM\Column(name: 'value', type: Types::INTEGER, nullable: true)]
    protected ?int $value = null;

    #[ORM\Column(name: 'comment', type: Types::TEXT, length: 65535, nullable: false)]
    protected string $comment = '';

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(?string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getType(): int {
        return $this->type;
    }

    public function setType(int $type): self {
        $this->type = $type;

        return $this;
    }

    public function getCreation(): int {
        return $this->creation;
    }

    public function setCreation(int $creation): self {
        $this->creation = $creation;

        return $this;
    }

    public function getValue(): int {
        return $this->value;
    }

    public function setValue(int $value): self {
        $this->value = $value;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;

        return $this;
    }
}
