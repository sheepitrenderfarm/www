<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

abstract class AwardEventFramesRendered extends Award {

    abstract public function getDate(): int;

    abstract public function getAuthor(): string;

    abstract public function getObjectiveLong(): string;

    abstract public function getObjectiveShort(): string;

    public function reward(): int {
        return 2000;
    }

    public function humanDescription(): string {
        return sprintf('Participated during the celebration of the %s rendered frame, celebrated on %s. To get the award, you simply had to render a frame around the date\'s event. We regularly do events like this to thank everyone who keeps SheepIt alive.<br><i>Award image by %s.</i>', $this->getObjectiveLong(), date('F, jS Y', $this->getDate()), $this->getAuthor());
    }

    public function imagePath(): string {
        return '/media/image/award/award_event_'.$this->getObjectiveShort().'.png';
    }

    public function category(): string {
        return 'event';
    }

    public function level(): int {
        // timestamp of the news
        return $this->getDate();
    }

    public function canBeEarn(): bool {
        return false;
    }

//     public function canBeEarn() {
//         return true;
//     }
//     
//     public function earn(?User $user, ?Frame $frame, ?Session $session) {
//         if (is_object($frame) && $frame->isPowerDetection() == false && $frame->isComputeMethod() == false) {
//             return time() < 1502654400; // 13 aout
//         }
//         return false;
//     }
//     
//     public function actionOnEarn(?User $user, ?Frame $frame, ?Session $session) {
//         $config = GlobalInject::getConfigService()->getData();
//         if (is_object($user)) {
//             $value = 2000;
//             
//             $coupon = new Gift();
//             $coupon->setAttribute('value', $value);
//             $coupon->setAttribute('expiration', time() + $config['gift']['duration']);
//             $coupon->setOwner($user->getId());
//             $coupon->setAttribute('comment', 'Gift 2000 points for a Frame (Event 40 million)');
//             $coupon->generateAccess();
//             $id = $coupon->getId();
//             $coupon->add(); // will overright the id attribute
//             $coupon->setId($id);
//             
//             $content_user = sprintf('Thanks for rendering a frame!<br>This week is a special week, 40,000,000 frame have been rendered on SheepIt.<br>To thanks everyone who have made this possible, a %d points gift is given to anyone who render a frame in next the few of days (offer end on sunday).<br>The %d points have been given you to as a gift, you can use in by going to this adress: <a href="%%%%GIFT_URL%%%%">use a gift</a>.<br><br>You can also find more a bout the 40 million frames event event on the <a href="https://www.sheepit-renderfarm.com/news.php?id=1502207950">news page</a>.', $value, $value);
//             
//             $coupon->sendEmail($content_user);
//         }
//     }
}
