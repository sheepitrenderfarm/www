<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsUserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsUser
 */
#[ORM\Table(name: 'stats_user')]
#[ORM\Index(name: 'date', columns: ['date'])]
#[ORM\Index(name: 'login', columns: ['login'])]
#[ORM\Entity(repositoryClass: StatsUserRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsUser extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: false)]
    protected \DateTimeInterface $date;

    #[ORM\JoinColumn(name: 'login', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'stats', cascade: ['persist'])]
    protected User $user;

    #[ORM\Column(name: 'rendered_frames', type: Types::INTEGER, nullable: false)]
    protected int $renderedFrames;

    #[ORM\Column(name: 'render_time', type: Types::BIGINT, nullable: false)]
    protected int $renderTime;

    #[ORM\Column(name: 'points_earn', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $pointsEarn;

    #[ORM\Column(name: 'points', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points;

    #[ORM\Column(name: 'team', type: Types::INTEGER, nullable: false)]
    protected int $team = -1;

    #[ORM\Column(name: 'team_contribution', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $teamContribution = 0.0;

    public function getId(): ?int {
        return $this->id;
    }

    public function getDate(): \DateTimeInterface {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self {
        $this->date = $date;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getRenderedFrames(): int {
        return $this->renderedFrames;
    }

    public function setRenderedFrames(int $renderedFrames): self {
        $this->renderedFrames = $renderedFrames;

        return $this;
    }

    public function getRenderTime(): int {
        return $this->renderTime;
    }

    public function setRenderTime(int $renderTime): self {
        $this->renderTime = $renderTime;

        return $this;
    }

    public function getPointsEarn(): ?float {
        return $this->pointsEarn;
    }

    public function setPointsEarn(float $pointsEarn): self {
        $this->pointsEarn = $pointsEarn;

        return $this;
    }

    public function getPoints(): float {
        return $this->points;
    }

    public function setPoints(float $points): self {
        $this->points = $points;

        return $this;
    }

    public function getTeam(): int {
        return $this->team;
    }

    public function setTeam(int $team): self {
        $this->team = $team;

        return $this;
    }

    public function getTeamContribution(): ?float {
        return $this->teamContribution;
    }

    public function setTeamContribution(float $teamContribution): self {
        $this->teamContribution = $teamContribution;

        return $this;
    }
}
