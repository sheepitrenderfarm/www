<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Repository\GPURepository;
use App\Service\GlobalInject;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gpu
 */
#[ORM\Table(name: 'gpu')]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Index(name: 'model', columns: ['model'])]
#[ORM\Index(name: 'ram', columns: ['memory'])]
#[ORM\Entity(repositoryClass: GPURepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class GPU extends BaseProcessingUnit {
    public const TYPE_OPTIX = 'OPTIX';

    #[ORM\Column(name: 'type', type: Types::STRING, length: 32, nullable: false)]
    protected string $type = '';

    #[ORM\Column(name: 'memory', type: Types::BIGINT, nullable: false)]
    protected int $memory;

    public function getType(): string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getMemory(): int {
        return $this->memory;
    }

    public function setMemory(int $memory): self {
        $this->memory = $memory;

        return $this;
    }

    public function description(): string {
        return htmlspecialchars($this->getModel());
    }

    public function isSupported(): bool {
        $model = strtolower($this->getModel());

        $blacklisted = array(
            'radeon(tm) hd 5450',
            'radeon(tm) hd 7450',
            'ati',
            'radeon(tm) r2',
            'radeon(tm) r3',
            'radeon(tm) r4',
            'radeon (tm) r4',
            'radeon(tm) r4',
            'radeon r4',
            'radeon (tm) r5',
            'radeon(tm) r5',
            'radeon r5 ',
            'radeon hd',
            'radeon(tm) hd',
        );

        foreach ($blacklisted as $b) {
            if (str_contains($model, $b)) {
                return false;
            }
        }

        return true;
    }

    public function getPowerPercent(): int {
        return (int)(100.0 * $this->getPower() / GlobalInject::getConfigService()->getData()['power']['gpu']['const']);
    }

    public function __toString(): string {
        return "GPU(id".$this->id.' type:'.$this->type.' model:'.$this->model.' memory:'.$this->memory.' rendered_frames:'.$this->rendered_frames.' power:'.$this->power.')';
    }

    public function getPowerConst(): int {
        return GlobalInject::getConfigService()->getData()['power']['gpu']['const'];
    }

    public function getPowerRatioOverCPU(): float {
        $config = GlobalInject::getConfigService()->getData();
        return (float)($config['power']['gpu']['const']) / (float)($config['power']['cpu']['const']);
    }
}
