<?php

namespace App\Entity;

use App\Constant;
use App\Persistent\Cache;
use App\Repository\ProjectRepository;
use App\Service\Daemon;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\LockMode;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Project
 */
#[ORM\Table(name: 'project')]
#[ORM\Index(name: 'status', columns: ['status'])]
#[ORM\Index(name: 'owner', columns: ['owner'])]
#[ORM\UniqueConstraint(name: 'name', columns: ['name'])]
#[ORM\Entity(repositoryClass: ProjectRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class Project extends CDNSyncEntity {
    public const COMPUTE_METHOD_PROJECT_ID = 1;
    public const POWER_DETECTION_PROJECT_ID = 2;

    public const PREEMPTIVE_BLOCK_CONST = 90;
    public const BLOCK_MESSAGES = array(
        1 => array('ownercanrender' => true, 'short' => 'Render time', 'long' => 'Render time too high (it should be under %d min per frame on the reference machine)<br>Did you split your project in enough tiles (or splits)?'),
        2 => array('ownercanrender' => true, 'short' => 'Crashed', 'long' => 'Blender crashed on members machines'),
        4 => array('ownercanrender' => false, 'short' => 'Nudity', 'long' => 'Nudity content'),
        5 => array('ownercanrender' => false, 'short' => 'Files path', 'long' => 'Files path contains a non-supported character'),
        6 => array('ownercanrender' => true, 'short' => 'Missing textures', 'long' => 'Missing textures in project (output is pink)'),
        7 => array('ownercanrender' => false, 'short' => 'No camera', 'long' => 'Cannot render, no camera'),
        8 => array('ownercanrender' => true, 'short' => 'Missing bake files', 'long' => 'Missing bake files for physics/fluids/smoke'),
        9 => array('ownercanrender' => true, 'short' => 'Blank image', 'long' => 'Generating blank or empty black image'),
        10 => array('ownercanrender' => true, 'short' => 'Benchmark', 'long' => 'Benchmark project'),
        11 => array('ownercanrender' => false, 'short' => 'Unrenderable', 'long' => 'Contains unrenderable frame(s)'),
        12 => array('ownercanrender' => true, 'short' => 'Not enough points', 'long' => 'Not enough points'),
        13 => array('ownercanrender' => false, 'short' => 'Image output is too big for SheepIt', 'long' => 'Image output is too big for SheepIt'),
        14 => array('ownercanrender' => true, 'short' => 'Blender version', 'long' => 'Wrong Blender version'),
        15 => array('ownercanrender' => false, 'short' => 'OSL limit', 'long' => 'Your project has reach the OSL data size limit, it can not be render on any GPU.<br>Either disable OSL (Open Shading Language) or use CPU compute method.<br>In both case you need to reupload it.'),
        70 => array('ownercanrender' => false, 'short' => 'Morals issue', 'long' => 'Your projects don\'t follow the morals of SheepIt. Thanks for joining but DON\'T come back'),
        90 => array('ownercanrender' => true, 'short' => 'Too heavy', 'long' => 'Your project has been detected as too heavy, to protect the render farm, the project has been blocked. Since it could be a false negative, you can redeem it by rendering ONE frame of it. If the render time is appropriate, it will be automatically unblocked'),
        97 => array('ownercanrender' => false, 'short' => 'Upload Failed', 'long' => 'Your project failed to upload properly. It\'s an internal bug on the SheepIt side. There are no files on the server. Please attempt to upload the project again.'),
        98 => array('ownercanrender' => true, 'short' => 'Not a 3D', 'long' => 'Not a 3D project'),
        99 => array('ownercanrender' => true, 'short' => 'Others/Unknown', 'long' => 'Others/Unknown'));

    protected Cache $cache_acl_renderer;
    protected array $cache_acl_manager = [];

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'status', type: Types::STRING, length: 200, nullable: false, options: ['default' => 'en_attente'])]
    protected string $status = Constant::WAITING;

    #[ORM\Column(name: 'name', type: Types::STRING, length: 64, nullable: false)]
    protected string $name;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'projects', cascade: ['persist'])]
    protected User $owner;

    #[ORM\JoinColumn(name: 'executable', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Blender::class)]
    protected Blender $executable;

    #[ORM\Column(name: 'password', type: Types::STRING, length: 32, nullable: false)]
    protected string $password = '';

    #[ORM\Column(name: 'use_exr', type: Types::BOOLEAN, nullable: false)]
    protected bool $use_exr = false;

    #[ORM\Column(name: 'lastupdatearchive', type: Types::INTEGER, nullable: false)]
    protected int $lastupdatearchive = 0;

    #[ORM\Column(name: 'archive_size', type: Types::INTEGER, nullable: false)]
    protected int $archive_size = 0;

    /**
     * Size on Shepherd (archive on www is not used)
     */
    #[ORM\Column(name: 'predicted_size', type: Types::BIGINT, nullable: false)]
    protected int $predicted_size = 0;

    #[ORM\Column(name: 'archive_md5', type: Types::STRING, length: 32, nullable: false)]
    protected string $archive_md5 = '';

    #[ORM\Column(name: 'public_render', type: Types::BOOLEAN, nullable: false)]
    protected bool $public_render = false;

    #[ORM\Column(name: 'public_thumbnail', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $public_thumbnail = true;

    #[ORM\Column(name: 'compute_method', type: Types::INTEGER, nullable: false, options: ['default' => '1'])]
    protected int $compute_method = 1;

    #[ORM\Column(name: 'generate_mp4', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $generate_mp4 = true;

    #[ORM\Column(name: 'can_generate_mp4', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $can_generate_mp4 = true;

    #[ORM\Column(name: 'path', type: Types::STRING, length: 256, nullable: false)]
    protected string $path;

    #[ORM\Column(name: 'picture_extension', type: Types::STRING, length: 16, nullable: false)]
    protected string $picture_extension = '';

    /**
     * @var array<int, string>
     */
    #[ORM\Column(name: 'chunks', type: Types::JSON, nullable: false)]
    protected array $chunks = [];

    #[ORM\Column(name: 'blocked', type: Types::INTEGER, nullable: false)]
    protected int $blocked = 0;

    #[ORM\Column(name: 'framerate', type: Types::FLOAT, precision: 10, scale: 0, nullable: false, options: ['default' => 25])]
    protected float $framerate = 25;

    #[ORM\JoinColumn(name: 'shepherd_server', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: Shepherd::class)]
    protected Shepherd $shepherd;

    #[ORM\Column(name: 'shepherd_owner_token', type: Types::STRING, length: 32, nullable: false)]
    protected string $shepherd_owner_token = '';

    #[ORM\Column(name: 'shepherd_owner_token_validity', type: Types::INTEGER, nullable: false)]
    protected int $shepherd_owner_token_validity = 0;

    #[ORM\Column(name: 'shepherd_thumbnail_token', type: Types::STRING, length: 32, nullable: false)]
    protected string $shepherd_thumbnail_token = '';

    #[ORM\Column(name: 'shepherd_thumbnail_token_validity', type: Types::INTEGER, nullable: false)]
    protected int $shepherd_thumbnail_token_validity = 0;

    #[ORM\Column(name: 'max_memory_used', type: Types::BIGINT, nullable: false, options: ['comment' => 'ram in kB'])]
    protected int $max_memory_used = 0;

    #[ORM\Column(name: 'vram_success', type: Types::BIGINT, nullable: false)]
    protected int $vram_success = 0;

    #[ORM\Column(name: 'vram_failure', type: Types::BIGINT, nullable: false)]
    protected int $vram_failure = 0;

    #[ORM\Column(name: 'owner_points_onlastupdate', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $owner_points_onlastupdate = 0.0;

    #[ORM\Column(name: 'engine', type: Types::STRING, length: 50, nullable: false)]
    protected string $engine = '';

    #[ORM\Column(name: 'samples', type: Types::INTEGER, nullable: false)]
    protected int $samples = 0;

    #[ORM\Column(name: 'render_on_gpu_headless', type: Types::BOOLEAN, nullable: false, options: ['default' => true])]
    protected bool $render_on_gpu_headless = true;

    #[ORM\Column(name: 'user_error', type: Types::INTEGER, nullable: false)]
    protected int $user_error = 0;

    #[ORM\Column(name: 'email_sent', type: Types::BOOLEAN, nullable: false)]
    protected bool $email_sent = false;

    #[ORM\Column(name: 'zip_generated', type: Types::INTEGER, nullable: false, options: ['default' => '0'])]
    protected int $zip_generated = 0;

    #[ORM\Column(name: 'mp4_preview_generated', type: Types::INTEGER, nullable: false, options: ['default' => '0'])]
    protected int $mp4_preview_generated = 0;

    #[ORM\Column(name: 'mp4_final_generated', type: Types::INTEGER, nullable: false, options: ['default' => '0'])]
    protected int $mp4_final_generated = 0;

    #[ORM\Column(name: 'cache_total', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_total = 0;

    #[ORM\Column(name: 'cache_rendertime_actual', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_rendertime_actual = 0;

    #[ORM\Column(name: 'cache_rendertime_ref', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_rendertime_ref = 0;

    #[ORM\Column(name: 'cache_preptime_actual', type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    protected int $cache_preptime_actual = 0;

    #[ORM\Column(name: 'cache_waiting', type: Types::INTEGER, nullable: false)]
    protected int $cache_waiting = 0;

    #[ORM\Column(name: 'cache_processing', type: Types::INTEGER, nullable: false)]
    protected int $cache_processing = 0;

    #[ORM\Column(name: 'cache_finished', type: Types::INTEGER, nullable: false)]
    protected int $cache_finished = 0;

    #[ORM\Column(name: 'cache_last_validation', type: Types::INTEGER, nullable: false)]
    protected int $cache_last_validation = 0;

    /**
     * @var Collection<Frame>
     */
    #[ORM\OneToMany(targetEntity: Frame::class, mappedBy: 'project', cascade: ['persist', 'remove'])]
    protected Collection $frames;

    public float $score; // for the scheduler

    public function __addFrame(Frame $frame): self {
        if (!$this->frames->contains($frame)) {
            $this->frames->add($frame);
            $frame->setProject($this);
        }

        return $this;
    }

    public function __removeFrame(Frame $frame): self {
        if ($this->frames->contains($frame)) {
            $this->frames->removeElement($frame);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    /**
     * @return Collection<Frame>
     */
    public function frames(): Collection {
        return $this->frames;
    }

    /**
     * @return Collection<int, Frame>
     */
    public function framesWithFinishedTiles(): Collection {
        return $this->frames()->filter(function (Frame $element) {
            return $element->getRenderTime() > 0;
        });
    }

    public function __construct() {
        $this->frames = new ArrayCollection();
        $this->init();

    }

    #[ORM\PostLoad]
    public function init(): void {
        $this->cache_acl_renderer = new Cache();
        $this->cache_acl_manager = array();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getStatus(): string {
        return $this->status;
    }

    public function setStatus(string $status): self {
        $this->status = $status;

        return $this;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getExecutable(): Blender {
        return $this->executable;
    }

    public function setExecutable(Blender $executable): self {
        $this->executable = $executable;

        return $this;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getUseExr(): bool {
        return $this->use_exr;
    }

    public function setUseExr(bool $use_exr): self {
        $this->use_exr = $use_exr;

        return $this;
    }

    public function getOwnerPointsOnlastupdate(): float {
        return $this->owner_points_onlastupdate;
    }

    public function setOwnerPointsOnlastupdate(float $ownerPointsOnlastupdate): self {
        $this->owner_points_onlastupdate = $ownerPointsOnlastupdate;

        return $this;
    }

    public function getLastupdatearchive(): int {
        return $this->lastupdatearchive;
    }

    public function setLastupdatearchive(int $lastupdatearchive): self {
        $this->lastupdatearchive = $lastupdatearchive;

        return $this;
    }

    public function getArchiveSize(): int {
        return $this->archive_size;
    }

    public function setArchiveSize(int $archive_size): self {
        $this->archive_size = $archive_size;

        return $this;
    }

    public function getPredictedSizeOnShepherd(): int {
        return $this->predicted_size;
    }

    public function setPredictedSizeOnShepherd(int $predicted_size): self {
        $this->predicted_size = $predicted_size;

        return $this;
    }

    public function getPublicRender(): bool {
        return $this->public_render;
    }

    public function setPublicRender(bool $public_render): self {
        $this->public_render = $public_render;

        return $this;
    }

    public function getPublicThumbnail(): bool {
        return $this->public_thumbnail;
    }

    public function setPublicThumbnail(bool $public_thumbnail): self {
        $this->public_thumbnail = $public_thumbnail;

        return $this;
    }

    public function getComputeMethod(): int {
        return $this->compute_method;
    }

    public function setComputeMethod(int $compute_method): self {
        $this->compute_method = $compute_method;

        return $this;
    }

    public function getGenerateMp4(): bool {
        return $this->generate_mp4;
    }

    public function setGenerateMp4(bool $generate_mp4): self {
        $this->generate_mp4 = $generate_mp4;

        return $this;
    }

    public function getCanGenerateMp4(): bool {
        return $this->can_generate_mp4;
    }

    public function setCanGenerateMp4(bool $can_generate_mp4): self {
        $this->can_generate_mp4 = $can_generate_mp4;

        return $this;
    }

    public function getPath(): string {
        return $this->path;
    }

    public function setPath(string $path): self {
        $this->path = $path;

        return $this;
    }

    public function getPictureExtension(): string {
        return $this->picture_extension;
    }

    public function setPictureExtension(string $picture_extension): self {
        $this->picture_extension = $picture_extension;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getChunks(): array {
        return $this->chunks;
    }

    public function setChunks(array $chunks): self {
        $this->chunks = $chunks;

        return $this;
    }

    public function getMd5(): string {
        return $this->archive_md5;
    }

    public function setMd5(string $md5): self {
        $this->archive_md5 = $md5;
        return $this;
    }

    public function getBlocked(): int {
        return $this->blocked;
    }

    public function setBlocked(int $blocked): self {
        $this->blocked = $blocked;

        return $this;
    }

    public function isBlocked(): bool {
        return $this->blocked != 0;
    }

    public function getFramerate(): float {
        return $this->framerate;
    }

    public function setFramerate(float $framerate): self {
        $this->framerate = $framerate;

        return $this;
    }

    public function getShepherdOwnerToken(): ?string {
        return $this->shepherd_owner_token;
    }

    public function setShepherdOwnerToken(string $shepherd_owner_token): self {
        $this->shepherd_owner_token = $shepherd_owner_token;

        return $this;
    }

    public function getShepherdOwnerTokenValidity(): ?int {
        return $this->shepherd_owner_token_validity;
    }

    public function setShepherdOwnerTokenValidity(int $shepherd_owner_token_validity): self {
        $this->shepherd_owner_token_validity = $shepherd_owner_token_validity;

        return $this;
    }

    public function getShepherdThumbnailToken(): ?string {
        return $this->shepherd_thumbnail_token;
    }

    public function setShepherdThumbnailToken(string $shepherd_thumbnail_token): self {
        $this->shepherd_thumbnail_token = $shepherd_thumbnail_token;

        return $this;
    }

    public function getShepherdThumbnailTokenValidity(): ?int {
        return $this->shepherd_thumbnail_token_validity;
    }

    public function setShepherdThumbnailTokenValidity(int $shepherd_thumbnail_token_validity): self {
        $this->shepherd_thumbnail_token_validity = $shepherd_thumbnail_token_validity;

        return $this;
    }

    public function getMaxMemoryUsed(): int {
        return $this->max_memory_used;
    }

    public function setMaxMemoryUsed(int $max_memory_used): self {
        $this->max_memory_used = $max_memory_used;

        return $this;
    }

    public function getVramSuccess(): int {
        return $this->vram_success;
    }

    public function setVramSuccess(int $vram_success): self {
        $this->vram_success = $vram_success;

        return $this;
    }

    public function getVramFailure(): int {
        return $this->vram_failure;
    }

    public function setVramFailure(int $vram_failure): self {
        $this->vram_failure = $vram_failure;

        return $this;
    }

    public function getEngine(): string {
        return $this->engine;
    }

    public function setEngine(string $engine): self {
        $this->engine = $engine;

        return $this;
    }

    public function getSamples(): int {
        return $this->samples;
    }

    public function setSamples(int $samples): self {
        $this->samples = $samples;

        return $this;
    }

    public function getRenderOnGpuHeadless(): bool {
        return $this->render_on_gpu_headless;
    }

    public function setRenderOnGpuHeadless(bool $render_on_gpu_headless): self {
        $this->render_on_gpu_headless = $render_on_gpu_headless;

        return $this;
    }

    public function getUserError(): int {
        return $this->user_error;
    }

    public function setUserError(int $user_error): self {
        $this->user_error = $user_error;

        return $this;
    }

    public function getEmailSent(): ?bool {
        return $this->email_sent;
    }

    public function setEmailSent(bool $email_sent): self {
        $this->email_sent = $email_sent;

        return $this;
    }

    public function getZipGenerated(): int {
        return $this->zip_generated;
    }

    public function setZipGenerated(int $zip_generated): self {
        $this->zip_generated = $zip_generated;

        return $this;
    }

    public function getMp4PreviewGenerated(): int {
        return $this->mp4_preview_generated;
    }

    public function setMp4PreviewGenerated(int $mp4_preview_generated): self {
        $this->mp4_preview_generated = $mp4_preview_generated;

        return $this;
    }

    public function getMp4FinalGenerated(): int {
        return $this->mp4_final_generated;
    }

    public function setMp4FinalGenerated(int $mp4_final_generated): self {
        $this->mp4_final_generated = $mp4_final_generated;

        return $this;
    }

    public function getOwner(): ?User {
        return $this->owner;
    }

    public function setOwner(?User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getShepherd(): Shepherd {
        return $this->shepherd;
    }

    public function setShepherd(Shepherd $shepherd): self {
        $this->shepherd = $shepherd;

        return $this;
    }

    public function getCacheTotal(): int {
        return $this->cache_total;
    }

    public function setCacheTotal(int $cache_total): self {
        $this->cache_total = $cache_total;

        return $this;
    }

    public function getCacheRendertimeActual(): int {
        return $this->cache_rendertime_actual;
    }

    public function setCacheRendertimeActual(int $cache_rendertime_actual): self {
        $this->cache_rendertime_actual = $cache_rendertime_actual;

        return $this;
    }

    public function getCachePrepTimeActual(): int {
        return $this->cache_preptime_actual;
    }

    public function setCachePrepTimeActual(int $cache_preptime_actual): self {
        $this->cache_preptime_actual = $cache_preptime_actual;

        return $this;
    }

    public function getCacheRendertimeRef(): int {
        return $this->cache_rendertime_ref;
    }

    public function setCacheRendertimeRef(int $cache_rendertime_ref): self {
        $this->cache_rendertime_ref = $cache_rendertime_ref;

        return $this;
    }

    public function getCacheWaiting(): int {
        return $this->cache_waiting;
    }

    public function setCacheWaiting(int $cache_waiting): self {
        $this->cache_waiting = $cache_waiting;

        return $this;
    }

    public function getCacheProcessing(): int {
        return $this->cache_processing;
    }

    public function setCacheProcessing(int $cache_processing): self {
        $this->cache_processing = $cache_processing;

        return $this;
    }

    public function getCacheFinished(): int {
        return $this->cache_finished;
    }

    public function setCacheFinished(int $cache_finished): self {
        $this->cache_finished = $cache_finished;

        return $this;
    }

    public function getCacheLastValidation(): int {
        return $this->cache_last_validation;
    }

    public function setCacheLastValidation(int $cache_last_validation): self {
        $this->cache_last_validation = $cache_last_validation;

        return $this;
    }

    /**
     * @return Collection|Frame[]
     */
    public function getFrames(): Collection {
        return $this->frames;
    }

    public function addFrame(Frame $frame): self {
        if (!$this->frames->contains($frame)) {
            $this->frames[] = $frame;
            $frame->setProject($this);
        }

        return $this;
    }

    /**
     * @return Tile[]
     */
    public function tiles(): array {
        return GlobalInject::getEntityManager()->getRepository(Tile::class)->tilesFor($this);
    }

    public function canUseCPU(): bool {
        if ($this->getEngine() == 'BLENDER_WORKBENCH') {
            return false;
        }
        if ($this->getEngine() == 'BLENDER_EEVEE') {
            return false;
        }
        if ($this->getEngine() == 'BLENDER_EEVEE_NEXT') {
            return false;
        }
        if ($this->getRenderOnGpuHeadless() == false) {
            return false;
        }

        return true;
    }

    public function canUseGPU(): bool {
        return true;
    }

    public function resetGPUUsage(): bool {
        $this->vram_success = 0;
        $this->vram_failure = 0;
        GlobalInject::getEntityManager()->flush($this);

        return true;
    }

    public function generateStatsCache(): array {
        $keys = array(Constant::STATS_TOTAL, Constant::STATS_RENDERTIME_ACTUAL, Constant::STATS_PREPTIME_ACTUAL, Constant::STATS_RENDERTIME_REF, Constant::WAITING, Constant::PROCESSING, Constant::FINISHED, Constant::STATS_LAST_VALIDATION);
        $ret = array();
        foreach ($keys as $k) {
            $ret[$k] = 0;
        }
        foreach ($this->tiles() as $tile) {
            $ret[$tile->getStatus()] += 1;
            $ret[Constant::STATS_TOTAL] += 1;
            if ($ret[Constant::STATS_LAST_VALIDATION] < $tile->getValidationTime()) {
                $ret[Constant::STATS_LAST_VALIDATION] = $tile->getValidationTime();
            }
            if ($tile->getRenderTime() > 0) {
                $ret[Constant::STATS_RENDERTIME_ACTUAL] += $tile->getRenderTime();
                $ret[Constant::STATS_RENDERTIME_REF] += $tile->getRenderTimeOnRefMachine();
                $ret[Constant::STATS_PREPTIME_ACTUAL] += $tile->getPrepTime();
            }
        }
        $this->updateStatsCache([], $ret);

        $status = Constant::WAITING;
        if ($this->getStatus() == Constant::PAUSED) {
            // keep a paused project, paused
            $status = Constant::PAUSED;
        }
        elseif ($ret[Constant::FINISHED] == $ret[Constant::STATS_TOTAL] && $ret[Constant::STATS_TOTAL] > 0) {
            $status = Constant::FINISHED;
            GlobalInject::getMain()->countPublicActiveProjects(false); // regenerate cache
        }
        elseif ($ret[Constant::FINISHED] == $ret[Constant::STATS_TOTAL] && $ret[Constant::STATS_TOTAL] == 0) {
            $status = Constant::WAITING;
        }
        elseif ($ret[Constant::PROCESSING] > 0) {
            $status = Constant::PROCESSING;
        }
        $this->setStatus($status);
        GlobalInject::getEntityManager()->flush($this);

        return $this->getCachedStatistics();
    }

    public function getCachedStatistics(): array {
        return [
            Constant::STATS_TOTAL => $this->getCacheTotal(),
            Constant::STATS_RENDERTIME_ACTUAL => $this->getCacheRenderTimeActual(),
            Constant::STATS_RENDERTIME_REF => $this->getCacheRenderTimeRef(),
            Constant::STATS_PREPTIME_ACTUAL => $this->getCachePrepTimeActual(),
            Constant::WAITING => $this->getCacheWaiting(),
            Constant::PROCESSING => $this->getCacheProcessing(),
            Constant::FINISHED => $this->getCacheFinished(),
            Constant::STATS_LAST_VALIDATION => $this->getCacheLastValidation(),
        ];
    }

    public function updateStatsCache(array $updates, ?array $setters = null): bool {
        $em = GlobalInject::getEntityManager();
        $em->wrapInTransaction(function () use ($em, $updates, $setters) {
            $em->refresh($this, LockMode::PESSIMISTIC_WRITE);

            foreach ($updates as $k => $v) {
                switch ($k) {
                    case Constant::STATS_TOTAL :
                        $this->setCacheTotal($this->getCacheTotal() + $v);
                        break;
                    case Constant::STATS_RENDERTIME_ACTUAL :
                        $this->setCacheRenderTimeActual($this->getCacheRenderTimeActual() + $v);
                        break;
                    case Constant::STATS_PREPTIME_ACTUAL :
                        $this->setCachePrepTimeActual($this->getCachePrepTimeActual() + $v);
                        break;
                    case Constant::STATS_RENDERTIME_REF :
                        $this->setCacheRenderTimeRef($this->getCacheRenderTimeRef() + $v);
                        break;
                    case Constant::WAITING :
                        $this->setCacheWaiting($this->getCacheWaiting() + $v);
                        break;
                    case Constant::PROCESSING :
                        $this->setCacheProcessing($this->getCacheProcessing() + $v);
                        break;
                    case Constant::FINISHED :
                        $this->setCacheFinished($this->getCacheFinished() + $v);
                        break;
                    case Constant::STATS_LAST_VALIDATION :
                        $this->setCacheLastValidation($this->getCacheLastValidation() + $v);
                        break;
                }
            }

            if (is_array($setters)) {
                foreach ($setters as $k => $v) {
                    switch ($k) {
                        case Constant::STATS_TOTAL :
                            $this->setCacheTotal($v);
                            break;
                        case Constant::STATS_RENDERTIME_ACTUAL :
                            $this->setCacheRenderTimeActual($v);
                            break;
                        case Constant::STATS_RENDERTIME_REF :
                            $this->setCacheRenderTimeRef($v);
                            break;
                        case Constant::WAITING :
                            $this->setCacheWaiting($v);
                            break;
                        case Constant::PROCESSING :
                            $this->setCacheProcessing($v);
                            break;
                        case Constant::FINISHED :
                            $this->setCacheFinished($v);
                            break;
                        case Constant::STATS_LAST_VALIDATION :
                            $this->setCacheLastValidation($v);
                            break;
                    }
                }
            }

        });

        if ($this->getCacheTotal() < 0 || $this->getCacheRenderTimeActual() < 0 || $this->getCacheRenderTimeRef() < 0 || $this->getCacheWaiting() < 0 || $this->getCacheProcessing() < 0 || $this->getCacheFinished() < 0 || $this->getCacheLastValidation() < 0) {
            Logger::critical(__METHOD__.' invalid stats for project '.$this->getId().' udates: '.json_encode($updates).' setters:'.json_encode($setters).' data: [cache: '.$this->getCacheTotal().', rendertimeactual: '.$this->getCacheRenderTimeActual().', rendertimeRef: '.$this->getCacheRenderTimeRef().', waiting: '.$this->getCacheWaiting().', processing: '.$this->getCacheProcessing().', finished:  '.$this->getCacheFinished().' lastvalidation: '.$this->getCacheLastValidation().']'.json_encode(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)));

            // only add a new task if no other are there
            if (GlobalInject::getEntityManager()->getRepository(Task::class)->countProjectGenerateStats($this) == 0) {
                $daemon = new Daemon(GlobalInject::getEntityManager(), GlobalInject::getConfigService(), GlobalInject::getEntityManager()->getRepository(Task::class));
                $task = new TaskProjectGenerateStats();
                $task->setProject($this->getId());
                $daemon->addTask($task);
            }
            return false;
        }

        return true;
    }

    public function emailOwnerOnFinish(): int {
        Logger::debug(__METHOD__.'(id:'.$this->getId().')');

        if ($this->getOwner()->noficationIsEnabled(User::NOTIFICATION_MASK_ON_PROJECT_FINISHED) == false) {
            return 0; // do nothing
        }

        $config = GlobalInject::getConfigService()->getData();

        $this_frames = $this->tiles();
        $cumulated_time = 0;
        $time_first_frame = time() + 10; // max value
        $time_last_frame = 0; // min value
        $owner = array('time' => 0, 'frames' => 0);
        $others = array('time' => 0, 'frames' => 0);
        $owner_login = $this->getOwner()->getId();

        foreach ($this_frames as $a_frame) {
            if ($a_frame->getStatus() == Constant::FINISHED) {
                $cumulated_time += $a_frame->getRenderTime();

                if ($a_frame->getRequestTime() < $time_first_frame) {
                    $time_first_frame = $a_frame->getRequestTime();
                }
                if ($time_last_frame < $a_frame->getValidationTime()) {
                    $time_last_frame = $a_frame->getValidationTime();
                }

                if ($owner_login == $a_frame->getUser()->getId()) {
                    $owner['frames'] += 1;
                    $owner['time'] += $a_frame->getRenderTime();
                }
                else {
                    $others['frames'] += 1;
                    $others['time'] += $a_frame->getRenderTime();
                }
            }
        }
        $percentage_frame = 0.0;
        $percentage_time = 0.0;
        if ($owner['frames'] + $others['frames'] > 0) {
            $percentage_frame = (float)(100.0 * $owner['frames']) / (float)($owner['frames'] + $others['frames']);
        }
        if ($owner['time'] + $others['time'] > 0) {
            $percentage_time = (float)(100.0 * $owner['time']) / (float)($owner['time'] + $others['time']);
        }
        $subject = sprintf('The project \'%s\' is finished', $this->getName());
        $url_admin = $config['site']['url'].GlobalInject::getRouter()->generate("app_project_manage", ['project' => $this->getId()]);
        $content_user = sprintf("The project %s is finished. You can now download your frames.<br>Cumulated time of render: %s<br>Real duration of render: %s",
            '<a href="'.$url_admin.'">'.$this->getName().'</a>',
            Misc::humanTime($cumulated_time, false),
            Misc::humanTime($time_last_frame - $time_first_frame, false)
        );
        $content_user .= '<br><br><i>Since disk space is very precious, your project will be automatically deleted in '.($config['storage']['duration']['project']['rendered'] - 1).' days.</i>';

        if (isset($config['email']['footer']['project_finish']) && $config['email']['footer']['project_finish'] != '') {
            $content_user .= $config['email']['footer']['project_finish'];
        }

        $content_user .= '<br><br>'.sprintf('Thanks for using %s.', $config['site']['name']);
        Mail::sendamail($this->getOwner()->getEmail(), $subject, $content_user, 'project_finish');
        return 0;
    }

    public function setTokenOwnerShepherdServer(): bool {
        return $this->shepherd->setBlendOwnerToken($this);
    }

    public function setTokenThumbnailShepherdServer(): bool {
        return $this->shepherd->setBlendThumbnailToken($this);
    }

    public function getMp4PreviewOnShepherd(): string {
        if ($this->setTokenOwnerShepherdServer()) {
            return $this->shepherd->getId().'/blend/'.$this->getShepherdOwnerToken().'/'.$this->getId().'/mp4/preview';
        }
        return "";
    }

    public function getMp4FinalOnShepherd(): string {
        if ($this->setTokenOwnerShepherdServer()) {
            return $this->shepherd->getId().'/blend/'.$this->getShepherdOwnerToken().'/'.$this->getId().'/mp4/final';
        }
        return "";
    }

    public function getZipOnShepherd(): string {
        if ($this->setTokenOwnerShepherdServer()) {
            return $this->shepherd->getId().'/blend/'.$this->getShepherdOwnerToken().'/'.$this->getId().'/zip';
        }
        return "";
    }

    public function pause(): void {
        $this->setStatus(Constant::PAUSED);

        $renderingTiles = $this->tilesWithStatus(Constant::PROCESSING);
        foreach ($renderingTiles as $tile) {
            $tile->reset();
        }

        GlobalInject::getEntityManager()->flush();
    }

    public function resume(): void {
        $this->setStatus(Constant::WAITING);
        $this->updateOwnerPointsAttribute();
        GlobalInject::getEntityManager()->flush();
    }

    public function updateOwnerPointsAttribute(): void {
        $points = $this->owner->getExtraPointsFromTeam();

        $this->setOwnerPointsOnlastupdate($points);
        GlobalInject::getEntityManager()->flush();
    }

    /**
     * @return Tile[]
     */
    public function tilesWithStatus(string $status_): array {
        return GlobalInject::getEntityManager()->getRepository(Tile::class)->tilesWithStatusFor($this, $status_);
    }

    /**
     * @return Frame[]
     */
    public function framesWithStatus(string $status_): array {
        return GlobalInject::getEntityManager()->getRepository(Frame::class)->findBy(['project' => $this->getId(), 'cache_status' => $status_]);
    }

    public function actionOnFinish(): void {
        $this->setStatus(Constant::FINISHED);
        GlobalInject::getEntityManager()->flush($this);

        GlobalInject::getMain()->countPublicActiveProjects(false); // regenerate cache

        Logger::info("project '".$this->getName()."'(id:".$this->getId().") is finished total rendertime: ".Misc::humanTime($this->getCacheRendertimeActual()));

        $pastProject = GlobalInject::getEntityManager()->getRepository(PastProject::class)->find($this->getId());
        if (is_object($pastProject)) {
            $pastProject->setRenderTime($this->getCacheRendertimeActual());
            $pastProject->setFinish(time());
            GlobalInject::getEntityManager()->flush($pastProject);
        }
    }

    public function sendNotificationEmailOnFirstFrameFinished(): void {
        $config = GlobalInject::getConfigService()->getData();
        if ($this->getEmailSent() == false) {
            $this->setEmailSent(true); // even if the user doesn't want an email, because we do send to sent it once
            GlobalInject::getEntityManager()->flush($this);

            if ($this->owner->noficationIsEnabled(User::NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED)) {
                $email = $this->owner->getEmail();
                $url = $config['site']['url'].GlobalInject::getRouter()->generate("app_project_frames", ['project' => $this->getId(), 'interval' => 1]);
                $content = sprintf("The first frame of your project '%s' has been rendered.<br>You can check it at <a href=\"%s\">this address</a>", $this->getName(), $url);
                Mail::sendamail($email, sprintf('The first frame of your project \'%s\' has been rendered.', $this->getName()), $content, 'project_first_frame');
            }
        }
    }

    public function criterionScore(SessionInterface $phpSession, Session $session): float {
        $total = 0.0;

        $criterions = $session->criterions();
        foreach ($criterions as $criterion_name => $criterion_object) {
            $val = $criterion_object->ponderation() * $criterion_object->scoreProject($phpSession, $session, $this);
            if ($val < 0) { // a negative val means AVOID
                return -1.0;
            }
            $total += $val;
        }
        return $total;
    }

    public function addBanforCriterionBanJobFromUserSendError(SessionInterface $phpSession): void {
        $ban = $phpSession->get('ban', []);
        $ban[$this->getId()] = $this->getId();
        $phpSession->set('ban', $ban);
    }

    public function updateMaxMemoryUsed(int $mem): void {
        if ($mem > $this->getMaxMemoryUsed()) {
            $this->setMaxMemoryUsed($mem);
            GlobalInject::getEntityManager()->flush();
        }
    }

    public function updateVramFailure(int $mem): void {
        if ($mem > $this->getVramFailure()) {
            $this->setVramFailure($mem);
            GlobalInject::getEntityManager()->flush();
        }
    }

    public function isFinished(): bool {
        $stats = $this->getCachedStatistics();
        return $stats[Constant::FINISHED] == $stats[Constant::STATS_TOTAL];
    }

    public function fullPathOfChunk(string $chunk): string {
        $config = GlobalInject::getConfigService()->getData();

        return $config['storage']['path'].'projects'.'/'.$this->getChunkStorageName($chunk);
    }

    public function getChunkStorageName(string $chunk): string {
        return $this->getId().'_'.$chunk;
    }

    // ACL stuffs
    public function canManageProject(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        if (array_key_exists($a_user_->getId(), $this->cache_acl_manager) == false) {
            $ret = false;

            if ($this->owner->getId() == $a_user_->getId()) {
                $ret = true;
            }
            elseif ($a_user_->isModerator()) {
                $ret = true;
            }
            elseif ($a_user_->canModifyAllProjects()) {
                $ret = true;
            }
            elseif ($a_user_->getACLManageProject($this->getId())) {
                $ret = true;
            }

            $this->cache_acl_manager[$a_user_->getId()] = $ret;
        }
        return $this->cache_acl_manager[$a_user_->getId()];
    }

    public function addManager(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_MANAGE_PROJECT, $a_user_->getId(), $this->getId());
        if (count($liaisons) > 0) {
            return false;
        }

        $l = new Liaison();
        $l->setType(Liaison::USER_MANAGE_PROJECT);
        $l->setPrimary((string)$a_user_->getId());
        $l->setSecondary((string)$this->getId());
        $l->setValue('1'); // not use for now

        unset($this->cache_acl_manager[$a_user_->getId()]); // clear the cache

        GlobalInject::getEntityManager()->persist($l);
        GlobalInject::getEntityManager()->flush();
        return true;
    }

    public function delManager(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        unset($this->cache_acl_manager[$a_user_->getId()]); // clear the cache

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_MANAGE_PROJECT, $a_user_->getId(), $this->getId());
        if (count($liaisons) == 0) {
            return false;
        }

        $l = array_pop($liaisons);
        GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        return true;
    }

    public function canSeeProject(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        $owner = $this->owner->getId() == $a_user_->getId();
        if ($owner) {
            return true;
        }

        // not the owner, using acl
        if ($a_user_->isAdmin()) {
            return true;
        }
        if ($a_user_->isModerator()) {
            return true;
        }
        if ($a_user_->canSeeAllProjects()) {
            return true;
        }
        if ($a_user_->canModifyAllProjects()) {
            return true;
        }
        if ($this->canManageProject($a_user_)) {
            return true;
        }
        return false;
    }

    public function canRenderProject(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        if ($this->cache_acl_renderer->exists($a_user_->getId()) == false) {
            if ($this->getPublicRender()) {
                $val = true;
            }
            else {
                if ($this->owner->getId() == $a_user_->getId()) {
                    $val = true;
                }
                else {
                    $val = $a_user_->getACLRendererProject($this->getId());
                }
            }
            $this->cache_acl_renderer->add($val, $a_user_->getId());
        }

        return $this->cache_acl_renderer->get($a_user_->getId());
    }

    public function addRenderer(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_RENDER_PROJECT, $a_user_->getId(), $this->getId());
        if (count($liaisons) > 0) {
            return false;
        }

        $this->cache_acl_renderer->remove($a_user_->getId()); // clear the cache

        $l = new Liaison();
        $l->setType(Liaison::USER_RENDER_PROJECT);
        $l->setPrimary($a_user_->getId());
        $l->setSecondary((string)$this->getId());
        $l->setValue('1'); // not use for now

        GlobalInject::getEntityManager()->persist($l);
        GlobalInject::getEntityManager()->flush();
        return true;
    }

    public function delRenderer(?User $a_user_): bool {
        if (is_object($a_user_) == false) {
            return false;
        }

        $this->cache_acl_renderer->remove($a_user_->getId()); // clear the cache

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_RENDER_PROJECT, $a_user_->getId(), $this->getId());
        if (count($liaisons) == 0) {
            return false;
        }

        $l = array_pop($liaisons);
        GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        return true;
    }

    public function addTeamRenderer(?Team $a_team_): bool {
        if (is_object($a_team_) == false) {
            return false;
        }

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::TEAM_RENDER_PROJECT, $a_team_->getId(), $this->getId());
        if (count($liaisons) > 0) {
            return false;
        }

        $this->cache_acl_renderer->remove($a_team_->getId()); // clear the cache

        $l = new Liaison();
        $l->setType(Liaison::TEAM_RENDER_PROJECT);
        $l->setPrimary((string)$a_team_->getId());
        $l->setSecondary((string)$this->getId());
        $l->setValue('1'); // not use for now

        GlobalInject::getEntityManager()->persist($l);
        GlobalInject::getEntityManager()->flush();
        return true;
    }

    public function delTeamRenderer(?Team $a_team_): bool {
        if (is_object($a_team_) == false) {
            return false;
        }

        $this->cache_acl_renderer->set(array()); // clear the cache

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::TEAM_RENDER_PROJECT, $a_team_->getId(), $this->getId());
        if (count($liaisons) == 0) {
            return false;
        }

        $l = array_pop($liaisons);
        GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        return true;
    }

    public function estinationEndOfRender(): int {
        // calculate the speed of rendering on the last two hours and extrapolate.

        if ($this->getStatus() == Constant::PAUSED) {
            return PHP_INT_MAX;
        }

        $now = time();
        $frames = $this->tiles();
        $frames_faites = 0;
        $debut_rendu = $now;
        $limit = 7200;
        $remaining_frame = count($frames);

        foreach ($frames as $a_frame) {
            if ($a_frame->getStatus() == Constant::FINISHED) {
                $remaining_frame--;
                if ($now - $a_frame->getValidationTime() < $limit) {
                    $frames_faites++;
                    if ($a_frame->getValidationTime() < $debut_rendu) {
                        $debut_rendu = $a_frame->getValidationTime();
                    }
                }
            }
        }
        if ($frames_faites == 0) {
            return PHP_INT_MAX;
        }
        else {
            $temps_frames_faites = time() - $debut_rendu;
            return $remaining_frame * $temps_frames_faites / $frames_faites;
        }
    }

    public function getAverageRenderTimeMachineRef(): int {
        $stats = $this->getCachedStatistics();
        if ($stats[Constant::FINISHED] != 0) {
            return $stats[Constant::STATS_RENDERTIME_REF] / $stats[Constant::FINISHED];
        }
        return 0;
    }

    public function getAveragePrepTime(): int {
        $stats = $this->getCachedStatistics();
        if ($stats[Constant::FINISHED] != 0) {
            return $stats[Constant::STATS_PREPTIME_ACTUAL] / $stats[Constant::FINISHED];
        }
        return 0;
    }

    public function getAverageRenderTime(): float {
        $stats = $this->getCachedStatistics();
        if ($stats[Constant::FINISHED] != 0) {
            return $stats[Constant::STATS_RENDERTIME_ACTUAL] / $stats[Constant::FINISHED];
        }
        return 0;
    }

    public function updatePoints(?Tile $tile): float {
        Logger::debug(__METHOD__."($tile)");

        if (is_object($tile) == false) {
            return 0;
        }

        $points_earned = 0;
        $reward = 0;

        $renderer = $tile->getUser();
        if (is_object($renderer)) {
            // when sponsoring a render, the actual renderer will get the points earn but not the 'regular' points
            // a random user from their sponsor user list will get the points.
            $points = $tile->getPointsForRenderer();
            $reward = $points;
            $renderer->setPointsEarn($renderer->getPointsEarn() + $points);

            if ($renderer->getSponsorGive()) {
                $sponsoreds = $renderer->getSponsorUsers(true);
                if (count($sponsoreds) == 0) {
                    // no sponsored user
                    $renderer->setPoints($renderer->getPoints() + $points);
                    $points_earned = $points;
                }
                else {
                    $a_sponsored = $sponsoreds[array_rand($sponsoreds)];
                    $a_sponsored->setPoints($a_sponsored->getPoints() + $points);
                    $points_earned = 0;
                }
            }
            else {
                $renderer->setPoints($renderer->getPoints() + $points);
                $points_earned = $points;
            }
        }

        $owner = $this->getOwner();
        if (is_object($owner)) {
            $points = $tile->getPointsForOwner();
            $owner->setPoints(max(0.0, $owner->getPoints() + $points)); // owner can not have a negative balance
            $tile->setCost(-1.0 * $points);
            $tile->setReward($reward);
            GlobalInject::getEntityManager()->getRepository(Frame::class)->updateCache($tile->getFrame(), $tile->getRenderTime(), $tile->getRenderTimeOnRefMachine(), -1.0 * $points);
        }
        GlobalInject::getEntityManager()->flush();

        return $points_earned;
    }

    public function postValidate(): void {
        GlobalInject::getEntityManager()->refresh($this);
        $config = GlobalInject::getConfigService()->getData();
        //         Logger::debug('Job::postValidate (id:'.$this->getId().')');
        if ($this->getBlocked() == Project::PREEMPTIVE_BLOCK_CONST) {
            $limit_render_time = $config['power']['rendertime_max_reference'] * 1.6;

            foreach ($this->tilesWithStatus(Constant::FINISHED) as $frame) {
                $maxi = $frame->getComputeDevice()->getPowerConst() * $limit_render_time;
                $actual = $frame->getDevicePower() * $frame->getRenderTime();
                if ($actual < $maxi) {
                    GlobalInject::getEntityManager()->getRepository(Project::class)->unblock($this);
                    break;
                }
            }
        }

        $this->sendNotificationEmailOnFirstFrameFinished();

        if ($this->isFinished()) {
            Logger::debug(__METHOD__.' project '.$this->getId().' is now finished');
            $this->actionOnFinish();
        }
    }

    public function getCost(): float {
        // TODO : do in SQL (it will be faster)
        $ret = 0.0;
        foreach ($this->tilesWithStatus(Constant::FINISHED) as $f) {
            $ret += $f->getCost();
        }

        return $ret;
    }

    public function reset(): void {
        foreach ($this->tilesWithStatus(Constant::FINISHED) as $tile) {
            $tile->reset();
        }
        $this->setMaxMemoryUsed(0);
        $this->setStatus(Constant::WAITING);
        $this->setEmailSent(false);
        $this->setZipGenerated(0);
        $this->setMp4PreviewGenerated(0);
        $this->setMp4FinalGenerated(0);
        GlobalInject::getEntityManager()->flush();

        $this->updateOwnerPointsAttribute();

        $this->generateStatsCache();
        GlobalInject::getEntityManager()->flush();
    }

    public function shouldHideFromUser(): bool {
        return $this->id == Project::COMPUTE_METHOD_PROJECT_ID || $this->id == Project::POWER_DETECTION_PROJECT_ID;
    }

    public function isSingleFrame(): bool {
        return $this->frames()->count() <= 1;
    }
}
