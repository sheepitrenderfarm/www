<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Constant;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\Misc;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TileReal extends Tile {
    /**
     * Either the machine (i.e. the CPU) or the GPU
     **/
    public function getComputeDevice(): ?BaseProcessingUnit {
        if (is_object($this->getGPU())) {
            return $this->getGPU();
        }
        else {
            return $this->getCpu();
        }
    }

    public function reset(bool $give_points_to_renderer = false): bool {
        Logger::debug('Frame::reset (project='.$this->getFrame()->getProject()->getId().', number='.$this->getNumber().') '.$this);
        if ($give_points_to_renderer && $this->getStatus() == Constant::PROCESSING) {
            $renderer = $this->getUser();
            if (is_object($renderer)) {
                $points = $this->getPointsForRenderer();
                $renderer->setPoints($renderer->getPoints() + $points);
                GlobalInject::getEntityManager()->flush($renderer);
                Logger::debug('Frame::reset give '.$points.' to renderer '.$renderer->getId().' (do not remove from the owner since it\'s a reset/error)');

                // also update the renderer's session points

                $session = GlobalInject::getEntityManager()->getRepository(Session::class)->find($this->getSession());
                if (is_object($session)) {
                    $session->setPoints($session->getPoints() + $points);
                    GlobalInject::getEntityManager()->flush($session);
                }
            }
        }

        $diff = array($this->getStatus() => -1);
        if ($this->getStatus() == Constant::WAITING) {
            $diff[Constant::WAITING] = 0;
        }
        else {
            $diff[Constant::WAITING] = 1;
        }
        if ($this->getStatus() == Constant::FINISHED) {
            $diff[Constant::STATS_RENDERTIME_ACTUAL] = -1 * $this->getRenderTime();
            $machine = is_object($this->getGpu()) ? $this->getGpu() : $this->getCpu();
            if (is_object($machine)) {
                $diff[Constant::STATS_RENDERTIME_REF] = -1 * $machine->getPower() * $this->getRenderTime() / $machine->getPowerConst();
            }
            GlobalInject::getEntityManager()->getRepository(Frame::class)->updateCache($this->getFrame(), -1 * $this->getRenderTime(), -1 * $this->getRenderTimeOnRefMachine(), -1 * $this->getCost());
        }

        $this->setStatus(Constant::WAITING);
        $this->setUser(NULL);
        $this->setRequestTime(-1);
        $this->setRenderTime(-1);
        $this->setValidationTime(-1);
        $this->setRemainingRenderTime(-1);
        $this->setCpu(null);
        $this->setGPU(null);
        $this->setDevicePower(0);
        $this->setSession(null);
        $this->setCost(0.0);
        $this->setReward(0.0);
        $this->setToken('');

        GlobalInject::getEntityManager()->flush();

        // update parent frame status
        GlobalInject::getEntityManager()->refresh($this->getFrame());
        $this->getFrame()->updateStatus();

        $job = $this->getFrame()->getProject();
        if ($job->getStatus() == Constant::FINISHED) {
            $job->setStatus(Constant::WAITING);
            $job->setZipGenerated(0);
            $job->setMp4FinalGenerated(0);
            $job->setMp4PreviewGenerated(0);
            GlobalInject::getEntityManager()->flush($job);
        }

        $shepherdServer = $job->getShepherd();
        $shepherdServer->tileReset($this);

        // update cache stats

        $job->updateStatsCache($diff);
        if ($job->getStatus() != Constant::PAUSED) { // paused project will stay paused after a tile reset
            // update parent job status
            $project_status = Constant::WAITING;
            $frames_status = [Constant::WAITING => 0, Constant::PROCESSING => 0, Constant::FINISHED => 0];
            foreach ($job->frames() as $aFrame) {
                $frames_status[$aFrame->getStatus()] += 1;
                if ($aFrame->getStatus() == Constant::PROCESSING) {
                    $job->setStatus(Constant::PROCESSING);
                    GlobalInject::getEntityManager()->flush($job);
                    return true;
                }
            }

            if ($frames_status[Constant::FINISHED] == 0 && $frames_status[Constant::PROCESSING] == 0) {
                $project_status = Constant::WAITING;
            }
            elseif ($frames_status[Constant::FINISHED] == $job->frames()->count()) {
                $project_status = Constant::FINISHED;
            }

            $job->setStatus($project_status);
            GlobalInject::getEntityManager()->flush($job);
        }

        return true;
    }

    public function resume(): bool {
//         Logger::debug('Frame::resume (job='.$this->getJob().' , number='.$this->getNumber().')');
        if ($this->getStatus() == Constant::PAUSED) {
            $this->reset();
        }
        return true;
    }

    public function validate(string $picture_file_, Session $session_): int {
        Logger::debug("Frame::validate(...) $this");

        // the main server doesn't support frame's validation anymore

        return -99;
    }

    public function validateFromShepherd(int $rendertime, int $preptime, int $memoryused): int {
        Logger::debug(__METHOD__."(...) $this");
        $config = GlobalInject::getConfigService()->getData();

        if ($this->getStatus() != Constant::PROCESSING) {
            Logger::error(__METHOD__." $this the frame is not in rendering");
            return -1;
        }

        $session = GlobalInject::getEntityManager()->getRepository(Session::class)->find($this->getSession());
        if (is_object($session) == false) {
            Logger::error(__METHOD__." $this failed to import session ".serialize($this->getSession()));
            return -21;
        }

        $this->setPrepTime($preptime);
        $this->setRenderTime($rendertime);
        // check if the render time given by the client is bigger than the actual duration between request and validation
        // to avoid hack on bumping the rendertime
        // in theory $max_duration <=> render time + download time
        $max_duration = max(1, (time() - $this->getRequestTime()) * 1.20); // 20% of error merging
        if ($this->getRenderTime() < 0 || $this->getRenderTime() > $max_duration) {
            Logger::error(__METHOD__."$this on $session render_time is bigger than actual time ($max_duration) reset to max_duration");
            $this->setRenderTime($max_duration);
        }

        $j = $this->getFrame()->getProject();
        $owner = $j->getOwner();

        if ($j->getStatus() == Constant::FINISHED || $j->getStatus() == Constant::PAUSED) {
            Logger::error(__METHOD__.' failed, project (id: '.$j->getId().') does not accept tile validation (project status '.$j->getStatus().'), tile id: '.$this->getId());
            //    @unlink($picture_file_);
            return -6;
        }

        $this->setStatus(Constant::FINISHED);
        $this->setValidationTime(time());

        $j->updateStatsCache(array(Constant::PROCESSING => -1, Constant::FINISHED => 1, Constant::STATS_PREPTIME_ACTUAL => $this->getPrepTime(), Constant::STATS_RENDERTIME_ACTUAL => $this->getRenderTime(), Constant::STATS_RENDERTIME_REF => $this->getDevicePower() * $this->getRenderTime() / $this->getComputeDevice()->getPowerConst()), array(Constant::STATS_LAST_VALIDATION => $this->getValidationTime()));

        if ($j->getStatus() == Constant::PAUSED) {
            // call the re-generation of stats because it will not generate the right value (it will set it as rendering instead of keeping at paused)
            $j->generateStatsCache();
        }

        if (is_object($owner)) {
            $owner->setOrderedFrames((int)($owner->getOrderedFrames() + 1));
            $owner->setOrderedFramesTime((int)($owner->getOrderedFramesTime() + $this->getRenderTime()));

            if ($j->getPublicRender()) {
                $owner->setOrderedFramesPublic((int)($owner->getOrderedFramesPublic() + 1));
            }
        }

        $points_earned = $j->updatePoints($this);

        $session->setRenderedFrames($session->getRenderedFrames() + 1);
        $session->setLastvalidatedframe(time());
        $session->setPoints($session->getPoints() + $points_earned);

        $renderer = $this->getUser();
        if (is_object($renderer)) {
            $renderer->setRenderedFrames((int)($renderer->getRenderedFrames() + 1));
            $renderer->setRenderTime((int)($renderer->getRenderTime() + $this->getRenderTime()));
            $renderer->setLastRenderedFrame(new \DateTime());

            if ($j->getPublicRender()) {
                $renderer->setRenderedFramesPublic((int)($renderer->getRenderedFramesPublic() + 1));
            }

        }
        else {
            Logger::error(__METHOD__." $this failed to import renderer ".serialize($this->getUser()->getId()));
            GlobalInject::getEntityManager()->flush();
            return -3;
        }

        $team = $renderer->getTeam();
        if (is_object($team)) {
            if ($j->getPublicRender() == false) {
                // private project, only add contribution if the project is renderable by the team
                if (count(GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::TEAM_RENDER_PROJECT, $team->getId(), $j->getId())) > 0) { // TODO: should use an higher level api
                    GlobalInject::getEntityManager()->getRepository(Team::class)->addContribution($team, $session->getUser(), $points_earned);
                }
            }
            else {
                GlobalInject::getEntityManager()->getRepository(Team::class)->addContribution($team, $session->getUser(), $points_earned); // don't use $renderer because it was modified by $j->updatePoints
            }
        }

        // create event
        $e = new Event();
        $e->setType(Event::TYPE_VALIDATE);
        $e->setSession($session->getId());
        $e->setTime(time());
        $e->setUser($session->getUser()->getId());
        $e->setProject($this->getFrame()->getProject()->getId());
        GlobalInject::getEntityManager()->persist($e);

        // update ram&vram
        $gpu = $this->getGPU();
        if (is_object($gpu)) { // rendered by a gpu
            $vram = $gpu->getMemory();
            $current_success = $j->getVramSuccess();
            if ($current_success == 0 || ($vram > 0 && $vram < $current_success)) {
                $j->setVramSuccess($vram);
            }
        }

        if ($memoryused > 0 && $memoryused < $session->getMemory()) {
            $j->updateMaxMemoryUsed($memoryused);
        }


        // update stats on shepherd
        $this->getFrame()->getProject()->getShepherd()->updateRenderedTiles(1);

        GlobalInject::getEntityManager()->flush();

        GlobalInject::getEntityManager()->refresh($this->getFrame());
        $this->getFrame()->updateStatus();

        $j->postValidate();
        return 0;
    }

    public function take(Session $a_session_): int {
        // lock the scene to avoid multiple take of the same frame
        $key = 'frame_'.$this->getFrame()->getId();
        $cache = GlobalInject::getMain()->getPersistentCache();
        $mutex = $cache->lock($key);

        GlobalInject::getEntityManager()->refresh($this);

        if ($this->getStatus() != Constant::WAITING) {
            Logger::error(__METHOD__."($a_session_), frame status is not '".Constant::WAITING."' (frame: $this)");
            $mutex->releaseLock();
            return -3;
        }
        else {
            // default is cpu
            $device_power = $a_session_->getPowerCpu();
            $gpu = null; // no gpu
            if (is_object($a_session_->getGpu())) {
                $project = $this->getFrame()->getProject();
                if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU)) {
                    // use gpu
                    $gpu = $a_session_->getGPU();
                    $device_power = $a_session_->getPowerGpu();
                }
            }

            $this->setStatus(Constant::PROCESSING);
            $this->setUser($a_session_->getUser());
            $this->setRequestTime(time());
            $this->setCPU($a_session_->getCpu());
            $this->setSession($a_session_->getId());
            $this->setGPU($gpu);
            $this->setDevicePower($device_power);
            if ($this->getToken() == '') {
                $do_request = true;
                $this->setToken(uniqid('', true));
            }
            else {
                $do_request = false;
            }

            GlobalInject::getEntityManager()->flush($this);

            Logger::debug("has taken $this");

            $mutex->releaseLock(); // unlock now since the lock will be used in updateStatsCache

            $job = $this->getFrame()->getProject();
            $shepherdServer = $job->getShepherd();
            if ($do_request && $shepherdServer->tileSetRendering($this) == false) {
                // request to shepherd failed
                // reset
                $this->setStatus(Constant::WAITING);
                $this->setUser(NULL);
                $this->setRequestTime(-1);
                $this->setCpu(null);
                $this->setGPU(null);
                $this->setSession(null);
                $this->setToken('');

                GlobalInject::getEntityManager()->flush($this);

                return -6;
            }

            $project = $this->getFrame()->getProject();
            $project->updateStatsCache(array(Constant::PROCESSING => 1, Constant::WAITING => -1));
            if ($project->getStatus() == Constant::WAITING) {
                $project->setStatus(Constant::PROCESSING);
                GlobalInject::getEntityManager()->flush($project);
            }

            $this->getFrame()->setCacheStatus(Constant::PROCESSING);
            GlobalInject::getEntityManager()->flush($this->getFrame());

            return 0;
        }
    }

    public function getPointsForOwner(): float {
        $config = GlobalInject::getConfigService()->getData();

        $project = $this->getFrame()->getProject();
        $owner = $project->getOwner();
        if (is_object($owner)) {
            if ($owner->getId() == $this->getUser()->getId()) {
                Logger::debug(__METHOD__." it's the owner who render the frame");
                // it's the owner who render the frame
                // he gets no penality
                return 0.0;
            }
            else {
                if ($config['rank']['points']['ordered']['minute'] == 0.0) {
                    // free render
                    return 0.0;
                }
                else {
                    // special advent calendar
                    $coef = GlobalInject::getEntityManager()->getRepository(AdventCalendarClaimedDay::class)->getBestRatioOwnerPointsClaimedForUser($this->getUser());
                    //
                    return min(-1.0 * Tile::MINIMUM_REWARD_PER_TILE, $coef * $this->getComputeDevice()->getPowerRatioOverCPU() * $config['rank']['points']['ordered']['minute'] * $this->getRenderTimeOnRefMachine() / 60.0 - 1.0);
                }
            }
        }
        return 0.0;
    }

    public function getPointsForRenderer(): float {
        $config = GlobalInject::getConfigService()->getData();

        $project = $this->getFrame()->getProject();
        $owner = $project->getOwner();
        if (is_object($owner)) {
            $coef = 1.0;

            // special advent calendar
            $coef = max(1.0, GlobalInject::getEntityManager()->getRepository(AdventCalendarClaimedDay::class)->getBestRatioRendererPointsClaimedForUser($this->getUser()));
            //

            if ($owner->getId() == $this->getUser()->getId()) {
                // it's the owner who render the frame

                // he gets fewer points
                $coef = $config['rank']['points']['coef_owner'];

            }

            return max(Tile::MINIMUM_REWARD_PER_TILE, $config['rank']['points']['rendered']['minute'] * $this->getComputeDevice()->getPowerRatioOverCPU() * $coef * $this->getRenderTimeOnRefMachineWithCoefAligned() / 60.0);
        }
        return 0.0;
    }

    public function getRenderTimeOnRefMachine(): float {
        if (is_null($this->getComputeDevice())) {
            return 0.0;
        }
        return $this->getDevicePower() * $this->getRenderTime() / $this->getComputeDevice()->getPowerConst();
    }

    public function getRenderTimeOnRefMachineWithCoefAligned(): float {
        $config = GlobalInject::getConfigService()->getData();

        if ($this->getComputeDevice() instanceof CPU) {
            return Misc::powerAlignCpu($this->getDevicePower()) * $this->getRenderTime() / $config['power']['cpu']['const'];
        }
        else {
            return Misc::powerAlignGpu($this->getDevicePower()) * $this->getRenderTime() / $config['power']['gpu']['const'];
        }
    }

    public function getUrlThumbnailOnShepherd(bool $ondemand = false): string {
        $project = $this->getFrame()->getProject();
        $shepherd = $project->getShepherd();
        if ($shepherd->getTimeout() == false) {
            $project->setTokenThumbnailShepherdServer();
            return $shepherd->getId().'/thumb/'.$project->getShepherdThumbnailToken().'/'.$project->getId().'/tile/'.$this->getId().'/thumbnail/'.($ondemand ? '1' : '0');
        }

        return "";
    }

    public function getUrlFullOnShepherd(): string {
        $project = $this->getFrame()->getProject();
        $shepherd = $project->getShepherd();
        if ($project->setTokenOwnerShepherdServer()) {
            return $shepherd->getId().'/blend/'.$project->getShepherdOwnerToken().'/'.$project->getId().'/tile/'.$this->getId().'/full';
        }
        return "";
    }

    public function getEstimatedRenderTime(): int {
        if ($this->getFrame()->getProject()->getCacheFinished() > 0) {
            if (is_object($this->getComputeDevice())) {
                $power = $this->getComputeDevice()->getPower();
                if ($power > 0 && $this->getComputeDevice()->getPowerPercent() != 0) {
                    $coef_power_session = 1.0 / (((float)$this->getComputeDevice()->getPowerPercent()) / 100.0);
                    return (int)($coef_power_session * (float)($this->getFrame()->getProject()->getCacheRendertimeRef()) / (float)($this->getFrame()->getProject()->getCacheFinished()));
                }
            }
        }
        return 0;
    }
}
