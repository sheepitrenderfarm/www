<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\AwardRepository;
use App\Service\GlobalInject;
use App\Utils\Mail;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Award
 */
#[ORM\Table(name: 'award')]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\DiscriminatorColumn(name: 'type', type: Types::STRING)]
#[ORM\Entity(repositoryClass: AwardRepository::class)]
abstract class Award extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'awards')]
    protected User $user;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    public function getId(): ?int {
        return $this->id;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getDoctrineType(): string {
        return strtolower((new \ReflectionClass($this))->getShortName());
    }

    public function getShortName(): string {
        return (new \ReflectionClass($this))->getShortName();
    }

    /**
     * Display the award on the user account page
     */
    public function isDisplayable(): bool {
        return true;
    }

    /**
     * ret false for old award
     **/
    public function canBeEarn(): bool {
        return true;
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        return false;
    }

    public function actionOnEarn(?User $user_, ?Tile $tile, ?Session $session): void {
        if ($this->canBeEarn() == false) {
            return;
        }

        if (is_object($user_)) {
            $value = $this->reward();

            $user_->setPoints($user_->getPoints() + $value);
            GlobalInject::getEntityManager()->flush($user_);

            if ($user_->noficationIsEnabled(User::NOTIFICATION_MASK_AWARD)) {
                $content_user = sprintf('You earned the %s award!<br><img src="cid:%s" alt="%s" /><br>This award gives you an extra %s points.<br>No need to do anything, they have been automatically added to your account.<br><br><i>You can unsubscribe to the notification on this link: <a href="%s">unsubscribe</a>. Don\'t worry you will still get the points even if you don\'t want to be notified.</i>', str_replace('Award', '', $this->getShortName()), basename($this->imagePath()), basename($this->imagePath()), number_format($value), $user_->getGlobalUnsubscribeUrl());
                Mail::sendamail($user_->getEmail(), 'You earned an award', $content_user, 'earned_award', array(__DIR__.'/../../public/'.$this->imagePath()));
            }
        }
    }

    /**
     * How difficult is to earn ?
     **/
    public function level(): int {
        return $this->reward();
    }

    abstract public function humanDescription(): string;

    /**
     * Reward in points
     */
    abstract public function reward(): int;

    abstract public function category(): string;

    abstract public function imagePath(): string;

    /**
     * Some awards are not earn after rendering a frame, they are giving at fixed time
     * This function will be called every hour
     **/
    public function cronHourly(): void {
    }

    /**
     * Some awards are not earn after rendering a frame, they are giving at fixed time
     * This function will be called every day
     **/
    public function cronDaily(): void {
    }

    /**
     * Get to a user who much is remaining to get the award
     **/
    public function remainingHumanText(User $user): ?string {
        return null;
    }
}
