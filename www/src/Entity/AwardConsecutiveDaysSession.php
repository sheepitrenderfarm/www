<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;

abstract class AwardConsecutiveDaysSession extends Award {
    public const MINIMUM_RENDERTIME_PER_DAY = 8;// in hours

    abstract public function getObjectiveInDays(): int;

    public function humanDescription(): string {
        return sprintf("Have a client rendering for %d consecutive days.\nThe client needs to be connected for at least %d continuous hours per day.\nThe award is calculated and given out once per day at 12PM CEST/CET.", $this->getObjectiveInDays(), AwardConsecutiveDaysSession::MINIMUM_RENDERTIME_PER_DAY);
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        return $user->getConsecutiveRenderDaysCurrent() >= $this->getObjectiveInDays();
    }

    public function imagePath(): string {
        return '/media/image/award/award_sessionduration_'.$this->getObjectiveInDays().'day.png';
    }

    public function category(): string {
        return 'session_duration';
    }

    public function cronDaily(): void {
        foreach (GlobalInject::getMain()->getBestRenderersFromCache() as $bestRenderer) {
            $user = GlobalInject::getEntityManager()->getRepository(User::class)->find($bestRenderer->getLogin());
            if (is_object($user) && $user->getConsecutiveRenderDaysCurrent() >= $this->getObjectiveInDays()) {
                $user->giveAward($this->getShortName());
            }
        }
    }
}
