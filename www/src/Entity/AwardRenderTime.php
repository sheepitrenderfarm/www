<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

abstract class AwardRenderTime extends Award {

    abstract public function getObjective(): int;

    public function humanDescription(): string {
        if ($this->getObjective() > 3600) {
            return sprintf('%s hours of render time', number_format($this->getObjective() / 3600));
        }
        else {
            return sprintf('%s hour of render time', number_format($this->getObjective() / 3600));
        }
    }

    public function earn(?User $user, ?Tile $tile, ?Session $session): bool {
        if (is_object($user) == false) {
            return false;
        }

        return ($user->getRenderTime() > $this->getObjective());
    }

    public function imagePath(): string {
        return '/media/image/award/award_rendertime_'.($this->getObjective() / 3600).'h.png';
    }

    public function category(): string {
        return 'render_time';
    }

    public function remainingHumanText(User $user): ?string {
        return sprintf('%s hours remaining to get this award', number_format(($this->getObjective() - $user->getRenderTime()) / 3600));
    }
}
