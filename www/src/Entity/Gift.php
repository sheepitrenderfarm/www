<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\GiftRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gift
 */
#[ORM\Table(name: 'gift')]
#[ORM\Index(name: 'owner', columns: ['owner'])]
#[ORM\UniqueConstraint(name: 'id', columns: ['id'])]
#[ORM\Entity(repositoryClass: GiftRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Gift extends Entity {
    #[ORM\Column(name: 'id', type: Types::STRING, length: 10, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'gifts', cascade: ['persist'])]
    protected User $owner;

    #[ORM\Column(name: 'value', type: Types::INTEGER, nullable: false)]
    protected int $value;

    #[ORM\Column(name: 'expiration', type: Types::INTEGER, nullable: false)]
    protected int $expiration;

    #[ORM\Column(name: 'comment', type: Types::TEXT, length: 65535, nullable: false)]
    protected string $comment = '';

    #[ORM\Column(name: '`key`', type: Types::STRING, length: 200, nullable: false)]
    protected string $key;

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getOwner(): User {
        return $this->owner;
    }

    public function setOwner(User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getValue(): int {
        return $this->value;
    }

    public function setValue(int $value): self {
        $this->value = $value;

        return $this;
    }

    public function getExpiration(): int {
        return $this->expiration;
    }

    public function setExpiration(int $expiration): self {
        $this->expiration = $expiration;

        return $this;
    }

    public function getComment(): string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getKey(): ?string {
        return $this->key;
    }

    public function setKey(string $key): self {
        $this->key = $key;

        return $this;
    }
}
