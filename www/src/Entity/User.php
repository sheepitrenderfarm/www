<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseUser;
use App\Constant;
use App\Persistent\Cache;
use App\Repository\UserRepository;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\Mail;
use App\Utils\Misc;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 */
#[ORM\Table(name: 'user')]
#[ORM\Index(name: 'team', columns: ['team'])]
#[ORM\UniqueConstraint(name: 'id', columns: ['id'])]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class User extends BaseUser implements UserInterface, PasswordAuthenticatedUserInterface {
    public const REMOVED_USER_ID = 'removed_user';

    public const ACL_MASK_CAN_DO_RENDERING = 1; // 2^0
    public const ACL_MASK_CAN_DO_ADD_PROJECT = 2;  // 2^1
    // 2^2
    // 2^3
    // 2^4
    // 2^5
    // 2^6
    public const ACL_MASK_CAN_SEE_ALL_PROJECT = 128; // 2^7
    public const ACL_MASK_CAN_MODIFY_ALL_PROJECT = 256; // 2^8
    public const ACL_MASK_MODERATOR = 512;    // 2^9
    public const ACL_MASK_ADMIN = 1024; // 2^10
    // 2^11
    // 2^12
    public const ACL_MASK_SUPER_ADMIN = 16384; // 2^14
    public const ACL_MASK_CAN_DISABLE_THUMBNAIL = 8192; // 2^13

    public const ACL_METHODS = array('canSeeAllProjects', 'canModifyAllProjects', 'isModerator', 'isAdmin', 'isSuperAdmin', 'canDisableFrameThumbnail');

    // 2^0
    public const NOTIFICATION_MASK_ON_PROJECT_FINISHED = 2;  // 2^1
    public const NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED = 4; // 2^2
    public const NOTIFICATION_MASK_NEWSLETTER = 8; // 2^3
    public const NOTIFICATION_MASK_AWARD = 16; // 2^4
    public const NOTIFICATION_MASK_REQUEST_JOIN_TEAM = 32; // 2^5
    public const NOTIFICATION_MASK_REQUEST_JOIN_TEAM_ACCEPTED = 64; // 2^6
    // 2^7
    public const NOTIFICATION_MASK_AFFILIATE_REFERRED = 256; // 2^8
    public const NOTIFICATION_MASK_AFFILIATE_REFERER = 512; // 2^9
    public const NOTIFICATION_MASK_PATREON = 1024; // 2^10
    public const NOTIFICATION_MASK_SESSION_BLOCKED = 2048; // 2^11

    public const SCHEDULER_MASK_RENDERMYPROJECTFIRST = 1; // 2^0
    public const SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST = 2; // 2^1
    public const SCHEDULER_MASK_HEAVY_PROJECT = 4; // 2^2

    protected Cache $cache_acl_renderer;
    protected Cache $cache_acl_manager;
    protected Cache $cache_blocked_renderer;
    protected Cache $cache_blocked_owner;
    protected Cache $cache_users_who_blocked_me;
    protected Cache $cache_high_priorty_users;

    #[ORM\Column(name: 'last_web_session', type: Types::INTEGER, nullable: false)]
    protected int $last_web_session = 0;

    #[ORM\Column(name: 'last_session', type: Types::INTEGER, nullable: false)]
    protected int $last_session = 0;

    #[ORM\Column(name: 'last_rendered_frame', type: Types::DATE_MUTABLE, nullable: true)]
    protected ?DateTimeInterface $last_rendered_frame = null;

    #[ORM\Column(name: 'website', type: Types::STRING, length: 255, nullable: false)]
    protected string $website = '';

    #[ORM\Column(name: 'socialnetwork_facebook', type: Types::STRING, length: 100, nullable: false)]
    protected string $socialnetwork_facebook = '';

    #[ORM\Column(name: 'socialnetwork_twitter', type: Types::STRING, length: 100, nullable: false)]
    protected string $socialnetwork_twitter = '';

    #[ORM\Column(name: 'socialnetwork_instagram', type: Types::STRING, length: 100, nullable: false)]
    protected string $socialnetwork_instagram = '';

    #[ORM\Column(name: 'socialnetwork_youtube', type: Types::STRING, length: 100, nullable: false)]
    protected string $socialnetwork_youtube = '';

    #[ORM\Column(name: 'render_time', type: Types::INTEGER, nullable: false)]
    protected int $render_time = 0;

    #[ORM\Column(name: 'rendered_frames', type: Types::INTEGER, nullable: false)]
    protected int $rendered_frames = 0;

    #[ORM\Column(name: 'ordered_frames', type: Types::INTEGER, nullable: false)]
    protected int $ordered_frames = 0;

    #[ORM\Column(name: 'rendered_frames_public', type: Types::INTEGER, nullable: false)]
    protected int $rendered_frames_public = 0;

    #[ORM\Column(name: 'ordered_frames_public', type: Types::INTEGER, nullable: false)]
    protected int $ordered_frames_public = 0;

    #[ORM\Column(name: 'ordered_frames_time', type: Types::INTEGER, nullable: false)]
    protected int $ordered_frames_time = 0;

    #[ORM\Column(name: 'nb_projects', type: Types::INTEGER, nullable: false)]
    protected int $nb_projects = 0;

    #[ORM\Column(name: 'level', type: Types::INTEGER, nullable: false)]
    protected int $level = 0;

    #[ORM\Column(name: 'notification', type: Types::INTEGER, nullable: false)]
    protected int $notification = 0;

    #[ORM\Column(name: 'notification_token', type: Types::STRING, length: 32, nullable: false)]
    protected string $notification_token = '';

    #[ORM\Column(name: 'scheduler', type: Types::INTEGER, nullable: false, options: ['default' => 1])]
    protected int $scheduler = 1;

    #[ORM\Column(name: 'points', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points = 0;

    #[ORM\Column(name: 'points_earn', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points_earn = 0;

    #[ORM\JoinColumn(name: 'team', referencedColumnName: 'id', onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'members', cascade: ['persist'])]
    protected ?Team $team = null;

    #[ORM\Column(name: 'team_join_time', type: Types::INTEGER, nullable: false)]
    protected int $team_join_time = 0;

    #[ORM\Column(name: 'team_contribution', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $team_contribution = 0;

    #[ORM\Column(name: 'consecutive_render_days_max', type: Types::INTEGER, nullable: false)]
    protected int $consecutive_render_days_max = 0;

    /**
     * Date on when last PayPal donation
     */
    #[ORM\Column(name: 'paypal_donation', type: Types::DATE_MUTABLE, nullable: true)]
    protected ?DateTimeInterface $paypal_donation = null;

    #[ORM\Column(name: 'ip_session', type: Types::STRING, length: 39, nullable: false, options: ['default' => ''])]
    protected string $ip_session = '';

    #[ORM\Column(name: 'ip_login', type: Types::STRING, length: 39, nullable: false, options: ['default' => ''])]
    protected string $ip_login = '';

    /**
     * @var Collection<Project>
     */
    #[ORM\OneToMany(targetEntity: Project::class, mappedBy: 'owner', cascade: ['persist', 'remove'])]
    protected Collection $projects;

    /**
     * @var Collection<PastProject>
     */
    #[ORM\OneToMany(targetEntity: PastProject::class, mappedBy: 'owner', cascade: ['persist', 'remove'])]
    protected Collection $pastProjects;

    /**
     * @var Collection<RenderDay>
     */
    #[ORM\OneToMany(targetEntity: RenderDay::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $renderdays;

    /**
     * @var Collection<AdventCalendarClaimedDay>
     */
    #[ORM\OneToMany(targetEntity: AdventCalendarClaimedDay::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $adventCalendarClaimedDays;

    /**
     * @var Collection<Session>
     */
    #[ORM\OneToMany(targetEntity: Session::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $sessions;

    /**
     * @var Collection<Donor>
     */
    #[ORM\OneToMany(targetEntity: Donor::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $donors;

    /**
     * @var Collection<SessionPast>
     */
    #[ORM\OneToMany(targetEntity: SessionPast::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $sessionPasts;

    /**
     * @var Collection<Gift>
     */
    #[ORM\OneToMany(targetEntity: Gift::class, mappedBy: 'owner', cascade: ['persist'])]
    protected Collection $gifts;

    /**
     * @var Collection<Award>
     */
    #[ORM\OneToMany(targetEntity: Award::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    protected Collection $awards;

    /**
     * @var Collection<UserPublicKey>
     */
    #[ORM\OneToMany(targetEntity: UserPublicKey::class, mappedBy: 'user', cascade: ['persist'])]
    protected Collection $publicKeys;

    #[ORM\Column(name: 'sponsor_receive', type: Types::BOOLEAN, nullable: false)]
    protected bool $sponsor_receive = true;

    #[ORM\Column(name: 'sponsor_give', type: Types::BOOLEAN, nullable: false)]
    protected bool $sponsor_give = false;

    /**
     * @var Collection<StatsUser>
     */
    #[ORM\OneToMany(targetEntity: StatsUser::class, mappedBy: 'user', cascade: ['persist'])]
    protected Collection $stats;

    /**
     * @return Collection<RenderDay>
     */
    public function renderDays(): Collection {
        return $this->renderdays;
    }

    /**
     * @return Collection<AdventCalendarClaimedDay>
     */
    public function adventCalendarClaimedDays(): Collection {
        return $this->adventCalendarClaimedDays;
    }

    /**
     * @return Collection<Project>
     */
    public function projects(): Collection {
        return $this->projects;
    }

    /**
     * @return Collection<PastProject>
     */
    public function pastProjects(): Collection {
        return $this->pastProjects;
    }


    public function __addRenderDay(RenderDay $renderDay): self {
        if (!$this->renderdays->contains($renderDay)) {
            $this->renderdays->add($renderDay);
        }

        return $this;
    }

    public function __removeRenderDay(RenderDay $renderDay): self {
        if ($this->renderdays->contains($renderDay)) {
            $this->renderdays->removeElement($renderDay);
        }

        return $this;
    }

    public function __addAdventCalendarClaimedDay(AdventCalendarClaimedDay $claimedDay): self {
        if (!$this->adventCalendarClaimedDays->contains($claimedDay)) {
            $this->adventCalendarClaimedDays->add($claimedDay);
        }

        return $this;
    }

    public function __removeAdventCalendarClaimedDay(AdventCalendarClaimedDay $claimedDay): self {
        if ($this->adventCalendarClaimedDays->contains($claimedDay)) {
            $this->adventCalendarClaimedDays->removeElement($claimedDay);
        }

        return $this;
    }

    public function addAward(Award $award): void {
        $award->setUser($this);
        if (!$this->awards->contains($award)) {
            $this->awards->add($award);
        }
    }

    public function removeAward(Award $award): void {
        $this->awards->removeElement($award);
    }

    public function getLastWebSession(): ?int {
        return $this->last_web_session;
    }

    public function setLastWebSession(int $lastWebSession): self {
        $this->last_web_session = $lastWebSession;

        return $this;
    }

    public function getLastSession(): ?int {
        return $this->last_session;
    }

    public function setLastSession(int $lastSession): self {
        $this->last_session = $lastSession;

        return $this;
    }

    public function getLastRenderedFrame(): ?DateTimeInterface {
        return $this->last_rendered_frame;
    }

    public function setLastRenderedFrame(?DateTimeInterface $last_rendered_frame): self {
        $this->last_rendered_frame = $last_rendered_frame;

        return $this;
    }

    public function getWebsite(): ?string {
        return $this->website;
    }

    public function setWebsite(string $website): self {
        $this->website = $website;

        return $this;
    }

    public function getSocialnetworkFacebook(): ?string {
        return $this->socialnetwork_facebook;
    }

    public function setSocialnetworkFacebook(string $socialnetworkFacebook): self {
        $this->socialnetwork_facebook = $socialnetworkFacebook;

        return $this;
    }

    public function getSocialnetworkTwitter(): ?string {
        return $this->socialnetwork_twitter;
    }

    public function setSocialnetworkTwitter(string $socialnetworkTwitter): self {
        $this->socialnetwork_twitter = $socialnetworkTwitter;

        return $this;
    }

    public function getSocialnetworkInstagram(): ?string {
        return $this->socialnetwork_instagram;
    }

    public function setSocialnetworkInstagram(string $socialnetworkInstagram): self {
        $this->socialnetwork_instagram = $socialnetworkInstagram;

        return $this;
    }

    public function getSocialnetworkYoutube(): ?string {
        return $this->socialnetwork_youtube;
    }

    public function setSocialnetworkYoutube(string $socialnetworkYoutube): self {
        $this->socialnetwork_youtube = $socialnetworkYoutube;

        return $this;
    }

    public function getRenderTime(): int {
        return $this->render_time;
    }

    public function setRenderTime(int $renderTime): self {
        $this->render_time = $renderTime;

        return $this;
    }

    public function getRenderedFrames(): int {
        return $this->rendered_frames;
    }

    public function setRenderedFrames(int $renderedFrames): self {
        $this->rendered_frames = $renderedFrames;

        return $this;
    }

    public function getOrderedFrames(): int {
        return $this->ordered_frames;
    }

    public function setOrderedFrames(int $orderedFrames): self {
        $this->ordered_frames = $orderedFrames;

        return $this;
    }

    public function getRenderedFramesPublic(): int {
        return $this->rendered_frames_public;
    }

    public function setRenderedFramesPublic(int $renderedFramesPublic): self {
        $this->rendered_frames_public = $renderedFramesPublic;

        return $this;
    }

    public function getOrderedFramesPublic(): int {
        return $this->ordered_frames_public;
    }

    public function setOrderedFramesPublic(int $orderedFramesPublic): self {
        $this->ordered_frames_public = $orderedFramesPublic;

        return $this;
    }

    public function getOrderedFramesTime(): int {
        return $this->ordered_frames_time;
    }

    public function setOrderedFramesTime(int $orderedFramesTime): self {
        $this->ordered_frames_time = $orderedFramesTime;

        return $this;
    }

    public function getNbProjects(): int {
        return $this->nb_projects;
    }

    public function setNbProjects(int $nbProjects): self {
        $this->nb_projects = $nbProjects;

        return $this;
    }

    public function getLevel(): int {
        return $this->level;
    }

    public function setLevel(int $level): self {
        $this->level = $level;

        return $this;
    }

    public function getNotification(): int {
        return $this->notification;
    }

    public function setNotification(int $notification): self {
        $this->notification = $notification;

        return $this;
    }

    public function getNotificationToken(): string {
        return $this->notification_token;
    }

    public function setNotificationToken(string $notification_token): self {
        $this->notification_token = $notification_token;

        return $this;
    }

    public function getScheduler(): int {
        return $this->scheduler;
    }

    public function setScheduler(int $scheduler): self {
        $this->scheduler = $scheduler;

        return $this;
    }

    public function getPoints(): ?float {
        return $this->points;
    }

    public function setPoints(float $points): self {
        $this->points = $points;

        return $this;
    }

    public function getPointsEarn(): ?float {
        return $this->points_earn;
    }

    public function setPointsEarn(float $pointsEarn): self {
        $this->points_earn = $pointsEarn;

        return $this;
    }

    public function getTeam(): ?Team {
        return $this->team;
    }

    public function setTeam(?Team $team): self {
        $this->team = $team;

        return $this;
    }

    public function getTeamJoinTime(): int {
        return $this->team_join_time;
    }

    public function setTeamJoinTime(int $teamJoinTime): self {
        $this->team_join_time = $teamJoinTime;

        return $this;
    }

    public function getTeamContribution(): ?float {
        return $this->team_contribution;
    }

    public function setTeamContribution(float $teamContribution): self {
        $this->team_contribution = $teamContribution;

        return $this;
    }

    public function getConsecutiveRenderDaysMax(): ?int {
        return $this->consecutive_render_days_max;
    }

    public function setConsecutiveRenderDaysMax(int $consecutiveRenderDaysMax): self {
        $this->consecutive_render_days_max = $consecutiveRenderDaysMax;

        return $this;
    }

    public function getSponsorReceive(): bool {
        return $this->sponsor_receive;
    }

    public function setSponsorReceive(bool $sponsor_receive): self {
        $this->sponsor_receive = $sponsor_receive;

        return $this;
    }

    public function getSponsorGive(): bool {
        return $this->sponsor_give;
    }

    public function setSponsorGive(bool $sponsor_give): self {
        $this->sponsor_give = $sponsor_give;

        return $this;
    }

    public function getPayPalDonation(): ?DateTimeInterface {
        return $this->paypal_donation;
    }

    public function setPayPalDonation(?DateTimeInterface $paypal_donation): self {
        $this->paypal_donation = $paypal_donation;

        return $this;
    }

    public function getIPSession(): string {
        return $this->ip_session;
    }

    public function setIPSession(string $ip_session): self {
        $this->ip_session = $ip_session;

        return $this;
    }

    public function getIPLogIn(): string {
        return $this->ip_login;
    }

    public function setIPLogIn(string $ip_login): self {
        $this->ip_login = $ip_login;

        return $this;
    }

    /**
     * @return Collection<Award>
     */
    public function awards(): Collection {
        return $this->awards;
    }

    /**
     * @return Collection<StatsUser>
     */
    public function stats(): Collection {
        return $this->stats;
    }

    public function __construct() {
        $this->sessions = new ArrayCollection();
        $this->awards = new ArrayCollection();
        $this->renderdays = new ArrayCollection();
        $this->adventCalendarClaimedDays = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->pastProjects = new ArrayCollection();
        $this->gifts = new ArrayCollection();
        $this->publicKeys = new ArrayCollection();
        $this->stats = new ArrayCollection();

        $this->init();
    }

    #[ORM\PostLoad]
    public function init(): void {
        $this->cache_acl_renderer = new Cache();
        $this->cache_acl_manager = new Cache();
        $this->cache_blocked_renderer = new Cache();
        $this->cache_blocked_owner = new Cache();
        $this->cache_users_who_blocked_me = new Cache();
        $this->cache_high_priorty_users = new Cache();
    }

    public function __addProject(Project $project): self {
        if (!$this->projects->contains($project)) {
            $this->projects->add($project);
            $project->setOwner($this);
        }

        return $this;
    }

    public function __removeProject(Project $project): self {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    public function __addPastProject(PastProject $pastProject): self {
        if (!$this->pastProjects->contains($pastProject)) {
            $this->pastProjects->add($pastProject);
            $pastProject->setOwner($this);
        }

        return $this;
    }

    public function __removePastProjec(PastProject $pastProject): self {
        if ($this->pastProjects->contains($pastProject)) {
            $this->pastProjects->removeElement($pastProject);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    public function __addSession(Session $session): self {
        if (!$this->sessions->contains($session)) {
            $this->sessions->add($session);
        }

        return $this;
    }

    public function __removeSession(Session $session): self {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
        }

        return $this;
    }

    public function __addGift(Gift $gift): self {
        if (!$this->gifts->contains($gift)) {
            $this->gifts->add($gift);
        }

        return $this;
    }

    public function __removeGift(Gift $gift): self {
        if ($this->gifts->contains($gift)) {
            $this->gifts->removeElement($gift);
        }

        return $this;
    }

    public function authenticate(string $password_): bool {
        return password_verify($password_, $this->getPassword());
    }

    public static function generateHashedPassword(string $login_, string $password_): string {
        return password_hash($password_, PASSWORD_DEFAULT);
    }

    /**
     * @return Tile[]
     */
    public function frames(?string $status_ = null): array {
        if (is_null($status_)) {
            return GlobalInject::getEntityManager()->getRepository(Tile::class)->findBy(['user' => $this->getId()]);
        }
        else {
            return GlobalInject::getEntityManager()->getRepository(Tile::class)->findBy(['user' => $this->getId(), 'status' => $status_]);
        }
    }

    /**
     * @return Collection<Session>
     */
    public function sessions(): Collection {
        return $this->sessions;
    }


    /**
     * @return Tile[]
     */
    public function lastFinishedFrames(int $limit): array {
        return GlobalInject::getEntityManager()->getRepository(Tile::class)->findBy(['user' => $this->getId(), 'status' => Constant::FINISHED], ['validation_time' => 'DESC'], $limit);
    }

    /**
     * @return Collection<Gift>
     */
    public function gifts(): Collection {
        return $this->gifts;
    }

    /**
     * @return Team[]
     */
    public function getWantedTeams(): array {
        $ret = array();

        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::TEAM_ASK_JOIN, null, $this->getId());
        foreach ($liaisons as $l) {
            $team = GlobalInject::getEntityManager()->getRepository(Team::class)->find($l->getPrimary());
            if (is_object($team)) {
                $ret [] = $team;
            }
        }

        return $ret;
    }

    /**
     * @return Collection<UserPublicKey>
     */
    public function getPublicKeys(): Collection {
        return $this->publicKeys;
    }

    public function getPublicKeyFromComment(string $comment): ?UserPublicKey {
        foreach ($this->publicKeys as $publicKey) {
            if ($publicKey->getComment() == $comment) {
                return $publicKey;
            }
        }
        return null;
    }

    /**
     * @return Project[]
     */
    public function getAvailableProjects(int $compute_method, int $max_ram): array {
        if ($this->canDoRendering() == false) {
            Logger::error(__METHOD__.' \''.$this->getId().'\' have no right to ask for rendering');
            return array();
        }

        $available = array();
        $blocked_owners = $this->getBlockedOwners();
        $rows = GlobalInject::getEntityManager()->getRepository(Project::class)->getAvailableProjects($this, $compute_method, $max_ram);

        foreach ($rows as $project) {
            if ($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID || $project->getId() == Project::POWER_DETECTION_PROJECT_ID) {
//                Logger::debug(__method__.' no compute method or power detection project (name: '.$project->getName().')');
                continue;
            }

            if ($this->canRenderProject($project) == false) {
                Logger::debug(__METHOD__.' can not render project');
                continue;
            }

            // light or heavy block ?
            if ($project->getBlocked() != 0 && array_key_exists($project->getBlocked(), Project::BLOCK_MESSAGES)) {
                if (Project::BLOCK_MESSAGES[$project->getBlocked()]['ownercanrender'] == false) {
                    Logger::debug(__METHOD__.' blocked project');
                    continue;
                }
            }

            if (in_array($project->getOwner()->getId(), $this->getUsersWhoBlockedMe())) {
                Logger::debug(__METHOD__.' blocked user (1)');
                continue;
            }

            if (in_array($project->getOwner()->getId(), $blocked_owners)) {
                Logger::debug(__METHOD__.' blocked user (2)');
                continue;
            }

            $stats = $project->getCachedStatistics();
            if (($stats[Constant::STATS_TOTAL] - $stats[Constant::PROCESSING] - $stats[Constant::FINISHED]) == 0) {
                if ($stats[Constant::STATS_TOTAL] > 0) {
                    Logger::debug(__METHOD__.' stats');
                    continue;
                }
            }

            $available [] = $project;
        }
        return $available;
    }

    public function getWorkerStatus(): string {
        $status = Constant::DOWN;

        $sessions = $this->sessions();
        foreach ($sessions as $session) {
            $tiles_session = $session->tilesWithStatus(Constant::PROCESSING);
            if (count($tiles_session) > 0) {
                return Constant::PROCESSING;
            }
            $status = Constant::WAITING;
        }

        return $status;
    }

    public function getUsersWhoBlockedMe(): array {
        if ($this->cache_users_who_blocked_me->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, null, $this->getId());
            foreach ($liaisons as $l) {
                $this->cache_users_who_blocked_me->add($l->getPrimary(), $l->getPrimary());
            }
            $this->cache_users_who_blocked_me->setFill(true);
        }
        return $this->cache_users_who_blocked_me->getAll();
    }

    public function getBlockedRenderers(): array {
        if ($this->cache_blocked_renderer->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, $this->getId());
            foreach ($liaisons as $l) {
                $this->cache_blocked_renderer->add($l->getSecondary(), $l->getSecondary());
            }
            $this->cache_blocked_renderer->setFill(true);
        }
        return $this->cache_blocked_renderer->getAll();
    }

    public function addBlockedRenderer(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, $this->getId(), $login_);
        if (count($liaisons) == 0) {
            $l = new Liaison();
            $l->setType(Liaison::BLOCKED_RENDERER);
            $l->setPrimary($this->getId());
            $l->setSecondary($login_);
            GlobalInject::getEntityManager()->persist($l);
            GlobalInject::getEntityManager()->flush($l);
        }
        $this->cache_users_who_blocked_me = new Cache(); // reset the cache
        $this->cache_blocked_renderer = new Cache(); // reset the cache
        $this->cache_blocked_owner = new Cache(); // reset the cache
    }

    public function removeBlockedRenderer(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, $this->getId(), $login_);
        foreach ($liaisons as $l) {
            GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        }
    }

    public function getBlockedOwners(): array {
        if ($this->cache_blocked_owner->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_OWNER, $this->getId());
            foreach ($liaisons as $l) {
                $this->cache_blocked_owner->add($l->getSecondary(), $l->getSecondary());
            }
            $this->cache_blocked_owner->setFill(true);
        }
        return $this->cache_blocked_owner->getAll();
    }

    public function addBlockedOwner(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_OWNER, $this->getId(), $login_);
        if (count($liaisons) == 0) {
            $l = new Liaison();
            $l->setType(Liaison::BLOCKED_OWNER);
            $l->setPrimary($this->getId());
            $l->setSecondary($login_);
            GlobalInject::getEntityManager()->persist($l);
            GlobalInject::getEntityManager()->flush($l);
        }
        $this->cache_users_who_blocked_me = new Cache(); // reset the cache
        $this->cache_blocked_renderer = new Cache(); // reset the cache
        $this->cache_blocked_owner = new Cache(); // reset the cache
    }

    public function removeBlockedOwner(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_OWNER, $this->getId(), $login_);
        foreach ($liaisons as $l) {
            GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        }
    }

    public function getHighPriorityUsers(): array {
        if ($this->cache_high_priorty_users->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::HIGH_PRIORITY_USER, $this->getId());
            foreach ($liaisons as $l) {
                $this->cache_high_priorty_users->add($l->getSecondary(), $l->getSecondary());
            }
            $this->cache_high_priorty_users->setFill(true);
        }
        return $this->cache_high_priorty_users->getAll();
    }

    public function addHighPriorityUser(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::HIGH_PRIORITY_USER, $this->getId(), $login_);
        if (count($liaisons) == 0) {
            $l = new Liaison();
            $l->setType(Liaison::HIGH_PRIORITY_USER);
            $l->setPrimary($this->getId());
            $l->setSecondary($login_);
            GlobalInject::getEntityManager()->persist($l);
            GlobalInject::getEntityManager()->flush($l);
            $this->cache_high_priorty_users = new Cache(); // reset the cache
        }
    }

    public function removeHighPriorityUser(string $login_): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::HIGH_PRIORITY_USER, $this->getId(), $login_);
        foreach ($liaisons as $l) {
            GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        }
    }

    /**
     * @return User[]
     */
    public function getSponsorUsers(bool $onlyEnabledReceived = false): array {
        $repo = GlobalInject::getEntityManager()->getRepository(User::class);
        $ret = [];
        foreach (GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::SPONSOR_USER, $this->getId()) as $l) {
            $user = $repo->find($l->getSecondary());
            if (is_object($user)) {
                if ($onlyEnabledReceived) {
                    if ($user->getSponsorReceive()) {
                        $ret [] = $user;
                    }
                }
                else {
                    $ret [] = $user;
                }
            }
        }
        return $ret;
    }

    public function addSponsorUser(User $other): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::SPONSOR_USER, $this->getId(), $other->getId());
        if (count($liaisons) == 0) {
            $l = new Liaison();
            $l->setType(Liaison::SPONSOR_USER);
            $l->setPrimary($this->getId());
            $l->setSecondary($other->getId());
            GlobalInject::getEntityManager()->persist($l);
            GlobalInject::getEntityManager()->flush($l);
        }
    }

    public function removeSponsorUser(User $other): void {
        $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::SPONSOR_USER, $this->getId(), $other->getId());
        foreach ($liaisons as $l) {
            GlobalInject::getEntityManager()->getRepository(Liaison::class)->remove($l);
        }
    }

    // ACL

    public function canDoRendering(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING);
    }

    public function canRenderProject(Project $project): bool {
        if ($this->canDoRendering() == false) {
            return false;
        }

        return $project->canRenderProject($this);
    }

    public function isModerator(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_MODERATOR) || $this->levelIsEnabled(User::ACL_MASK_ADMIN) || $this->levelIsEnabled(User::ACL_MASK_SUPER_ADMIN);
    }

    public function isAdmin(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_ADMIN) || $this->levelIsEnabled(User::ACL_MASK_SUPER_ADMIN);
    }

    public function isSuperAdmin(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_SUPER_ADMIN);
    }

    public function canSeeAllProjects(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_CAN_SEE_ALL_PROJECT);
    }

    public function canModifyAllProjects(): bool {
        return $this->levelIsEnabled(User::ACL_MASK_CAN_MODIFY_ALL_PROJECT);
    }

    public function canDisableFrameThumbnail(): bool {
        return $this->isAdmin() || $this->levelIsEnabled(User::ACL_MASK_CAN_DISABLE_THUMBNAIL);
    }

    public function isPatreon(): bool {
        $one_month_before = date_format(DateTime::createFromFormat('Y-m-d', date('Y-m-01'))->sub(\DateInterval::createFromDateString('1 month')), 'Y-m'); // use 01 due to bug on month duration in datatime
        return is_object(GlobalInject::getEntityManager()->getRepository(Patreon::class)->findOneBy(['user' => $this->getId(), 'lastCharge' => date_format(new DateTime(), 'Y-m')])) || is_object(GlobalInject::getEntityManager()->getRepository(Patreon::class)->findOneBy(['user' => $this->getId(), 'lastCharge' => $one_month_before]));
    }

    public function isPayPalDonator(): bool {
        if ($this->paypal_donation != null) {
            $twoMonthsAgo = (new DateTime())->sub(\DateInterval::createFromDateString('2 month'));
            return $twoMonthsAgo < $this->paypal_donation;
        }
        return false;
    }

    public function isDonator(): bool {
        return $this->isPayPalDonator() || $this->isPatreon();
    }

    public function getNumberOfConcurrentProject(): int {
        $current = 0;

        $projects = $this->projects();
        foreach ($projects as $s) {
            $status = $s->getStatus();
            if ($status != Constant::FINISHED && $status != Constant::PAUSED) {
                $current++;
            }
        }

        return $current;
    }

    /**
     * Return null if no ranking
     **/
    public function getRanking(): ?array {
        $users = GlobalInject::getMain()->getBestRenderersFromCache();

        $i = 1;
        foreach ($users as $user) {
            if ($user->getLogin() == $this->getId()) {
                return array($i, count($users));
            }
            $i++;
        }
        return null;
    }

    /**
     * Return a map of awards
     * key: Award class name
     * value: boolean for earning
     **/
    public function awardsWithStatus(): array {
        $ret = array();

        $all_awards = GlobalInject::getEntityManager()->getRepository(Award::class)->awardsAvailable();
        foreach ($all_awards as $award_name) {
            $ret[$award_name] = false;
        }
        foreach ($this->awards() as $row) { // awards i have earned
            $ret[$row->getShortName()] = true;
        }
        return $ret;
    }

    public function awardsWithDate(): array {
        $ret = array();

        $all_awards = GlobalInject::getEntityManager()->getRepository(Award::class)->awardsAvailable();
        foreach ($all_awards as $award_name) {
            $class_name = "\\App\\Entity\\".$award_name;
            $a = new $class_name();
            $a->isEarned = false;
            $a->earnTime = 0;
            $ret[$award_name] = $a;
        }
        foreach (GlobalInject::getEntityManager()->getRepository(Award::class)->findBy(array('user' => $this->getId())) as $award1) {
            if (array_key_exists($award1->getShortName(), $ret)) {
                $ret[$award1->getShortName()]->isEarned = true;
                $ret[$award1->getShortName()]->earnTime = $award1->getTime();
            }
        }
        return $ret;
    }

    public function updateAwards(?Tile $frame, ?Session $session): void {
        $all_awards = $this->awardsWithStatus();
        foreach ($all_awards as $award_name => $earned) {
            if ($earned == false) {
                $class_name = "\\App\\Entity\\".$award_name;
                /** @var Award $award */
                $award = new $class_name();
                if ($award->earn($this, $frame, $session)) {
//                    $award->setType($award_name);
                    $award->setUser($this);
                    $award->setTime(time());
                    GlobalInject::getEntityManager()->persist($award);
                    GlobalInject::getEntityManager()->flush($award);
                    $award->actionOnEarn($this, $frame, $session);
                    Logger::debug(__METHOD__.' User '.$this->getId().' earned the award '.$award_name);
                }
            }
        }
    }

    public function giveAward(string $type): void {
        $all_awards = $this->awardsWithStatus();
        if (array_key_exists($type, $all_awards) && $all_awards[$type] == false) {
            $class_name = "\\App\Entity\\".$type;
            /** @var Award $award */
            $award = new $class_name();
            //$award->setType($type);
            // $award->setUser($this);
            $award->setTime(time());
            $this->addAward($award);
            GlobalInject::getEntityManager()->persist($award);
            GlobalInject::getEntityManager()->flush($award);

            $award->actionOnEarn($this, null, null);
            Logger::debug(__METHOD__.' User '.$this->getId().' earned the award '.$type);
        }
        elseif (array_key_exists($type, $all_awards) && $all_awards[$type] == true) {
            // do nothing award already earn
        }
        else {
            Logger::debug(__METHOD__." '$type' not in awards available");
        }
    }

    public function affiliate(): void {
        $config = GlobalInject::getConfigService()->getData();
        $affiliate = GlobalInject::getEntityManager()->getRepository(User::class)->find($this->getAffiliate());
        if (is_object($affiliate)) {
            $affiliate->setPoints($affiliate->getPoints() + $config['affiliate_reward']);

            $this->setPoints($this->getPoints() + $config['affiliate_reward']);
            GlobalInject::getEntityManager()->flush($this);

            if ($affiliate->noficationIsEnabled(User::NOTIFICATION_MASK_AFFILIATE_REFERER)) {
                $content_user = sprintf('Hello,<br>Your referral has rendered %s public frames, this gives both of you %s points.<br><br><i>You can unsubscribe to the notification by changing your notification level on your account page.</i>', number_format($config['affiliate_minimum_frame']), number_format($config['affiliate_reward']));
                Mail::sendamail($affiliate->getEmail(), sprintf('Your referral has earned you %s points', number_format($config['affiliate_reward'])), $content_user, 'affiliate_master');
            }

            if ($this->noficationIsEnabled(User::NOTIFICATION_MASK_AFFILIATE_REFERRED)) {
                $content_affiliate = sprintf('Hello,<br>Because you have been referred by a user and you have renderer %s public frames, you are both getting %s points.<br><br><i>You can unsubscribe to the notification by changing your notification level on your account page.</i>', number_format($config['affiliate_minimum_frame']), number_format($config['affiliate_reward']));
                Mail::sendamail($this->getEmail(), sprintf('Because you have been referred, you have earn %s points', number_format($config['affiliate_reward'])), $content_affiliate, 'affiliate_slave');
            }
        }
    }

    public function getACLManageProject(int $project_id): string|bool|null {
        if ($this->cache_acl_manager->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_MANAGE_PROJECT, $this->getId(), null);
            foreach ($liaisons as $liaison) {
                $this->cache_acl_manager->add($liaison->getValue(), $liaison->getSecondary());
            }
            $this->cache_acl_manager->setFill(true);
        }
        if ($this->cache_acl_manager->exists($project_id) == false) {
            // since it's not on cache, it does not exist on the database => return false
            return false;
        }
        else {
            return $this->cache_acl_manager->get($project_id);
        }
    }

    public function getACLRendererProject(int $project_id): string|bool|null {
        if ($this->cache_acl_renderer->isFilled() == false) {
            $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::USER_RENDER_PROJECT, $this->getId(), null);
            foreach ($liaisons as $liaison) {
                $this->cache_acl_renderer->add($liaison->getValue(), $liaison->getSecondary());
            }
            $this->cache_acl_renderer->setFill(true);

            if (is_object($this->getTeam())) {
                $liaisons = GlobalInject::getEntityManager()->getRepository(Liaison::class)->getLiaisons(Liaison::TEAM_RENDER_PROJECT, $this->getTeam()->getId(), null);
                foreach ($liaisons as $liaison) {
                    $this->cache_acl_renderer->add($this->getId(), $liaison->getSecondary());
                }
            }
        }
        if ($this->cache_acl_renderer->exists($project_id) == false) {
            // since it's not on cache, it does not exist on the database => return false
            return false;
        }
        else {
            return $this->cache_acl_renderer->get($project_id);
        }
    }

    public function setLevelFromMask(int $mask, int $value_): void {
        if ($value_ == 1) {
            if ((($this->getLevel() / $mask) % 2 === 1) == false) {
                $this->setLevel($this->getLevel() + $mask);
            }
        }
        else {
            if ((($this->getLevel() / $mask) % 2 === 1) == true) {
                $this->setLevel($this->getLevel() - $mask);
            }
        }
    }

    public function setNotificationFromMask(int $mask, int $value_): void {
        if ($value_ == 1) {
            if ((($this->getNotification() / $mask) % 2 === 1) == false) {
                $this->setNotification($this->getNotification() + $mask);
            }
        }
        else {
            if ((($this->getNotification() / $mask) % 2 === 1) == true) {
                $this->setNotification($this->getNotification() - $mask);
            }
        }
    }

    public function setSchedulerFromMask(int $mask, int $value_): void {
        if ($value_ == 1) {
            if ((($this->getScheduler() / $mask) % 2 === 1) == false) {
                $this->setScheduler($this->getScheduler() + $mask);
            }
        }
        else {
            if ((($this->getScheduler() / $mask) % 2 === 1) == true) {
                $this->setScheduler($this->getScheduler() - $mask);
            }
        }
    }

    public function levelIsEnabled(int $mask): bool {
//        Logger::debug(__method__.' level: '.$this->getLevel().' mask: '.$mask);
        return (($this->getLevel() / $mask) % 2 === 1);
    }

    public function noficationIsEnabled(int $mask): bool {
        return (($this->getNotification() / $mask) % 2 === 1);
    }

    public function schedulerIsEnabled(int $mask): bool {
        return (($this->getScheduler() / $mask) % 2 === 1);
    }

    public function getExtraPointsFromTeam(): float {
        $team = $this->getTeam();

        $ret = $this->getPoints();
        if (is_object($team)) {
            $members = count($team->members());
            if ($members > 0) {
                $now = time();
                $coef = max(0.001, ($now - $this->getTeamJoinTime()) / (max(1, $now - $team->getCreationTime())));
//                 Logger::info(__method__.' bonus '.number_format($team->getPoints() / $members).' vs '.number_format($coef * $team->getPoints()));
                $ret += min($coef * $team->getPoints(), $team->getPoints() / $members);
//                 $ret += $team->getPoints() / $members;
            }
        }

        return $ret;
    }

    public function addRenderDay(RenderDay $renderday): void {
        $renderday->setUser($this);
        if (!$this->renderdays->contains($renderday)) {
            $this->renderdays->add($renderday);
        }
    }

    public function removeRenderDay(RenderDay $renderday): void {
        $this->renderdays->removeElement($renderday);
    }

    public function addAdventCalendarClaimedDay(AdventCalendarClaimedDay $claimedDay): void {
        $claimedDay->setUser($this);
        if (!$this->adventCalendarClaimedDays->contains($claimedDay)) {
            $this->adventCalendarClaimedDays->add($claimedDay);
        }
    }

    public function getConsecutiveRenderDaysCurrent(): int {
        $days = array();

        for ($i = time() - 86400; $i >= $this->getRegistrationTime() - 86400; $i -= 86400) { // start from yesterday
            $days[date('Y-m-d', $i)] = false;
        }
        foreach (GlobalInject::getEntityManager()->getRepository(RenderDay::class)->findBy(['user' => $this->getId()]) as $r) {
            $date = $r->getDate()->format('Y-m-d');
            if (array_key_exists($date, $days)) {
                $days[$date] = true;
            }
        }

        $i = 0;
        foreach ($days as $day => $value) {
            if ($value == false) {
                return $i;
            }
            $i++;
        }

        return $i;
    }

    public function getGlobalUnsubscribeUrl(): string {
        if ($this->getNotificationToken() == '') {
            $this->setNotificationToken(Misc::code(32));
            GlobalInject::getEntityManager()->flush($this);
        }

        return GlobalInject::getConfigService()->getData()['site']['url'].GlobalInject::getRouter()->generate('app_user_unsubscribe', ['token' => $this->getNotificationToken()], UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    public function getRoles(): array {
        $roles = ['ROLE_USER'];

        if ($this->isModerator()) {
            $roles[] = 'ROLE_MOD';
        }

        if ($this->isSuperAdmin()) {
            $roles[] = 'ROLE_ADMIN';
        }

        return $roles;
    }

    public function getSalt(): ?string {
        return null;
    }

    public function getUserName(): string {
        return $this->getUserIdentifier();
    }

    public function getUserIdentifier(): string {
        return $this->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string {
        return serialize([
            $this->id,
            $this->password,
            $this->email,
            $this->level,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize(string $serialized): void {
        list(
            $this->id,
            $this->password,
            $this->email,
            $this->level,
            ) = unserialize($serialized);
    }

    public function eraseCredentials(): void {
        // TODO: Implement eraseCredentials() method.
    }
}
