<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\SessionsStatusRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * SessionsStatus
 */
#[ORM\Table(name: 'session_status')]
#[ORM\Entity(repositoryClass: SessionsStatusRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class SessionsStatus extends Entity {

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $time;

    #[ORM\Column(name: 'disabled', type: Types::INTEGER, nullable: false)]
    protected int $disabled;

    #[ORM\Column(name: 'unknown', type: Types::INTEGER, nullable: false)]
    protected int $unknown = 0;

    #[ORM\Column(name: 'running', type: Types::INTEGER, nullable: false)]
    protected int $running;

    #[ORM\Column(name: 'paused', type: Types::INTEGER, nullable: false)]
    protected int $paused = 0;

    #[ORM\Column(name: 'sleeping', type: Types::INTEGER, nullable: false)]
    protected int $sleeping = 0;

    #[ORM\Column(name: 'broken_zero_validation', type: Types::INTEGER, nullable: false)]
    protected int $broken_zero_validation = 0;

    #[ORM\Column(name: 'rendering', type: Types::INTEGER, nullable: false)]
    protected int $rendering;

    #[ORM\Column(name: 'idle', type: Types::INTEGER, nullable: false)]
    protected int $idle;

    public function getTime(): ?int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getDisabled(): ?int {
        return $this->disabled;
    }

    public function setDisabled(int $disabled): self {
        $this->disabled = $disabled;

        return $this;
    }

    public function getUnknown(): ?int {
        return $this->unknown;
    }

    public function setUnknown(int $unknown): self {
        $this->unknown = $unknown;

        return $this;
    }

    public function getRunning(): ?int {
        return $this->running;
    }

    public function setRunning(int $running): self {
        $this->running = $running;

        return $this;
    }

    public function getBrokenZeroValidation(): ?int {
        return $this->broken_zero_validation;
    }

    public function setBrokenZeroValidation(int $brokenZeroValidation): self {
        $this->broken_zero_validation = $brokenZeroValidation;

        return $this;
    }

    public function getRendering(): ?int {
        return $this->rendering;
    }

    public function setRendering(int $rendering): self {
        $this->rendering = $rendering;

        return $this;
    }

    public function getPaused(): ?int {
        return $this->paused;
    }

    public function setPaused(int $paused): self {
        $this->paused = $paused;

        return $this;
    }

    public function getSleeping(): ?int {
        return $this->sleeping;
    }

    public function setSleeping(int $sleeping): self {
        $this->sleeping = $sleeping;

        return $this;
    }

    public function getIdle(): ?int {
        return $this->idle;
    }

    public function setIdle(int $idle): self {
        $this->idle = $idle;

        return $this;
    }
}
