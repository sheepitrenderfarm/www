<?php
/**
 * Copyright (C) 2009-2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Repository\CPURepository;
use App\Service\GlobalInject;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cpu
 */
#[ORM\Table(name: 'cpu')]
#[ORM\Index(name: 'model', columns: ['model'])]
#[ORM\Entity(repositoryClass: CPURepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class CPU extends BaseProcessingUnit {
    #[ORM\Column(name: 'cores', type: Types::INTEGER, nullable: false)]
    protected int $cores = 1;

    public function getCores(): int {
        return $this->cores;
    }

    public function setCores(int $cores): self {
        $this->cores = $cores;

        return $this;
    }

    public function getPowerPercent(): int {
        return (int)(100.0 * $this->getPower() / GlobalInject::getConfigService()->getData()['power']['cpu']['const']);
    }

    public function frequency(): string {
        $freq = $this->getModel();
        $strstr = strstr($freq, '@');
        if ($strstr !== '' and $strstr !== false) {
            $freq = trim(substr($strstr, 1));
        }
        else {
            $strstr = strrchr($freq, ' ');
            if (stristr($freq, 'GHZ') != '') {
                $freq = trim($strstr);
            }

        }
        return $freq.'x'.$this->getCores();
    }

    public function description(): string {
        $model = $this->getModel().' x'.$this->getCores();
        $model = str_replace(array(
            '(tm)',
            '(TM)',
            '(R)',
            'Quad Core Processor',
            'Eight-Core Processor',
            'CPU',
        ), '', $model);

        return htmlspecialchars($model);
    }

    public function getPowerConst(): int {
        return GlobalInject::getConfigService()->getData()['power']['cpu']['const'];
    }

    public function getPowerRatioOverCPU(): float {
        return 1.0;
    }
}
