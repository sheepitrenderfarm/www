<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseProcessingUnit;
use App\Constant;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\Misc;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class TilePowerDetection extends Tile {
    public const SCALE = 5128.0; // the goal is to have 100% -> 8172 like the old system

    private float $speedSamplesRendered = -1;

    public function getSpeedSamplesRendered(): float {
        return $this->speedSamplesRendered;
    }

    public function setSpeedSamplesRendered(float $speedSamplesRendered): void {
        $this->speedSamplesRendered = $speedSamplesRendered;
    }

    public const FRAME_NUMBER = 340;

    public function reset(bool $give_points_to_renderer = false): bool {
        return false;
    }

    public function resume(): bool {
        return false;
    }

    public function update(): bool {
        return false;
    }

    public function take(Session $a_session_): int {
        return -5;
    }

    public function validate(string $picture_file_, Session $session): int {
        Logger::debug(__METHOD__);
        $config = GlobalInject::getConfigService()->getData();

        @unlink($picture_file_);

        if ($this->getSpeedSamplesRendered() <= 0) { // not initialized
            Logger::error(__METHOD__."($picture_file_) no SpeedsamplesRendered data");
            return -30;
        }

        $renderer = GlobalInject::getEntityManager()->getRepository(User::class)->find($session->getUser()->getId());
        if (is_object($renderer)) {
            // no increment of rendered frame to keep the 10 rendered frame rules for actual frames
            $renderer->setRenderTime((int)($renderer->getRenderTime() + $this->getRenderTime()));
            GlobalInject::getEntityManager()->flush($renderer);
        }
        else {
            Logger::error(__METHOD__."($picture_file_,) $this failed to import renderer ".serialize($this->getUser()->getId()));
            return -3;
        }

        $power_this_frame = $this->getSpeedsamplesRendered() * self::SCALE;

        $device = $session->getComputeDevice();

        $session->setRenderedFrames($session->getRenderedFrames() + 1);
        $session->setLastvalidatedframe(time());

        if (Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_CPU) && $session->getPowerCpu() <= 0) {
            $session->setPowerCpu($power_this_frame);
            Logger::debug(__METHOD__.' $power_this_frame '.number_format(100.0 * $power_this_frame / $config['power']['cpu']['const']).' % CPU');
        }
        elseif (Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_GPU)) {
            $session->setPowerGpu($power_this_frame);
            Logger::debug(__METHOD__.' $power_this_frame '.number_format(100.0 * $power_this_frame / $config['power']['gpu']['const']).' % GPU');
        }

        // re-import the device because the power is override to -1
        GlobalInject::getEntityManager()->refresh($device);

        // hack detection, check if the client has sent a 'fake' power value.
        if ($device->getPower() > 0 && $power_this_frame > $device->getPower() * $config['power']['max_diff_powertest']) {
            $from_user = 100.0 * $power_this_frame / $device->getPowerConst();

            Logger::info(sprintf('Hack detection! Session from %s has a suspicious value on power detection.<br>On database: %d %%<br>from user %d %%', $session->getUser()->getId(), $device->getPowerPercent(), $from_user));
            GlobalInject::getMain()->getDiscordWebHook()->sessionPowerDetectionHack($session->getUser()->getId(), $config['site']['url'].GlobalInject::getRouter()->generate('app_session_single', ['sessionid' => $session->getId()]), $from_user, $device);
        }
        else {
            // only update device on new user or new device
            if ((is_object($device) && ($device->getRenderedFrames() == 0 || $device->getPower() <= 0)) || ($session->getUser() != null && $session->getUser()->getRenderedFrames() == 0)) {
                $power = $device->getPower() == -1 ? 0 : $device->getPower();
                if ($device->getPower() == -1) { // old bug
                    $device->setRenderedFrames(0);
                }
                $requested_power_new = ($power * (float)($device->getRenderedFrames()) + $power_this_frame) / ((float)($device->getRenderedFrames()) + 1.0);
                $device->setRenderedFrames((int)($device->getRenderedFrames() + 1));
                $device->setPower($requested_power_new);
            }
        }

        $points = $config['rank']['points']['rendered']['power_detection_frame'];
        $session->setPoints($session->getPoints() + $points);

        $renderer = $session->getUser();
        $renderer->setPoints($renderer->getPoints() + $points);
        $renderer->setPointsEarn($renderer->getPointsEarn() + $points);

        GlobalInject::getEntityManager()->flush();

        return 0;
    }

    public function getComputeDevice(): ?BaseProcessingUnit {
        return null;
    }

    public function validateFromShepherd(int $rendertime, int $preptime, int $memoryused): int {
        return -99;
    }

    public function getRenderTimeOnRefMachine(): float {
        return 0.0;
    }

    public function getRenderTimeOnRefMachineWithCoefAligned(): float {
        return 0.0;
    }

    public function getUrlThumbnailOnShepherd(bool $ondemand = false): string {
        return "";
    }

    public function getUrlFullOnShepherd(): string {
        return "";
    }

    public function getPointsForOwner(): float {
        return 0.0;
    }

    public function getPointsForRenderer(): float {
        return 0.0;
    }

    public function getEstimatedRenderTime(): int {
        return 0;
    }

    public function getBlenderCommandline(): string {
        return GlobalInject::getConfigService()->getData()['renderer']['command']['power_detection'];
    }
}
