<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * PastProject
 */
#[ORM\Table(name: 'scene_past')]
#[ORM\Index(name: 'owner', columns: ['owner'])]
#[ORM\Entity]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class PastProject extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $id;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'pastProjects')]
    protected User $owner;

    #[ORM\Column(name: 'name', type: Types::STRING, length: 32, nullable: false)]
    protected string $name;

    #[ORM\Column(name: 'creation', type: Types::INTEGER, length: 32, nullable: false)]
    protected int $creation = 0;

    #[ORM\Column(name: 'deletion', type: Types::INTEGER, length: 32, nullable: false)]
    protected int $deletion = 0;

    #[ORM\Column(name: 'finish', type: Types::INTEGER, length: 32, nullable: false)]
    protected int $finish = 0;

    #[ORM\Column(name: 'deletion_reason', type: Types::STRING, length: 32, nullable: false)]
    protected string $deletion_reason = '';

    #[ORM\Column(name: 'rendertime', type: Types::INTEGER, length: 32, nullable: false)]
    protected int $rendertime = 0;

    #[ORM\Column(name: 'blocked', type: Types::INTEGER, length: 32, nullable: false)]
    protected int $blocked = 0;

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getOwner(): ?User {
        return $this->owner;
    }

    public function setOwner(User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getCreation(): int {
        return $this->creation;
    }

    public function setCreation(int $creation): self {
        $this->creation = $creation;

        return $this;
    }

    public function getDeletion(): int {
        return $this->deletion;
    }

    public function setDeletion(int $deletion): self {
        $this->deletion = $deletion;

        return $this;
    }

    public function getFinish(): int {
        return $this->finish;
    }

    public function setFinish(int $finish): self {
        $this->finish = $finish;

        return $this;
    }

    public function getDeletionReason(): ?string {
        return $this->deletion_reason;
    }

    public function setDeletionReason(string $deletionReason): self {
        $this->deletion_reason = $deletionReason;

        return $this;
    }

    public function getBlocked(): int {
        return $this->blocked;
    }

    public function setBlocked(int $blocked): self {
        $this->blocked = $blocked;

        return $this;
    }

    public function getRendertime(): int {
        return $this->rendertime;
    }

    public function setRendertime(int $rendertime): self {
        $this->rendertime = $rendertime;

        return $this;
    }

}
