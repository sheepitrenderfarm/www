<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\AdventCalendarClaimedDayRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdventCalendarClaimedDay
 */
#[ORM\Table(name: 'advent_claimeday')]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'date', columns: ['date'])]
#[ORM\Entity(repositoryClass: AdventCalendarClaimedDayRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class AdventCalendarClaimedDay extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: false)]
    protected \DateTimeInterface $date;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'adventCalendarClaimedDays')]
    protected User $user;

    public function getId(): ?int {
        return $this->id;
    }

    public function getDate(): \DateTimeInterface {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;
        // it should be automatic but not in doctrine 2 :(
        if (is_object($user)) {
            $user->__addAdventCalendarClaimedDay($this);
        }
        else {
            $this->user->__removeAdventCalendarClaimedDay($this);
        }

        $this->user = $user;

        return $this;
    }
}
