<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\Logger;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ShepherdMock extends Shepherd {
    public const ID = 'mock';

    public function addProject(Project $project, int $width_, int $height_): bool {
        Logger::debug(__METHOD__.' '.$project);

        return true;
    }

    public function delProject(Project $project): bool {
        return true;
    }

    public function tileSetRendering(Tile $tile): bool {
        return true;
    }

    public function tileReset(Tile $tile): bool {
        return true;
    }

    public function setBlendOwnerToken(Project $project): bool {
        return true;
    }

    public function setBlendThumbnailToken(Project $project): bool {
        return true;
    }

    public function setGenerateMP4(Project $project, bool $mp4): bool {

        return true;
    }

    public function taskReset(int $id): bool {
        return true;
    }

    public function removeOrphan(): bool {
        return true;
    }

    public function updatePredictedProjectsSize(): bool {
        return true;
    }

    public function getTasks(): array {
        return array();
    }

    /*
     * Get the stats from the node and save it
     */
    public function updateStats(int $now): bool {
        return true;
    }

    public function checkIfStillTimeout(): void {
    }

    public function isEnabled(): bool {
        return $this->getEnable() != 0;
    }

    public function askForPartialFrameArchive(Project $project): bool {
        return true;
    }

    public function getStorageUsed(Project $project): float {
        return 1000000000;
    }

    public function getStoragePrediction(Project $project): float {
        return 1000000000;
    }

    public function getVersion(): string {
        return '6.6.6';
    }

    public function getLastStats(): ?StatsShepherd {
        return null;
    }

    public function getRenderingFrameCount(): int {
        return 0;
    }

    public function getBlendsCount(): int {
        return 0;
    }

    public function generateValidationUrl(Tile $tile): string {
        return "";
    }

    /**
     * @inerhitDoc
     */
    public function getTaskQueuePosition(Project $project): array|bool {
        return ['total_task' => 1000, 'queue_position' => 6, 'tasks' => 20];
    }
}
