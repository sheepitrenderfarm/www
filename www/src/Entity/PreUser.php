<?php
/**
 * Copyright (C) 2009-2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Base\BaseUser;
use App\Repository\PreUserRepository;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Utils\Mail;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Preuser
 */
#[ORM\Table(name: 'preuser')]
#[ORM\UniqueConstraint(name: 'id', columns: ['id'])]
#[ORM\Entity(repositoryClass: PreUserRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class PreUser extends BaseUser {

    #[ORM\Column(name: '`key`', type: Types::STRING, length: 32, nullable: false)]
    protected string $key;

    public function getKey(): string {
        return $this->key;
    }

    public function setKey(string $key): self {
        $this->key = $key;

        return $this;
    }

    public function activateLink(): string {
        $config = GlobalInject::getConfigService()->getData();
        return $config['site']['url'].GlobalInject::getRouter()->generate('app_user_confirm_user', ['preuser' => $this->getId(), 'key' => $this->getKey()]);
    }

    public function sendRegistrationConfirmationEmail(): bool {
        $config = GlobalInject::getConfigService()->getData();

        Logger::debug("PreUser::sendRegistrationConfirmationEmail for ".$this);

        $link = $this->activateLink();
        $content = sprintf('A registration has been made on the render farm using this e-mail.<br>To confirm it, please use: <a href="%s">%s</a><br><br>If you didn\'t register, simply delete this message.', $link, $link);

        return Mail::sendamail($this->getEmail(), 'Registration confirmation', $content, 'user_registration');
    }
}
