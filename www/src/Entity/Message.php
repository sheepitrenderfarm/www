<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\MessageRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
#[ORM\Table(name: 'message')]
#[ORM\Index(name: 'sender', columns: ['sender'])]
#[ORM\Index(name: 'receiver', columns: ['receiver'])]
#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Message extends Entity {

    public const MESSAGE_PROJECT_ALLOW = 'message_project_allow';
    public const MESSAGE_PROJECT_FORBID = 'message_project_forbid';
    public const MESSAGE_RENDERING_ALLOW = 'message_rendering_allow';
    public const MESSAGE_RENDERING_FORBID = 'message_rendering_forbid';

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'sender', type: Types::STRING, length: 32, nullable: false)]
    protected string $sender;

    #[ORM\Column(name: 'receiver', type: Types::STRING, length: 32, nullable: false)]
    protected string $receiver;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    #[ORM\Column(name: 'content', type: Types::STRING, nullable: false)]
    protected string $content;

    public function getId(): ?int {
        return $this->id;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getSender(): string {
        return $this->sender;
    }

    public function setSender(string $sender): self {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): string {
        return $this->receiver;
    }

    public function setReceiver(string $receiver): self {
        $this->receiver = $receiver;

        return $this;
    }

    public function getContent(): string {
        return $this->content;
    }

    public function setContent(string $content): self {
        $this->content = $content;

        return $this;
    }
}
