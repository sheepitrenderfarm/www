<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsGlobalRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsGlobal
 */
#[ORM\Table(name: 'stats_global')]
#[ORM\Entity(repositoryClass: StatsGlobalRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsGlobal extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    protected int $id;

    #[ORM\Column(name: 'sessions', type: Types::INTEGER, nullable: false)]
    protected int $sessions;

    #[ORM\Column(name: 'power', type: Types::INTEGER, nullable: false)]
    protected int $power;

    #[ORM\Column(name: 'frames_rendered', type: Types::INTEGER, nullable: false)]
    protected int $framesRendered;

    #[ORM\Column(name: 'time_rendered', type: Types::BIGINT, nullable: false)]
    protected int $timeRendered;

    #[ORM\Column(name: 'frames_ordered', type: Types::BIGINT, nullable: false)]
    protected int $framesOrdered;

    #[ORM\Column(name: 'frames_remaining', type: Types::INTEGER, nullable: false)]
    protected int $framesRemaining;

    #[ORM\Column(name: 'frames_remaining_public', type: Types::INTEGER, nullable: false)]
    protected int $framesRemainingPublic;

    #[ORM\Column(name: 'frames_processing', type: Types::INTEGER, nullable: false)]
    protected int $framesProcessing;

    #[ORM\Column(name: 'time_ordered', type: Types::BIGINT, nullable: false)]
    protected int $timeOrdered;

    #[ORM\Column(name: 'active_projects', type: Types::INTEGER, nullable: false)]
    protected int $activeProjects;

    #[ORM\Column(name: 'projects_created', type: Types::INTEGER, nullable: false)]
    protected int $projectsCreated;

    #[ORM\Column(name: 'tasks', type: Types::INTEGER, nullable: false)]
    protected int $tasks;

    #[ORM\Column(name: 'active_renderers', type: Types::INTEGER, nullable: false)]
    protected int $active_renderers;

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getSessions(): int {
        return $this->sessions;
    }

    public function setSessions(int $sessions): self {
        $this->sessions = $sessions;

        return $this;
    }

    public function getPower(): int {
        return $this->power;
    }

    public function setPower(int $power): self {
        $this->power = $power;

        return $this;
    }

    public function getFramesRendered(): int {
        return $this->framesRendered;
    }

    public function setFramesRendered(int $framesRendered): self {
        $this->framesRendered = $framesRendered;

        return $this;
    }

    public function getTimeRendered(): int {
        return $this->timeRendered;
    }

    public function setTimeRendered(int $timeRendered): self {
        $this->timeRendered = $timeRendered;

        return $this;
    }

    public function getFramesOrdered(): int {
        return $this->framesOrdered;
    }

    public function setFramesOrdered(int $framesOrdered): self {
        $this->framesOrdered = $framesOrdered;

        return $this;
    }

    public function getFramesRemaining(): int {
        return $this->framesRemaining;
    }

    public function setFramesRemaining(int $framesRemaining): self {
        $this->framesRemaining = $framesRemaining;

        return $this;
    }

    public function getFramesRemainingPublic(): int {
        return $this->framesRemainingPublic;
    }

    public function setFramesRemainingPublic(int $framesRemainingPublic): self {
        $this->framesRemainingPublic = $framesRemainingPublic;

        return $this;
    }

    public function getFramesProcessing(): int {
        return $this->framesProcessing;
    }

    public function setFramesProcessing(int $framesProcessing): self {
        $this->framesProcessing = $framesProcessing;

        return $this;
    }

    public function getTimeOrdered(): int {
        return $this->timeOrdered;
    }

    public function setTimeOrdered(int $timeOrdered): self {
        $this->timeOrdered = $timeOrdered;

        return $this;
    }

    public function getActiveProjects(): int {
        return $this->activeProjects;
    }

    public function setActiveProjects(int $activeProjects): self {
        $this->activeProjects = $activeProjects;

        return $this;
    }

    public function getProjectsCreated(): int {
        return $this->projectsCreated;
    }

    public function setProjectsCreated(int $projectsCreated): self {
        $this->projectsCreated = $projectsCreated;

        return $this;
    }

    public function getTasks(): int {
        return $this->tasks;
    }

    public function setTasks(int $tasks): self {
        $this->tasks = $tasks;

        return $this;
    }

    public function getActiveRenderers(): int {
        return $this->active_renderers;
    }

    public function setActiveRenderers(int $active_renderers): self {
        $this->active_renderers = $active_renderers;

        return $this;
    }
}
