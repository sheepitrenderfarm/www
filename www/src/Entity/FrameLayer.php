<?php

namespace App\Entity;

use App\Scheduler\ClientProtocol;
use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Frame created all the tile on top of each other
 *
 * getSplit() will give the number of layer
 */
#[ORM\Entity]
class FrameLayer extends Frame {

    public function getPythonScript(Tile $tile, ClientProtocol $client_protocol, bool $use_gpu): string {
        $config = GlobalInject::getConfigService()->getData();

        $count_layers = $this->getSplit();

        $project = $this->getProject();

        $script = "import bpy\n";
        $script .= (new Blender())->getScript();

        if ($this->getSplit() == 1) {
            // Do not disable denoising because it's a non-split project
        }
        else {
            // disabled denoising on title project
            $script .= "try:\n";
            $script .= "\tfor layer in bpy.context.scene.render.layers:\n";
            $script .= "\t\tlayer.cycles.use_denoising = False\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }

        // disable border in tile split.
        $use_border_true = "bpy.context.scene.render.use_border = True\n";
        $use_crop_to_border_true = "bpy.context.scene.render.use_crop_to_border = True\n";
        $use_border_false = "bpy.context.scene.render.use_border = False\n";
        $use_crop_to_border_false = "bpy.context.scene.render.use_crop_to_border = False\n";
        if (str_contains($use_border_true, $script)) {
            $script = str_replace($use_border_true, $use_border_false, $script);
        }
        else {
            $script .= $use_border_false;
        }
        if (str_contains($use_crop_to_border_true, $script)) {
            $script = str_replace($use_crop_to_border_true, $use_crop_to_border_false, $script);
        }
        else {
            $script .= $use_crop_to_border_false;
        }

        $script .= "bpy.context.scene.cycles.seed = ".(int)($this->getNumber())."\n";
        $script .= "bpy.context.scene.cycles.samples = ".ceil($project->getSamples() / $count_layers)."\n";
        $script .= "bpy.context.scene.cycles.aa_samples = ".ceil($project->getSamples() / $count_layers)."\n";
        if ($project->getUseExr()) {
            // don't force to multi, it's already an exr (either single or multi)
            //    $script .= "bpy.context.scene.render.image_settings.file_format = 'OPEN_EXR_MULTILAYER'\n";
        }
        else {
            $script .= "bpy.context.scene.render.image_settings.file_format = 'PNG'\n";
        }

        // force composer on right device
        if ($use_gpu) {
            $script .= "try:\n";
            $script .= "\tfor scene in bpy.data.scenes:\n";
            $script .= "\t\tscene.render.compositor_device = 'GPU'\n";
            $script .= "\t\tscene.cycles.denoising_use_gpu = True\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }
        else {
            $script .= "try:\n";
            $script .= "\tfor scene in bpy.data.scenes:\n";
            $script .= "\t\tscene.render.compositor_device = 'CPU'\n";
            $script .= "\t\tscene.cycles.denoising_use_gpu = False\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }

        $filename = $project->getUseExr() ? $config['storage']['path'].'scripts/conf_exr.py' : $config['storage']['path'].'scripts/conf.py';
        if (file_exists($filename)) {
            $script .= file_get_contents($filename);
            $script .= "\n";
        }

        return $script;
    }

    public function getSplitHumanExplanation(): string {
        $ret = 'All frames might look the same, but they are actually not.<br>';
        $ret .= 'Each frame is rendered with a different seed, a cycles lower to 1/n, and then reassembled.<br>';
        $ret .= 'Using this method:<br>';
        $ret .= '* The first frame is the background<br>';
        $ret .= '* The second frame is mixed with 50% opacity<br>';
        $ret .= '* The third frame is mixed with 33% opacity<br>';
        $ret .= '* And so on, each Nth frame is mixed with 100/n % opacity<br>';
        return $ret;
    }
}