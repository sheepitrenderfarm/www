<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\ServerRequestTrait;
use App\Utils\Misc;
use Doctrine\ORM\Mapping as ORM;

/**
 * ShepherdReal
 */
#[ORM\Entity]
class ShepherdReal extends Shepherd {
    use ServerRequestTrait;

    protected ?int $cache_rendering_frame_count = null;

    public function isEnabled(): bool {
        if ($this->getEnable() == 0) {
            return false;
        }
//        // check the cap
//        if ($this->getCap() != 0 && $this->getTotal() > $this->getCap()) {
//            return false;
//        }
//        if ($this->getCapToday() != 0 && $this->getTotalToday() > $this->getCapToday()) {
//            return false;
//        }
        return true;
    }

    public function addProject(Project $project, int $width_, int $height_): bool {
        Logger::debug(__METHOD__.' '.$project);

        GlobalInject::getEntityManager()->refresh($project);

        $json = array();

        $json['blend'] = $project->getId();
        $json['framerate'] = $project->getFramerate();
        $json['width'] = $width_;
        $json['height'] = $height_;
        $json['image_extension'] = $project->getPictureExtension();
        $json['mp4'] = $project->getGenerateMp4();
        $json['token_owner'] = $project->getShepherdOwnerToken();
        $json['token_owner_validity'] = $project->getShepherdOwnerTokenValidity();
        $json['token_thumbnail'] = $project->getShepherdThumbnailToken();
        $json['token_thumbnail_validity'] = $project->getShepherdThumbnailTokenValidity();

        $json['frames'] = [];

        foreach ($project->frames() as $frame) {
            $tiles = [];
            foreach ($frame->tiles() as $tile) {
                $tiles [] = array('uid' => $tile->getId(), 'number' => $tile->getNumber(), 'image_extension' => $project->getUseExr() ? 'exr' : 'png', 'token' => $tile->getToken());
            }
            $json['frames'] [] = array('type' => $project->getUseExr() || $frame->tiles()->count() == 1 ? 'full' : ($frame instanceof FrameLayer ? 'layer' : 'region'), 'uid' => $frame->getId(), 'number' => $frame->getNumber(), 'image_extension' => $project->getPictureExtension(), 'tiles' => $tiles);
        }

        $response = $this->httpPostWithStringNoTimeout($this->getId().'/master/blend', json_encode($json));
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    private function delJobFromId(int $jobId): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/blend/'.$jobId.'/remove');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function delProject(Project $project): bool {
        Logger::debug(__METHOD__);
        return $this->delJobFromId($project->getId());
    }

    public function tileSetRendering(Tile $tile): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/tile/'.$tile->getId().'/rendering/'.$tile->getToken());
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function tileReset(Tile $tile): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/tile/'.$tile->getId().'/reset'.$tile->getToken());
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function setBlendOwnerToken(Project $project): bool {
        $config = GlobalInject::getConfigService()->getData();
        if ($this->getTimeout()) {
            return false;
        }

        if ($project->getShepherdOwnerTokenValidity() == 0 || $project->getShepherdOwnerTokenValidity() < time() + 500) {
            // need to set a new token
            $token = Misc::code(32);
            $validity = time() + $config['shepherd']['token_duration']['owner'];

            $response = $this->httpPostNoTimeout($this->getId().'/master/blend/'.$project->getId().'/token/owner', array('token' => $token, 'validity' => $validity));
            if ($response === false) {
                Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
                return false;
            }

            $project->setShepherdOwnerTokenValidity($validity);
            $project->setShepherdOwnerToken($token);
            GlobalInject::getEntityManager()->flush();
            return true;
        }
        else {
            // no need to set a new token, it's still valid
            return true;
        }
    }

    public function setBlendThumbnailToken(Project $project): bool {
        $config = GlobalInject::getConfigService()->getData();
        if ($this->getTimeout()) {
            return false;
        }

        if ($project->getShepherdThumbnailTokenValidity() == 0 || $project->getShepherdThumbnailTokenValidity() < time() + 500) {
            // need to set a new token
            $token = Misc::code(32);
            $validity = time() + $config['shepherd']['token_duration']['thumbnail'];

            $response = $this->httpPostNoTimeout($this->getId().'/master/blend/'.$project->getId().'/token/thumbnail', array('token' => $token, 'validity' => $validity));
            if ($response === false) {
                Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
                return false;
            }

            $project->setShepherdThumbnailTokenValidity($validity);
            $project->setShepherdThumbnailToken($token);
            GlobalInject::getEntityManager()->flush();
            return true;
        }
        else {
            // no need to set a new token, it's still valid
            return true;
        }
    }

    public function setGenerateMP4(Project $project, bool $mp4): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/blend/'.$project->getId().'/generatemp4/'.($mp4 ? '1' : '0'));
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function askForPartialFrameArchive(Project $project): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/blend/'.$project->getId().'/generatepartialarchive');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function taskReset(int $id): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/task/'.$id.'/reset');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        return true;
    }

    public function getTasks(): array {
        if ($this->getTimeout()) {
            return array();
        }

        $response = $this->httpGet($this->getId().'/master/tasks');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return array();
        }

        return json_decode($response->getBody(), true);
    }

    public function getStorageUsed(Project $project): float {
        if ($this->getTimeout()) {
            Logger::debug(__METHOD__.' shepherd in timeout');
            return 0.0;
        }

        $response = $this->httpGetNoTimeout($this->getId().'/master/blend/'.$project->getId().'/storage');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return (float)(0);
        }

        return (float)(''.$response->getBody());
    }

    public function getStoragePrediction(Project $project): float {
        if ($this->getTimeout()) {
            Logger::debug(__METHOD__.' shepherd in timeout');
            return 0.0;
        }

        $response = $this->httpGetNoTimeout($this->getId().'/master/blend/'.$project->getId().'/storage_prediction');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return (float)(0);
        }

        return (float)(''.$response->getBody());
    }

    public function getVersion(): string {
        if ($this->getTimeout()) {
            return 'timeout';
        }

        $response = $this->httpGet($this->getId().'/master/version');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return 'failed';
        }

        return $response->getBody();
    }

    public function removeOrphan(): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/blend/list');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        foreach (json_decode($response->getBody(), true) as $arr) {
            $project = GlobalInject::getEntityManager()->getRepository(Project::class)->find($arr['id']);
            if (is_null($project) || $project->getShepherd()->getId() != $this->getId()) {
                $this->delJobFromId($arr['id']);
            }
        }
        return true;
    }

    public function updatePredictedProjectsSize(): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/blend/list');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        $projectRepository = GlobalInject::getEntityManager()->getRepository(Project::class);
        $average_usage_per_project = GlobalInject::getEntityManager()->getRepository(StatsShepherd::class)->getAverageDiskUsagePerProject();

        foreach (json_decode($response->getBody(), true) as $arr) {
            /** @var ?Project $project */
            $project = $projectRepository->find($arr['id']);
            if (is_object($project)) {
                if (array_key_exists('size_prediction', $arr) && (int)$arr['size_prediction'] != 0) {
                    $prediction = (int)$arr['size_prediction'];
                }
                else {
                    $prediction = $average_usage_per_project;
                }
                $project->setPredictedSizeOnShepherd($prediction);
                GlobalInject::getEntityManager()->flush($project);
            }
        }
        return true;
    }

    /*
     * Get the stats from the node and save it
     */
    public function updateStats(int $now): bool {
        if ($this->getTimeout()) {
            return false;
        }

        $response = $this->httpGet($this->getId().'/master/monitoring');
        if ($response === false) {
            Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
            return false;
        }

        $data = @json_decode($response->getBody(), true);
        if (is_array($data)) {
            $sm = new StatsShepherd();
            $sm->setTime($now);
            $sm->setShepherd($this);

            if (array_key_exists('cpu', $data)) {
                $sm->setCpu($data['cpu']['value']);
                $sm->setCpuMax($data['cpu']['max']);
            }
            if (array_key_exists('cpu pressure', $data)) {
                $sm->setCpuPressure($data['cpu pressure']['value']);
            }
            if (array_key_exists('ram', $data)) {
                $sm->setRam($data['ram']['value']);
                $sm->setRamMax($data['ram']['max']);
            }
            if (array_key_exists('disk', $data)) {
                $sm->setDisk($data['disk']['value']);
            }
            if (array_key_exists('disk pressure', $data)) {
                $sm->setDiskPressure($data['disk pressure']['value']);
            }
            if (array_key_exists('network-rx', $data)) {
                $sm->setNetworkRX($data['network-rx']['value']);
            }
            if (array_key_exists('network-tx', $data)) {
                $sm->setNetworkTX($data['network-tx']['value']);
            }
            if (array_key_exists('task', $data)) {
                $sm->setTask($data['task']['value']);
            }
            if (array_key_exists('httpd', $data)) {
                $sm->setHttpd((int)($data['httpd']['value']));
            }

            $sm->setDiskPrediction(GlobalInject::getEntityManager()->getRepository(Shepherd::class)->getPredictedStorageUsage($this));
            $sm->setBlends($this->getBlendsCount());
            $sm->setRenderingFrames($this->getRenderingFrameCount());
            $sm->setRenderedTiles($this->getRenderedTiles());

            $this->setRenderedTiles(0);
            GlobalInject::getEntityManager()->flush($this);

            GlobalInject::getEntityManager()->persist($sm);
            GlobalInject::getEntityManager()->flush($sm);
        }

        return true;
    }

    public function checkIfStillTimeout(): void {
        if ($this->getTimeout()) {
            $response = $this->httpGet($this->getId().'/master/alive');
            if ($response === false) {
                Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
                return;
            }

            $this->setTimeout(false);
            GlobalInject::getEntityManager()->flush($this);
        }
    }

    public function getLastStats(): ?StatsShepherd {
        return GlobalInject::getEntityManager()->getRepository(StatsShepherd::class)->getLast($this);
    }

    public function getRenderingFrameCount(): int {
        if (is_null($this->cache_rendering_frame_count)) {
            $this->cache_rendering_frame_count = GlobalInject::getEntityManager()->getRepository(Shepherd::class)->getRenderingFrameCount($this);
        }
        return $this->cache_rendering_frame_count;
    }

    public function getBlendsCount(): int {
        return GlobalInject::getEntityManager()->getRepository(Shepherd::class)->getBlendsCount($this);
    }

    public function generateValidationUrl(Tile $tile): string {
        return $this->getId().'/renderer/tile?token='.$tile->getToken().'&id='.$tile->getId();
    }

    /**
     * @inerhitDoc
     */
    public function getTaskQueuePosition(Project $project): array|bool {
        if ($this->getTimeout()) {
            return false;
        }

        if ($project->setTokenOwnerShepherdServer()) {
            $response = $this->httpGet($this->getId().'/blend/'.$project->getShepherdOwnerToken().'/'.$project->getId().'/taskqueue');
            if ($response === false) {
                Logger::debug(__METHOD__.':'.__LINE__.' failed on '.$this->getId());
                return false;
            }

            $ret = @json_decode($response->getBody(), true);
            return is_array($ret) && array_key_exists('total_task', $ret) && array_key_exists('queue_position', $ret) && array_key_exists('tasks', $ret) ? false : $ret;
        }
        return false;

    }
}
