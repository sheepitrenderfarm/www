<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\StatsTeamRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * StatsTeam
 */
#[ORM\Table(name: 'stats_team')]
#[ORM\Index(name: 'date', columns: ['date'])]
#[ORM\Entity(repositoryClass: StatsTeamRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class StatsTeam extends Entity {
    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'date', type: Types::DATE_MUTABLE, nullable: false)]
    protected \DateTimeInterface $date;

    #[ORM\JoinColumn(name: 'team', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'stats', cascade: ['persist'])]
    protected Team $team;

    #[ORM\Column(name: 'points', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points;

    #[ORM\Column(name: 'members', type: Types::INTEGER, nullable: false)]
    protected int $members;

    public function getId(): ?int {
        return $this->id;
    }

    public function setDate(\DateTimeInterface $date): self {
        $this->date = $date;

        return $this;
    }

    public function getDate(): \DateTimeInterface {
        return $this->date;
    }

    public function setTeam(Team $team): self {
        $this->team = $team;

        return $this;
    }

    public function getTeam(): Team {
        return $this->team;
    }

    public function getPoints(): ?float {
        return $this->points;
    }

    public function setPoints(float $points): self {
        $this->points = $points;

        return $this;
    }

    public function getMembers(): int {
        return $this->members;
    }

    public function setMembers(int $members): self {
        $this->members = $members;

        return $this;
    }
}
