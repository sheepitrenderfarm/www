<?php

namespace App\Entity;

use App\Entity;
use App\Repository\HWIDBlacklistRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * HWIDBlacklist
 */
#[ORM\Table(name: 'hwid_blacklist')]
#[ORM\Index(name: 'hwid', columns: ['hwid'])]
#[ORM\Index(name: 'login', columns: ['login'])]
#[ORM\Entity(repositoryClass: HWIDBlacklistRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
#[ORM\HasLifecycleCallbacks]
class HWIDBlacklist extends Entity {
    public const REASONS = [
        1 => 'Multiple account',
        2 => 'Not allow to render',
    ];

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'hwid', type: Types::STRING, length: 50, nullable: false)]
    protected string $hwid = '';

    #[ORM\Column(name: 'login', type: Types::STRING, length: 50, nullable: false)]
    protected string $login = '';

    #[ORM\Column(name: 'reason', type: Types::INTEGER, nullable: false, options: ['default' => '-1'])]
    protected int $reason = -1;

    public function getId(): ?int {
        return $this->id;
    }

    public function getHWID(): string {
        return $this->hwid;
    }

    public function setHWID(string $hwid): self {
        $this->hwid = $hwid;
        return $this;
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function setLogin(string $login): self {
        $this->login = $login;
        return $this;
    }

    public function getReason(): int {
        return $this->reason;
    }

    public function setReason(int $reason): self {
        $this->reason = $reason;
        return $this;
    }
}