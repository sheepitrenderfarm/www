<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\EventRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
#[ORM\Table(name: 'event')]
#[ORM\Index(name: 'session', columns: ['session'])]
#[ORM\Index(name: 'data', columns: ['data'])]
#[ORM\Index(name: 'user', columns: ['user'])]
#[ORM\Index(name: 'project', columns: ['project'])]
#[ORM\Index(name: 'type', columns: ['type'])]
#[ORM\Entity(repositoryClass: EventRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Event extends Entity {

    public const TYPE_REQUEST = 'request';
    public const TYPE_KILL = 'kill';
    public const TYPE_RENDERING = 'rendering';
    public const TYPE_VALIDATE = 'validate';
    public const TYPE_LOGOFF = 'logoff';
    public const TYPE_LOGIN = 'login';
    public const TYPE_SENDERROR = 'senderror';
    public const TYPE_SESSIONRESET = 'session_reset';
    public const TYPE_SESSIONCLEAR = 'session_clear';
    public const TYPE_FRAMERESET = 'frame_reset';

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\Column(name: 'session', type: Types::INTEGER, nullable: true)]
    protected ?int $session = null;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    #[ORM\Column(name: 'user', type: Types::STRING, length: 32, nullable: false)]
    protected string $user;

    #[ORM\Column(name: 'type', type: Types::STRING, length: 50, nullable: false)]
    protected string $type;

    #[ORM\Column(name: 'project', type: Types::INTEGER, nullable: false)]
    protected int $project = 0;

    #[ORM\Column(name: 'data', type: Types::STRING, length: 256, nullable: false)]
    protected string $data = '';

    public function getId(): ?int {
        return $this->id;
    }

    public function getSession(): ?int {
        return $this->session;
    }

    public function setSession(?int $session): self {
        $this->session = $session;

        return $this;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }

    public function getUser(): ?string {
        return $this->user;
    }

    public function setUser(string $user): self {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getProject(): ?int {
        return $this->project;
    }

    public function setProject(int $project): self {
        $this->project = $project;

        return $this;
    }

    public function getData(): ?string {
        return $this->data;
    }

    public function setData(string $data): self {
        $this->data = $data;

        return $this;
    }
}
