<?php

namespace App\Entity;

use App\Scheduler\ClientProtocol;
use App\Service\GlobalInject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Frame created with a chessboard patter
 *
 * getSplit() will give the width x height of the board, example 16 -> 4x4 chessboard
 */
#[ORM\Entity]
class FrameChessboard extends Frame {

    public function getPythonScript(Tile $tile, ClientProtocol $client_protocol, bool $use_gpu): string {
        $config = GlobalInject::getConfigService()->getData();

        $width_chessboard = (int)(sqrt($this->getSplit()));
        $height_chessboard = (int)(sqrt($this->getSplit()));

        $script = '';

        $x_min = ((int)($tile->getNumber() / $width_chessboard)) / (float)($width_chessboard);
        $x_max = $x_min + 1.0 / (float)($width_chessboard);

        $y_min = ((int)($tile->getNumber() % $height_chessboard)) / (float)($height_chessboard);
        $y_max = $y_min + 1.0 / (float)($height_chessboard);

        $script .= "import bpy\n";
        $project = $this->getProject();
        $script .= (new Blender())->getScript();
        if ($project->getUseExr()) {
            // don't force to multi, it's already an exr (either single or multi)
            // $script .= "bpy.context.scene.render.image_settings.file_format = 'OPEN_EXR_MULTILAYER'\n";
        }
        else {
            if ($this->getSplit() > 1) { // allow compositing on not-splited frame
                $script .= "bpy.context.scene.render.image_settings.file_format = 'PNG'\n";
                $script .= "bpy.context.scene.render.use_compositing = False\n";
            }
        }
        $script .= "bpy.context.scene.render.use_stamp = False\n";
        $script .= "## use border and crop\n";
        $script .= "bpy.context.scene.render.use_border = True\n";
        $script .= "bpy.context.scene.render.use_crop_to_border = True\n";
        $script .= "## set border for this tile\n";
        $script .= "bpy.context.scene.render.border_min_x = $x_min\n";
        $script .= "bpy.context.scene.render.border_max_x = $x_max\n";
        $script .= "bpy.context.scene.render.border_min_y = $y_min\n";
        $script .= "bpy.context.scene.render.border_max_y = $y_max\n";

        if ($this->getSplit() == 1) {
            // Do not disable denoising because it's a non-split project
        }
        else {
            // disabled denoising on title project
            $script .= "try:\n";
            $script .= "\tfor layer in bpy.context.scene.render.layers:\n";
            $script .= "\t\tlayer.cycles.use_denoising = False\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }

        // force composer on right device
        if ($use_gpu) {
            $script .= "try:\n";
            $script .= "\tfor scene in bpy.data.scenes:\n";
            $script .= "\t\tscene.render.compositor_device = 'GPU'\n";
            $script .= "\t\tscene.cycles.denoising_use_gpu = True\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }
        else {
            $script .= "try:\n";
            $script .= "\tfor scene in bpy.data.scenes:\n";
            $script .= "\t\tscene.render.compositor_device = 'CPU'\n";
            $script .= "\t\tscene.cycles.denoising_use_gpu = False\n";
            $script .= "except AttributeError:\n";
            $script .= "\tpass\n";
        }

        $filename = $project->getUseExr() ? $config['storage']['path'].'scripts/conf_exr.py' : $config['storage']['path'].'scripts/conf.py';
        if (file_exists($filename)) {
            $script .= file_get_contents($filename);
            $script .= "\n";
        }

        return $script;
    }

    public function getSplitHumanExplanation(): string {
        return '';
    }
}