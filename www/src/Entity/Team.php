<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 */
#[ORM\Table(name: 'team')]
#[ORM\Index(name: 'owner', columns: ['owner'])]
#[ORM\Entity(repositoryClass: TeamRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class Team extends Entity {
    public const NO_JOIN = 0;

    public const OK = 0;
    public const ERROR_TEAM_FULL = -1;
    public const ERROR_WRONG_TEAM = -2;
    public const UNKNOWN = -99;

    #[ORM\Column(name: 'id', type: Types::INTEGER, nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    #[ORM\JoinColumn(name: 'owner', referencedColumnName: 'id', nullable: false)]
    #[ORM\ManyToOne(targetEntity: User::class)]
    protected User $owner;

    #[ORM\Column(name: 'name', type: Types::STRING, length: 60, nullable: false)]
    protected string $name;

    #[ORM\Column(name: 'description', type: Types::TEXT, length: 65535, nullable: false)]
    protected string $description;

    #[ORM\Column(name: 'creation_time', type: Types::INTEGER, nullable: false)]
    protected int $creation_time;

    #[ORM\Column(name: 'points', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points = 0.0;

    #[ORM\Column(name: 'points_former_user', type: Types::FLOAT, precision: 10, scale: 0, nullable: false)]
    protected float $points_former_user = 0.0;

    /**
     * @var Collection<User>
     */
    #[ORM\OneToMany(targetEntity: User::class, mappedBy: 'team', cascade: ['persist'])]
    protected Collection $members;

    /**
     * @var Collection<StatsTeam>
     */
    #[ORM\OneToMany(targetEntity: StatsTeam::class, mappedBy: 'team', cascade: ['persist'])]
    protected Collection $stats;

    public function getId(): ?int {
        return $this->id;
    }

    public function getOwner(): User {
        return $this->owner;
    }

    public function setOwner(User $owner): self {
        $this->owner = $owner;

        return $this;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(string $description): self {
        $this->description = $description;

        return $this;
    }

    public function getCreationTime(): int {
        return $this->creation_time;
    }

    public function setCreationTime(int $creationTime): self {
        $this->creation_time = $creationTime;

        return $this;
    }

    public function getPoints(): float {
        return $this->points;
    }

    public function setPoints(float $points): self {
        $this->points = $points;

        return $this;
    }

    public function getPointsFormerUser(): float {
        return $this->points_former_user;
    }

    public function setPointsFormerUser(float $pointsFormerUser): self {
        $this->points_former_user = $pointsFormerUser;

        return $this;
    }

    public function __addMember(User $user): self {
        if (!$this->members->contains($user)) {
            $this->members->add($user);
            $user->setTeam($this);
        }

        return $this;
    }

    public function __removeMember(User $user): self {
        if ($this->members->contains($user)) {
            $this->members->removeElement($user);
            // set the owning side to null (unless already changed)
        }

        return $this;
    }

    /**
     * @return Collection<User>
     */
    public function members(): Collection {
        return $this->members;
    }

    /**
     * @return Collection<StatsTeam>
     */
    public function stats(): Collection {
        return $this->stats;
    }

    public function __construct() {
        $this->stats = new ArrayCollection();
        $this->members = new ArrayCollection();
    }

    public function getLastNewMember(): ?User {
        $last = null;
        foreach ($this->members() as $user) {
            if (is_null($last) || $last->getTeamJoinTime() < $user->getTeamJoinTime()) {
                $last = $user;
            }
        }
        return $last;
    }
}
