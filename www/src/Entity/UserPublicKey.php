<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Entity;

use App\Entity;
use App\Repository\UserPublicKeyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserPublicKey
 */
#[ORM\Table(name: 'user_public_key')]
#[ORM\Entity(repositoryClass: UserPublicKeyRepository::class)]
#[ORM\Cache(usage: 'NONSTRICT_READ_WRITE')]
class UserPublicKey extends Entity {

    #[ORM\Column(name: 'id', type: Types::STRING, length: 100, nullable: false)]
    #[ORM\Id]
    protected string $id;

    #[ORM\JoinColumn(name: 'user', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'publicKeys')]
    protected User $user;

    #[ORM\Column(name: 'comment', type: Types::STRING, length: 100, nullable: false)]
    protected string $comment;

    #[ORM\Column(name: 'time', type: Types::INTEGER, nullable: false)]
    protected int $time;

    public function getId(): string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;

        return $this;
    }

    public function getUser(): User {
        return $this->user;
    }

    public function setUser(User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getComment(): ?string {
        return $this->comment;
    }

    public function setComment(string $comment): self {
        $this->comment = $comment;

        return $this;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time): self {
        $this->time = $time;

        return $this;
    }
}
