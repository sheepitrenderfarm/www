<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Constant;
use App\Entity\Blender;
use App\Entity\Frame;
use App\Entity\FrameChessboard;
use App\Entity\FrameLayer;
use App\Entity\Liaison;
use App\Entity\PastProject;
use App\Entity\Project;
use App\Entity\ProjectAnalyse;
use App\Entity\Team;
use App\Entity\Tile;
use App\Entity\TileReal;
use App\Entity\User;
use App\Repository\AdventCalendarClaimedDayRepository;
use App\Repository\BlenderRepository;
use App\Repository\CPURepository;
use App\Repository\GPURepository;
use App\Repository\LiaisonRepository;
use App\Repository\ProjectAnalyseRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionErrorRepository;
use App\Repository\TeamRepository;
use App\Scheduler\ClientError;
use App\Service\ConfigService;
use App\Service\Logger;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\File;
use App\Utils\Misc;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Project controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/project')]
class ProjectController extends BaseController {
    public const MAX_SIZE = Constant::PROJECT_ARCHIVE_MAX_SIZE;

    private ProjectRepository $projectRepository;
    private LiaisonRepository $liaisonRepository;
    private TeamRepository $teamRepository;
    private SessionErrorRepository $sessionErrorRepository;
    private CPURepository $cpuRepository;
    private GPURepository $gpuRepository;
    private AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository;
    private BlenderRepository $blenderRepository;
    private ProjectAnalyseRepository $projectAnalyseRepository;

    public function __construct(UrlGeneratorInterface $router, ConfigService $configService, HTML $html, Main $main, ProjectAnalyseRepository $projectAnalyseRepository, ProjectRepository $projectRepository, TeamRepository $teamRepository, LiaisonRepository $liaisonRepository, SessionErrorRepository $sessionErrorRepository, CPURepository $cpuRepository, GPURepository $gpuRepository, EntityManagerInterface $entityManager, AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository, BlenderRepository $blenderRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->projectRepository = $projectRepository;
        $this->teamRepository = $teamRepository;
        $this->liaisonRepository = $liaisonRepository;
        $this->sessionErrorRepository = $sessionErrorRepository;
        $this->cpuRepository = $cpuRepository;
        $this->gpuRepository = $gpuRepository;
        $this->adventCalendarClaimedDayRepository = $adventCalendarClaimedDayRepository;
        $this->blenderRepository = $blenderRepository;
        $this->projectAnalyseRepository = $projectAnalyseRepository;

        $this->config = $configService->getData();
    }

    /**
     * Progress on upload file
     */
    #[Route(path: '/internal/progress')]
    public function upload_progress(Request $request): JsonResponse {
        $upload_progress = [];
        if ($request->request->has('uid')) {
            $uid = $request->request->get('uid');
            $data = uploadprogress_get_info($uid);
            if (is_array($data)) {
                $upload_progress['content_length'] = $data['bytes_total'];
                $upload_progress['bytes_processed'] = $data['bytes_uploaded'];
            }
        }
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-Type: application/json');
        return new JsonResponse($upload_progress);
    }

    /**
     * Upload of file
     */
    #[Route(path: '/internal/upload')]
    public function add_step1(Request $request): Response {
        if ($request->files->has('addproject_archive') == false) {
            return $this->render('error.html.twig', [
                'error_title' => 'Oops',
                'message' => 'File upload failed (1).',
            ]);
        }
        else {
            $errorMessage = $this->addProjectCheck(null, null, null);
            if (is_string($errorMessage)) {
                return new Response($errorMessage);
            }

            /** @var ?UploadedFile $file */
            $file = $request->files->get('addproject_archive');
            if (is_null($file) || $file->isValid() == false) {
                if (is_object($file) && $file->getError() != UPLOAD_ERR_PARTIAL) {
                    Logger::error(__METHOD__.' error upload failed: '.$file->getErrorMessage());
                }

                return $this->render('error.html.twig', [
                    'error_title' => 'Oops',
                    'message' => 'Upload failed.',
                ]);
            }

            if ($file->getSize() > self::MAX_SIZE) {
                unlink($file->getPath());

                return $this->render('error.html.twig', [
                    'error_title' => 'Oops',
                    'message' => sprintf("The file you are uploading is too big, it must be under %sB.", Misc::humanSize(self::MAX_SIZE)),
                ]);
            }

            $name = $file->getClientOriginalName();

            $working_dir = File::tempdir($this->config['tmp_dir']);
            if ($file->getClientOriginalExtension() == 'blend' || $file->getClientOriginalExtension() == 'zip') {
                $file->move($working_dir, $name); // TODO handle FileException
                $target = $working_dir.'/'.$name;
            }
            else {
                Logger::debug(__METHOD__.':'.__LINE__.' other extension: '.$file->getClientOriginalExtension());
                $error_message = '<br/>Error mostly due to a wrong file type<br>The renderfarm supports <strong>blender file</strong> or <strong>zip file</strong>.';
                $error_message .= '<br/>Your file type is <strong>'.$file->getMimeType().'</strong>.';

                File::rmdirRecurse($working_dir);

                return $this->render('error.html.twig', [
                    'error_title' => 'Oops',
                    'message' => $error_message,
                ]);
            }

            $projectAnalyse = new ProjectAnalyse();
            $projectAnalyse->setId($this->projectAnalyseRepository->generateToken());
            $projectAnalyse->setOwner($this->getUser());
            $projectAnalyse->setPath($target);
            $projectAnalyse->setDate(new DateTime());
            $projectAnalyse->setName(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));

            $this->entityManager->persist($projectAnalyse);
            $this->entityManager->flush();

            return new RedirectResponse($this->generateUrl('app_project_add_step2', ['token' => $projectAnalyse->getId()]));
        }
    }

    /**
     * Display of waiting to analyse your project
     */
    #[Route(path: '/add/{token}')]
    public function add_step2(string $token): Response {
        return $this->render('project/add_step2.html.twig', [
            'token' => $token,
        ]);
    }

    /**
     * Actual analyse of the project
     */
    #[Route(path: '/add_analyse/{token}')]
    public function add_step3(string $token, Request $request): Response {
        $projectAnalyse = $this->projectAnalyseRepository->findOneBy(['id' => $token, 'owner' => $this->getUser()->getId()]);
        if (is_null($projectAnalyse)) {
            return $this->render('error.html.twig', [
                'error_title' => 'Oops',
                'message' => 'Failed to find uploaded file, please try again.',
            ]);
        }
        elseif ($projectAnalyse->getStatus() == Constant::WAITING) {
            return new Response(json_encode(['status' => 'RETRY']));
        }
        elseif ($projectAnalyse->getStatus() == Constant::PROCESSING) {
            return new Response(json_encode(['status' => 'PROCESSING', 'analysed' => $projectAnalyse->getAnalysedFilesCount(), 'total' => $projectAnalyse->getTotalFileCount()]));
        }
        elseif ($projectAnalyse->getStatus() == Constant::FINISHED) {
            return $this->render('project/analyse_finished.html.twig', [
                'projectAnalyse' => $projectAnalyse,
                'dirname' => dirname($projectAnalyse->getPath()),
                'result' => json_decode($projectAnalyse->getResult(), true),
                'token' => $token,
            ]);
        }
        else {
            // should never happen
            return new Response('Internal error, please retry to upload your file');
        }
    }

    /**
     * Add a project to the farm
     */
    #[Route(path: '/add_internal')]
    public function add_internal(Request $request): Response {
        if ($request->request->has('start_frame') == false ||
            $request->request->has('end_frame') == false ||
            $request->request->has('step_frame') == false ||
            $request->request->has('path') == false ||
            $request->request->has('framerate') == false ||
            $request->request->has('width') == false ||
            $request->request->has('height') == false ||
            $request->request->has('executable') == false ||
            $request->request->has('compute_method') == false ||
            $request->request->has('engine') == false ||
            $request->request->has('render_on_gpu_headless') == false ||
            $request->request->has('public_render') == false ||
            $request->request->has('public_thumbnail') == false ||
            $request->request->has('generate_mp4') == false ||
            $request->request->has('type') == false ||
            $request->request->has('split_tiles') == false ||
            $request->request->has('cycles_samples') == false ||
            $request->request->has('samples_pixel') == false ||
            $request->request->has('image_extension') == false ||
            $request->request->has('use_adaptive_sampling') == false ||
            $request->request->has('token') == false ||
            ctype_alnum($request->request->get('token')) == false ||
            $request->request->has('archive') == false) {
            return new Response('missing parameter');
        }
        set_time_limit(300); // dirty hack :(

        $token = $request->request->get('token');
        $frame_start = $request->request->get('start_frame');
        $frame_end = $request->request->get('end_frame');

        $projectAnalyse = $this->projectAnalyseRepository->findOneBy(['id' => $token, 'owner' => $this->getUser()->getId()]);

        if (is_null($projectAnalyse)) {
            Logger::error(__METHOD__.' failed to found project analyse (token: '.$token.' )');
            return new Response('<p class="error"><center><span style="color:red">Failed add the project: failed to found data, you need to re-upload your file.</center></span></p>');
        }

        $errorMessage = $this->addProjectCheck($frame_start, $frame_end, $request->request->get('executable'));
        if (is_string($errorMessage)) {
            return new Response('<p class="error"><span style="color:red">'.$errorMessage.'</span></p>');
        }

        $frame_step = (int)($request->request->get('step_frame'));
        if ($frame_step <= 0) {
            $frame_step = 1;
        }

        $tile_per_frame = 1;
        $frame_classname = '';

        $project = new Project();
        if ($request->request->get('samples_pixel') > 0) {
            $project->setSamples($request->request->get('samples_pixel'));
        }
        if ($request->request->get('type') == 'animation') {
            if ($request->request->get('split_tiles') == 1 || ($request->request->get('split_tiles') == -1 && $request->request->get('split_samples') == 1)) {
                $tile_per_frame = 1;
                $frame_classname = FrameChessboard::class;
            }
            elseif ($request->request->get('split_tiles') == -1) {
                $frame_classname = FrameLayer::class;
                if ($request->request->has('split_samples') && $request->request->get('split_samples') > 0) {
                    $tile_per_frame = (int)($request->request->get('split_samples'));
                }
            }
            else {
                $frame_classname = FrameChessboard::class;
                if ((int)($request->request->get('split_tiles')) > 1) {
                    $tile_per_frame = (int)($request->request->get('split_tiles')) * (int)($request->request->get('split_tiles'));
                }
            }
        }
        elseif ($request->request->get('type') == 'singleframe') {
            $frame_end = $frame_start;

            if ($request->request->has('split_samples') && $request->request->get('split_samples') > 0) {
                $frame_classname = FrameLayer::class;
                $tile_per_frame = (int)($request->request->get('split_samples'));
            }
            else {
                $frame_classname = FrameChessboard::class;
                $tile_per_frame = $this->config['project']['singleframe']['nb_tile'] * $this->config['project']['singleframe']['nb_tile'];
            }
        }

        if ((($frame_end - $frame_start) * $tile_per_frame / $frame_step) > $this->config['project']['max_frames']) {

            $html = '<p><span style="color:red">Error: ';
            if ($tile_per_frame > 1) {
                $html .= sprintf('You can add project up to %s tiles, this project is over this limit, with your current tile setup you can go up to %s frames.', number_format($this->config['project']['max_frames']), number_format($this->config['project']['max_frames'] / $tile_per_frame));
            }
            else {
                $html .= sprintf('You can add project up to %s frames, this project is over this limit.', number_format($this->config['project']['max_frames']));
            }
            $html .= '</span></p>';
            return new Response($html);
        }

        if ($request->request->get('image_extension') == '.exr') {
            $project->setUseExr(true);
        }
        else {
            $project->setUseExr(false);
        }
        $project->setPictureExtension(substr($request->request->get('image_extension'), 1, 3));

//        $project->setStatus(Constant::WAITINGFORARCHIVECREATION);
        $project->setPath($request->request->get('path'));
        $project->setFramerate($request->request->get('framerate'));
        $project->setEngine($request->request->get('engine'));
        $project->setRenderOnGpuHeadless(boolval($request->request->get('render_on_gpu_headless')));

        if ($request->request->has('max_ram_optional')) {
            $project->setMaxMemoryUsed((int)($request->request->get('max_ram_optional')) * 1024); // MB -> kB
        }

        $project->setExecutable($this->blenderRepository->findOneBy(['bf_code' => $request->request->get('executable'), 'enable' => true]));
        $project->setPublicRender($request->request->get('public_render') == '1');
        $project->setComputeMethod((int)($request->request->get('compute_method')));
        if ($project->getComputeMethod() == 0) {
            $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        }

        $project->setName($this->projectRepository->generateUniqueProjectName(basename($request->request->get('archive')), $frame_start, $frame_end));
        $project->setOwner($this->getUser());
        $project->setPublicThumbnail($this->getUser()->canDisableFrameThumbnail() ? $request->request->get('public_thumbnail') == '1' : '1'); // set to 1 if the user can not disable thumbnail

        if ((int)$request->request->get('width') % 2 == 1 || (int)$request->request->get('height') % 2 == 1) {
            Logger::debug(__METHOD__.sprintf(' disabled mp4 because %dx%d is not divisible by 2', (int)$request->request->get('width'), (int)$request->request->get('height')));
            $project->setGenerateMp4(false);
            $project->setCanGenerateMp4(false);
        }
        else {
            $project->setGenerateMp4($request->request->get('generate_mp4') == '1');
            $project->setCanGenerateMp4(true);
        }
        $project->setStatus(Constant::WAITING);


//        if (file_exists($request->getSession()->get('upload-'.$token, [])['filename']) == false) {
//            Logger::error("(ajax) project file not found ".$request->getSession()->get('upload-'.$token, [])['filename']);
//            return new Response('Your project failed to upload properly. It\'s an internal bug on the SheepIt side. There are no files on the server. Please attempt to upload the project again.');
//        }

        $shepherds = $this->main->bestShepherdServers();
        if (count($shepherds) < 1) {
            Logger::error(__METHOD__.' failed to found a shepherd (all enabled? in timeout ? cap_frames reached?');
            return new Response('<p class="error"><center><span style="color:red">Failed add the project: failed to found an available frame storage server (shepherd).</center></span></p>');
        }
        reset($shepherds);
        $shepherd = current($shepherds);
        $project->setShepherd($shepherd);
        // set token on shepherd
        $project->setShepherdOwnerTokenValidity(time() + $this->config['shepherd']['token_duration']['owner']);
        $project->setShepherdOwnerToken(Misc::code(32));
        $project->setShepherdThumbnailTokenValidity(time() + $this->config['shepherd']['token_duration']['thumbnail']);
        $project->setShepherdThumbnailToken(Misc::code(32));
        $project->setLastupdatearchive(time());

        $this->entityManager->persist($project);
        $this->entityManager->flush($project);
        Logger::info("Project '".$project->getName()."' (".$project->getId().") created by ".$this->getUser()->getId());

        $this->getUser()->setNbProjects($this->getUser()->getNbProjects() + 1);

        $pastProject = new PastProject();
        $pastProject->setId($project->getId());
        $pastProject->setName($project->getName());
        $pastProject->setOwner($project->getOwner());
        $pastProject->setCreation(time());
        $this->entityManager->persist($pastProject);

        $this->main->addLatestPublicProject($project);

        $ret = $this->projectRepository->addFrames($project, $frame_start, $frame_end, $frame_step, (int)$request->request->get('width'), (int)$request->request->get('height'), $tile_per_frame, $frame_classname, TileReal::class, dirname($projectAnalyse->getPath()));
        if ($ret > 0) {
            if ($request->request->get('cycles_samples') != 0) {
                $sample_per_tile = (int)$request->request->get('cycles_samples') / $tile_per_frame;
                if ($this->config['project']['max_samples_per_tile'] < $sample_per_tile) {
                    if ((int)$request->request->get('samples_pixel') == 4096 && $request->request->get('use_adaptive_sampling') == '1') {
                        // exception for adaptives samples at 4096 because it's the default on blender
                        Logger::debug(__METHOD__.' do not block since it is a special case for default blend values');
                    }
                    else {
                        $this->projectRepository->block($project, 90);
                    }
                }
            }
            $this->main->countPublicActiveProjects(false); // regenerate cache

            $serverUrl = (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? "https" : "http")."://".$_SERVER['HTTP_HOST'];

            $this->entityManager->remove($projectAnalyse);
            $this->entityManager->flush();

            return new Response($serverUrl.$this->generateUrl("app_project_manage", ['project' => $project->getId()]));
        }
        else {
            Logger::error(__METHOD__." addproject failed ret ".serialize($ret));
            // do not remove the project, it's already removed on addFrames

            if ($ret == -1) {
                return new Response('<p class="error"><center><span style="color:red">Failed add the project: failed to found an available frame storage server (shepherd).</center></span></p>');
            }
            else {
                return new Response('Failed to add project');
            }
        }
    }

    /**
     * Block a project (no not be rendered anymore)
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/block/{reason}')]
    public function update_block(Project $project, int $reason): Response {
        $this->projectRepository->block($project, $reason);
        return new Response('OK');
    }

    /**
     * Unblock a project
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/unblock')]
    public function update_unblock(Project $project): Response {
        $this->projectRepository->unblock($project);
        return new Response('OK');
    }

    /**
     * Change renderer binary
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/binary/{binary}')]
    public function update_binary(Project $project, string $binary): Response {
        if (($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID || $project->getId() == Project::POWER_DETECTION_PROJECT_ID) && $this->getUser()->isSuperAdmin() == false) {
            return new Response('You have no permission to modify this project');
        }
        elseif ($project->canManageProject($this->getUser())) {
            $binaryObject = null;
            foreach ($this->blenderRepository->findAll() as $blenderBinary) {
                if ($blenderBinary->getId() == $binary) {
                    $binaryObject = $blenderBinary;
                    break;
                }
            }

            if (is_null($binaryObject)) {
                return new Response('Bad value');
            }
            else {
                $project->setExecutable($binaryObject);
                $this->entityManager->flush($project);
            }
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Change render method
     */
    #[Route(path: '/{project}/computemethod/{method}/{enable}')]
    public function update_computemethod(Project $project, string $method, bool $enable): Response {
        if (($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID || $project->getId() == Project::POWER_DETECTION_PROJECT_ID) && $this->getUser()->isSuperAdmin() == false) {
            return new Response('You have no permission to modify this project');
        }
        elseif ($project->canManageProject($this->getUser())) {
            if ($method == 'cpu') {
                $project->setComputeMethod(Misc::setMaskValue($project->getComputeMethod(), Constant::COMPUTE_CPU, (int)$enable));
                $project->setComputeMethod(Misc::setMaskValue($project->getComputeMethod(), Constant::COMPUTE_GPU, (int)(!$enable)));

                $current_timeref = $project->getCacheRenderTimeRef();
                $new_timeref = $current_timeref * (float)($this->config['power']['gpu']['const']) / (float)($this->config['power']['cpu']['const']);
                $project->setCacheRendertimeRef($new_timeref);
            }
            elseif ($method == 'gpu') {
                if ($enable == false && ($project->getEngine() == 'BLENDER_EEVEE' || $project->getEngine() == 'BLENDER_EEVEE_NEXT')) { // no cpu on eevee projects
                    return new Response('CPU rendering not supported by eevee project');
                }
                $project->setComputeMethod(Misc::setMaskValue($project->getComputeMethod(), Constant::COMPUTE_GPU, (int)$enable));
                $project->setComputeMethod(Misc::setMaskValue($project->getComputeMethod(), Constant::COMPUTE_CPU, (int)(!$enable)));

                $current_timeref = $project->getCacheRenderTimeRef();
                $new_timeref = $current_timeref * (float)($this->config['power']['cpu']['const']) / (float)($this->config['power']['gpu']['const']);
                $project->setCacheRendertimeRef($new_timeref);
            }

            $this->entityManager->flush($project);
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Change visibility (public render)
     */
    #[Route(path: '/{project}/visibility/{public}')]
    public function update_visibility(Project $project, bool $public): Response {
        if (($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID || $project->getId() == Project::POWER_DETECTION_PROJECT_ID) && $this->getUser()->isSuperAdmin() == false) {
            return new Response('You have no permission to modify this project');
        }
        elseif ($project->canManageProject($this->getUser())) {
            $project->setPublicRender($public);
            $this->entityManager->flush($project);
            $this->main->countPublicActiveProjects(false); // regenerate cache
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Change mp4 generation
     */
    #[Route(path: '/{project}/mp4/{generate}')]
    public function update_mp4(Project $project, bool $generate): Response {
        if ($project->canManageProject($this->getUser())) {
            $project->setGenerateMp4($generate);
            $this->entityManager->flush($project);
            $project->getShepherd()->setGenerateMP4($project, $generate);
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }


    /**
     * Allow team to render a project
     */
    #[Route(path: '/{project}/acl/renderer/team/add/{team}')]
    public function acl_renderer_team_add(Project $project, Team $team): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->addTeamRenderer($team)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Forbid team to render a project
     */
    #[Route(path: '/{project}/acl/renderer/team/del/{team}')]
    public function acl_renderer_team_del(Project $project, Team $team): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->delTeamRenderer($team)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Allow a user to render a project
     */
    #[Route(path: '/{project}/acl/renderer/user/add/{user}')]
    public function acl_renderer_user_add(Project $project, User $user): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->addRenderer($user)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Forbid a user to render a project
     */
    #[Route(path: '/{project}/acl/renderer/user/del/{user}')]
    public function acl_renderer_user_del(Project $project, User $user): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->delRenderer($user)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Allow a user to manage a project
     */
    #[Route(path: '/{project}/acl/manager/user/add/{user}')]
    public function acl_manager_user_add(Project $project, User $user): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->addManager($user)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    /**
     * Forbid a user to manage a project
     */
    #[Route(path: '/{project}/acl/manager/user/del/{user}')]
    public function acl_manager_user_del(Project $project, User $user): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->delManager($user)) {
                return new Response('OK');
            }
            else {
                return new Response('NOK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[Route(path: '/{project}/chart')]
    public function chart(Project $project): Response {
        if ($project->canSeeProject($this->getUser()) == false) {
            return $this->render('error.html.twig', [
                    'error_title' => 'Forbidden',
                    'message' => 'You are not allowed to see this project.',
                ]
            );
        }

        $tiles = $project->tilesWithStatus(Constant::FINISHED);
        if (count($tiles) < 2) {
            // we need at least 2 frames to make a line
            return $this->render('error.html.twig', [
                    'error_title' => 'Error',
                    'message' => 'Not enough data to generate stats.',
                ]
            );
        }

        $frames = $project->framesWithStatus(Constant::FINISHED);
        $show_rendertime_frames = count($frames) > 1;

        $users = array();
        foreach ($tiles as $tile) {
            $users[$tile->getUser()->getId()] = $tile->getUser()->getId();
        }
        natcasesort($users);

        return $this->render('project/chart.html.twig', [
            'project' => $project,
            'users' => $users,
            'frames' => $frames,
            'show_rendertime_frames' => $show_rendertime_frames,
            'progression_data' => $this->progression_array_javascript($tiles),
            'piechart_data' => $this->piechart_array_javascript($tiles),
            'rendertime_data' => $this->rendertime_array_javascript($frames),


        ]);
    }

    #[Route(path: '/{project}/resume')]
    public function resume(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            if ($project->getOwner()->getNumberOfConcurrentProject() >= $this->config['project']['max_concurrent_project_per_user']) {
                return new Response("Resume is disabled because you have too many active projects");
            }
            else {
                $project->resume();
                return new Response('OK');
            }
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[Route(path: '/{project}/pause')]
    public function pause(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            $project->pause();
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/{project}/boost')]
    public function boost(Project $project): Response {
        $project->setOwnerPointsOnlastupdate($this->config['rank']['points']['max']);
        $this->entityManager->flush();
        return new Response('OK');
    }

    #[Route(path: '/{project}/remove')]
    public function remove(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            $this->projectRepository->removeDelay($project);
            return new RedirectResponse($this->generateUrl('app_user_profile_generic'));
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[Route(path: '/{project}/remove_no_redirect')]
    public function remove_no_redirect(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            $this->projectRepository->removeDelay($project);
            return new Response('EMPTY');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[Route(path: '/{project}/remove_no_redirect2')]
    public function remove_no_redirect2(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            $this->projectRepository->removeDelay($project);
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this project');
        }
    }

    #[Route(path: '/{project}/reset')]
    public function reset(Project $project): Response {
        if ($project->canManageProject($this->getUser()) == false) {
            return new Response('You have no permission to modify this project');
        }

        $project->reset();
        return new Response('OK');
    }

    #[Route(path: '/{project}', requirements: ['project' => '\d+'])]
    public function manage(Project $project): Response {
        if ($project->canSeeProject($this->getUser()) == false) {
            return $this->render('error.html.twig', [
                    'error_title' => 'Forbidden',
                    'message' => 'You are not allowed to manage this project.',
                ]
            );
        }

        $project->setTokenOwnerShepherdServer();

        // Managers
        $manager_links = $this->liaisonRepository->getLiaisons(Liaison::USER_MANAGE_PROJECT, null, $project->getId());

        // Renderers
        $display_renderers_panel = ($project->isBlocked() == false);
        $renderer_links = $this->liaisonRepository->getLiaisons(Liaison::USER_RENDER_PROJECT, null, $project->getId());
        $team_links = array();
        foreach ($this->liaisonRepository->getLiaisons(Liaison::TEAM_RENDER_PROJECT, null, $project->getId()) as $l) {
            $team_links [] = $this->teamRepository->find($l->getPrimary());
        }

        // Actions
        $is_boosted = $project->getOwnerPointsOnlastupdate() >= $this->config['rank']['points']['max'];

        // Compute method
        $checked_cpu = Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU);
        $checked_gpu = Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU);

        $types_pie = array();
        $errors = $this->sessionErrorRepository->findBy(['project' => $project->getId()]);
        foreach ($errors as $row) {
            if (array_key_exists($row->getType(), $types_pie) == false) {
                $types_pie[$row->getType()] = 0;
            }
            $types_pie[$row->getType()] += 1;
        }

        $error_type_js_values = "[['Error', 'Value'],";
        foreach ($types_pie as $type => $count) {
            $error_type_js_values .= '[\''.ClientError::toHuman($type).'\','.$count.'],';
        }
        $error_type_js_values .= ']';

        return $this->render('project/manage.html.twig', [
            'project' => $project,
            'errors' => $errors,
            'is_boosted' => $is_boosted,
            'checked_cpu' => $checked_cpu,
            'checked_gpu' => $checked_gpu,
            'renderer_links' => $renderer_links,
            'team_links' => $team_links,
            'display_renderers_panel' => $display_renderers_panel,
            'manager_links' => $manager_links,
            'errors_type_js_values' => $error_type_js_values,
            'block_messages_short' => array_map(function ($d) {
                return $d['short'];
            }, Project::BLOCK_MESSAGES),
            'blender_binaries' => $this->blenderRepository->findAll(),
            'display_generate_mp4' => $project->isSingleFrame() == false && $project->getUseExr() == false,
        ]);
    }

    #[Route(path: '/frames/{project}/interval/{interval}', defaults: ['interval' => 10])]
    public function frames(Project $project, int $interval): Response {
        if ($project->canSeeProject($this->getUser()) == false) {
            return $this->render('error.html.twig', [
                    'error_title' => 'Error',
                    'message' => 'You are not authorized to see this project.',
                ]
            );
        }

        return $this->render('project/frames.html.twig', [
            'project' => $project,
            'interval' => $interval,
        ]);
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/action/regeneratetoken')]
    public function regeneratetoken(Project $project): Response {
        $project->setShepherdOwnerToken("");
        $project->setShepherdThumbnailToken("");
        $project->setShepherdOwnerTokenValidity(0);
        $project->setShepherdThumbnailTokenValidity(0);
        $this->entityManager->flush();

        return new RedirectResponse($this->generateUrl('app_project_manage', ['project' => $project->getId()]));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/action/resetgpuusage')]
    public function resetgpuusage(Project $project): Response {
        $project->resetGPUUsage();

        return new RedirectResponse($this->generateUrl('app_project_manage', ['project' => $project->getId()]));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{project}/action/regeneratestats')]
    public function regeneratestats(Project $project): Response {
        $project->generateStatsCache();
        $this->entityManager->flush();

        return new RedirectResponse($this->generateUrl('app_project_manage', ['project' => $project->getId()]));
    }

    #[Route(path: '/{project}/action/partialframearchive')]
    public function askpartialframearchive(Project $project): Response {
        if ($project->canManageProject($this->getUser())) {
            // first pause the project to avoid new frame on the shepherd
            $project->pause();
            $shepherd = $project->getShepherd();
            if ($shepherd->askForPartialFrameArchive($project)) {
                return new Response('OK');
            }
        }
        return new Response('failed-askpartialframearchive');
    }

    #[Route(path: '/estimator', methods: 'POST')]
    public function estimator(Request $request): Response {
        if ($request->request->has('time') &&
            $request->request->has('count') &&
            $request->request->has('device')) {

            $content = '';
            $device_id_str = $request->request->get('device');
            $dev = null;
            if (str_starts_with($device_id_str, 'cpu_')) {
                $dev = $this->cpuRepository->find((int)(substr($device_id_str, 4)));
            }
            elseif (str_starts_with($device_id_str, 'gpu_')) {
                $dev = $this->gpuRepository->find((int)(substr($device_id_str, 4)));
            }

            if (is_object($dev)) {
                $time_per_frame = ceil((float)($request->request->get('time')) * 60.0 * $dev->getPower() / $dev->getPowerConst());
                $cost_per_frame = -1.0 * $time_per_frame * $this->config['rank']['points']['ordered']['minute'] / 60.0;
                $content .= '<br>';
                $content .= '<h4>How much your project will cost you</h4>';
                $content .= sprintf('Your project will cost you up to <strong>%s points</strong>.', number_format($cost_per_frame * $dev->getPowerRatioOverCPU() * (int)($request->request->get('count'))));
                $content .= '<h4>How you could split your project</h4>';
                $content .= 'Having a very low render time is not necessarily a good thing. Please keep the render time over 1 minute.<br>If you could try to pick a render time of about 15 minutes, you can keep a margin of error for the max render time.';
                $content .= '<table class="table table-bordered table-striped  ">';
                $content .= '<thead>';
                $content .= '<tr>';
                $content .= '<th style="text-align: center; vertical-align: middle;">Number of tiles</th>';
                $content .= '<th style="text-align: center; vertical-align: middle;">Expected render time</th>';
                $content .= '</tr>';
                $content .= '</thead>';
                $content .= '<tbody>';
                for ($i = 16.0; $i > 0; $i--) {
                    $time_splited = $time_per_frame / $i;
                    if ($time_splited < 60 && $i != 1) {
                        continue;
                    }
                    $content .= '<tr>';

                    $content .= '<td style="text-align: center; vertical-align: middle;" >';
                    if ($i == 1) {
                        $content .= 'No split';
                    }
                    else {
                        $content .= $i;
                    }
                    $content .= '</td>';

                    $content .= '<td style="text-align: center; vertical-align: middle;" >';
                    if ($time_splited < $this->config['power']['rendertime_max_reference']) {
                        $content .= '<span class="label label-success">';
                    }
                    else {
                        $content .= '<span class="label label-danger">';
                    }
                    $content .= Misc::humanTime($time_splited, true);
                    $content .= '</span>';
                    $content .= '</td>';

                    $content .= '</tr>';
                }
                $content .= '</tbody>';
                $content .= '</table>';
            }
            else {
                return new Response('failed to import device');
            }
            return new Response($content);
        }
        else {
            return new Response('failed-estimator');
        }
    }

    private function nearestInf(array $tab, int $val): int {
        $mini = 0;
        $place = 0;
        for ($i = 0; $i < sizeof($tab); $i++) {
            if ($mini < $tab[$i] and $tab[$i] <= $val) {
                $place = $i;
                $mini = $tab[$i];
            }

        }
        return $place;
    }

    /**
     * @param array $frames_ List of FINISHED frames
     */
    private function progression_array_javascript(array $frames_): string {
        if (count($frames_) == 0) {
            return "[]";
        }

        $ret = '[';
        $ret .= "['Time', 'rendered frames'],";

        usort($frames_, '\App\Utils\Sort::sortByAttributeRequestTime');

        $temp_depart = time() + 10;
        $temp_fin = 0;

        foreach ($frames_ as $a_frame) {
            if ($a_frame->getRequestTime() < $temp_depart) {
                $temp_depart = $a_frame->getRequestTime();
            }
            if ($temp_fin < $a_frame->getRequestTime()) {
                $temp_fin = $a_frame->getRequestTime();
            }
        }

        $tab_request_time = array();

        foreach ($frames_ as $a_frame) {
            $request_time = $a_frame->getRequestTime();
            $tab_request_time [] = (int)($request_time) - $temp_depart;
        }

        $diff_max_temps = $temp_fin - $temp_depart;

        $tab_avancement = array();

        for ($j = 0; $j < sizeof($tab_request_time); $j++) {
            $nb_frame_rendues = 0;
            for ($i = 0; $i < sizeof($tab_request_time); $i++) {
                if ($tab_request_time[$i] < $tab_request_time[$j]) {
                    $nb_frame_rendues += 1;
                }
            }
            $tab_avancement [] = $nb_frame_rendues;
        }

        $n = (int)$diff_max_temps / 100;

        for ($j = 0; $j < $diff_max_temps; $j = $j + $n) {
            $ret .= "['".Misc::humanTime($j, false)."',".$tab_avancement[$this->nearestInf($tab_request_time, $j)].'],';
        }
        $ret .= ']';
        return $ret;
    }

    /**
     * @param Tile[] $frames_ List of FINISHED frames
     */
    private function piechart_array_javascript(array $frames_): string {
        $ret = '[';
        $ret .= "['User', 'frames'],";

        $datas = array();

        foreach ($frames_ as $a_frame) {
            $key = $a_frame->getUser()->getId();
            if (array_key_exists($key, $datas) == false) {
                $datas[$key] = 0;
            }
            $datas[$key] += 1;
        }

        arsort($datas);
        foreach ($datas as $login => $nb_frames) {
            $ret .= '[\''.$login.'\','.$nb_frames.'],';
        }

        $ret .= ']';
        return $ret;
    }

    /**
     * @param Frame[] $frames_ List of FINISHED frames
     */
    private function rendertime_array_javascript(array $frames_): string {
        $ret = '[';
        $ret .= "['Frame', 'Render time on reference machine (in secondes)'],";

        foreach ($frames_ as $a_frame) {
            $ret .= "['".$a_frame->getNumber()."',".$a_frame->getRenderTimeRef().'],';
        }

        $ret .= ']';
        return $ret;
    }

    private function addProjectCheck(?int $frame_start, ?int $frame_end, ?string $binary): ?string {
        if ($this->config['project']['maintenance']['enable']) {
            return sprintf("Server in maintenance, you can't add a project. %s", $this->config['project']['maintenance']['reason']);
        }

        $avatar_path = $this->config['storage']['path'].'users/avatars/big/'.$this->getUser()->getId().'.png';
        if (file_exists($avatar_path) == false) {
            return sprintf("You can't add a project, yet. %s", "Friendly community is really important to us. Please add a profile image in order to add a project.<br />To add one, go on your account page.");
        }

        if ($this->getUser()->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) == false) {
            return 'You can not add a project because an admin explicitly disabled it for your account.<br />If you think it is a mistake, contact us on Discord.';
        }

        $max_concurrent = $this->adventCalendarClaimedDayRepository->getProjectsAddForUser($this->getUser()); // before $this->config['project']['max_concurrent_project_per_user']
        $current = $this->getUser()->getNumberOfConcurrentProject();
        if ($current >= $max_concurrent) {
            return sprintf('You can not add a project because you have exceeded your limit of concurrent projects. <br>The limit have been set to prevent overloading the render farm with projects from a single person.<br>Once one of your projects has been rendered, you will be able to add another.<br>Your current limit is %d projects.', $max_concurrent);
        }

        if (is_int($frame_start) && is_int($frame_end) && ($frame_start < 0 || $frame_end < 0)) {
            return 'Frame start and end need to be a positive number';
        }

        if (is_string($binary)) {
            if (is_null($this->blenderRepository->findOneBy(['bf_code' => $binary]))) {
                $error = '<span style="color:red">The Blender version used by the blend is not supported by SheepIt. <br>';
                $error .= 'We support:</span><ul>';
                $blenders = $this->blenderRepository->findAll();
                usort($blenders, function (Blender $o1, Blender $o2): int {
                    return $o1->getHuman() <=> $o2->getHuman();
                });
                foreach ($blenders as $blender) {
                    $error .= '<li><span style="color:red">'.$blender->getHuman().'</span></li>';
                }
                $error .= '</ul>';
                return $error;
            }
        }

        return null;
    }
}
