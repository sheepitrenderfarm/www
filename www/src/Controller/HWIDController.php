<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\HWIDBlacklist;
use App\Repository\EventRepository;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\SessionRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * HWID controller
 */
#[Route(path: '/hwid')]
class HWIDController extends BaseController {
    private SessionRepository $sessionRepository;
    private EventRepository $eventRepository;
    private HWIDBlacklistRepository $HWIDBlacklistRepository;

    public function __construct(
        Main $main,
        UrlGeneratorInterface $router,
        HTML $html,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        SessionRepository $sessionRepository,
        EventRepository $eventRepository,
        HWIDBlacklistRepository $HWIDBlacklistRepository,
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->sessionRepository = $sessionRepository;
        $this->eventRepository = $eventRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;
    }

    /**
     * Add HWID to blacklist
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/blacklist/add/{user}/{hwid}/{reason}')]
    public function add(string $user, string $hwid, int $reason): Response {
        if ($hwid != '') {
            $this->HWIDBlacklistRepository->add($hwid, $user, $reason);

            $this->main->getDiscordWebHook()->userMessage($this->getUser()->getId(), $user, '', 'Add HWID blacklist for \''.$user.'\' reason: '.HWIDBlacklist::REASONS[$reason]);

            return new Response('OK');
        }
        else {
            return new Response('Missing parameter');
        }
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/blacklist/{blacklist}/remove')]
    public function remove(HWIDBlacklist $blacklist): Response {
        $this->HWIDBlacklistRepository->remove($blacklist);

        return new Response('OK');
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/search/{search}')]
    public function search(string $search): Response {

        $ret = '<div class="container"><h2>Results</h2>';

        $ret .= '<h3>From past sessions</h3>';
        $ret .= '<div class="container">';
        $events = $this->eventRepository->findHWID($search);
        if (count($events) == 0) {
            $ret .= 'No session found';
        }
        else {
            $ret .= '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
            $ret .= '<thead>';
            $ret .= '<tr>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">Session</th>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">User</th>';
            $ret .= '</tr>';
            $ret .= '</thead>';
            $ret .= '<tbody>';
            foreach ($events as $event) {
                $ret .= '<tr style="text-align: center; vertical-align: center;" >';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= '<a href="'.$this->generateUrl("app_session_single", ['sessionid' => $event->getSession()]).'">';
                $ret .= $event->getSession();
                $ret .= '</a>';
                $ret .= '</td>';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= $this->html->retUserProfileLink($event->getUser());
                $ret .= '</td>';

                $ret .= '</tr>';
            }

            $ret .= '</tbody>';
            $ret .= '</table>';
        }


        $ret .= '</div>';

        $ret .= '<h3>From active sessions</h3>';
        $ret .= '<div class="container">';
        $sessions = $this->sessionRepository->findBy(['hwid' => $search]);
        if (count($sessions) == 0) {
            $ret .= 'No session found';
        }
        else {
            $ret .= '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
            $ret .= '<thead>';
            $ret .= '<tr>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">Session</th>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">Country</th>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">Model</th>';
            $ret .= '<th style="text-align: center; vertical-align: middle;">User</th>';
            $ret .= '</tr>';
            $ret .= '</thead>';
            $ret .= '<tbody>';
            foreach ($sessions as $session) {
                $ret .= '<tr style="text-align: center; vertical-align: center;" >';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= '<a href="'.$this->generateUrl("app_session_single", ['sessionid' => $session->getId()]).'">';
                $ret .= $session->getId();
                $ret .= '</a>';
                $ret .= '</td>';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= '<img data-src="/media/image/flag/'.$session->getCountry().'.png" />';
                $ret .= '</td>';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= $session->getComputeDeviceStrHuman();
                $ret .= '</td>';

                $ret .= '<td style="text-align: center; vertical-align: middle;" >';
                $ret .= $this->html->retUserProfileLink($session->getUser()->getId());
                $ret .= '</td>';

                $ret .= '</tr>';
            }

            $ret .= '</tbody>';
            $ret .= '</table>';
        }
        $ret .= '</div>';


        $ret .= '</div>';

        return new Response($ret);
    }
}