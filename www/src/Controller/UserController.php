<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Event;
use App\Entity\Gift;
use App\Entity\Liaison;
use App\Entity\Message;
use App\Entity\PreUser;
use App\Entity\TaskUserRemove;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Repository\GiftRepository;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\LiaisonRepository;
use App\Repository\MessageRepository;
use App\Repository\PatreonRepository;
use App\Repository\PreUserRepository;
use App\Repository\SessionRepository;
use App\Repository\UserPublicKeyRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\Daemon;
use App\Service\Image;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Mail;
use App\Utils\Misc;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;

/**
 * User controller
 */
#[Route(path: '/user')]
class UserController extends BaseController {
    private Daemon $daemon;
    private LiaisonRepository $liaisonRepository;
    private UserRepository $userRepository;
    private PreUserRepository $preUserRepository;
    private UserPublicKeyRepository $userPublicKeyRepository;
    private GiftRepository $giftRepository;
    private SessionRepository $sessionRepository;
    private MessageRepository $messageRepository;
    private PatreonRepository $patreonRepository;
    private HWIDBlacklistRepository $HWIDBlacklistRepository;

    public function __construct(
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        PreUserRepository $preUserRepository,
        LiaisonRepository $liaisonRepository,
        UserPublicKeyRepository $userPublicKeyRepository,
        GiftRepository $giftRepository,
        SessionRepository $sessionRepository,
        MessageRepository $messageRepository,
        PatreonRepository $patreonRepository,
        HWIDBlacklistRepository $HWIDBlacklistRepository,
        ConfigService $configService,
        HTML $html,
        Daemon $daemon
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->daemon = $daemon;
        $this->patreonRepository = $patreonRepository;
        $this->userRepository = $userRepository;
        $this->userPublicKeyRepository = $userPublicKeyRepository;
        $this->preUserRepository = $preUserRepository;
        $this->liaisonRepository = $liaisonRepository;
        $this->giftRepository = $giftRepository;
        $this->sessionRepository = $sessionRepository;
        $this->messageRepository = $messageRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;

        $this->config = $configService->getData();
    }

    /**
     * Display username
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/whoiam')]
    public function whoiam(): Response {
        return new Response($this->getUser()->getUserIdentifier());
    }

    #[Route(path: '/authenticate')]
    public function authenticate(Request $request): Response {
        if ($request->request->has('login') && $request->request->has('password')) {
            $user = $this->userRepository->find($request->request->get('login'));
            if (is_object($user) && $user->authenticate($request->request->get('password'))) {
                return new Response('OK');
            }
        }
        return new Response('NOK');
    }

    #[Route(path: '/signin/{redirect}', defaults: ['redirect' => '/'])]
    public function signin(string $redirect): Response {
        $redirect = urldecode($redirect);

        // only redirect to a known route
        /** @var RouterInterface $router */
        $router = $this->container->get('router');
        $routes = $router->getRouteCollection();

        $context = new RequestContext('/');
        $matcher = new UrlMatcher($routes, $context);
        try {
            $matcher->match($redirect);

            // no exception, the route exists, we can use it
            $redirect = urldecode($redirect);
        }
        catch (ResourceNotFoundException $e) {
            $redirect = $this->generateUrl('app_home_index');
        }

        if (is_object($this->getUser())) {
            // user logged in, simply redirect him to destination
            return new RedirectResponse($redirect);
        }

        return $this->render('user/signin.html.twig', [
            'redirect' => $redirect,
        ]);
    }

    #[Route(path: '/logout')]
    public function logout(Request $request): RedirectResponse {
        if ($request->getSession()->has('session')) {
            $session = $this->sessionRepository->find($request->getSession()->get('session'));
            if (is_object($session)) {
                $this->sessionRepository->remove($session, Event::TYPE_LOGOFF);
            }
        }

        return new RedirectResponse($this->generateUrl('app_user_logout_internal'));
    }

    /**
     * This route will never be actually called, it's the logout route from security.yaml
     */
    #[Route(path: '/logout_internal')]
    public function logout_internal(): Response {
        return new Response('');
    }

    /**
     * Reset password and email the user
     */
    #[Route(path: '/reset/password')]
    public function reset_password(Request $request): Response {
        if ($request->request->has('email') && filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $user = $this->userRepository->importFromEmail($request->request->get('email'));
            if (is_object($user)) {
                $new_password = $this->userRepository->resetPassword($user);
                $content = sprintf('Hello,<br>Your password has been reset.<br>To sign in, you have to use:<br>Login: %s<br>Password: %s', $user->getId(), $new_password);
                Mail::sendamail($user->getEmail(), 'Your password has been reset', $content, 'reset_password');
                return new Response('OK');
            }
            else {
                $preuser = $this->preUserRepository->importFromEmail($request->request->get('email'));
                if (is_object($preuser)) {
                    $preuser->sendRegistrationConfirmationEmail();
                    return new Response('OK');
                }
                return new Response('If you have reset on this address, you will get an link to reset your password.');
            }
        }
        else {
            return new Response('wrong-request');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/password')]
    public function update_password(Request $request): Response {
        if ($request->request->has('old') && $request->request->has('new') && $request->request->has('verif')) {
            $old = $request->request->get('old');
            $new = $request->request->get('new');
            $verif = $request->request->get('verif');

            if ($new != $verif) {
                return new Response('Passwords does not match');
            }
            else {
                $user = $this->getUser();
                if ($user->authenticate($old) == false) {
                    return new Response('Authentication failed');
                }
                else {
                    $this->getUser()->setPassword(User::generateHashedPassword($this->getUser()->getId(), $new));

                    $this->entityManager->flush($this->getUser());
                    return new Response('Password successfully changed');
                }
            }
        }
        else {
            return new Response('wrong-request');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/email')]
    public function update_email(Request $request): Response {
        if ($request->request->has('email') && filter_var($request->request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $key = Misc::code(32);
            $l = new Liaison();
            $l->setType(Liaison::USER_CHANGE_EMAIL_KEY);
            $l->setPrimary($this->getUser()->getId());
            $l->setSecondary($key);
            $l->setValue($request->request->get('email'));
            $this->entityManager->persist($l);
            $this->entityManager->flush($l);

            $link = $this->config['site']['url'].$this->generateUrl('app_user_confirm_email', ['preuser' => $this->getUser()->getId(), 'key' => $key]);
            $content = sprintf('To validate your new email address you need to use this link: <br><a href="%s">%s</a><br>', $link, $link);
            Mail::sendamail($request->request->get('email'), 'New email confirmation', $content, 'change_email');

            return new Response('An email have been sent to your new email address to validate it.');
        }
        else {
            return new Response('Bad value');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/website')]
    public function update_website(Request $request): Response {
        if ($request->request->has('website')) {
            $this->getUser()->setWebsite(htmlspecialchars($request->request->get('website'), FILTER_SANITIZE_URL));
            $this->entityManager->flush($this->getUser());

            return new Response('OK');
        }
        else {
            return new Response('Bad value');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/socialnetwork')]
    public function update_socialnetwork(Request $request): Response {
        if ($request->request->has('facebook') && $request->request->has('twitter') && $request->request->has('youtube') && $request->request->has('instagram')) {
            $bad_value = false;
            if ($request->request->get('facebook') != '') {
                if (str_contains($request->request->get('facebook'), 'facebook.com')) {
                    $this->getUser()->setSocialnetworkFacebook(htmlspecialchars(filter_var($request->request->get('facebook'), FILTER_SANITIZE_URL)));
                }
                else {
                    $bad_value = true;
                }
            }
            if ($request->request->get('twitter') != '') {
                if (str_contains($request->request->get('twitter'), 'twitter.com') || str_contains($request->request->get('twitter'), 'x.com')) {
                    $this->getUser()->setSocialnetworkTwitter(htmlspecialchars(filter_var($request->request->get('twitter'), FILTER_SANITIZE_URL)));
                }
                else {
                    $bad_value = true;
                }
            }

            if ($request->request->get('instagram') != '') {
                if (str_contains($request->request->get('instagram'), 'instagram.com')) {
                    $this->getUser()->setSocialnetworkInstagram(htmlspecialchars(filter_var($request->request->get('instagram'), FILTER_SANITIZE_URL)));
                }
                else {
                    $bad_value = true;
                }
            }

            if ($request->request->get('youtube') != '') {
                if (str_contains($request->request->get('youtube'), 'youtube.com')) {
                    $this->getUser()->setSocialnetworkYoutube(htmlspecialchars(filter_var($request->request->get('youtube'), FILTER_SANITIZE_URL)));
                }
                else {
                    $bad_value = true;
                }
            }

            if ($bad_value == false) {
                $this->entityManager->flush($this->getUser());

                return new Response('OK');
            }
            else {
                return new Response('Bad value '.$bad_value);
            }
        }
        else {
            return new Response('Bad value');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/notification/{key}/{value}')]
    public function update_notification(int $key, int $value): Response {
        $this->getUser()->setNotificationFromMask($key, $value);
        $this->entityManager->flush($this->getUser());

        return new Response('Notification level changed');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/scheduler/{key}/{value}')]
    public function update_scheduler(string $key, int $value): Response {
        if ($key == 'render_my_project_first') {
            $this->getUser()->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST, $value);
            $this->entityManager->flush($this->getUser());
            return new Response('Settings changed');
        }

        if ($key == 'render_my_team_projects_first') {
            $this->getUser()->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST, $value);
            $this->entityManager->flush($this->getUser());
            return new Response('Settings changed');
        }

        if ($key == 'render_heavy_project') {
            $this->getUser()->setSchedulerFromMask(User::SCHEDULER_MASK_HEAVY_PROJECT, $value);
            $this->entityManager->flush($this->getUser());
            return new Response('Settings changed');
        }

        return new Response('NOK');
    }

    /**
     * Invert render right of a user.
     * If he could render, it can't anymore (and vice-versa)
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/update/render_right/{user}')]
    public function update_render_right(User $user): Response {
        $content = $user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING) ? 'Hello, <br>An admin on SheepIt-renderfarm disabled your permission to render any project on the renderfarm.<br >It\'s usually due to broken machine, if you want to talk to admin directly, please contact us on Discord.' : 'Hello, <br>An admin on SheepIt-renderfarm enable your permission to render project on the renderfarm.';
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, $user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING) ? 0 : 1);

        // blacklist his hardware too
        if ($user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING) == 0) {
            foreach ($user->sessions() as $session) {
                if ($session->gethwid() != '') {
                    $this->HWIDBlacklistRepository->add($session->gethwid(), $session->getUser()->getId(), 2);
                }
            }
        }
        else {
            foreach ($this->HWIDBlacklistRepository->findBy(['login' => $user->getId()]) as $blacklist) {
                $this->HWIDBlacklistRepository->remove($blacklist);
            }
        }

        $message = new Message();
        $message->setSender($this->getUser()->getId());
        $message->setReceiver($user->getId());
        $message->setContent($user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING) ? Message::MESSAGE_RENDERING_ALLOW : Message::MESSAGE_RENDERING_FORBID);
        $message->setTime(time());
        $this->entityManager->persist($message);
        $this->entityManager->flush($message);

        $this->entityManager->flush($user);
        Mail::sendamail($user->getEmail(), 'Permission to render projects on project on SheepIt-renderfarm.com', $content, 'render_right');

        $this->main->getDiscordWebHook()->userRightToRender($user->getId(), $user->levelIsEnabled(User::ACL_MASK_CAN_DO_RENDERING), $this->getUser()->getId());
        return new Response('OK');
    }

    /**
     * Invert project creation right of a user.
     * If he could add a project, it can't anymore (and vice-versa)
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/update/project_right/{user}')]
    public function update_project_right(User $user): Response {
        $content = $user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) ? 'Hello, <br>An admin on SheepItRenderfarm disabled your permission to add any project on the renderfarm.<br >It\'s usually due to abuse from you, if you think it\'s not justified please contact us on Discord.' : 'Hello, <br>An admin on SheepItRenderfarm enable your permission to add a project on the renderfarm.';
        $user->setLevelFromMask(User::ACL_MASK_CAN_DO_ADD_PROJECT, $user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) ? 0 : 1);

        $message = new Message();
        $message->setSender($this->getUser()->getId());
        $message->setReceiver($user->getId());
        $message->setContent($user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT) ? Message::MESSAGE_PROJECT_ALLOW : Message::MESSAGE_PROJECT_FORBID);
        $message->setTime(time());
        $this->entityManager->persist($message);
        $this->entityManager->flush($message);

        $this->entityManager->flush($user);
        Mail::sendamail($user->getEmail(), 'Permission to add a project on Sheepit-renderfarm.com', $content, 'add_project_right');
        $this->main->getDiscordWebHook()->userRightAddProject($user->getId(), $user->levelIsEnabled(User::ACL_MASK_CAN_DO_ADD_PROJECT), $this->getUser()->getId());
        return new Response('OK');
    }

    /**
     * Send a message to a user
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/message/{user}')]
    public function send_message(User $user, Request $request): Response {
        if ($request->request->has('comment')) {
            $discordWebHook = $this->main->getDiscordWebHook();

            if ($user->getId() == $this->getUser()->getId()) {
                // answer of a user to an admin

                $message = new Message();
                $message->setSender($this->getUser()->getId());
                $message->setReceiver('admin');
                $message->setContent(strip_tags($request->request->get('comment')));
                $message->setTime(time());
                $this->entityManager->persist($message);
                $this->entityManager->flush($message);

                $discordWebHook->userMessage($this->getUser()->getId(), 'sheepmin', $this->router->generate('app_user_messages', ['user' => $this->getUser()->getId()], UrlGeneratorInterface::ABSOLUTE_URL), strip_tags($request->request->get('comment')));
            }
            elseif ($this->getUser()->isModerator()) {
                // message from admin to user

                $message = new Message();
                $message->setSender($this->getUser()->getId());
                $message->setReceiver($user->getId());
                $message->setContent(strip_tags($request->request->get('comment')));
                $message->setTime(time());
                $this->entityManager->persist($message);
                $this->entityManager->flush($message);

                $footer = '<br><br>Answering this email directly will NOT work.<br><br><a href="'.$this->config['site']['url'].$this->generateUrl('app_user_messages', ['user' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_PATH).'">You can answer this message by going on this link</a>';

                $content = 'Hello,<br> I\'m '.$this->getUser()->getId().' an admin on SheepIt-renderfarm.<br><br>'.str_replace("\n", '<br>', $request->request->get('comment').$footer);
                Mail::sendamail($user->getEmail(), 'Message from the admin', $content, 'send_message');

                $discordWebHook->userMessage($this->getUser()->getId(), $user->getId(), $this->router->generate('app_user_messages', ['user' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_URL), strip_tags($request->request->get('comment')));
            }
            else {
                // not allowed, a regular user is trying to message another regular user
            }

            return new Response('OK');
        }
        else {
            return new Response('Bad value');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/profile')]
    public function profile_generic(): RedirectResponse {
        return new RedirectResponse($this->generateUrl("app_user_profile", ["user" => $this->getUser()->getId()]));
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{user}/profile')]
    public function profile(User $user): Response {
        $type = 'public'; // 'admin' 'private'
        if ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'private';
        }
        elseif ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator() == true) {
            $type = 'private';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator() == true) {
            $type = 'admin';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'public';
        }
        else {
            $type = 'public';
        }

        $rank = $user->getRanking();

        return $this->render('user/profile.html.twig', [
                'type' => $type,
                'user' => $user,
                'rank' => $rank,
            ]
        );
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{user}/edit')]
    public function edit(User $user): Response {
        $type = 'public'; // 'admin' 'private'
        if ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'private';
        }
        elseif ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator() == true) {
            $type = 'private';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator() == true) {
            $type = 'admin';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'public';
        }
        else {
            $type = 'public';
        }

        if ($type == 'public') {
            //should not display anything...
            return new Response('');
        }
        $rank = $user->getRanking();

        return $this->render('user/edit.html.twig', [
            'rank' => $rank,
            'user' => $user,
            'type' => $type,
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{user}/edit/settings_section/{show_category}')]
    public function print_user_settings_section(User $user, string $show_category = ''): Response {
        $type = 'public'; // 'admin' 'private'
        if ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'private';
        }
        elseif ($user->getId() == $this->getUser()->getId() && $this->getUser()->isModerator()) {
            $type = 'private';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator()) {
            $type = 'admin';
        }
        elseif ($user->getId() != $this->getUser()->getId() && $this->getUser()->isModerator() == false) {
            $type = 'public';
        }
        else {
            $type = 'public';
        }

        if ($type == 'public') {
            //should not display anything...
            return new Response('');
        }

        return $this->render('user/edit_settings.html.twig', [
                'type' => $type,
                'user' => $user,
                'show_category' => $show_category,
            ]
        );
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{user}/message')]
    public function messages(User $user): Response {
        if ($this->getUser()->isModerator() || $user->getId() == $this->getUser()->getId()) {
            return $this->render('user/message.html.twig', [
                    'user' => $user,
                    'messages' => $this->messageRepository->forUser($user),
                ]
            );
        }
        else {
            return $this->render('error.html.twig', [
                'error_title' => 'Forbidden',
                'message' => 'You are not allowed to see this page.',
            ]);
        }
    }

    #[Route(path: '/lostpassword')]
    public function lostpassword(): Response {
        return $this->render('user/lost_password.html.twig', []);
    }

    #[Route(path: '/register')]
    public function print_register(Request $request): Response {
        $affiliate_user = null;
        $affiliate_program = null;
        if ($request->getSession()->has('affiliate')) {
            if (array_key_exists($request->getSession()->get('affiliate'), $this->config['affiliate'])) {
                $affiliate_program = $this->config['affiliate'][$request->getSession()->get('affiliate')];
            }
            else {
                $affiliate_user = $this->userRepository->find($request->getSession()->get('affiliate'));
            }
        }

        return $this->render('user/register.html.twig', [
                'header_default_container' => true,
                'affiliate_program' => $affiliate_program,
                'affiliate_user' => $affiliate_user,
                'affiliate_user_minimum_frame' => $this->config['affiliate_minimum_frame'],
                'affiliate_user_reward' => $this->config['affiliate_reward'],
            ]
        );
    }

    #[Route(path: '/do_register')]
    public function register(Request $request): Response {
        if ($request->request->has('login') && $request->request->has('password') && $request->request->has('password_confirm') && $request->request->has('email')) {
            if ($this->preUserRepository->checkUserName($request->request->get('login')) == false || $this->preUserRepository->checkEmail($request->request->get('email')) == false) {
                return new Response('Incorrect values');
            }

            $ok = true;
            $user = $this->userRepository->find($request->request->get('login'));
            if (is_object($user)) {
                $ok = false;
            }

            $preuser = $this->preUserRepository->find($request->request->get('login'));
            if (is_object($preuser)) {
                $ok = false;
            }

            if (is_object($this->preUserRepository->importFromEmail($request->request->get('email'))) || is_object($this->userRepository->importFromEmail($request->request->get('email')))) {
                return new Response('Sorry, it looks like the email used belongs to an existing account. If you forgot your password, you can reset at this <a href="'.$this->generateUrl('app_user_lostpassword').'">address</a>');
            }

            if (preg_match('`^([_a-zA-Z0-9-]+)$`', $request->request->get('login')) && $ok === true && $request->request->get('password') !== '' && $request->request->get('password_confirm') == $request->request->get('password') && preg_match('`^([_a-zA-Z0-9-]+)(\.([_a-zA-Z0-9-]+))*@([a-zA-Z0-9-]+)(\.([a-zA-Z0-9-]+))+$`', $request->request->get('email'))) {
                if (Mail::isSpamDomain($request->request->get('email'))) {
                    return new Response("Moderators and admins need to be able to contact you if needed, that's why no 'spam email' is allowed.");
                }
                else {
                    $key = Misc::code(32);

                    $preuser = new PreUser();
                    $preuser->setId($request->request->get('login'));
                    $preuser->setPassword(User::generateHashedPassword($preuser->getId(), $request->request->get('password')));
                    $preuser->setRegistrationTime(time());
                    $preuser->setEmail($request->request->get('email'));
                    $preuser->setKey($key);
                    $preuser->setAffiliate($request->getSession()->has('affiliate') ? $request->getSession()->get('affiliate') : '');


                    $this->entityManager->persist($preuser);
                    $this->entityManager->flush($preuser);

                    if ($preuser->sendRegistrationConfirmationEmail() == false) {
                        return new Response('Failed to send email to recipient, cancelling registration.');
                    }
                    else {
                        return new Response('OK');
                    }
                }
            }
        }

        return new Response('Bad value');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/confirm/email/{key}', methods: ['GET'])]
    public function confirm_email(string $key): Response {
        $liaisons = $this->liaisonRepository->getLiaisons(Liaison::USER_CHANGE_EMAIL_KEY, $this->getUser()->getId(), $key);
        if (count($liaisons) == 1) {
            $l = array_pop($liaisons);
            $this->getUser()->setEmail($l->getValue());
            $this->liaisonRepository->remove($l);
            $this->entityManager->flush();

            return $this->render('message.html.twig', [
                'title_info' => 'Success',
                'message' => '<h2>Email confirmation</h2><p class="ok">Email changed !</p>',
            ]);
        }
        else {
            return $this->render('error.html.twig', [
                'error_title' => 'An error occured !',
                'message' => 'Please contact the site manager. Reference: Error #4601.',
            ]);
        }
    }

    #[Route(path: '/confirm/user/{preuser}/{key}', methods: ['GET'])]
    public function confirm_user(PreUser $preuser, string $key): Response {
        $user = $this->userRepository->find($preuser);
        if (is_object($user) == false) {
            if ($key == $preuser->getKey()) {
                $user = new User();
                $user->setId($preuser->getId());
                $user->setPassword($preuser->getPassword());
                $user->setRegistrationTime($preuser->getRegistrationTime());
                $user->setEmail($preuser->getEmail());
                $user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1); // the user can do render but only his or public ones
                $user->setLevelFromMask(User::ACL_MASK_CAN_DO_ADD_PROJECT, 1);
                $user->setAffiliate($preuser->getAffiliate());
                $user->setSchedulerFromMask(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST, 1);
                $user->setSponsorReceive(true);
                $user->setSponsorGive(false);
                $user->setNotificationToken(Misc::code(32));

                $user->setNotificationFromMask(User::NOTIFICATION_MASK_ON_PROJECT_FINISHED, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_NEWSLETTER, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_AWARD, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM_ACCEPTED, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_AFFILIATE_REFERRED, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_AFFILIATE_REFERER, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_PATREON, 1);
                $user->setNotificationFromMask(User::NOTIFICATION_MASK_SESSION_BLOCKED, 1);

                $this->entityManager->persist($user);
                $this->entityManager->flush($user);

                $this->preUserRepository->remove($preuser);

                // affiliation
                if ($user->getAffiliate() != '' && array_key_exists($user->getAffiliate(), $this->config['affiliate'])) {
                    $gift = new Gift();
                    $gift->setValue($this->config['affiliate'][$user->getAffiliate()]['gift']['value']);
                    $gift->setOwner($user);
                    $gift->setComment($this->config['affiliate'][$user->getAffiliate()]['gift']['text']);
                    $gift = $this->giftRepository->generateAccess($gift);
                    $this->entityManager->persist($gift);
                    $this->entityManager->flush($gift);
                    $this->giftRepository->sendEmail($gift, $this->config['notification']['gift']);
                }

                $error = 0;
            }
            else {
                $error = 2;
            }
        }
        else {
            $error = 4;
        }

        if ($error == 0) {
            return $this->render('user/registration_confirm.html.twig', [
                'header_default_container' => true,
            ]);
        }
        else {
            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'Error while progressing your registration, did you use the link of the e-mail?',
            ]);
        }
    }

    #[Route(path: '/unsubscribe/{token}')]
    public function unsubscribe(string $token): Response {
        $user = $this->userRepository->findOneBy(['notification_token' => $token]);
        if (is_object($user) == false) {
            return $this->render('error.html.twig', [
                'error_title' => 'An error occured !',
                'message' => 'User not found.',
            ]);
        }
        else {
            $notifications = array(
                User::NOTIFICATION_MASK_ONFIRSTFRAMEFINISHED => 'The first frame of my project is rendered',
                User::NOTIFICATION_MASK_NEWSLETTER => 'News or an important event is posted',
                User::NOTIFICATION_MASK_AWARD => 'I get an award',
                User::NOTIFICATION_MASK_AFFILIATE_REFERRED => sprintf('A referral has rendered %s public frames', number_format($this->config['affiliate_minimum_frame'])),
                User::NOTIFICATION_MASK_AFFILIATE_REFERER => 'I get the referrer reward',
                User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM => 'Someone requests to join the team I own',
                User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM_ACCEPTED => 'I am accepted into a team',
                User::NOTIFICATION_MASK_PATREON => 'I get my Patreon reward',
                User::NOTIFICATION_MASK_SESSION_BLOCKED => 'My render session has been blocked',
            );

            return $this->render('user/unsubscribe.html.twig', [
                'header_default_container' => true,
                'notifications' => $notifications,
                'user' => $user,
            ]);
        }
    }

    #[Route(path: '/unsubscribe/{token}/action/{key}/{value}')]
    public function unsubscribe_action(string $token, int $key, int $value): Response {
        $user = $this->userRepository->findOneBy(['notification_token' => $token]);
        if (is_object($user)) {
            $user->setNotificationFromMask($key, $value);
            $this->entityManager->flush($user);

            return new Response('Notification level changed');
        }
        else {
            return new Response('Failed');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/remove')]
    public function print_remove(): Response {
        return $this->render('user/remove.html.twig', [
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/remove_actual')]
    public function remove_actual(Request $request): Response {
        if ($request->request->has('comment')) {

            $u = $this->userRepository->find(User::REMOVED_USER_ID);
            if (is_object($u)) {
                $u->setOrderedFrames($this->getUser()->getOrderedFrames() + $u->getOrderedFrames());
                $u->setOrderedFramesTime($this->getUser()->getOrderedFramesTime() + $u->getOrderedFramesTime());
                $u->setNbProjects($this->getUser()->getNbProjects() + $u->getNbProjects());
                $u->setRenderTime($this->getUser()->getRenderTime() + $u->getRenderTime());
                $u->setRenderedFrames($this->getUser()->getRenderedFrames() + $u->getRenderedFrames());
                $u->setPointsEarn($this->getUser()->getPointsEarn() + $u->getPointsEarn());
                $this->entityManager->flush($u);
            }

            $contents = 'User '.$this->getUser()->getId().' ('.$this->getUser()->getEmail().') deleted its account<br>';
            $contents .= '<ul>';
            $contents .= '<li>Registered on : '.$this->html->getTimeWithLocalTimeZone('F jS, Y', $this->getUser()->getRegistrationTime()).'</li>';
            $contents .= '<li>Points: '.number_format($this->getUser()->getPoints()).'</li>';
            $contents .= '<li>Render time: '.Misc::humanTime($this->getUser()->getRenderTime()).'</li>';
            $contents .= '<li>Ordered time: '.Misc::humanTime($this->getUser()->getOrderedFramesTime()).'</li>';
            $contents .= '<li>Ordered frame: '.$this->getUser()->getOrderedFrames().'</li>';
            $contents .= '<li>Rendered frame: '.$this->getUser()->getRenderedFrames().'</li>';
            $contents .= '<li>Ordered project: '.$this->getUser()->getNbProjects().'</li>';
            $contents .= '</ul>';

            $contents .= 'Reason: </br>';
            $contents .= '<p>'.$request->request->get('comment').'</p>';
            Mail::sendamail($this->config['admin']['email'], 'User '.$this->getUser()->getId().' deleted its account', $contents, 'admin');

            $task = new TaskUserRemove();
            $task->setData($this->getUser()->getId());
            $this->daemon->addTask($task);

            // set a random password so the user can't log in anymore
            $this->getUser()->setPassword(User::generateHashedPassword($this->getUser()->getId(), Misc::code(16)));
            $this->entityManager->flush($this->getUser());

            session_destroy();

            return new Response('OK');
        }
        return new Response('FAILED');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/update/avatar')]
    public function update_avatar(Request $request): Response {
        if ($request->files->has('new_avatar')) {
            try {
                /** @var ?UploadedFile $uploadedFile */
                $uploadedFile = $request->files->get('new_avatar');
                if (is_object($uploadedFile) && $uploadedFile->isValid()) {
                    $anImage = new Image($uploadedFile->getPathname());
                    if ($anImage->isImage()) {
                        $targetBig = $this->config['storage']['path'].'users/avatars/big/'.$this->getUser()->getId().'.png';
                        $ret = $anImage->generateThumbnail($this->config['avatar']['big']['width'], $this->config['avatar']['big']['height'], $targetBig);
                        if ($ret != true) {
                            unlink($uploadedFile->getRealPath());

                            return $this->render('error.html.twig', [
                                'error_title' => 'Error',
                                'message' => 'Failed to generate avatar (big-#1902a)',
                            ]);
                        }
                        $targetSmall = $this->config['storage']['path'].'users/avatars/small/'.$this->getUser()->getId().'.png';
                        $ret = $anImage->generateThumbnail($this->config['avatar']['small']['width'], $this->config['avatar']['small']['height'], $targetSmall);
                        if ($ret != true) {
                            unlink($uploadedFile->getRealPath());

                            return $this->render('error.html.twig', [
                                'error_title' => 'Error',
                                'message' => 'Failed to generate avatar (small-#1902b)',
                            ]);
                        }
                    }
                    unlink($uploadedFile->getRealPath());
                }
            }
            catch (FileNotFoundException $e) {
                return $this->render('error.html.twig', [
                    'error_title' => 'Error',
                    'message' => 'Failed to get avatar (#1902c)',
                ]);
            }
        }
        return $this->redirectToRoute('app_user_profile_generic');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/renderkey/add/{comment}')]
    public function renderkey_add(string $comment): Response {
        $this->userPublicKeyRepository->add($this->getUser(), strip_tags($comment));
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/renderkey/del/{renderkey}')]
    public function renderkey_del(UserPublicKey $renderkey): Response {
        foreach ($this->getUser()->getPublicKeys() as $key) {
            if ($key->getId() == $renderkey->getId()) {
                // remove any active session first
                foreach ($this->getUser()->sessions() as $session) {
                    if (is_object($session->getRenderKey()) && $session->getRenderKey() == $renderkey) {
                        $this->sessionRepository->remove($session);
                    }
                }
                $this->userPublicKeyRepository->remove($key);
                return new Response('OK');
            }
        }

        return new Response('renderkeydel-failed');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/priority/add/{user}')]
    public function priority_add(User $user): Response {
        $this->getUser()->addHighPriorityUser($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/priority/remove/{user}')]
    public function priority_remove(User $user): Response {
        $this->getUser()->removeHighPriorityUser($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/block/owner/add/{user}')]
    public function block_add_owner(User $user): Response {
        $this->getUser()->addBlockedOwner($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/block/owner/remove/{user}')]
    public function block_remove_owner(User $user): Response {
        $this->getUser()->removeBlockedOwner($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/block/renderer/add/{user}')]
    public function block_add_renderer(User $user): Response {
        $this->getUser()->addBlockedRenderer($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/block/renderer/remove/{user}')]
    public function block_remove_renderer(User $user): Response {
        $this->getUser()->removeBlockedRenderer($user->getId());
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/sponsor/add/{user}')]
    public function sponsor_add(User $user): Response {
        $this->getUser()->addSponsorUser($user);
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/sponsor/remove/{user}')]
    public function sponsor_remove(User $user): Response {
        $this->getUser()->removeSponsorUser($user);
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/sponsor/give/enable/{enable}')]
    public function sponsor_give_enable(bool $enable): Response {
        $this->getUser()->setSponsorGive($enable);
        $this->entityManager->flush();
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/sponsor/receive/enable/{enable}')]
    public function sponsor_receive_enable(bool $enable): Response {
        $this->getUser()->setSponsorReceive($enable);
        $this->entityManager->flush();
        return new Response('OK');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/patreon/{email}')]
    public function patreon(string $email): Response {
        $patreon = $this->patreonRepository->find($email);
        if (is_object($patreon)) {
            $date = new DateTime();

            $reward_already_given = is_string($patreon->getLastCharge()) && $patreon->getLastCharge() != NULL;
            $patreon->setUser($this->getUser());
            $patreon->setLastCharge(date_format($date, 'Y-m'));
            $this->entityManager->flush();
            if ($reward_already_given == false) {
                $this->patreonRepository->giveReward($patreon);
            }

            return $this->render('message.html.twig', [
                'header_default_container' => true,
                'title_info' => 'Successfully linked',
                'message' => 'Your Patreon account is now link to your SheepIt account.<br>You have received the reward for this month',
            ]);

        }
        else {
            return new Response('', 404);
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/list_from_term')]
    public function list_from_term(Request $request): JsonResponse {
        $list = array();
        if ($request->query->has('term')) {
            $term = trim($request->query->get('term'));

            $a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));

            // replace multiple spaces with one
            $term = preg_replace('/\s+/', ' ', $term);

            // allow space, any unicode letter and digit, underscore and dash
            if (preg_match("/[^\040\pL\pN_-]/u", $term)) {
                return new JsonResponse($a_json_invalid);
            }

            $users = $this->userRepository->findFromPattern($term);
            foreach ($users as $u) {
                if ($u->getId() != $this->getUser()->getUserIdentifier()) { // do not add myself to the blacklist, priority etc. lists
                    $list [] = $u->getId();
                }
            }
        }

        natcasesort($list);

        return new JsonResponse($list);
    }
}
