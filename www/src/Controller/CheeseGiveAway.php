<?php

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\AwardCheeseGiveway;
use App\Entity\User;
use App\Repository\RenderDayRepository;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Controller to handle Discord giveaway
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/cheese')]
class CheeseGiveAway extends BaseController {
    private RenderDayRepository $renderDayRepository;

    public function __construct(UrlGeneratorInterface $router, HTML $html, Main $main, RenderDayRepository $renderDayRepository, ConfigService $configService, EntityManagerInterface $entityManager) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->renderDayRepository = $renderDayRepository;
    }


    #[Route(path: '/')]
    public function main(): Response {
        return $this->render(
            'cheese/main.html.twig',
            [
                'title' => 'Cheese is life',
                'header_default_container' => true,
            ]
        );
    }


    #[Route(path: '/cheese')]
    public function cheese(): Response {
        $awardGiveaway = new AwardCheeseGiveway();
        $have_award = $this->getUser()->awardsWithStatus()[$awardGiveaway->getShortName()];


        if ($have_award == false) {
            $day = $this->renderDayRepository->fillUpCheeseHole($this->getUser());
            GlobalInject::getEntityManager()->flush();
            $this->getUser()->setNotificationFromMask(User::NOTIFICATION_MASK_AWARD, 0); // no email for this special award
            $this->getUser()->giveAward($awardGiveaway->getShortName());

            $message = 'Do you like cheese with or without holes?<br> Here is a free render day to plug the hole on your render streak for the '.$day->format('F jS, Y').'.<br>';
            $message .= 'It might take up to 48h to update your max consecutive render day value.';
        }
        else {
            $message = 'You already have earn the give away so you can not get another reward.';
        }

        return $this->render(
            'cheese/take.html.twig',
            [
                'title' => 'Cheese is life',
                'header_default_container' => true,
                'message' => $message,
            ]
        );

    }
}