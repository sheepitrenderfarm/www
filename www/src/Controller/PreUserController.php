<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\PreUser;
use App\Repository\PreUserRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * PreUser controller
 */
#[Route(path: '/preuser')]
class PreUserController extends BaseController {
    private PreUserRepository $preUserRepository;

    public function __construct(
        Main $main,
        HTML $html,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        PreUserRepository $preUserRepository,
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->preUserRepository = $preUserRepository;
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{user}/remove')]
    public function remove(PreUser $user): Response {
        $this->preUserRepository->remove($user);

        return new Response('OK');
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{user}/send_email')]
    public function send_email(PreUser $user): Response {
        $user->sendRegistrationConfirmationEmail();

        return new Response('OK');
    }

    #[Route(path: '/check/username/{username}')]
    public function check_username(string $username): Response {
        return new Response($this->preUserRepository->checkUserName($username) ? 'OK' : 'NOK');

    }

    #[Route(path: '/check/email/{email}')]
    public function check_email(string $email): Response {
        return new Response($this->preUserRepository->checkEmail($email) ? 'OK' : 'NOK');
    }
}