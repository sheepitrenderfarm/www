<?php

namespace App\Controller;

use App\AdventCalendar\ui\AdventCalendar;
use App\Base\BaseController;
use App\Repository\AdventCalendarClaimedDayRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * AdventCalendarClaimedDayController controller
 */
#[Route(path: '/adventcalendar')]
class AdventCalendarClaimedDayController extends BaseController {
    private AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository;
    private AdventCalendar $adventCalendar;

    public function __construct(
        AdventCalendar $adventCalendar,
        AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository,
        Main $main,
        HTML $html,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->adventCalendarClaimedDayRepository = $adventCalendarClaimedDayRepository;
        $this->adventCalendar = $adventCalendar;
    }

    /**
     * Claim a day
     */
    #[Route(path: '/claim')]
    public function claim(): Response {
        if (is_null($this->getUser())) {
            return new Response($this->adventCalendar->htmlCalendar('Need to log in to claim a gift'));
        }
        else {
            // logged in
            if (AdventCalendar::isActive()) {
                $text = $this->adventCalendarClaimedDayRepository->addForToday($this->getUser());

                return new Response($this->adventCalendar->htmlCalendar($text));
            }
            else {
                return new Response('Event not active');
            }
        }
    }
}
