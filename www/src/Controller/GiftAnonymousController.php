<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\GiftAnonymous;
use App\Repository\GiftAnonymousRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Gift Anonymous controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/giftanonymous')]
class GiftAnonymousController extends BaseController {
    private GiftAnonymousRepository $giftAnonymousRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, HTML $html, GiftAnonymousRepository $giftAnonymousRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->giftAnonymousRepository = $giftAnonymousRepository;
    }

    #[Route(path: '/{gift}/show')]
    public function show(GiftAnonymous $gift): Response {
        return $this->render('gift/anonymous/show.html.twig', [
            'gift' => $gift,
        ]);
    }

    #[Route(path: '/{gift}/claim')]
    public function claim(GiftAnonymous $gift): Response {
        $this->giftAnonymousRepository->take($gift, $this->getUser());

        return $this->render('gift/claim.html.twig', [
            'header_default_container' => true,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/add', methods: 'POST')]
    public function add(Request $request): Response {
        $form = $request->request->all();
        if (array_key_exists('gift_anonymous_comment', $form) && array_key_exists('gift_anonymous_amount', $form)) {
            $gift = new GiftAnonymous();
            $gift->setValue((int)($form['gift_anonymous_amount']));
            if ($form['gift_anonymous_comment'] != '') {
                $gift->setComment($form['gift_anonymous_comment']);
            }
            $this->giftAnonymousRepository->generateAccess($gift);
            $gift->setType(GiftAnonymous::TYPE_OTHER);
            $gift->setCreation(time());

            $this->giftAnonymousRepository->add($gift);

            return new RedirectResponse($this->generateUrl("app_admin_gifts"));
        }
        else {
            return new Response('problem #giftanonymous-3987');
        }
    }
}