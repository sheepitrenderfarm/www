<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Constant;
use App\Entity\Event;
use App\Entity\HWIDBlacklist;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\SessionError;
use App\Entity\Tile;
use App\Repository\EventRepository;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionErrorRepository;
use App\Repository\SessionPastRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Repository\UserRepository;
use App\Scheduler\ClientError;
use App\Scheduler\Criterion\CriterionBanJobFromSQLEventUserSendError;
use App\Scheduler\Criterion\CriterionBlenderBinaryIsAvailable;
use App\Scheduler\Criterion\CriterionComputeMethod;
use App\Scheduler\Criterion\CriterionMaxRenderTimeFromUser;
use App\Scheduler\Criterion\CriterionMemory;
use App\Scheduler\Criterion\CriterionProjectRenderUnder2h;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Session controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/session')]
class SessionController extends BaseController {
    private SessionRepository $sessionRepository;
    private SessionPastRepository $sessionPastRepository;
    private HWIDBlacklistRepository $HWIDBlacklistRepository;
    private TileRepository $tileRepository;
    private EventRepository $eventRepository;
    private SessionErrorRepository $sessionErrorRepository;
    private ProjectRepository $projectRepository;
    private UserRepository $userRepository;

    public function __construct(
        Main $main,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        HTML $html,
        ProjectRepository $projectRepository,
        UserRepository $userRepository,
        SessionRepository $sessionRepository,
        SessionPastRepository $sessionPastRepository,
        SessionErrorRepository $sessionErrorRepository,
        HWIDBlacklistRepository $HWIDBlacklistRepository,
        TileRepository $tileRepository,
        EventRepository $eventRepository) {

        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->projectRepository = $projectRepository;
        $this->userRepository = $userRepository;
        $this->sessionRepository = $sessionRepository;
        $this->sessionPastRepository = $sessionPastRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;
        $this->tileRepository = $tileRepository;
        $this->eventRepository = $eventRepository;
        $this->sessionErrorRepository = $sessionErrorRepository;
    }

    /**
     * Display RUNNING session
     */
    #[Route(path: '/{sessionid}')]
    public function single(int $sessionid): Response {
        $session = $this->sessionRepository->find($sessionid);
        if (is_null($session)) {
            // is it a past session ?
            $sessionPast = $this->sessionPastRepository->find($sessionid);
            if (is_object($sessionPast)) {
                return new RedirectResponse($this->router->generate('app_session_past', ['id' => $sessionPast->getId()]));
            }
            else {
                throw new NotFoundHttpException();
            }
        }

        $session_owner = $session->getUser()->getId();

        if ($this->getUser()->isModerator() == false && $session_owner != $this->getUser()->getId()) {
            // a regular user want to see the session of someone else

            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'You do not have right to access to this session',
            ]);
        }

        $n2 = '';
        if (Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_CPU)) {
            $n2 .= 'CPU';
        }
        if (Misc::isMaskEnabled($session->getComputeMethod(), Constant::COMPUTE_GPU)) {
            if (strlen($n2) > 0) {
                $n2 .= ',';
            }
            $n2 .= 'GPU';
        }

        $available_projects = [];

        $owner = $this->userRepository->find($session->getUser()->getId());
        if (is_object($owner)) {
            $blocked_by_me = $owner->getUsersWhoBlockedMe();
            $i_have_blocked = $owner->getBlockedOwners();
            $jobs = $this->projectRepository->findAll();
            foreach ($jobs as $k => $job) {
                if ($job->shouldHideFromUser()) {
                    unset($jobs[$k]);
                    continue;
                }

                if ($job->getStatus() == Constant::PAUSED) {
                    unset($jobs[$k]);
                    continue;
                }

                if ($owner->canRenderProject($job) == false) {
                    unset($jobs[$k]);
                    continue;
                }

                if ($job->isBlocked() && $job->getOwner()->getId() != $session->getUser()->getId()) {
                    unset($jobs[$k]);
                    continue;
                }

                $stats = $job->getCachedStatistics();
                if (($stats[Constant::STATS_TOTAL] - $stats[Constant::PROCESSING] - $stats[Constant::FINISHED]) == 0) {
                    if ($job->getStatus() != Constant::WAITING) {
                        unset($jobs[$k]);
                        continue;
                    }
                    if ($stats[Constant::STATS_TOTAL] > 0) {
                        unset($jobs[$k]);
                        continue;
                    }
                }
            }

            $previous_error = $this->sessionErrorRepository->findBy(['session' => $session->getId()]);

            $criterions = $session->criterions();
            $criterions [] = new CriterionComputeMethod($this->router, $this->entityManager, $this->config, 1);
            $criterions [] = new CriterionMemory($this->router, $this->entityManager, $this->config, 1);
            $criterions [] = new CriterionBlenderBinaryIsAvailable($this->router, $this->entityManager, $this->config, 1);
            $criterions [] = new CriterionBanJobFromSQLEventUserSendError($this->router, $this->entityManager, $this->config, 1, $previous_error);
            $criterions [] = new CriterionProjectRenderUnder2h($this->router, $this->entityManager, $this->config, 1);
            $criterions [] = new CriterionMaxRenderTimeFromUser($this->router, $this->entityManager, $this->config, 1);

            foreach ($jobs as $k => $project) {
                $score = 0.0;
                $status = null;
                foreach ($criterions as $criterion_object) {
                    $val1 = $criterion_object->ponderation();
                    $val2 = $criterion_object->scoreProject(new \Symfony\Component\HttpFoundation\Session\Session(new MockArraySessionStorage()), $session, $project); // give an empty phpsession
                    $score += $val1 * $val2;


                    if (is_null($status) && ($val1 * $val2) < 0) {
                        $status = '<span style= "color: red">'.$criterion_object->humanExplanationOnRefusal($session, $project).'</span>';
                    }
                }

                if (is_null($status)) {
                    $status = '<span style= "color: green">Renderable</span>';
                }
                /** @phpstan-ignore-next-line */
                ($jobs[$k])->status_str = $status;
                $jobs[$k]->score = $score;
            }

            usort($jobs, function (Project $a, Project $b) {
                return $b->score <=> $a->score; // best score first
            });

            foreach ($jobs as $job) {
                $duration = 0;
                $device = $session->getComputeDeviceFor($job->getComputeMethod());
                if (is_object($device) && $job->getCacheFinished() > 0) {
                    $power = $device->getPower();
                    if ($power > 0 && $device->getPowerPercent() > 0) {
                        $coef_power_session = 1.0 / ((float)$device->getPowerPercent() / 100.0);
                        $duration = $coef_power_session * (float)($job->getCacheRendertimeRef()) / (float)($job->getCacheFinished());
                        if ($duration > 0) {
                            $duration += $job->getAveragePrepTime();
                        }
                    }
                }

                /** @phpstan-ignore-next-line */
                $status = $job->status_str;
                if (in_array($job->getOwner()->getId(), $i_have_blocked)) {
                    $status = '<span style= "color: red">The user is on your blacklist</span>';
                }
                else {
                    if (in_array($job->getOwner()->getId(), $blocked_by_me)) {
                        $status = '<span style= "color: red">The owner has blacklisted you</span>';
                    }
                }

                $available_projects [] = [
                    'project_name' => $job->getName(),
                    'project_id' => $job->getId(),
                    'duration' => $duration,
                    'status' => $status,
                ];
            }
        }

        $hwid_blacklist = $this->HWIDBlacklistRepository->findOneBy(['hwid' => $session->gethwid()]);

        $errors = $this->getUser()->isModerator() ? $this->sessionErrorRepository->findBy(['session' => $session->getId()]) : [];
        usort($errors, '\App\Utils\Sort::sortReverseByAttributeId');

        $frames_processing_twig = [];

        $frames_processing = $this->entityManager->getRepository(Tile::class)->findBy(['status' => Constant::PROCESSING, 'session' => $session->getId()]);
        if (count($frames_processing) > 0) {
            foreach ($frames_processing as $f2) {
                $j2 = $f2->getFrame()->getProject();
                $frames_processing_twig [] = [
                    'project_id' => $j2->getId(),
                    'project_path' => $j2->getPath(),
                    'frame_number' => $f2->getFrame()->getNumber(),
                    'rendering_for' => $f2->getRemainingRenderTime(),
                    'remaining' => $f2->getRemainingRenderTime(),
                ];
            }
        }

        return $this->render('session/running.html.twig', [
                'header_default_container' => true,
                'session' => $session,
                'errors' => $errors,
                'session_cluster_cpu' => $this->sessionRepository->getCluster($session, Constant::COMPUTE_CPU),
                'session_cluster_gpu' => $this->sessionRepository->getCluster($session, Constant::COMPUTE_GPU),
                'is_latest_version' => $session->getVersion() == $this->config['client']['jar']['stable']['version'],
                'blacklist_reason' => is_object($hwid_blacklist) ? HWIDBlacklist::REASONS[$hwid_blacklist->getReason()] : '',
                'block_reason' => $session->getBlocked() != 0 ? Session::BLOCK_MESSAGES[$session->getBlocked()]['long'] : '',
                'sessions_block_reasons' => array_map(function ($d) {
                    return $d['long'];
                }, Session::BLOCK_MESSAGES),
                'blacklist_reasons' => HWIDBlacklist::REASONS,
                'frames_processing' => $frames_processing_twig,
                'available_projects' => $available_projects,
                'compute_method' => $n2,
            ]
        );
    }

    #[Route(path: '/past/{id}')]
    public function past(int $id): Response {
        $session = $this->sessionPastRepository->find($id);
        if (is_null($session)) {
            throw new NotFoundHttpException();
        }

        if ($this->getUser()->isModerator() == false && $this->getUser()->getId() != $session->getUser()->getId()) {
            throw new NotFoundHttpException();
        }

        $errors = $this->getUser()->isModerator() ? $this->sessionErrorRepository->findBy(['session' => $session->getId()]) : [];
        usort($errors, '\App\Utils\Sort::sortReverseByAttributeId');

        return $this->render('session/past.html.twig', [
                'header_default_container' => true,
                'session' => $session,
                'is_latest_version' => $session->getVersion() == $this->config['client']['jar']['stable']['version'],
                'errors' => $errors,
            ]
        );
    }


    #[Route(path: '/{sessionid}/frames')]
    public function frames(int $sessionid): Response {
        $sessionRunning = $this->sessionRepository->find($sessionid);
        $sessionPast = $this->sessionPastRepository->find($sessionid);

        $session = $sessionPast ?: $sessionRunning;
        if (is_null($session)) {
            throw new NotFoundHttpException();
        }

        if ($this->getUser()->isModerator()) {
            // can see all sessions
        }
        else {
            if ($session->getUser()->getId() != $this->getUser()->getId()) {
                // a regular user want to see the session of someone else
                return new Response('You do not have right to access to this session');
            }
        }

        $frame = $this->tileRepository->findOneBy(['session' => $sessionid, 'status' => Constant::FINISHED]);
        if (is_null($frame)) {
            return new Response('No frame rendered');
        }
        else {
            $rendered_projects = [];
            foreach ($this->sessionRepository->getProjectsRendered($sessionid) as $project_data) {
                $project = $this->projectRepository->find($project_data['id']);
                if (is_object($project)) {
                    $rendered_projects [] = [
                        'id' => $project_data['id'],
                        'name' => $project_data['name'],
                        'session_id' => $sessionid,
                    ];
                }
            }

            $rendered_tiles = [];

            foreach ($this->sessionRepository->getTilesRendered($sessionid) as $tile_data) {
                $project = $this->projectRepository->find($tile_data['project_id']);
                if ($project->canSeeProject($this->getUser()) || $project->canRenderProject($this->getUser())) {
                    // TODO: ugly
                    $url_thumbnail_shepherd = $project->getShepherd()->getId().'/thumb/'.$project->getShepherdThumbnailToken().'/'.$project->getId().'/tile/'.$tile_data['id'].'/thumbnail';

                    $rendered_tiles [] = [
                        'id' => $tile_data['id'],
                        'url_thumbnail_shepherd' => $url_thumbnail_shepherd,
                        'validation_time' => $tile_data['validation_time'],
                        'render_time' => $tile_data['render_time'],
                        'reward' => $tile_data['reward'],
                        'can_see_project' => $project->canSeeProject($this->getUser()),
                        'project_id' => $tile_data['project_id'],
                        'project_name' => $tile_data['project_name'],
                    ];
                }
            }

            return $this->render('session/frames.html.twig', [
                'rendered_projects' => $rendered_projects,
                'rendered_tiles' => $rendered_tiles,
            ]);

        }
    }

    /**
     * Timeline only available on RUNNING session
     */
    #[Route(path: '/{sessionid}/timeline')]
    public function timeline(int $sessionid): Response {
        if ($this->getUser()->isModerator()) {
            // can see all sessions
        }
        else {
            $login_event = $this->eventRepository->findOneBy(['session' => $sessionid, 'type' => Event::TYPE_LOGIN]);
            if (is_object($login_event) && $login_event->getUser() != $this->getUser()->getId()) {
                // a regular user want to see the session of someone else
                return new Response('You do not have right to access to this session');
            }
            elseif (is_object($login_event) == false) {
                return new Response('Session does not exist');
            }
        }

        $rows = $this->eventRepository->findBy(['session' => $sessionid]);
        if (count($rows) == 0) {
            throw new NotFoundHttpException();
        }

        $values = array();
        foreach ($rows as $o) {
            $values[$o->getTime()] = $o;
            if ($o->getType() == Event::TYPE_VALIDATE) {
                $fake = clone $o;
                $fake->setTime($fake->getTime() - 1);
                $fake->setType(Event::TYPE_RENDERING);
                $values[$fake->getTime()] = $fake;
            }
            else {
                if ($o->getType() == Event::TYPE_REQUEST) {
                    $fake = clone $o;
                    $fake->setTime($fake->getTime() + 1);
                    $fake->setType(Event::TYPE_RENDERING);
                    $values[$fake->getTime()] = $fake;
                }
            }
        }

        foreach ($this->sessionErrorRepository->findBy(['session' => $sessionid]) as $o) {
            if ($o->getType() == ClientError::TYPE_OK) {
                continue; // It's a debug dump sent on every session start
            }
            $values[$o->getTime()] = SessionError::toEvent($o);
        }

        $ret = [];

        asort($values);

        while (count($values) > 0) {
            /* @var Event $ev_start */
            $ev_start = array_shift($values);
            if (count($values) == 0) {
                /* @var ?Event $ev_end */
                $ev_end = null;
            }
            else {
                $ev_end = null;
                foreach ($values as $time => $event) {
                    if ($event->getType() == $ev_start->getType() && $event->getType() == Event::TYPE_RENDERING) {
                        $ev_end = clone $event;
                        unset($values[$time]);
                    }
                    else {
                        break;
                    }
                }
            }
            if ($ev_end == null) {
                $ev_end = clone $ev_start;
                $ev_end->setTime($ev_end->getTime() + 1);
            }

            if ($ev_end->getTime() < $ev_start->getTime()) {
                $ev_end->setTime($ev_start->getTime() + 1);
            }

            $ret [] = [
                $ev_start->getType(),
                "p".$ev_start->getProject(),
                $ev_start->getTime() * 1000,
                $ev_end->getTime() * 1000,
            ];
        }

        return new JsonResponse($ret);
    }

    #[Route(path: '/log/{filename}')]
    public function log(string $filename): Response {
        $time = intval(@substr($filename, strlen('error_'), @strrpos($filename, '_') - strlen('error_')));
        $sessionid = @substr($filename, @strrpos($filename, '_') + 1);

        $contents = '';
        $data = @file_get_contents($this->config['log']['error_dir'].'/'.date('Y-m-d', $time).'/'.$filename.'.log');
        if (is_string($data)) {
            $lines = explode("\n", $data);
            foreach ($lines as $line) {
                $style = '';
                if (strpos($line, '(debug)') !== false) {
                    $style = 'class="level_debug"';
                }
                else {
                    if (strpos($line, '(error)') !== false) {
                        $style = 'class="level_error"';
                    }
                    else {
                        if (strpos($line, '(info)') !== false) {
                            $style = 'class="level_info"';
                        }
                    }
                }

                $contents .= "<span $style>";
                $contents .= strip_tags($line);
                $contents .= '<br>';
                $contents .= '</span>';
            }
        }

        return $this->render('session/log.html.twig', [
                'header_default_container' => true,
                'session_id' => $sessionid,
                'contents' => $contents,
            ]
        );
    }

    /**
     * Reset all the frames from a project rendered by a session
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{sessionid}/reset/project/{projectid}')]
    public function reset_project(int $sessionid, int $projectid): Response {

        $project = $this->projectRepository->find($projectid);
        if (is_object($project) == false) {
            return new Response('no such project');
        }

        foreach ($project->tilesWithStatus(Constant::FINISHED) as $tile) {
            if ($tile->getSession() == $sessionid) {
                $tile->reset();
            }
        }

        return new RedirectResponse($this->generateUrl('app_session_single', ['sessionid' => $sessionid]));
    }

    /**
     * Reset all the frames from a project rendered by a session
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{sessionid}/reset')]
    public function reset_all(int $sessionid): Response {
        $session = $this->sessionRepository->find($sessionid);
        if (is_object($session) == false) {
            $session = new Session();
            $session->setId($sessionid);
        }

        $tiles = $session->tiles();
        foreach ($tiles as $tile) {
            $tile->reset();
        }

        return new Response('OK');
    }

    /**
     * Reset all the frames from a project rendered by a session
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{session}/block/{reason}')]
    public function block(Session $session, int $reason): Response {
        $session->setBlocked($reason);
        $this->entityManager->flush($session);

        return new Response('OK');
    }

    /**
     * Pause/Resume a session
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{session}/running/{state}')]
    public function running(Session $session, bool $state): Response {
        if ($this->getUser()->getId() == $session->getUser()->getId() || $this->getUser()->isModerator()) {
            $session->setRunning($state);
            $this->entityManager->flush($session);

            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this session');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/points_earned')]
    public function points_earned(Request $request): Response {
        $session = $this->sessionRepository->find($request->getSession()->get('session'));
        if (is_object($session)) {
            return new Response(sprintf("%d", (int)($session->getPoints())));
        }
        else {
            return new Response('');
        }
    }
}
