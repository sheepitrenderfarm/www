<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Gift;
use App\Entity\User;
use App\Repository\GiftRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Gift controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/gift')]
class GiftController extends BaseController {
    private GiftRepository $giftRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, ConfigService $configService, HTML $html, GiftRepository $giftRepository, EntityManagerInterface $entityManager) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->giftRepository = $giftRepository;
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/delete/{gift}')]
    public function delete(Gift $gift): Response {
        $this->giftRepository->remove($gift);
        return new Response('OK');
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/add/{owner}/{amount}/{send_email}')]
    public function add(Request $request, User $owner, int $amount, bool $send_email): Response {
        $gift = new Gift();
        $gift->setValue($amount);
        $gift->setOwner($owner);
        if ($request->request->has('comment')) {
            $gift->setComment($request->request->get('comment'));
        }
        $gift = $this->giftRepository->generateAccess($gift);

        $this->entityManager->persist($gift);
        $this->entityManager->flush($gift);
        if ($send_email) {
            $this->giftRepository->sendEmail($gift, $this->config['notification']['gift']);
        }
        return new Response('OK');
    }

    #[Route(path: '/{gift}/key/{key}/claim')]
    public function claim(Gift $gift, string $key): Response {
        if ($gift->getExpiration() < time()) {
            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'You can not claim your gift because it is expired.',
            ]);
        }
        else {
            $this->giftRepository->take($gift, $this->getUser());

            return $this->render('gift/claim.html.twig', [
                'header_default_container' => true,
            ]);
        }
    }

    #[Route(path: '/{gift}/key/{key}/show')]
    public function show(Gift $gift, string $key): Response {
        if ($gift->getKey() != $key) {
            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'Can not import gift.',
            ]);
        }
        else {
            $status = $this->giftRepository->canBeUsed($gift);
            if ($status !== true) {
                return $this->render('error.html.twig', [
                    'error_title' => 'Error',
                    'message' => $status,
                ]);
            }
            else {
                return $this->render('gift/named/show.html.twig', [
                    'gift' => $gift,
                ]);

            }
        }
    }
}
