<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\UI\HTML;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Theme controller
 */
#[Route(path: '/theme')]
class ThemeController extends BaseController {
    #[Route(path: '/switch')]
    public function switch(): Response {
        $new_theme = $this->html->isDarkTheme() ? HTML::THEME_LIGHT : HTML::THEME_DARK;

        $response = new Response('OK');
        $response->headers->setCookie(new Cookie('theme', $new_theme, time() + 365 * 86400));
        return $response;
    }
}