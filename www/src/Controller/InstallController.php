<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Controller\Install\InstallChecker;
use App\Service\GlobalInject;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Install checker controller
 */
#[Route(path: '/install')]
class InstallController extends BaseController {
    #[Route(path: '')]
    public function main(): Response {
        if (GlobalInject::getConfigService()->getData()['install']['checker'] == false) {
            return new Response('Checker disabled');
        }

        $results = [];

        foreach (Misc::findSubClasses(InstallChecker::class) as $class_name) {
            $check = new $class_name(GlobalInject::getConfigService(), $this->entityManager);

            $results[$check->name()] = $check->check();
        }

        return $this->render(
            'install/install.html.twig',
            [
                'version' => getenv('VERSION'),
                'results' => $results,
            ]
        );
    }
}
