<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Repository\ShepherdServerRepository;
use App\Repository\StatsServerRepository;
use App\Repository\StatsShepherdRepository;
use App\Service\ConfigService;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolHostStats extends AdminToolBase implements AdminToolInterface {
    private ConfigService $configService;
    private UrlGeneratorInterface $router;
    private StatsShepherdRepository $statsShepherdRepository;
    private StatsServerRepository $statsServerRepository;
    private ShepherdServerRepository $shepherdServerRepository;
    private HTML $html;

    public function __construct(Security $security, ConfigService $configService, HTML $html, UrlGeneratorInterface $router, StatsShepherdRepository $statsShepherdRepository, StatsServerRepository $statsServerRepository, ShepherdServerRepository $shepherdServerRepository) {
        parent::__construct($security);
        $this->configService = $configService;
        $this->router = $router;
        $this->statsShepherdRepository = $statsShepherdRepository;
        $this->statsServerRepository = $statsServerRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Servers Monitoring';
    }

    public function getIcon(): string {
        return 'microchip';
    }

    public function show(): void {
        $config = $this->configService->getData();

        $this->html->printBreadcrumbs('Servers stats', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));

        $line_httpd_by_hour = "['Time', 'www', 'client', 'api'],";
        $line_cpu_by_hour = "['Time', 'cpu', 'scheduler stop'],";
        $line_memory = "['Time', 'ram', 'swap'],";
        $line_network_by_hour = "['Time', 'RX', 'TX'],";

        $httpd_average = [];
        $network_average = [];

        foreach ($this->statsServerRepository->findAfter(time() - 4 * 24 * 3600) as $o) {
            $hour = $this->html->getTimeWithLocalTimeZone("d-m H", $o->getId());
            if (array_key_exists($hour, $httpd_average) == false) {
                $httpd_average[$hour] = ['total_www' => 0, 'count_www' => 0.0, 'total_client' => 0, 'count_client' => 0.0, 'total_api' => 0, 'count_api' => 0.0];
            }

            $httpd_average[$hour]['total_www'] += (int)($o->getHttpdWorkersWWW());
            $httpd_average[$hour]['count_www'] += 1.0;
            $httpd_average[$hour]['total_client'] += (int)($o->getHttpdWorkersClient());
            $httpd_average[$hour]['count_client'] += 1.0;
            $httpd_average[$hour]['total_api'] += (int)($o->getHttpdWorkersApi());
            $httpd_average[$hour]['count_api'] += 1.0;

            if (array_key_exists($hour, $network_average) == false) {
                $network_average[$hour] = ['total_rx' => 0, 'count_rx' => 0.0, 'total_tx' => 0, 'count_tx' => 0.0];
            }

            $network_average[$hour]['total_rx'] += $o->getNetworkRX();
            $network_average[$hour]['count_rx'] += 1.0;
            $network_average[$hour]['total_tx'] += $o->getNetworkTX();
            $network_average[$hour]['count_tx'] += 1.0;

            $line_cpu_by_hour .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getId())."',".$o->getCpuLoad().','.$config['scheduler']['trigger_overload'].'],';
            $line_memory .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getId())."',".(int)($o->getRamUsed() / 1024.0 / 1024.0).','.(int)($o->getSwapUsed() / 1024.0 / 1024.0).'],';
        }

        array_shift($httpd_average); // first data is always zero
        array_shift($network_average); // first data is always zero

        foreach ($httpd_average as $hour => $data) {
            $line_httpd_by_hour .= "['".$hour.":00',";

            $line_httpd_by_hour .= sprintf("%.1f", $data['total_www'] / $data['count_www']);
            $line_httpd_by_hour .= ",";
            $line_httpd_by_hour .= sprintf("%.1f", $data['total_client'] / $data['count_client']);
            $line_httpd_by_hour .= ",";
            $line_httpd_by_hour .= sprintf("%.1f", $data['total_api'] / $data['count_api']);
            $line_httpd_by_hour .= '],';
        }

        foreach ($network_average as $hour => $data) {
            $line_network_by_hour .= "['".$hour.":00',";

            $line_network_by_hour .= (int)($data['total_rx'] / $data['count_rx']);
            $line_network_by_hour .= ",";
            $line_network_by_hour .= (int)($data['total_tx'] / $data['count_tx']);
            $line_network_by_hour .= '],';
        }

        echo '<script>';
        echo 'var www_line_cpu_per_hour = ['.$line_cpu_by_hour.'];'."\n";
        echo 'var www_line_httpd_per_hour = ['.$line_httpd_by_hour.'];'."\n";
        echo 'var www_line_memory = ['.$line_memory.'];'."\n";
        echo 'var www_line_network = ['.$line_network_by_hour.'];'."\n";
        ?>
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
        linechart2("Number of process on the CPU", www_line_cpu_per_hour, "www_line_cpu_per_hour_div");
        areachart("Apache busy worker per vhost", {}, www_line_httpd_per_hour, "www_line_httpd_per_hour_div");
        linechart2("Memory usage (in MB)", www_line_memory, "www_line_memory_div");
        linechart2("Network (in bps)", www_line_network, "www_line_network_div");
        }
        </script>
        <?php
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">'; // I-1
        echo '<h2>www</h2>';
        echo '<div id="www_line_cpu_per_hour_div" style="width: 100%; height: 500px;"></div>';
        echo '<div id="www_line_memory_div" style="width: 100%; height: 500px;"></div>';
        echo '<div id="www_line_httpd_per_hour_div" style="width: 100%; height: 500px;"></div>';
        echo '<div id="www_line_network_div" style="width: 100%; height: 500px;"></div>';

        foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
            if ($shepherd instanceof ShepherdInternal or $shepherd instanceof ShepherdMock) {
                continue;
            }

            $shepherdIdTrim = 's'.str_replace(array('.', '/', '-'), '_', MISC::humanServerHost($shepherd->getId()));
            $line_cpu = "['Time', 'cpu', 'cpu full'],";
            $line_cpu_pressure = "['Time', 'cpu pressure'],";
            $line_disk = "['Time', 'disk used', 'disk prediction', 'disk max'],";
            $line_disk_pressure = "['Time', 'disk pressure'],";
            $line_memory = "['Time', 'ram', 'ram max'],";
            $line_network = "['Time', 'RX', 'TX'],";
            $line_httpd = "['Time', 'worker'],";
            $line_task = "['Time', 'task'],";


            foreach ($this->statsShepherdRepository->findAfter(time() - 4 * 24 * 60 * 60, $shepherd) as $o) {
                $line_cpu .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".$o->getCpu().','.$o->getCpuMax().'],';
                $line_cpu_pressure .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".$o->getCpuPressure().'],';
                $line_disk .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".(int)($o->getDisk() / 1024.0 / 1024.0 / 1024.0).','.(int)($o->getDiskPrediction() / 1024.0 / 1024.0 / 1024.0).','.(int)($shepherd->getStorageMax() / 1024.0 / 1024.0 / 1024.0).'],';
                $line_disk_pressure .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".$o->getDiskPressure().'],';
                $line_memory .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".(int)($o->getRam() / 1024.0 / 1024.0).','.(int)($o->getRamMax() / 1024.0 / 1024.0).'],';
                $line_network .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".(int)($o->getNetworkRX()).",".(int)($o->getNetworkTX()).'],';
                $line_httpd .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".$o->getHttpd().'],';
                $line_task .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $o->getTime())."',".$o->getTask().'],';
            }

            echo '<script>';
            echo "\n";
            echo 'var '.$shepherdIdTrim.'_line_cpu = ['.$line_cpu.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_cpu_pressure = ['.$line_cpu_pressure.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_disk = ['.$line_disk.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_disk_pressure = ['.$line_disk_pressure.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_memory = ['.$line_memory.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_network = ['.$line_network.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_httpd = ['.$line_httpd.'];'."\n";
            echo 'var '.$shepherdIdTrim.'_line_task = ['.$line_task.'];'."\n";
            ?>
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
            linechart2("Number of process on the CPU", <?php echo $shepherdIdTrim; ?>_line_cpu, "<?php echo $shepherdIdTrim; ?>_line_cpu_div");
            linechart2("CPU pressure", <?php echo $shepherdIdTrim; ?>_line_cpu_pressure, "<?php echo $shepherdIdTrim; ?>_line_cpu_pressure_div");
            linechart2("Disk usage (in GB)", <?php echo $shepherdIdTrim; ?>_line_disk, "<?php echo $shepherdIdTrim; ?>_line_disk_div");
            linechart2("Disk pressure", <?php echo $shepherdIdTrim; ?>_line_disk_pressure, "<?php echo $shepherdIdTrim; ?>_line_disk_pressure_div");
            linechart2("Memory usage (in MB)", <?php echo $shepherdIdTrim; ?>_line_memory, "<?php echo $shepherdIdTrim; ?>_line_memory_div");
            linechart2("Network (in bps)", <?php echo $shepherdIdTrim; ?>_line_network, "<?php echo $shepherdIdTrim; ?>_line_network_div");
            linechart2("Apache worker", <?php echo $shepherdIdTrim; ?>_line_httpd, "<?php echo $shepherdIdTrim; ?>_line_httpd_div");
            linechart2("Task", <?php echo $shepherdIdTrim; ?>_line_task, "<?php echo $shepherdIdTrim; ?>_line_task_div");
            }
            </script>
            <?php

            echo '<h2>'.MISC::humanServerHost($shepherd->getId()).'</h2>';
            echo '<div id="'.$shepherdIdTrim.'_line_cpu_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_cpu_pressure_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_memory_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_network_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_httpd_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_disk_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_disk_pressure_div" style="width: 100%; height: 400px;"></div>';
            echo '<div id="'.$shepherdIdTrim.'_line_task_div" style="width: 100%; height: 400px;"></div>';
        }

        echo '</div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}
