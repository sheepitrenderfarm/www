<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\AwardRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolAwards extends AdminToolBase implements AdminToolInterface {

    private UrlGeneratorInterface $router;
    private AwardRepository $awardRepository;

    public function __construct(Security $security, UrlGeneratorInterface $router, AwardRepository $awardRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->awardRepository = $awardRepository;
    }

    public function getTitle(): string {
        return "Awards";
    }

    public function getIcon(): string {
        return 'star';
    }

    public function show(): void {
        $counts = $this->awardRepository->countByType();

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';
        $categories = array();
        $awards_available = $this->awardRepository->awardsAvailable();
        foreach ($awards_available as $award_classname) {
            $class_name1 = "\\App\Entity\\".$award_classname;
            $award = new $class_name1();
            $cat = $award->category();
            if (array_key_exists($cat, $categories) == false) {
                $categories[$cat] = array();
            }
            $categories[$cat] [] = $award;
        }
        echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
        echo '<thead>';
        echo '<th style="text-align: center; vertical-align: middle;">Image</th>';
        echo '<th style="text-align: center; vertical-align: middle;">Name</th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Reward</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;">Description</th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">User Count</a></th>';
        echo '</thead>';

        foreach ($categories as $cat_name => $cat_contents) {
            echo '<tr><td colspan="5"><h1>'.$cat_name.'</h1></td></tr>';
            usort($cat_contents, '\App\Utils\Sort::sortAwardByLevel');
            foreach ($cat_contents as $award) {
                $short_classname = $award->getDoctrineType();
                echo '<tr>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo '<img src="'.$award->imagePath().'" >';
                echo '</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo str_replace('Award', '', $short_classname);
                echo '</td style="text-align: center; vertical-align: middle;">';

                echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$award->reward().'">'.number_format($award->reward()).'</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo $award->humanDescription();
                echo '</td>';

                $count = 0;
                if (array_key_exists($short_classname, $counts)) {
                    $count = $counts[$short_classname];
                }
                echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$count.'">';
                if ($count > 0) {
                    echo number_format($count);
                }
                echo '</td>';

                echo '</tr>';
            }
        }

        echo '</table>';

        if ($this->user->isAdmin()) {
            echo '<h2>Give an award</h2>';

            echo '<div class="container">';
            echo '<div class="input-group">';
            echo '<select name="select" id="award_type" name="award_type" >';
            foreach ($awards_available as $award_name) {
                echo '<option value="'.$award_name.'">'.str_replace('Award', '', $award_name).'</option>';
            }
            echo '</select>';
            echo 'for ';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    $("#award_owner").autocomplete({minLength: 3, source: "<?php echo $this->router->generate('app_user_list_from_term'); ?>"});
                });
            </script>

            <input id="award_owner"/>
            <input type="button" class="btn btn-primary" onclick="admin_award_give(); return false" value="Give"/>
            </form>
            </div>
            <?php
        }
        echo '</div>';
        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}