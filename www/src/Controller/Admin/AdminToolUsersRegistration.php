<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\PreUserRepository;
use App\Repository\StatsGlobalRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\UI\HTML;
use DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolUsersRegistration extends AdminToolBase implements AdminToolInterface {
    private ConfigService $configService;
    private UrlGeneratorInterface $router;
    private UserRepository $userRepository;
    private PreUserRepository $preUserRepository;
    private HTML $html;
    private StatsGlobalRepository $statsGlobalRepository;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, ConfigService $configService, UserRepository $userRepository, PreUserRepository $preUserRepository, StatsGlobalRepository $statsGlobalRepository) {
        parent::__construct($security);
        $this->configService = $configService;
        $this->router = $router;
        $this->userRepository = $userRepository;
        $this->preUserRepository = $preUserRepository;
        $this->statsGlobalRepository = $statsGlobalRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Users registration';
    }

    public function getIcon(): string {
        return 'user-plus';
    }

    public function show(): void {
        $config = $this->configService->getData();

        $this->html->printBreadcrumbs('Registrations', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $preusers = $this->preUserRepository->findAll();
        if (count($preusers) > 0) {
            usort($preusers, '\App\Utils\Sort::sortByAttributeRegistrationTime');

            echo '<h1>'.sprintf('Unvalidated Users (%d)', count($preusers)).'</h1>';
            echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
            echo '<tr>';
            echo '<th style="text-align: center; vertical-align: middle;">Login</th>';
            echo '<th style="text-align: center; vertical-align: middle;">E-mail</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Date</th>';
            if ($this->user->isModerator()) {
                echo '<th></th>'; // delete
            }
            if ($this->user->isAdmin()) {
                echo '<th></th>'; // activate
            }
            if ($this->user->isModerator()) {
                echo '<th></th>'; // send email
            }
            echo '</tr>';
            foreach ($preusers as $user) {
                echo '<tr style="vertical-align: middle;" >';

                echo '<td>';
                echo $user->getId();
                echo '</td>';

                echo '<td>';
                echo $user->getEmail();
                echo '</td>';

                echo '<td>';
                echo $this->html->getTimeWithLocalTimeZone('H\hi M d', $user->getRegistrationTime());
                echo '</td>';

                if ($this->user->isModerator()) {
                    echo '<td style="text-align: center; vertical-align: middle;">';
                    echo '<input type="button" class="btn btn-danger" onclick="admin_preuser_delete(\''.$user->getId().'\'); return false" value="Delete" />';
                    echo '</td>';
                }

                if ($this->user->isAdmin()) {
                    echo '<td style="text-align: center; vertical-align: middle;">';
                    echo '<a href="'.$user->activateLink().'">Activate</a>';
                    echo '</td>';
                }

                if ($this->user->isModerator()) {
                    echo '<td style="text-align: center; vertical-align: middle;">';
                    echo '<input type="button" class="btn btn-primary" onclick="admin_preuser_send_registration_confirmation_email(\''.$user->getId().'\'); return false" value="Re-send e-mail" />';
                    echo '</td>';
                }

                echo '</tr>';
            }
            echo '</table>';
        }

        echo '<h1>New users over 24h</h1>';

        $yesterday = time() - 86400;

        echo '<div class="container">';
        echo '<table class="table table-bordered table-striped table-comparison table-responsive sortable" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';

        echo '<tr><th style="text-align: center; vertical-align: middle;">Login</th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Rendered Frames</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Ordered Frames</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Projects</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Points</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;">Registration</th>';
        echo '</tr>';

        foreach ($this->userRepository->findRegisteredAfter($yesterday) as $user) {
            echo '<tr style="vertical-align: middle;" >';

            echo '<td>';
            $this->html->printOwnerLink($user->getId());
            echo '</td>';

            echo '<td data-sort="'.$user->getRenderedFrames().'" style="text-align: center; vertical-align: middle;">';
            echo number_format($user->getRenderedFrames());
            echo '</td>';

            echo '<td data-sort="'.$user->getOrderedFrames().'" style="text-align: center; vertical-align: middle;">';
            echo number_format($user->getOrderedFrames());
            echo '</td>';

            echo '<td data-sort="'.$user->getNbProjects().'" style="text-align: center; vertical-align: middle;">';
            echo number_format($user->getNbProjects());
            echo '</td>';

            echo '<td data-sort="'.(int)($user->getPoints()).'" style="text-align: center; vertical-align: middle;">';
            echo '<span ';
            if ($user->getPoints() > 0) {
                echo 'style="color: green;"';
            }
            else {
                if ($user->getPoints() < 0) {
                    echo 'style="color: red;"';
                }
            }
            echo '>';
            echo number_format($user->getPoints());
            echo '</span>';
            echo '</td>';

            echo '<td>';
            echo $this->html->getTimeWithLocalTimeZone('H\hi M d', $user->getRegistrationTime());
            echo '</td>';

            echo '</tr>';
        }
        echo '</table>';

        $registration_amount_per_day = "['Day', 'registration'],";
        $registration_amount_per_week = "['Week', 'registration'],";
        $registration_amount_per_month = "['Month', 'registration'],";
        $active_renderers_str = "['Day', 'active renderers'],";
        $affiliates_str = "['Source', 'registration'],";
        $registrations_per_day = array();
        $registrations_per_week = array();
        $registrations_per_month = array();
        $affiliates = array();

        foreach ($this->userRepository->getRegistrationTimes() as $RegistrationTimesData) {
            $registrationTime = $RegistrationTimesData['time'];
            $day = $this->html->getTimeWithLocalTimeZone("d-m-Y", $registrationTime);
            if (array_key_exists($day, $registrations_per_day) == false) {
                $registrations_per_day[$day] = 0;
            }

            $registrations_per_day[$day] += 1;

            $week = $this->html->getTimeWithLocalTimeZone("W-Y", $registrationTime);
            if (array_key_exists($week, $registrations_per_week) == false) {
                $registrations_per_week[$week] = 0;
            }

            $registrations_per_week[$week] += 1;

            $month = $this->html->getTimeWithLocalTimeZone('m-Y', $registrationTime);
            if (array_key_exists($month, $registrations_per_month) == false) {
                $registrations_per_month[$month] = 0;
            }
            $registrations_per_month[$month] += 1;
        }

        foreach ($this->userRepository->getAffiliates() as $affiliateData) {
            if ($affiliateData['affiliate'] != '') {
                $affiliates[$affiliateData['affiliate']] = $affiliateData['count'];
            }
        }

        for ($i = 90; $i >= 0; $i--) {
            $day = $this->html->getTimeWithLocalTimeZone("d-m-Y", strtotime(sprintf('-%d days', $i)));
            $nb = array_key_exists($day, $registrations_per_day) ? $registrations_per_day[$day] : 0;

            $registration_amount_per_day .= "['".$day."',".$nb.'],';
        }
        for ($i = 52; $i >= 0; $i--) {
            $week = $this->html->getTimeWithLocalTimeZone("W-Y", strtotime(sprintf('-%d weeks', $i)));
            $nb = array_key_exists($week, $registrations_per_week) ? $registrations_per_week[$week] : 0;

            $registration_amount_per_week .= "['".$week."',".$nb.'],';
        }

        for ($i = 60; $i >= 0; $i--) {
            $month = $this->html->getTimeWithLocalTimeZone("m-Y", strtotime(sprintf('-%d months', $i)));
            $nb = array_key_exists($month, $registrations_per_month) ? $registrations_per_month[$month] : 0;

            $registration_amount_per_month .= "['".$month."',".$nb.'],';
        }

        arsort($affiliates);
        foreach ($affiliates as $k => $v) {
            if ($k == '') {
                continue;
            }
            $affiliates_str .= "['".$k."',".$v.'],';
        }

        $start_time = max(1675983600, time() - 6 * 30 * 24 * 3600);  // 1675983600 is 2023-02-10, add of feature
        $active_renderers = [];
        for ($i = $start_time; $i < time(); $i += 86400) {
            $date = new DateTime();
            $date->setTimestamp($i);
            $active_renderers[$date->format('Y-m-d')] = 0;
        }
        foreach ($this->statsGlobalRepository->findAfter($start_time) as $stats) {
            $date = new DateTime();
            $date->setTimestamp($stats->getId());
            $active_renderers[$date->format('Y-m-d')] = $stats->getActiveRenderers();
        }
        foreach ($active_renderers as $day => $value) {
            $active_renderers_str .= "['".$day."',".$value.'],';
        }

        echo '<script>';
        echo 'var data_affiliates = ['.$affiliates_str.'];'."\n";
        echo 'var data_active_renderers = ['.$active_renderers_str.'];'."\n";
        echo 'var data_registration_amount_per_day = ['.$registration_amount_per_day.'];'."\n";
        echo 'var data_registration_amount_per_week = ['.$registration_amount_per_week.'];'."\n";
        echo 'var data_registration_amount_per_month = ['.$registration_amount_per_month.'];'."\n";
        ?>
        const ChartStyles = getComputedStyle(document.documentElement);
        let colorChartText = ChartStyles.getPropertyValue('--color-chart-text').trim();

        var options = {
        backgroundColor: { fill: "transparent" },
        titleTextStyle: {
        color: colorChartText,
        },
        legend: {
        textStyle: { color: colorChartText },
        positions: 'in'
        },
        'vAxis': { minValue: 0 }
        };
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
        <?php
        if ($this->user->isAdmin()) { // don't display for Moderator
            ?>
            piechart("Affiliates", data_affiliates, "piechart_div_affiliates");
            <?php
        }
        ?>
        linechart("Registrations per day", options, data_registration_amount_per_day, "linechart_div_registration_amount_per_day");
        linechart("Registrations per week", options, data_registration_amount_per_week, "linechart_div_registration_amount_per_week");
        linechart("Registrations per month", options, data_registration_amount_per_month, "linechart_div_registration_amount_per_month");
        linechart("Active renderers", options, data_active_renderers, "linechart_div_active_renderers");
        }
        </script>
        <?php

        if ($this->user->isAdmin()) { // don't display for Moderator
            echo '<h1>Registrations</h1>';
        }
        echo '<div class="container">';
        echo '<div id="linechart_div_registration_amount_per_day" style="width: 1200px; height: 500px;"></div>';
        echo '<div id="linechart_div_registration_amount_per_week" style="width: 1200px; height: 500px;"></div>';
        echo '<div id="linechart_div_registration_amount_per_month" style="width: 1200px; height: 500px;"></div>';
        echo '<div id="linechart_div_active_renderers" style="width: 1200px; height: 500px;"></div>';

        if ($this->user->isAdmin()) { // don't display for Moderator
            echo '<div id="piechart_div_affiliates" style="width: 1200px; height: 500px;"></div>';
            echo '<div>';
            echo 'Active: ';
            echo '<ul>';
            ksort($config['affiliate']);
            foreach ($config['affiliate'] as $name => $data) {
                echo '<li>'.$name.'</li>';
            }
            echo '</ul>';
            echo '</div>';
        }
        echo '</div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}