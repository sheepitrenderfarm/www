<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Service\ConfigService;
use App\UI\HTML;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolLog extends AdminToolBase implements AdminToolInterface {
    private ConfigService $configService;
    private UrlGeneratorInterface $router;
    private HTML $html;

    public function __construct(Security $security, ConfigService $configService, HTML $html, UrlGeneratorInterface $router) {
        parent::__construct($security);
        $this->configService = $configService;
        $this->router = $router;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Log';
    }

    public function getIcon(): string {
        return 'magnifying-glass';
    }

    public function show(): void {
        $config = $this->configService->getData();

        $this->html->printBreadcrumbs('Log', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $module_ = 'ferme';

        $data = @file_get_contents($config['log']['dir'].'/'.strtolower($module_).'.log');
        if (is_string($data) == false || $data === '') {
            echo 'File log does not exist';

            echo '</section>';
            echo '</div>';
            echo '</div>';

            return;
        }
        $lines = array_slice(explode("\n", $data), -100);
        echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';
        echo '<tr>';
        echo '<th style="text-align: center; vertical-align: middle;">Date</th>';
        echo '<th style="text-align: center; vertical-align: middle;">Source</th>';
        echo '<th style="text-align: center; vertical-align: middle;">Level</th>';
        echo '<th style="text-align: center; vertical-align: middle;">Content</th>';
        echo '</tr>';
        foreach ($lines as $line) {
            $level = '';

            $expl = explode(' - ', $line, 4);

            if (count($expl) != 4) {
                continue;
            }

            $level = 'level_'.strtolower($expl[2]);

            echo '<tr class="'.$level.'">';

            echo '<td style="text-align: center; vertical-align: middle; width: 95px;">';
            echo $expl[0];
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle; width: 90px;">';
            echo $expl[1];
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle; width: 90px;">';
            echo $expl[2];
            echo '</td>';

            echo '<td style="vertical-align: middle;">';
            echo $expl[3];
            echo '</td>';

            echo '</tr>';
        }
        echo '</table>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}
