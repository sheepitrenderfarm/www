<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Repository\ShepherdServerRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolShepherdsHealth extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private ShepherdServerRepository $shepherdServerRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, ShepherdServerRepository $shepherdServerRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Shepherds health";
    }

    public function getIcon(): string {
        return 'heartbeat';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Shepherds health', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';
        ?>
        <i>Not in real time, it's from last monitoring cron (every 5min).</i>
        <br>
        <br>
        <table class="table table-hover table-bordered table-striped" style="margin-left: auto; margin-right: auto;width:100%;" border="0">
            <thead>
            <tr>
                <th style="text-align: center; vertical-align: middle;">Shepherd</th>

                <th style="text-align: center; vertical-align: middle;">CPU</th>
                <th style="text-align: center; vertical-align: middle;">RAM</th>
                <th style="text-align: center; vertical-align: middle;">Disk usage</th>
                <th style="text-align: center; vertical-align: middle;">Bandwidth RX / TX (bps)</th>
                <th style="text-align: center; vertical-align: middle;">Projects</th>
                <th style="text-align: center; vertical-align: middle;">Rendering frames</th>
                <th style="text-align: center; vertical-align: middle;">Running tasks</th>
                <th style="text-align: center; vertical-align: middle;">Workers https</th>
            </tr>
            <thead>
            <tbody>
        <?php
        foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
            if ($shepherd instanceof ShepherdInternal or $shepherd instanceof ShepherdMock) {
                continue;
            }
            $stats = $shepherd->getLastStats();
            if (is_object($stats)) {
                echo '<tr>';
                echo '<td style="text-align: center; vertical-align: middle;"><a href="'.$shepherd->getId().'/admin/status">'.MISC::humanServerHost($shepherd->getId()).'</a></td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.sprintf("%.1f", $stats->getCpu()).' / '.$stats->getCpuMax().'</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.Misc::humanSize($stats->getRam()).'B / '.Misc::humanSize($stats->getRamMax()).'B</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.Misc::humanSize($stats->getDisk()).'B / '.Misc::humanSize($shepherd->getStorageMax()).'B</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.Misc::humanSize($stats->getNetworkRX()).' / '.Misc::humanSize($stats->getNetworkTX()).'</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.number_format($this->shepherdServerRepository->getBlendsCount($shepherd)).'</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.number_format($stats->getRenderingFrames()).'</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.number_format($stats->getTask()).'</td>';
                echo '<td style="text-align: center; vertical-align: middle;">'.number_format($stats->getHttpd()).'</td>';
                echo '</tr>';
            }
            else {
                echo '<tr>';
                echo '<td style="text-align: center; vertical-align: middle;">';
                echo '<a href="'.$shepherd->getId().'/admin/status" >'.MISC::humanServerHost($shepherd->getId()).'</a>';
                echo '</td>';
                echo '<td style="text-align: center; vertical-align: middle;" colspan="7"><span style="color:red">Shepherd down</span></td>';
                echo '</tr>';
            }
        }

        echo '</tbody>';
        echo '</table>';
        echo '<br>';
        echo '</div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}
