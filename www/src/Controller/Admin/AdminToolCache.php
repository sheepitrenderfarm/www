<?php

namespace App\Controller\Admin;

use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolCache extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private Main $main;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, Main $main) {
        parent::__construct($security);
        $this->router = $router;
        $this->main = $main;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Persistent cache';
    }

    public function getIcon(): string {
        return 'archive';
    }

    public function show(): void {
        $cache = $this->main->getPersistentCache();

        $this->html->printBreadcrumbs('Persistent cache contents', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="string">Key</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;">Value</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        foreach ($cache->getAllKeys() as $key) {
            echo '<tr>';
            echo '<td style="text-align: center; vertical-align: middle;">';
            echo $key;
            echo '</td>';
            echo '<td>';
            Misc::printArray($cache->get($key));
            echo '</td>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}