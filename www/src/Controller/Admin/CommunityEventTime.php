<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

class CommunityEventTime extends CommunityEvent {
    public function prediction(): int {
        $a = $this->stats_current->getId();
        $b = $this->stats_current->getId() - $this->stats_last->getId();
        $c = $this->target - $this->stats_current->getTimeOrdered();
        $d = $this->stats_current->getTimeOrdered() - $this->stats_last->getTimeOrdered();
        if ($d == 0) {
            return -1;
        }
        return (int)((float)($a) + (float)($b) * (float)($c) / (float)($d));
    }

    public function humanTitle(): string {
        return sprintf("%d years of rendering", $this->target / (365 * 24 * 3600));
    }
}