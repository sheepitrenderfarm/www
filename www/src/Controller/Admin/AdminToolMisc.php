<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Constant;
use App\Repository\ProjectRepository;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolMisc extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private ProjectRepository $projectRepository;
    private HTML $html;
    private Main $main;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, Main $main, ProjectRepository $projectRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->projectRepository = $projectRepository;
        $this->html = $html;
        $this->main = $main;
    }

    public function getTitle(): string {
        return "Misc chart";
    }

    public function getIcon(): string {
        return 'bar-chart';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Various charts', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $projects = $this->projectRepository->findAll();
        unset($projects[0]); // remove the project with ID 0

        $compute_methods = array();
        $types = array();
        $versions = array();
        foreach ($projects as $project) {
            $version = $project->getExecutable()->getId();
            if (array_key_exists($version, $versions) == false) {
                $versions[$version] = 0;
            }
            $versions[$version] += 1;

            // TODO: remove the old const
            $method = sprintf("%s%s",
                Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_CPU) ? 'CPU ' : '',
                Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU) || Misc::isMaskEnabled($project->getComputeMethod(), 2) || Misc::isMaskEnabled($project->getComputeMethod(), 4) ? 'GPU ' : '',
            );

            if (array_key_exists($method, $compute_methods) == false) {
                $compute_methods[$method] = 0;
            }
            $compute_methods[$method] += 1;
        }

        $js_values = "[['Version', 'Projects'],";
        ksort($versions);
        foreach ($versions as $version => $nb) {
            $js_values .= '[\''.$version.'\','.$nb.'],';
        }
        $js_values .= ']';

        $project_compute_method_js_values = "[['Project', 'Method'],";
        ksort($compute_methods);
        foreach ($compute_methods as $n => $value) {
            $project_compute_method_js_values .= '[\''.$n.'\','.$value.'],';
        }
        $project_compute_method_js_values .= ']';

        $global_stats = $this->main->globalStats(400);
        $projects_per_week_graph_data = "[['Date', 'projects created'],";

        $days = array();
        $day = null;
        foreach ($global_stats as $data) {
            $day = 7 * 3600 * 24 * (int)($data->getId() / (3600 * 24 * 7));
            if (isset($days[$day]) == false) {
                $days[$day] = array('min' => PHP_INT_MAX, 'max' => 0);
            }

            if ($data->getProjectsCreated() < $days[$day]['min']) {
                $days[$day]['min'] = $data->getProjectsCreated();
            }
            if ($data->getFramesRendered() > $days[$day]['max']) {
                $days[$day]['max'] = $data->getProjectsCreated();
            }
        }

        if (array_key_exists(1658361600, $days)) { // bug July 23, 2022 where the same account was deleted multiple time
            $days[1658361600]["max"] = $days[1658361600]["max"] - 3000;
        }

        if (is_null($day) == false) {
            unset($days[$day]); // remove last
        }
        reset($days);
        unset($days[key($days)]); // remove first
        foreach ($days as $day => $datas1) {
            $projects_per_week_graph_data .= "['".date("Y-W", $day)."',".($datas1['max'] - $datas1['min']).'],';
        }
        $projects_per_week_graph_data .= ']';

        echo '<script>';
        echo 'var piechart_executable_project_version = '.$js_values.';';
        echo 'var piechart_project_compute_method = '.$project_compute_method_js_values.';';
        echo 'var projects_per_week = '.$projects_per_week_graph_data.';';

        echo "var options = {";
        echo "backgroundColor: { fill: 'transparent' },";
        echo "'colors': [ '#dc5430' ],";
        echo "'legend': { position: 'in' },";
        echo "'vAxis': { minValue: 0 }";
        echo '};';
        echo "var options2 = {";
        echo "'colors': [ '#dc5430' ],";
        echo "'legend': { position: 'none' },";
        echo "'vAxis': { minValue: 0 }";
        echo '};';
        echo 'google.charts.setOnLoadCallback(drawChart);';
        echo 'function drawChart() {';
        echo 'piechart("Version distribution", piechart_executable_project_version, "piechart_executable_project_version_div");';
        echo 'piechart("Compute method of project", piechart_project_compute_method, "piechart_project_compute_method_div");';
        echo 'linechart("Number of project created per week", options2, projects_per_week, "projects_per_week_div");';
        echo '}';
        echo '</script>';

        echo '<div id="piechart_executable_project_version_div" style="width: 800px; height: 500px;"></div>';
        echo '<div id="piechart_project_compute_method_div" style="width: 800px; height: 500px;"></div>';
        echo '<div id="projects_per_week_div" style="width: 100%; height: 1000px;"></div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }

}
