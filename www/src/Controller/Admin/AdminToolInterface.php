<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

interface AdminToolInterface {
    public function getTitle(): string;

    public function getIcon(): string;

    public function show(): void;
}