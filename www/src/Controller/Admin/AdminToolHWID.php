<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\HWIDBlacklist;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use App\UI\HTML;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolHWID extends AdminToolBase implements AdminToolInterface {
    private HTML $html;
    private UrlGeneratorInterface $router;

    private UserRepository $userRepository;
    private HWIDBlacklistRepository $HWIDBlacklistRepository;
    private SessionRepository $sessionRepository;


    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, UserRepository $userRepository, HWIDBlacklistRepository $HWIDBlacklistRepository, SessionRepository $sessionRepository) {
        parent::__construct($security);
        $this->html = $html;
        $this->router = $router;
        $this->userRepository = $userRepository;
        $this->sessionRepository = $sessionRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;
    }

    public function getTitle(): string {
        return "HWID";
    }

    public function getIcon(): string {
        return 'shield';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('HWID', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $hwidblacklists = $this->HWIDBlacklistRepository->findAll();
        if (count($hwidblacklists) > 0) {
            echo '<h1>HWID blacklist</h1>';
            echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
            echo '<tr>';
            echo '<th style="text-align: center; vertical-align: middle;">HWID</th>';
            echo '<th style="text-align: center; vertical-align: middle;">login</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Reason</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Action</th>';

            echo '</tr>';
            foreach ($hwidblacklists as $hwidblacklist) {
                echo '<tr style="text-align: center; vertical-align: center;" >';

                echo '<td style="text-align: center; vertical-align: middle;">';
                $session = $this->sessionRepository->findOneBy(['hwid' => $hwidblacklist->getHWID()]);
                if (is_object($session)) {
                    echo '<a href="'.$this->router->generate('app_session_single', ['sessionid' => $session->getId()]).'">';
                }
                echo $hwidblacklist->getHWID();
                if (is_object($session)) {
                    echo '</a>';
                }
                echo '</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                if (is_object($this->userRepository->find($hwidblacklist->getLogin()))) {
                    echo $this->html->retUserProfileLink($hwidblacklist->getLogin());
                }
                else {
                    echo $hwidblacklist->getLogin();
                }
                echo '</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo HWIDBlacklist::REASONS[$hwidblacklist->getReason()];
                echo '</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo '<input type="button" class="btn btn-danger" onclick="admin_hwid_blacklist_delete(\''.$hwidblacklist->getId().'\'); return false" value="Remove" />';
                echo '</td>';


                echo '</tr>';
            }
            echo '</table>';
        }

        echo '<h1>HWID search</h1>';

        echo 'Search for an HWID';
        echo '<input type="text" id="hwid_search_input" name="hwid_search_input" style="margin-left: 6px;" value="" size="52" maxlength="52" />';
        echo '<input type="button" class="btn btn-primary" style="margin-left: 6px;" id="add_submit" value="Search" onclick="doSearchHWID()"/>';

        echo '<div id="search_hwid_result"></div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}