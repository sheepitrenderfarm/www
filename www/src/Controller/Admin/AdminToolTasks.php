<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\ProjectRepository;
use App\Repository\ShepherdServerRepository;
use App\Repository\TaskRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolTasks extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private TaskRepository $taskRepository;
    private ProjectRepository $projectRepository;
    private ShepherdServerRepository $shepherdServerRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, TaskRepository $taskRepository, ProjectRepository $projectRepository, ShepherdServerRepository $shepherdServerRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->taskRepository = $taskRepository;
        $this->projectRepository = $projectRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Tasks";
    }

    public function getIcon(): string {
        return 'tasks';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Tasks', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo "<h2>www</h2>";
        $tasks = $this->taskRepository->findAll();
        if (count($tasks) == 0) {
            echo 'No task';
        }
        else {
            echo '<table class="table table-bordered table-striped table-responsive" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';
            echo '<thead>';
            echo '<tr>';
            echo '<th style="text-align: center; vertical-align: middle;">ID</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Time</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Type</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Status</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Project</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Data</th>';
            echo '<th style="text-align: center; vertical-align: middle;">Action</th>';
            echo '</tr>';
            echo '<thead>';
            echo '<tbody>';

            usort($tasks, '\App\Utils\Sort::sortByAttributeId');
            foreach ($tasks as $task) {
                echo '<tr style="vertical-align: middle;">';

                echo '<td style="text-align: center; vertical-align: middle;" >'.$task->getId().'</td>';
                echo '<td style="text-align: center; vertical-align: middle;" >';
                if ($task->getTime() != 0) {
                    echo $this->html->getTimeWithLocalTimeZone("M d, H:i", $task->getTime());
                }
                echo '</td>';
                echo '<td style="text-align: center; vertical-align: middle;" >'.(new \ReflectionClass($task))->getShortName().'</td>';
                $human = $this->html->humanStatus($task->getStatus());
                echo '<td style="text-align: center; vertical-align: middle;" ><span class="msg_'.$human['style'].'">'.$human['text'].'</span></td>';
                $project = $this->projectRepository->find($task->getProject());
                if (is_object($project)) {
                    echo '<td style="text-align: center; vertical-align: middle;" ><a href="'.$this->router->generate("app_project_manage", ['project' => $project->getId()]).'">'.$project->getName().'</a></td>';
                }
                else {
                    echo '<td style="text-align: center; vertical-align: middle;" >'.$task->getProject().'</td>';
                }

                echo '<td style="text-align: center; vertical-align: middle;" >'.Misc::stringLimit(strip_tags($task->getData()), 32).'</td>';

                echo '<td style="text-align: center; vertical-align: middle;" >';
                echo '<input type="button" class="btn btn-danger" onclick="admin_reset_task(\''.$task->getId().'\'); return false" value="Reset" />';
                echo '</td>';

                echo '</tr>';
            }
            echo '</body>';
            echo '</table>';
        }

        foreach ($this->shepherdServerRepository->findAll() as $shepherdServer) {
            if ($shepherdServer->getEnable() == false) {
                continue;
            }
            echo '<h2>'.MISC::humanServerHost($shepherdServer->getId()).'</h2>';

            $tasks = $shepherdServer->getTasks();
            if (count($tasks) == 0) {
                echo 'No task';
            }
            else {
                echo '<table class="table table-bordered table-striped table-responsive" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';
                echo '<thead>';
                echo '<tr>';
                echo '<th style="text-align: center; vertical-align: middle;">ID</th>';
                echo '<th style="text-align: center; vertical-align: middle;">Type</th>';
                echo '<th style="text-align: center; vertical-align: middle;">Status</th>';
                echo '<th style="text-align: center; vertical-align: middle;">Data</th>';
                echo '<th style="text-align: center; vertical-align: middle;">Action</th>';
                echo '</tr>';
                echo '<thead>';
                echo '<tbody>';
                foreach ($tasks as $task) {
                    echo '<tr>';
                    echo '<td style="text-align: center; vertical-align: middle;" >'.$task['id'].'</td>';
                    echo '<td style="text-align: center; vertical-align: middle;" >'.$task['type'].'</td>';
                    echo '<td style="text-align: center; vertical-align: middle;" >'.$task['status'].'</td>';
                    echo '<td style="text-align: center; vertical-align: middle;" >';
                    foreach (array('tile', 'frame', 'blend') as $attribute) {
                        if (array_key_exists($attribute, $task)) {
                            echo $attribute.': '.$task[$attribute];
                        }
                    }
                    echo '</td>';
                    echo '<td style="text-align: center; vertical-align: middle;" >';
                    echo '<input type="button" class="btn btn-danger" onclick="admin_reset_task_on_shepherd(\''.$task['id'].'\',\''.urlencode(urlencode($shepherdServer->getId())).'\'); return false" value="Reset" />';
                    echo '</td>';

                    echo '</tr>';
                }
                echo '</tbody>';
                echo '</table>';
            }
        }

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}