<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Service\RequestTimeProfiler;
use Symfony\Component\Security\Core\Security;

class AdminToolResponseTime extends AdminToolBase implements AdminToolInterface {

    private RequestTimeProfiler $profiler;

    public function __construct(Security $security, RequestTimeProfiler $profiler) {
        parent::__construct($security);
        $this->profiler = $profiler;
    }

    public function getTitle(): string {
        return "ResponseTime";
    }

    public function getIcon(): string {
        return 'balance-scale';
    }

    public function show(): void {
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $data = $this->profiler->getAverage();

        echo '<h1>Average request time</h1>';

        echo '<table class="table table-bordered table-striped table-responsive" style="margin-left: auto; margin-right: auto;width:60%;" border="0">';
        echo '<tr><th style="text-align: center; vertical-align: middle;">Route</th><th style="text-align: center; vertical-align: middle;">Duration</th></tr>';
        foreach ($data as $route => $average) {
            echo '<tr>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            echo $route;
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            echo number_format($average);
            echo ' ms';
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}