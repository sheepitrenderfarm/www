<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\Repository\AdventCalendarClaimedDayRepository;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolAdventCalendar extends AdminToolBase implements AdminToolInterface {

    private UrlGeneratorInterface $router;
    private AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository;
    private EntityManagerInterface $entityManager;

    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, AdventCalendarClaimedDayRepository $adventCalendarClaimedDayRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->adventCalendarClaimedDayRepository = $adventCalendarClaimedDayRepository;
        $this->entityManager = $entityManager;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Advent Calendar";
    }

    public function getIcon(): string {
        return 'tree';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Advent calendar claims', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $data = [];
        for ($i = 1; $i < 25; $i++) {
            $data[$i] = 0;
        }

        foreach ($this->adventCalendarClaimedDayRepository->getClaimedPerDay() as $day => $value) {
            $data[$day] = $value;
        }

        $per_day = "['Time', 'claims'],";
        foreach ($data as $day => $value) {
            $per_day .= "['$day',$value],";
        }

        echo '<script>';
        echo 'var column_per_day = ['.$per_day.'];'."\n";

        ?>
        var options = {
        vAxis: { minValue: 0 },
        legend: 'none',
        title: "Claims per day",
        isStacked: true
        };
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
        stackedcolumnchartwithoptions("Claims per day", column_per_day, "per_day_div", options)
        }
        </script>
        <div class="container">
            <div id="per_day_div" style="width: 1200px; height: 500px;"></div>
            <br>
            Gift for each days:<br>
            <ul>
                <?php
                for ($i = 1; $i < 25; $i++) {
                    $class_name = '\App\AdventCalendar\gift\AdventCalendarGiftDay'.$i;
                    /** @var AdventCalendarGiftDay $gift */
                    $gift = new $class_name(null, $this->entityManager);
                    echo '<li>'.sprintf("%2d -> %s", $i, $gift->getText()).'</li>';
                }
                ?>
            </ul>
        </div>
        <?php
        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}