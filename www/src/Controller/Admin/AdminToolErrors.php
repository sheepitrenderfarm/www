<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\SessionErrorRepository;
use App\Scheduler\ClientError;
use App\UI\HTML;
use DateTime;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolErrors extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private SessionErrorRepository $sessionErrorRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, SessionErrorRepository $sessionErrorRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->sessionErrorRepository = $sessionErrorRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "User's errors";
    }

    public function getIcon(): string {
        return 'bug';
    }

    public function show(): void {
        $days = 30;

        $this->html->printBreadcrumbs("User's error", array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $error_type_js_values_js_values = "[['Error', 'Value'],";

        $in_percent = array_key_exists('percent', $_GET) && $_GET['percent'] == '1';

        $types = array(
            ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR,
            ClientError::TYPE_NOOUTPUTFILE,
            ClientError::TYPE_RENDERER_OUT_OF_MEMORY,
            ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY,
            ClientError::TYPE_RENDERER_KILLED,
            ClientError::TYPE_RENDERER_KILLED_SERVER,
            ClientError::TYPE_RENDERER_CRASHED,
            ClientError::TYPE_DOWNLOAD_FILE,
            ClientError::TYPE_MISSING_RENDER,
            ClientError::TYPE_MISSING_SCENE,
            ClientError::TYPE_FAILED_TO_EXECUTE,
            ClientError::TYPE_RENDERER_MISSING_LIBRARIES,
            ClientError::TYPE_RENDERER_NOT_AVAILABLE,
            ClientError::TYPE_CAN_NOT_CREATE_DIRECTORY,
            ClientError::TYPE_NETWORK_ISSUE,
            ClientError::TYPE_WRONG_CONFIGURATION,
            ClientError::TYPE_AUTHENTICATION_FAILED,
            ClientError::TYPE_TOO_OLD_CLIENT,
            ClientError::TYPE_SESSION_DISABLED,
            ClientError::TYPE_OS_NOT_SUPPORTED,
            ClientError::TYPE_CPU_NOT_SUPPORTED,
            ClientError::TYPE_GPU_NOT_SUPPORTED,
            ClientError::TYPE_NO_SPACE_LEFT_ON_DEVICE,
            ClientError::TYPE_FRAME_VALIDATION_FAILED,
            ClientError::TYPE_RENDERER_KILLED_BY_USER,
            ClientError::TYPE_RENDERER_KILLED_BY_USER_OVER_TIME,
            ClientError::TYPE_ENGINE_NOT_AVAILABLE,
            ClientError::TYPE_COLOR_MANAGEMENT,
            ClientError::TYPE_IMAGE_WRONG_DIMENSION,
            ClientError::TYPE_CANNOT_READ_FILE,
            ClientError::TYPE_DETECT_DEVICE_ERROR,
            ClientError::TYPE_GPU_OR_DRIVER_ERROR,
            ClientError::TYPE_SERVER_DOWN,
            ClientError::TYPE_UNKNOWN,
        );
        $datas = array();

        $start = time() - $days * 24 * 3600;
        $end = time();

        for ($i = $start; $i <= $end; $i += 4000) {
            $day = $this->html->getTimeWithLocalTimeZone("Y-m-d", $i);
            if (array_key_exists($day, $datas) == false) {
                $datas[$day] = array();
                foreach ($types as $t) {
                    $datas[$day][$t] = 0;
                }
            }
        }
        $types_pie = array();
        foreach ($types as $t) {
            $types_pie[$t] = 0;
        }

        foreach ($datas as $day => $data1) {
            $data_sql = $this->sessionErrorRepository->countForDay(DateTime::createFromFormat('Y-m-d', $day)->setTime(0, 0, 0));
            foreach ($data_sql as $type => $count) {
                if (array_key_exists($type, $datas[$day]) == false) {
                    $datas[$day][ClientError::TYPE_UNKNOWN] += $count;
                    $types_pie[ClientError::TYPE_UNKNOWN] += $count;
                }
                else {
                    $datas[$day][$type] += $count;
                    $types_pie[$type] += $count;
                }
            }
        }

        foreach ($types as $t) {
            $count = 0;
            if (array_key_exists($t, $types_pie)) {
                $count = $types_pie[$t];
                unset($types_pie[$t]);
            }
            $error_type_js_values_js_values .= '[\''.ClientError::toHuman($t).'\','.$count.'],';
        }

        foreach ($types_pie as $type => $count) {
            $error_type_js_values_js_values .= '[\''.ClientError::toHuman($type).'\','.$count.'],';
        }
        $error_type_js_values_js_values .= ']';

        $status_per_day = "['Time',";
        foreach ($types as $t) {
            $status_per_day .= "'".ClientError::toHuman($t)."',";
        }
        $status_per_day .= "],";

        foreach ($datas as $day => $data) {
            $total = 1.0;
            if ($in_percent) {
                $total = 0.0;
                foreach ($types as $t) {
                    $total += 0.01 * $data[$t];
                }
            }
            if ($total == 0) {
                $total = 1.0;
            }
            $status_per_day .= "['".$day."',";
            foreach ($types as $t) {
                $status_per_day .= $data[$t] / $total.',';
            }
            $status_per_day .= '],';
        }

        echo '<script>';
        echo 'var error_type = '.$error_type_js_values_js_values.';';
        echo 'var column_per_day = ['.$status_per_day.'];'."\n";

        echo "var options = {";
        echo "'backgroundColor': { fill: 'transparent' },";
        echo "'colors': [ '#dc5430' ],";
        echo "'legend': { position: 'in' },";
        echo "'vAxis': { minValue: 0 }";
        echo '};';
        echo 'google.charts.setOnLoadCallback(drawChart);';
        echo 'function drawChart() {';
        echo 'piechart("Type of error sent by client on last '.$days.' days", error_type, "piechart_error_type_div");';
        echo 'stackedcolumnchart("Client\'s error", column_per_day, "sessions_status_day_div");';
        echo '}';
        echo '</script>';

        echo '<div id="piechart_error_type_div" style="width: 100%; height: 500px;"></div>';
        echo '<br>';
        if ($in_percent) {
            echo '<a href="?show=errors&percent=0">In values</a>';
        }
        else {
            echo '<a href="?show=errors&percent=1">In percent</a>';
        }
        echo '<br>';
        echo '<div id="sessions_status_day_div" style="width: 100%; height: 1000px;"></div>';

        echo '<h1>Last user\'s errors</h1>';
        echo '<ul>';
        foreach ($this->sessionErrorRepository->last(50) as $error) {
            if ($error->getType() != ClientError::TYPE_RENDERER_KILLED_BY_USER && $error->getType() != ClientError::TYPE_OK) {
                $filename = 'error_'.$error->getTime().'_'.$error->getSession();
                echo '<li><a href="'.$this->router->generate("app_session_log", ['filename' => $filename]).'">'.$this->html->getTimeWithLocalTimeZone("H:i.s", $error->getTime()).'</a>';
                echo ' ';
                echo ClientError::toHuman($error->getType());
                echo '</li>';
            }
        }
        echo '</ul>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}
