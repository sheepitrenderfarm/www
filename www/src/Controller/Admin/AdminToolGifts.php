<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\GiftAnonymous;
use App\Repository\GiftAnonymousRepository;
use App\Repository\GiftRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolGifts extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private GiftAnonymousRepository $giftAnonymousRepository;
    private GiftRepository $giftRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, GiftAnonymousRepository $giftAnonymousRepository, GiftRepository $giftRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->giftAnonymousRepository = $giftAnonymousRepository;
        $this->giftRepository = $giftRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Gifts";
    }

    public function getIcon(): string {
        return 'gift';
    }

    public function show(): void {
        $gifts = $this->giftRepository->findAll();
        usort($gifts, '\App\Utils\Sort::sortByAttributeExpiration');

        $now = time();

        $this->html->printBreadcrumbs(sprintf('Gifts (%d)', count($gifts)), array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        ?>
        <table class="table table-bordered table-striped table-comparision table-responsive sortable">
        <thead>
        <tr>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">ID</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Expiration</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Value</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Owner</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Comment</a></th>
            <?php if ($this->getUser()->isAdmin()) { ?>
                <th style="text-align: center; vertical-align: middle;">Link</th>
                <th style="text-align: center; vertical-align: middle;">Action</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($gifts as $gift) {
            $class = '';
            if ($gift->getExpiration() < $now) {
                $class = 'class="expired"';
            }

            echo '<tr '.$class.'>';

            echo '<td>';
            echo $gift->getId();
            echo '</td>';

            echo '<td data-sort="'.$gift->getExpiration().'">';
            echo $this->html->getTimeWithLocalTimeZone('M d Y', $gift->getExpiration());
            echo '</td>';

            echo '<td data-sort="'.$gift->getValue().'">';
            echo number_format($gift->getValue());
            echo '</td>';

            echo '<td>';
            $this->html->printOwnerLink($gift->getOwner()->getId());
            echo '</td>';

            echo '<td>';
            echo $gift->getComment();
            echo '</td>';

            if ($this->getUser()->isAdmin()) {
                echo '<td>';
                echo sprintf('<a href="%s">public link</a>', $this->router->generate('app_gift_show', ['gift' => $gift->getId(), 'key' => $gift->getKey()]));
                echo '</td>';

                echo '<td>';
                echo '<input type="button" class="btn btn-danger" onclick="admin_gift_delete(\''.$gift->getId().'\'); return false" value="Delete" />';
                echo '</td>';
            }
        }
        echo '</table>';

    if ($this->getUser()->isAdmin()) {
        $gifts = $this->giftAnonymousRepository->findAll();

        echo '<h1>'.sprintf('Anonymous gifts (%d)', count($gifts)).'</h1>';

        ?>
    <table class="table table-bordered table-striped table-comparision table-responsive sortable">
        <thead>
        <tr>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">ID</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Type</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Creation</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Value</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Comment</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#">Link</a></th>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($gifts as $gift) {
            echo '<tr>';

            echo '<td>';
            echo Misc::stringLimit($gift->getId(), 30);
            echo '</td>';

            echo '<td>';
            if ($gift->getType() == GiftAnonymous::TYPE_PATREON) {
                echo 'Patreon';
            }
            else {
                echo $gift->getType();
            }
            echo '</td>';

            echo '<td data-sort="'.$gift->getCreation().'">';
            if ($gift->getCreation() != '') {
                echo $this->html->getTimeWithLocalTimeZone('M d Y', $gift->getCreation());
            }
            echo '</td>';

            echo '<td data-sort="'.$gift->getValue().'">';
            echo number_format($gift->getValue());
            echo '</td>';

            echo '<td>';
            echo $gift->getComment();
            echo '</td>';

            echo '<td>';
            echo '<a href="'.$this->router->generate('app_giftanonymous_show', ['gift' => $gift->getId()]).'">public link</a>';
            echo '</td>';
        }
        echo '</table>';
    }

        if ($this->getUser()->isAdmin()) {
            echo '<h1>Create a user gift</h1>';

            echo '<table>';
            echo '<tr>';
            echo '<td>Generate gift of </td>';
            echo '<td><input type="text" id="gift_value" name="gift_value" value="" size="8" maxlength="8" /></td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td>For </td>';
            echo '<td>';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    $("#gift_owner").autocomplete({minLength: 3, source: "<?php echo $this->router->generate('app_user_list_from_term'); ?>"});
                });
            </script>
            <?php

            echo '<input id="gift_owner" />';

            echo '</td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td>Comment</td>';
            echo '<td><input type="text" id="gift_comment" name="gift_comment" value="" size="50" maxlength="" /></td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td>Send email to owner</td>';
            echo '<td><input type="checkbox" name="gift_send_email" id="gift_send_email" value="1" checked="checked" /></td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td><input type="button" class="btn btn-primary" onclick="admin_gift_create(); return false" value="Create" /></td>';
            echo '<td></td>';
            echo '<tr>';
            echo '</table>';
            echo '</form>';

            echo '<h1>Create an anonymous gift</h1>';

            echo '<table>';

            echo '<form action="'.$this->router->generate('app_giftanonymous_add').'" method="post" enctype="multipart/form-data">';
            echo '<tr>';
            echo '<td>Generate gift of </td>';
            echo '<td><input type="text" id="gift_anonymous_amount" name="gift_anonymous_amount" value="" size="8" maxlength="8" /></td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td>Comment</td>';
            echo '<td><input type="text" id="gift_anonymous_comment" name="gift_anonymous_comment" value="" size="50" maxlength="" /></td>';
            echo '<tr>';

            echo '<tr>';
            echo '<td>';
            echo '<input type="submit" class="btn btn-primary" id="add_submit" value="Create"/>';
            echo '</td>';
            echo '<td></td>';
            echo '<tr>';
            echo '</table>';
            echo '</form>';
        }

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }

}
