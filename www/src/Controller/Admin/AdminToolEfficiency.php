<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\SessionsStatusRepository;
use App\Service\Main;
use App\UI\HTML;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolEfficiency extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private Main $main;
    private SessionsStatusRepository $sessionStatusRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, Main $main, SessionsStatusRepository $sessionStatusRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->main = $main;
        $this->sessionStatusRepository = $sessionStatusRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Efficiency";
    }

    public function getIcon(): string {
        return 'line-chart';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Global efficiency', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));

        $four_days_ago = time() - 4 * 24 * 60 * 60;
        $days = 60;

        $datas = array_reverse($this->sessionStatusRepository->findBy([], ['time' => 'DESC'], $days * 24));

        $status_per_hour = "['Time', 'working fine', 'idle', 'sleeping', 'paused', 'broken (zero validation)', 'disabled', 'unknown'],";
        foreach ($datas as $i => $data) {
            if ($four_days_ago < $data->getTime()) {
                if (array_key_exists('number', $_GET)) {
                    $total = 1.0;
                    $multi = 1.0;
                }
                else {
                    $total = 0;
                    $total += $data->getRendering();
                    $total += $data->getRunning();
                    $total += $data->getPaused();
                    $total += $data->getSleeping();
                    $total += $data->getBrokenZeroValidation();
                    $total += $data->getIdle();
                    $total += $data->getDisabled();
                    $total += $data->getUnknown();
                    $total = (float)($total);
                    $multi = 100.0;
                }

                if ($total == 0) { // division per 0
                    $total = 1.0;
                }

                $status_per_hour .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $data->getTime())."',".($multi * ($data->getRendering() + $data->getRunning()) / $total).','.($multi * $data->getIdle() / $total).','.($multi * $data->getSleeping() / $total).','.($multi * $data->getPaused() / $total).','.($multi * $data->getBrokenZeroValidation() / $total).','.($multi * $data->getDisabled() / $total).','.($multi * $data->getUnknown() / $total).'],';
            }
        }

        $days = array();
        $status_per_day = "['Time', 'working fine', 'idle', 'sleeping', 'paused', 'broken (zero validation)', 'disabled', 'unknown'],";
        foreach ($datas as $i => $data) {
            $day1 = $this->html->getTimeWithLocalTimeZone("d-m-Y", $data->getTime());

            if (array_key_exists($day1, $days) == false) {
                $days[$day1] = array('rendering' => 0, 'running' => 0, 'sleeping' => 0, 'paused' => 0, 'broken_zero_validation' => 0, 'idle' => 0, 'disabled' => 0, 'unknown' => 0, 'total' => 0);
            }

            $days[$day1]['total'] += $data->getRendering();
            $days[$day1]['total'] += $data->getRunning();
            $days[$day1]['total'] += $data->getSleeping();
            $days[$day1]['total'] += $data->getPaused();
            $days[$day1]['total'] += $data->getBrokenZeroValidation();
            $days[$day1]['total'] += $data->getIdle();
            $days[$day1]['total'] += $data->getDisabled();
            $days[$day1]['total'] += $data->getUnknown();

            $days[$day1]['rendering'] += $data->getRendering();
            $days[$day1]['running'] += $data->getRunning();
            $days[$day1]['paused'] += $data->getPaused();
            $days[$day1]['sleeping'] += $data->getSleeping();
            $days[$day1]['broken_zero_validation'] += $data->getBrokenZeroValidation();
            $days[$day1]['idle'] += $data->getIdle();
            $days[$day1]['disabled'] += $data->getDisabled();
            $days[$day1]['unknown'] += $data->getUnknown();
        }

        foreach ($days as $i => $data) {
            if (array_key_exists('number', $_GET)) {
                $total = 24.0;
                $multi = 1.0;
            }
            else {
                $total = 0;
                $total += $data['rendering'];
                $total += $data['running'];
                $total += $data['paused'];
                $total += $data['sleeping'];
                $total += $data['broken_zero_validation'];
                $total += $data['idle'];
                $total += $data['disabled'];
                $total += $data['unknown'];
                $total = (float)($total);
                $multi = 100.0;
            }

            if ($total == 0) {
                $total = 1;
            }

            $status_per_day .= "['".$i."',".($multi * ($data['rendering'] + $data['running']) / $total).','.($multi * $data['idle'] / $total).','.($multi * $data['sleeping'] / $total).','.($multi * $data['paused'] / $total).','.($multi * $data['broken_zero_validation'] / $total).','.($multi * $data['disabled'] / $total).','.($multi * $data['unknown'] / $total).'],';
        }

        $datas = $this->main->globalStats(4);

        $line_by_hour = "['Time', 'connected client', 'processing frame'],";

        foreach ($datas as $i => $data) {
            $line_by_hour .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $data->getId())."',".$data->getSessions().','.$data->getFramesProcessing().'],';
        }

        echo '<script>';
        echo 'var line_by_hour = ['.$line_by_hour.'];'."\n";
        echo 'var column_per_hour = ['.$status_per_hour.'];'."\n";
        echo 'var column_per_day = ['.$status_per_day.'];'."\n";

        ?>
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
        linechart2("Efficiency machine / processing frame", line_by_hour, "line_by_hour_div");
        <?php
        if (array_key_exists('number', $_GET)) {
            echo 'stackedcolumnchart("Efficiency machine per hour", column_per_hour, "sessions_status_hour_div");'."\n";
            echo 'stackedcolumnchart("Efficiency machine per day", column_per_day, "sessions_status_day_div");'."\n";
        }
        else {
            echo 'stackedcolumnchart("Efficiency machine per hour (in percent)", column_per_hour, "sessions_status_hour_div");'."\n";
            echo 'stackedcolumnchart("Efficiency machine per day (in percent)", column_per_day, "sessions_status_day_div");'."\n";
        }
        ?>
        }
        </script>
        <?php

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">'; // I-1
        echo '<div id="line_by_hour_div" style="width: 1200px; height: 500px;"></div>';
        echo '<div id="sessions_status_hour_div" style="width: 1200px; height: 500px;"></div>';
        echo '<div id="sessions_status_day_div" style="width: 1200px; height: 500px;"></div>';
        echo '</div>'; // I-1
        echo '</section>';
        echo '</div>';
    }

}