<?php

namespace App\Controller\Admin;

use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Composer\InstalledVersions;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolConfig extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private array $config;
    private HTML $html;
    private Main $main;

    public function __construct(Security $security, Main $main, HTML $html, UrlGeneratorInterface $router, ConfigService $configService) {
        parent::__construct($security);
        $this->router = $router;
        $this->config = $configService->getData();
        $this->html = $html;
        $this->main = $main;

    }

    public function getTitle(): string {
        return 'Configuration';
    }

    public function getIcon(): string {
        return 'cogs';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Configuration', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable">';
        echo '<thead>';
        echo '<tr>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="string">Key</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;">Value</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        foreach ($this->getData() as $key => $value) {
            echo '<tr>';
            echo '<td style="text-align: center; vertical-align: middle;">';
            echo $key;
            echo '</td>';
            echo '<td style="text-align: center; vertical-align: middle;">';
            echo $value;
            echo '</td>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table>';
        echo '</section>';
        echo '</div>';
        echo '</div>';
    }

    /**
     * @return array<string, string>
     */
    private function getData(): array {
        $data = [];

        $data['Users can render'] = $this->config['scheduler']['running'] ? 'Yes' : 'No';
        $data['Users can add project'] = $this->config['project']['maintenance']['enable'] ? 'No' : 'Yes';
        $data['Rate limit, max rendering frames'] = $this->config['project']['rate_limit']['max_rendering_tile'];
        $data['Projects per user'] = number_format($this->config['project']['max_concurrent_project_per_user']);
        $data['Max users per team'] = number_format($this->config['team']['max_member']);
        $data['Max frames per project'] = number_format($this->config['project']['max_frames']);
        $data['Min client version'] = $this->config['client']['minimum_version']['all'];
        $data['Min Blender version'] = $this->config['renderer']['minimal_support'];
        $data['Blend reader version'] = InstalledVersions::getPrettyVersion('sheepitrenderfarm/blend-reader');
        $cache = $this->main->getPersistentCache();
        $time = $cache->get('last_successfully_email');
        if (is_int($time) && $time > 0) {
            $data['Last successfully email'] = $this->html->getTimeWithLocalTimeZone('F jS H:i:s', $time);
        }

        return $data;
    }
}