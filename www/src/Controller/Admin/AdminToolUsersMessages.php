<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\UI\HTML;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolUsersMessages extends AdminToolBase implements AdminToolInterface {

    private UrlGeneratorInterface $router;
    private MessageRepository $messageRepository;
    private UserRepository $userRepository;

    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, MessageRepository $messageRepository, UserRepository $userRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->messageRepository = $messageRepository;
        $this->userRepository = $userRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Messages";
    }

    public function getIcon(): string {
        return 'envelope';
    }

    public function show(): void {
        $limit = 50;
        $messages = $this->messageRepository->findBy([], ['time' => 'DESC'], $limit);

        $this->html->printBreadcrumbs(sprintf('Last %d messages', $limit), array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<article class="chatSys">';
        echo '<section class="window">';
        foreach ($messages as $message) {
            $from_admin = false;
            $senderUser = $this->userRepository->find($message->getSender());
            if (is_object($senderUser)) {
                $from_admin = $senderUser->isModerator() || $senderUser->getId() == 'admin';
            }
            if ($from_admin) {
                $this->html->printMessageLeft($message, $message->getReceiver(), true);
            }
            else {
                $this->html->printMessageRight($message, $message->getSender());
            }
        }
        echo '</section>';
        echo '</article>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}