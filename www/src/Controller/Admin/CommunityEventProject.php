<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

class CommunityEventProject extends CommunityEvent {
    public function prediction(): int {
        $a = $this->stats_current->getId();
        $b = $this->stats_current->getId() - $this->stats_last->getId();
        $c = $this->target - $this->stats_current->getProjectsCreated();
        $d = $this->stats_current->getProjectsCreated() - $this->stats_last->getProjectsCreated();
        if ($d == 0) {
            return -1;
        }
        return (int)((float)($a) + (float)($b) * (float)($c) / (float)($d));
    }

    public function humanTitle(): string {
        return sprintf("%s projects created", number_format($this->target));
    }
}
