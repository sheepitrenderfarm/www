<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Repository\ShepherdServerRepository;
use App\Repository\StatsShepherdRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolTasksOnShepherdsSumup extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private StatsShepherdRepository $statsShepherdRepository;
    private ShepherdServerRepository $shepherdServerRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, StatsShepherdRepository $statsShepherdRepository, ShepherdServerRepository $shepherdServerRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->statsShepherdRepository = $statsShepherdRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Tasks on shepherds';
    }

    public function getIcon(): string {
        return 'cubes';
    }

    public function show(): void {
        $this->html->printBreadcrumbs('Tasks per shepherd', array(
            array('title' => 'Admin', 'url' => $this->router->generate('app_admin_main')),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        $shepherds = $this->shepherdServerRepository->findAll();
        foreach ($shepherds as $k => $shepherdServer) {
            if ($shepherdServer instanceof ShepherdInternal or $shepherdServer instanceof ShepherdMock) {
                unset($shepherds[$k]);
            }
        }
        $tasks_shepherds = [];
        foreach ($shepherds as $shepherdServer) {
            foreach ($this->statsShepherdRepository->findAfter(time() - 7 * 24 * 60 * 60, $shepherdServer) as $o) {
                if (array_key_exists($o->getTime(), $tasks_shepherds) == false) {
                    $tasks_shepherds[$o->getTime()] = [];
                }
                $tasks_shepherds[$o->getTime()][$shepherdServer->getId()] = $o->getTask();
            }
        }

        ksort($tasks_shepherds);

        $tasks_per_shepherd = "['Time', ";
        foreach ($shepherds as $shepherdServer) {
            $tasks_per_shepherd .= "'".MISC::humanServerHost($shepherdServer->getId())."'";
            $tasks_per_shepherd .= ',';
        }
        $tasks_per_shepherd .= "],";
        foreach ($tasks_shepherds as $time => $data) {
            $tasks_per_shepherd .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $time)."',";
            foreach ($shepherds as $shep) {
                $task_count = array_key_exists($shep->getId(), $data) ? $data[$shep->getId()] : 0;
                $tasks_per_shepherd .= "$task_count , ";
            }
            $tasks_per_shepherd .= '],';
        }

        echo '<script>';
        echo 'var data_tasks_shepherds = ['.$tasks_per_shepherd.'];'."\n";
        ?>
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
        linechart2("Tasks per shepherd", data_tasks_shepherds, "linechart_tasks_shepherds");
        }
        </script>
        <?php
        echo '<div id="linechart_tasks_shepherds" style="width: 1200px; height: 500px;"></div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}