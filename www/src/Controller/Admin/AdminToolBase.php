<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class AdminToolBase {
    protected User $user;

    public function __construct(Security $security) {
        // force cast
        if (is_object($security->getUser())) {
            /** @phpstan-ignore-next-line */
            $this->user = $security->getUser();
        }
    }


    protected function getUser(): User {
        return $this->user;
    }
}