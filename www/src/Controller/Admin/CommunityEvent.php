<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Entity\StatsGlobal;

abstract class CommunityEvent {

    protected StatsGlobal $stats_current;
    protected StatsGlobal $stats_last;
    protected int $target;

    public function __construct(StatsGlobal $current, StatsGlobal $last, int $target) {
        $this->stats_current = $current;
        $this->stats_last = $last;
        $this->target = $target;
    }

    abstract public function prediction(): int;

    abstract public function humanTitle(): string;

    public function __toString(): string {
        return static::class.'(target: '.$this->target.')';
    }
}
