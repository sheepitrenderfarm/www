<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\CDN\CDNBucket;
use App\Entity\Shepherd;
use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Repository\CDNRepository;
use App\Repository\ShepherdServerRepository;
use App\Repository\StatsCDNRepository;
use App\Repository\StatsShepherdRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolServers extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private StatsCDNRepository $statsMirrorRepository;
    private CDNRepository $CDNRepository;
    private ShepherdServerRepository $shepherdServerRepository;
    private StatsShepherdRepository $statsShepherdRepository;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, StatsCDNRepository $statsMirrorRepository, CDNRepository $CDNRepository, ShepherdServerRepository $shepherdServerRepository, StatsShepherdRepository $statsShepherdRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->CDNRepository = $CDNRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->statsShepherdRepository = $statsShepherdRepository;
        $this->statsMirrorRepository = $statsMirrorRepository;
        $this->html = $html;
    }

    public function getTitle(): string {
        return "Servers";
    }

    public function getIcon(): string {
        return 'database';
    }

    public function show(): void {
        // cdn
        $binary_mirrors = $this->CDNRepository->findAll();
        $binary_mirror_per_day = "['Date',";
        foreach ($binary_mirrors as $t) {
            $binary_mirror_per_day .= "'".($t instanceof CDNBucket ? 'bucket' : Misc::humanServerHost($t->getId()))."',";
        }
        $binary_mirror_per_day .= "],";

        $times = [];
        foreach ($this->statsMirrorRepository->findAfter(time() - 30 * 24 * 3600) as $sm) {
            if (array_key_exists($sm->getDate()->getTimestamp(), $times) == false) {
                $times[$sm->getDate()->getTimestamp()] = [];
            }
            $times[$sm->getDate()->getTimestamp()][$sm->getUrl()] = $sm->getTotal();
        }
        ksort($times);

        foreach ($times as $time => $mirrors) {
            $binary_mirror_per_day .= "['".$this->html->getTimeWithLocalTimeZone('M d', $time)."',";

            foreach ($binary_mirrors as $mirror) {
                $binary_mirror_per_day .= sprintf("%.2f", (isset($times[$time][$mirror->getId()]) ? $times[$time][$mirror->getId()] : 0) / 1024.0 / 1024.0 / 1024.0 / 1024.0).',';
            }
            $binary_mirror_per_day .= '],';
        }

        echo '<script>';
        echo 'var binary_mirror_per_day = ['.$binary_mirror_per_day.'];'."\n";

        echo 'function drawMirrorUsageChart() {';
        echo 'stackedcolumnchart("CDN usage (in TB)", binary_mirror_per_day, "binary_mirror_per_day_div");';
        echo '}';
        echo '</script>';

        $this->html->printBreadcrumbs('Servers', array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<h2>CDN</h2>';
        echo '<div class="container">';
        echo '<table class="table table-bordered table-striped table-comparision table-responsive sortable" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';
        echo '<tr>';
        echo '<th style="text-align: center; vertical-align: middle;width:30%;"><a class="sortable" href="#" data-default-order="desc">URL</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Download today</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Total today</a></th>';
        echo '</tr>';

        $mirrors = $this->CDNRepository->findAll();
        foreach ($mirrors as $server) {
            echo '<tr style="vertical-align: middle;">';

            echo '<td data-sort="'.MISC::humanServerHost($server->getId()).'" style="text-align: center; vertical-align: middle;">'.Misc::humanServerHost($server->getId()).'</td>';

            echo '<td data-sort="'.$server->getDownLoad().'" style="text-align: center; vertical-align: middle;">';
            if ($server->getDownLoad() != 0 || $server->getDownload() != 0) {
                echo number_format($server->getDownload());
            }
            echo '</td>';

            echo '<td data-sort="'.$server->getTotal().'" style="text-align: center; vertical-align: middle;">';
            if ($server->getTotal() != 0) {
                echo Misc::humanSize($server->getTotal());
            }
            echo '</td>';

            echo '</tr>';
        }
        echo '</table>';
        echo '<div id="binary_mirror_per_day_div" style="width: 100%; height: 500px;"></div>';
        echo '</div>';

        $shepherdServers = $this->shepherdServerRepository->findAll();
        foreach ($shepherdServers as $k => $shepherdServer) {
            if ($shepherdServer instanceof ShepherdInternal or $shepherdServer instanceof ShepherdMock) {
                unset($shepherdServers[$k]);
            }
        }
        echo '<h2>Shepherd</h2>';
        echo '<div class="container">';
        echo '<table class="table table-bordered table-comparision table-responsive sortable" style="margin-left: auto; margin-right: auto;width:100%;" border="0">';
        echo '<th style="text-align: center; vertical-align: middle;width:20%;"><a class="sortable" href="#" data-default-order="desc">URL</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;">Status</th>';
        if ($this->user->isModerator()) {
            echo '<th style="text-align: center; vertical-align: middle;">Owner</th>';
        }
        echo '<th style="text-align: center; vertical-align: middle;">Maintainer</th>';
        if ($this->user->isModerator()) {
            echo '<th style="text-align: center; vertical-align: middle;">Version</th>';
        }
        echo '<th style="text-align: center; vertical-align: middle;">Rendering frames</th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Projects</a></th>';
        echo '<th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Storage</a></th>';
        if ($this->user->isModerator()) {
            echo '<th style="text-align: center; vertical-align: middle;">Action</th>';
        }

        echo '</tr>';
        foreach ($shepherdServers as $server) {
            /** @var Shepherd $server */
            echo '<tr style="vertical-align: middle;"';
            if ($server->isEnabled() == false || $server->getEnable() == false || $server->allowNewRenderingFrame() == false || $server->getTimeout() || $server->getAllowNewBlend() == false) {
                echo 'class="danger"';
            }
            echo '>';

            echo '<td data-sort="'.str_replace('https://', '', $server->getId()).'" style="text-align: center; vertical-align: middle;">';
            echo '<a href="'.$server->getId().'/admin/status" >'.MISC::humanServerHost($server->getId()).'</a>';
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            if ($server->getEnable() == false) {
                echo '<strong>Disabled</strong>';
            }
            elseif ($server->allowNewRenderingFrame() == false) {
                echo '<strong>Overloaded</strong>';
            }
            elseif ($server->getTimeout()) {
                echo '<strong>Timeout</strong>';
            }
            elseif ($server->getAllowNewBlend() == false) {
                echo '<strong>No allowing new project</strong>';
            }
            echo '</td>';

            if ($this->user->isModerator()) {
                echo '<td style="text-align: center; vertical-align: middle;">';
                $this->html->printOwnerLink($server->getOwner()->getId());
                echo '</td>';
            }
            echo '<td style="text-align: center; vertical-align: middle;">';
            $this->html->printOwnerLink($server->getMaintainer()->getId());
            echo '</td>';

            if ($this->user->isModerator()) {
                $unique_id = Misc::code(20);
                $js_method_name = 'requestVersion_'.$unique_id;
                ?>
                <script>
                    function <?php echo $js_method_name?>() {
                        return requestThemShowOnId('version_<?php echo $unique_id;?>', '<?php echo $this->router->generate('app_shepherd_version', ['shepherdId' => urlencode(urlencode($server->getId()))]) ?>');
                    }

                    google.charts.setOnLoadCallback(<?php echo $js_method_name?>);
                </script>
                <?php

                echo '<td style="text-align: center; vertical-align: middle;" id="version_'.$unique_id.'">';
                echo '<div style="width: 44px;height: 44px;background: url(\'/media/image/fancybox_loading.gif\') center center no-repeat;"></div>';

                echo '</td>';
            }

            echo '<td style="text-align: center; vertical-align: middle;">';
            echo '<span style="color:'.($server->allowNewRenderingFrame() ? 'green' : 'red').'">';
            echo number_format($this->shepherdServerRepository->getRenderingFrameCount($server));
            echo '</span>';
            echo '</td>';

            $nbProject = $this->shepherdServerRepository->getBlendsCount($server);

            echo '<td data-sort="'.$nbProject.'" style="text-align: center; vertical-align: middle;">';
            echo number_format($nbProject);
            echo '</td>';

            echo '<td data-sort="'.$server->getStorageMax().'" style="text-align: center; vertical-align: middle;">';
            $s = $this->statsShepherdRepository->getLast($server);
            if (is_object($s)) {
                echo Misc::humanSize($s->getDisk());
            }
            echo ' / ';
            if ($server->getStorageMax() > 0) {
                echo Misc::humanSize($server->getStorageMax()).'B';
            }
            echo '<br>';
            echo 'Predicted: '.Misc::humanSize($this->shepherdServerRepository->getPredictedStorageUsage($server)).'B';
            echo '</td>';

            if ($this->user->isModerator()) {
                echo '<td style="text-align: center; vertical-align: middle;">';
                if ($server->getEnable() == false) {
                    echo '<input type="button" class="btn btn-primary" onclick="admin_shepherd_enable(\''.urlencode(urlencode($server->getId())).'\', \'1\'); return false" value="Enable" />';
                }
                else {
                    echo '<input type="button" class="btn btn-danger" onclick="admin_shepherd_enable(\''.urlencode(urlencode($server->getId())).'\', \'0\'); return false" value="Disable" />';
                }
                echo '<br>';
                if ($server->getAllowNewBlend() == false) {
                    echo '<input type="button" class="btn btn-primary" onclick="admin_shepherd_allow_new_blend(\''.urlencode(urlencode($server->getId())).'\', \'1\'); return false" value="Accept new project" />';
                }
                else {
                    echo '<input type="button" class="btn btn-danger" onclick="admin_shepherd_allow_new_blend(\''.urlencode(urlencode($server->getId())).'\', \'0\'); return false" value="Refuse new project" />';
                }
                echo '</td>';
            }

            echo '</tr>';
        }
        echo '</table>';

        $shepherd_rendering_frames = array();
        $shepherd_rendered_tiles = array();

        $column_shepherd_rendering_frames = "['Time', ";
        $column_shepherd_rendered_tiles = "['Time', ";
        foreach ($this->shepherdServerRepository->findAll() as $k => $v) {
            $column_shepherd_rendering_frames .= "'";
            $column_shepherd_rendering_frames .= MISC::humanServerHost($v->getId());
            $column_shepherd_rendering_frames .= "'";
            $column_shepherd_rendering_frames .= ',';

            $column_shepherd_rendered_tiles .= "'";
            $column_shepherd_rendered_tiles .= MISC::humanServerHost($v->getId());
            $column_shepherd_rendered_tiles .= "'";
            $column_shepherd_rendered_tiles .= ',';
        }
        $column_shepherd_rendering_frames .= '],';
        $column_shepherd_rendered_tiles .= '],';

        foreach ($this->statsShepherdRepository->findAfter(time() - 4 * 24 * 3600) as $d) {
            $hour = $this->html->getTimeWithLocalTimeZone("d-m H", $d->getTime());
            if (array_key_exists($hour, $shepherd_rendering_frames) == false) {
                $shepherd_rendering_frames[$hour] = array();
            }
            if (array_key_exists($d->getShepherd()->getId(), $shepherd_rendering_frames[$hour]) == false) {
                $shepherd_rendering_frames[$hour][$d->getShepherd()->getId()] = 0;
            }

            $shepherd_rendering_frames[$hour][$d->getShepherd()->getId()] += $d->getRenderingFrames();

            if (array_key_exists($hour, $shepherd_rendered_tiles) == false) {
                $shepherd_rendered_tiles[$hour] = array();
            }
            if (array_key_exists($d->getShepherd()->getId(), $shepherd_rendered_tiles[$hour]) == false) {
                $shepherd_rendered_tiles[$hour][$d->getShepherd()->getId()] = 0;
            }

            $shepherd_rendered_tiles[$hour][$d->getShepherd()->getId()] += $d->getRenderedTiles();
        }

        $now_hour = $this->html->getTimeWithLocalTimeZone("d-m H", time());
        foreach ($shepherd_rendering_frames as $time => $values) {
            $column_shepherd_rendering_frames .= "['".$time.":00',";
            foreach ($this->shepherdServerRepository->findAll() as $k => $v) {
                $column_shepherd_rendering_frames .= array_key_exists($v->getId(), $values) ? $values[$v->getId()] : 0;
                $column_shepherd_rendering_frames .= ',';
            }
            $column_shepherd_rendering_frames .= '],';
        }

        foreach ($shepherd_rendered_tiles as $time => $values) {
            $column_shepherd_rendered_tiles .= "['".$time.":00',";
            foreach ($this->shepherdServerRepository->findAll() as $k => $v) {
                $column_shepherd_rendered_tiles .= array_key_exists($v->getId(), $values) ? $values[$v->getId()] : 0;
                $column_shepherd_rendered_tiles .= ',';
            }
            $column_shepherd_rendered_tiles .= '],';
        }

        ?>

        <div id="shepherd_rendering_frames_div" style="width: 100%; height: 600px"></div>
        <div id="shepherd_rendered_tiles_div" style="width: 100%; height: 600px"></div>
        <script>
            var column_shepherd_rendering_frames = [<?php echo $column_shepherd_rendering_frames;?>];
            var column_shepherd_rendered_tiles = [<?php echo $column_shepherd_rendered_tiles;?>];

            function drawShepherdRenderingFramesChart() {
                stackedcolumnchart("Rendering frames", column_shepherd_rendering_frames, "shepherd_rendering_frames_div");
                stackedcolumnchart("Rendered frames", column_shepherd_rendered_tiles, "shepherd_rendered_tiles_div");
            }

            google.charts.setOnLoadCallback(drawCharts);

            function drawCharts() {
                drawMirrorUsageChart();

                drawShepherdRenderingFramesChart();
            }

            function initMap() {
                var zero_zero = {lat: 0, lng: 0};
                var map = new google.maps.Map(document.getElementById('map'), {zoom: 2, center: zero_zero});
                new google.maps.Marker({position: {lat: 51.0028863, lng: 2.1078047}, map: map, icon: {url: "https://maps.google.com/mapfiles/ms/icons/yellow-dot.png"}});
                <?php
                foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
                    $shepherdIdTrim = str_replace(array('.', '/', '-'), '_', MISC::humanServerHost($shepherd->getId()));
                    $position = explode(' ', $shepherd->getLocationGeographic(), 2);
                    echo 'new google.maps.Marker({position: {lat: '.$position[0].', lng: '.$position[1].'}, map: map, icon: {url: "https://maps.google.com/mapfiles/ms/icons/blue-dot.png"}});';
                    echo "\n";
                }
                ?>
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMisrD7A3ynQD0wcsGfg1ZWOwc-Z1z9EU&callback=initMap">
        </script>

        <?php
        echo '</div>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}
