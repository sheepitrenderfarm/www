<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\TileRepository;
use App\UI\HTML;
use App\Utils\Misc;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolFrames extends AdminToolBase implements AdminToolInterface {
    private UrlGeneratorInterface $router;
    private HTML $html;
    private TileRepository $frameRepository;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, TileRepository $frameRepository) {
        parent::__construct($security);
        $this->router = $router;
        $this->html = $html;
        $this->frameRepository = $frameRepository;
    }

    public function getTitle(): string {
        return 'Last frames';
    }

    public function getIcon(): string {
        return 'layer-group';
    }

    public function show(): void {
        $n = 50;

        $tiles = $this->frameRepository->getLastRendered($n);

        $this->html->printBreadcrumbs(sprintf('Last %d rendered frames', $n), array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<div id="see_frames" class="four_columns">';
        foreach ($tiles as $tile) {
            echo '<div class="col-md-3">';
            echo '<div class="w-box frames" style="border: 0px">';
            echo '<a href="'.$tile->getUrlFullOnShepherd().'" rel="gallery" data-fancybox-type="image" class="tfancybox">';
            echo '<img style="border: 1px solid black;" data-src="'.$tile->getUrlThumbnailOnShepherd(true).'" alt="" title="" align="middle" /></a>';
            echo '<br>';
            echo 'validated on: '.$this->html->getTimeWithLocalTimeZone('H:i.s M d', $tile->getValidationTime()).'<br>';
            echo 'render time: '.Misc::humanTime($tile->getRenderTime(), true).'<br>';
            echo 'project: '.'<a href="'.$this->router->generate("app_project_manage", ['project' => $tile->getFrame()->getProject()->getId()]).'">'.$tile->getFrame()->getProject()->getName().'</a><br>';
            echo 'rendered by: ';
            $this->html->printOwnerLink($tile->getUser()->getId());
            echo '<br>';
            echo '</div>';
            echo '</div>';
        }
        echo '</div>'; // four_columns

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}