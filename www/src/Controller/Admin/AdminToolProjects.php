<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Constant;
use App\Repository\ProjectRepository;
use App\UI\HTML;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

class AdminToolProjects extends AdminToolBase implements AdminToolInterface {

    private ProjectRepository $projectRepository;
    private UrlGeneratorInterface $router;
    private HTML $html;

    public function __construct(Security $security, HTML $html, UrlGeneratorInterface $router, ProjectRepository $projectRepository) {
        parent::__construct($security);
        $this->projectRepository = $projectRepository;
        $this->router = $router;
        $this->html = $html;
    }

    public function getTitle(): string {
        return 'Projects';
    }

    public function getIcon(): string {
        return 'cube';
    }

    public function show(): void {
        $projects = $this->projectRepository->findAll();
        ksort($projects);

        $this->html->printBreadcrumbs(sprintf('Projects (%d)', count($projects) - 2), array(
            array('title' => 'Admin', 'url' => $this->router->generate("app_admin_main")),
        ));
        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        ?>
        <table class="table table-bordered table-striped table-comparision table-responsive sortable">
        <thead>
        <tr>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">ID</a></th>
            <th style="text-align: center; vertical-align: middle;" style="overflow-wrap: anywhere;">Name</a></th>
            <th style="text-align: center; vertical-align: middle;">Owner</a></th>
            <th style="text-align: center; vertical-align: middle;">Status</a></th>
            <th style="text-align: center; vertical-align: middle;">Done/Rendering/Waiting</a></th>
            <th style="text-align: center; vertical-align: middle;"><a class="sortable" href="#" data-default-order="desc" data-type="int">Creation</a></th>
            <?php
            if ($this->user->isAdmin()) {
                echo '<th style="text-align: center; vertical-align: middle;">Action</a></th>';
            }
            ?>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($projects as $project) {
            if ($project->shouldHideFromUser()) {
                continue;
            }
            echo '<tr>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            echo $project->getId();
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            echo '<a href="'.$this->router->generate("app_project_manage", ['project' => $project->getId()]).'">'.$project->getName().'</a>';
            echo '</td>';

            echo '<td class="table_avatar" data-sort="'.$project->getOwner()->getId().'">';
            $this->html->printAvatar($project->getOwner()->getId());
            echo '<span style="display: none;">'.$project->getOwner()->getId().'</span> <a href="'.$this->router->generate('app_user_profile', ['user' => $project->getOwner()->getId()]).'" >'.$project->getOwner()->getId().'</a></td>';

            $actual_status = $project->getStatus();
            $human = $this->html->humanStatus($actual_status);
            echo '<td style="text-align: center; vertical-align: middle;" class="msg_'.$human['style'].'">';
            echo $human['text'];
            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle;">';
            $frames_status = $project->getCachedStatistics();
            $status_s = array(Constant::FINISHED, Constant::PROCESSING, Constant::WAITING);
            $frames_status2 = array();

            foreach ($status_s as $status) {
                if (array_key_exists($status, $frames_status)) {
                    $frames_status2 [] = '<span class="msg_'.$this->html->status2CSSstyle($status).'">'.$frames_status[$status].'</span>';
                }
                else {
                    $frames_status2 [] = '0';
                }
            }
            echo implode('/', $frames_status2);

            echo '</td>';

            echo '<td style="text-align: center; vertical-align: middle;" data-sort="'.$project->getLastUpdateArchive().'">';
            echo $this->html->getTimeWithLocalTimeZone('H:i.s d-m-Y', $project->getLastUpdateArchive());
            echo '</td>';

            if ($this->user->isAdmin()) {
                echo '<td style="text-align: center; vertical-align: middle;">';

                echo '<input type="button" class="btn btn-danger" onclick="requestReloadOnSuccess(\''.$this->router->generate('app_project_remove_no_redirect2', ['project' => $project->getId()]).'\'); return false" value="Delete" />';
                if ($actual_status == Constant::PAUSED) {
                    echo '<input type="button" class="btn btn-success" onclick="requestReloadOnSuccess(\''.$this->router->generate('app_project_resume', ['project' => $project->getId()]).'\'); return false" value="Resume" />';
                }
                else {
                    if ($actual_status == Constant::WAITING || $actual_status == Constant::PROCESSING) {
                        echo '<input type="button"  class="btn btn-warning" onclick="requestReloadOnSuccess(\''.$this->router->generate('app_project_pause', ['project' => $project->getId()]).'\'); return false" value="Pause" />';
                    }
                }

                echo '</td>';

            }
            echo '</tr>';
        }

        echo '</table>';

        echo '</section>';
        echo '</div>';
        echo '</div>';
    }
}