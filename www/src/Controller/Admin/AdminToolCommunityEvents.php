<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Admin;

use App\Repository\StatsGlobalRepository;
use DateTime;
use Symfony\Component\Security\Core\Security;

class AdminToolCommunityEvents extends AdminToolBase implements AdminToolInterface {
    private StatsGlobalRepository $statsGlobalRepository;

    public function __construct(Security $security, StatsGlobalRepository $statsGlobalRepository) {
        parent::__construct($security);
        $this->statsGlobalRepository = $statsGlobalRepository;
    }

    public function getTitle(): string {
        return "Event prediction";
    }

    public function getIcon(): string {
        return 'compass';
    }

    public function show(): void {
        $current_stat = $this->statsGlobalRepository->findOneBy([], ['id' => 'DESC']);

        $window = 30 * 24 * 3600;

        $last_stat = $this->statsGlobalRepository->findOneBetween(time() - $window - 24 * 3600, time() - $window);
        if (is_null($last_stat)) {
            echo '<div class="container">';

            echo '<h1>Possible events to thank the community</h1>';
            echo 'No stats available yet';
            return;
        }

        $events = array();

        $o = new CommunityEventFrame($current_stat, $last_stat, 80000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 90000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 100000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 150000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 200000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 300000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 400000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventFrame($current_stat, $last_stat, 500000000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 500 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 600 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 700 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 750 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 800 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 900 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 1000 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 1500 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 2000 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventTime($current_stat, $last_stat, 3000 * 365 * 24 * 3600);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventProject($current_stat, $last_stat, 300000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventProject($current_stat, $last_stat, 500000);
        $events[$o->prediction()] = $o;

        $o = new CommunityEventProject($current_stat, $last_stat, 1000000);
        $events[$o->prediction()] = $o;

        ksort($events);

        echo '<section class="slice animate-hover-slide color-three">';
        echo '<div class="w-section inverse">';
        echo '<div class="container">';

        echo '<h1>Possible events to thank the community</h1>';

        echo '<table class="table table-bordered table-striped table-responsive" style="margin-left: auto; margin-right: auto;width:60%;" border="0">';
        echo '<tr><th style="text-align: center; vertical-align: middle;">Event</th><th style="text-align: center; vertical-align: middle;">Prediction</th></tr>';
        foreach ($events as $time => $event) {
            if (time() < $time) {
                echo '<tr>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                echo $event->humanTitle();
                echo '</td>';

                echo '<td style="text-align: center; vertical-align: middle;">';
                $date = new DateTime();
                $date->setTimestamp($time);
                echo $date->format('F jS, Y');
                echo '</td>';
                echo '</tr>';
            }
        }
        echo '</table>';
        echo 'Current: <br>';
        echo 'rendered frames: '.number_format($current_stat->getFramesOrdered() / 1000000).' million frame<br>';
        echo 'rendered time: '.sprintf("%s years of rendering", number_format($current_stat->getTimeOrdered() / (365 * 24 * 3600))).'<br>';
        echo 'projects created: '.number_format($current_stat->getProjectsCreated());
        echo '<br>';
        echo '</div>';
        echo '</section>';
        echo '</div>';
        echo '</div>';
    }

}