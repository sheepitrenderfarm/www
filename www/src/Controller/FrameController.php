<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Frame;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Frame controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/frame')]
class FrameController extends BaseController {
    #[Route(path: '/{frame}/tiles')]
    public function tiles(Frame $frame): Response {
        if ($frame->getProject()->canSeeProject($this->getUser()) == false) {
            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'You are not authorized to see this project',
            ]);
        }
        else {
            return $this->render('frame/all_tiles.html.twig', [
                    'frame' => $frame,
                ]
            );
        }
    }
}