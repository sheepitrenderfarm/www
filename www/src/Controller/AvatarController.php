<?php

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\User;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle avatar
 */
#[Route(path: '/media/image/avatar')]
class AvatarController extends BaseController {

    #[Route(path: '/{size}/{user}.png', requirements: ['size' => 'small|big'], methods: 'GET')]
    public function getFile(string $size, User $user): BinaryFileResponse {
        $path = $this->config['storage']['path'].'users/avatars/'.$size.'/'.$user->getId().'.png';
        if (file_exists($path) == false) {
            $path = __DIR__.'/../../public/media/image/avatar_default_'.$size.'.png';
        }

        $headers = [];
        // if it's user own avatar => no cache
        if (is_object($this->getUser()) && $user->getId() == $this->getUser()->getUserIdentifier()) {
            $headers['Cache-Control'] = 'no-cache';
        }

        return new BinaryFileResponse($path, 200, $headers);
    }
}