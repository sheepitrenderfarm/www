<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\GPU;
use App\Entity\User;
use App\Repository\CPURepository;
use App\Repository\GPURepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Rank controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/ranking')]
class RankingController extends BaseController {
    private CPURepository $CPURepository;
    private GPURepository $GPURepository;

    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        Main $main,
        HTML $html,
        CPURepository $CPURepository,
        GPURepository $GPURepository
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->CPURepository = $CPURepository;
        $this->GPURepository = $GPURepository;
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/machine')]
    public function machine(): Response {
        $user_cpus = array();
        $user_gpus = array();

        foreach ($this->getUser()->sessions() as $s) {
            if (is_object($s->getCpu())) {
                $user_cpus [] = $s->getCpu()->getId();
            }
            if (is_object($s->getGPU())) {
                $user_gpus [] = $s->getGPU()->getId();
            }
        }

        $cpus = $this->CPURepository->findAll();
        $gpus = [];
        foreach ($this->GPURepository->findAll() as $gpu) {
            if ($gpu->getType() == GPU::TYPE_OPTIX) {
                $found = false;
                foreach ($gpus as $k2 => $gpu2) {
                    if ($gpu2->getRenderedFrames() != 0 && $gpu->getModel() == $gpu2->getModel() && ($gpu->getRenderedFrames() + $gpu2->getRenderedFrames() > 0)) {
                        $found = true;
                        $gpu2->setPower(($gpu->getRenderedFrames() * $gpu->getPower() + $gpu2->getRenderedFrames() * $gpu2->getPower()) / ($gpu->getRenderedFrames() + $gpu2->getRenderedFrames()));
                        $gpu2->setRenderedFrames($gpu->getRenderedFrames() + $gpu2->getRenderedFrames());
                        break;
                    }
                }
                if ($found == false) {
                    $gpus [] = $gpu;
                }
            }
        }

        usort($gpus, '\App\Utils\Sort::sortPower');
        usort($cpus, '\App\Utils\Sort::sortPower');

        $cpus_twig = [];
        $gpus_twig = [];

        foreach ($cpus as $cpu) {
            if ($cpu->getPower() == 0 || $cpu->getRenderedFrames() == 0) {
                continue;
            }

            $cpus_twig [] = [
                'id' => $cpu->getId(),
                'is_mine' => in_array($cpu->getId(), $user_cpus),
                'description' => $cpu->description(),
                'power' => 100 * $cpu->getPower() / $this->config['power']['cpu']['const'],
                'power_admin' => 100 * Misc::powerAlignCpu($cpu->getPower()) / $this->config['power']['cpu']['const'],
            ];
        }

        foreach ($gpus as $gpu) {
            if ($gpu->getPower() == 0 || $gpu->getRenderedFrames() == 0) {
                continue;
            }

            $gpus_twig [] = [
                'id' => $gpu->getId(),
                'is_mine' => in_array($gpu->getId(), $user_gpus),
                'description' => $gpu->description(),
                'memory_GB' => number_format($gpu->getMemory() / (1024.0 * 1024.0 * 1024.0)),
                'memory_int' => $gpu->getMemory(),
                'power' => 100 * $gpu->getPower() / $this->config['power']['gpu']['const'],
                'power_admin' => 100 * Misc::powerAlignGpu($gpu->getPower()) / $this->config['power']['gpu']['const'],
            ];

        }

        return $this->render('ranking/machine.html.twig', [
            'cpus' => $cpus_twig,
            'gpus' => $gpus_twig,
            'percent_slow_machine' => intval(100.0 * (float)($this->config['power']['threshold_slow_computer']) / (float)($this->config['power']['cpu']['const'])),
            'percent_very_slow_machine' => intval(100.0 * (float)($this->config['power']['threshold_very_slow_computer']) / (float)($this->config['power']['cpu']['const'])),
        ]);
    }

    #[Route(path: '/user')]
    public function user(): Response {
        $users = [];

        $i = 1;
        foreach ($this->main->getBestRenderersFromCache() as $obj) {
            if ($obj->getLogin() == User::REMOVED_USER_ID) {
                continue;
            }

            $users [] = [
                'id' => $obj->getLogin(),
                'rank' => $i,
                'points' => $obj->getPointsEarn(),
                'frames' => $obj->getRenderedFrames(),
                'time' => $obj->getRenderTime(),
            ];
            $i++;
        }

        return $this->render('ranking/user.html.twig', [
            'users' => $users,
        ]);
    }
}
