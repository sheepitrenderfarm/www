<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Controller\Admin\AdminToolAdventCalendar;
use App\Controller\Admin\AdminToolAwards;
use App\Controller\Admin\AdminToolCache;
use App\Controller\Admin\AdminToolCommunityEvents;
use App\Controller\Admin\AdminToolConfig;
use App\Controller\Admin\AdminToolEfficiency;
use App\Controller\Admin\AdminToolErrors;
use App\Controller\Admin\AdminToolFrames;
use App\Controller\Admin\AdminToolGifts;
use App\Controller\Admin\AdminToolHostStats;
use App\Controller\Admin\AdminToolHWID;
use App\Controller\Admin\AdminToolInterface;
use App\Controller\Admin\AdminToolLog;
use App\Controller\Admin\AdminToolMisc;
use App\Controller\Admin\AdminToolProjects;
use App\Controller\Admin\AdminToolResponseTime;
use App\Controller\Admin\AdminToolServers;
use App\Controller\Admin\AdminToolShepherdsHealth;
use App\Controller\Admin\AdminToolTasks;
use App\Controller\Admin\AdminToolTasksOnShepherdsSumup;
use App\Controller\Admin\AdminToolUsersMessages;
use App\Controller\Admin\AdminToolUsersRegistration;
use App\Entity\User;
use App\Repository\ShepherdServerRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Admin controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/fulladmin')]
class AdminController extends BaseController {
    /**
     * @var array<string, AdminToolInterface>
     */
    private array $actions;
    private ShepherdServerRepository $shepherdServerRepository;

    private AdminToolProjects $adminToolProjects;
    private AdminToolFrames $adminToolFrames;
    private AdminToolUsersRegistration $adminToolUsersRegistration;
    private AdminToolCache $adminToolCache;
    private AdminToolResponseTime $adminToolResponseTime;
    private AdminToolGifts $adminToolGifts;
    private AdminToolLog $adminToolLog;
    private AdminToolUsersMessages $adminToolUsersMessages;
    private AdminToolEfficiency $adminToolEfficiency;
    private AdminToolServers $adminToolServers;
    private AdminToolMisc $adminToolMisc;
    private AdminToolErrors $adminToolErrors;
    private AdminToolTasks $adminToolTasks;
    private AdminToolTasksOnShepherdsSumup $adminToolTasksOnShepherdsSumup;
    private AdminToolAwards $adminToolAwards;
    private AdminToolCommunityEvents $adminToolCommunityEvents;
    private AdminToolHostStats $adminToolHostStats;
    private AdminToolConfig $adminToolConfig;
    private AdminToolAdventCalendar $adventCalendar;
    private AdminToolHWID $adminToolHWIDBlacklist;
    private AdminToolShepherdsHealth $adminToolShepherdsHealth;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $router,
        Main $main,
        HTML $html,
        ConfigService $configService,

        AdminToolProjects $adminToolProjects,
        AdminToolFrames $adminToolFrames,
        AdminToolUsersRegistration $adminToolUsersRegistration,
        AdminToolCache $adminToolCache,
        AdminToolResponseTime $adminToolResponseTime,
        AdminToolGifts $adminToolGifts,
        AdminToolLog $adminToolLog,
        AdminToolUsersMessages $adminToolUsersMessages,
        AdminToolEfficiency $adminToolEfficiency,
        AdminToolServers $adminToolServers,
        AdminToolShepherdsHealth $adminToolShepherdsHealth,
        AdminToolMisc $adminToolMisc,
        AdminToolErrors $adminToolErrors,
        AdminToolTasks $adminToolTasks,
        AdminToolTasksOnShepherdsSumup $adminToolTasksOnShepherdsSumup,
        AdminToolAwards $adminToolAwards,
        AdminToolCommunityEvents $adminToolCommunityEvents,
        AdminToolHostStats $adminToolHostStats,
        AdminToolConfig $adminToolConfig,
        AdminToolAdventCalendar $adventCalendar,
        AdminToolHWID $adminToolHWIDBlacklist,
        ShepherdServerRepository $shepherdServerRepository,
    ) {

        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->shepherdServerRepository = $shepherdServerRepository;

        $this->adminToolProjects = $adminToolProjects;
        $this->adminToolFrames = $adminToolFrames;
        $this->adminToolUsersRegistration = $adminToolUsersRegistration;
        $this->adminToolCache = $adminToolCache;
        $this->adminToolResponseTime = $adminToolResponseTime;
        $this->adminToolGifts = $adminToolGifts;
        $this->adminToolLog = $adminToolLog;
        $this->adminToolUsersMessages = $adminToolUsersMessages;
        $this->adminToolEfficiency = $adminToolEfficiency;
        $this->adminToolServers = $adminToolServers;
        $this->adminToolShepherdsHealth = $adminToolShepherdsHealth;
        $this->adminToolMisc = $adminToolMisc;
        $this->adminToolErrors = $adminToolErrors;
        $this->adminToolTasks = $adminToolTasks;
        $this->adminToolTasksOnShepherdsSumup = $adminToolTasksOnShepherdsSumup;
        $this->adminToolAwards = $adminToolAwards;
        $this->adminToolCommunityEvents = $adminToolCommunityEvents;
        $this->adminToolHostStats = $adminToolHostStats;
        $this->adminToolConfig = $adminToolConfig;
        $this->adminToolHWIDBlacklist = $adminToolHWIDBlacklist;
        $this->adventCalendar = $adventCalendar;
    }

    private function isAllowed(string $route): bool {
        return array_key_exists($route, $this->actions);
    }

    private function buildAllowedActions(): void {
        $mirror_owners = array();
        foreach ($this->shepherdServerRepository->findAll() as $s) {
            $mirror_owners [] = $s->getOwner()->getId();
        }

        $this->actions = [];
        if ($this->getUser()->isModerator()) {
            $this->actions['app_admin_projects'] = $this->adminToolProjects;
            $this->actions['app_admin_frames'] = $this->adminToolFrames;
            $this->actions['app_admin_users_registration'] = $this->adminToolUsersRegistration;
            if ($this->getUser()->isAdmin()) {
                $this->actions['app_admin_persistentcache'] = $this->adminToolCache;
                $this->actions['app_admin_responsetime'] = $this->adminToolResponseTime;
                $this->actions['app_admin_gifts'] = $this->adminToolGifts;
                $this->actions['app_admin_log'] = $this->adminToolLog;
            }
            $this->actions['app_admin_users_messages'] = $this->adminToolUsersMessages;
            $this->actions['app_admin_hwid'] = $this->adminToolHWIDBlacklist;
            $this->actions['app_admin_efficiency'] = $this->adminToolEfficiency;
            $this->actions['app_admin_mirrors'] = $this->adminToolServers;
            $this->actions['app_admin_misc_chart'] = $this->adminToolMisc;
            $this->actions['app_admin_errors'] = $this->adminToolErrors;
            $this->actions['app_admin_tasks'] = $this->adminToolTasks;
            $this->actions['app_admin_tasks_shepherd'] = $this->adminToolTasksOnShepherdsSumup;
            $this->actions['app_admin_shepherds_health'] = $this->adminToolShepherdsHealth;
            $this->actions['app_admin_awards'] = $this->adminToolAwards;
            $this->actions['app_admin_event_prediction'] = $this->adminToolCommunityEvents;
            $this->actions['app_admin_host'] = $this->adminToolHostStats;
            $this->actions['app_admin_config'] = $this->adminToolConfig;
            $this->actions['app_admin_advent_calendar'] = $this->adventCalendar;
        }
        elseif (in_array($this->getUser()->getId(), $mirror_owners)) {
            $this->actions['app_admin_host'] = $this->adminToolHostStats;
            $this->actions['app_admin_mirrors'] = $this->adminToolServers;
            $this->actions['app_admin_tasks_shepherd'] = $this->adminToolTasksOnShepherdsSumup;
            $this->actions['app_admin_shepherds_health'] = $this->adminToolShepherdsHealth;
        }
    }

    private function handle(string $route): Response {
        $this->buildAllowedActions();
        if ($this->isAllowed($route)) {
            $action = $this->actions[$route];
            $twig_file = substr($route, strlen('app_admin_'));
            return $this->render(
                'admin/sub/'.$twig_file.'.html.twig',
                [
                    'title' => $action->getTitle(),
                ]
            );
        }
        else {
            return new Response('', 401);
        }
    }

    #[Route(path: '')]
    public function main(): Response {
        $this->buildAllowedActions();

        return $this->render(
            'admin/fulladmin.html.twig',
            [
                'header_default_container' => true,
                'actions' => $this->actions,
            ]
        );
    }

    #[Route(path: '/mirrors')]
    public function mirrors(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[Route(path: '/shepherds_health')]
    public function shepherds_health(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/projects')]
    public function projects(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/frames')]
    public function frames(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/users_messages')]
    public function users_messages(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/users_registration')]
    public function users_registration(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/hwid')]
    public function hwid(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/persistentcache')]
    public function persistentcache(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/log')]
    public function log(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/gifts')]
    public function gifts(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route(path: '/responsetime')]
    public function responsetime(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/efficiency')]
    public function efficiency(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/misc_chart')]
    public function misc_chart(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/errors')]
    public function errors(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/tasks')]
    public function tasks(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[Route(path: '/tasks_shepherd')]
    public function tasks_shepherd(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/awards')]
    public function awards(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/event_prediction')]
    public function event_prediction(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[Route(path: '/host')]
    public function host(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[Route(path: '/config')]
    public function config(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[Route(path: '/advent_calendar')]
    public function advent_calendar(Request $request): Response {
        return $this->handle($request->attributes->get('_route'));
    }

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/give_award/{user}/{award}')]
    public function give_award(User $user, string $award): Response {
        $user->giveAward($award);
        return new Response('OK');
    }
}