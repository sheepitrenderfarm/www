<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Repository\ShepherdServerRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Shepherd controller
 */
#[Route(path: '/shepherd')]
class ShepherdController extends BaseController {
    private ShepherdServerRepository $shepherdServerRepository;

    public function __construct(
        Main $main,
        HTML $html,
        UrlGeneratorInterface $router,
        ConfigService $configService,
        EntityManagerInterface $entityManager,
        ShepherdServerRepository $shepherdServerRepository
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->shepherdServerRepository = $shepherdServerRepository;
    }

    /**
     * Reset a task on a shepherd
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{shepherdId}/task/reset/{task}')]
    public function task_reset(string $shepherdId, int $task): Response {
        $shepherd = $this->shepherdServerRepository->find(urldecode($shepherdId));
        if (is_object($shepherd)) {
            $shepherd->taskReset($task);
            return new Response('OK');
        }
        else {
            return new Response('Shepherd not found');
        }
    }

    /**
     * Change enable status
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{shepherdId}/enable/{status}')]
    public function update_enable(string $shepherdId, bool $status): Response {
        $shepherd = $this->shepherdServerRepository->find(urldecode($shepherdId));
        if (is_object($shepherd)) {
            $shepherd->setEnable($status);
            $this->entityManager->flush($shepherd);

            $this->main->getDiscordWebHook()->shepherdEnable($shepherd->getId().'/admin/status', $shepherd->getEnable(), $this->getUser()->getId());

            return new Response('OK');
        }
        else {
            return new Response('Shepherd not found');
        }
    }

    /**
     * Get the shepherd version
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{shepherdId}/version', methods: ['POST', 'GET'])]
    public function version(string $shepherdId): Response {
        $shepherd = $this->shepherdServerRepository->find(urldecode(urldecode($shepherdId)));
        if (is_object($shepherd)) {
            $version = $shepherd->getVersion();
            $html = '<span style="color:'.($version == $this->getLatestDeployedOnGitlab() ? 'green' : 'red').'">';
            $html .= $version;
            $html .= '</span>';
            return new Response($html);
        }
        else {
            return new Response('Shepherd not found ');
        }
    }

    /**
     * Change allow new project status
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{shepherdId}/allow_new_project/{allow}')]
    public function update_allow_new_project(string $shepherdId, bool $allow): Response {
        $shepherd = $this->shepherdServerRepository->find(urldecode($shepherdId));
        if (is_object($shepherd)) {
            $shepherd->setAllowNewBlend($allow);
            $this->entityManager->flush($shepherd);

            $this->main->getDiscordWebHook()->shepherdPause($shepherd->getId(), $shepherd->getAllowNewBlend(), $this->getUser()->getId());

            return new Response('OK');
        }
        else {
            return new Response('Shepherd not found');
        }
    }


    private function getLatestDeployedOnGitlab(): string {
        $data = file_get_contents('https://gitlab.com/api/v4/projects/20825965/repository/tags');
        if (is_string($data)) {
            $json = json_decode($data, true);
            if (is_array($json) && array_key_exists('0', $json) && array_key_exists('name', $json[0])) {
                return $json[0]['name'];
            }
        }
        return "";
    }

}