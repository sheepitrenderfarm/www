<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Get started controller
 */
#[Route(path: '/getstarted')]
class GetStartedController extends BaseController {

    #[Route(path: '')]
    public function main(): Response {
        return $this->render('getstarted/getstarted.html.twig', [
                'client_windows' => file_exists(__DIR__.'/../../public/'.$this->config['client']['binary']['stable']['path']),
                'client_apple' => file_exists(__DIR__.'/../../public/'.$this->config['client']['mac']['stable']['path']),
                'client_java' => file_exists(__DIR__.'/../../public/'.$this->config['client']['jar']['stable']['path']),
            ]
        );
    }
}
