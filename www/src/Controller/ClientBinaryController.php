<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Auhtor Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace App\Controller;

use App\Base\BaseController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Download of jar and exe client
 */
#[Route(path: '/media/applet')]
class ClientBinaryController extends BaseController {
    #[Route(path: '/client-binary-latest.php', methods: 'GET')]
    public function clientExeLatest(): BinaryFileResponse {
        $path = __DIR__.'/../../public/'.$this->config['client']['binary']['stable']['path'];

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($path)
        );

        return $response;
    }

    #[Route(path: '/client-binary-beta.php', methods: 'GET')]
    public function clientExeBeta(): BinaryFileResponse {
        $path = __DIR__.'/../../public/'.$this->config['client']['binary']['beta']['path'];

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($path)
        );

        return $response;
    }

    #[Route(path: '/client-latest.php', methods: 'GET')]
    public function clientJarLatest(): BinaryFileResponse {
        $path = __DIR__.'/../../public/'.$this->config['client']['jar']['stable']['path'];

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($path)
        );

        return $response;
    }

    #[Route(path: '/client-mac-latest.php', methods: 'GET')]
    public function clientMacLatest(): BinaryFileResponse {
        $path = __DIR__.'/../../public/'.$this->config['client']['mac']['stable']['path'];

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($path)
        );

        return $response;
    }

    #[Route(path: '/client-beta.php', methods: 'GET')]
    public function clientJarBeta(): BinaryFileResponse {
        $path = __DIR__.'/../../public/'.$this->config['client']['jar']['beta']['path'];

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($path)
        );

        return $response;
    }

    #[Route(path: '/client-info.php', methods: 'GET')]
    public function clientInfo(Request $request): Response {
        $arg = $request->get('get');
        if ($arg == 'md5') {
            $path = __DIR__.'/../../public/'.$this->config['client']['jar']['stable']['path'];

            return new Response(md5_file($path));
        }
        elseif ($arg == 'version') {
            return new Response($this->config['client']['jar']['stable']['version']);
        }
        else {
            return new Response('', 400);
        }
    }
}