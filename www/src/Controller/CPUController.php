<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\CPU;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * CPU controller
 */
#[Route(path: '/cpu')]
class CPUController extends BaseController {
    /**
     * Reset power on a cpu
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{cpu}/reset')]
    public function reset(CPU $cpu): Response {
        $cpu->setRenderedFrames(0);
        $cpu->setPower(0);

        $this->entityManager->flush($cpu);

        return new Response('OK');
    }
}
