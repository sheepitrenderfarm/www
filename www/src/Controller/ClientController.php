<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Auhtor Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\CDN\CDNInternal;
use App\Constant;
use App\Entity\Event;
use App\Entity\GPU;
use App\Entity\Session;
use App\Entity\SessionError;
use App\Entity\Tile;
use App\Repository\BlenderArchRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Scheduler\ClientError;
use App\Scheduler\ClientProtocol;
use App\Scheduler\ClientProtocol60;
use App\Scheduler\ClientProtocolService;
use App\Service\ConfigService;
use App\Service\Image;
use App\Service\Logger;
use App\Service\Main;
use App\Service\RequestTimeProfiler;
use App\UI\HTML;
use App\Utils\Misc;
use App\Utils\Network;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use TypeError;

#[Route(path: '/server')]
class ClientController extends BaseController {

    private SessionRepository $sessionRepository;
    private ProjectRepository $projectRepository;
    private BlenderArchRepository $blenderArchRepository;
    private TileRepository $tileRepository;
    private ClientProtocolService $clientProtocolService;
    private RequestTimeProfiler $requestTimeProfiler;

    public function __construct(
        Main $main,
        HTML $html,
        UrlGeneratorInterface $router,
        SessionRepository $sessionRepository,
        ProjectRepository $projectRepository,
        TileRepository $tileRepository,
        BlenderArchRepository $blenderArchRepository,
        EntityManagerInterface $entityManager,
        ClientProtocolService $clientProtocolService,
        ConfigService $configService,
        RequestTimeProfiler $requestTimeProfiler
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->tileRepository = $tileRepository;
        $this->sessionRepository = $sessionRepository;
        $this->projectRepository = $projectRepository;
        $this->blenderArchRepository = $blenderArchRepository;
        $this->clientProtocolService = $clientProtocolService;
        $this->requestTimeProfiler = $requestTimeProfiler;
    }

    #[Route(path: '/config.php', methods: 'POST')]
    public function config(Request $request): Response {
        $start = microtime(true);
        $requestArray = $request->request->all();

        if ($request->request->has('version') == false) {
            return new Response($this->clientProtocolService->getHandler(ClientProtocol60::MINIMUM_VERSION, null, $request->getSession())->configOutput(ClientProtocol::CONFIGURATION_ERROR_NO_CLIENT_VERSION_GIVEN, null), 200, ['Content-Type' => 'text/xml']);
        }

        $handler = $this->clientProtocolService->getHandler($requestArray['version'], null, $request->getSession());
        $error_state = ClientProtocol::OK;
        $publickey_id = null;
        $new_session = null;
        if (array_key_exists('version', $requestArray) == false) {
            $error_state = ClientProtocol::CONFIGURATION_ERROR_NO_CLIENT_VERSION_GIVEN;
        }
        else {
            if ($this->clientProtocolService->isSupported($requestArray['version'], $requestArray['os']) == false) {
                $error_state = ClientProtocol::CONFIGURATION_ERROR_CLIENT_TOO_OLD;
            }
        }
        if ($error_state == ClientProtocol::OK) {
            list($error_state, $user, $publickey_id) = $handler->userAuth($requestArray);
            if ($error_state == ClientProtocol::OK) {
                $error_state = $handler->configInput($user, $publickey_id, $requestArray);
                if ($error_state == ClientProtocol::OK) {
                    $new_session = $handler->getSession();
                    $request->getSession()->set('session', $new_session->getId());
                    $error_state = ClientProtocol::OK;
                }
            }
        }

        if ($error_state == ClientProtocol::OK && is_object($new_session)) {
            $e = new Event();
            $e->setType(Event::TYPE_LOGIN);
            $e->setTime(time());
            $e->setUser($new_session->getUser()->getId());
            $e->setSession($new_session->getId());
            $e->setData($new_session->getVersion());
            $this->entityManager->persist($e);

            // update last_session
            $user = $handler->getSession()->getUser();
            $user->setLastSession(time());

            // IP
            $new_session->setIP($request->getClientIp());
            $new_session->getUser()->setIPSession($request->getClientIp());

            // for country flag
            if (Network::isGoogleCloud($request->getClientIp())) {
                $new_session->setCountry('GO');
            }
            else {
                $data = Network::getLocationFromIP($request->getClientIp());
                if (is_array($data)) {
                    $path = __DIR__.'/../../public/media/image/flag/'.$data['country'].'.png';

                    if (file_exists($path)) {
                        $new_session->setCountry($data['country']);
                    }
                }
                else {
                    Logger::error("(server/config.php) failed to lookup country for ".$request->getClientIp().' (B)');
                }
            }
            $this->entityManager->flush();
        }

        $this->requestTimeProfiler->setTime(RequestTimeProfiler::CREATE_SESSION, 1000 * (microtime(true) - $start));
        return new Response($handler->configOutput($error_state, $publickey_id), 200, ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/request_job.php', methods: 'POST')]
    public function request_job(Request $request): Response {
        $start = microtime(true);

        $requestArray = $request->query->all();

        $my_session = null;
        $job = null;
        $frame = null;

        if ($request->getSession()->has('session')) {
            $my_session = $this->sessionRepository->find($request->getSession()->get('session'));
            if (is_object($my_session)) {
                $my_session->keepMeAlive();
                $status = ClientProtocol::OK;
            }
            else {
                $status = ClientProtocol::JOB_REQUEST_ERROR_DEAD_SESSION;
                Logger::error('(request_job) error user has no session (sessionid: '.$request->getSession()->get('session').')');
            }
        }
        else {
            $status = ClientProtocol::JOB_REQUEST_ERROR_DEAD_SESSION;
            Logger::error('(request_job) error no session found ($_SESSION: '.json_encode($request->getSession()).')');
        }

        $version = '';
        if (is_object($my_session)) {
            $version = $my_session->getVersion();
        }
        $handler = $this->clientProtocolService->getHandler($version, $my_session, $request->getSession());
        $frame = null;
        $remaining_frames = 0;
        $renderable_jobs = 0;
        if ($status == ClientProtocol::OK) {
            list($status, $frame, $remaining_frames, $renderable_jobs) = $handler->jobRequestInput($requestArray);
        }

        if ($status == ClientProtocol::JOB_REQUEST_NOJOB && is_object($my_session)) {
            $points_earned = 10;
            $my_session->setPoints($my_session->getPoints() + $points_earned);
            $user = $my_session->getUser();
            $user->setPoints($user->getPoints() + $points_earned);
            $user->setPointsEarn($user->getPointsEarn() + $points_earned);
            $this->entityManager->flush();
        }

        $end = microtime(true);
        $this->requestTimeProfiler->setTime(RequestTimeProfiler::REQUEST_JOB, 1000 * ($end - $start));
        return new Response($handler->jobRequestOutput($status, $remaining_frames, $renderable_jobs, $frame, $this->get_list_of_files_to_delete_from_input_xml()), headers: ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/speedtest.php', methods: 'POST')]
    public function speedtest(Request $request): Response {
        return new Response();
    }

    #[Route(path: '/keepmealive.php', methods: 'GET')]
    public function keepmealive(Request $request): Response {
        $requestArray = $request->query->all();

        $my_session = null;
        if ($request->getSession()->has('session')) {
            $my_session = $this->sessionRepository->find($request->getSession()->get('session'));
        }

        $version = '';
        if (is_object($my_session)) {
            $version = $my_session->getVersion();
        }
        $handler = $this->clientProtocolService->getHandler($version, $my_session, $request->getSession());

        $status = $handler->keepMeAliveInput($requestArray);

        return new Response($handler->keepMeAliveOutput($status), headers: ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/error.php', methods: 'POST')]
    public function error(Request $request): Response {
        /** @var ?Session $my_session */
        $my_session = null;
        $session = 'nosession';
        $login = 'nologin';

        if ($request->getSession()->has('session')) {
            $session = $request->getSession()->get('session');
            $my_session = $this->sessionRepository->find($session);
            if (is_object($my_session) == false) {
                $login = 'session-error';
            }
            else {
                $login = $my_session->getUser()->getId();
                $my_session->keepMeAlive();
            }
        }

        $version = '';
        if (is_object($my_session)) {
            $version = $my_session->getVersion();
        }
        $handler = $this->clientProtocolService->getHandler($version, $my_session, $request->getSession());

        $now = time();
        $dir_path = date('Y-m-d', $now);
        @mkdir($this->config['log']['error_dir'].'/'.$dir_path, 0755, true);
        $path = $this->config['log']['error_dir'].'/'.$dir_path.'/'.'error_'.$now.'_'.$session.'.log';

        list($status, $tile_id, $frame_number, $render_time, $memory_used, $type) = $handler->errorInput($request->query->all(), $request->files, $path);

        if ($status == ClientProtocol::OK) {
            $tile = $this->tileRepository->find($tile_id);

            if (is_object($my_session) && is_int($type)) { // no type -> no error :) ) {
                $e = new SessionError();
                $e->setTime($now);
                $e->setUser($login);
                $e->setSession($my_session->getId());
                if (is_object($tile)) {
                    $e->setProject($tile->getFrame()->getProject()->getId());
                }
                $e->setType($type);
                $this->entityManager->persist($e);
            }

            if (is_object($tile)) {
                if ($type == ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR) {
                    if (is_object($my_session)) {
                        Logger::warning('Disabling session of '.$my_session->getUser()->getId().' due to python load error (OS: '.$my_session->getOS().')');
                        $my_session->setBlocked(6); // disable the session
                    }
                }

                if ($type == ClientError::TYPE_CACHE_DIR_DELETED && is_object($my_session)) {
                    Logger::warning('Disabling session of '.$my_session->getUser()->getId().' due to cache-dir deleted error');
                    $my_session->setBlocked(11); // disable the session
                }

                if ($type == ClientError::TYPE_CURRENTLY_HEADLESS && is_object($my_session)) {
                    $my_session->setHeadless(true);
                }
                if ($tile->getStatus() == Constant::PROCESSING && is_object($my_session) && $tile->getUser()->getId() == $my_session->getUser()->getId()) {
                    Logger::debug('(error.php) User '.$my_session->getUser()->getId().' (session: '.$my_session->getId().') send an error, project:'.$tile->getFrame()->getProject()->getId());
                    if ($render_time > 0) {
                        $tile->setRenderTime($render_time);

                        $max_duration = max(1, (time() - $tile->getRequestTime()) * 1.20); // 20% of error merging

                        if ($tile->getRenderTime() < 0 || $tile->getRenderTime() > $max_duration) {
                            Logger::error(__METHOD__."A user try to fake a rendertime, render_time is bigger than actual time ($max_duration) reset to max_duration");
                            $tile->setRenderTime($max_duration);
                        }
                    }
                    $tile->reset(true);
                    $job = $tile->getFrame()->getProject();
                    if ($type == ClientError::TYPE_RENDERER_MISSING_LIBRARIES) {
                        $bans = $request->getSession()->get('ban-executable', []);
                        $bans[$job->getExecutable()->getId()] = $job->getExecutable()->getId(); // for the CriterionBanJobFromUserSendError
                        $request->getSession()->set('ban-executable', $bans);
                    }
                    else {
                        $job->addBanforCriterionBanJobFromUserSendError($request->getSession());
                        if (ClientError::shouldBeReportedOnJobError($type)) {
                            $job->setUserError($job->getUserError() + 1);
                        }

                        $block = ClientError::shouldBlockProject(file_get_contents($path));
                        if ($block > 0) {
                            $this->projectRepository->block($job, $block);
                        }

                        if ($type == ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY) {
                            $gpu = $my_session->getComputeDevice();
                            if (is_object($gpu) && $gpu instanceof GPU) {
                                $job->updateVramFailure($gpu->getMemory());
                            }
                        }
                        elseif ($type == ClientError::TYPE_RENDERER_OUT_OF_MEMORY) {
                            if ($memory_used == 0 && is_object($my_session->getCPU())) {
                                $job->updateMaxMemoryUsed($my_session->getMemoryAllowed2());
                            }
                            elseif ($memory_used != 0 && $memory_used < $my_session->getMemory() && $my_session->getVersion() != $this->config['client']['github_version']) {
                                $job->updateMaxMemoryUsed($memory_used);
                            }
                        }
                    }
                }
                elseif ($tile->isComputeMethod() && ClientError::shouldDisableTheSession($type)) {
                    if (is_object($my_session)) {
                        $my_session->setBlocked($type == ClientError::TYPE_DENOISING_NOT_SUPPORTED ? 8 : 99); // disable the session
                    }
                }
            }
        }

        $this->entityManager->flush();

        return new Response($handler->jobValidationOutput($status), headers: ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/chunk.php', methods: 'GET')]
    public function chunk(Request $request): Response {
        $start = microtime(true);
        $error_state = -1;
        $my_session = null;

        if ($request->getSession()->has('session')) {
            $my_session = $this->sessionRepository->find($request->getSession()->get('session'));
            if (is_object($my_session)) {
                $error_state = 0;
                $my_session->keepMeAlive();
            }
            else {
                $error_state = 1;
            }
        }

        if ($error_state == 0) {
            if ($request->query->has('chunk') == false) {
                $error_state = 2;
            }
            else {
                $part = $request->query->get('chunk');
                // TODO: sanitization ? ?

                // is it a project or a binary ?
                $project = $this->projectRepository->findFromChunk($part);
                $blenderArch = $this->blenderArchRepository->findFromChunk($part);
                if (is_object($project) || is_object($blenderArch)) {
                    // TODO: security...
                    $cdn = $my_session->getCDN(is_object($project) ? $project : $blenderArch);

                    $cdn->addDownload(filesize(is_object($project) ? $project->fullPathOfChunk($part) : $blenderArch->fullPathOfChunk($part)));
                    $this->requestTimeProfiler->setTime(RequestTimeProfiler::ARCHIVE_PROJECT, 1000 * (microtime(true) - $start));

                    $remote_part = is_object($blenderArch) ? $part : $project->getChunkStorageName($part);

                    if ($cdn->isLocal()) {
                        // internal/local
                        $local_path = $cdn->getTarget($remote_part);
                        try {
                            return new BinaryFileResponse($local_path);
                        }
                            /** @phpstan-ignore-next-line */
                        catch (TypeError $e) {
                            Logger::error(__METHOD__.' Exception TypeError for local_path '.$local_path.' file_exists: '.json_encode(file_exists($local_path)).' is_readable: '.json_encode(is_readable($local_path)));
                            return new Response('', 404);
                        }
                            /** @phpstan-ignore-next-line */
                        catch (FileNotFoundException $e) {
                            Logger::error(__METHOD__.' Exception FileNotFoundException for local_path '.$local_path);
                            return new Response('', 404);
                        }
                    }
                    else {
                        // external
                        return new RedirectResponse($cdn->getTarget($remote_part));
                    }
                }
            }
        }

        return new Response('', 404);
    }

    #[Route(path: '/binary.php', methods: 'GET')]
    public function binary(Request $request): Response {
        // TODO Remove once all client have migrated to the full chunk binary blender version
        $start = microtime(true);
        $error_state = -1;
        /** @var ?Session $my_session */
        $my_session = null;

        Logger::debug(__METHOD__.':'.__LINE__.' ');

        if ($request->getSession()->has('session')) {
            $my_session = $this->sessionRepository->find($request->getSession()->get('session'));
            if (is_object($my_session)) {
                $error_state = 0;
                $my_session->keepMeAlive();
            }
            else {
                $error_state = 1;
            }
        }

        if ($error_state == 0) {
            if ($request->query->has('job') == false) {
                $error_state = 2;
            }
            else {
                $job_id = $request->query->get('job');
                $tile = $this->tileRepository->find($job_id); // for backward compatibility it's actually the tile uid
                if (is_object($tile)) {
                    $project = $tile->getFrame()->getProject();
                    $blenderArch = $this->blenderArchRepository->getFor($my_session, $project);
                    if (is_null($blenderArch)) {
                        Logger::error(__METHOD__." (10) Failed to get renderer");
                        return new Response('', 404);
                    }

                    $filename = $blenderArch->getPath();
                    $path = $this->config['storage']['path'].'binaries/'.$filename;

                    if ($request->getSession()->has('download_binary')) {
                        $download_binary = json_decode($request->getSession()->get('download_binary'), true);
                    }
                    else {
                        $download_binary = [];
                    }

                    if (array_key_exists($project->getId(), $download_binary) && $download_binary[$project->getId()] > 0) {
                        // already try to download => most likely a retry so cdn has a failure
                        // -> fallback to internal
                        $h = new CDNInternal();
                    }
                    else {
                        $h = $my_session->getCDN($blenderArch);
                        $download_binary[$project->getId()] = 1;
                    }

                    $h->addDownload(filesize($path));
                    $this->requestTimeProfiler->setTime(RequestTimeProfiler::ARCHIVE_BINARY, 1000 * (microtime(true) - $start));

                    $download_binary[$project->getId()] += 1;
                    $request->getSession()->set('download_binary', json_encode($download_binary));

                    if ($h->isLocal()) {
                        // internal/local
                        return new BinaryFileResponse($h->getTarget($filename));
                    }
                    else {
                        // external
                        return new RedirectResponse($h->getTarget($filename));
                    }
                }
            }
        }

        return new Response('', 404);
    }

    #[Route(path: '/send_frame.php')]
    public function send_frame(Request $request): Response {
        $error_state = ClientProtocol::UNKNOWN;
        /** @var ?Tile $a_frame */
        $a_frame = null;
        /** @var ?Session $my_session */
        $my_session = null;

        if ($request->getSession()->has('session')) {
            $session_id = $request->getSession()->get('session');
            $my_session = $this->sessionRepository->find($session_id);
            if (is_object($my_session)) {
                $error_state = ClientProtocol::OK;
                $my_session->keepMeAlive();

                if ($my_session->getBlocked() != 0) {
                    $error_state = ClientProtocol::JOB_VALIDATION_ERROR_SESSION_DISABLED;
                }
            }
        }
        else {
            $error_state = ClientProtocol::INTERVAL_VALUE;
        }

        $version = '';
        if (is_object($my_session)) {
            $version = $my_session->getVersion();
        }
        $handler = $this->clientProtocolService->getHandler($version, $my_session, $request->getSession());

        if ($error_state == ClientProtocol::OK) {
            list($error_state, $a_frame, $frame_number, $file) = $handler->jobValidationInput($request->query->all(), $request->files);
            $a_image = new Image($file);
            if (filesize($file) == 0 || $a_image->isImage() == false) {
                Logger::error("(send_frame.php) error 032 the file is not a picture (frame: $a_frame) (path: $file is_file ".serialize($file)." file_exists ".file_exists($file).")");
                $error_state = ClientProtocol::JOB_VALIDATION_ERROR_FRAME_IS_NOT_IMAGE;
                @unlink($file);
                if (is_object($a_frame) == true && is_object($my_session) == true && is_object($a_frame->getUser()) && $my_session->getUser()->getId() == $a_frame->getUser()->getId()) {
                    $a_frame->reset(true); // reset but also give credit to user
                }
            }
            else {
                $ret = $my_session->validateFrame($a_frame, $file, $request->getSession());
                if ($ret == 0) {
                    $error_state = ClientProtocol::OK;
                    $e = new Event();
                    $e->setType(Event::TYPE_VALIDATE);
                    $e->setSession($my_session->getId());
                    $e->setTime(time());
                    $e->setUser($my_session->getUser()->getId());
                    $e->setProject($a_frame->getFrame()->getProject()->getId());
                    $this->entityManager->persist($e);

                    $job = $a_frame->getFrame()->getProject();
                    $gpu = $a_frame->getGPU();
                    if (is_object($gpu)) { // rendered by a gpu
                        $vram = $gpu->getMemory();
                        $current_success = $job->getVramSuccess();
                        if ($current_success == 0 || ($vram > 0 && $vram < $current_success)) {
                            $job->setVramSuccess($vram);
                        }
                    }

                    if ($request->request->has('memory_used') || $request->request->has('memoryused')) {
                        $mem = 0;
                        if ($request->request->has('memory_used')) {
                            $mem = $request->request->get('memory_used');
                        }
                        if ($request->request->has('memoryused')) {
                            $mem = $request->request->get('memoryused');
                        }
                        if ($mem != 0 && $mem < $my_session->getMemory() && $my_session->getVersion() != $this->config['client']['github_version']) {
                            $job->updateMaxMemoryUsed($mem);
                        }
                    }
                }
                elseif ($ret == -60) {
                    $error_state = ClientProtocol::OK;
                }
                else {
                    Logger::error("(send_frame.php) failed to validate frame $a_frame ret: $ret");
                    @unlink($file);
                    if ($ret == ClientProtocol::JOB_VALIDATION_ERROR_BROKEN_MACHINE) {
                        $error_state = $ret;
                    }
                    else {
                        $error_state = ClientProtocol::INTERVAL_VALUE; // silent the error
                    }

                    $project = $a_frame->getFrame()->getProject();
                    /** @phpstan-ignore-next-line */
                    if (is_object($project)) {
                        $phpSession = $request->getSession();
                        $ban = [];
                        if ($phpSession->has('ban')) {
                            $ban = $phpSession->get('ban');
                        }
                        $ban[$project->getId()] = $project->getId(); // for the CriterionBanJobFromUserSendError
                        $phpSession->set('ban', $ban);
                    }
                }
            }
        }

        if ($error_state == ClientProtocol::INTERVAL_VALUE) {
            if (is_object($a_frame) && is_object($my_session) && $my_session->getUser()->getId() == $a_frame->getUser()->getId() && $a_frame->getStatus() == Constant::PROCESSING) {
                $a_frame->reset(true); // reset but also give credit to user because he sent time
            }
            $error_state = ClientProtocol::OK; // silent the error
        }

        $this->entityManager->flush();

        return new Response($handler->jobValidationOutput($error_state), headers: ['Content-Type' => 'text/xml']);
    }

    /**
     * @return string[]
     */
    private function get_list_of_files_to_delete_from_input_xml(): array {
        $ret = array();

        $input_dom = Misc::loadImputXML();
        if ($input_dom !== false) {
            $cache_node = $input_dom->getElementsByTagName('cache')->item(0);
            if (is_null($cache_node) == false) {
                $file_nodes = $cache_node->getElementsByTagName('file');
                foreach ($file_nodes as $a_node) {
                    if ($a_node->hasAttribute('md5')) {
                        $blenderBinary = $this->blenderArchRepository->findFromChunk($a_node->getAttribute('md5'));
                        if (is_null($blenderBinary)) { // old client send the md5 of the full zip not the chunk, compatibility with protocol60
                            $blenderBinary = $this->blenderArchRepository->findOneBy(['md5' => $a_node->getAttribute('md5')]);
                        }
                        $delete = false;
                        if (is_null($blenderBinary)) {
                            // it's not a binary.
                            // Is it a project chunk md5 ?

                            $project = $this->projectRepository->findFromChunk($a_node->getAttribute('md5'));
                            if (is_null($project)) {
                                $delete = true;
                            }
                            elseif ($project->getStatus() == Constant::FINISHED || $project->getStatus() == Constant::PAUSED) {
                                $delete = true;
                            }
                        }

                        if ($delete) {
                            $ret [] = $a_node->getAttribute('md5');
                        }
                    }
                }
            }
        }
        return $ret;
    }
}