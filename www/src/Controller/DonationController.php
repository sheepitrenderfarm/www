<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Donor;
use App\Repository\DonorRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\Logger;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Donation controller
 */
#[Route(path: '/donation')]
class DonationController extends BaseController {
    private UserRepository $userRepository;
    private DonorRepository $donorRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, HTML $html, UserRepository $userRepository, DonorRepository $donorRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->config = $configService->getData();
        $this->userRepository = $userRepository;
        $this->donorRepository = $donorRepository;
    }

    #[Route(path: '')]
    public function main(): Response {
        if ($this->config['donation']['enable'] == false) {
            return $this->render('error.html.twig', [
                'error_title' => 'Error',
                'message' => 'No donation is possible at the moment',
            ]);
        }
        else {

            $col_displayed = 0;
            if (isset($this->config['donation']['paypal']['ID']) && $this->config['donation']['paypal']['ID'] != '') {
                $col_displayed++;
            }

            $col_displayed++;// patreon
            $col_displayed++; // spreadshirt

            if (isset($this->config['donation']['bitcoin']['ID']) && $this->config['donation']['bitcoin']['ID'] != '') {
                $col_displayed++;
            }

            $user_donors = $this->userRepository->getAllDonors();
            shuffle($user_donors);

            $donors = $this->donorRepository->findAll();
            shuffle($donors);

            return $this->render('donation/main.html.twig', [
                    'donors_list' => $donors,
                    'donors_users' => $user_donors,
                    'col_width' => intval(12 / $col_displayed),
                    'paypal' => isset($this->config['donation']['paypal']['ID']) && $this->config['donation']['paypal']['ID'] != '' ? $this->config['donation']['paypal']['ID'] : null,
                    'bitcoin' => isset($this->config['donation']['bitcoin']['ID']) && $this->config['donation']['bitcoin']['ID'] != '' ? $this->config['donation']['bitcoin']['ID'] : null,
                    'spreadshirt' => isset($this->config['donation']['spreadshirt']['ID']) && $this->config['donation']['spreadshirt']['ID'] != '' ? $this->config['donation']['spreadshirt']['ID'] : null,
                ]
            );
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/paypal/completed')]
    public function paypal_completed(): Response {
        $this->getUser()->giveAward('AwardContributor');

        return $this->render('donation/paypal_completed.html.twig', [
            'header_default_container' => true,
        ]);
    }

    #[Route(path: '/logo/{donor}.png', methods: 'GET')]
    public function donor_logo(Donor $donor): Response {
        return new Response(base64_decode($donor->getImage()));
    }

    #[Route(path: '/paypal/ipn')]
    public function paypal_ipn(Request $request): Response {
        Logger::info(__METHOD__.' transaction received');
        // TODO: check if the transaction is valid

        $payer_email = $request->request->get('payer_email');

        $payer = $this->userRepository->importFromEmail($payer_email);
        if (is_object($payer)) {
            $payer->setPayPalDonation(new \DateTime());
            Logger::info(__METHOD__.' new donation from '.$payer->getId());
            $this->entityManager->flush();
        }

        return new Response();
    }
}