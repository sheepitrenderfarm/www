<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Terms of use controller
 */
#[Route(path: '/termsofuse')]
class TermsOfUseController extends BaseController {
    #[Route(path: '')]
    public function main(): Response {
        return $this->render('tos/main.html.twig', [

            ]
        );
    }
}