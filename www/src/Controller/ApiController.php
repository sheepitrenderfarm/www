<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net> 2016
 *
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\User;
use App\Repository\ProjectRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Service\ConfigService;
use App\Service\DiscordWebHook;
use App\Service\Main;
use App\Service\NetworkUsage\MonitoringNetwork;
use App\Service\NetworkUsage\NetworkUsage;
use App\Service\RequestTimeProfiler;
use App\UI\HTML;
use App\Utils\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * External api controller
 */
#[Route(path: '/')]
class ApiController extends BaseController {
    private TileRepository $frameRepository;
    private ProjectRepository $projectRepository;
    private SessionRepository $sessionRepository;
    private TileRepository $tileRepository;
    private DiscordWebHook $discordWebHook;
    private RequestTimeProfiler $requestTimeProfiler;

    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        Main $main,
        HTML $html,
        ProjectRepository $projectRepository,
        TileRepository $frameRepository,
        SessionRepository $sessionRepository,
        TileRepository $tileRepository,
        ConfigService $configService,
        RequestTimeProfiler $requestTimeProfiler
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->projectRepository = $projectRepository;
        $this->frameRepository = $frameRepository;
        $this->sessionRepository = $sessionRepository;
        $this->tileRepository = $tileRepository;
        $this->discordWebHook = $main->getDiscordWebHook();
        $this->requestTimeProfiler = $requestTimeProfiler;
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/api.php', condition: "request.query.get('action') == 'show_sessions'")]
    public function show_sessions(): Response {
        $ret = '<root>';
        foreach ($this->getUser()->sessions() as $session) {
            $ret .= '<session id="'.$session->getId().'" hostname="'.$session->getHostname().'" device="'.$session->getComputeDeviceStrHuman().'" />';
        }
        $ret .= '</root>';
        return new Response($ret, 200, ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/api.php', condition: "request.query.get('action') == 'mirror_sync'")]
    public function mirror_sync(): Response {
        $ret = '<root>';
        foreach ($this->projectRepository->findAll() as $project) {
            $ret .= '<scene id="'.$project->getId().'" />';
        }
        $ret .= '</root>';
        return new Response($ret, 200, ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/api.php', condition: "request.query.get('action') == 'global_stats'")]
    public function global_stats(): Response {
        $s = new Session();
        // generic user for the stats
        $a_user = new User();
        $a_user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1);
        $a_user->setId('generic_login_'.time());

        $active_projects = 0;
        $projects = $this->projectRepository->findAll();
        foreach ($projects as $project) {
            $status = $project->getStatus();
            if ($status == Constant::PROCESSING || $status == Constant::WAITING) {
                if ($project->isBlocked() == false && $project->getId() != Project::COMPUTE_METHOD_PROJECT_ID && ($project->getCachedStatistics()['total'] > 0) && $project->canRenderProject($a_user)) {
                    $active_projects += 1;
                }
            }
        }

        $ret = '<root>';
        $ret .= '<stats ';
        $load = sys_getloadavg(); // last 1, 5 and 15 minutes.
        $ret .= 'load="'.round($load[1], 1).'" ';
        $ret .= 'overload="'.($s->systemIsOverLoad() ? '1' : '0').'" ';
        $ret .= 'frames_remaining="'.$this->frameRepository->countPublicRemainingFrames().'" ';
        $ret .= 'connected_clients="'.$this->sessionRepository->countConnectedMachines().'" ';
        $ret .= 'frames_rendering="'.$this->tileRepository->count(['status' => Constant::PROCESSING]).'" ';
        $ret .= 'active_projects="'.$active_projects.'" ';
        $ret .= 'active_renderers_updated_daily="'.count($this->main->getBestRenderersFromCache()).'" ';

        $ret .= ' />';
        $ret .= '</root>';
        return new Response($ret, 200, ['Content-Type' => 'text/xml']);
    }

    #[Route(path: '/api.php', condition: "request.query.get('action') == 'global_stats_json'")]
    public function global_stats_json(): JsonResponse {

        $s = new Session();
        // generic user for the stats
        $a_user = new User();
        $a_user->setLevelFromMask(User::ACL_MASK_CAN_DO_RENDERING, 1);
        $a_user->setId('generic_login_'.time());

        $active_projects = 0;
        foreach ($this->projectRepository->findAll() as $project) {
            $status = $project->getStatus();
            if ($status == Constant::PROCESSING || $status == Constant::WAITING) {
                if ($project->isBlocked() == false && $project->getId() != Project::COMPUTE_METHOD_PROJECT_ID && ($project->getCachedStatistics()['total'] > 0) && $project->canRenderProject($a_user)) {
                    $active_projects += 1;
                }
            }
        }

        $data = array();
        $data['overload'] = $s->systemIsOverLoad();
        $load = sys_getloadavg(); // last 1, 5 and 15 minutes.
        $data['load'] = round($load[1], 1);
        $data['frames_remaining'] = $this->frameRepository->countPublicRemainingFrames();
        $data['connected_clients'] = $this->sessionRepository->countConnectedMachines();
        $data['frames_rendering'] = count($this->main->frames(Constant::PROCESSING));
        $data['active_projects'] = $active_projects;
        $data['active_renderers_updated_daily'] = count($this->main->getBestRenderersFromCache());

        return new JsonResponse($data);
    }

    #[Route(path: '/api/shepherd/finish.php')]
    public function finish(Request $request): Response {
        $start = microtime(true);
        if ($request->request->has('type')) {

            if ($request->request->get('type') == 'tile' && $request->request->has('tile') && $request->request->has('rendertime') && $request->request->has('memory_used')) {
                $tile_uid = $request->request->get('tile');
                $rendertime = $request->request->get('rendertime');
                $memory_used = $request->request->get('memory_used');
                $preptime = $request->request->has('preptime') ? (int)$request->request->get('preptime') : 0;

                $frame = $this->frameRepository->find($tile_uid);
                if (is_object($frame)) {
                    $ret = $frame->validateFromShepherd($rendertime, $preptime, $memory_used);
                    if ($ret == 0) {
                        $user = $frame->getUser();
                        if (is_object($user)) {
                            $user->updateAwards($frame, $this->sessionRepository->find($frame->getSession()));
                            if ($user->getRenderedFramesPublic() == $this->config['affiliate_minimum_frame']) {
                                $user->affiliate();
                            }
                        }
                    }
                }
                $end = microtime(true);
                $this->requestTimeProfiler->setTime(RequestTimeProfiler::VALIDATE_JOB, 1000 * ($end - $start));
            }
            elseif ($request->request->get('type') == 'zip' && $request->request->has('blend')) {
                $blend = $request->request->get('blend');
                $project = $this->projectRepository->find($blend);
                if (is_object($project)) {
                    $project->setZipGenerated(time());
                    $this->entityManager->flush($project);
                    if ($project->isFinished()) {
                        $project->emailOwnerOnFinish();
                    }
                    else {
                        // partial zip creation
                        $user = $project->getOwner();
                        $email = $user->getEmail();
                        $url = $this->config['site']['url'].$this->generateUrl("app_project_manage", ['project' => $project->getId()]);
                        $content = sprintf('Your partial archive frame of the project %s is now available, you can download via <a href="%s">project administration page</a>.', $project->getName().'</a>', $url);
                        Mail::sendamail($email, 'You can download your project archive frame.', $content, 'project_download_partial');
                    }
                }
            }
            elseif ($request->request->get('type') == 'mp4_preview' && $request->request->has('blend')) {
                $blend = $request->request->get('blend');
                $project = $this->projectRepository->find($blend);
                if (is_object($project)) {
                    $project->setMp4PreviewGenerated(time());
                    $this->entityManager->flush($project);
                }
            }
            elseif ($request->request->get('type') == 'mp4_final' && $request->request->has('blend')) {
                $blend = $request->request->get('blend');
                $project = $this->projectRepository->find($blend);
                if (is_object($project)) {
                    $project->setMp4FinalGenerated(time());
                    $this->entityManager->flush($project);
                }
            }
        }
        return new Response();
    }

    #[Route(path: '/api/shepherd/failed.php')]
    public function failed(Request $request): Response {
        if ($request->request->has('type') && $request->request->get('type') == 'tile' && $request->request->has('id')) {
            $tile = $this->tileRepository->find($request->request->get('id'));
            if (is_object($tile)) {
                $tile->reset();
            }
        }
        elseif ($request->request->has('type') && $request->request->has('blend')) {
            $project = $this->projectRepository->find($request->request->get('blend'));
            if (is_object($project)) {
                $logs = '';
                if ($request->request->has('log')) {
                    $logs = join("\n", json_decode($request->request->get('log'), true));
                }
                $shepherd = $project->getShepherd()->getId();
                $this->discordWebHook->shepherdTaskFailed($shepherd, $request->request->get('type'), $request->request->get('blend'), $logs);
            }
        }
        return new Response();
    }

    #[Route(path: '/api/internal/network')]
    public function network(Request $request): JsonResponse {
        $data = array();

        if ($request->query->has('host')) {
            $networkUsage = new NetworkUsage($request->query->get('host'));
            $previousNetworkUsage = $networkUsage->get();
            $currentNetworkUsage = $networkUsage->generate();

            // stats can not be negative
            $data['rx'] = max(0, (new MonitoringNetwork('rx', $previousNetworkUsage, $currentNetworkUsage))->getValue());
            $data['tx'] = max(0, (new MonitoringNetwork('tx', $previousNetworkUsage, $currentNetworkUsage))->getValue());
        }

        return new JsonResponse($data);
    }
}
