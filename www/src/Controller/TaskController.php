<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Constant;
use App\Entity\Task;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Task controller
 */
#[Route(path: '/task')]
class TaskController extends BaseController {

    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{task}/reset')]
    public function reset(Task $task): Response {
        $task->setStatus(Constant::WAITING);
        $this->entityManager->flush($task);

        return new Response('OK');
    }
}