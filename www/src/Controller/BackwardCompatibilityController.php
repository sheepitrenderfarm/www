<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\GiftAnonymous;
use App\Entity\News;
use App\Entity\Team;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Old url controller (usually from social network post)
 */
#[Route(path: '/')]
class BackwardCompatibilityController extends BaseController {

    #[Route(path: '/news.php?id={news}', methods: 'GET')]
    public function news(News $news): RedirectResponse {
        return $this->redirect($this->generateUrl('app_news_single', ['news' => $news->getId()]));
    }

    #[Route(path: '/servers.php', methods: 'GET')]
    public function servers(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_servers_main'));
    }

    #[Route(path: '/renderers.php', methods: 'GET')]
    public function renderers(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_ranking_user'));
    }

    #[Route(path: '/machines.php', methods: 'GET')]
    public function machines(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_ranking_machine'));
    }

    #[Route(path: '/donation.php', methods: 'GET')]
    public function donation(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_donation_main'));
    }

    #[Route(path: '/faq.php', methods: 'GET')]
    public function faq(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_faq_main'));
    }

    #[Route(path: '/reference_machine.blend', methods: 'GET')]
    public function reference_machine_blend(): BinaryFileResponse {
        return new BinaryFileResponse(__DIR__.'/../reference_machine.blend');
    }

    #[Route(path: '/gift_anonymous.php?id={gift}', methods: 'GET')]
    public function gift_anonymous(GiftAnonymous $gift): RedirectResponse {
        return $this->redirect($this->generateUrl('app_giftanonymous_show', ['gift' => $gift->getId()]));
    }

    #[Route(path: '/project', methods: 'GET')]
    public function project(): RedirectResponse {
        return $this->redirect($this->generateUrl('app_home_projects'));
    }

    #[Route(path: '/team.php?id={team}', methods: 'GET')]
    public function team(Team $team): RedirectResponse {
        return $this->redirect($this->generateUrl('app_team_single', ['team' => $team->getId()]));
    }

    #[Route(path: '/confirm.php?login={login}&key={key}', methods: 'GET')]
    public function confirm_user(string $login, string $key): RedirectResponse {
        return $this->redirect($this->generateUrl('app_user_confirm_user', ['preuser' => $login, 'key' => $key]));
    }

    #[Route(path: '/account.php', methods: 'GET', condition: "request.query.get('mode') == 'profile'")]
    public function user_profile(Request $request): RedirectResponse {
        if ($request->query->has('login')) {
            return $this->redirect($this->generateUrl('app_user_profile', ['user' => $request->query->get('login')]));
        }
        else {
            throw new NotFoundHttpException("Page not found");
        }
    }

    #[Route(path: '/project/frames/{project_id}/interval/highLightTitle.png', methods: 'GET')]
    public function no_js_bug(int $project_id): Response {
        return new Response('', 404);
    }
}