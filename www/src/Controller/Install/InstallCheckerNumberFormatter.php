<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

class InstallCheckerNumberFormatter implements InstallChecker {
    public function name(): string {
        return 'NumberFormatter class';
    }

    public function check(): string {
        if (class_exists('NumberFormatter') == false) {
            return '<span style="color: red;">NumberFormatter not present<br>Do "apt-get install php-intl"</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}
