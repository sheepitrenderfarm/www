<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerConfig implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }


    public function name(): string {
        return 'Configuration file';
    }

    public function check(): string {
        $config = $this->configService->getData();


        if (isset($config['storage']['path'])) {
            if (str_ends_with($config['storage']['path'], '/') == false) {
                return $this->failed('$config[\'storage\'][\'path\'] must end with a \'/\'');
            }
        }

        return $this->ok();
    }

    private function ok(): string {
        return '<span style="color: green;">OK</span>';
    }

    private function failed(string $msg): string {
        return '<span style="color: red;">'.$msg.'</span>';
    }
}
