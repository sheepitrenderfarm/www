<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Entity\Blender;
use App\Entity\BlenderArch;
use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerBlenderBinaries implements InstallChecker {
    private ConfigService $configService;
    private EntityManagerInterface $entityManager;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
        $this->entityManager = $entityManager;
    }

    public function name(): string {
        return 'Blender binaries';
    }

    public function check(): string {
        $config = $this->configService->getData();

        if (array_key_exists('renderer', $config) == false) {
            return '<span style="color: red;">$config[\'renderer\'] does not exists. <br>Add "$_config[\'renderer\'] = array();" to config.local.inc.php</span>';
        }

        if (is_array($config['renderer']) == false) {
            return '<span style="color: red;">$_config[\'renderer\'] should be an array. <br>Please check your config.local.inc.php</span>';
        }

        if (count($this->entityManager->getRepository(Blender::class)->findAll()) == 0) {
            return '<span style="color: red;">$_config[\'renderer\'][\'binary\'] should be an array. <br>Add something like "$_config[\'renderer\'][\'binary\'] = array(\'blender276b\' => \'blender276b\');" to your config.local.inc.php</span>';
        }

        $missing_zip = '';
        foreach ($this->entityManager->getRepository(Blender::class)->findAll() as $blenderBinary) {
            foreach (BlenderArch::SUPPORTED_ARCHS as $arch => $arch_txt) {
                $blenderArchObject = new BlenderArch();
                $blenderArchObject->setArch($arch);
                $blenderArchObject->setBlender($blenderBinary);
                $path = $config['storage']['path'].'binaries/'.$blenderArchObject->getPath();
                if (file_exists($path) == false) {
                    $missing_zip .= $blenderArchObject->getPath().' MISSING<br>';
                }
            }
        }

        if ($missing_zip == '') {
            return '<span style="color: green;">OK</span>';
        }
        else {
            return '<span style="color: red;">'.$missing_zip.'</span>';
        }
    }
}
