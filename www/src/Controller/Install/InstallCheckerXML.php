<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

class InstallCheckerXML implements InstallChecker {
    public function name(): string {
        return 'XML class';
    }

    public function check(): string {
        if (class_exists('DomDocument') == false) {
            return '<span style="color: red;">DomDocument not present<br>Do "apt-get install php-xml"</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}
