<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerPersistantCacheFile implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }

    public function name(): string {
        return 'PersistantCacheFile';
    }

    public function check(): string {
        $config = $this->configService->getData();

        if (array_key_exists('persistent_cache', $config) == false) {
            return '<span style="color: red;">$config[\'persistent_cache\'] does not exists. <br>Add "$config[\'persistent_cache\'] = array();" to config.local.inc.php</span>';
        }

        if (is_array($config['persistent_cache']) == false) {
            return '<span style="color: red;">$config[\'persistent_cache\'] should be an array. <br>Check your config.local.inc.php</span>';
        }

        if (array_key_exists('type', $config['persistent_cache']) == false) {
            return '<span style="color: red;">$config[\'persistent_cache\'][\'type\'] does not exists. <br>Add "$config[\'persistent_cache\'][\'type\'] = \'something\';" to config.local.inc.php</span>';
        }

        if ($config['persistent_cache']['type'] == 'file') {
            if (is_dir($config['persistent_cache']['path']) == false) {
                return '<span style="color: red;">'.$config['persistent_cache']['path'].' is not a directory</span>';
            }
        }
        return '<span style="color: green;">OK</span>';
    }
}
