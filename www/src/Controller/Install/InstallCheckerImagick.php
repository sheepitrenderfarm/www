<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

class InstallCheckerImagick implements InstallChecker {
    public function name(): string {
        return 'Imagick & Image lib';
    }

    public function check(): string {
        if (class_exists('Imagick') == false) {
            return '<span style="color: red;">Imagick not present<br>Do "apt-get install php-imagick"</span>';
        }

        $ret = exec("convert --version");
        if ($ret == '') {
            return '<span style="color: red;">"convert" command could not be launch.<br>Do "apt-get install imagemagick"</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}
