<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerBlenderReader implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }

    public function name(): string {
        return 'Blender reader';
    }

    public function check(): string {
        $config = $this->configService->getData();

        if (file_exists($config['reader']['blender4.0']) == false) {
            return '<span style="color: red;">'.$config['reader']['blender4.0'].' is missing for $config[\'reader\'][\'blender3.0\']</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}