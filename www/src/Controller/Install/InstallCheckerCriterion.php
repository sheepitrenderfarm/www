<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerCriterion implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }

    public function name(): string {
        return 'Criterion';
    }

    public function check(): string {
        $config = $this->configService->getData();

        if (array_key_exists('scheduler', $config) == false) {
            return '<span style="color: red;">$config[\'scheduler\'] does not exists</span>';
        }

        if (is_array($config['scheduler']) == false) {
            return '<span style="color: red;">$config[\'scheduler\'] is not an array</span>';
        }

        if (array_key_exists('criterion', $config['scheduler']) == false) {
            return '<span style="color: red;">$config[\'scheduler\'][\'criterion\'] does not exists</span>';
        }

        if (is_array($config['scheduler']['criterion']) == false) {
            return '<span style="color: red;">$config[\'scheduler\'] is not an array</span>';
        }

        foreach ($config['scheduler']['criterion'] as $scheduler) {
            if (is_array($scheduler)) {
                foreach ($scheduler as $class_name => $weight) {
                    if (class_exists("\\App\\Scheduler\\Criterion\\".$class_name) == false) {
                        return '<span style="color: red;">'.$class_name.' does not exist</span>';
                    }
                }
            }
        }

        return '<span style="color: green;">OK</span>';
    }
}
