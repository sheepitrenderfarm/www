<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Entity\Project;
use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class InstallCheckerProjectZero implements InstallChecker {
    private array $config;
    private EntityManagerInterface $entityManager;


    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->config = $configService->getData();
        $this->entityManager = $entityManager;
    }

    public function name(): string {
        return 'ComputeMethod and PowerDetection project';
    }

    public function check(): string {

        // project
        try {
            $project = $this->entityManager->getRepository(Project::class)->find(Project::COMPUTE_METHOD_PROJECT_ID);
            if (is_null($project)) {
                return $this->failed('Project id: '.Project::COMPUTE_METHOD_PROJECT_ID.' is not present on SQL database');
            }

            foreach ($project->getChunks() as $chunk) {
                $path = $this->config['storage']['path'].'projects'.'/'.$project->getChunkStorageName($chunk);
                if (file_exists($path) == false) {
                    return $this->failed($path.' does not exist');
                }
            }
        }
        catch (Exception $e) {
            return $this->failed('Project id: '.Project::COMPUTE_METHOD_PROJECT_ID.' is not present on SQL database');
        }

        // jobs
        try {
            $project = $this->entityManager->getRepository(Project::class)->find(Project::POWER_DETECTION_PROJECT_ID);
            if (is_null($project)) {
                return $this->failed('Project id: '.Project::POWER_DETECTION_PROJECT_ID.' is not present on SQL database');
            }

            if ($project->frames()->count() == 0) {
                return $this->failed('Project id: '.Project::POWER_DETECTION_PROJECT_ID.' has no frame');
            }

            if (count($project->tiles()) == 0) {
                return $this->failed('Project id: '.Project::POWER_DETECTION_PROJECT_ID.' has no tile');
            }

            foreach ($project->getChunks() as $chunk) {
                $path = $this->config['storage']['path'].'projects'.'/'.$project->getChunkStorageName($chunk);
                if (file_exists($path) == false) {
                    return $this->failed($path.' does not exist');
                }
            }
        }
        catch (Exception $e) {
            return $this->failed('Project id: '.Project::POWER_DETECTION_PROJECT_ID.' is not present on SQL database (e)');
        }

        return $this->ok();
    }

    private function ok(): string {
        return '<span style="color: green;">OK</span>';
    }

    private function failed(string $msg): string {
        return '<span style="color: red;">'.$msg.'</span>';
    }
}
