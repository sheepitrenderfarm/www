<?php
/**
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerPythonClient implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }

    public function name(): string {
        return 'Python client files';
    }

    public function check(): string {
        $config = $this->configService->getData();

        $filename_conf = $config['storage']['path'].'scripts/conf.py';

        if (file_exists($filename_conf) == false) {
            return '<span style="color: red;">'.$filename_conf.' does not exist</span>';
        }

        $filename_conf_exr = $config['storage']['path'].'scripts/conf_exr.py';

        if (file_exists($filename_conf_exr) == false) {
            return '<span style="color: red;">'.$filename_conf_exr.' does not exist</span>';
        }
        return '<span style="color: green;">OK</span>';
    }
}