<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

class InstallCheckerArchiveBuilder implements InstallChecker {
    public function name(): string {
        return 'Archive Builder';
    }

    public function check(): string {
        $ret = exec("zip --version");
        if ($ret == '') {
            return '<span style="color: red;">"zip" command could not be launch.</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}