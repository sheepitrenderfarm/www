<?php
/**
 * Copyright (C) 2016 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller\Install;

use App\Service\ConfigService;
use Doctrine\ORM\EntityManagerInterface;

class InstallCheckerStorage implements InstallChecker {
    private ConfigService $configService;

    public function __construct(ConfigService $configService, EntityManagerInterface $entityManager) {
        $this->configService = $configService;
    }

    public function name(): string {
        return 'Storage';
    }

    public function check(): string {
        $config = $this->configService->getData();

        if (array_key_exists('storage', $config) == false) {
            return '<span style="color: red;">$config[\'storage\'] does not exists. <br>Add "$config[\'storage\'] = array();" to config.local.inc.php</span>';
        }

        if (is_array($config['storage']) == false) {
            return '<span style="color: red;">$config[\'storage\'] should be an array. <br>Check your config.local.inc.php</span>';
        }

        if (array_key_exists('path', $config['storage']) == false) {
            return '<span style="color: red;">$config[\'storage\'][\'path\'] does not exists. <br>Add "$config[\'storage\'][\'path\'] = \'something\';" to config.local.inc.php</span>';
        }

        if (is_dir($config['storage']['path']) == false) {
            return '<span style="color: red;">'.$config['storage']['path'].' is not a directory</span>';
        }

        if (is_dir($config['storage']['path'].'projects'.'/') == false) {
            return '<span style="color: red;">'.$config['storage']['path'].'projects/ is not a directory</span>';
        }

        if (is_dir($config['storage']['path'].'binaries'.'/') == false) {
            return '<span style="color: red;">'.$config['storage']['path'].'binaries/ is not a directory</span>';
        }

        if (is_dir($config['storage']['path'].'cache'.'/') == false) {
            return '<span style="color: red;">'.$config['storage']['path'].'cache/ is not a directory</span>';
        }

        if (is_dir($config['storage']['path'].'users/avatars'.'/') == false) {
            return '<span style="color: red;">'.$config['storage']['path'].'users/avatars/ is not a directory</span>';
        }

        return '<span style="color: green;">OK</span>';
    }
}
