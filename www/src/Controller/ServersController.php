<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\ShepherdInternal;
use App\Entity\ShepherdMock;
use App\Repository\ShepherdServerRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Servers controller
 */
#[Route(path: '/servers')]
class ServersController extends BaseController {
    private ShepherdServerRepository $shepherdServerRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, HTML $html, ShepherdServerRepository $shepherdServerRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->shepherdServerRepository = $shepherdServerRepository;
    }

    #[Route(path: '')]
    public function main(): Response {
        $shepherds = [];
        foreach ($this->shepherdServerRepository->findAll() as $shepherd) {
            if ($shepherd->getId() == 'https://shepherd-fallback.sheepit-renderfarm.com') {
                continue;
            }
            if ($shepherd instanceof ShepherdInternal or $shepherd instanceof ShepherdMock) {
                continue;
            }
            $stats = $shepherd->getLastStats();
            $position = explode(' ', $shepherd->getLocationGeographic(), 2);
            $data = [
                'lon' => $position[1],
                'lat' => $position[0],
                'id_human' => MISC::humanServerHost($shepherd->getId()),
                'is_alive' => is_object($stats),
            ];

            if (is_object($stats)) {
                list($index, $color) = $this->thermometer(max($stats->getNetworkRX(), $stats->getNetworkTX()), 1000000);
                $data['network_rx_index'] = $index;
                $data['network_rx_color'] = $color;

                list($index, $color) = $this->thermometer($stats->getCpu(), 8);
                $data['cpu_index'] = $index;
                $data['cpu_color'] = $color;

                list($index, $color) = $this->thermometer($stats->getDisk(), $shepherd->getStorageMax());
                $data['storage_index'] = $index;
                $data['storage_color'] = $color;

                list($index, $color) = $this->thermometer($stats->getTask(), 800);
                $data['task_index'] = $index;
                $data['task_color'] = $color;
            }

            $shepherds [] = $data;
        }

        return $this->render('servers/servers.html.twig', [
            'shepherds' => $shepherds,
        ]);
    }

    /**
     * @return array{0: int, 1: string}
     */
    public function thermometer(int $input, float $max = 1.0): array {
        $val = floatval($input) / floatval($max);
        if ($val > 0.80) {
            return array(4, 'red');
        }
        else {
            if ($val > 0.30) {
                return array(3, 'orange');
            }
            else {
                if ($val > 0.15) {
                    return array(1, 'green');
                }
                else {
                    return array(0, 'green');
                }
            }
        }
    }
}
