<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Team;
use App\Entity\User;
use App\Repository\SessionRepository;
use App\Repository\TeamRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Team controller
 */
#[Route(path: '/team')]
class TeamController extends BaseController {
    private TeamRepository $teamRepository;
    private SessionRepository $sessionRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, HTML $html, ConfigService $configService, TeamRepository $teamRepository, SessionRepository $sessionRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->teamRepository = $teamRepository;
        $this->sessionRepository = $sessionRepository;

    }

    #[Route(path: '')]
    public function all(): Response {
        $points_90days = $this->teamRepository->getTeamPointsOverDays(90);
        $points_180days = $this->teamRepository->getTeamPointsOverDays(180);
        $teamsMemberCount = $this->teamRepository->getMemberCountPerTeam();
        $teams = $this->teamRepository->findAll();

        $teams_twig = [];

        $index = 0;
        foreach ($points_90days as $teamRank90) {
            $team = null;
            $teamRank180 = null;

            foreach ($teams as $a_team) {
                if ($teamRank90['team'] == $a_team->getId()) {
                    $team = $a_team;
                    foreach ($points_180days as $teamRank) {
                        if ($teamRank['team'] == $a_team->getId()) {
                            $teamRank180 = $teamRank;
                        }
                    }

                    break;
                }
            }

            if (is_object($team)) {
                $teams_twig [] = [
                    'id' => $team->getId(),
                    'name' => $team->getName(),
                    'rank' => $index + 1,
                    'points' => $team->getPoints(),
                    'rank90_points' => $teamRank90['points'],
                    'rank180_points' => is_array($teamRank180) ? $teamRank180['points'] : 0,
                    'count_members' => array_key_exists($team->getId(), $teamsMemberCount) ? $teamsMemberCount[$team->getId()] : 0,
                ];
            }
            $index++;
        }

        return $this->render('team/all.html.twig', [
                'teams' => $teams_twig,
                'max_member' => $this->config['team']['max_member'],
            ]
        );

    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}', requirements: ['team' => '\d+'], methods: 'GET')]
    public function single(Team $team): Response {
        $points_90days = 0;
        $points_180days = 0;

        $rank_team = 0;
        foreach ($this->teamRepository->getTeamPointsOverDays(90) as $index => $teamRank90) {
            if ($teamRank90['team'] == $team->getId()) {
                $points_90days = $teamRank90['points'];
                $rank_team = $index + 1;
                break;
            }
        }

        foreach ($this->teamRepository->getTeamPointsOverDays(180) as $teamRank180) {
            if ($teamRank180['team'] == $team->getId()) {
                $points_180days = $teamRank180['points'];
                break;
            }
        }

        $owner = $team->getOwner();
        $members = $team->members();
        $my_team = $this->getUser()->getTeam();
        $display_join = false;

        if ($members->contains($this->getUser()) == false) {
            $wantedTeams = $this->getUser()->getWantedTeams();
            $join_request_pending = false;
            if (count($wantedTeams) == 0) {
                $join_request_pending = false;
            }
            foreach ($wantedTeams as $wantedTeam) {
                if ($wantedTeam->getId() == $team->getId()) {
                    $join_request_pending = true;
                }
            }

            if ($join_request_pending == false && is_null($my_team)) {
                $display_join = true;
            }
        }
        else {
            $join_request_pending = false;
        }

        $members_twig = [];
        $array = $members->toArray();
        uasort($array, '\App\Utils\Sort::sortTeamMember');
        $sessions = $this->sessionRepository->findAll();
        $rank = 1;
        foreach ($array as $user) {
            $member = [
                'id' => $user->getId(),
                'join_time' => $user->getTeamJoinTime(),
                'contribution' => $user->getTeamContribution(),
                'last_rendered_frame_timestamp' => is_object($user->getLastRenderedFrame()) ? $user->getLastRenderedFrame()->getTimestamp() : 0,
                'rank' => $rank,
            ];

            $found = false;
            foreach ($sessions as $s) {
                if ($s->getUser()->getId() == $user->getId()) {
                    $found = true;
                    break;
                }
            }

            $member['rendering'] = $found;

            $members_twig [] = $member;
            $rank++;
        }

        return $this->render('team/single.html.twig', [
                'team' => $team,
                'display_leave_team' => (is_object($my_team) && $my_team->getId() == $team->getId()) && ($this->getUser()->getId() != $owner->getId()),
                'display_team_is_full' => count($members) == $this->config['team']['max_member'],
                'display_cannot_join' => is_object($my_team) && $my_team->getId() != $team->getId(),
                'display_join' => $display_join && count($members) != $this->config['team']['max_member'],
                'display_request_pending' => $join_request_pending,
                'points_90days' => $points_90days,
                'points_180days' => $points_180days,
                'owner_join_request' => $this->getUser()->getId() == $owner->getId() ? $this->teamRepository->getJoinRequests($team) : [],
                'rank' => $rank_team,
                'members' => $members_twig,
                'display_admin' => $this->getUser()->getId() == $owner->getId(),
            ]
        );
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/add', methods: 'POST')]
    public function add(Request $request): Response {
        $form = $request->request->all();
        if ($form['action'] == 'add' && array_key_exists('team_create_name', $form) && $form['team_create_name'] != '' && array_key_exists('team_create_description', $form)) {
            $team = new Team();
            $team->setName(strip_tags($form['team_create_name']));
            $team->setDescription(strip_tags($form['team_create_description']));

            $team->setPoints(0);
            $team->setOwner($this->getUser());
            $team->setCreationTime(time());

            if ($this->teamRepository->add($team) != true) {
                return new Response('problem #team-2683');
            }
            else {
                return new RedirectResponse($this->generateUrl("app_team_single", ['team' => $team->getId()]));
            }
        }
        else {
            return new Response('problem #team-2684');
        }
    }

    /**
     * Leave my team
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/leave')]
    public function leave(): Response {
        $myteam = $this->getUser()->getTeam();
        if (is_object($myteam)) {
            return new Response($this->teamRepository->removeMember($myteam, $this->getUser()) == Team::OK ? 'OK' : 'NOK-1083a failed to do removeMember');
        }
        else {
            return new Response('NOK-1083b Failed: no team');
        }
    }

    /**
     * Leave my team and delete it
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/leave_them_delete')]
    public function leave_them_delete(): Response {
        $myteam = $this->getUser()->getTeam();
        if (is_object($myteam)) {
            return new Response($this->teamRepository->remove($myteam) ? 'OK' : 'NOK-1087 failed to do remove');
        }
        else {
            return new Response('NOK-1083b Failed: no team');
        }
    }

    /**
     * Leave my team and them give ownership to someone else
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/leave_them_new_ownership/{newOwner}')]
    public function leave_them_new_ownership(User $newOwner): Response {
        $myteam = $this->getUser()->getTeam();
        if (is_object($myteam)) {
            return new Response($this->teamRepository->changeOwner($myteam, $newOwner) ? 'OK' : 'NOK-1088 failed to do new owner');
        }
        else {
            return new Response('NOK-1083b Failed: no team');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/update', methods: 'POST')]
    public function update(Team $team, Request $request): Response {
        if ($this->getUser()->getId() == $team->getOwner()->getId()) {
            $form = $request->request->all();
            if (array_key_exists('team_update_name', $form) && $form['team_update_name'] != '' && array_key_exists('team_update_description', $form)) {

                $previous_name = $team->getName();
                $previous_description = $team->getDescription();
                $team->setName(strip_tags($form['team_update_name']));
                $team->setDescription(strip_tags($form['team_update_description']));
                $this->entityManager->flush();

                $title = 'The information of your SheepIt Renderfarm team has been updated by the owner';
                $contents = 'Hello<br>';
                $contents .= 'The information of your SheepIt Renderfarm team has been updated by the owner.<br>';
                $contents .= '<a href="'.$this->config['site']['url'].$this->generateUrl("app_team_single", ['team' => $team->getId()]).'">Go to your team page</a>.';

                $contents .= '<h3>OLD</h3>';
                $contents .= '<strong>Name:</strong> '.$previous_name;
                $contents .= '<br>';
                $contents .= '<strong>Description:</strong> '.$previous_description;

                $contents .= '<h3>NEW:</h3>';
                $contents .= '<strong>Name:</strong> '.$team->getName();
                $contents .= '<br>';
                $contents .= '<strong>Description:</strong> '.$team->getDescription();
                $contents .= '<br>';

                $emails = [];
                foreach ($team->members() as $user) {
                    $emails [] = $user->getEmail();
                }

                Mail::sendamail($emails, $title, $contents, 'sheepit');

                return new RedirectResponse($this->generateUrl("app_team_single", ['team' => $team->getId()]));
            }
        }
        return new Response('problem #team-3021');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/askforjoin')]
    public function askforjoin(Team $team): Response {
        return new Response($this->teamRepository->askForJoin($team, $this->getUser()) == Team::OK ? 'OK' : 'NOK-1081 failed to do askForJoin');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/canceljoin')]
    public function cancelJoin(Team $team): Response {
        return new Response($this->teamRepository->cancelAskJoin($team, $this->getUser()) == Team::OK ? 'OK' : 'NOK-1082 failed to do cancelAskJoin');
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/adduser/{user}')]
    public function adduser(Team $team, User $user): Response {
        if ($this->getUser() == $team->getOwner()) {
            return new Response($this->teamRepository->addMember($team, $user) == Team::OK ? 'OK' : 'error: failed to add user to team (error: 1084)');
        }
        else {
            return new Response('error: you are not the owner of the team');
        }
    }

    /**
     * Refuse an ask-to-join request on a team
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/refuseuser/{user}')]
    public function refuseuser(Team $team, User $user): Response {
        if ($this->getUser() == $team->getOwner()) {
            return new Response($this->teamRepository->cancelAskJoin($team, $user) == Team::OK ? 'OK' : 'error: failed to remove request to join the team');
        }
        else {
            return new Response('error: you are not the owner of the team');
        }
    }

    /**
     * Remove a member of the team
     */
    #[IsGranted('ROLE_USER')]
    #[Route(path: '/{team}/removeuser/{user}')]
    public function removeuser(Team $team, User $user): Response {
        if ($this->getUser() == $team->getOwner()) {
            return new Response($this->teamRepository->removeMember($team, $user) == Team::OK ? 'OK' : 'error: failed to add user to team (error: 1088)');
        }
        else {
            return new Response('error: you are not the owner of the team');
        }
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/list_from_term')]
    public function list_from_term(Request $request): JsonResponse {
        /** @var array<int, array{value: string, label: string}> $list */
        $list = array();
        if ($request->query->has('term')) {
            $term = trim($request->query->get('term'));

            $a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
            $json_invalid = json_encode($a_json_invalid);

            // replace multiple spaces with one
            $term = preg_replace('/\s+/', ' ', $term);

            // allow space, any unicode letter and digit, underscore and dash
            if (preg_match("/[^\040\pL\pN_-]/u", $term)) {
                return new JsonResponse($a_json_invalid);
            }

            $teams = $this->teamRepository->findAll();
            foreach ($teams as $t) {
                if (is_string(stristr(strtolower($t->getName()), $term))) {
                    $list [] = ['value' => $t->getId(), 'label' => $t->getName()];
                }
            }
        }

        /** @var array<int, array{value: string, label: string}> $list */
        usort($list, function (array $a, array $b): int {
            return strnatcasecmp($a['label'], $b['label']);
        });
        return new JsonResponse($list);
    }
}
