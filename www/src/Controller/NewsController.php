<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\News;
use App\Repository\NewsRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * News controller
 */
#[Route(path: '/news')]
class NewsController extends BaseController {
    private NewsRepository $newsRepository;

    public function __construct(Main $main, UrlGeneratorInterface $router, EntityManagerInterface $entityManager, ConfigService $configService, HTML $html, NewsRepository $newsRepository) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->newsRepository = $newsRepository;
    }

    #[Route(path: '')]
    public function all(): Response {
        $all_news = $this->newsRepository->findAll();
        foreach ($all_news as $k => $v) {
            if ($v->getId() < 0) {
                unset($all_news[$k]);
            }
        }
        usort($all_news, '\App\Utils\Sort::sortByAttributeId');
        $all_news = array_reverse($all_news);

        return $this->render(
            'news/all.html.twig',
            [
                'title' => 'News',
                'header_default_container' => true,
                'all_news' => $all_news,
                'thumb_width' => $this->config['news']['thumbs']['width'],
                'thumb_height' => $this->config['news']['thumbs']['height'],
            ]
        );
    }

    #[Route(path: '/{news}', methods: 'GET')]
    public function single(News $news): Response {
        $all_news = $this->newsRepository->findAll();
        usort($all_news, '\App\Utils\Sort::sortByAttributeId');

        return $this->render(
            'news/single.html.twig',
            [
                'title' => 'News',
                'news' => $news,
                'other_news' => array_reverse($all_news),
            ]
        );
    }
}