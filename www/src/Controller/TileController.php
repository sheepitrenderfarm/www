<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\Tile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Tile controller
 */
#[IsGranted('ROLE_USER')]
#[Route(path: '/tile')]
class TileController extends BaseController {

    #[Route(path: '/{tile}/reset')]
    public function reset(Tile $tile): Response {
        $project = $tile->getFrame()->getProject();
        if ($project->canManageProject($this->getUser()) || $this->getUser()->isModerator()) {
            $tile->reset();
            return new Response('OK');
        }
        else {
            return new Response('You have no permission to modify this frame');
        }
    }
}