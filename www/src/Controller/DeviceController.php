<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Repository\CPURepository;
use App\Repository\GPURepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Search controller
 */
#[Route(path: '/device')]
class DeviceController extends BaseController {
    private CPURepository $cpuRepository;
    private GPURepository $gpuRepository;

    public function __construct(Main $main,
        HTML $html,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        CPURepository $cpuRepository,
        GPURepository $gpuRepository
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->cpuRepository = $cpuRepository;
        $this->gpuRepository = $gpuRepository;
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/search')]
    public function search(Request $request): JsonResponse {
        /** @var array<int, array{label: string, value: string}> $list */
        $list = array();
        if ($request->query->has('term')) {
            $term = trim($request->query->get('term'));
            $a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
            $json_invalid = json_encode($a_json_invalid);

            // replace multiple spaces with one
            $term = preg_replace('/\s+/', ' ', $term);

            // allow space, any unicode letter and digit, underscore and dash
            if (preg_match("/[^\040\pL\pN_-]/u", $term)) {
                return new JsonResponse($json_invalid);
            }

            if ($term != "") {
                foreach ($this->cpuRepository->findAll() as $u) {
                    if ($u->getPower() > 0 && is_string(stristr(strtolower($u->getModel()), $term))) {
                        $found = false;
                        $description = $u->description();
                        foreach ($list as $arr) {
                            $l = $arr['label'];


                            if ($l == $description) {
                                $found = true;
                                continue;
                            }
                        }

                        if ($found == false) {
                            $list [] = array("value" => 'cpu_'.$u->getId(), "label" => $description);
                        }
                    }
                }
                foreach ($this->gpuRepository->findAll() as $u) {
                    if ($u->getPower() > 0 && is_string(stristr(strtolower($u->getModel()), $term))) {
                        $found = false;
                        $description = $u->description();
                        foreach ($list as $arr) {
                            $l = $arr['label'];


                            if ($l == $description) {
                                $found = true;
                                continue;
                            }
                        }

                        if ($found == false) {
                            $list [] = array("value" => 'gpu_'.$u->getId(), "label" => $description);
                        }
                    }
                }
            }
        }

        /** @var array<int, array{label: string, value: string}> $list */
        usort($list, function (array $a, array $b): int {
            return strcmp($a['label'], $b['label']);
        });

        return new JsonResponse($list);
    }
}
