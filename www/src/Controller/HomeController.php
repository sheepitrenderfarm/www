<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Constant;
use App\Entity\BlenderArch;
use App\Entity\CPU;
use App\Entity\GPU;
use App\Repository\BlenderRepository;
use App\Repository\CPURepository;
use App\Repository\NewsRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Repository\UserRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\UI\HTML;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Home controller
 */
#[Route(path: '')]
class HomeController extends BaseController {
    private TileRepository $tileRepository;
    private NewsRepository $newsRepository;
    private CPURepository $cpuRepository;
    private SessionRepository $sessionRepository;
    private UserRepository $userRepository;
    private ProjectRepository $projectRepository;
    private BlenderRepository $blenderRepository;

//    private AdventCalendar $adventCalendar;

    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        ConfigService $configService,
        Main $main,
        TileRepository $tileRepository,
        NewsRepository $newsRepository,
        ProjectRepository $projectRepository,
        UserRepository $userRepository,
        CPURepository $cpuRepository,
        SessionRepository $sessionRepository,
        BlenderRepository $blenderRepository,
//        AdventCalendar $adventCalendar,
        HTML $html
    ) {
        parent::__construct($main, $router, $entityManager, $configService, $html);
        $this->tileRepository = $tileRepository;
        $this->newsRepository = $newsRepository;
        $this->projectRepository = $projectRepository;
        $this->sessionRepository = $sessionRepository;
        $this->cpuRepository = $cpuRepository;
        $this->userRepository = $userRepository;
        $this->blenderRepository = $blenderRepository;
//        $this->adventCalendar = $adventCalendar;
    }

    #[Route(path: '/')]
    public function index(Request $request): RedirectResponse {
        $tag = $request->get('tag', false);
        if (is_string($tag)) {
            $request->getSession()->set('affiliate', $tag);
        }

        return new RedirectResponse($this->generateUrl('app_home_main'));
    }

    #[Route(path: '/home')]
    public function main(): Response {
        // NEWS
        $all_news2 = $this->newsRepository->findAll();
        $all_news = array();
        $first_news = array();
        foreach (array_reverse($all_news2) as $k => $a_news) {
            if ($a_news->getId() < 0 /*|| $a_news->getId() == '1502207950' */) {
                continue;
            }
            if ($a_news->getHomepage() == '1') {
                array_unshift($first_news, $a_news);
            }
            else {
                array_push($all_news, $a_news);
            }
        }

        usort($all_news, '\App\Utils\Sort::sortByAttributeId');
        foreach (array_reverse($all_news) as $a_news) {
            if (count($first_news) < 4) {
                array_unshift($first_news, $a_news);
            }
        }

        usort($first_news, '\App\Utils\Sort::sortByAttributeId');

        return $this->render('home/main.html.twig', [
            'first_news' => $first_news,
//            'advent_calendar_is_active' => AdventCalendar::isActive(),
            'average_connected_machine' => $this->config['page']['home']['average_connected_machine'],
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/home/machines')]
    public function machines(): Response {
        $rows2 = $this->sessionRepository->findAll();

        $breadcrumb_title = sprintf('Connected machines (%d)', count($rows2));

        $sessions_by_hours = '';
        $sessions_by_day = '';
        $js_values_os = '';
        $js_values_compute_device = '';
        $unique_users = '';
        $js_values = '';
        $js_session_linux = '';
        $js_session_windows = '';
        $js_session_mac = '';
        $js_values_gpu_headless = '';
        $js_values_power_cpu = '';
        $js_values_power_gpu = '';
        $js_values_scheduler = '';
        $sessions = [];

        if (count($rows2) != 0) {
            $unique_users = array();
            $now = time();

            usort($rows2, '\App\Utils\Sort::sortLoginSession');
            $frames_processing = $this->tileRepository->findBy(['status' => Constant::PROCESSING]);

            $datas = $this->main->globalStats(120);

            $sessions_by_hours = "['Time', 'machines'],";
            $sessions_by_day = "['Day', 'machines'],";

            $min_timescope_sessions = time() - ($this->config['graph']['sessions']['timescope'] + 1) * 3600 * 24;
            $days = array();
            foreach ($datas as $i => $data) {
                if ($min_timescope_sessions < $data->getId()) {
                    $sessions_by_hours .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $data->getId())."',".$data->getSessions().'],';
                }

                $day = 3600 * 24 * (int)($data->getId() / (3600 * 24));
                if (isset($days[$day]) == false) {
                    $days[$day] = array('sessions' => 0, 'sessions_elements' => 0);
                }
                $days[$day]['sessions'] += $data->getSessions();
                $days[$day]['sessions_elements'] += 1;
            }

            foreach ($days as $day => $datas1) {
                $sessions_by_day .= "['".$this->html->getTimeWithLocalTimeZone("d-m-Y", $day)."',".(int)($datas1['sessions'] / (float)($datas1['sessions_elements'])).'],';
            }

            $js_values_os = "[['OS', 'Clients'],";
            $oss = $this->sessionRepository->countOS();
            if (array_key_exists('macm1', $oss) && array_key_exists('mac', $oss)) {
                $oss['mac'] += $oss['macm1'];
                unset($oss['macm1']);
            }
            arsort($oss);
            foreach ($oss as $os => $nb) {
                $js_values_os .= '[\''.$os.'\','.$nb.'],';
            }
            $js_values_os .= ']';

            $compute_devices = $this->sessionRepository->countComputeMethod();
            arsort($compute_devices);
            $js_values_compute_device = "[['Type', 'Clients'],";
            foreach ($compute_devices as $type => $value) {
                $type_human = trim(sprintf("%s%s", Misc::isMaskEnabled($type, Constant::COMPUTE_CPU) ? 'CPU ' : '', Misc::isMaskEnabled($type, Constant::COMPUTE_GPU) ? 'GPU ' : ''));
                if ($type_human != '') {
                    $js_values_compute_device .= '[\''.$type_human.'\','.$value.'],';
                }
            }
            $js_values_compute_device .= ']';

            if ($this->getUser()->isModerator()) {
                $versions = array();
                $versions_linux = array();
                $versions_windows = array();
                $versions_mac = array();
                $gpu_headless = array(false => 0, true => 0);
                $powers_cpu = array();
                $powers_gpu = array();
                $schedulers = array();
                foreach ($rows2 as $session) {
                    $version = $session->getVersion();
                    if (array_key_exists($version, $versions) == false) {
                        $versions[$version] = 0;
                    }
                    $versions[$version] += 1;

                    if ($session->getOS() == 'linux') {
                        if (array_key_exists($version, $versions_linux) == false) {
                            $versions_linux[$version] = 0;
                        }
                        $versions_linux[$version] += 1;
                    }
                    if ($session->getOS() == 'mac' || $session->getOS() == 'macm1') {
                        if (array_key_exists($version, $versions_mac) == false) {
                            $versions_mac[$version] = 0;
                        }
                        $versions_mac[$version] += 1;
                    }
                    if ($session->getOS() == 'windows') {
                        if (array_key_exists($version, $versions_windows) == false) {
                            $versions_windows[$version] = 0;
                        }
                        $versions_windows[$version] += 1;
                    }

                    $machine = $session->getComputeDevice();
                    if (is_object($machine)) {
                        if ($machine instanceof CPU) {
                            $n = (int)($this->config['power']['cpu']['const'] * $this->config['graph']['sessions']['step_power']);
                            $power = $n * (int)($machine->getPower() / $n);
                            if (array_key_exists($power, $powers_cpu) == false) {
                                $powers_cpu[$power] = 0;
                            }
                            $powers_cpu[$power] += 1;
                        }
                        else {
                            $n = (int)($this->config['power']['gpu']['const'] * $this->config['graph']['sessions']['step_power']);
                            $power = $n * (int)($machine->getPower() / $n);
                            if (array_key_exists($power, $powers_gpu) == false) {
                                $powers_gpu[$power] = 0;
                            }
                            $powers_gpu[$power] += 1;
                        }

                        if ($machine instanceof GPU) {
                            $gpu_headless[$session->getHeadless()] += 1;
                        }
                    }

                    $scheduler = $session->getScheduler();
                    if ($scheduler == '') {
                        $scheduler = 'default';
                    }
                    if (array_key_exists($scheduler, $schedulers) == false) {
                        $schedulers[$scheduler] = 0;
                    }
                    $schedulers[$scheduler] += 1;

                }

                $js_values = "[['Version', 'Clients'],";
                uksort($versions, '\App\Utils\Sort::sortVersion');
                foreach ($versions as $version => $nb) {
                    if ($version == $this->config['client']['jar']['stable']['version']) {
                        $version = '['.$version.']';
                    }
                    $js_values .= '[\''.$version.'\','.$nb.'],';
                }
                $js_values .= ']';

                $js_session_linux = "[['Version', 'Clients'],";
                uksort($versions_linux, '\App\Utils\Sort::sortVersion');
                foreach ($versions_linux as $version => $nb) {
                    if ($version == $this->config['client']['jar']['stable']['version']) {
                        $version = '['.$version.']';
                    }
                    $js_session_linux .= '[\''.$version.'\','.$nb.'],';
                }
                $js_session_linux .= ']';

                $js_session_windows = "[['Version', 'Clients'],";
                uksort($versions_windows, '\App\Utils\Sort::sortVersion');
                foreach ($versions_windows as $version => $nb) {
                    if ($version == $this->config['client']['jar']['stable']['version']) {
                        $version = '['.$version.']';
                    }
                    $js_session_windows .= '[\''.$version.'\','.$nb.'],';
                }
                $js_session_windows .= ']';

                $js_session_mac = "[['Version', 'Clients'],";
                uksort($versions_mac, '\App\Utils\Sort::sortVersion');
                foreach ($versions_mac as $version => $nb) {
                    if ($version == $this->config['client']['jar']['stable']['version']) {
                        $version = '['.$version.']';
                    }
                    $js_session_mac .= '[\''.$version.'\','.$nb.'],';
                }
                $js_session_mac .= ']';

                $js_values_gpu_headless = "[['Headless', 'Clients'],";
                arsort($gpu_headless);
                foreach ($gpu_headless as $type => $nb) {
                    $js_values_gpu_headless .= '[\''.($type ? 'Headless' : 'With Display').'\','.$nb.'],';
                }
                $js_values_gpu_headless .= ']';

                $js_values_power_cpu = "[['Power', 'Clients'],";
                ksort($powers_cpu);
                foreach ($powers_cpu as $power => $nb) {
                    $val = 100.0 * $power / $this->config['power']['cpu']['const'];
                    $js_values_power_cpu .= '[\''.sprintf("%.0f-%.0f %%", $val, $val + 100.0 * $this->config['graph']['sessions']['step_power']).'\','.$nb.'],';
                }
                $js_values_power_cpu .= ']';

                $js_values_power_gpu = "[['Power', 'Clients'],";
                ksort($powers_gpu);
                foreach ($powers_gpu as $power => $nb) {
                    $val = 100.0 * $power / $this->config['power']['gpu']['const'];
                    $js_values_power_gpu .= '[\''.sprintf("%.0f-%.0f %%", $val, $val + 100.0 * $this->config['graph']['sessions']['step_power']).'\','.$nb.'],';
                }
                $js_values_power_gpu .= ']';

                $js_values_scheduler = "[['Scheduler', 'Clients'],";
                ksort($schedulers);
                foreach ($schedulers as $scheduler => $nb) {
                    $js_values_scheduler .= '[\''.$scheduler.'\','.$nb.'],';
                }
                $js_values_scheduler .= ']';
            }

            $sessions = [];
            foreach ($rows2 as $row) {
                $unique_users[$row->getUser()->getId()] = $row->getUser()->getId();

                $cpu = $this->cpuRepository->find($row->getCpu()->getId());
                if (is_object($cpu)) {

                    $session_enabled = $row->getBlocked() == 0;
                    $session_running = $row->getRunning();

                    $rendering_frames = 0;
                    foreach ($frames_processing as $a_frame) {
                        if ($a_frame->getSession() == $row->getId()) {
                            $rendering_frames++;
                        }
                    }

                    $session_twig = [];
                    $style = '';
                    if (($session_enabled == false || $session_running == false) && $this->getUser()->isModerator()) {
                        $style = 'class="danger"';
                    }
                    $session_twig['style_tr'] = $style;

                    $session_twig['country'] = $row->getCountry();
                    $session_twig['id'] = $row->getId();
                    $session_twig['user_id'] = $row->getUser()->getId();
                    $session_twig['device'] = $row->getComputeDeviceStrHuman();
                    $session_twig['creationtime'] = $row->getCreationTime();
                    $session_twig['last_request'] = $row->getTimestamp();
                    $session_twig['last_validation'] = $row->getLastvalidatedframe();
                    $session_twig['ram_int'] = $row->getComputeDeviceMemoryInMB();
                    $session_twig['renderedFrames_int'] = $row->getRenderedframes();
                    $session_twig['os'] = $row->getOS();
                    $session_twig['version'] = $row->getVersion();
                    $session_twig['version_int'] = Misc::versionToInteger($row->getVersion());
                    $session_twig['is_latest_version'] = $row->getVersion() == $this->config['client']['jar']['stable']['version'];
                    $session_twig['power'] = $row->getHumanPower();
                    $session_twig['power_int'] = $row->getComputeDevice()->getPower();
                    $session_twig['max_rendertime'] = $row->getMaxRendertime();
                    $session_twig['enable'] = $session_enabled;

                    $MB = $row->getComputeDeviceMemoryInMB();
                    if ($MB > 1024) {
                        $session_twig['ram'] = sprintf('%.1f GB', $MB / 1024.0);
                    }
                    else {
                        $session_twig['ram'] = sprintf('%d MB', $MB);
                    }

                    $rendering_frames_str = ''.$row->getRenderedFrames();
                    if ($this->getUser()->isModerator() && $rendering_frames != 0) {
                        if ($rendering_frames > 1) {
                            $rendering_frames_str .= '<span style="color:red">';
                        }
                        $rendering_frames_str .= ' (+'.$rendering_frames.')';
                        if ($rendering_frames > 1) {
                            $rendering_frames_str .= '</span>';
                        }
                    }
                    $session_twig['renderedFrames'] = $rendering_frames_str;


                    $time = $row->getLastvalidatedframe();
                    $time_sort = PHP_INT_MAX;
                    if ($time != 0) {
                        $time_sort = $now - $time;
                    }

                    $last_validation_str = '';
                    if ($time > 0) {
                        $last_validation_str .= '<span ';
                        if ($now - $time < $this->config['session']['timeoutrender'] * 2.1) {
                            $last_validation_str .= 'style= "color: green;"';
                        }
                        else {
                            $last_validation_str .= 'style= "color: red;"';
                        }
                        $last_validation_str .= '>';
                        $last_validation_str .= Misc::humanTime($now - $time, false); // in minutes
                        $last_validation_str .= '</span>';
                    }
                    $session_twig['last_validation_str'] = $last_validation_str;


                    $sessions [] = $session_twig;
                }
            }
        }

        return $this->render('home/machines.html.twig', [
            'title_breadcrumbs' => $breadcrumb_title,
            'sessions_by_hours' => $sessions_by_hours,
            'sessions_by_day' => $sessions_by_day,
            'js_values_os' => $js_values_os,
            'js_values_compute_device' => $js_values_compute_device,
            'unique_users' => $unique_users,
            'piechart_session_version' => $js_values,
            'piechart_session_version_linux' => $js_session_linux,
            'piechart_session_version_windows' => $js_session_windows,
            'piechart_session_version_mac' => $js_session_mac,
            'piechart_gpu_headless' => $js_values_gpu_headless,
            'piechart_session_power_cpu' => $js_values_power_cpu,
            'piechart_session_power_gpu' => $js_values_power_gpu,
            'piechart_session_scheduler' => $js_values_scheduler,
            'sessions' => $sessions,


        ]);
    }

    #[Route(path: '/home/binaries')]
    public function binaries(): Response {
        $blenders = [];

        foreach ($this->blenderRepository->findAll() as $blenderBinary) {
            $blender = [];
            $blender [] = $blenderBinary->getHuman();
            foreach (BlenderArch::SUPPORTED_ARCHS as $arch => $arch_txt) {
                $path = $this->config['storage']['path'].'binaries/'.strtolower($blenderBinary->getId().'_'.$arch.'_64bit').'.'.$this->config['archive']['extension'];
                $blender [] = '<span style="color: '.(file_exists($path) ? 'green;">OK' : 'red;">MISSING').'</span>';
            }

            $blenders [] = $blender;
        }

        $projects = $this->projectRepository->findAll();

        $versions = array();
        foreach ($projects as $project) {
            $version = $project->getExecutable();
            if (array_key_exists($version->getId(), $versions) == false) {
                $versions[$version->getId()] = 0;
            }
            $versions[$version->getId()] += 1;
        }


        $js_project_version_values = "[['Version', 'Projects'],";
        ksort($versions);
        foreach ($versions as $version => $nb) {
            $display = $version;
            foreach ($this->blenderRepository->findAll() as $blenderBinary) {
                if ($blenderBinary->getId() == $version) {
                    $display = $blenderBinary->getHuman();
                }
            }
            $js_project_version_values .= '[\''.$display.'\','.$nb.'],';
        }
        $js_project_version_values .= ']';

        return $this->render('home/binaries.html.twig', [
                'archs' => array_values(BlenderArch::SUPPORTED_ARCHS),
                'blenders' => $blenders,
                'js_project_version_values' => $js_project_version_values,
            ]
        );
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/home/frames')]
    public function frames(): Response {
        $projects = array();
        $tiles_twig = [];
        $tiles = $this->tileRepository->findBy(['status' => Constant::PROCESSING]);
        usort($tiles, '\App\Utils\Sort::sortByAttributeProject');
        foreach ($tiles as $key => $tile) {
            $project = $tile->getFrame()->getProject();
            $name = $project->getName();
            $duration = time() - $tile->getRequestTime();

            if (array_key_exists($name, $projects) == false) {
                $projects[$name] = 0;
            }
            $projects[$name] += 1;

            if ($this->getUser()->isAdmin() == false) {
                if ($project->canManageProject($this->getUser()) == false && $project->canRenderProject($this->getUser()) == false) {
                    unset($tiles[$key]);
                    continue;
                }
            }

            // Project
            $path = $project->getName();
            if ($project->canManageProject($this->getUser())) {
                $path = '<a href="'.$this->generateUrl("app_project_manage", ['project' => $project->getId()]).'">'.$path.'</a>';
            }

            // Estimated time
            $est_time = '';
            $est_time_value = $tile->getEstimatedRenderTime();
            if ($est_time_value != 0) {
                $est_time_value += $project->getAveragePrepTime();
                $est_time = Misc::humanTime($est_time_value, false);
            }

            // Remaining time
            $rem_time = '';
            $rem_time_value = '';
            if ($tile->getRemainingRenderTime() != 0) {
                $rem_time = Misc::humanTime($tile->getRemainingRenderTime());
                $rem_time_value = $tile->getRemainingRenderTime();
            }

            // machine
            $session = $this->sessionRepository->find($tile->getSession());
            $machine_str = 'unknown';
            if (is_object($session)) {
                $machine_str = $session->getRenderingComputeDeviceStrHuman($tile);
            }

            if (($this->getUser()->isAdmin() || $tile->getUser()->getId() == $this->getUser()->getId()) && $tile->getSession() != null) {
                $machine_str = '<a href="'.$this->generateUrl("app_session_single", ['sessionid' => $tile->getSession()]).'">'.$machine_str.'</a>';
            }

            $tiles_twig [] = [
                'id' => $tile->getId(),
                'owner' => $tile->getUser()->getId(),
                'project_name' => $project->getName(),
                'project_path' => $path,
                'frame_number' => $tile->getFrame()->getNumber(),
                'duration' => $duration,
                'machine_str' => $machine_str,
                'est_time' => $est_time,
                'est_time_value' => $est_time_value,
                'rem_time' => $rem_time,
                'rem_time_value' => $rem_time_value,
                'session' => $session,
            ];
        }


        $datas = $this->main->globalStats(max($this->config['graph']['frame_rendered']['timescope'], $this->config['graph']['frame_remaining']['timescope']));

        $line_remaining_frames_by_hour = "['Time', 'frames_remaining'],";
        $line_remaining_frames_public_by_hour = "['Time', 'Remaining frames'],";
        $line_rendered_frames_by_day = "['Time', 'rendered frames'],";
        $line_time_ordered_per_day = "['Time', 'render time (in days)'],";

        $min_timescope_frame_remaining = time() - ($this->config['graph']['frame_remaining']['timescope'] + 1) * 3600 * 24;
        $days = array();
        foreach ($datas as $data) {
            if ($min_timescope_frame_remaining < $data->getId()) {
                $line_remaining_frames_by_hour .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $data->getId())."',".$data->getFramesRemaining().'],';
                $line_remaining_frames_public_by_hour .= "['".$this->html->getTimeWithLocalTimeZone("d-m H:i", $data->getId())."',".$data->getFramesRemainingPublic().'],';
            }

            $day = 3600 * 24 * (int)($data->getId() / (3600 * 24));
            if (isset($days[$day]) == false) {
                $days[$day] = array('min' => PHP_INT_MAX, 'max' => 0, 'min_time' => PHP_INT_MAX, 'max_time' => 0);
            }

            if ($data->getFramesRendered() < $days[$day]['min']) {
                $days[$day]['min'] = $data->getFramesRendered();
            }
            if ($data->getFramesRendered() > $days[$day]['max']) {
                $days[$day]['max'] = $data->getFramesRendered();
            }
            if ($data->getTimeOrdered() < $days[$day]['min_time']) {
                $days[$day]['min_time'] = $data->getTimeOrdered();
            }
            if ($data->getTimeOrdered() > $days[$day]['max_time']) {
                $days[$day]['max_time'] = $data->getTimeOrdered();
            }
        }

        $day = 3600 * 24 * (int)(time() / (3600 * 24));
        if (isset($days[$day]) == false) {
            $days[$day] = array('min' => PHP_INT_MAX, 'max' => 0, 'min_time' => PHP_INT_MAX, 'max_time' => 0);
        }

        $userStats = $this->userRepository->getSumStats();
        $frames_rendered = intval($userStats['rendered_frames']);
        $time_ordered = intval($userStats['ordered_frames_time']);

        if ($frames_rendered < $days[$day]['min']) {
            $days[$day]['min'] = $frames_rendered;
        }
        if ($frames_rendered > $days[$day]['max']) {
            $days[$day]['max'] = $frames_rendered;
        }
        if ($time_ordered < $days[$day]['min_time']) {
            $days[$day]['min_time'] = $time_ordered;
        }
        if ($time_ordered > $days[$day]['max_time']) {
            $days[$day]['max_time'] = $time_ordered;
        }

        $day_before = 3600 * 24 * (int)((time() / (3600 * 24)) - 1);
        if (isset($days[$day_before]) == true) {
            $days[$day]['min'] = $days[$day_before]['max'];
        }
        $day_before = 3600 * 24 * (int)((time() / (3600 * 24)) - 1);
        if (isset($days[$day_before]) == true) {
            $days[$day]['min_time'] = $days[$day_before]['max_time'];
        }
        unset($days[$day]);

        reset($days);
        unset($days[key($days)]);

        $min_timescope_frame_rendered = time() - ($this->config['graph']['frame_rendered']['timescope'] + 1) * 3600 * 24;
        foreach ($days as $day => $datas1) {
            if ($min_timescope_frame_rendered <= $day) {
                $line_rendered_frames_by_day .= "['".$this->html->getTimeWithLocalTimeZone("M d", $day)."',".($datas1['max'] - $datas1['min']).'],';
                $line_time_ordered_per_day .= "['".$this->html->getTimeWithLocalTimeZone("M d", $day)."',".((int)(($datas1['max_time'] - $datas1['min_time']) / 86400)).'],';
            }
        }

        arsort($projects);
        $project_distribution_str = "['Project', 'rendering frames'],";
        foreach ($projects as $k => $v) {
            $project_distribution_str .= "['".addslashes($k)."',$v],";
        }

        return $this->render('home/frames.html.twig', [
            'project_distribution_str' => $project_distribution_str,
            'line_time_ordered_per_day' => $line_time_ordered_per_day,
            'line_rendered_frames_by_day' => $line_rendered_frames_by_day,
            'line_remaining_frames_public_by_hour' => $line_remaining_frames_public_by_hour,
            'line_remaining_frames_by_hour' => $line_remaining_frames_by_hour,
            'tiles' => $tiles_twig,
        ]);
    }

    #[IsGranted('ROLE_USER')]
    #[Route(path: '/home/projects')]
    public function projects(): Response {
        $all_projects = $this->projectRepository->findAll();
        $active_projects = array();
        $private_projects = array();
        $blocked_projects = array();

        $active_projects_count = 0;
        if (count($all_projects) > 0) {
            foreach ($all_projects as $project) {
                if ($project->shouldHideFromUser()) {
                    continue;
                }
                $status = $project->getStatus();
                if ($status == Constant::WAITING || $status == Constant::PROCESSING) {
                    if ($project->IsBlocked() && ($this->getUser()->isModerator() || $project->canManageProject($this->getUser()) == false)) {
                        $blocked_projects [] = $project;
                    }
                    else {

                        if ($project->getCachedStatistics()['total'] > 0 && ($project->canManageProject($this->getUser()) == true || $project->canRenderProject($this->getUser()) == true)) {
                            if ($this->getUser()->isModerator() && $project->getPublicRender() == false) {
                                $private_projects [] = $project;
                            }
                            else {
                                $active_projects [] = $project;
                            }
                        }
                    }
                }
            }
            $active_projects_count = count($active_projects);
        }

        $breadcrumb_title = sprintf('Active projects (%s)', $active_projects_count);
        $extras = array();
        if ($this->getUser()->isModerator()) {
            $extras [] = 'creation_time';
            $extras [] = 'last_validation';
            $extras [] = 'average_rendertime';
            $extras [] = 'cluster';
            $extras [] = 'user_error';
            $extras [] = 'gpu_memory_used';
            $extras [] = 'points_on_creation';
            $extras [] = 'shepherd';
        }
        $extras [] = 'memory_used';

        usort($active_projects, '\App\Utils\Sort::sortProjectByOwnerPoints');

        return $this->render('home/projects.html.twig', [
            'title_breadcrumbs' => $breadcrumb_title,
            'extras' => $extras,
            'active_projects' => $active_projects,
            'private_projects' => $private_projects,
            'blocked_projects' => $blocked_projects,
            'active_extras' => $extras,
            'private_extras' => $extras,
            'blocked_extras' => array_merge($extras, ['blocked']),
        ]);
    }
}
