<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Controller;

use App\Base\BaseController;
use App\Entity\GPU;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * GPU controller
 */
#[Route(path: '/gpu')]
class GPUController extends BaseController {
    /**
     * Reset power on a gpu
     */
    #[IsGranted('ROLE_MOD')]
    #[Route(path: '/{gpu}/reset')]
    public function reset(GPU $gpu): Response {
        $gpu->setRenderedFrames(0);
        $gpu->setPower(0);

        $this->entityManager->flush($gpu);

        return new Response('OK');
    }
}
