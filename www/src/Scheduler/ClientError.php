<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler;

class ClientError {
    // id have to be kept synchronised with the client side.
    public const TYPE_OK = 0;
    public const TYPE_UNKNOWN = 99;
    public const TYPE_WRONG_CONFIGURATION = 1;
    public const TYPE_AUTHENTICATION_FAILED = 2;
    public const TYPE_TOO_OLD_CLIENT = 3;
    public const TYPE_SESSION_DISABLED = 4;
    public const TYPE_RENDERER_NOT_AVAILABLE = 5;
    public const TYPE_MISSING_RENDER = 6;
    public const TYPE_MISSING_SCENE = 7;
    public const TYPE_NOOUTPUTFILE = 8;
    public const TYPE_DOWNLOAD_FILE = 9;
    public const TYPE_CAN_NOT_CREATE_DIRECTORY = 10;
    public const TYPE_NETWORK_ISSUE = 11;
    public const TYPE_RENDERER_CRASHED = 12;
    public const TYPE_RENDERER_OUT_OF_VIDEO_MEMORY = 13;
    public const TYPE_RENDERER_KILLED = 14;
    public const TYPE_RENDERER_MISSING_LIBRARIES = 15;
    public const TYPE_FAILED_TO_EXECUTE = 16;
    public const TYPE_OS_NOT_SUPPORTED = 17;
    public const TYPE_CPU_NOT_SUPPORTED = 18;
    public const TYPE_GPU_NOT_SUPPORTED = 19;
    public const TYPE_RENDERER_KILLED_BY_USER = 20;
    public const TYPE_RENDERER_OUT_OF_MEMORY = 21;
    public const TYPE_RENDERER_KILLED_SERVER = 22;
    public const TYPE_RENDERER_KILLED_BY_USER_OVER_TIME = 23;
    public const TYPE_RENDERER_KILLED_BY_USER_INCPOMPATIBLE_PROCESS = 34;
    public const TYPE_RENDERER_CRASHED_PYTHON_ERROR = 24;
    public const TYPE_FRAME_VALIDATION_FAILED = 25;
    public const TYPE_IMAGE_WRONG_DIMENSION = 26;
    public const TYPE_ENGINE_NOT_AVAILABLE = 27;
    public const TYPE_CURRENTLY_HEADLESS = 28;
    public const TYPE_DENOISING_NOT_SUPPORTED = 29;
    public const TYPE_CANNOT_READ_FILE = 30;
    public const TYPE_DETECT_DEVICE_ERROR = 31;
    public const TYPE_GPU_OR_DRIVER_ERROR = 32;
    public const TYPE_COLOR_MANAGEMENT = 33;
    public const TYPE_CACHE_DIR_DELETED = 35;
    public const TYPE_NO_SPACE_LEFT_ON_DEVICE = 100;
    public const TYPE_BAD_UPLOAD_RESPONSE = 101;
    public const TYPE_SERVER_DOWN = 104;

    public static function toHuman(int|string $type): string {
        // Have to be the same as client file: src/com/sheepit/client/Error.java
        switch ($type) {
            case "null":
                return 'Unknow(string null)';
            case "":
                return 'Unknow(empty)';
            case ClientError::TYPE_OK:
                return 'No error';
            case ClientError::TYPE_WRONG_CONFIGURATION:
                return 'Wrong configuration';
            case ClientError::TYPE_AUTHENTICATION_FAILED:
                return 'Authentication failed';
            case ClientError::TYPE_TOO_OLD_CLIENT:
                return 'Too old client';
            case ClientError::TYPE_SESSION_DISABLED:
                return 'Session disabled';
            case ClientError::TYPE_RENDERER_NOT_AVAILABLE:
                return 'Renderer not available';
            case ClientError::TYPE_MISSING_RENDER:
                return 'Missing renderer';
            case ClientError::TYPE_MISSING_SCENE:
                return 'Missing project';
            case ClientError::TYPE_NOOUTPUTFILE:
                return 'No outputfile';
            case ClientError::TYPE_DOWNLOAD_FILE:
                return 'Download file';
            case ClientError::TYPE_CAN_NOT_CREATE_DIRECTORY:
                return 'Can not create directory';
            case ClientError::TYPE_NETWORK_ISSUE:
                return 'Network issue';
            case ClientError::TYPE_RENDERER_CRASHED:
                return 'Renderer crashed';
            case ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR:
                return 'Python error';
            case ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY:
                return 'Renderer out of video memory';
            case ClientError::TYPE_RENDERER_KILLED:
                return 'Renderer killed';
            case ClientError::TYPE_RENDERER_MISSING_LIBRARIES:
                return 'Renderer missing libraries';
            case ClientError::TYPE_FAILED_TO_EXECUTE:
                return 'Failed to execute';
            case ClientError::TYPE_OS_NOT_SUPPORTED:
                return 'OS not supported';
            case ClientError::TYPE_CPU_NOT_SUPPORTED:
                return 'CPU not supported';
            case ClientError::TYPE_GPU_NOT_SUPPORTED:
                return 'GPU not supported';
            case ClientError::TYPE_GPU_OR_DRIVER_ERROR:
                return 'GPU driver issue';
            case ClientError::TYPE_DETECT_DEVICE_ERROR:
                return 'Failed to detect render device';
            case ClientError::TYPE_CANNOT_READ_FILE:
                return 'Failed to read the project files.';
            case ClientError::TYPE_RENDERER_KILLED_BY_USER:
                return 'User has blocked the project';
            case ClientError::TYPE_RENDERER_KILLED_SERVER:
                return 'Server has killed the rendering';
            case ClientError::TYPE_RENDERER_KILLED_BY_USER_OVER_TIME:
                return 'User has killed the rendering du to over time settings';
            case ClientError::TYPE_RENDERER_KILLED_BY_USER_INCPOMPATIBLE_PROCESS:
                return 'User has killed the rendering due to incompatible process';
            case ClientError::TYPE_RENDERER_OUT_OF_MEMORY:
                return 'Renderer out of memory';
            case ClientError::TYPE_FRAME_VALIDATION_FAILED:
                return 'Frame validation on Shepherd failed';
            case ClientError::TYPE_IMAGE_WRONG_DIMENSION:
                return 'Wrong image dimension';
            case ClientError::TYPE_ENGINE_NOT_AVAILABLE:
                return 'Engine not available';
            case ClientError::TYPE_NO_SPACE_LEFT_ON_DEVICE:
                return 'No free space on device';
            case ClientError::TYPE_BAD_UPLOAD_RESPONSE:
                return 'Failed to upload to shepherd';
            case ClientError::TYPE_CURRENTLY_HEADLESS:
                return 'Currently headless';
            case ClientError::TYPE_DENOISING_NOT_SUPPORTED:
                return 'Denoising is not supported on this device';
            case ClientError::TYPE_COLOR_MANAGEMENT:
                return 'Color management';
            case ClientError::TYPE_SERVER_DOWN:
                return 'Server down';
            case ClientError::TYPE_CACHE_DIR_DELETED:
                return 'Cache-dir deleted';
            case ClientError::TYPE_UNKNOWN:
                return 'unknown';
            case null:
                return 'Unknow(null)';
            default:
                return 'Unknow('.$type.')';
        }
    }

    public static function fromClientLog(string $str): int|string {
        // Have to be the same as client file: src/com/sheepit/client/Error.java
        switch ($str) {
            case 'OK':
                return ClientError::TYPE_OK;
            case 'UNKNOWN':
                return ClientError::TYPE_UNKNOWN;
            case 'WRONG_CONFIGURATION':
                return ClientError::TYPE_WRONG_CONFIGURATION;
            case 'AUTHENTICATION_FAILED':
                return ClientError::TYPE_AUTHENTICATION_FAILED;
            case 'TOO_OLD_CLIENT':
                return ClientError::TYPE_TOO_OLD_CLIENT;
            case 'SESSION_DISABLED':
                return ClientError::TYPE_SESSION_DISABLED;
            case 'RENDERER_NOT_AVAILABLE':
                return ClientError::TYPE_RENDERER_NOT_AVAILABLE;
            case 'MISSING_RENDER':   // old spelling
                return ClientError::TYPE_MISSING_RENDER;
            case 'MISSING_RENDERER': // new spelling
                return ClientError::TYPE_MISSING_RENDER;
            case 'MISSING_SCENE':
                return ClientError::TYPE_MISSING_SCENE;
            case 'NOOUTPUTFILE':
                return ClientError::TYPE_NOOUTPUTFILE;
            case 'DOWNLOAD_FILE':
                return ClientError::TYPE_DOWNLOAD_FILE;
            case 'CAN_NOT_CREATE_DIRECTORY':
                return ClientError::TYPE_CAN_NOT_CREATE_DIRECTORY;
            case 'NETWORK_ISSUE':
                return ClientError::TYPE_NETWORK_ISSUE;
            case 'RENDERER_CRASHED':
                return ClientError::TYPE_RENDERER_CRASHED;
            case 'RENDERER_CRASHED_PYTHON_ERROR':
                return ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR;
            case 'RENDERER_OUT_OF_VIDEO_MEMORY':
                return ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY;
            case 'RENDERER_KILLED':
                return ClientError::TYPE_RENDERER_KILLED;
            case 'RENDERER_MISSING_LIBRARIES':
                return ClientError::TYPE_RENDERER_MISSING_LIBRARIES;
            case 'FAILED_TO_EXECUTE':
                return ClientError::TYPE_FAILED_TO_EXECUTE;
            case 'OS_NOT_SUPPORTED':
                return ClientError::TYPE_OS_NOT_SUPPORTED;
            case 'CPU_NOT_SUPPORTED':
                return ClientError::TYPE_CPU_NOT_SUPPORTED;
            case 'GPU_NOT_SUPPORTED':
                return ClientError::TYPE_GPU_NOT_SUPPORTED;
            case 'GPU_OR_DRIVER_ERROR':
                return ClientError::TYPE_GPU_OR_DRIVER_ERROR;
            case 'RENDERER_KILLED_BY_USER':
                return ClientError::TYPE_RENDERER_KILLED_BY_USER;
            case 'RENDERER_KILLED_BY_SERVER':
                return ClientError::TYPE_RENDERER_KILLED_SERVER;
            case 'RENDERER_KILLED_BY_USER_OVER_TIME':
                return ClientError::TYPE_RENDERER_KILLED_BY_USER_OVER_TIME;
            case 'RENDERER_KILLED_BY_USER_INCOMPATIBLE_PROCESS':
                return ClientError::TYPE_RENDERER_KILLED_BY_USER_INCPOMPATIBLE_PROCESS;
            case 'RENDERER_OUT_OF_MEMORY':
                return ClientError::TYPE_RENDERER_OUT_OF_MEMORY;
            case 'DENOISING_NOT_SUPPORTED':
                return ClientError::TYPE_DENOISING_NOT_SUPPORTED;
            case 'VALIDATION_FAILED':
                return ClientError::TYPE_FRAME_VALIDATION_FAILED;
            case 'IMAGE_WRONG_DIMENSION':
                return ClientError::TYPE_IMAGE_WRONG_DIMENSION;
            case 'ENGINE_NOT_AVAILABLE':
                return ClientError::TYPE_ENGINE_NOT_AVAILABLE;
            case 'NO_SPACE_LEFT_ON_DEVICE':
                return ClientError::TYPE_NO_SPACE_LEFT_ON_DEVICE;
            case 'ERROR_BAD_UPLOAD_RESPONSE':
                return ClientError::TYPE_BAD_UPLOAD_RESPONSE;
            case 'SERVER_DOWN':
                return ClientError::TYPE_SERVER_DOWN;
            case 'CANNOT_READ_FILE':
                return ClientError::TYPE_CANNOT_READ_FILE;
            case 'DETECT_DEVICE_ERROR':
                return ClientError::TYPE_DETECT_DEVICE_ERROR;
            case 'CURRENTLY_HEADLESS':
                return ClientError::TYPE_CURRENTLY_HEADLESS;
            case 'COLOR_MANAGEMENT_ERROR':
                return ClientError::TYPE_COLOR_MANAGEMENT;
            case 'TYPE_CACHE_DIR_DELETED':
                return ClientError::TYPE_CACHE_DIR_DELETED;
            default:
                return $str;
        }
    }

    public static function getErrorTypeFromFile(string $path): string {
        //    06-05 11:49:12 (error) Client::work problem with runRenderer (ret NOOUTPUTFILE)
        //    06-05 11:49:12 (debug) Sending error to server (type: NOOUTPUTFILE)

        $ret = null;
        $lines = file_get_contents($path);

        $token = 'Fatal Python error: Py_Initialize';
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'RENDERER_CRASHED_PYTHON_ERROR';
        }

        $token = 'ERROR_BAD_UPLOAD_RESPONSE';
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'ERROR_BAD_UPLOAD_RESPONSE';
        }

        $token = 'JOB_VALIDATION_ERROR_SESSION_DISABLED';
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SESSION_DISABLED';
        }

        $token = 'JOB_VALIDATION_ERROR_BROKEN_MACHINE';
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SESSION_DISABLED';
        }

        $token = "Unexpected response from HTTP Stacktimeout"; // missing space on the client side
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SERVER_DOWN';
        }

        $token = "Unexpected response from HTTP StackConnection reset"; // missing space on the client side
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SERVER_DOWN';
        }

        $token = "Unexpected response from HTTP StackSoftware caused connection abort: socket write error"; // missing space on the client side
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SERVER_DOWN';
        }

        $token = "Unexpected response from HTTP StackFailed to connect to client.sheepit-renderfarm.com"; // missing space on the client side
        $n = strpos($lines, $token);
        if ($n !== false) {
            return 'SERVER_DOWN';
        }
        $token = 'Sending error to server (type: ';
        $n = strpos($lines, $token);
        if ($n !== false) {
            $expl = explode(')', substr($lines, $n + strlen($token)));
            $ret = $expl[0];
        }
        if (is_null($ret)) {
            // old version
            $token = 'Client::work problem with runRenderer (ret ';
            $n = strpos($lines, $token);
            if ($n !== false) {
                $expl = explode(')', substr($lines, $n + strlen($token)));
                $ret = $expl[0];
            }
        }
        if (is_null($ret)) {
            // old version
            $token = 'Client::work problem with runRenderer (ret ';
            $n = strpos($lines, $token);
            if ($n !== false) {
                $expl = explode(')', substr($lines, $n + strlen($token)));
                $ret = $expl[0];
            }
        }
        if (is_null($ret)) {
            // old version
            $token = 'Client::work problem with downloadSceneFile (ret -1)';
            $n = strpos($lines, $token);
            if ($n !== false) {
                return 'DOWNLOAD_FILE'; //ClientError::type_download_file;
            }
            $token = 'Client::work problem with runRenderer (ret NOOUTPUTFILE)';
            $n = strpos($lines, $token);
            if ($n !== false) {
                return 'NOOUTPUTFILE';
            }
            $token = 'Client::work job preparation failed (scene';
            $n = strpos($lines, $token);
            if ($n !== false) {
                return 'MISSING_SCENE';
            }
        }

        if (is_null($ret)) {
            $token = 'ERROR Type :: ';
            $n = strpos($lines, $token);
            if ($n !== false) {
                $ret = substr($lines, $n + strlen($token), strpos(substr($lines, $n + strlen($token)), "\n"));
            }
        }

        return $ret;
    }

    public static function shouldBeReportedOnJobError(int $type): bool {
        switch ($type) {
            case ClientError::TYPE_WRONG_CONFIGURATION:
            case ClientError::TYPE_OK:
            case ClientError::TYPE_AUTHENTICATION_FAILED:
            case ClientError::TYPE_TOO_OLD_CLIENT:
            case ClientError::TYPE_SESSION_DISABLED:
            case ClientError::TYPE_RENDERER_NOT_AVAILABLE:
            case ClientError::TYPE_MISSING_RENDER:
            case ClientError::TYPE_MISSING_SCENE:
            case ClientError::TYPE_DOWNLOAD_FILE:
            case ClientError::TYPE_CAN_NOT_CREATE_DIRECTORY:
            case ClientError::TYPE_NETWORK_ISSUE:
            case ClientError::TYPE_RENDERER_MISSING_LIBRARIES:
            case ClientError::TYPE_OS_NOT_SUPPORTED:
            case ClientError::TYPE_CPU_NOT_SUPPORTED:
            case ClientError::TYPE_GPU_NOT_SUPPORTED:
            case ClientError::TYPE_RENDERER_KILLED_BY_USER:
            case ClientError::TYPE_RENDERER_KILLED_SERVER:
            case ClientError::TYPE_RENDERER_KILLED_BY_USER_OVER_TIME:
            case ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR:
            case ClientError::TYPE_RENDERER_KILLED_BY_USER_INCPOMPATIBLE_PROCESS:
            case ClientError::TYPE_DENOISING_NOT_SUPPORTED:
            case ClientError::TYPE_SERVER_DOWN:
            case ClientError::TYPE_COLOR_MANAGEMENT:
                return false;

            case ClientError::TYPE_RENDERER_KILLED:
            case ClientError::TYPE_NOOUTPUTFILE:
            case ClientError::TYPE_RENDERER_CRASHED:
            case ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY:
            case ClientError::TYPE_RENDERER_OUT_OF_MEMORY:
            case ClientError::TYPE_FAILED_TO_EXECUTE:
            case ClientError::TYPE_ENGINE_NOT_AVAILABLE:
            case ClientError::TYPE_UNKNOWN:
            default:
                return true;
        }
    }

    /**
     * To be use on compute method and power detection frame
     **/
    public static function shouldDisableTheSession(int $type): bool {
        switch ($type) {
            case ClientError::TYPE_RENDERER_NOT_AVAILABLE:
            case ClientError::TYPE_CAN_NOT_CREATE_DIRECTORY:
            case ClientError::TYPE_RENDERER_MISSING_LIBRARIES:
            case ClientError::TYPE_OS_NOT_SUPPORTED:
            case ClientError::TYPE_CPU_NOT_SUPPORTED:
            case ClientError::TYPE_GPU_NOT_SUPPORTED:
            case ClientError::TYPE_RENDERER_CRASHED:
            case ClientError::TYPE_RENDERER_OUT_OF_VIDEO_MEMORY:
            case ClientError::TYPE_RENDERER_OUT_OF_MEMORY:
            case ClientError::TYPE_FAILED_TO_EXECUTE:
            case ClientError::TYPE_RENDERER_CRASHED_PYTHON_ERROR:
            case ClientError::TYPE_DENOISING_NOT_SUPPORTED:
            case ClientError::TYPE_CANNOT_READ_FILE:
            case ClientError::TYPE_DETECT_DEVICE_ERROR:
            case ClientError::TYPE_CACHE_DIR_DELETED:
            case ClientError::TYPE_UNKNOWN:
                return true;

            default:
                return false;
        }
    }

    /**
     * @return int negative to nothing, positive: block code
     */
    public static function shouldBlockProject(string $contents): int {
        if (preg_match("/Requested OSL group data size \(\d+\) is greater than the maximum supported with OptiX \(\d+\)/", $contents) == 1) {
            return 15; // see Project::blocked_messages
        }
        return -1;
    }
}
