<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler;

use App\Constant;
use App\Entity\CPU;
use App\Entity\Event;
use App\Entity\GPU;
use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Tile;
use App\Entity\TilePowerDetection;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Entity\WebSessionClient;
use App\Repository\BlenderArchRepository;
use App\Repository\CPURepository;
use App\Repository\GPURepository;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Repository\UserPublicKeyRepository;
use App\Repository\UserRepository;
use App\Repository\WebSessionClientRepository;
use App\Service\Logger;
use App\Service\Main;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Handle client with version 6.xxx.xxx
 */
class ClientProtocol60 implements ClientProtocol {
    public const MINIMUM_VERSION = "6.0.0";

    protected string $version;
    protected ?Session $session;
    protected UrlGeneratorInterface $router;
    protected EntityManagerInterface $entityManager;
    protected CPURepository $CPURepository;
    protected GPURepository $GPURepository;
    protected WebSessionClientRepository $webSessionClientRepository;
    protected UserPublicKeyRepository $userPublicKeyRepository;
    protected UserRepository $userRepository;
    protected TileRepository $tileRepository;
    protected ProjectRepository $projectRepository;
    protected SessionRepository $sessionRepository;
    protected HWIDBlacklistRepository $HWIDBlacklistRepository;
    protected BlenderArchRepository $blenderArchRepository;
    protected Main $main;
    protected SessionInterface $phpSession;
    protected array $config;

    public function __construct(
        string $version_,
        ?Session $session_,
        UrlGeneratorInterface $router,
        SessionInterface $phpSession,
        EntityManagerInterface $entityManager,
        Main $main,
        SessionRepository $sessionRepository,
        HWIDBlacklistRepository $HWIDBlacklistRepository,
        ProjectRepository $projectRepository,
        TileRepository $tileRepository,
        CPURepository $CPURepository,
        GPURepository $GPURepository,
        UserRepository $userRepository,
        UserPublicKeyRepository $userPublicKeyRepository,
        WebSessionClientRepository $webSessionClientRepository,
        BlenderArchRepository $blenderArchRepository,
        array $config
    ) {
        $this->version = $version_;
        $this->session = $session_;
        $this->router = $router;
        $this->phpSession = $phpSession;
        $this->entityManager = $entityManager;
        $this->main = $main;
        $this->sessionRepository = $sessionRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;
        $this->projectRepository = $projectRepository;
        $this->tileRepository = $tileRepository;
        $this->CPURepository = $CPURepository;
        $this->GPURepository = $GPURepository;
        $this->userRepository = $userRepository;
        $this->userPublicKeyRepository = $userPublicKeyRepository;
        $this->webSessionClientRepository = $webSessionClientRepository;
        $this->blenderArchRepository = $blenderArchRepository;
        $this->config = $config;
    }

    public function __toString(): string {
        return static::class.'(version:'.$this->version.', session: '.$this->session.')';
    }

    public function getSession(): Session {
        return $this->session;
    }

    public function userAuth(array $request_args): array {
        $user = null;
        $publickey_id = null;

        if (array_key_exists('login', $request_args) && $request_args['login'] != '' && array_key_exists('password', $request_args) && $request_args['password'] != '') {
            $requestUser = $this->userRepository->find($request_args['login']);
            if (is_object($requestUser) == false) {
                Logger::debug(__METHOD__." user '".$request_args['login']."' does not exist");
                $error_state = ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED;
            }
            else {
                if ($requestUser->authenticate($request_args['password'])) {
                    $this->entityManager->refresh($requestUser); // on phpunit, fetch the renderkey, in prod: useless :(
                    $user = $requestUser;
                    $error_state = ClientProtocol::OK;

                    $publicKey = $requestUser->getPublicKeyFromComment('client');
                    if (is_object($publicKey)) {
                        $publickey_id = $publicKey;
                    }
                    else {
                        $publickey_id = $this->userPublicKeyRepository->add($requestUser, 'client');
                    }
                }
                else {
                    $publicKey = $this->userPublicKeyRepository->find($request_args['password']);
                    if (is_object($publicKey) && $publicKey->getUser()->getId() == $requestUser->getId()) {
                        $user = $requestUser;
                        $error_state = ClientProtocol::OK;
                        $publickey_id = $publicKey;
                    }
                    else {
                        $error_state = ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED;
                        Logger::debug(__METHOD__.' user '.$requestUser->getId().' failed to authenticate');
                    }
                }
            }
        }
        else {
            $error_state = ClientProtocol::CONFIGURATION_ERROR_AUTH_FAILED;
        }
        return array($error_state, $user, $publickey_id);
    }

    public function configInput(User $user, ?UserPublicKey $renderKey, array $request_args): int {
        if (array_key_exists('os', $request_args) && array_key_exists('bits', $request_args) && array_key_exists('cpu_model_name', $request_args) && array_key_exists('cpu_cores', $request_args) && array_key_exists('ram', $request_args)) {
            if ($request_args['bits'] != '64bit') {
                // only support 64bits now
                return ClientProtocol::CONFIGURATION_ERROR_ARCH_NO_SUPPORTED;
            }

            $cpu = new CPU();
            $cpu->setModel($request_args['cpu_model_name']);
            $cpu->setCores($request_args['cpu_cores']);

            $cpu_sql = $this->CPURepository->findFromData($cpu);

            if (is_object($cpu_sql) == false) { // does the cpu exists?
                $this->entityManager->persist($cpu);
                $this->entityManager->flush($cpu);
            }
            else {
                $cpu = $cpu_sql;
            }

            $need_new_session = false;
            $web_session = $this->main->importWebSessionClientFromCookie();
            if (is_object($web_session)) {
                Logger::debug(__METHOD__.' a session is already exists, try to continue it '.$web_session->getData());
                // a session is already exists, try to continue it

                $new_session = $web_session->loadSessionFromData();
                if (is_object($new_session)) {

                    // override attribute from actual request
                    if (array_key_exists('hostname', $request_args)) {
                        $new_session->setHostname($request_args['hostname']);
                    }
                    $new_session->setVersion($this->version);
                    $new_session->sethwid(array_key_exists('hwid', $request_args) ? $request_args['hwid'] : '');
                    $new_session->setNoBigDownload(array_key_exists('disable_large_downloads', $request_args) && $request_args['disable_large_downloads'] == '1');
                    $new_session->setRenderKey($renderKey);

                    $this->entityManager->persist($new_session);
                    $this->entityManager->flush($new_session);
                    $this->session = $new_session;

                    Logger::debug('re-use session login=\''.$new_session->getUser()->getId().'\' cpu=\''.$cpu->getModel().'\'x'.$cpu->getCores().' OS='.$new_session->getOS().' client_version='.$new_session->getVersion().' session_id='.$new_session->getId());
                }
                else {
                    $need_new_session = true;
                    Logger::debug(__METHOD__.' failed to used old session ');
                }
            }
            else {
                $need_new_session = true;
            }
            if ($need_new_session) {
                Logger::debug(__METHOD__.' no session create a new one');
                // no session create a new one
                $new_session = new Session();
                $new_session->setUser($user);
                $new_session->setCpu($cpu);
                $new_session->setTimestamp(time());
                $new_session->setOS($request_args['os']);
                $new_session->setVersion($this->version);
                $new_session->setCreationtime(time());
                $new_session->setComputeMethod(0);
                $new_session->setGpu(null);
                $new_session->setMemory($request_args['ram']);
                $new_session->setBlocked(0);
                $new_session->setRunning(true);
                $new_session->setUserAgent(@$_SERVER['HTTP_USER_AGENT'] ?? '');
                $new_session->setHeadless(array_key_exists('headless', $request_args) && $request_args['headless']);
                $new_session->sethwid(array_key_exists('hwid', $request_args) ? $request_args['hwid'] : '');
                $new_session->setNoBigDownload(array_key_exists('disable_large_downloads', $request_args) && $request_args['disable_large_downloads'] == '1');
                $new_session->setRenderKey($renderKey);
                if (array_key_exists('extras', $request_args)) {
                    $new_session->setExtras($request_args['extras']);
                }
                if (array_key_exists('hostname', $request_args)) {
                    $new_session->setHostname($request_args['hostname']);
                }
                if (array_key_exists('ui', $request_args)) {
                    $new_session->setUi(filter_var($request_args['ui'], FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                }

                // hack for compatibility with old client
                // to be removed once the minimum client version is over 7.23029.0
                if ($this->version != '6.0.0' && Misc::versionToInteger($this->version) < Misc::versionToInteger('7.23029.0')) {
                    $new_session->setPowerCpu($cpu->getPower());
                }
                // end of hack


                $this->entityManager->persist($new_session);
                $this->entityManager->flush($new_session);

                $saved_web_session = $this->webSessionClientRepository->loadFromSessionId($new_session->getId());
                if (is_object($saved_web_session)) {
                    $this->webSessionClientRepository->remove($saved_web_session); // override the web session
                }
                $web_session = new WebSessionClient();
                $web_session->setUser($new_session->getUser());
                $web_session->setExpirationTime(0);
                $web_session->setSessionId($new_session->getId());
                $web_session->createKey();
                $this->entityManager->persist($web_session);
                $this->entityManager->flush($web_session);
                $web_session->saveToCookie();

                $this->session = $new_session;

                Logger::debug('new session login=\''.$new_session->getUser()->getId().'\' cpu=\''.$cpu->getModel().'\'x'.$cpu->getCores().' OS='.$new_session->getOS().' client_version='.$new_session->getVersion().' session_id='.$new_session->getId());
            }

            return ClientProtocol::OK;
        }
        else {
            Logger::debug(__METHOD__.' missing parameter os:'.serialize(array_key_exists('os', $request_args)).' bits:'.serialize(array_key_exists('bits', $request_args)).' cpu_family:'.serialize(array_key_exists('cpu_family', $request_args)).' cpu_model:'.serialize(array_key_exists('cpu_model', $request_args)).' cpu_model_name:'.serialize(array_key_exists('cpu_model_name', $request_args)).' cpu_cores:'.serialize(array_key_exists('cpu_cores', $request_args)).' ram:'.serialize(array_key_exists('ram', $request_args)).' request:'.serialize($request_args));
            return ClientProtocol::CONFIGURATION_ERROR_MISSING_PARAMETER;
        }
    }

    public function configOutput(int $status, ?UserPublicKey $publickey = null): string {
        if ($status != ClientProtocol::OK) {
            $dom = $this->xmlStatus('config', (string)($status));
            return $dom->saveXML();
        }
        else {
            $dom = $this->xmlStatus('config', (string)($status));
            $root_node = $dom->getElementsByTagName('config')->item(0);
            if (is_object($publickey)) {
                $root_node->setAttribute('publickey', $publickey->getId());
            }

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'request-job');
            $request_node->setAttribute('path', '/server/request_job.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'download-binary');
            $request_node->setAttribute('path', '/server/binary.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'download-chunk');
            $request_node->setAttribute('path', '/server/chunk.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'error');
            $request_node->setAttribute('path', '/server/error.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'keepmealive');
            $request_node->setAttribute('path', '/server/keepmealive.php');
            $request_node->setAttribute('max-period', (string)$this->config['session']['heartbeat']);
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'logout');
            $request_node->setAttribute('path', $this->router->generate('app_user_logout'));
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'speedtest-answer');
            $request_node->setAttribute('path', $this->router->generate('app_client_speedtest'));
            $root_node->appendChild($request_node);

            $speedtest_node = $dom->createElement('speedtest');

            // for now disable the speed test
//            foreach ($this->mirrorServerRepository->findAll() as $mirror) {
//                if ($mirror instanceof MirrorProject && $mirror->isActualMirror() && $mirror->isEnabled()) {
//                    $target_node = $dom->createElement('target');
//                    $target_node->setAttribute('url', $mirror->getId().'/speedtest.zip');
//                    $speedtest_node->appendChild($target_node);
//                }
//            }
            $root_node->appendChild($speedtest_node);

            return $dom->saveXML();
        }
    }

    public function jobRequestInput(array $args): array {
        $j = ClientProtocol::JOB_REQUEST_ERROR_SESSION_DISABLED;
        $current_computemethod = $this->session->getComputeMethod();
        if (array_key_exists('computemethod', $args)) {
            $computeMethod = $args['computemethod'];
            if ($computeMethod != ClientProtocol::COMPUTE_METHOD_CPU_ONLY && array_key_exists('gpu_model', $args) && array_key_exists('gpu_ram', $args)) {
                $round_vram = round($args['gpu_ram'] / (256 * 1024 * 1024)) * 256 * 1024 * 1024; // round to the closest 256MB
                $gpu_type = 'UNKNOWN2';
                if (array_key_exists('gpu_type', $args)) {
                    // 'casting' (to avoid sql injection and unkown type
                    if ($args['gpu_type'] == GPU::TYPE_OPTIX) {
                        $gpu_type = GPU::TYPE_OPTIX;
                    }
                    else {
                        $gpu_type = 'UNKNOWN';
                    }
                }
                $g = new GPU();
                $g->setType($gpu_type);
                $g->setModel(trim(str_replace(array('with Max-Q Design', '6GB', '16GB', '24GB', 'NVIDIA', ' (TM)', '(TM)'), '', $args['gpu_model'])));
                $g->setMemory($round_vram);
                $gpu = $this->GPURepository->findFromData($g);
                if (is_object($gpu) == false) {
                    $this->entityManager->persist($g);
                    $this->entityManager->flush($g);
                    $gpu = $g;
                }
                $this->session->setGpu($gpu);

                // hack for compatibility with old client
                // to be removed once the minimum client version is over  7.23029.0
                if ($this->version != '6.0.0' && Misc::versionToInteger($this->version) < Misc::versionToInteger('7.23029.0')) {
                    $this->session->setPowerGpu($gpu->getPower());
                }
                // end of hack

                $this->session->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, ($computeMethod == ClientProtocol::COMPUTE_METHOD_BOTH ? 1 : 0)));

                if ($g->getType() == GPU::TYPE_OPTIX) {
                    $this->session->setComputeMethod(Misc::setMaskValue($this->session->getComputeMethod(), Constant::COMPUTE_GPU, 1));
                }
                else {
                    Logger::error(__METHOD__.':'.__LINE__.' unknown type of gpu, user: '.$this->session->getUser()->getId().' request: '.json_encode($args));
                }
            }
            elseif (is_object($this->session->getGPU())) {
                $this->session->setGpu(null);
                $this->session->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
            }
            else {
                $this->session->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
            }
        }
        else {
            $this->session->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
        }

        if ($this->config['session']['google-collab'] == $this->session->gethwid() && Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_GPU) == false && Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_CPU)) {
            $this->session->setBlocked(8); // collab
            $this->entityManager->flush($this->session);
            return array($j, null, 0, 0);
        }

        $this->session->setPaused(false);
        $this->session->setSleeping(false);
        $this->entityManager->flush($this->session);

        $remaining_frames = 0;
        $renderable_jobs = 0;
        $status = ClientProtocol::OK;
        if ($this->session->getBlocked() != 0) {
            $status = ClientProtocol::JOB_REQUEST_ERROR_SESSION_DISABLED;
            $j = $this->session->getBlocked() == 8 ? ClientProtocol::JOB_REQUEST_ERROR_SESSION_DISABLED_DENOISING_NOT_SUPPORTED : ClientProtocol::JOB_REQUEST_ERROR_SESSION_DISABLED;
        }

        $user = $this->session->getUser();
        if ($user->canDoRendering() == false) {
            Logger::debug(__METHOD__.' user'.$user->getId().' does not have right to render');
            $status = ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER;
            $j = ClientProtocol::JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER;
        }

        if (is_object($this->HWIDBlacklistRepository->findOneBy(['hwid' => $this->session->gethwid()]))) {
            Logger::debug(__METHOD__.' User '.$user->getId().' with HWID '.$this->session->gethwid().' has no right to render because of hwid blacklist');
            $status = ClientProtocol::JOB_REQUEST_NOJOB;
            $j = ClientProtocol::JOB_REQUEST_NOJOB;
        }

        if ($this->session->getRunning() == false) {
            $status = ClientProtocol::JOB_REQUEST_NOJOB;
            $j = ClientProtocol::JOB_REQUEST_NOJOB;
        }

        if (array_key_exists('cpu_cores', $args)) {
            $cores = $args['cpu_cores'];
            $cpu = $this->session->getCPU();
            if (is_object($cpu) && $cpu->getCores() != $cores) {
                $cpu_new = new CPU();
                $cpu_new->setModel($cpu->getModel());
                $cpu_new->setPower($cpu->getPower());
                $cpu_new->setCores($cores);
                $cpu_sql = $this->CPURepository->findFromData($cpu_new);
                if (is_object($cpu_sql) == false) { // does the machine exists?
                    $cpu_new->setRenderedFrames(0);
                    $this->entityManager->persist($cpu_new);
                    $this->entityManager->flush($cpu_new);
                }
                else {
                    $cpu_new = $cpu_sql;
                }
                $this->session->setCpu($cpu_new);
                $this->session->setPowerCpu(0); // reset to ask re-do the power detection frame
            }
        }

        if (array_key_exists('ram_max', $args)) {
            $ram = (int)($args['ram_max']);
            if ($ram != $this->session->getMemoryAllowed()) {
                $this->session->setMemoryAllowed($ram);
            }
        }

        if (array_key_exists('rendertime_max', $args)) {
            $max_time = (int)($args['rendertime_max']);
            if ($max_time != $this->session->getMaxRendertime()) {
                $this->session->setMaxRendertime($max_time);
            }
        }

        if (array_key_exists('network_dl', $args) && array_key_exists('network_up', $args)) {
            $this->session->setNetworkSpeedDownload((int)($args['network_dl']));
            $this->session->setNetworkSpeedUpload((int)($args['network_up']));
        }

        $this->entityManager->flush($this->session);
        if ($status == ClientProtocol::OK) {
            if ($current_computemethod != $this->session->getComputeMethod()) {
                $j = $this->tileRepository->find(Project::COMPUTE_METHOD_PROJECT_ID); // ComputeMethod
                $remaining_frames = $this->tileRepository->countAvailableFramesForUser($this->session->getUser());
            }
            else {
                if ($this->config['scheduler']['running'] == false) {
                    $remaining_frames = 0;
                    $j = ClientProtocol::JOB_REQUEST_SERVER_IN_MAINTENANCE;
                }
                else {
                    Logger::debug(__METHOD__.':'.__LINE__.' regular requestJob');
                    list($j, $remaining_frames, $renderable_jobs) = $this->session->requestJob($this->phpSession);
                }
            }
        }


        if (is_object($j)) {
            return array(ClientProtocol::OK, $j, $remaining_frames, $renderable_jobs);
        }
        elseif (is_int($j) && $j == ClientProtocol::JOB_REQUEST_NOJOB) { // no job, no need to log an error
            return array(ClientProtocol::JOB_REQUEST_NOJOB, null, $remaining_frames, $renderable_jobs);
        }
        elseif (is_int($j)) {
            if ($j != ClientProtocol::JOB_REQUEST_SERVER_IN_MAINTENANCE && $j != ClientProtocol::JOB_REQUEST_SERVER_OVERLOADED) {
                Logger::error(__METHOD__." error: return of requestJob ".serialize($j));
            }
            return array($j, null, 0, 0);
        }
        else {
            Logger::error(__METHOD__." error_state 50: return of requestJob ".serialize($j));
            return array(ClientProtocol::UNKNOWN, null, $remaining_frames, $renderable_jobs);
        }
    }

    public function jobRequestOutput(int $status, int $remaining_frames, int $renderable_jobs, ?Tile $tile, array $md5s): string {
        $project = null;
        $md5_renderer = '';
        if ($status == ClientProtocol::OK && is_object($tile)) {
            $project = $tile->getFrame()->getProject();
            $blenderArch = $this->blenderArchRepository->getFor($this->session, $project);

            if (is_null($blenderArch)) {
                $status = ClientProtocol::JOB_REQUEST_ERROR_RENDERER_NOT_AVAILABLE;
                Logger::error(__METHOD__." (4) Failed to get renderer for $tile)");
                $tile->reset();
            }
            else {
                $md5_renderer = $blenderArch->getMd5();
            }
        }

        /** @var ?User $user */
        $user = null;
        if (is_object($this->session)) {
            $user = $this->session->getUser();

            $active_projects = $this->main->countPublicActiveProjects();
        }
        else {
            $user = null;
            $active_projects = 0;
        }

        $dom = $this->xmlStatus('jobrequest', (string)($status));
        $root_node = $dom->getElementsByTagName('jobrequest')->item(0);

        if (is_object($this->session)) {
            $stats_node = $dom->createElement('stats');
            $stats_node->setAttribute('credits_session', (string)((int)($this->session->getPoints())));
            $stats_node->setAttribute('credits_total', (string)((int)($user->getPoints())));
            $stats_node->setAttribute('frame_remaining', (string)($remaining_frames));
            $stats_node->setAttribute('waiting_project', (string)($active_projects));
            $stats_node->setAttribute('connected_machine', (string)($this->sessionRepository->countConnectedMachines()));
            $stats_node->setAttribute('renderable_project', (string)($renderable_jobs));
            $root_node->appendChild($stats_node);
        }
        if (count($md5s) > 0 && $root_node != null) {
            foreach ($md5s as $md5_file) {
                $file_node = $dom->createElement('file');
                $file_node->setAttribute('md5', $md5_file);
                $file_node->setAttribute('action', 'delete');
                $root_node->appendChild($file_node);
            }
        }

        if ($status == ClientProtocol::OK) {
            $e = new Event();
            $e->setType(Event::TYPE_REQUEST);
            $e->setTime(time());
            $e->setSession($this->session->getId());
            $e->setUser($this->session->getUser()->getId());
            $e->setProject($tile->getFrame()->getProject()->getId());
            $this->entityManager->persist($e);
            $this->entityManager->flush($e);

            // more user friendly name
            if ($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID) {
                $project->setName('Can Blender be launched?');
            }
            elseif ($project->getId() == Project::POWER_DETECTION_PROJECT_ID) {
                $project->setName('Check computer strength.');
                $project->setComputeMethod(0);
                if (Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_CPU) && $this->session->getPowerCpu() <= 0) {
                    $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
                }
                elseif (Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_GPU)) {
                    $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
                }
            }

            $use_gpu = Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_GPU) && Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU);
            $script = $tile->getFrame()->getPythonScript($tile, $this, $use_gpu);

            $job_node = $dom->createElement('job');
            $job_node->setAttribute('id', (string)($tile->getId()));
            $job_node->setAttribute('use_gpu', $use_gpu ? '1' : '0');

            $chunks_node = $dom->createElement('chunks');
            foreach ($project->getChunks() as $chunk) {
                $chunk_node = $dom->createElement('chunk');
                $chunk_node->setAttribute('id', $chunk);
                $chunk_node->setAttribute('md5', $chunk); // right now md5 is the ID
                $chunks_node->appendChild($chunk_node);
            }
            $job_node->appendChild($chunks_node);

            $job_node->setAttribute('path', $project->getPath());
            $job_node->setAttribute('frame', sprintf('%04d', $tile->getFrame()->getNumber()));
            $job_node->setAttribute('synchronous_upload', $tile->isComputeMethod() || $tile->isPowerDetection() ? '1' : '0');
            $job_node->setAttribute('extras', '');
            $job_node->setAttribute('validation_url', urlencode($project->getShepherd()->generateValidationUrl($tile)));
            $job_node->setAttribute('name', $project->getName());
            $job_node->setAttribute('password', $project->getPassword());

            $renderer_node = $dom->createElement('renderer');
            $renderer_node->setAttribute('md5', $md5_renderer);
            $renderer_node->setAttribute('commandline', str_replace('%%ENGINE%%', $project->getEngine(), $tile->getBlenderCommandline()));
            $renderer_node->setAttribute('update_method', 'remainingtime');
            $job_node->appendChild($renderer_node);

            $script_node = $dom->createElement('script', $script);
            $job_node->appendChild($script_node);

            $root_node->appendChild($job_node);
        }
        return $dom->saveXML();
    }

    public function jobValidationInput(array $args, FileBag $files, bool $disable_security_check = false): array {
        // TODO handle $args['cores']

        $status = ClientProtocol::UNKNOWN;
        $a_frame = null;
        $file = null;

        if ($files->has('file') == false) {
            Logger::error(__METHOD__.' (1) missing parameter file '.serialize($files));
            return array(ClientProtocol::JOB_VALIDATION_ERROR_MISSING_PARAMETER, null, null, null);
        }

        if (array_key_exists('rendertime', $args) == false || array_key_exists('job', $args) == false || array_key_exists('frame', $args) == false) {
            Logger::error(__METHOD__.' (2) missing parameter '.serialize($args));
            return array(ClientProtocol::JOB_VALIDATION_ERROR_MISSING_PARAMETER, null, null, null);
        }
        else {
            $render_time = $args['rendertime'];
            if (is_numeric($args['rendertime']) == false) {
                return array(ClientProtocol::JOB_VALIDATION_ERROR_MISSING_PARAMETER, null, null, null);
            }
            $frame_number = $args['frame'];
            $a_frame = $this->tileRepository->find($args['job']);
            if (is_object($a_frame) == false) {
                Logger::error(__METHOD__.' (3) failed to import Frame (job:'.$args['job'].' frame: '.$frame_number.')');
                return array(ClientProtocol::UNKNOWN, null, null, null);
            }
            else {
                $a_frame->setRenderTime($render_time);
                $status = ClientProtocol::OK;

                if ($a_frame instanceof TilePowerDetection && array_key_exists('speedsamples', $args)) {
                    $a_frame->setSpeedSamplesRendered((float)($args['speedsamples']));
                }
            }
        }

        try {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $files->get('file');
            if ($uploadedFile->isValid()) {
                $tmp_file = tempnam($this->config['tmp_dir'], 'FER');
                if ($disable_security_check) { // for unit tests
                    $tmp_file_post = $uploadedFile->getRealPath();
                    if (file_exists($tmp_file_post)) {
                        rename($tmp_file_post, $tmp_file);
                        $file = $tmp_file;
                        $status = ClientProtocol::OK;
                    }
                    else {
                        Logger::error(__METHOD__." (4) file does not exist (frame: $a_frame) (path: $tmp_file tmp_file_post ".file_exists($tmp_file_post).")");
                        $status = ClientProtocol::JOB_VALIDATION_ERROR_UPLOAD_FAILED;
                        if (is_object($this->session) && $this->session->getUser()->getId() == $a_frame->getUser()->getId()) {
                            $a_frame->reset();
                        }
                        @unlink($tmp_file_post);
                    }
                }
                else {
                    $uploadedFile->move($this->config['tmp_dir'], basename($tmp_file));
                    $file = $tmp_file;
                    $status = ClientProtocol::OK;
                }
            }
            else {
                Logger::error(__METHOD__." (6) failed to upload file, frame: $a_frame)");
                $status = ClientProtocol::JOB_VALIDATION_ERROR_UPLOAD_FAILED;
                if (is_object($this->session) && is_object($a_frame->getUser()) && $this->session->getUser()->getId() == $a_frame->getUser()->getId()) {
                    $a_frame->reset();
                }
            }
        }
        catch (FileException $e) {
            Logger::error(__METHOD__." (9) failed, frame: $a_frame)");
            $status = ClientProtocol::JOB_VALIDATION_ERROR_UPLOAD_FAILED;
            if (is_object($this->session) && $this->session->getUser()->getId() == $a_frame->getUser()->getId()) {
                $a_frame->reset();
            }
        }

        return array($status, $a_frame, $frame_number, $file);
    }

    public function jobValidationOutput(int $status): string {
        $dom = $this->xmlStatus('jobvalidate', (string)($status));
        return $dom->saveXML();
    }

    public function keepMeAliveInput(array $request_args): int {
        $status = ClientProtocol::UNKNOWN;

        if (is_object($this->session)) {
            if (array_key_exists('paused', $request_args)) {
                $this->session->setPaused($request_args['paused'] == 'true');
            }
            if (array_key_exists('sleeping', $request_args)) {
                $this->session->setSleeping($request_args['sleeping'] == 'true');
            }

            $this->session->keepMeAlive();

            $status = ClientProtocol::OK;

            if (array_key_exists('frame', $request_args) && array_key_exists('job', $request_args)) {
                $tile = $this->tileRepository->find($request_args['job']);
                if (is_object($tile) == false) {
                    Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().')  for tile:'.$request_args['job'].' because frame does not exist');
                    $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                }
                else {
                    $project = $tile->getFrame()->getProject();

                    $e = new Event();
                    $e->setType(Event::TYPE_RENDERING);
                    $e->setSession($this->session->getId());
                    $e->setTime(time());
                    $e->setUser($this->session->getUser()->getId());
                    $e->setProject($project->getId());
                    $this->entityManager->persist($e);
                    $this->entityManager->flush($e);

                    if ($tile->isComputeMethod() || $tile->isPowerDetection()) {
                        // special case, never kill the process
                        Logger::debug(__METHOD__.':'.__LINE__.' very slow machine detected ? rendering: '.Misc::humanTime(@$request_args['rendertime']).' remaining: '.Misc::humanTime(@$request_args['remainingtime']));
                    }
                    else {
                        if ($tile->getStatus() != Constant::PROCESSING) {
                            Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().') for tile:'.$tile->getId().' because frame is not rendering (it\'s '.$tile->getStatus().')');
                            $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                        }
                        elseif ($tile->getSession() != $this->session->getId()) {
                            Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().') for tile:'.$tile->getId().' because frame is rendering but not by this session (it\'s by '.$tile->getSession().')');
                            $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                        }
                        else {
                            if (array_key_exists('remainingtime', $request_args)) {
                                $tile->setRemainingRenderTime((int)($request_args['remainingtime'])); // cast for sql injection
                                $this->entityManager->flush($tile);

                                if (array_key_exists('rendertime', $request_args)) {
                                    $rendertime = $request_args['rendertime'];
                                    if ($rendertime > $this->config['power']['rendertime_max_reference'] && $request_args['remainingtime'] > $this->config['session']['timeoutrender']) {
                                        Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().') for tile:'.$tile->getId().' because the job will take to much time to render (remaining '.Misc::humanTime($request_args['remainingtime'], false).')');
                                        $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                                    }
                                }
                            }
                        }

                        if ($project->getStatus() != Constant::WAITING && $project->getStatus() != Constant::PROCESSING) {
                            Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().') for tile:'.$tile->getId().' because job is not rendering (it\'s '.$project->getStatus().')');
                            $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                        }
                        else {
                            if ($project->isBlocked() && $this->session->getUser()->getId() != $project->getOwner()->getId()) {
                                $status = ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
                                Logger::debug('(keepmealive) send kill to '.$this->session->getUser()->getId().' (session:'.$this->session->getId().') for tile:'.$tile->getId().' because the job is blocked');
                            }
                        }
                    }
                }

                if ($status == ClientProtocol::KEEPMEALIVE_STOP_RENDERING) {
                    $e = new Event();
                    $e->setType(Event::TYPE_KILL);
                    $e->setSession($this->session->getId());
                    $e->setTime(time());
                    $e->setUser($this->session->getUser()->getId());
                    $e->setProject($request_args['job']);
                    $this->entityManager->persist($e);
                    $this->entityManager->flush($e);
                }
            }
        }
        else {
            Logger::debug(__METHOD__.' session is null');
            return ClientProtocol::KEEPMEALIVE_STOP_RENDERING;
        }

        return $status;
    }

    public function keepMeAliveOutput(int $status): string {
        $dom = $this->xmlStatus('keepmealive', (string)($status));
        return $dom->saveXML();
    }

    public function errorInput(array $args, FileBag $files, string $destination): array {
        $job_id = null;
        $frame_number = null;
        $render_time = 0;
        $memory_used = 0;
        $type = null;

        if ($files->has('file')) {
            /** @var UploadedFile $file */
            $file = $files->get('file');
            $tmp_file_post = $file->getRealPath();
            if (is_uploaded_file($tmp_file_post)) {
                move_uploaded_file($tmp_file_post, $destination);
            }
            else {
                @unlink($tmp_file_post);
            }
        }

        if (array_key_exists('frame', $args) && array_key_exists('job', $args)) {
            $job_id = (int)$args['job'];
            $frame_number = (string)$args['frame'];
        }

        if (array_key_exists('render_time', $args)) {
            $render_time = (int)$args['render_time'];
        }

        if (array_key_exists('type', $args)) {
            $type = (int)$args['type'];
        }

        if (array_key_exists('memoryused', $args)) {
            $memory_used = (int)($args['memoryused']);
        }

        return array(ClientProtocol::OK, $job_id, $frame_number, $render_time, $memory_used, $type);
    }

    protected function xmlStatus(string $root, string $status): DOMDocument {
        $dom = new DomDocument('1.0', 'utf-8');
        $root_node = $dom->createElement($root);
        $root_node->setAttribute('status', $status);
        $dom->appendChild($root_node);
        return $dom;
    }
}
