<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler;

use App\CDN\CDNBucket;
use App\Constant;
use App\Entity\Event;
use App\Entity\Project;
use App\Entity\Tile;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Service\Logger;
use App\Utils\Misc;

/**
 * Handle client
 */
class ClientProtocol61 extends ClientProtocol60 {
    public const MINIMUM_VERSION = "7.24323.0";

    public function configOutput(int $status, ?UserPublicKey $publickey = null): string {
        if ($status != ClientProtocol::OK) {
            $dom = $this->xmlStatus('config', (string)($status));
            return $dom->saveXML();
        }
        else {
            $dom = $this->xmlStatus('config', (string)($status));
            $root_node = $dom->getElementsByTagName('config')->item(0);
            if (is_object($publickey)) {
                $root_node->setAttribute('publickey', $publickey->getId());
            }

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'request-job');
            $request_node->setAttribute('path', '/server/request_job.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'download-chunk');
            $request_node->setAttribute('path', '/server/chunk.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'error');
            $request_node->setAttribute('path', '/server/error.php');
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'keepmealive');
            $request_node->setAttribute('path', '/server/keepmealive.php');
            $request_node->setAttribute('max-period', (string)$this->config['session']['heartbeat']);
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'logout');
            $request_node->setAttribute('path', $this->router->generate('app_user_logout'));
            $root_node->appendChild($request_node);

            $request_node = $dom->createElement('request');
            $request_node->setAttribute('type', 'speedtest-answer');
            $request_node->setAttribute('path', $this->router->generate('app_client_speedtest'));
            $root_node->appendChild($request_node);

            $speedtest_node = $dom->createElement('speedtest');

            // for now disable the speed test
//            foreach ($this->mirrorServerRepository->findAll() as $mirror) {
//                if ($mirror instanceof MirrorProject && $mirror->isActualMirror() && $mirror->isEnabled()) {
//                    $target_node = $dom->createElement('target');
//                    $target_node->setAttribute('url', $mirror->getId().'/speedtest.zip');
//                    $speedtest_node->appendChild($target_node);
//                }
//            }
            $root_node->appendChild($speedtest_node);

            return $dom->saveXML();
        }
    }

    public function jobRequestOutput(int $status, int $remaining_frames, int $renderable_jobs, ?Tile $tile, array $md5s): string {
        if ($status == ClientProtocol::OK && is_object($tile)) {
            $project = $tile->getFrame()->getProject();
            $blenderArch = $this->blenderArchRepository->getFor($this->session, $project);

            if (is_null($blenderArch)) {
                $status = ClientProtocol::JOB_REQUEST_ERROR_RENDERER_NOT_AVAILABLE;
                Logger::error(__METHOD__." (4) Failed to get renderer for $tile)");
                $tile->reset();

                $dom = $this->xmlStatus('jobrequest', (string)($status));
                return $dom->saveXML();
            }

        }
        else {
            $dom = $this->xmlStatus('jobrequest', (string)($status));
            return $dom->saveXML();
        }

        /** @var ?User $user */
        $user = null;
        if (is_object($this->session)) {
            $user = $this->session->getUser();

            $active_projects = $this->main->countPublicActiveProjects();
        }
        else {
            $user = null;
            $active_projects = 0;
        }

        $dom = $this->xmlStatus('jobrequest', (string)($status));
        $root_node = $dom->getElementsByTagName('jobrequest')->item(0);

        if (is_object($this->session)) {
            $stats_node = $dom->createElement('stats');
            $stats_node->setAttribute('credits_session', (string)((int)($this->session->getPoints())));
            $stats_node->setAttribute('credits_total', (string)((int)($user->getPoints())));
            $stats_node->setAttribute('frame_remaining', (string)($remaining_frames));
            $stats_node->setAttribute('waiting_project', (string)($active_projects));
            $stats_node->setAttribute('connected_machine', (string)($this->sessionRepository->countConnectedMachines()));
            $stats_node->setAttribute('renderable_project', (string)($renderable_jobs));
            $root_node->appendChild($stats_node);
        }
        if (count($md5s) > 0 && $root_node != null) {
            foreach ($md5s as $md5_file) {
                $file_node = $dom->createElement('file');
                $file_node->setAttribute('md5', $md5_file);
                $file_node->setAttribute('action', 'delete');
                $root_node->appendChild($file_node);
            }
        }

        $e = new Event();
        $e->setType(Event::TYPE_REQUEST);
        $e->setTime(time());
        $e->setSession($this->session->getId());
        $e->setUser($this->session->getUser()->getId());
        $e->setProject($tile->getFrame()->getProject()->getId());
        $this->entityManager->persist($e);
        $this->entityManager->flush($e);

        // more user friendly name
        if ($project->getId() == Project::COMPUTE_METHOD_PROJECT_ID) {
            $project->setName('Can Blender be launched?');
        }
        elseif ($project->getId() == Project::POWER_DETECTION_PROJECT_ID) {
            $project->setName('Check computer strength.');
            $project->setComputeMethod(0);
            if (Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_CPU) && $this->session->getPowerCpu() <= 0) {
                $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_CPU, 1));
            }
            elseif (Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_GPU)) {
                $project->setComputeMethod(Misc::setMaskValue(0, Constant::COMPUTE_GPU, 1));
            }
        }

        $use_gpu = Misc::isMaskEnabled($this->session->getComputeMethod(), Constant::COMPUTE_GPU) && Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU);
        $script = $tile->getFrame()->getPythonScript($tile, $this, $use_gpu);

        $job_node = $dom->createElement('job');
        $job_node->setAttribute('id', (string)($tile->getId()));
        $job_node->setAttribute('use_gpu', $use_gpu ? '1' : '0');
        $job_node->setAttribute('size', (string)$project->getArchiveSize());
        $job_node->setAttribute('md5', $project->getMd5());

        $cdn = new CDNBucket();

        $chunks_node = $dom->createElement('chunks');
        foreach ($project->getChunks() as $chunk) {
            $chunk_node = $dom->createElement('chunk');
            $chunk_node->setAttribute('id', $chunk);
            $chunk_node->setAttribute('md5', $chunk); // right now md5 is the ID
            $chunks_node->appendChild($chunk_node);
        }
        $job_node->appendChild($chunks_node);

        $job_node->setAttribute('path', $project->getPath());
        $job_node->setAttribute('frame', sprintf('%04d', $tile->getFrame()->getNumber()));
        $job_node->setAttribute('synchronous_upload', $tile->isComputeMethod() || $tile->isPowerDetection() ? '1' : '0');
        $job_node->setAttribute('extras', '');
        $job_node->setAttribute('validation_url', urlencode($project->getShepherd()->generateValidationUrl($tile)));
        $job_node->setAttribute('name', $project->getName());
        $job_node->setAttribute('password', $project->getPassword());

        $renderer_node = $dom->createElement('renderer');
        $renderer_node->setAttribute('md5', $blenderArch->getMd5());
        $renderer_node->setAttribute('size', (string)$blenderArch->getArchiveSize());
        $renderer_node->setAttribute('commandline', str_replace('%%ENGINE%%', $project->getEngine(), $tile->getBlenderCommandline()));
        $renderer_node->setAttribute('update_method', 'remainingtime');
        $chunks_node = $dom->createElement('chunks');
        foreach ($blenderArch->getChunks() as $chunk) {
            $chunk_node = $dom->createElement('chunk');
            $chunk_node->setAttribute('id', $chunk);
            $chunk_node->setAttribute('md5', $chunk); // right now md5 is the ID
            $chunks_node->appendChild($chunk_node);
        }
        $renderer_node->appendChild($chunks_node);
        $job_node->appendChild($renderer_node);

        $script_node = $dom->createElement('script', $script);
        $job_node->appendChild($script_node);

        $root_node->appendChild($job_node);

        return $dom->saveXML();
    }
}
