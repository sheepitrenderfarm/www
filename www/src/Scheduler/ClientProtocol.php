<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler;

use App\Entity\Tile;
use App\Entity\Session;
use App\Entity\User;
use App\Entity\UserPublicKey;
use Symfony\Component\HttpFoundation\FileBag;

interface ClientProtocol {
    public const OK = 0;
    public const UNKNOWN = 999;
    public const INTERVAL_VALUE = 950;

    public const CONFIGURATION_ERROR_NO_CLIENT_VERSION_GIVEN = 100;
    public const CONFIGURATION_ERROR_CLIENT_TOO_OLD = 101;
    public const CONFIGURATION_ERROR_AUTH_FAILED = 102;
    public const CONFIGURATION_ERROR_WEB_SESSION_EXPIRED = 103;
    public const CONFIGURATION_ERROR_MISSING_PARAMETER = 104;
    public const CONFIGURATION_ERROR_ARCH_NO_SUPPORTED = 105;

    public const JOB_REQUEST_NOJOB = 200;
    public const JOB_REQUEST_ERROR_NO_RENDERING_RIGHT_USER = 201;
    public const JOB_REQUEST_ERROR_DEAD_SESSION = 202;
    public const JOB_REQUEST_ERROR_SESSION_DISABLED = 203;
    public const JOB_REQUEST_ERROR_SESSION_DISABLED_DENOISING_NOT_SUPPORTED = 208;
    public const JOB_REQUEST_ERROR_INTERNAL_ERROR = 204;
    public const JOB_REQUEST_ERROR_RENDERER_NOT_AVAILABLE = 205;
    public const JOB_REQUEST_SERVER_IN_MAINTENANCE = 206;
    public const JOB_REQUEST_SERVER_OVERLOADED = 207;

    public const JOB_VALIDATION_ERROR_MISSING_PARAMETER = 300;
    public const JOB_VALIDATION_ERROR_BROKEN_MACHINE = 301; // in GPU the generated frame is black
    public const JOB_VALIDATION_ERROR_FRAME_IS_NOT_IMAGE = 302;
    public const JOB_VALIDATION_ERROR_UPLOAD_FAILED = 303;
    public const JOB_VALIDATION_ERROR_SESSION_DISABLED = 304; // missing heartbeat or broken machine
    public const JOB_VALIDATION_ERROR_WRONG_MD5 = 305; // only used on proto 3.2
    public const JOB_VALIDATION_ERROR_IMAGE_WRONG_DIMENSION = 308;

    public const KEEPMEALIVE_STOP_RENDERING = 400;

    public const COMPUTE_METHOD_BOTH = 0;
    public const COMPUTE_METHOD_CPU_ONLY = 1;
    public const COMPUTE_METHOD_GPU_ONLY = 2;


    public function getSession(): Session;

    /**
     * @return array{0: int, 1: ?User, 2: ?UserPublicKey}
     */
    public function userAuth(array $request_args): array;

    public function configInput(User $user, ?UserPublicKey $renderKey, array $request_args): int;

    public function configOutput(int $status, ?UserPublicKey $publickey): string;


    /**
     * Return [status, tile, remaining_tiles, renderable_project]
     * @return array{0: int, 1: ?Tile, 2: int, 3: int}
     */
    public function jobRequestInput(array $args): array;

    public function jobRequestOutput(int $status, int $remaining_frames, int $renderable_jobs, ?Tile $tile, array $md5s): string;

    /**
     * @return array{0: int, 1: ?Tile, 2: ?string, 3: ?string}
     */
    public function jobValidationInput(array $args, FileBag $files): array;

    public function jobValidationOutput(int $status): string;

    public function keepMeAliveInput(array $request_args): int;

    public function keepMeAliveOutput(int $status): string;

    /**
     * @return array{0: int, 1: int, 2: string, 3: int, 4: int, 5: ?int}
     */
    public function errorInput(array $args, FileBag $files, string $destination): array;

    public function __toString(): string;
}
