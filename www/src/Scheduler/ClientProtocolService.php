<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler;

use App\Entity\Session;
use App\Repository\BlenderArchRepository;
use App\Repository\CPURepository;
use App\Repository\GPURepository;
use App\Repository\HWIDBlacklistRepository;
use App\Repository\ProjectRepository;
use App\Repository\SessionRepository;
use App\Repository\TileRepository;
use App\Repository\UserPublicKeyRepository;
use App\Repository\UserRepository;
use App\Repository\WebSessionClientRepository;
use App\Service\ConfigService;
use App\Service\Main;
use App\Utils\Misc;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ClientProtocolService {
    private ConfigService $configService;
    private UrlGeneratorInterface $router;
    private EntityManagerInterface $entityManager;
    private Main $main;
    private SessionRepository $sessionRepository;
    private HWIDBlacklistRepository $HWIDBlacklistRepository;
    private ProjectRepository $projectRepository;
    private TileRepository $tileRepository;
    private CPURepository $CPURepository;
    private GPURepository $GPURepository;
    private UserRepository $userRepository;
    private UserPublicKeyRepository $userPublicKeyRepository;
    private WebSessionClientRepository $webSessionClientRepository;
    private BlenderArchRepository $blenderArchRepository;

    public function __construct(ConfigService $configService,
        UrlGeneratorInterface $router,
        EntityManagerInterface $entityManager,
        Main $main,
        SessionRepository $sessionRepository,
        HWIDBlacklistRepository $HWIDBlacklistRepository,
        ProjectRepository $projectRepository,
        TileRepository $tileRepository,
        CPURepository $CPURepository,
        GPURepository $GPURepository,
        UserRepository $userRepository,
        UserPublicKeyRepository $userPublicKeyRepository,
        WebSessionClientRepository $webSessionClientRepository,
        BlenderArchRepository $blenderArchRepository
    ) {
        $this->configService = $configService;
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->main = $main;
        $this->sessionRepository = $sessionRepository;
        $this->HWIDBlacklistRepository = $HWIDBlacklistRepository;
        $this->projectRepository = $projectRepository;
        $this->tileRepository = $tileRepository;
        $this->CPURepository = $CPURepository;
        $this->GPURepository = $GPURepository;
        $this->userRepository = $userRepository;
        $this->userPublicKeyRepository = $userPublicKeyRepository;
        $this->webSessionClientRepository = $webSessionClientRepository;
        $this->blenderArchRepository = $blenderArchRepository;
    }

    public function getHandler(string $version_str, ?Session $session_, SessionInterface $phpSession): ClientProtocol {
        // for multi protocol supports
        // 3.3.xxxx => 30003xxxx
        $version60_num = Misc::versionToInteger(ClientProtocol60::MINIMUM_VERSION);
        $version61_num = Misc::versionToInteger(ClientProtocol61::MINIMUM_VERSION);

        if ($this->configService->getData()['client']['github_version'] == $version_str || $version_str == '') {
            // force to latest
            // Logger::debug(__method__.' found github version -> force to latest');
            $version_num = $version60_num;
        }
        else {
            $version_num = Misc::versionToInteger($version_str);
        }

        if ($version_num < $version61_num) {
            return new ClientProtocol60(
                $version_str,
                $session_,
                $this->router,
                $phpSession,
                $this->entityManager,
                $this->main,
                $this->sessionRepository,
                $this->HWIDBlacklistRepository,
                $this->projectRepository,
                $this->tileRepository,
                $this->CPURepository,
                $this->GPURepository,
                $this->userRepository,
                $this->userPublicKeyRepository,
                $this->webSessionClientRepository,
                $this->blenderArchRepository,
                $this->configService->getData()
            );
        }
//        else if ($version_num < $version51_num) {
//            return new ClientProtocol51(...);
//        }
//        else if ($version_num < $version60_num) {
//            return new ClientProtocol60(...);
//        }
        else {
            return new ClientProtocol61(
                $version_str,
                $session_,
                $this->router,
                $phpSession,
                $this->entityManager,
                $this->main,
                $this->sessionRepository,
                $this->HWIDBlacklistRepository,
                $this->projectRepository,
                $this->tileRepository,
                $this->CPURepository,
                $this->GPURepository,
                $this->userRepository,
                $this->userPublicKeyRepository,
                $this->webSessionClientRepository,
                $this->blenderArchRepository,
                $this->configService->getData()
            );
        }
    }

    public function isSupported(string $version_str, string $os): bool {
        $config = $this->configService->getData();
        if ($config['client']['github_version'] == $version_str) {
            return true;
        }

        if (Misc::versionToInteger($version_str) < Misc::versionToInteger($config['client']['minimum_version']['all'])) {
            return false;
        }
        if (array_key_exists($os, $config['client']['minimum_version']) && Misc::versionToInteger($version_str) < Misc::versionToInteger($config['client']['minimum_version'][$os])) {
            return false;
        }

        return true;
    }
}