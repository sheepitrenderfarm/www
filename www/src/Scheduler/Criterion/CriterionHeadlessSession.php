<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionHeadlessSession extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        return $this->isAllowed($session, $project) ? 0.0 : -1.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if ($this->isAllowed($session, $project) == false) {
            return 'No eevee or Grease Pencil project on headless session';
        }
        return '';
    }

    private function isAllowed(?Session $session, ?Project $project): bool {
        if (is_object($session) && is_object($project)) {
            if ($session->getHeadless() && $project->getRenderOnGpuHeadless() == false) {
                if (Misc::isMaskEnabled($project->getComputeMethod(), Constant::COMPUTE_GPU)) {
                    $gpu = $session->getGPU();
                    if (is_object($gpu)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
