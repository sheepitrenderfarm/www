<?php
/**
 * Copyright (C) 2013-2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Detect if a frame will longer than 90min to avoid to kill it later on.
 **/
class CriterionBanHeavyFrame extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $rendertime = $project->getAverageRenderTimeMachineRef();
            if ($rendertime > 0) {
                $device = $session->getComputeDeviceFor($project->getComputeMethod());
                if (is_object($device) && $device->getPower() > 0) {
                    $predicted = $rendertime * $device->getPowerConst() / $device->getPower();
                    //if ($predicted > $this->config['session']['timeoutrender']) {
                    if ($predicted > 4000) { // ~1h
                        return -1.0;
                    }

                    $endOfRender = $project->estinationEndOfRender();
                    if ($endOfRender != PHP_INT_MAX && $endOfRender < $predicted) {
                        return -1.0;
                    }
                }
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Cannot be rendered under 1h';
    }
}
