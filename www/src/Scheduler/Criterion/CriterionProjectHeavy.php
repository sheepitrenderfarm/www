<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * If a frame's rendertime is over 3min, give it a boost.
 * The user need to have checked the 'heavy frame first' option on the scheduler
 * and the render device needs to be over 200%
 */
class CriterionProjectHeavy extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session)) {
            $user = $session->getUser();
            $device = $session->getComputeDeviceFor($project->getComputeMethod());
            if ($user->schedulerIsEnabled(User::SCHEDULER_MASK_HEAVY_PROJECT) && is_object($device)) {
                if ($device->getPowerPercent() > 200 && $project->getCacheFinished() > 0) {
                    $power = $device->getPower();
                    if ($power > 0) {
                        $duration = $project->getAverageRenderTimeMachineRef() / ($device->getPowerPercent() / 100.0);
                        if (180 < $duration) {
                            return 1.0;
                        }
                    }
                }
            }
        }
        return 0.0;
    }
}
