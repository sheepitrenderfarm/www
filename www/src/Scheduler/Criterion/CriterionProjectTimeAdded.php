<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectTimeAdded extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            return min(1.0, (time() - $project->getLastUpdateArchive()) / ($this->config['storage']['duration']['project']['rendered'] * 3600 * 24));
        }
        return 0.0;
    }
}
