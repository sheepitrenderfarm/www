<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionOldValidation extends Criterion {

    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            if ($session->getUser()->getId() == $project->getOwner()->getId()) {
                return 0.0;
            }

            $last_validation = 0;
            foreach ($project->tilesWithStatus(Constant::FINISHED) as $a_frame) {
                if ($last_validation < $a_frame->getValidationTime()) {
                    $last_validation = $a_frame->getValidationTime();
                }
            }

            if ($last_validation != 0) {
                return (int)((time() - $last_validation) / 3600);
            }
        }
        return 0.0;
    }
}
