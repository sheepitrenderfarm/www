<?php
/**
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectProgression extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $stats = $project->getCachedStatistics();
            if ($stats[Constant::STATS_TOTAL] > 0) {
                return $stats[Constant::FINISHED] / $stats[Constant::STATS_TOTAL];
            }
        }
        return 0.0;
    }
}
