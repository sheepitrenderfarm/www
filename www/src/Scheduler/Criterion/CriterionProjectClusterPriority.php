<?php
/**
 * Copyright (C) 2025 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Split sessions and projects is 3 clusters (LOW, MID, HIGH) depending on the power or difficulty
 * Priority order:
 * LOW  session ->  LOW, MID, HIGH project
 * MID  session ->  MID, LOW, HIGH project
 * HIGH session ->  HIGH, MID, LOW project
 */
class CriterionProjectClusterPriority extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {

        if (is_object($session) && is_object($project)) {
            $project_cluster = $this->entityManager->getRepository(Project::class)->getCluster($project);
            $cluster_session = $this->entityManager->getRepository(Session::class)->getCluster($session, $project->getComputeMethod());

            if ($cluster_session === false) { // not the same compute method
                return 0.0;
            }

            switch ($cluster_session) {
                case Constant::CLUSTER_LOW:
                {
                    switch ($project_cluster) {
                        case Constant::CLUSTER_LOW:
                            return 1.0;
                        case Constant::CLUSTER_MID:
                            return 0.5;
                        case Constant::CLUSTER_HIGH:
                            return 0.0;
                    }
                }

                case Constant::CLUSTER_MID:
                {
                    switch ($project_cluster) {
                        case Constant::CLUSTER_LOW:
                            return 0.5;
                        case Constant::CLUSTER_MID:
                            return 1.0;
                        case Constant::CLUSTER_HIGH:
                            return 0.0;
                    }
                }
                case Constant::CLUSTER_HIGH:
                {
                    switch ($project_cluster) {
                        case Constant::CLUSTER_LOW:
                            return 0.0;
                        case Constant::CLUSTER_MID:
                            return 0.5;
                        case Constant::CLUSTER_HIGH:
                            return 1.0;
                    }
                }
            }
        }
        return 0.0;
    }
}
