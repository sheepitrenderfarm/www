<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\SessionError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CriterionBanJobFromSQLEventUserSendError extends Criterion {
    /** @var SessionError[] */
    private ?array $previous_error_projects;

    public function __construct(UrlGeneratorInterface $router, EntityManagerInterface $entityManager, array $config, float $ponderation_, ?array $previous_error_projects_) {
        parent::__construct($router, $entityManager, $config, $ponderation_);
        $this->previous_error_projects = $previous_error_projects_;
    }

    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_array($this->previous_error_projects)) {
            foreach ($this->previous_error_projects as $sessionError) {
                if ($project->getId() == $sessionError->getProject()) {
                    return -1;
                }
            }
        }

        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Computer has previously failed to render project';
    }
}
