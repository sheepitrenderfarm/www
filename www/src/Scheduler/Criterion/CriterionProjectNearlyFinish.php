<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectNearlyFinish extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            if ($session->getUser()->getId() == $project->getOwner()->getId()) {
                return 0.0; // disable if the owner is the renderer
            }
            $stats = $project->getCachedStatistics();
            $diff = $stats['total'] - $stats[Constant::FINISHED] - $stats[Constant::PROCESSING];
            if (0 < $diff && $diff < 10) {
                return 1.0;
            }
        }
        return 0.0;
    }
}
