<?php
/**
 * Copyright (C) 2019 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionBlenderBinaryIsAvailable extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        $path = $this->getBinaryPath($session, $project);
        if (is_null($path) || file_exists($path) == false) {
            return -1;
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if (is_null($project)) {
            return 'Blender binary available';
        }
        $path = $this->getBinaryPath($session, $project);
        if (is_null($path)) {
            return 'Blender binary available';
        }
        else {
            if (file_exists($path) == false) {
                $executable = $project->getExecutable()->getId();
                foreach ($this->config['renderer']['binary'] as $human => $version) {
                    if ($version['filename'] == $executable) {
                        $executable = "'".$human."'";
                    }
                }

                return $executable.' not available for '.$session->getOS().' , see <a href="'.$this->router->generate('app_home_binaries').'">binaries available</a>';
            }
        }
        return 'Blender binary available';
    }

    private function getBinaryPath(?Session $session, ?Project $project): ?string {
        if (is_null($session)) {
            return null;
        }

        if (is_null($project)) {
            return null;
        }

        return $this->config['storage']['path'].'binaries/'.strtolower($project->getExecutable()->getId().'_'.$session->getOS().'_64bit').'.'.$this->config['archive']['extension'];
    }
}
