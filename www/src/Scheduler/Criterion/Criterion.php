<?php
/**
 * Copyright (C) 2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Criterion {
    protected array $config;
    protected UrlGeneratorInterface $router;
    protected EntityManagerInterface $entityManager;
    protected float $ponderation;

    public function __construct(UrlGeneratorInterface $router, EntityManagerInterface $entityManager, array $config, float $ponderation_ = 0.0) {
        $this->entityManager = $entityManager;
        $this->config = $config;
        $this->ponderation = $ponderation_;
        $this->router = $router;
    }

    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        return 0.0;
    }

    public function ponderation(): float {
        return $this->ponderation;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Cannot render due to: '.(new \ReflectionClass($this))->getShortName();
    }
}
