<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionBanJobFromUserSendError extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        // see /server/error.php
        // $_SESSION['ban'][$frame->getJob()] = $project->getId(); // for the CriteronSendErrorFrame

        if (is_object($project)) {
            $bans = $this->getProjectBans($phpSession);
            if (array_key_exists($project->getId(), $bans)) {
                if ($bans[$project->getId()] == $project->getId()) {
                    return -1;
                }
            }

            // check if the renderer is working on the client
            // the error have been set on /server/error.php
            $bans = $this->getBinaryBans($phpSession);
            if (array_key_exists($project->getExecutable()->getId(), $bans)) {
                return -1;
            }
        }

        return 0.0;
    }

    public function getProjectBans(?SessionInterface $phpSession): array {
        if (is_null($phpSession) == false && $phpSession->has('ban')) {
            return $phpSession->get('ban');
        }

        return array();
    }

    public function getBinaryBans(?SessionInterface $phpSession): array {
        if (is_null($phpSession) == false && $phpSession->has('ban-executable')) {
            return $phpSession->get('ban-executable');
        }

        return array();
    }
}
