<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * If a shepherd has blacklisted a country, the render session can not upload a frame to that shepherd.
 */
class CriterionSessionCountryBlacklist extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session) && $session->getCountry() != '') {
            $shepherd = $project->getShepherd();
            return in_array($session->getCountry(), explode(',', $shepherd->getCountryExclude())) ? -1.0 : 0;
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Shepherd unavailable';
    }
}
