<?php
/**
 * Copyright (C) 2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectAlreadyDownloaded extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session) && is_object($project)) {
            $previous_project_ids = $session->getPreviousRenderedProjects();
            if (in_array($project->getId(), $previous_project_ids)) {
                return 1.0;
            }
        }
        return 0.0;
    }

}
