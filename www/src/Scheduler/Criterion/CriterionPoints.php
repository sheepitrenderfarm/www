<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionPoints extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $points = $project->getOwnerPointsOnlastupdate();
            if ($points < (-1 * $this->config['rank']['points']['max'])) {
                $points = -1 * $this->config['rank']['points']['max'];
            }
            if ($this->config['rank']['points']['max'] < $points) {
                $points = $this->config['rank']['points']['max'];
            }

            return ($this->config['rank']['points']['max'] + $points) / (2 * $this->config['rank']['points']['max']);
        }

        return 0.0;
    }
}
