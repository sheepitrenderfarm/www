<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Rate limit a project if the owner has 0 points
 */
class CriterionProjectRateLimit extends Criterion {

    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        return $this->score($session, $project);
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if ($this->score($session, $project) < 0.0) {
            return 'Project rate-limited due to lack of points.';
        }
        return '';
    }

    private function score(?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            $owner = $project->getOwner();
            if ($session->getUser()->getId() == $owner->getId()) {
                return 0.0; // owner can render his own project
            }
            elseif ($owner->getPoints() <= 0.0 && count($owner->sessions()) == 0) {
                // rate limit the project
                $stats = $project->getCachedStatistics();
                if ($stats[Constant::PROCESSING] >= $this->config['project']['rate_limit']['max_rendering_tile']) {
                    return -1.0;
                }

            }
        }
        return 0.0;
    }
}