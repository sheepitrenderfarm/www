<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Task;
use App\Entity\TaskProjectUploadToMirror;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionDontGiveProjectNotMirrored extends Criterion {

    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        return $this->isAllowed($session, $project) ? 0.0 : -1.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if ($this->isAllowed($session, $project) == false) {
            return 'Project is not fully synced on all mirrors.';
        }
        else {
            return '';
        }
    }

    private function isAllowed(?Session $session, ?Project $project): bool {
        if (is_object($project)) {
            // if it's the owner, dallas server can handle it
            if ($session->getUser()->getId() == $project->getOwner()) {
                return true;
            }

            // only hold it for 1hour
            if (time() - 3600 < $project->getLastUpdateArchive()) {
                foreach ($this->entityManager->getRepository(Task::class)->findAll() as $t) {
                    if ($t->getProject() == $project->getId() && $t instanceof TaskProjectUploadToMirror) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
