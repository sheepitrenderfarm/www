<?php
/**
 * Copyright (C) 2011 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionMemory extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session) && is_object($project)) {
            $machine_memory = $session->getMemoryAllowed2();
            if ($machine_memory > 0) {
                if ($project->getMaxMemoryUsed() >= $machine_memory) {
                    return -1;
                }
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if (is_object($session) && is_object($project)) {
            $session_memory = $session->getMemoryAllowed2();
            if ($session_memory > 0) {
                $project_ram = $project->getMaxMemoryUsed() * 1000;
                $machine_memory = $session_memory * 1000;
                return 'Not enough free memory, requiring: '.Misc::humanSize($project_ram).'B, available: '.Misc::humanSize($machine_memory).'B';
            }
        }
        return 'Not enough free memory';
    }
}
