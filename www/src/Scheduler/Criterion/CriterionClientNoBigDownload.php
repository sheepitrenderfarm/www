<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionClientNoBigDownload extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session) && is_object($project)) {
            return $session->getNoBigDownload() && $project->getArchiveSize() > Constant::PROJECT_ARCHIVE_LIMIT_BIG ? -1.0 : 0.0;
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'No big archive download on this computer';
    }
}
