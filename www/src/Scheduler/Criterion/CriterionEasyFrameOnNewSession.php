<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionEasyFrameOnNewSession extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        // new session should render easy frame first to encourage people to render more
        // (to increase the session duration)
        if (is_object($session)) {
            if ($session->getRenderedFrames() < 16) {
                $crit = new CriterionRenderTimeRef($this->router, $this->entityManager, $this->config, $this->ponderation());
                return $crit->scoreProject($phpSession, $session, $project);
            }
        }
        return 0.0;
    }
}
