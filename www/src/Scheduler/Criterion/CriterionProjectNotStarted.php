<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectNotStarted extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $stats = $project->getCachedStatistics();
            if (array_key_exists(Constant::FINISHED, $stats) && array_key_exists('total', $stats)) {
                if ($stats['total'] > 0 && $stats[Constant::FINISHED] == 0) {
                    return 1;
                }
            }
        }
        return 0.0;
    }
}
