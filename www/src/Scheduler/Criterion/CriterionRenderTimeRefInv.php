<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionRenderTimeRefInv extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $rendertime = $project->getAverageRenderTimeMachineRef();
            if ($rendertime > 0) {
                return $rendertime / $this->config['power']['rendertime_max_reference'];
            }
        }
        return 0.0;
    }
}
