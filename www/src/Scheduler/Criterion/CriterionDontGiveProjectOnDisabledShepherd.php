<?php
/**
 * Copyright (C) 2020 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\Shepherd;
use App\Repository\ShepherdServerRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionDontGiveProjectOnDisabledShepherd extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            if ($project->getShepherd()->getEnable() == false || $project->getShepherd()->getTimeout()) {
                return -1.0;
            }

            if ($project->getShepherd()->allowNewRenderingFrame() == false) {
                return -1.0;
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if (is_object($project)) {
            if ($project->getShepherd()->getEnable() == 0) {
                return "Project's storage server is disabled";
            }
            if ($project->getShepherd()->getTimeout()) {
                return "Project's storage temporarily unavailable (timeout)";
            }

            // from allowNesRenderingFrame function to get more details
            /** @var ShepherdServerRepository $repo */
            $repo = $this->entityManager->getRepository(Shepherd::class);
            $stats = $repo->getLastStats($project->getShepherd());
            if (is_object($stats) && $project->getShepherd()->getRenderingFrameCount() > $stats->getRenderingFrames() + $this->config['shepherd']['ramp_up']) {
                return "Project's storage server is been rate-limited to protect it";
            }

            if ($project->getShepherd()->allowNewRenderingFrame() == false) {
                return "Project's storage server is overloaded";
            }
        }
        return '';
    }
}
