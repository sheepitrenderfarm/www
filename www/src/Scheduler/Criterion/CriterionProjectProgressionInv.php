<?php
/**
 * Copyright (C) 2013-2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectProgressionInv extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $stats = $project->getCachedStatistics();
            if ($stats[Constant::STATS_TOTAL] > 0) {
                return ($stats[Constant::STATS_TOTAL] - $stats[Constant::FINISHED]) / $stats['total'];
            }
        }
        return 0.0;
    }
}
