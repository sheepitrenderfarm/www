<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionGPUMemory extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session) && is_object($project)) {
            $gpu = $session->getGPU();
            if (is_object($gpu)) {
                $vram = $gpu->getMemory();
                if ($vram > 0) {
                    $current_failure = $project->getVramFailure();
                    if ($current_failure != 0 && $vram < $current_failure) {
                        return -1;
                    }
                }
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Not enough <i>vram</i>';
    }
}
