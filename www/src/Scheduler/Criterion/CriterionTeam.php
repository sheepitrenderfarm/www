<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionTeam extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            $renderer = $session->getUser();
            if ($renderer->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYTEAMSPROJECTSFIRST) == false) {
                return 0.0;
            }
            $renderer_team = $renderer->getTeam();
            if (is_object($renderer_team)) {
                $owner_project = $project->getOwner();
                $owner_team = $owner_project->getTeam();
                if (is_object($owner_team)) {
                    if ($owner_team->getId() == $renderer_team->getId()) {
                        return 1.0;
                    }
                }
            }
        }
        return 0.0;
    }
}
