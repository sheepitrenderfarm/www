<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionFreshProject extends Criterion {
    // check if the project is new i.e. no frame have been rendered yet.
    // the goal is to limit the number of rendering frame at the beginning
    // in case the project is "wrong" (wrong config, render time to high...)
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            if ($project->getOwner()->getId() == $project->getOwner()) {
                return 0.0;
            }

            $stats = $project->getCachedStatistics();
            if ($stats[Constant::FINISHED] != 0) {
                return 0.0;
            }

            if ($stats[Constant::PROCESSING] > 12) {
                return -1;
            }
        }
        return 0.0;
    }
}
