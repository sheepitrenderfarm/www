<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionComputeMethod extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($session) && is_object($project)) {
            $server_side = $project->getComputeMethod();
            $client_side = $session->getComputeMethod();

            if (Misc::isMaskEnabled($server_side, Constant::COMPUTE_CPU) && Misc::isMaskEnabled($client_side, Constant::COMPUTE_CPU) ||
                Misc::isMaskEnabled($server_side, Constant::COMPUTE_GPU) && Misc::isMaskEnabled($client_side, Constant::COMPUTE_GPU)) {
                return 0;
            }
            else {
                return -1;
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        if (is_object($session) && is_object($project)) {
            $server_side = $project->getComputeMethod();
            $client_side = $session->getComputeMethod();

            if (Misc::isMaskEnabled($server_side, Constant::COMPUTE_CPU) && Misc::isMaskEnabled($client_side, Constant::COMPUTE_CPU) == false) {
                return 'Requires CPU';
            }
            if (Misc::isMaskEnabled($server_side, Constant::COMPUTE_GPU) && Misc::isMaskEnabled($client_side, Constant::COMPUTE_GPU) == false) {
                return 'Requires Nvidia Optix GPU';
            }
        }
        return 'Compute device mismatch';
    }
}
