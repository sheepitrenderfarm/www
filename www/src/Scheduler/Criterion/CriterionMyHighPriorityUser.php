<?php
/**
 * Copyright (C) 2015 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionMyHighPriorityUser extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            $renderer = $session->getUser();
            $users = $renderer->getHighPriorityUsers();
            $owner_project = $project->getOwner();
            if (is_object($owner_project)) {
                if (in_array($owner_project->getId(), $users)) {
                    return 1.0;
                }
            }
        }
        return 0.0;
    }
}
