<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectBlocked extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            if ($project->isBlocked()) {
                if (is_object($session) && $session->getUser()->getId() == $project->getOwner()->getId()) {
                    return 0.0;
                }
                return -1;
            }
        }
        return 0.0;
    }
}
