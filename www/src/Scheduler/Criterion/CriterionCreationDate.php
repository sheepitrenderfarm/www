<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionCreationDate extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project)) {
            $now = time();
            $creationtime = $project->getLastUpdateArchive();

            $diff = $now - $creationtime; // in sec
            $diff /= 3600.0; // in hours

            $max = 48.0;

            if ($diff > $max) {
                return 1.0;
            }
            else {
                return $diff / $max;
            }
        }
        return 0.0;
    }
}
