<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Utils\Misc;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectRenderUnder2h extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            $device = $session->getComputeDeviceFor($project->getComputeMethod());
            if (is_object($device) && $project->getCacheFinished() > 0) {
                $power = (float)$device->getPowerPercent();
                if ($power > 0.0) {
                    $coef_power_session = 1.0 / ($power / 100.0);
                    $duration = $coef_power_session * (float)($project->getCacheRendertimeRef()) / (float)($project->getCacheFinished());
                    return $duration > $this->config['session']['timeoutrender'] ? -1.0 : 0.0;
                }
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Cannot be rendered under '.Misc::humanTime($this->config['session']['timeoutrender']);
    }
}
