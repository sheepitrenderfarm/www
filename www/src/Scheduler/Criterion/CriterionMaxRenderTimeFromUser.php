<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionMaxRenderTimeFromUser extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            if ($session->getMaxRendertime() > 0) {
                $avg = $project->getAverageRenderTimeMachineRef();
                if ($avg > 0) {
                    $device = $session->getComputeDeviceFor($project->getComputeMethod());
                    if (is_object($device) && $device->getPower() > 0) {
                        $time_on_device = $avg * $device->getPowerConst() / $device->getPower();

                        // add prep time who is not power dependant
                        $time_on_device += $project->getAveragePrepTime();

                        if ($time_on_device < $session->getMaxRendertime()) {
                            return 1.0;
                        }
                        else {
                            return -1.0;
                        }
                    }
                }
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Over user\'s time limit';
    }
}
