<?php
/**
 * Copyright (C) 2018 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectDoNotGiveEasyForFastComputer extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            if ($session->getScheduler() == '' || $session->getScheduler() == 'default' || $session->getScheduler() == 'best_memory_machine' || $session->getScheduler() == 'best_power_machine') {
                $rendertime = $project->getAverageRenderTimeMachineRef();
                if ($rendertime > 90) {
                    return 1.0;
                }
            }
        }
        return 0.0;
    }
}
