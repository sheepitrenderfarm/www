<?php
/**
 * Copyright (C) 2013 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Entity\Project;
use App\Entity\Session;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionOwner extends Criterion {
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            $renderer = $session->getUser();
            if ($renderer->schedulerIsEnabled(User::SCHEDULER_MASK_RENDERMYPROJECTFIRST) == false) {
                return 0.0;
            }
            if ($session->getUser()->getId() == $project->getOwner()->getId()) {
                return 1.0;
            }
        }
        return 0.0;
    }
}
