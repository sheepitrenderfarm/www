<?php
/**
 * Copyright (C) 2014 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Scheduler\Criterion;

use App\Constant;
use App\Entity\Project;
use App\Entity\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CriterionProjectNew extends Criterion {
    // check if the project is new i.e. no frame have been rendered yet.
    // the goal is to limit the number of rendering frame at the beginning
    // in case the project is "not ok" (wrong config, render time to high...)
    public function scoreProject(?SessionInterface $phpSession, ?Session $session, ?Project $project): float {
        if (is_object($project) && is_object($session)) {
            if ($session->getUser()->getId() == $project->getOwner()->getId()) {
                return 0.0;
            }

            $stats = $project->getCachedStatistics();
            if ($stats[Constant::FINISHED] != 0) {
                return 0.0;
            }

            if ($stats[Constant::PROCESSING] > 10) { // too many people are trying to render this project
                return 1.0;
            }

            if ($stats[Constant::FINISHED] == 0) { // new project
                return 1.0;
            }
        }
        return 0.0;
    }

    public function humanExplanationOnRefusal(?Session $session, ?Project $project): string {
        return 'Project too heavy for this computer.';
    }
}
