<?php

namespace App\Repository;

use App\Entity\PreUser;
use App\Utils\Mail;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<PreUser>
 */
class PreUserRepository extends BaseEntityRepository {
    private UserRepository $userRepository;

    public function __construct(ManagerRegistry $registry, UserRepository $userRepository) {
        parent::__construct($registry, PreUser::class);
        $this->userRepository = $userRepository;
    }

    public function importFromEmail(string $email_): ?PreUser {
        return $this->findOneBy(array('email' => $email_));
    }

    public function checkUserName(string $username): bool {
        if (preg_match('`^([_a-zA-Z0-9-]+)$`', $username)) {

            // no number for account
            if (is_numeric($username)) {
                return false;
            }

            if (strlen($username) <= 3) {
                return false;
            }

            if (is_object($this->userRepository->find($username))) {
                return false;
            }

            if (is_object($this->find($username))) {
                return false;
            }


            return true;
        }
        else {
            return false;
        }
    }

    public function checkEmail(string $email): bool {
        if (\filter_var($email, \FILTER_VALIDATE_EMAIL) && Mail::isSpamDomain($email) == false) {
            return true;
        }
        else {
            return false;
        }
    }

}