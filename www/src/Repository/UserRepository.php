<?php

namespace App\Repository;

use App\Entity;
use App\Entity\Award;
use App\Entity\Event;
use App\Entity\Message;
use App\Entity\Patreon;
use App\Entity\Project;
use App\Entity\ProjectAnalyse;
use App\Entity\SessionError;
use App\Entity\Tile;
use App\Entity\Gift;
use App\Entity\Liaison;
use App\Entity\Session;
use App\Entity\StatsUser;
use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Entity\WebSessionClient;
use App\Service\ConfigService;
use App\Service\Main;
use App\Utils\Misc;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @template-extends BaseEntityRepository<User>
 */
class UserRepository extends BaseEntityRepository {
    private Main $main;
    private array $config;

    public function __construct(ManagerRegistry $registry, Main $main, ConfigService $configService) {
        parent::__construct($registry, User::class);
        $this->main = $main;
        $this->config = $configService->getData();
    }

    /**
     * @return User[]
     */
    public function findFromPattern(string $pattern): array {
        $qb = $this->_em->createQueryBuilder()
            ->select('u')
            ->from($this->_entityName, 'u');

        $qb->where($qb->expr()->like('u.id', ':pattern'));
        $qb->setParameter('pattern', '%'.$pattern.'%');

        return $qb->getQuery()->getResult();
    }

    public function importFromEmail(string $email_): ?User {
        return $this->findOneBy(array('email' => $email_));
    }

    public function remove(Entity|User $user, string $reason = 'unknown'): bool {
        if ($user instanceof User == false) {
            throw new Exception(__METHOD__.' wrong input type '.get_class($user));
        }

        $this->_em->refresh($user);

        $list = $user->sessions();
        foreach ($list as $o) {
            $this->_em->getRepository(Session::class)->remove($o);
        }

        $list = $user->projects();
        foreach ($list as $o) {
            $this->_em->getRepository(Project::class)->remove($o);
        }

        $list = $user->gifts();
        foreach ($list as $o) {
            $this->_em->getRepository(Gift::class)->remove($o);
        }

        $list = $user->getPublicKeys();
        foreach ($list as $o) {
            $this->_em->getRepository(UserPublicKey::class)->remove($o);
        }

        foreach ($this->_em->getRepository(Message::class)->forUser($user) as $o) {
            $this->_em->getRepository(Message::class)->remove($o);
        }


        // ban renderer
        $liaisons = $this->_em->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, $user->getId());
        foreach ($liaisons as $l) {
            $this->_em->getRepository(Liaison::class)->remove($l);
        }

        // my high priority users
        $liaisons = $this->_em->getRepository(Liaison::class)->getLiaisons(Liaison::HIGH_PRIORITY_USER, $user->getId());
        foreach ($liaisons as $l) {
            $this->_em->getRepository(Liaison::class)->remove($l);
        }

        // users who have prioritized me
        foreach ($this->_em->getRepository(Liaison::class)->getLiaisons(Liaison::HIGH_PRIORITY_USER, NULL, $user->getId()) as $l) {
            $this->_em->getRepository(Liaison::class)->remove($l);
        }

        // users who have blacklisted (owner) me
        foreach ($this->_em->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_OWNER, NULL, $user->getId()) as $l) {
            $this->_em->getRepository(Liaison::class)->remove($l);
        }

        // users who have blacklisted (renderer) me
        foreach ($this->_em->getRepository(Liaison::class)->getLiaisons(Liaison::BLOCKED_RENDERER, NULL, $user->getId()) as $l) {
            $this->_em->getRepository(Liaison::class)->remove($l);
        }

        $web_sessions = $this->_em->getRepository(WebSessionClient::class)->findBy(['user' => $user->getId()]);
        foreach ($web_sessions as $web_session) {
            $this->_em->getRepository(WebSessionClient::class)->remove($web_session);
        }

        // awards
        foreach ($user->awards() as $o) {
            $this->_em->getRepository(Award::class)->remove($o);
        }

        //stats
        $this->_em->getRepository(StatsUser::class)->removeForUser($user);

        // event
        $this->_em->getRepository(Event::class)->removeForUser($user);

        // event
        $this->_em->getRepository(SessionError::class)->removeForUser($user);

        // tiles
        $removedUser = $this->_em->getRepository(User::class)->find(User::REMOVED_USER_ID);
        foreach ($this->_em->getRepository(Tile::class)->findBy(['user' => $user->getId()]) as $tile) {
            $tile->setUser($removedUser);
        }

        // team
        if (is_object($user->getTeam()) && $user->getTeam()->getOwner()->getId() == $user->getId()) {
            // admin will be the new owner
            $user->getTeam()->setOwner($this->find('admin'));
            $this->_em->flush($user->getTeam());
        }

        // patreon
        foreach ($this->_em->getRepository(Patreon::class)->findBy(['user' => $user->getId()]) as $p) {
            $this->_em->getRepository(Patreon::class)->remove($p);
        }

        // project analyse
        foreach ($this->_em->getRepository(ProjectAnalyse::class)->findBy(['owner' => $user->getId()]) as $o) {
            $this->_em->getRepository(ProjectAnalyse::class)->remove($o);
        }

        // update best renderers
        $best = $this->main->getBestRenderersFromCache();
        foreach ($best as $k => $dic) {
            if ($dic->getLogin() == $user->getId()) {
                unset($best[$k]);
                break;
            }
        }
        $this->main->getPersistentCache()->set('best_renderer', serialize($best));

        $this->_em->remove($user);
        $this->_em->flush();

        // avatars
        @unlink($this->config['storage']['path'].'users/avatars/small/'.$user->getId().'.png');
        @unlink($this->config['storage']['path'].'users/avatars/big/'.$user->getId().'.png');

        return true;
    }

    /**
     * @return array{rendered_frames: int, render_time: int, ordered_frames: int, ordered_frames_time: int, nb_projects: int}
     */
    public function getSumStats(): array {
        /** @phpstan-ignore-next-line */
        return $this->_em
            ->createQuery("SELECT SUM(u.render_time) AS render_time, SUM(u.rendered_frames) AS rendered_frames, SUM(u.ordered_frames) AS ordered_frames, SUM(u.ordered_frames_time) AS ordered_frames_time, SUM(u.nb_projects) AS nb_projects from App\Entity\User u")
            ->getResult()[0];
    }

    /**
     * @return array<int, array<string, int>>
     */
    public function getRegistrationTimes(): array {
        return $this->_em
            ->createQuery("SELECT u.time from App\Entity\User u")
            ->getResult();
    }

    public function getAffiliates(): array {
        return $this->_em
            ->createQuery("SELECT COUNT(u.affiliate) AS count, u.affiliate from App\Entity\User u GROUP BY u.affiliate")
            ->getResult();
    }

    /**
     * @return Collection<User>
     */
    public function findRegisteredAfter(int $start): Collection {
        $criteria = new Criteria();
        $criteria->where($criteria->expr()->gt('time', $start));
        $criteria->orderBy(['time' => Criteria::DESC]);

        return $this->matching($criteria);
    }

    /**
     * @return User[]
     */
    public function getAllDonors(): array {
        $paypals = array_filter($this->_em->createQuery('SELECT u FROM App\Entity\User u WHERE u.paypal_donation is NOT NULL')->getResult(), function ($user) {
            return $user->isPayPalDonator();
        });
        $patrons = $this->_em->getRepository(Patreon::class)->findActivePatrons();

        return array_unique(array_merge($paypals, $patrons));
    }

    public function updateConsecutiveRenderDays(User $user): void {
        $currentRenderDays = $user->getConsecutiveRenderDaysCurrent();
        if ($currentRenderDays > $user->getConsecutiveRenderDaysMax()) {
            $user->setConsecutiveRenderDaysMax($currentRenderDays);
            $this->_em->flush($user);
        }
    }

    public function resetPassword(User $user): string {
        $new_password = Misc::code(8);
        $user->setPassword(User::generateHashedPassword($user->getId(), $new_password));
        $this->_em->flush($user);
        return $new_password;
    }
}