<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\GPU;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<GPU>
 */
class GPURepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, GPU::class);
    }

    public function findFromData(GPU $object_): ?GPU {
        // if the memory is 4294967296 (4G) most likely a 32bits issue
        if ($object_->getMemory() == 4294967296) {
            return $this->findOneBy(array('model' => $object_->getModel(), 'type' => $object_->getType()));
        }
        else {
            return $this->findOneBy(array('model' => $object_->getModel(), 'type' => $object_->getType(), 'memory' => $object_->getMemory()));
        }
    }
}