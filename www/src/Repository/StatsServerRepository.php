<?php

namespace App\Repository;

use App\Entity\StatsServer;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsServer>
 */
class StatsServerRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsServer::class);
    }

    /**
     * @return StatsServer[]
     */
    public function findAfter(int $time): array {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsServer s WHERE :start < s.id')
            ->setParameter('start', $time)
            ->getResult();
    }

}