<?php
/**
 * Copyright (C) 2024 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\SessionError;
use App\Entity\User;
use App\Persistent\PersistentCache;
use App\Scheduler\ClientError;
use App\Service\Main;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<SessionError>
 */
class SessionErrorRepository extends BaseEntityRepository {
    private PersistentCache $persistentCache;

    public function __construct(ManagerRegistry $registry, Main $main) {
        parent::__construct($registry, SessionError::class);
        $this->persistentCache = $main->getPersistentCache();
    }

    /**
     * @return SessionError[]
     */
    public function last(int $n): array {
        return $this->_em
            ->createQuery('SELECT e FROM App\Entity\SessionError e ORDER BY e.time DESC')
            ->setMaxResults($n)
            ->getResult();
    }

    /**
     * @return array where key is type of error and value is count
     */
    public function countForDay(\DateTime $date): array {
        $key = 'event_count_error_day_'.$date->format('Y-m-d');
        $ret = $this->persistentCache->get($key);
        if ($ret === false) { // no data
            $start = $date->getTimestamp();
            $end = $start + 24 * 3600;
            $data = $this->_em
                ->createQuery("SELECT e.type AS type, COUNT(e.type) AS count FROM App\Entity\SessionError e WHERE :start < e.time AND e.time < :end AND e.type != :ok AND e.type != :empty GROUP BY e.type")
                ->setParameter('start', $start)
                ->setParameter('end', $end)
                ->setParameter('ok', ClientError::TYPE_OK)
                ->setParameter('empty', '')
                ->getArrayResult();

            $ret = [];
            foreach ($data as $values) {
                $ret[$values['type']] = $values['count'];
            }

            if (date('Y-m-d') != $date->format('Y-m-d')) { // never cache data for today
                $this->persistentCache->set($key, $ret);
            }
        }

        return $ret;
    }

    /**
     * Remove cached data older than 31 days ago
     */
    public function removeCache(): void {
        $now = time();

        $key = 'event_count_error_day_'.date('Y-m-d', $now - 86400 * 31);
        $this->persistentCache->remove($key);
    }

    public function removeForUser(User $user): bool {
        $this->_em
            ->createQuery('DELETE FROM App\Entity\SessionError e WHERE e.user = :login')
            ->setParameter('login', $user->getId())
            ->getResult();
        return true;
    }
}