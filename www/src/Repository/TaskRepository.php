<?php

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Task;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Task>
 */
class TaskRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Task::class);
    }

    public function countProjectGenerateStats(Project $project): int {
        return $this->_em
            ->createQuery('SELECT count(t) AS total FROM App\Entity\Task t WHERE t.project = :project AND t INSTANCE OF App\Entity\TaskProjectGenerateStats')
            ->setParameter('project', $project->getId())
            ->getSingleScalarResult();
    }
}