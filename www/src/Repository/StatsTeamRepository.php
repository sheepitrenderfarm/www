<?php

namespace App\Repository;

use App\Entity\StatsTeam;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsTeam>
 */
class StatsTeamRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsTeam::class);
    }

    /**
     * @return StatsTeam[]
     */
    public function findBetween(int $start, int $end): array {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsTeam s WHERE s.date BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getResult();
    }
}