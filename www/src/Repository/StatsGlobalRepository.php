<?php

namespace App\Repository;

use App\Entity\StatsGlobal;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsGlobal>
 */
class StatsGlobalRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsGlobal::class);
    }

    public function findOneBetween(int $start, int $end): ?StatsGlobal {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsGlobal s WHERE :start < s.id AND s.id < :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @return StatsGlobal[]
     */
    public function findAfter(int $time): array {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsGlobal s WHERE s.id > :start')
            ->setParameter('start', $time)
            ->getResult();
    }
}
