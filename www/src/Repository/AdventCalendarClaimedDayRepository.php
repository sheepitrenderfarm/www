<?php

namespace App\Repository;

use App\AdventCalendar\gift\AdventCalendarGiftDay;
use App\AdventCalendar\gift\type\AdventCalendarGiftExtraProject;
use App\AdventCalendar\gift\type\AdventCalendarGiftRatioOwnerPoints;
use App\AdventCalendar\gift\type\AdventCalendarGiftRatioRendererPoints;
use App\AdventCalendar\ui\AdventCalendar;
use App\Entity\AdventCalendarClaimedDay;
use App\Entity\User;
use App\Service\ConfigService;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<AdventCalendarClaimedDay>
 */
class AdventCalendarClaimedDayRepository extends BaseEntityRepository {
    private ConfigService $configService;

    public function __construct(ManagerRegistry $registry, ConfigService $configService) {
        parent::__construct($registry, AdventCalendarClaimedDay::class);
        $this->configService = $configService;
    }

    public function addFor(User $user): void {
        if ($this->count(['date' => new DateTime(), 'user' => $user]) == 0) {
            $rd = new AdventCalendarClaimedDay();
            $rd->setDate(new DateTime());
            $rd->setUser($user);
            $this->_em->persist($rd);
            $this->_em->flush($rd);
            $user->addAdventCalendarClaimedDay($rd);
        }
    }

    public function addForToday(User $user): string {
        // TODO: remove dependency to HTML
//        $date = DateTime::createFromFormat('d/m/Y', $this->HTML->getTimeWithLocalTimeZone('d/m/Y', time()));
//        if ($this->count(['date' => $date, 'user' => $user]) == 0) {
//            $class_name = '\App\AdventCalendar\gift\'AdventCalendarGiftDay'.$date->format('j');
//            if (class_exists($class_name)) {
//                $rd = new AdventCalendarClaimedDay();
//                $rd->setDate($date);
//                $rd->setUser($user);
//                $this->_em->persist($rd);
//                $this->_em->flush($rd);
//                $user->addAdventCalendarClaimedDay($rd);
//
//                /** @var AdventCalendarGiftDay $gift */
//                $gift = new $class_name($user, $this->_em);
//
//                return $gift->onClaim();
//            }
//        }
        return '';
    }

    public function getBestRatioRendererPointsClaimedForUser(User $user): float {
        $best = 1.0;
        if (AdventCalendar::isActive()) {
            foreach ($user->adventCalendarClaimedDays() as $claimedDay) {
                $class_name = '\App\AdventCalendar\gift\AdventCalendarGiftDay'.$claimedDay->getDate()->format('j');
                /** @var AdventCalendarGiftDay $gift */
                $gift = new $class_name($user, $this->_em);
                if ($gift instanceof AdventCalendarGiftRatioRendererPoints) {
                    $best = max($best, $gift->getReward());
                }
            }
        }
        return $best;
    }

    public function getBestRatioOwnerPointsClaimedForUser(User $user): float {
        $best = 1.0;
        if (AdventCalendar::isActive()) {
            foreach ($user->adventCalendarClaimedDays() as $claimedDay) {
                $class_name = '\App\AdventCalendar\gift\AdventCalendarGiftDay'.$claimedDay->getDate()->format('j');
                /** @var AdventCalendarGiftDay $gift */
                $gift = new $class_name($user, $this->_em);
                if ($gift instanceof AdventCalendarGiftRatioOwnerPoints) {
                    $best = min($best, $gift->getReward());
                }
            }
        }
        return $best;
    }

    public function getProjectsAddForUser(User $user): float {
        $best = $this->configService->getData()['project']['max_concurrent_project_per_user'];
        if (AdventCalendar::isActive()) {
            foreach ($user->adventCalendarClaimedDays() as $claimedDay) {
                $class_name = '\App\AdventCalendar\gift\AdventCalendarGiftDay'.$claimedDay->getDate()->format('j');
                /** @var AdventCalendarGiftDay $gift */
                $gift = new $class_name($user, $this->_em);
                if ($gift instanceof AdventCalendarGiftExtraProject) {
                    $best = max($best, $gift->getReward());
                }
            }
        }
        return $best;
    }

    /**
     * @return array key is day of the month
     */
    public function getClaimedPerDay(): array {
        $data = $this->_em
            ->createQuery("SELECT u.date, COUNT(u.date) AS count FROM App\Entity\AdventCalendarClaimedDay u GROUP BY u.date")
            ->getArrayResult();

        $ret = [];
        foreach ($data as $values) {
            $ret[$values['date']->format('j')] = $values['count'];
        }
        return $ret;
    }

    public function getUniqueUsers(): array {
        $data = $this->_em
            ->createQuery("SELECT DISTINCT(u.user) as login FROM App\Entity\AdventCalendarClaimedDay u")
            ->getArrayResult();
        $ret = [];
        foreach ($data as $values) {
            $ret [] = $values['login'];
        }
        return $ret;
    }
}