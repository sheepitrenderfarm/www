<?php

namespace App\Repository;

use App\Entity\StatsUser;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsUser>
 */
class StatsUserRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsUser::class);
    }

    public function removeForUser(User $user): bool {
        $this->_em
            ->createQuery('DELETE FROM App\Entity\StatsUser e WHERE e.user = :login')
            ->setParameter('login', $user->getId())
            ->getResult();
        return true;
    }
}