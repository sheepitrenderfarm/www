<?php

namespace App\Repository;

use App\Constant;
use App\Entity\Frame;
use App\Entity\Tile;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Frame>
 */
class FrameRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Frame::class);
    }

    public function addTiles(Frame $frame, string $tile_classname): int {
        $added = 0;
        for ($i = 0; $i < $frame->getSplit(); $i++) {
            /** @var Tile $tile */
            $tile = new $tile_classname();
            $tile->setFrame($frame);
            $tile->setStatus(Constant::WAITING);
            $tile->setNumber($i);
            $tile->setToken(uniqid('', true));
            $tile->setCost(0.0);
            $tile->setReward(0.0);

            $added++;
            $this->_em->persist($tile);
        }

        return $added;
    }

    public function updateCache(Frame $frame, float $diff_rendertime, float $diff_rendertime_ref, int $diff_cost): void {
        $frame->setCacheCost($frame->getCost() + $diff_cost);
        $frame->setCacheRenderTime($frame->getRenderTime() + $diff_rendertime);
        $frame->setCacheRenderTimeRef($frame->getRenderTimeRef() + $diff_rendertime_ref);

        $total = 0;
        $finished = 0;
        foreach ($frame->tiles() as $tile) {
            if ($tile->getStatus() == Constant::PROCESSING) {
                $frame->setCacheStatus(Constant::PROCESSING);
                return;
            }
            elseif ($tile->getStatus() == Constant::FINISHED) {
                $finished++;
            }
            $total++;
        }
        if ($total == $finished) {
            $frame->setCacheStatus(Constant::FINISHED);
        }
        elseif ($finished > 0) {
            $frame->setCacheStatus(Constant::PROCESSING);
        }
        else {
            $frame->setCacheStatus(Constant::WAITING);
        }
    }
}