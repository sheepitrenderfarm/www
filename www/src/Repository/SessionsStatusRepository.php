<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\SessionsStatus;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<SessionsStatus>
 */
class SessionsStatusRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, SessionsStatus::class);
    }
}