<?php

namespace App\Repository;

use App\Entity\Blender;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Blender>
 */
class BlenderRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Blender::class);
    }

    /**
     * Find all enabled Blender
     * @return Blender[]
     */
    public function findAll(): array {
        $list = $this->findBy(['enable' => true]);
        arsort($list);

        return $list;
    }

    public function getDefault(): Blender {
        return $this->findAll()[0];
    }
}