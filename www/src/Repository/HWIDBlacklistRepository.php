<?php

namespace App\Repository;

use App\Entity;
use App\Entity\HWIDBlacklist;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<HWIDBlacklist>
 */
class HWIDBlacklistRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, HWIDBlacklist::class);

    }

    public function remove(Entity|HWIDBlacklist $blacklist, string $reason = 'unknown'): bool {
        if ($blacklist instanceof HWIDBlacklist == false) {
            return false;
        }

        $this->_em->remove($blacklist);
        $this->_em->flush();

        return true;
    }

    public function add(string $hwid, string $login, int $reason): HWIDBlacklist {
        $blacklist = new HWIDBlacklist();
        $blacklist->setHWID($hwid);
        $blacklist->setLogin($login);
        $blacklist->setReason($reason);

        $search = $this->findOneBy(['hwid' => $hwid]);
        if (is_object($search)) {
            return $search;
        }

        $this->_em->persist($blacklist);
        $this->_em->flush($blacklist);
        return $blacklist;
    }
}