<?php

namespace App\Repository;

use App\Entity;
use App\Service\Logger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use InvalidArgumentException;

/**
 * @template T of object
 * @template-extends ServiceEntityRepository<T>
 */
abstract class BaseEntityRepository extends ServiceEntityRepository {

    public function remove(Entity $o, string $reason = 'unknown'): bool {
        try {
            $this->_em->remove($o);
            $this->_em->flush();
        }
        catch (InvalidArgumentException $e) {
            Logger::error(__METHOD__.':'.__LINE__.' failed '.$e);
            // most likely because the entity is already been removed
        }
        return true;
    }

    /**
     * @param mixed $id The identifier.
     * @param int|null $lockMode One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     * @return T|null
     */
    public function find($id, $lockMode = null, $lockVersion = null): ?object {
        return $id == null ? null : $this->_em->find($this->_entityName, $id);
    }
}