<?php

namespace App\Repository;

use App\Entity\Patreon;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Patreon>
 */
class PatreonRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Patreon::class);
    }

    public function add(Patreon $patreon): bool {
        $this->_em->persist($patreon);
        $this->_em->flush($patreon);

        return true;
    }

    public function giveReward(Patreon $patreon): bool {
        $user = $patreon->getUser();
        if (is_object($user)) {
            $user->setPoints($user->getPoints() + Patreon::REWARD_PER_MONTH);
            $user->giveAward('AwardContributor');
            $this->_em->flush();
            return true;
        }
        return false;
    }

    /**
     * @return User[]
     */
    public function findActivePatrons(): array {
        $ret = [];
        foreach ($this->findAll() as $patron) {
            $user = $patron->getUser();
            if (is_object($user) && $user->isPatreon()) {
                $ret [] = $user;
            }
        }
        return $ret;
    }
}