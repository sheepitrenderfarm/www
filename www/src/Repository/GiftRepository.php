<?php

namespace App\Repository;

use App\Entity\Gift;
use App\Entity\User;
use App\Service\ConfigService;
use App\UI\HTML;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @template-extends BaseEntityRepository<Gift>
 */
class GiftRepository extends BaseEntityRepository {

    /**
     * For logged user
     */
    private Security $security;

    private UrlGeneratorInterface $router;
    private array $config;
    private HTML $html;

    public function __construct(ManagerRegistry $registry, Security $security, UrlGeneratorInterface $router, ConfigService $configService, HTML $html) {
        parent::__construct($registry, Gift::class);
        $this->security = $security;
        $this->router = $router;
        $this->config = $configService->getData();
        $this->html = $html;
    }

    public function generateAccess(Gift $gift): Gift {
        $gift->setExpiration(time() + $this->config['gift']['duration']);

        $len = 7;

        $id = Misc::code($len);
        while (is_object($this->find($id))) {
            $len++;
            $id = Misc::code($len);
        }

        $key = Misc::code(32);

        $gift->setId($id);
        $gift->setKey($key);

        return $gift;
    }

    /**
     * @return bool|string return true for able to be used, and the explanation when it can not be.
     */
    public function canBeUsed(Gift $gift): bool|string {
        if (is_object($this->security->getUser()) == false) {
            return 'You need to be authenticated to use a gift.';
        }

        if ($this->security->getUser()->getUserIdentifier() != $gift->getOwner()->getId()) {
            return 'You are not the owner of the gift.';
        }

        if ($gift->getExpiration() < time()) {
            return 'The gift has expired on '.$this->html->getTimeWithLocalTimeZone('F, jS Y', $gift->getExpiration());
        }

        return true;
    }

    public function take(Gift $gift, ?User $user_): bool {
        if (is_object($user_) == false) {
            return false;
        }

        $user_->setPoints($user_->getPoints() + $gift->getValue());
        $this->remove($gift);
        $this->_em->flush();
        return true;
    }

    public function sendEmail(Gift $gift, string $msg, ?string $subject_ = null): void {
        $gift_url = $this->router->generate('app_gift_show', ['gift' => $gift->getId(), 'key' => $gift->getKey()], UrlGeneratorInterface::ABSOLUTE_URL);
        $text_email = str_replace(array('%%GIFT_URL%%', '%%GIFT_VALUE%%', '%%GIFT_REASON%%'), array($gift_url, number_format($gift->getValue()), $gift->getComment()), $msg);
        $subject = 'You received a gift';
        if (is_null($subject_) == false) {
            $subject = $subject_;
        }

        Mail::sendamail($gift->getOwner()->getEmail(), $subject, $text_email, 'gift');
    }
}