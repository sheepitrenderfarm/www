<?php

namespace App\Repository;

use App\Entity\Liaison;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Liaison>
 */
class LiaisonRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Liaison::class);
    }

    /**
     * @return Liaison[]
     */
    public function getLiaisons(string $type_, string|int|null $primary_ = null, string|int|null $secondary_ = null): array {
//         Logger::debug("Main::getLiaisons($type_,$primary_,$secondary_)");
        if (is_null($primary_) == false and is_null($secondary_) == false) {
            return $this->findBy(array('type' => $type_, 'primary' => $primary_, 'secondary' => $secondary_));
        }
        /** @phpstan-ignore-next-line */
        elseif (is_null($primary_) == false and is_null($secondary_)) {
            return $this->findBy(array('type' => $type_, 'primary' => $primary_));
        }
        elseif (is_null($primary_) == true and is_null($secondary_) == false) {
            return $this->findBy(array('type' => $type_, 'secondary' => $secondary_));
        }
        elseif (is_null($primary_) == true and is_null($secondary_) == true) {
            return $this->findBy(array('type' => $type_));
        }
        else {
            return [];
        }
    }
}