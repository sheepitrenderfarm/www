<?php

namespace App\Repository;

use App\Entity\GiftAnonymous;
use App\Entity\User;
use App\Service\ConfigService;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @template-extends BaseEntityRepository<GiftAnonymous>
 */
class GiftAnonymousRepository extends BaseEntityRepository {
    private array $config;
    private UrlGeneratorInterface $router;

    public function __construct(ManagerRegistry $registry, ConfigService $configService, UrlGeneratorInterface $router) {
        parent::__construct($registry, GiftAnonymous::class);
        $this->config = $configService->getData();
        $this->router = $router;
    }

    public function add(GiftAnonymous $gift): bool {
        $this->_em->persist($gift);
        $this->_em->flush();

        return true;
    }

    public function generateAccess(GiftAnonymous $gift): void {
        $len = 128;

        $id = Misc::code($len);
        while (is_object($this->find($id))) {
            $id = Misc::code($len);
        }

        $gift->setId($id);
    }

    public function take(GiftAnonymous $gift, User $user_): bool {
        $user_->setPoints($user_->getPoints() + $gift->getValue());
        $this->remove($gift);
        $this->_em->flush($user_);

        if ($gift->getType() == GiftAnonymous::TYPE_PATREON) {
            $user_->giveAward('AwardContributor');
        }
        elseif ($gift->getType() == GiftAnonymous::TYPE_PAYPAL) {
            $user_->giveAward('AwardContributor');
        }

        return true;
    }

    public function sendEmail(GiftAnonymous $gift, string $email_contents, string $email_subject, string $email_address): void {
        $text = str_replace(array('%%GIFT_URL%%', '%%GIFT_VALUE%%', '%%GIFT_REASON%%'), array($this->config['site']['url'].$this->router->generate("app_giftanonymous_show", ["gift" => $gift->getId()], UrlGeneratorInterface::ABSOLUTE_PATH), number_format($gift->getValue()), $gift->getComment()), $email_contents);

        Mail::sendamail($email_address, $email_subject, $text, 'gift_anonymous');
    }
}