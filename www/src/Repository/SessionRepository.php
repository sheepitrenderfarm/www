<?php

namespace App\Repository;

use App\Constant;
use App\Entity;
use App\Entity\AwardRandomLootDrop;
use App\Entity\CPU;
use App\Entity\Event;
use App\Entity\Session;
use App\Entity\SessionPast;
use App\Entity\WebSessionClient;
use App\Service\Clustering;
use App\Service\ConfigService;
use App\Service\GlobalInject;
use App\Service\Main;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Session>
 */
class SessionRepository extends BaseEntityRepository {
    private Main $main;
    private array $config;
    private array $cache_cluster;

    public function __construct(ManagerRegistry $registry, Main $main, ConfigService $configService) {
        parent::__construct($registry, Session::class);
        $this->main = $main;
        $this->config = $configService->getData();
        $this->cache_cluster = [];
    }

    public function remove(Entity|Session $session, string $reason = 'unknown'): bool {
        if ($session instanceof Session == false) {
            return false;
        }
        AwardRandomLootDrop::addSession($this->main->getPersistentCache(), $session);

        foreach ($session->tilesWithStatus(Constant::PROCESSING) as $tile) {
            $tile->reset();
        }

        $web_session = $this->_em->getRepository(WebSessionClient::class)->loadFromSessionId($session->getId());
        if (is_object($web_session)) {
            $web_session->saveSessionToData($session);
            $web_session->setExpirationTime(time() + $this->config['web-session-client']['duration']);
            $this->_em->persist($web_session);
        }

        $this->_em->getRepository(Event::class)->removeForSession($session);

        $e = new Event();
        $e->setType($reason); // Event::type_logoff or Event::type_sessionclear
        $e->setSession($session->getId());
        $e->setTime(time());
        $e->setUser($session->getUser()->getId());
        $e->setData(serialize(array(
            'cpu' => $session->getCPU()?->getId(),
            'gpu' => $session->getGPU()?->getId(),
            'memory' => $session->getMemory(),
            'memory_allowed' => $session->getMemoryAllowed(),
            'hwid' => $session->gethwid(),
        )));
        $this->_em->persist($e);

        $session->updateOwnerConsecutiveRenderDays();

        $sessionPast = new SessionPast();
        $sessionPast->setId($session->getId());
        $sessionPast->setUser($session->getUser());
        $sessionPast->setCpu($session->getCpu());
        $sessionPast->setGpu($session->getGpu());
        $sessionPast->setMemory($session->getMemory());
        $sessionPast->setOS($session->getOS());
        $sessionPast->setVersion($session->getVersion());
        $sessionPast->setCreationtime($session->getCreationtime());
        $sessionPast->setEndtime(time());
        $sessionPast->setRenderedframes($session->getRenderedframes());
        $sessionPast->setLastvalidatedframe($session->getLastvalidatedframe());
        $sessionPast->setLastrequestjob($session->getLastrequestjob());
        $sessionPast->setPoints($session->getPoints());
        $sessionPast->sethwid($session->gethwid());
        $sessionPast->setReason($reason);

        $this->_em->persist($sessionPast);

        $this->_em->remove($session);
        $this->_em->flush();

        return true;
    }

    public function countConnectedMachines(): int {
        return $this->count([]);
    }

    /**
     * @return array<int, array{id: int, name: string}>
     */
    public function getProjectsRendered(int $sessionid): array {
        return $this->_em
            ->createQuery("SELECT DISTINCT p.id, p.name FROM App\Entity\Tile t INNER JOIN App\Entity\Frame f WITH f.id = t.frame INNER JOIN App\Entity\Project p WITH f.project = p.id WHERE t.session = :session_id AND t.status = :status")
            ->setParameter('status', Constant::FINISHED)
            ->setParameter('session_id', $sessionid)
            ->getArrayResult();
    }

    /**
     * Get tiles of a session, ordered by validation time
     * @return array<int, array{id: string, validation_time: int, render_time: int, reward: float, project_id: int, project_name: string}>
     */
    public function getTilesRendered(int $sessionid): array {
        return $this->_em
            ->createQuery("SELECT t.id as id, t.validation_time as validation_time, t.render_time as render_time, t.reward as reward, p.id as project_id, p.name as project_name FROM App\Entity\Tile t INNER JOIN App\Entity\Frame f WITH f.id = t.frame INNER JOIN App\Entity\Project p WITH f.project = p.id WHERE t.session = :session_id AND t.status = :status ORDER BY t.validation_time ASC")
            ->setParameter('status', Constant::FINISHED)
            ->setParameter('session_id', $sessionid)
            ->getArrayResult();
    }

    /**
     * Count each connected OS
     * returns ['windows' => 3, 'linux' => 5,...]
     */
    public function countOS(): array {
        $ret = [];
        foreach ($this->_em->createQuery("SELECT t.os, count(t.os) as total FROM App\Entity\Session t GROUP BY t.os")->getArrayResult() as $row) {
            $ret[$row['os']] = $row['total'];
        }
        return $ret;

    }

    /**
     * Count each connected compute_method
     */
    public function countComputeMethod(): array {
        $ret = [];
        foreach ($this->_em->createQuery("SELECT t.compute_method, count(t.compute_method) as total FROM App\Entity\Session t GROUP BY t.compute_method")->getArrayResult() as $row) {
            $ret[$row['compute_method']] = $row['total'];
        }
        return $ret;
    }

    public function getActiveCPU(): array {
        return $this->_em
            ->createQuery('SELECT j FROM App\Entity\Session j WHERE j.power_cpu > 0')
            ->getResult();
    }

    public function getActiveGPU(): array {
        return $this->_em
            ->createQuery('SELECT j FROM App\Entity\Session j WHERE j.power_gpu > 0')
            ->getResult();
    }

    public function getCluster(Session $session, int $compute_method): bool|int {
        if (array_key_exists($compute_method, $this->cache_cluster)) {
            $clusters = $this->cache_cluster[$compute_method];
        }
        else {

            $device = $session->getComputeDeviceFor($compute_method);
            if (is_null($device)) {
                return false;
            }

            $clustering = new Clustering();

            if ($device instanceof CPU) {
                $sessions = GlobalInject::getEntityManager()->getRepository(Session::class)->getActiveCPU();
                $clusters = $clustering->getSameSizeClusters($sessions, "getPowerCpu");
            }
            else {
                // GPU
                $sessions = GlobalInject::getEntityManager()->getRepository(Session::class)->getActiveGPU();
                $clusters = $clustering->getSameSizeClusters($sessions, "getPowerGpu");
            }

            $this->cache_cluster[$compute_method] = $clusters;
        }


        foreach ($this->cache_cluster[$compute_method] as $groupIndex => $group) {
            foreach ($group as $a_session) {
                if ($a_session->getId() == $session->getId()) {
                    return $groupIndex;
                }
            }
        }

        return false;
    }
}