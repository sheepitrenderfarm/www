<?php
/**
 * Copyright (C) 2022 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\Message;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Message>
 */
class MessageRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Message::class);
    }

    /**
     * @return Message[]
     */
    public function forUser(User $user): array {
        return $this->_em
            ->createQuery('SELECT m FROM App\Entity\Message m WHERE m.receiver = :user OR m.sender = :user')
            ->setParameter('user', $user->getId())
            ->getResult();
    }
}