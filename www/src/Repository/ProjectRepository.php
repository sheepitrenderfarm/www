<?php

namespace App\Repository;

use App\CDN\CDNBucket;
use App\Constant;
use App\Entity;
use App\Entity\Frame;
use App\Entity\Liaison;
use App\Entity\PastProject;
use App\Entity\Project;
use App\Entity\TaskProjectRemove;
use App\Entity\TaskProjectUploadToMirror;
use App\Entity\User;
use App\Service\Clustering;
use App\Service\ConfigService;
use App\Service\Daemon;
use App\Service\GlobalInject;
use App\Service\Logger;
use App\Service\Main;
use App\Utils\Mail;
use App\Utils\Misc;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @template-extends BaseEntityRepository<Project>
 */
class ProjectRepository extends BaseEntityRepository {
    protected Main $main;
    protected TaskRepository $taskRepository;
    protected LiaisonRepository $liaisonRepository;
    protected FrameRepository $frameRepository;
    protected PastProjectRepository $pastProjectRepository;
    protected UserRepository $userRepository;
    protected ShepherdServerRepository $shepherdServerRepository;
    protected array $config;
    protected UrlGeneratorInterface $router;
    private Daemon $daemon;
    private array $cache_cluster;

    public function __construct(ManagerRegistry $registry, Main $main, UserRepository $userRepository, ShepherdServerRepository $shepherdServerRepository, TaskRepository $taskRepository, PastProjectRepository $pastProjectRepository, LiaisonRepository $liaisonRepository, FrameRepository $frameRepository, UrlGeneratorInterface $router, ConfigService $configService, Daemon $daemon) {
        parent::__construct($registry, Project::class);
        $this->main = $main;
        $this->userRepository = $userRepository;
        $this->taskRepository = $taskRepository;
        $this->liaisonRepository = $liaisonRepository;
        $this->frameRepository = $frameRepository;
        $this->pastProjectRepository = $pastProjectRepository;
        $this->shepherdServerRepository = $shepherdServerRepository;
        $this->router = $router;
        $this->config = $configService->getData();
        $this->daemon = $daemon;
        $this->cache_cluster = [];
    }

    public function remove(Entity|Project $project, string $reason = 'unknown'): bool {
        if ($project instanceof Project == false) {
            throw new Exception(__METHOD__.' wrong input type '.get_class($project));
        }

        $this->_em->refresh($project);

        $project->getShepherd()->delProject($project);

        // no need, since doctrine will automatically remove
        // foreach ($project->frames() as $frame) {
        //    $this->_em->getRepository(Frame::class)->remove($frame);
        // }

        $pastProject = $this->_em->getRepository(PastProject::class)->find($project->getId());
        if (is_object($pastProject)) {
            $pastProject->setDeletion(time());
        }

        // first remove the future tasks.
        foreach ($this->taskRepository->findAll() as $task) {
            if ($task->getProject() == $project->getId()) {
                $this->taskRepository->remove($task);
            }
        }

        $liaisons = array_merge($this->liaisonRepository->getLiaisons(Liaison::USER_MANAGE_PROJECT, null, $project->getId()), $this->liaisonRepository->getLiaisons(Liaison::USER_RENDER_PROJECT, null, $project->getId()));
        foreach ($liaisons as $a_liaison) {
            $this->liaisonRepository->remove($a_liaison);
        }

        $cache = GlobalInject::getMain()->getPersistentCache();
        foreach ($project->getChunks() as $chunk) {
            @unlink($project->fullPathOfChunk($chunk));
            $cache->remove('chunk_'.$chunk);
        }

        $this->main->getPersistentCache()->remove('project_'.$project->getId());
        $this->main->countPublicActiveProjects(false); // regenerate cache


        foreach ($project->getChunks() as $chunk) {
            (new CDNBucket())->removeFile($project->getChunkStorageName($chunk));
        }

        try {
            $this->_em->remove($project);

        }
        catch (ORMException|ORMInvalidArgumentException $e) {
            Logger::error(__METHOD__.' id\''.$project->getId().'\'  failed'.$e);
            // project might already be removed when TaskProjectRemove was created
        }
        $this->_em->flush();

        return true;
    }

    public function removeDelay(Project $project): bool {
        Logger::debug(__METHOD__.'(for project id='.$project->getId().')');

        // first remove the future tasks.
        foreach ($this->taskRepository->findBy(['project' => $project->getId()]) as $task) {
            $this->taskRepository->remove($task);
        }

        $task = new TaskProjectRemove();
        $task->setProject($project->getId());
        $this->daemon->addTask($task);

        // hide the project from the users
        $project->setOwner($this->userRepository->find(User::REMOVED_USER_ID));

        // pause the project so no more frames are rendered
        $project->setStatus(Constant::PAUSED);
        // do not reset the processing tiles because it will take too much time

        $this->_em->flush();

        return true;
    }

    public function addFrames(Project $project, int $start_frame_, int $end_frame_, int $step_frame_, int $width_, int $height_, int $nb_part_per_frame, string $frame_classname, string $tile_classname, ?string $workdir_dir_ = NULL): int {
        Logger::debug(__METHOD__."(start: $start_frame_, end: $end_frame_, step: $step_frame_, nb_part_per_frame: $nb_part_per_frame, workdir_dir: ".serialize($workdir_dir_).")");

        if (is_string($workdir_dir_)) {
            $task = new TaskProjectUploadToMirror();
            $task->setProject($project->getId());
            $task->setData($workdir_dir_);
            $this->daemon->addTask($task);
        }

        $project->setStatus(Constant::WAITING);
        $project->updateOwnerPointsAttribute();

        $frame_list = array();
        for ($i = $start_frame_; $i <= $end_frame_; $i += $step_frame_) {
            /** @var Frame $a_frame */
            $a_frame = new $frame_classname();
            $a_frame->setSplit($nb_part_per_frame);
            $a_frame->setProject($project);
            $a_frame->setNumber($i);
            $a_frame->setCacheRenderTime(0);
            $a_frame->setCacheRenderTimeRef(0);
            $a_frame->setCacheCost(0.0);

            $this->_em->persist($a_frame);
            $frame_list [] = $a_frame;
        }

        $total_tile = 0;
        foreach ($frame_list as $frame) {
            $total_tile += $this->frameRepository->addTiles($frame, $tile_classname);
        }

        $this->_em->flush();

        $project->generateStatsCache();

        $this->_em->flush();

        if ($project->getShepherd()->addProject($project, $width_, $height_)) {
            return $total_tile;
        }

        // failed to add job
        // try to use the fallback shepherd (only in prod)
        // if it failed rollback and remove the job
        $old_shepherd = $project->getShepherd();

        $fallback = $this->shepherdServerRepository->find('https://shepherd-fallback.sheepit-renderfarm.com');
        if (is_object($fallback) && $fallback->getAllowNewBlend() && $fallback->isEnabled()) {
            $project->setShepherd($fallback);
            $this->_em->flush();
            /** @phpstan-ignore-next-line */
            if ($project->getShepherd()->addProject($project, $width_, $height_)) {
                return $total_tile;
            }
        }

        Logger::error(__METHOD__.' failed to add project on shepherd '.$old_shepherd->getId().', rollback and remove the new project');
        $this->remove($project);

        return -1;
    }

    /**
     * @return Project[]
     */
    public function getAvailableProjects(User $user, int $compute_method, int $max_ram): array {
        if (Misc::isMaskEnabled($compute_method, Constant::COMPUTE_CPU) && Misc::isMaskEnabled($compute_method, Constant::COMPUTE_GPU)) {
            // BOTH
            return $this->_em
                ->createQuery('SELECT j FROM App\Entity\Project j WHERE j.max_memory_used <= :max_ram AND (j.status = :waiting OR j.status = :processing) AND (j.blocked = 0 OR j.owner = :owner)')
                ->setParameter('waiting', Constant::WAITING)
                ->setParameter('processing', Constant::PROCESSING)
                ->setParameter('owner', $user->getId())
                ->setParameter('max_ram', $max_ram)
                ->getResult();
        }
        else {
            // CPU or GPU ONLY
            $compute = Misc::isMaskEnabled($compute_method, Constant::COMPUTE_CPU) ? Constant::COMPUTE_CPU : Constant::COMPUTE_GPU;
            return $this->_em
                ->createQuery('SELECT j FROM App\Entity\Project j WHERE j.max_memory_used <= :max_ram AND j.compute_method = :compute AND (j.status = :waiting OR j.status = :processing) AND (j.blocked = 0 OR j.owner = :owner)')
                ->setParameter('waiting', Constant::WAITING)
                ->setParameter('processing', Constant::PROCESSING)
                ->setParameter('owner', $user->getId())
                ->setParameter('compute', $compute)
                ->setParameter('max_ram', $max_ram)
                ->getResult();

        }

    }

    public function countPublicActiveProjects(): int {
        $ret = 0;
        $rows = $this->_em
            ->createQuery("SELECT DISTINCT (j.id) AS data FROM App\Entity\Project j WHERE (j.status = :waiting OR j.status = :processing) AND j.blocked = 0 AND j.public_render = 1")
            ->setParameter('waiting', Constant::WAITING)
            ->setParameter('processing', Constant::PROCESSING)
            ->getResult();

        foreach ($rows as $row) {
            $project = $this->find($row['data']);
            if (is_object($project)) {
                $ret += 1;
            }
        }
        return $ret;
    }

    public function generateUniqueProjectName(string $name, int $start, int $end): string {
        $maxlength = 64;
        $prefix = $name;
        $suffix = '';
        if (strlen($prefix) + strlen($suffix) > $maxlength) {
            $prefix = substr($prefix, 0, $maxlength - strlen($suffix));
        }

        if ($this->projectNameExists($prefix.$suffix)) {
            if ($start + 1 == $end || $start == $end) {
                $suffix = ' '.$start;
            }
            else {
                $suffix = ' '.$start.'-'.$end;
            }


            if (strlen($prefix) + strlen($suffix) > $maxlength) {
                $prefix = substr($prefix, 0, $maxlength - strlen($suffix));
            }

            while ($this->projectNameExists($prefix.$suffix)) {
                $suffix .= ' '.Misc::code(4);

                if (strlen($prefix) + strlen($suffix) > $maxlength) {
                    $prefix = substr($prefix, 0, $maxlength - strlen($suffix));
                }
            }
        }

        return $prefix.$suffix;
    }

    public function projectNameExists(string $name_): bool {
        return is_object($this->findOneBy(array('name' => $name_)));
    }

    public function block(Project $project, int $reason_): void {
        $project->setBlocked($reason_);
        $this->_em->flush();

        $owner = $project->getOwner();

        $can_render_text = Project::BLOCK_MESSAGES[$project->getBlocked()]['ownercanrender'] ? 'Members can not render your project BUT you still can.' : 'Members (including you) can not render your project.';
        $url_admin = $this->config['site']['url'].$this->router->generate("app_project_manage", ['project' => $project->getId()], UrlGeneratorInterface::ABSOLUTE_PATH);
        $content_user = sprintf("The project %s has been blocked because %s<br>%s", '<a href="'.$url_admin.'">'.$project->getName().'</a>', sprintf(Project::BLOCK_MESSAGES[$reason_]['long'], $this->config['power']['rendertime_max_reference'] / 60), $can_render_text);
        $subject = sprintf("The project '%s' has been blocked", $project->getName());
        Mail::sendamail($owner->getEmail(), $subject, $content_user, 'project_block');
        $project->generateStatsCache();

        $pastProject = $this->pastProjectRepository->find($project->getId());
        if (is_object($pastProject)) {
            $pastProject->setBlocked($reason_);
        }
        $this->_em->flush();
        Logger::info(sprintf("Project %s (id: %d) has been blocked due to: %s", $project->getName(), $project->getId(), Project::BLOCK_MESSAGES[$reason_]['short']));
    }

    public function unblock(Project $project): void {
        $project->setBlocked(0);
        $this->_em->flush();
        $project->generateStatsCache();

        $pastProject = $this->pastProjectRepository->find($project->getId());
        if (is_object($pastProject)) {
            $pastProject->setBlocked(0);
        }
        $this->_em->flush();
    }

    public function findFromChunk(string $part): ?Project {
        $prefix = 'project_';
        $cache = GlobalInject::getMain()->getPersistentCache();
        $project_id = $cache->get('chunk_'.$part);
        if (is_string($project_id) && str_starts_with($project_id, $prefix)) {
            return $this->find(substr($project_id, strlen($prefix)));
        }
        else {
            /** @var ?Project $project */
            $project = $this->_em
                ->createQuery('SELECT p FROM App\Entity\Project p WHERE p.chunks LIKE :search')
                ->setParameter('search', '%'.$part.'%')
                ->setMaxResults(1)
                ->getOneOrNullResult();

            if (is_object($project)) {
                foreach ($project->getChunks() as $chunk) {
                    $cache->set('chunk_'.$chunk, $prefix.$project->getId());
                }
            }

            return $project;
        }
    }

    public function getSizeChunk(int $project_size): int {
        $minimum_splits = 4.0;
        // At least 4 parts
        // Maximum part size at 50MB
        // Example:
        // 2MB   -> 4x 500kB
        // 700MB -> 14x 50MB

        $number_split_parts = (int)($project_size / $minimum_splits);
        $size_split_parts = $project_size > Constant::MAX_CHUNK_SIZE ? Constant::MAX_CHUNK_SIZE : $project_size;

        $chunk_size = min($number_split_parts, $size_split_parts);
        Logger::debug(__METHOD__.' archive '.Misc::humanSize($project_size).' chunk size '.Misc::humanSize($chunk_size));
        return $chunk_size;
    }

    public function createChunks(Project $project, string $archive): bool {
        $cache = GlobalInject::getMain()->getPersistentCache();
        $size_file = filesize($archive);

        $chunk_size = $this->getSizeChunk($size_file) + 1;

        $file = fopen($archive, 'r');
        if ($file === false) {
            return false;
        }

        while (feof($file) == false) {
            $data = fread($file, $chunk_size);
            $md5 = md5($data);

            file_put_contents($project->fullPathOfChunk($md5), $data);

            $project->setChunks(array_merge($project->getChunks(), [$md5]));

            $cache->set('chunk_'.$md5, $project->getId());
        }
        fclose($file);

        $project->setArchiveSize($size_file);
        $project->setMd5(md5_file($archive));

        $this->_em->flush($project);

        return true;
    }

    public function getCluster(Project $project): int {
        if (array_key_exists($project->getComputeMethod(), $this->cache_cluster) == false) {
            $projects = array_merge(
                $this->findBy(['compute_method' => $project->getComputeMethod(), 'status' => Constant::PROCESSING]),
                $this->findBy(['compute_method' => $project->getComputeMethod(), 'status' => Constant::WAITING]));

            $clustering = new Clustering();
            $this->cache_cluster[$project->getComputeMethod()] = $clustering->getSameSizeClusters($projects, "getAverageRenderTimeMachineRef");
        }

//        dd($this->cache_cluster[$project->getComputeMethod()]);

        foreach ($this->cache_cluster[$project->getComputeMethod()] as $groupIndex => $group) {
            foreach ($group as $a_project) {
                if ($a_project->getId() == $project->getId()) {
                    return $groupIndex;
                }
            }
        }


        return Constant::CLUSTER_UNKNOWN;
    }
}