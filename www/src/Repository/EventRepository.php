<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\Session;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Event>
 */
class EventRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Event::class);
    }

    /**
     * @return Event[]
     */
    public function findBetween(int $start, int $end, string $type): array {
        return $this->_em
            ->createQuery('SELECT e FROM App\Entity\Event e WHERE :start < e.time AND e.time < :end AND e.type = :type')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('type', $type)
            ->getResult();
    }


    public function removeForUser(User $user): bool {
        $this->_em
            ->createQuery('DELETE FROM App\Entity\Event e WHERE e.user = :login')
            ->setParameter('login', $user->getId())
            ->getResult();
        return true;
    }

    public function removeForSession(Session $session): bool {
        $this->_em
            ->createQuery('DELETE FROM App\Entity\Event e WHERE e.session = :session_id')
            ->setParameter('session_id', $session->getId())
            ->getResult();
        return true;
    }

    /**
     * @return Event[]
     */
    public function findHWID(string $search): array {
        return $this->_em
            ->createQuery('SELECT e FROM App\Entity\Event e WHERE ( e.type = :logoff OR e.type = :session_clear ) AND e.data LIKE :search')
            ->setParameter('logoff', Event::TYPE_LOGOFF)
            ->setParameter('session_clear', Event::TYPE_SESSIONCLEAR)
            ->setParameter('search', '%'.$search.'%')
            ->getResult();
    }
}