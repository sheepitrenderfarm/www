<?php

namespace App\Repository;

use App\Entity\Donor;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Donor>
 */
class DonorRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Donor::class);
    }
}