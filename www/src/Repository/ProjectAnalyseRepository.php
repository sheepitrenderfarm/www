<?php

namespace App\Repository;

use App\Entity;
use App\Entity\ProjectAnalyse;
use App\Service\Logger;
use App\Utils\Misc;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use InvalidArgumentException;

/**
 * @template-extends BaseEntityRepository<ProjectAnalyse>
 */
class ProjectAnalyseRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ProjectAnalyse::class);
    }

    public function generateToken(): string {
        $token = Misc::code(6);

        while (is_object($this->find($token))) {
            $token .= Misc::code(1);
        }

        return $token;
    }

    public function remove(Entity|ProjectAnalyse $o, string $reason = 'unknown'): bool {
        if ($o instanceof ProjectAnalyse == false) {
            throw new Exception(__METHOD__.' wrong input type '.get_class($o));
        }

        if (file_exists($o->getPath())) {
            unlink($o->getPath());
        }

        try {
            $this->_em->remove($o);
            $this->_em->flush();
        }
        catch (InvalidArgumentException $e) {
            Logger::error(__METHOD__.':'.__LINE__.' failed '.$e);
            // most likely because the entity is already been removed
        }
        return true;
    }
}