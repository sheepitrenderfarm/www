<?php

namespace App\Repository;

use App\Entity\BlenderArch;
use App\Entity\Project;
use App\Entity\Session;
use App\Service\GlobalInject;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<BlenderArch>
 */
class BlenderArchRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, BlenderArch::class);
    }

    public function getFor(Session $session, Project $project): ?BlenderArch {
        return $this->findOneBy(['blender' => $project->getExecutable()->getId(), 'arch' => $session->getOS()]);
    }

    public function findFromChunk(string $part): ?BlenderArch {
        $prefix = 'blenderarch_';
        $cache = GlobalInject::getMain()->getPersistentCache();
        $blenderarch_id = $cache->get('chunk_'.$part);
        if (is_string($blenderarch_id) && str_starts_with($blenderarch_id, $prefix)) {
            return $this->find(substr($blenderarch_id, strlen($prefix)));
        }
        else {
            /** @var ?BlenderArch $blenderArch */
            $blenderArch = $this->_em
                ->createQuery('SELECT p FROM App\Entity\BlenderArch p WHERE p.chunks LIKE :search')
                ->setParameter('search', '%'.$part.'%')
                ->setMaxResults(1)
                ->getOneOrNullResult();
            if (is_object($blenderArch)) {
                foreach ($blenderArch->getChunks() as $chunk) {
                    $cache->set('chunk_'.$chunk, $prefix.$blenderArch->getId());
                }
            }

            return $blenderArch;
        }
    }
}