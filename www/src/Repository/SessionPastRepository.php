<?php

namespace App\Repository;

use App\Entity\SessionPast;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<SessionPast>
 */
class SessionPastRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, SessionPast::class);
    }

    /**
     * @return SessionPast[]
     */
    public function lastFor(User $user, int $limit): array {
        return $this->findBy(['user' => $user], ['creationtime' => 'DESC'], $limit);
    }
}