<?php

namespace App\Repository;

use App\Entity\PastProject;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<PastProject>
 */
class PastProjectRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, PastProject::class);
    }
}