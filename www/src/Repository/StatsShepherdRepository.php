<?php

namespace App\Repository;

use App\Entity\Shepherd;
use App\Entity\StatsShepherd;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsShepherd>
 */
class StatsShepherdRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsShepherd::class);
    }

    public function getLast(Shepherd $shepherd): ?StatsShepherd {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsShepherd s WHERE s.shepherd = :search AND :start < s.time ORDER BY s.time DESC')
            ->setParameter('search', $shepherd->getId())
            ->setParameter('start', time() - 10 * 60)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    /**
     * @return StatsShepherd[]
     */
    public function findAfter(int $time, ?Shepherd $shepherd = null): array {
        if (is_object($shepherd)) {
            return $this->_em
                ->createQuery('SELECT s FROM App\Entity\StatsShepherd s WHERE s.shepherd = :search AND :start < s.time')
                ->setParameter('start', $time)
                ->setParameter('search', $shepherd->getId())
                ->getResult();
        }
        else {
            return $this->_em
                ->createQuery('SELECT s FROM App\Entity\StatsShepherd s WHERE :start < s.time')
                ->setParameter('start', $time)
                ->getResult();
        }
    }

    public function getAverageDiskUsagePerProject(): int {
        $data = [];
        foreach ($this->_em
                     ->createQuery('SELECT s FROM App\Entity\StatsShepherd s WHERE :start < s.time ORDER BY s.time DESC')
                     ->setParameter('start', time() - 10 * 60)
                     ->getResult() as $stats) {
            /** @var StatsShepherd $stats */
            $data[$stats->getShepherd()->getId()] = $stats;
        }

        $disk = 0;
        $projects = 0;
        foreach ($data as $stats) {
            $projects += $stats->getBlends();
            $disk += $stats->getDisk();
        }

        if ($disk != 0 and $projects != 0) {
            return (int)($disk / (float)$projects);
        }
        else {
            // default value taken on feb 2025 from 5000 project on prod
            return 6300000000;
        }
    }
}