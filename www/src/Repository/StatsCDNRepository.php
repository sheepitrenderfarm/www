<?php

namespace App\Repository;

use App\Entity\StatsCDN;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<StatsCDN>
 */
class StatsCDNRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, StatsCDN::class);
    }

    /**
     * @return StatsCDN[]
     */
    public function findAfter(int $time): array {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\StatsCDN s WHERE s.date > :start')
            ->setParameter('start', $time)
            ->getResult();
    }
}