<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\CPU;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<CPU>
 */
class CPURepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, CPU::class);
    }

    public function findFromData(CPU $object_): ?CPU {
        return $this->findOneBy(array('model' => $object_->getModel(), 'cores' => $object_->getCores()));
    }
}