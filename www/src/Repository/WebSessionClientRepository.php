<?php

namespace App\Repository;

use App\Entity\WebSessionClient;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<WebSessionClient>
 */
class WebSessionClientRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, WebSessionClient::class);
    }

    public function findExpirateBefore(int $start): array {
        return $this->_em
            ->createQuery('SELECT s FROM App\Entity\WebSessionClient s WHERE :start < s.expirationTime')
            ->setParameter('start', $start)
            ->getResult();
    }

    public function loadFromSessionId(int $id_): ?WebSessionClient {
        return $this->findOneBy(array('session_id' => $id_));
    }
}