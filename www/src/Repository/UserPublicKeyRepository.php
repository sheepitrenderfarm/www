<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserPublicKey;
use App\Utils\Misc;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<UserPublicKey>
 */
class UserPublicKeyRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, UserPublicKey::class);
    }

    public function add(User $user, string $comment = ''): ?UserPublicKey {
        $id = Misc::code(40);
        while (is_object($this->find($id))) {
            $id .= Misc::code(1);
        }

        $o = new UserPublicKey();
        $o->setTime(time());
        $o->setComment($comment);
        $o->setId($id);
        $o->setUser($user);
        $this->_em->persist($o);
        $this->_em->flush($o);

        return $o;
    }
}