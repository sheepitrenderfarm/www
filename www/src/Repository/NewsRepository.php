<?php
/**
 * Copyright (C) 2021 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

namespace App\Repository;

use App\Entity\News;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<News>
 */
class NewsRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, News::class);
    }
}