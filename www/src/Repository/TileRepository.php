<?php

namespace App\Repository;

use App\Constant;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\Tile;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Tile>
 */
class TileRepository extends BaseEntityRepository {
    protected ProjectRepository $projectRepository;

    public function __construct(ManagerRegistry $registry, ProjectRepository $projectRepository) {
        parent::__construct($registry, Tile::class);
        $this->projectRepository = $projectRepository;
    }

    public function countRenderedSince(int $time): int {
        return $this->_em
            ->createQuery('SELECT count(f) AS total FROM App\Entity\Tile f WHERE f.status = :status AND f.validation_time > :time')
            ->setParameter('status', Constant::FINISHED)
            ->setParameter('time', $time)
            ->getSingleScalarResult();
    }

    public function countAvailablePublicFrame(): int {
        return $this->_em
            ->createQuery('SELECT count(t) AS total FROM App\Entity\Tile t INNER JOIN App\Entity\Frame f WITH f.id = t.frame INNER JOIN App\Entity\Project p WITH f.project = p.id  WHERE t.status = :waiting and p.status = :waiting')
            ->setParameter('waiting', Constant::WAITING)
            ->getSingleScalarResult();
    }

    public function countAvailableFramesForUser(User $user): int {
        if ($user->canDoRendering() == false) {
            return 0;
        }

        // BUG missing private jobs where user is allowed to renderer

        return $this->_em
            ->createQuery("SELECT count(f) AS total FROM App\Entity\Tile t INNER JOIN App\Entity\Frame f WITH f.id = t.frame INNER JOIN App\Entity\Project p WITH f.project = p.id WHERE (p.status = :waiting OR p.status = :processing) AND t.status = :waiting AND ((p.blocked = 0 AND p.owner = :owner) OR (p.blocked = 0 AND p.public_render = 1))")
            ->setParameter('waiting', Constant::WAITING)
            ->setParameter('processing', Constant::PROCESSING)
            ->setParameter('owner', $user->getId())
            ->getSingleScalarResult();
    }

    /**
     * @return Tile[]
     */
    public function getLastRendered(int $limit): array {
        return $this->findBy(['status' => Constant::FINISHED], ['validation_time' => 'DESC'], $limit);
    }

    public function countPublicRemainingFrames(): int {
        return $this->_em
            ->createQuery("SELECT SUM(p.cache_waiting) FROM App\Entity\Project p WHERE (p.status = :waiting or p.status = :processing) AND p.public_render = 1 AND p.blocked = 0")
            ->setParameter('waiting', Constant::WAITING)
            ->setParameter('processing', Constant::PROCESSING)
            ->getSingleScalarResult() ?: 0;
    }

    public function getCountRenderedFramesSinceLast12hours(): int {
        return $this->countRenderedSince(time() - 12 * 3600);
    }

    /**
     * @return Tile[]
     */
    public function tilesFor(Project $project): array {
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin('t.frame', 'f')
            ->innerJoin('f.project', 'p');
        $qb->where($qb->expr()->eq('p.id', ':pattern'));
        $qb->setParameter('pattern', $project->getId());

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Tile[]
     */
    public function tilesWithStatusFor(Project $project, string $status): array {
        //->createQuery("SELECT count(f) AS total FROM App\Entity\Tile f INNER JOIN App\Entity\Project j WITH j.id = f.job INNER JOIN App\Entity\Scene s WITH f.scene = s.id WHERE f.status = :waiting AND j.status = :waiting AND s.status = :waiting AND ((j.blocked = 0 AND s.owner = :owner) OR (j.blocked = 0 AND s.public_render = 1))")
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin('t.frame', 'f')
            ->innerJoin('f.project', 'p');
        $qb->where('p.id = :pattern AND t.status = :status');
        $qb->setParameter('pattern', $project->getId());
        $qb->setParameter('status', $status);
        return $qb->getQuery()->getResult();
    }

    public function findOneWithStatusFor(Project $project, string $status): ?Tile {
        $qb = $this->createQueryBuilder('t')
            ->select('t')
            ->innerJoin('t.frame', 'f')
            ->innerJoin('f.project', 'p');
        $qb->where('p.id = :pattern AND t.status = :status');
        $qb->setParameter('pattern', $project->getId());
        $qb->setParameter('status', $status);
        $qb->orderBy('RAND()');
        $qb->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function countWaitingTileFor(Shepherd $sheperd): int {
        return $this->_em
            ->createQuery("SELECT SUM(p.cache_waiting) FROM App\Entity\Project p WHERE (p.status = :waiting or p.status = :processing) AND p.public_render = 1 AND p.blocked = 0 AND p.shepherd = :search")
            ->setParameter('waiting', Constant::WAITING)
            ->setParameter('processing', Constant::PROCESSING)
            ->setParameter('search', $sheperd->getId())
            ->getSingleScalarResult() ?: 0;
    }

    public function countProcessingTileFor(Shepherd $sheperd): int {
        return $this->_em
            ->createQuery("SELECT SUM(p.cache_processing) FROM App\Entity\Project p WHERE (p.status = :waiting or p.status = :processing) AND p.public_render = 1 AND p.blocked = 0 AND p.shepherd = :search")
            ->setParameter('waiting', Constant::WAITING)
            ->setParameter('processing', Constant::PROCESSING)
            ->setParameter('search', $sheperd->getId())
            ->getSingleScalarResult() ?: 0;
    }


}