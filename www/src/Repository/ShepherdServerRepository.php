<?php

namespace App\Repository;

use App\Constant;
use App\Entity\Project;
use App\Entity\Shepherd;
use App\Entity\StatsShepherd;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Shepherd>
 */
class ShepherdServerRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Shepherd::class);
    }

    public function getRenderingFrameCount(Shepherd $shepherd): int {
        return $this->_em
            ->createQuery('select count(f) as count_attr from App\Entity\Tile t INNER JOIN App\Entity\Frame f WITH f.id = t.frame INNER JOIN App\Entity\Project p WITH f.project = p.id  where t.status = :processing and p.shepherd = :shepherd')
            ->setParameter('shepherd', $shepherd->getId())
            ->setParameter('processing', Constant::PROCESSING)
            ->getSingleScalarResult();

    }

    public function getBlendsCount(Shepherd $shepherd): int {
        return $this->_em
            ->createQuery('SELECT COUNT(j) AS job_count FROM App\Entity\Project j WHERE j.shepherd = :shepherd')
            ->setParameter('shepherd', $shepherd->getId())
            ->getSingleScalarResult();
    }

    public function getLastStats(Shepherd $shepherd): ?StatsShepherd {
        return $this->_em->getRepository(StatsShepherd::class)->getLast($shepherd);
    }

    public function lastProject(Shepherd $shepherd): ?Project {
        return $this->_em
            ->createQuery('SELECT j FROM App\Entity\Project j WHERE j.shepherd = :search ORDER BY j.id DESC')
            ->setParameter('search', $shepherd->getId())
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public function getPredictedStorageUsage(Shepherd $shepherd): int {
        return (int)$this->_em
            ->createQuery('SELECT SUM(p.predicted_size) AS size FROM App\Entity\Project p WHERE p.shepherd = :search')
            ->setParameter('search', $shepherd->getId())
            ->getSingleScalarResult();
    }
}
