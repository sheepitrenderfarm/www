<?php

namespace App\Repository;

use App\Entity\Award;
use App\Utils\Misc;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<Award>
 */
class AwardRepository extends BaseEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Award::class);
    }

    public function countByType(): array {
        $data = $this->_em->createQuery("SELECT TYPE(a), count(TYPE(a)), TYPE(a) AS HIDDEN award_type FROM App\Entity\Award a GROUP BY award_type")
            ->getScalarResult();

        $ret = [];
        foreach ($data as $values) {
            $ret[$values[1]] = $values[2];
        }
        return $ret;
    }

    public function awardsAvailable(): array {
        $awards = array();
        foreach (Misc::findSubClasses(Award::class) as $classname) {
            $awards [] = (new \ReflectionClass($classname))->getShortName();
        }
        return $awards;
    }
}