<?php

namespace App\Repository;

use App\Entity;
use App\Entity\Liaison;
use App\Entity\Team;
use App\Entity\User;
use App\Service\ConfigService;
use App\Service\Logger;
use App\Utils\Mail;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @template-extends BaseEntityRepository<Team>
 */
class TeamRepository extends BaseEntityRepository {
    private array $config;
    private StatsTeamRepository $statsTeamRepository;
    private UserRepository $userRepository;
    private LiaisonRepository $liaisonRepository;
    private UrlGeneratorInterface $router;

    public function __construct(ManagerRegistry $registry, ConfigService $configService, UrlGeneratorInterface $router, StatsTeamRepository $statsTeamRepository, UserRepository $userRepository, LiaisonRepository $liaisonRepository) {
        parent::__construct($registry, Team::class);
        $this->config = $configService->getData();
        $this->router = $router;
        $this->statsTeamRepository = $statsTeamRepository;
        $this->userRepository = $userRepository;
        $this->liaisonRepository = $liaisonRepository;
    }

    public function remove(Entity|Team $team, string $reason = 'unknown'): bool {
        if ($team instanceof Team == false) {
            throw new Exception(__METHOD__.' wrong input type '.get_class($team));
        }

        $members = $team->members();

        foreach ($members as $user) {
            $this->removeMember($team, $user);
        }

        $liaisons = $this->liaisonRepository->getLiaisons(Liaison::TEAM_ASK_JOIN, $team->getId());
        foreach ($liaisons as $l) {
            $this->_em->remove($l);
        }

        foreach ($team->stats() as $o) {
            $this->_em->remove($o);
        }

        $this->_em->remove($team);
        $this->_em->flush();
        return true;
    }

    public function add(Team $team): bool {
        $this->_em->persist($team);
        $this->_em->flush();

        $this->addMember($team, $team->getOwner());

        $this->_em->flush();
        return true;
    }

    /**
     * @return array key is team id, value is count member
     */
    public function getMemberCountPerTeam(): array {
        $data = $this->_em
            ->createQuery("SELECT IDENTITY(u.team) as team, COUNT(u.team) AS count FROM App\Entity\User u WHERE u.team IS NOT NULL GROUP BY u.team")
            ->getArrayResult();

        $ret = [];
        foreach ($data as $values) {
            $ret[$values['team']] = $values['count'];
        }
        return $ret;
    }

    /**
     * @return array<int, array{team: int, points: int}>
     */
    public function getTeamPointsOverDays(int $days = 30): array {
        /** @var array<int, array{team: int, points: int}> $renderer */
        $renderer = array();

        // Init
        foreach ($this->findAll() as $t) {
            $o = array();
            $o['team'] = $t->getId();
            $o['points'] = 0;
            $renderer[$t->getId()] = $o;
        }

        $start_data = array();

        $rows = $this->statsTeamRepository->findBy(['date' => new \DateTime('-'.$days.' day')]);
        if (count($rows) < 10) {
            // missing data, usually a cron who crashed
            $rows = $this->statsTeamRepository->findBy(['date' => new \DateTime('-'.($days + 1).' day')]);
        }
        foreach ($rows as $su) {
            $start_data[$su->getTeam()->getId()] = $su;
        }

        $rows = $this->statsTeamRepository->findBy(['date' => new \DateTime('-1 day')]);
        if (count($rows) < 10) {
            // missing data, usually a cron who crashed
            $rows = $this->statsTeamRepository->findBy(['date' => new \DateTime('-2 day')]);
        }
        foreach ($rows as $su) {
            $o = array();
            $o['team'] = $su->getTeam()->getId();
            if (array_key_exists($su->getTeam()->getId(), $start_data)) {
                $o['points'] = $su->getPoints() - $start_data[$su->getTeam()->getId()]->getPoints();
            }
            else {
                // if the StatsTeam does not exist it's because the team was not create at that time
                // $o->setPoints($su->getPoints());
                $o['points'] = $su->getPoints();
            }

            //$renderer[$su->getTeam()] = $o;
            $renderer[$su->getTeam()->getId()] = $o;
        }

        //arsort($renderer);
        /** @var array<int, array{team: int, points: int}> $renderer */
        usort($renderer, '\App\Utils\Sort::sortTeamPoints');
        return $renderer;
    }

    /**
     * return OK, ERROR_TEAM_FULL, ERROR_WRONG_TEAM
     **/
    public function addMember(Team $team, ?User $user_): int {
        if (is_object($user_)) {
            $wantedTeams = $user_->getWantedTeams();
            $found = false;
            if (count($wantedTeams) == 0) {
                $found = true;
            }
            foreach ($wantedTeams as $wantedTeam) {
                if ($wantedTeam->getId() == $team->getId()) {
                    $found = true;
                }
            }
            if ($found == false) {
                Logger::debug(__METHOD__.' wrong team');
                return Team::ERROR_WRONG_TEAM;
            }

            if (count($team->members()) + 1 > $this->config['team']['max_member']) {
                Logger::error("Team::addMember can not add member, max_member (".$this->config['team']['max_member'].") reached");
                return Team::ERROR_TEAM_FULL;
            }

            $user_->setTeam($team);
            $user_->setTeamJoinTime(time());
            $user_->setTeamContribution(0.0);
            $this->_em->flush($user_);
            $team->__addMember($user_);

            foreach ($user_->getWantedTeams() as $team1) {
                $this->cancelAskJoin($team1, $user_);
            }

            if ($user_->noficationIsEnabled(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM_ACCEPTED)) {
                $content = 'You have been accepted to the team <a href="'.$this->config['site']['url'].$this->router->generate('app_team_single', ['team' => $team->getId()], UrlGeneratorInterface::ABSOLUTE_PATH).'">'.$team->getName().'</a>.';
                Mail::sendamail($user_->getEmail(), 'Welcome to your new team.', $content, 'team_add_member');
            }

            return Team::OK;
        }
        return Team::UNKNOWN;
    }

    public function askForJoin(Team $team_, ?User $user_): int {
        if (is_object($user_)) {
            $team = $user_->getTeam();
            if (is_object($team)) {
                Logger::error("Team::askForJoin($user_) for $team_, ret wrong_team1");
                return Team::ERROR_WRONG_TEAM;
            }

            $liaisons = $this->liaisonRepository->getLiaisons(Liaison::TEAM_ASK_JOIN, $team_->getId(), $user_->getId());
            if (count($liaisons) > 0) {
                return Team::OK;
            }

            $l = new Liaison();
            $l->setType(Liaison::TEAM_ASK_JOIN);
            $l->setPrimary((string)($team_->getId()));
            $l->setSecondary($user_->getId());
            $l->setValue(strval(time()));

            $this->_em->persist($l);
            $this->_em->flush($l);

            if ($team_->getOwner()->noficationIsEnabled(User::NOTIFICATION_MASK_REQUEST_JOIN_TEAM)) {
                $content = 'User <a href="'.$this->config['site']['url'].$this->router->generate('app_user_profile', ['user' => $user_->getId()]).'" >'.$user_->getId().'</a> asked to be part of team <a href="'.$this->config['site']['url'].'/team/'.$team_->getId().'">'.$team_->getName().'</a>.<br>You can accept the request by go in team\'s page.';
                Mail::sendamail($team_->getOwner()->getEmail(), 'User '.$user_->getId().' asked to be part of your team', $content, 'team_ask_join');
            }
            Logger::debug("Team::askForJoin($user_) for ".$team_->getId().", ret OK");
            return Team::OK;
        }
        Logger::error("Team::askForJoin($user_) for ".$team_->getId().", ret unknown4");
        return Team::UNKNOWN;
    }

    public function cancelAskJoin(Team $team, ?User $user_): int {
        if (is_object($user_)) {
            foreach ($this->liaisonRepository->getLiaisons(Liaison::TEAM_ASK_JOIN, $team->getId(), $user_->getId()) as $liaison) {
                $this->liaisonRepository->remove($liaison);
            }

            return Team::OK;
        }
        Logger::debug(__METHOD__." for ".$team->getId().", ret unknown5");
        return Team::UNKNOWN;
    }

    /**
     * @return array<string, int>
     */
    public function getJoinRequests(Team $team): array {
        $ret = array();
        foreach ($this->liaisonRepository->getLiaisons(Liaison::TEAM_ASK_JOIN, $team->getId()) as $liaison) {
            $user = $this->userRepository->find($liaison->getSecondary());
            if (is_object($user) && is_string($liaison->getValue())) {
                $ret[$user->getId()] = intval($liaison->getValue());
            }
        }

        return $ret;
    }

    /**
     * return OK, ERROR_TEAM_FULL, ERROR_WRONG_TEAM
     **/
    public function removeMember(Team $team, ?User $user_): int {
        if (is_object($user_)) {
            if (is_object($user_->getTeam()) && $user_->getTeam()->getId() != $team->getId()) {
                return Team::ERROR_WRONG_TEAM;
            }
            $team->setPointsFormerUser($team->getPointsFormerUser() + $user_->getTeamContribution());

            $user_->setTeam(null);
            $user_->setTeamJoinTime(Team::NO_JOIN);
            $user_->setTeamContribution(0.0);
            $this->_em->flush();

            $team->__removeMember($user_);

            // check if it's the owner who is leaving the team, if so give it back to 'admin'

            if ($user_->getId() == $team->getOwner()->getId()) {
                $team->setOwner($this->userRepository->find('admin'));
                $this->_em->flush($team);
            }

            return Team::OK;
        }
        return Team::UNKNOWN;
    }

    public function changeOwner(Team $team, User $newOwner): bool {
        if (is_object($newOwner->getTeam()) == false || $newOwner->getTeam()->getId() != $team->getId()) {
            return false;
        }

        if ($team->getOwner()->getId() == $newOwner->getId()) {
            Logger::error(__METHOD__.' failed current owner and new owner are the same');
            // already the team owner...
            return false;
        }

        $team->setPointsFormerUser($team->getPointsFormerUser() + $team->getOwner()->getTeamContribution());

        $team->getOwner()->setTeam(null);
        $team->getOwner()->setTeamJoinTime(Team::NO_JOIN);
        $team->getOwner()->setTeamContribution(0.0);
        $this->_em->flush();

        $team->__removeMember($team->getOwner());

        $team->setOwner($newOwner);
        $this->_em->flush($team);

        $url = $this->config['site']['url'].$this->router->generate("app_team_single", ["team" => $team->getId()], UrlGeneratorInterface::ABSOLUTE_PATH);
        $email_subject = 'You are now the owner of the team "'.$team->getName().'"';
        $email_contents = 'Hello,<br>';
        $email_contents .= 'The current owner of your team has left the team. They have selected you as new owner.<br>You can now manage the team.<br>';
        $email_contents .= '<a href="'.$url.'">'.$team->getName().'</a>';
        Mail::sendamail($newOwner->getEmail(), $email_subject, $email_contents, 'sheepit');

        return true;
    }

    public function addContribution(Team $team, ?User $user_, float $value_): bool {
        if (is_object($user_) && is_object($user_->getTeam()) && $user_->getTeam()->getId() == $team->getId() && $value_ > 0) {
            $team->setPoints($team->getPoints() + $value_);

            $user_->setTeamContribution($user_->getTeamContribution() + $value_);
            $this->_em->flush();

            return true;
        }
        return false;
    }
}