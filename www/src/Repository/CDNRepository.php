<?php

namespace App\Repository;

use App\CDN\CDN;
use App\CDN\CDNBucket;
use App\CDN\CDNInternal;

class CDNRepository {

    /**
     * @return CDN[]
     */
    public function findAll(): array {
        $ret = [];

        foreach ([new CDNInternal(), new CDNBucket()] as $cdn) {
            if ($cdn->isConfigurated()) {
                $ret [] = $cdn;
            }
        }

        return $ret;
    }

    public function find(string $id): ?CDN {
        foreach ($this->findAll() as $s) {
            if ($s->getId() == $id) {
                return $s;
            }
        }
        return null;
    }
}