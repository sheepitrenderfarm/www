<?php

namespace App\Repository;

use App\Entity\RenderDay;
use App\Entity\User;
use DateTime;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @template-extends BaseEntityRepository<RenderDay>
 */
class RenderDayRepository extends BaseEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, RenderDay::class);
    }

    public function addFor(User $user): void {
        if ($this->count(['date' => new DateTime(), 'user' => $user]) == 0) {
            $rd = new RenderDay();
            $rd->setDate(new DateTime());
            $rd->setUser($user);
            $this->_em->persist($rd);
            $this->_em->flush($rd);
            $user->addRenderDay($rd);
        }
    }

    public function add(User $user, int $timestamp): void {
        $date = (new DateTime())->setTimestamp($timestamp);
        if ($this->count(['date' => $date, 'user' => $user]) == 0) {
            $rd = new RenderDay();
            $rd->setDate($date);
            $rd->setUser($user);
            $this->_em->persist($rd);
            $this->_em->flush($rd);
            $user->addRenderDay($rd);
        }
    }

    /**
     * @return RenderDay[]
     */
    public function getConsecutiveRenderDaysFor(User $user): array {
        return $this->findBy(['user' => $user]);
    }

    public function fillUpCheeseHole(User $user): DateTime {
        $days = array();

        $min = $user->getRegistrationTime();
        for ($i = time() - 86400; $i >= $min; $i -= 86400) {
            $days[date('Y-m-d', $i)] = false;
        }

        foreach ($this->getConsecutiveRenderDaysFor($user) as $r) {
            $days[$r->getDate()->format('Y-m-d')] = true;
        }

        $last_false = date('Y-m-d', time() - 86400);

        foreach ($days as $day => $status) {
            if ($status == false) {
                $last_false = $day;
                break;
            }
        }

        $date = DateTime::createFromFormat('Y-m-d', $last_false);
        $this->add($user, $date->getTimestamp());

        return $date;
    }
}