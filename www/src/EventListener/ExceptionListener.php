<?php

namespace App\EventListener;

use App\Base\BaseController;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener extends BaseController {
    public function onKernelException(ExceptionEvent $event): void {
        if (
            (array_key_exists('APP_ENV', $_ENV) && $_ENV['APP_ENV'] != 'prod')
//            !$event->isMasterRequest()
            || !$event->getThrowable() instanceof NotFoundHttpException
        ) {
            return;
        }

        $event->setResponse($this->render('404.html.twig', []));
    }
}