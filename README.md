# Main website for SheepIt renderfarm.

## Install:
 * copy blender build to storage/binaries
   * https://sheepit-main-weur-projects.sheepit-r2-binaries.win/blender4.3.1_mac_64bit.zip
   * https://sheepit-main-weur-projects.sheepit-r2-binaries.win/blender4.3.1_macm1_64bit.zip
   * https://sheepit-main-weur-projects.sheepit-r2-binaries.win/blender4.3.1_windows_64bit.zip
   * https://sheepit-main-weur-projects.sheepit-r2-binaries.win/blender4.3.1_linux_64bit.zip
 * use docker compose file provided.
 * go on http://localhost:3000/install to check if everything is ok.

## Credentials

Administrator user is:
* login: admin
* password: sheepit

## Ports
Main website on http://localhost:3000

PHPMyadmin on http://localhost:8000

## Add a real shepherd
If you want a real shepherd:
* go on sql, table 'server_shepherd' and add a row.
  * ID is the public url (like https://shepherd.sheeepit-renderfarm.com)
  * TYPE must be 'real'
* Remove the 'mock' shepherd 

    